<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;
use UniSharp\LaravelFilemanager\Middlewares\CreateDefaultFolder;
use UniSharp\LaravelFilemanager\Middlewares\MultiUser;

$middleware = [CreateDefaultFolder::class, MultiUser::class];
$as         = 'unisharp.lfm.';

Route::group(['middleware' => $middleware, 'prefix' => 'filemanager'], function () {
    // display main layout
    Route::get('/', [
        'uses' => '\UniSharp\LaravelFilemanager\Controllers\LfmController@show',
        'as'   => 'show',
    ]);

    // display integration error messages
    Route::get('/errors', [
        'uses' => '\UniSharp\LaravelFilemanager\Controllers\LfmController@getErrors',
        'as'   => 'getErrors',
    ]);

    // upload
    Route::any('/upload', [
        'uses' => '\UniSharp\LaravelFilemanager\Controllers\UploadController@upload',
        'as'   => 'upload',
    ]);

    // list images & files
    Route::get('/jsonitems', [
        'uses' => '\UniSharp\LaravelFilemanager\Controllers\ItemsController@getItems',
        'as'   => 'getItems',
    ]);

    Route::get('/move', [
        'uses' => '\UniSharp\LaravelFilemanager\Controllers\ItemsController@move',
        'as'   => 'move',
    ]);

    Route::get('/domove', [
        'uses' => '\UniSharp\LaravelFilemanager\Controllers\ItemsController@domove',
        'as'   => 'domove'
    ]);

    // folders
    Route::get('/newfolder', [
        'uses' => '\UniSharp\LaravelFilemanager\Controllers\FolderController@getAddfolder',
        'as'   => 'getAddfolder',
    ]);

    // list folders
    Route::get('/folders', [
        'uses' => '\UniSharp\LaravelFilemanager\Controllers\FolderController@getFolders',
        'as'   => 'getFolders',
    ]);

    // crop
    Route::get('/crop', [
        'uses' => '\UniSharp\LaravelFilemanager\Controllers\CropController@getCrop',
        'as'   => 'getCrop',
    ]);
    Route::get('/cropimage', [
        'uses' => '\UniSharp\LaravelFilemanager\Controllers\CropController@getCropimage',
        'as'   => 'getCropimage',
    ]);
    Route::get('/cropnewimage', [
        'uses' => '\UniSharp\LaravelFilemanager\Controllers\CropController@getNewCropimage',
        'as'   => 'getCropimage',
    ]);

    // rename
    Route::get('/rename', [
        'uses' => '\UniSharp\LaravelFilemanager\Controllers\RenameController@getRename',
        'as'   => 'getRename',
    ]);

    // scale/resize
    Route::get('/resize', [
        'uses' => '\UniSharp\LaravelFilemanager\Controllers\ResizeController@getResize',
        'as'   => 'getResize',
    ]);
    Route::get('/doresize', [
        'uses' => '\UniSharp\LaravelFilemanager\Controllers\ResizeController@performResize',
        'as'   => 'performResize',
    ]);

    // download
    Route::get('/download', [
        'uses' => '\UniSharp\LaravelFilemanager\Controllers\DownloadController@getDownload',
        'as'   => 'getDownload',
    ]);

    // delete
    Route::get('/delete', [
        'uses' => '\UniSharp\LaravelFilemanager\Controllers\DeleteController@getDelete',
        'as'   => 'getDelete',
    ]);
});
Route::group(['middleware' => 'checkActiveUser'], function () {

Route::get('/', 'IndexController@index')->name('home');
Route::post('/dangnhap', 'LoginController@store')->name('auth.login');
Route::get('/dangxuat', 'LoginController@destroy')->name('destroy');
Route::post('/dangky', 'RegisterController@store')->name('auth.register');
Route::get('/dang-ky-tro-thanh-nha-cung-cap', 'BecomeVendorController@getBecome')->name('auth.becomevendor');
Route::post('/dang-ky-tro-thanh-nha-cung-cap', 'BecomeVendorController@store')->name('becomevendor');
Route::get('/danh-sach-khach-san', 'HotelController@index')->name('hotel.ds');
Route::get('/{slug}s-ctks{id}.html', 'HotelController@detail')->name('hotel.ct');
Route::get('/trang-chu-khach-san', 'HotelController@home')->name('hotel.tc');
Route::get('/hotel-favourite/{id}','HotelController@favourite')->name('hotel.favourite');
Route::get('/list-map-hotel.html','HotelController@map')->name('hotel.map');
Route::get('/favouriteTour/{id}','IndexController@favouriteTour')->name('favouriteTour');
Route::get('/favouriteHotel/{id}','IndexController@favouriteHotel')->name('favouriteHotel');
Route::get('/favouriteCar/{id}','IndexController@favouriteCar')->name('favouriteCar');

Route::get('/list-post-home', 'PageArticleController@listHome')->name('post.home');
Route::get('/tin-tuc.html', 'PageArticleController@listArticle')->name('post');
Route::get('/tin-tuc/{slug}-{id}.html', 'PageArticleController@articleDetail')->name('post.detail');
Route::get('search', 'PageArticleController@articleSearch')->name('post.search');
Route::post('comment/{id}', 'PageArticleController@articleComment')->name('post.comment');
Route::get('comment-delete/{id}', 'PageArticleController@commentDelete')->name('post.comment-delete');
Route::post('comment-update/{id}', 'PageArticleController@commentUpdate')->name('post.comment-update');

Route::get('/lien-he.html', 'ContactController@index')->name('lienhe');
Route::post('/lien-he.html', 'ContactController@store')->name('post.lienhe');
Route::get('/list-tour.html', 'TourController@listTour')->name('public.listTour');
Route::get('/{slug}s-ctt{id}.html', 'TourController@detailTour')->name('public.detailTour');
Route::post('/search-tour', 'TourController@searchTour')->name('public.searchTour');
Route::get('/tour.html', 'TourController@indexTour')->name('public.indexTour');
Route::get('/list-map-tour.html', 'TourController@map')->name('tour.map');
Route::get('/tour-favorite/{id}','TourController@favorite')->name('tour.favorite');

Route::get('/list-car.html', 'CarController@listCar')->name('public.listCar');
Route::get('/car.html', 'CarController@indexCar')->name('public.indexCar');
Route::get('/{slug}s-ctx{id}.html', 'CarController@detailCar')->name('public.detailCar');
Route::get('/car-favourite/{id}','CarController@favourite')->name('car.favourite');

Route::get('/list-map-car.html', 'CarController@listMapCar')->name('public.map.listCar');

Route::get('/about.html', 'PageController@about')->name('public.about');
Route::post('/add-newletter.html', 'IndexController@addNewletter')->name('public.add_newletter');

Route::get('/chinh-sach-va-bao-mat.html', 'TermAndPrivacyPolicyController@index')->name('termandprivacypolicy.index');
Route::get('/space-home','PageSpaceController@space')->name('space');
Route::get('/space-detail/{slug}-{id}.html','PageSpaceController@spaceDetail')->name('space.detail');
Route::get('/list-space','PageSpaceController@listSpace')->name('space.list');
Route::get('/space-favourite/{id}','PageSpaceController@spaceFavourite')->name('space.favourite');
Route::get('/list-map-space.html', 'PageSpaceController@listMapSpace')->name('space.spaceMap');
});

Route::get('/admin/login','LoginController@getAdminLogin')->name('get.admin_login');
Route::post('/admin/login','LoginController@postAdminLogin')->name('post.admin_login');
Route::get('/admin/logout', 'LoginController@logoutAdmin')->name('get.admin_logout');

Route::get('{any}', 'UboldController@index');

