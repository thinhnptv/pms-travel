// Dropzone.autoDiscover = false;
jQuery(document).ready(function(){
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    // Summernote
    $('#biographical').summernote({
        height: 180,                 // set editor height
        minHeight: null,             // set minimum height of editor
        maxHeight: null,             // set maximum height of editor
        focus: false                 // set focus to editable area after initializing summernote
    });
    
    $('#content').summernote({
        height: 180,                 // set editor height
        minHeight: null,             // set minimum height of editor
        maxHeight: null,             // set maximum height of editor
        focus: false                 // set focus to editable area after initializing summernote
    });
    
    // Select2
    $('.select2').select2();
   
    
});

$(document).on('click', '#showpass', function (e) {
    if ($(this).hasClass('mdi-eye-outline')) {
        $(this).removeClass('mdi-eye-outline');
        $(this).addClass('mdi-eye-off-outline');
        $('#password').attr('type','text');
    }else{
        $(this).addClass('mdi-eye-outline');
        $(this).removeClass('mdi-eye-off-outline');
        $('#password').attr('type','password');
    }
});
function redireactToUrl(url, params = {}) {
    params = $.param(params)
    location.href = `${url}?${params}`
}

function parseParamsFromUrlToObject(url = '') {
    let search;
    if (url == '') {
        search = location.search.substring(1);
    } else {
        search = new URL(url).search.substring(1);
    }
    result = decodeURI(search).replace(/"/g, '\\"').replace(/&/g, '","').replace(/=/g, '":"')
    try {
        return JSON.parse(`{"${result}"}`);
    } catch (e) { }
}

function search(_this) {
    let value = $(_this).val();
    let key = $(_this).attr('name');
    let paramSearch = parseParamsFromUrlToObject();
    
    if(paramSearch === undefined){
        paramSearch = {};
    }
    
    if (value == 0) {
        try {
            delete paramSearch[key];
        } catch (e) { }
    } else {
        paramSearch[key] = value;
    }
    redireactToUrl('', paramSearch);
}