<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTourPersonTypeBookedsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tour_person_type_bookeds', function (Blueprint $table) {
            $table->string('title')->nullable();
            $table->string('description')->nullable();
            $table->integer('min')->nullable();
            $table->integer('max')->nullable();
            $table->integer('number')->nullable();
            $table->integer('price')->nullable();
            $table->integer('booking_tour_id')->unsigned()->nullable();
            $table->foreign('booking_tour_id')->references('id')->on('booking_tours')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tour_person_type_bookeds');
    }
}
