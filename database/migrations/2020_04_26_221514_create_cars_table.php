<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cars', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->nullable();
            $table->longText('content')->nullable();
            $table->string('thumnail')->nullable();
            $table->longText('album')->nullable();
            $table->integer('passenger')->nullable();
            $table->string('gear_shift')->nullable();
            $table->integer('baggage')->nullable();
            $table->integer('quantily')->nullable();
            $table->integer('price')->nullable();
            $table->integer('sale')->nullable();
            $table->integer('door')->nullable();
            $table->integer('location_id')->unsigned()->nullable();
            $table->foreign('location_id')->references('id')->on('locations')->onDelete('set null');
            $table->string('address')->nullable();
            $table->string('latitu')->nullable();
            $table->string('longtitu')->nullable();
            $table->integer('mapzoom')->nullable();
            $table->string('seo_title')->nullable();
            $table->string('seo_description')->nullable();
            $table->string('slug')->nullable();
            $table->tinyInteger('featured')->nullable();
            $table->tinyInteger('deafault_state')->nullable();
            $table->tinyInteger('status')->default(1);
            $table->tinyInteger('rating_star')->default(5);
            $table->tinyInteger('car_form')->default(1);
            $table->tinyInteger('sale_holiday')->default(0);
            $table->integer('brand_id')->unsigned()->nullable();
            $table->foreign('brand_id')->references('id')->on('car_brands')->onDelete('set null');
            $table->integer('vendor_id')->unsigned()->nullable();
            $table->foreign('vendor_id')->references('id')->on('users')->onDelete('set null');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cars');
    }
}
