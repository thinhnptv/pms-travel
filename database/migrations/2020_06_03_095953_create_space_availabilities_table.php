<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSpaceAvailabilitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('space_availabilities', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('space_id')->unsigned()->nullable();
            $table->foreign('space_id')->references('id')->on('spaces')->onDelete('set null');
            $table->date('date');
            $table->integer('remaining');
            $table->integer('price');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('space_availabilities');
    }
}
