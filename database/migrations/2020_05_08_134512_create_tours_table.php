<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateToursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tours', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->nullable();
            $table->longText('content')->nullable();
            $table->text('description')->nullable();
            $table->integer('category_id')->unsigned()->nullable();
            $table->foreign('category_id')->references('id')->on('tour_categories')->onDelete('set null');
            $table->string('video')->nullable();
            $table->string('duration')->nullable();
            $table->integer('min_people')->nullable();
            $table->integer('max_people')->nullable();
            $table->string('banner')->nullable();
            $table->longText('album')->nullable();
            $table->integer('location_id')->unsigned()->nullable();
            $table->foreign('location_id')->references('id')->on('locations')->onDelete('set null');
            $table->integer('destination_id')->unsigned()->nullable();
            $table->foreign('destination_id')->references('id')->on('destinations')->onDelete('set null');
            $table->string('address')->nullable();
            $table->string('latitu')->nullable();
            $table->string('longtitu')->nullable();
            $table->integer('mapzoom')->nullable();
            $table->string('seo_title')->nullable();
            $table->string('seo_description')->nullable();
            $table->string('slug')->nullable();
            $table->tinyInteger('is_open_hour')->nullable();
            $table->tinyInteger('is_person_type')->nullable();
            $table->tinyInteger('status')->default(1);
            $table->tinyInteger('rating_star')->default(5);
            $table->integer('vendor_id')->unsigned()->nullable();
            $table->foreign('vendor_id')->references('id')->on('users')->onDelete('set null');
            $table->integer('price')->nullable();
            $table->integer('sale')->nullable();
            $table->tinyInteger('is_featured')->nullable();
            $table->tinyInteger('sale_holiday')->default(0);
            $table->string('thumnail')->nullable();
            $table->string('import_url')->nullable();
            $table->string('export_url')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tours');
    }
}
