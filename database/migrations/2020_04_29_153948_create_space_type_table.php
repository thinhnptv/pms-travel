<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSpaceTypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('space_type', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('space_id')->unsigned()->nullable();
            $table->foreign('space_id')->references('id')->on('spaces')->onDelete('cascade');
            $table->integer('space_term_id')->unsigned()->nullable();
            $table->foreign('space_term_id')->references('id')->on('space_terms')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('space_type');
    }
}
