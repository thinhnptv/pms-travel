<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHotelTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hotels', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('location_id')->unsigned()->nullable();
            $table->foreign('location_id')->references('id')->on('locations')->onDelete('set null');
            $table->integer('vendor_id')->unsigned()->nullable();
            $table->foreign('vendor_id')->references('id')->on('users')->onDelete('set null');
            $table->string('name')->nullable();
            $table->tinyInteger('status')->default(1);
            $table->string('address')->nullable();
            $table->string('checkin')->nullable();
            $table->string('checkout')->nullable();
            $table->string('description')->nullable();
            $table->longText('content')->nullable();
            $table->string('title')->nullable();
            $table->string('latitu')->nullable();
            $table->string('longtitu')->nullable();
            $table->string('seo_description')->nullable();
            $table->string('seo_title')->nullable();
            $table->integer('price')->nullable();
            $table->integer ('rating_star')->nullable();
            $table->longText ('album')->nullable();
            $table->string ('banner')->nullable();
            $table->string ('feature')->nullable(); 
            $table->integer ('zoom')->nullable(); 
            $table->integer ('availability')->nullable(); 
            $table->string ('video')->nullable(); 
            $table->integer ('type_id')->unsigned()->nullable(); 
            $table->foreign('type_id')->references('id')->on('hotel_types')->onDelete('set null');
            $table->string ('slug')->nullable(); 
            $table->tinyInteger ('evaluate_star')->default(5); //đánh giá
            $table->integer ('hotel_sale')->nullable(); //giảm giá ks
            $table->tinyInteger ('sale_holiday')->default(0); //đẩy lên trang chủ

            $table->timestamps();

           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hotel');
    }
}
