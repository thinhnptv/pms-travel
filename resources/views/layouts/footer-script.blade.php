        <!-- Vendor js -->
        <script src="{{ URL::asset('assets/js/vendor.min.js')}}"></script>

        @yield('script')

        <!-- App js -->
        <script src="{{ URL::asset('assets/js/app.min.js')}}"></script>
        <script src="{{ URL::asset('assets/libs/toastrnew/toastr.min.js')}}"></script>
        @yield('script-bottom')