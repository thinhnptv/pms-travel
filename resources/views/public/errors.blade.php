{{-- alert  --}}
@if (count($errors))
<div class="form-group">
  <div class="alert alert-danger">
      <ul>
        @foreach ($errors->all() as $error)
            <li>
              {{ $error }}
            </li>
            <script>
              $(document).ready(function() {
                  $.toast({
                      heading: 'Error',
                      text: "{{ $error }}",
                      showHideTransition: 'fade',
                      icon: 'error'
                  });
              });
          </script>
        @endforeach
      </ul>
  </div>
</div>              
@endif
{{-- end alert --}}