@extends('public.master')
@section('content')
<main id="main">
    <section class="section-page section-detail__page">
        <div class="bs-container">
            <div class="bs-row">
                <div class="bs-col">
                    <div class="module module-page">
                        <div class="module-content">
                            <form action="{{ route('public.listTour') }}" method="GET">
                                @csrf
                                <div class="grid">
                                    <div class="item-grid">
                                        <div class="item-grid__header  clickShow">
                                            <div class="item_icon">
                                                <img src="{{url('/')}}/pms/images/icon_map.png" alt="icon_map.png">
                                            </div>
                                            <div class="item__content">
                                                <p class="p__text ">
                                                    <input type="text" class="add-text-destination" placeholder="Nơi khởi hành" readonly>
                                                    <input type="hidden" name="start_destination" class="start_destination">
                                                </p>
                                                <i class="fas fa-caret-down"></i>
                                            </div>
                                        </div>
                                        <div class="item-grid__content">
                                            <ul class="item">
                                                @foreach($listDestination as $item)
                                                    <li data-id="{{ $item->id }}" class="item-list option-start-destination" >
                                                        <i class="fas fa-map-marker-alt"></i>
                                                        <span class="span__name ">
                                                            {{ $item->name }}
                                                        </span>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="item-grid">
                                        <div class="item-grid__header  clickShow">
                                            <div class="item_icon">
                                                <img src="{{url('/')}}/pms/images/icon_map.png" alt="icon_map.png">
                                            </div>
                                            <div class="item__content">
                                                <p class="p__text">
                                                    <input type="text" class="add-text-location" placeholder="Điểm đến" readonly>
                                                    <input type="hidden" name="end_location" class="end_location">
                                                </p>
                                                <i class="fas fa-caret-down"></i>
                                            </div>
                                        </div>
                                        <div class="item-grid__content">
                                            <ul class="item">
                                                @foreach($listLocation as $item)
                                                    <li data-id="{{ $item->id }}" class="item-list option-end-location">
                                                        <i class="fas fa-map-marker-alt"></i>
                                                        <span class="span__name ">
                                                            {{ $item->name }}
                                                        </span>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="item-grid">
                                        <div class="item-grid__header">
                                            <div class="item_icon">
                                                <img src="{{url('/')}}/pms/images/icon_check.png" alt="icon_check.png">
                                            </div>
                                            <div class="item__content">
                                                <p class="p__text acDates">
                                                    <input type="text" name="daterange" readonly />
                                                </p>
                                                <i class="fas fa-caret-down"></i>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="item-grid">
                                        <button class="button black">
                                            Tìm kiếm
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="section-category">
        <div class="bs-container">
            <div class="bs-row">
                <div class="bs-col">
                    <div class="module module-category">
                        <div class="module-header">
                            <ul class="category">
                                <li class="category-list">
                                    <a href="/" class="category-list__link">
                                        Trang chủ
                                    </a>
                                </li>
                                <li class="category-list">
                                    <a href="tour.html" class="category-list__link">
                                        Tour
                                    </a>
                                </li>
                                <li class="category-list">
                                    <a href="#" class="category-list__link">
                                        Phú Quốc
                                    </a>
                                </li>
                                <li class="category-list">
                                    <span class="category-list__link">
                                        {{ $detailTour->title }}
                                    </span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="section-video">
        <iframe width="100%" height="446" src="{{ $detailTour->video }}" frameborder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </section>
    <section class="section-detail">
        <div class="bs-container">
            <div class="bs-row">
                <div class="bs-col">
                    <div class="module module-detail">
                        <div class="module-header clearfix">
                            <div class="header-info">
                                <h2 class="title bold">
                                    {{ $detailTour->title }}
                                </h2>
                                <p class="p__category">
                                    <span class="span__star">
                                        @for ($j = 0; $j < round($detailTour->avgReview, 0); $j++)
                                            <i class="fas fa-star"></i>
                                        @endfor
                                    </span>
                                    <span class="span__number bold">
                                    {{ round($detailTour->avgReview, 0) }}/5
                                    </span>
                                    <span class="span__name bold">
                                        Tuyệt vời
                                    </span>
                                    <span class="span__text">
                                        | {{ sizeof($detailTour->reviewTour) }} đánh giá
                                    </span>
                                </p>
                                <p class="p__address">
                                    <img src="{{url('/')}}/pms/images/icon_map.png" class="img__link" alt="icon_map.png">
                                    <span class="span__name">
                                        {{ $detailTour->address }}
                                    </span>
                                </p>
                            </div>
                            <div class="header-buy">
                                <p class="p__name medium">
                                    Giá tour <span class="span__number black">
                                    @if($detailTour->sale !== null && $detailTour->sale < $detailTour->price)
                                        {{ number_format($detailTour->sale, 0, ",", ".") }} đ
                                    @else
                                        {{ number_format($detailTour->price, 0, ",", ".") }} đ
                                    @endif
                                </span>
                                </p>
                                <a href="#" class="buy__link black">
                                    Đặt tour ngay
                                </a>
                            </div>
                        </div>
                        <div class="module-content">
                            <ul class="selection">
                                <li class="selection-list">
                                    <span class="span__name medium">
                                        Lịch trình:
                                    </span>
                                    {{ $detailTour->duration }}
                                </li>
                                <li class="selection-list">
                                    <span class="span__name medium">
                                        Hình thức:
                                    </span>
                                    {{ optional($detailTour->tourCategory)->name }}
                                </li>
                                <li class="selection-list">
                                    <span class="span__name medium">
                                        Loại tour:
                                    </span>
                                    @foreach($detailTour->termstyle as $item)
                                        {{ $item->name }}
                                    @endforeach
                                </li>
                                <li class="selection-list">
                                    <span class="span__name medium">
                                        Số lượng:
                                    </span>
                                    {{ $detailTour->max_people }} người/tour
                                </li>
                            </ul>
                            <div class="slide-gallria">
                                <div class="galleria">
                                    @if (!empty($detailTour->album))
                                        @foreach (json_decode($detailTour->album) as $item)
                                            <a href="{{url('/')}}{{ $item }}">
                                                <img class="img__links" src="{{url('/')}}{{ $item }}"
                                                    data-big="{{ $item }}">
                                            </a>
                                        @endforeach
                                    @else
                                        <a href="{{ url('/') }}/uploads/images/default.png">
                                            <img class="img__links" src="{{ url('/') }}/uploads/images/default.png"
                                                data-big="{{ url('/') }}/uploads/images/default.png">
                                        </a>
                                    @endif
                                </div>
                            </div>
                            
                            <div class="content">
                                <h3 class="title__info">
                                    Thông tin chi tiết
                                </h3>
                                <div class="p__text">
                                    {!! $detailTour->content !!}
                                </div>
                            </div>
                            <div class="not">
                                <h3 class="title-not medium">
                                    Chú ý:
                                </h3>
                                @foreach($detailTour->exclude as $item)
                                    <p class="p__not">
                                        {{ $item->title }}
                                    </p>
                                @endforeach
                            </div>
                            <div class="journeys">
                                <h3 class="title-journeys">
                                    Hành trình
                                </h3>
                                <div class="bs-row row-lg-10 row-md-5 row-sm-5 row-xs-5 row-tn">
                                    @foreach($detailTour->itinerary as $item )
                                        <div class="bs-col lg-20-5 md-20-5 sm-33-5 xs-50-5 tn-100">
                                            <div class="item-journeys">
                                                <a href="#" class="journeys__link"></a>
                                                <div class="ImagesFrame">
                                                    <div class="ImagesFrameCrop0">
                                                    <img src="{{url('/')}}{{ $item->image }}" alt="{{ $item->title }}">
                                                    </div>
                                                </div>
                                                <div class="journeys__info">
                                                    <span class="span__day bold">
                                                        {{ $item->title }}
                                                    </span>
                                                    <span class="span__text bold">
                                                        {{ $item->description }}
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                            <div class="basis">
                                <h3 class="title-basis">
                                    Cơ sở vật chất, dịch vụ đi kèm
                                </h3>
                                <ul class="item-basis clearfix">
                                    @foreach($detailTour->include as $item)
                                        <li class="item-basis__list">
                                            <i class="fas fa-check"></i>
                                            {{ $item->title }}
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                            <div class="question">
                                <h3 class="title-question">
                                    Câu hỏi thường gặp
                                </h3>

                                <div class="panel-group" id="accordion">

                                    @foreach($detailTour->faq as $item)
                                        <div class="panel">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" data-parent="#accordion" href="#question-{{ $item->id }}">
                                                        <img src="{{url('/')}}/pms/images/icon_message.png" alt="icon_message.png"
                                                            class="icon icon__message">
                                                            {{ $item->title }}
                                                        <i class="fas fa-angle-down"></i>
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="question-{{ $item->id }}" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    {!! $item->content !!}
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                            <div class="evaluate">
                                <h3 class="title-evaluate">
                                    Đánh giá
                                </h3>
                                <div class="evaluate-container">
                                    <div class="bs-row row-lg-10 row-md-10 row-ms-5 row-xs">
                                        <div class="bs-col lg-25-10 md-25-10 sm-30-5 xs-100">
                                            <div class="item">
                                                <span class="span__number medium">
                                                    {{ $detailTour->avgReview }}<sub>/5</sub>
                                                </span>
                                                <p class="p__text light">
                                                    Tuyệt vời
                                                </p>
                                                <p class="p__evaluate">
                                                    Dựa trên 3 đánh giá
                                                </p>
                                            </div>
                                        </div>
                                        <div class="bs-col lg-75-10 md-75-10 sm-70-5 xs-100">
                                            <div class="evaluate-content">
                                                <p class="p__evaluate ">
                                                    <span class="span__name">
                                                        Tuyệt vời
                                                    </span>
                                                    <span class="span__number">
                                                        3
                                                    </span>
                                                </p>
                                                <p class="p__evaluate ">
                                                    <span class="span__name">
                                                        Rất tốt
                                                    </span>
                                                    <span class="span__number">
                                                        0
                                                    </span>
                                                </p>
                                                <p class="p__evaluate">
                                                    <span class="span__name">
                                                        Cũng được
                                                    </span>
                                                    <span class="span__number">
                                                        0
                                                    </span>
                                                </p>
                                                <p class="p__evaluate">
                                                    <span class="span__name">
                                                        Trung bình
                                                    </span>
                                                    <span class="span__number">
                                                        0
                                                    </span>
                                                </p>
                                                <p class="p__evaluate">
                                                    <span class="span__name">
                                                        Kém
                                                    </span>
                                                    <span class="span__number">
                                                        0
                                                    </span>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="comment">
                                @foreach($reviewTour as $item)
                                    <div class="comment-container">
                                        <div class="avata">
                                            <div class="ImagesFrame">
                                                <div class="ImagesFrameCrop0">
                                                    <img src="{{url('/')}}{{ $item->user->avatar }}" alt="{{ $item->user->lastname }}">
                                                </div>
                                            </div>

                                        </div>
                                        <div class="content">

                                            <h3 class="title__name bold">
                                                {{ $item->user->firstname }} {{ $item->user->lastname }}
                                                <span class="span__star">
                                                    @for ($stars = 0; $stars < $item->average; $stars++)
                                                        <i class="fas fa-star"></i>
                                                    @endfor
                                                </span>
                                            </h3>
                                            <p class="p__day">
                                                {{ $item->created_at }}
                                            </p>
                                            <p class="p__content">
                                                {!! $item->content !!}
                                            </p>
                                        </div>
                                    </div>
                                @endforeach
                                <div class="fexPagintaion text-center clearfix">
                                    {{ $reviewTour->links() }}
                                </div>
                                <div class="comment-footer">
                                    <p class="p__total">
                                        Hiển thị {{ $reviewTour->firstItem().'-'. $reviewTour->lastItem() }} trong tổng số {{ $reviewTour->total() }} đánh giá
                                    </p>
                                    <p class="p__login">
                                        Bạn cần phải <a href="#" class="span__name">đăng nhập</a> để đánh giá chuyến đi
                                    </p>
                                </div>
                            </div>
                            <div class="like">
                                <h3 class="title-like">
                                    Có thể bạn cũng thích
                                </h3>
                                @php
                                    // dd($listTourInvolve);
                                @endphp
                                <div class="bs-row row-lg-10 row-md-10 row-sm-5 row-xs-5">
                                    @foreach ($listTourInvolve as $item)
                                        <div class="bs-col lg-25-10 md-25-10 sm-33-5 xs-50-5 tn-100">
                                            <div class="item-like">
                                                <div class="avata">
                                                    <a href="{{ route('public.detailTour', [$item->slug , $item->id]) }}" class="item__links"></a>
                                                    <div class="ImagesFrame">
                                                        <div class="ImagesFrameCrop0">
                                                            <img src="{{url('/')}}{{ $item->thumnail }}" alt="{{ $item->title }}">
                                                        </div>
                                                    </div>
                                                    @if($item->sale !== null && $item->sale < $item->price)
                                                        <span class="span__sale">
                                                            <span class="span__number">
                                                                {{ round((($item->price - $item->sale)/$item->price)*100) }}%
                                                            </span>
                                                        </span>
                                                    @endif
                                                    <a class="span___love" href="#">
                                                        <i class="fas fa-heart"></i>
                                                        {{ sizeof($item->wishListTour) }}
                                                    </a>
                                                </div>
                                                <div class="content">
                                                    <div class="content-header">
                                                        <h2 class="title">
                                                            <a href="{{ route('public.detailTour', [$item->slug , $item->id]) }}" class="title__links">
                                                                <span class="span__name medium">
                                                                    {{ $item->title }}
                                                                </span>
                                                                <p class="p__text">
                                                                    {{ $item->description }}
                                                                </p>
                                                            </a>
                                                        </h2>
                                                        <p class="p__price">
                                                            GIÁ:
                                                            <span class="span__price bold">
                                                                @if($item->sale !== null && $item->sale < $item->price)
                                                                    {{ number_format($item->sale, 0, ",", ".") }} đ
                                                                @else
                                                                    {{ number_format($item->price, 0, ",", ".") }} đ
                                                                @endif
                                                            </span>
                                                        </p>
                                                    </div>
                                                    <div class="content-footer">
                                                        <div class="text">
                                                            <p class="p__time">
                                                                <img src="{{url('/')}}/pms/images/icon_time.png" alt="icon_time.png" class="icon">
                                                                <span class="span__name medium">
                                                                    Lịch trình:
                                                                </span>
                                                                {{ $item->duration }} ngày
                                                            </p>
                                                        </div>
                                                        <div class="text">
                                                            <p class="p__time">
                                                                <img src="{{url('/')}}/pms/images/icon_check.png" alt="icon_check.png" class="icon">
                                                                <span class="span__name  medium">
                                                                    Ngày đi:
                                                                </span>
                                                                @if(!$item->startDateTour->isEmpty())
                                                                    {{ $item->startDateTour['0']['start_date'] }}
                                                                @endif
                                                            </p>
                                                        </div>
                                                        <div class="text">
                                                            <p class="p__time">
                                                                <img src="{{url('/')}}/pms/images/icon_map.png" alt="icon_map.png" class="icon">
                                                                <span class="span__name  medium">
                                                                    Khởi hành: </span>
                                                                    {{ optional($item->location)->name }}
                                                            </p>
                                                        </div>
                                                        <div class="like-dvaluate">
                                                            <p class="p__like">
                                                                @for ($j = 0; $j < round($item->avgReview, 0); $j++)
                                                                    <i class="fas fa-star"></i>
                                                                @endfor
                                                            </p>
                                                            <p class="p__like medium">
                                                                {{ sizeof($item->reviewTour) }} đánh giá
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </section>
</main>
@endsection
@push('js')
<script src="{{url('/')}}/pms/js/galleria.js"></script>
<script>

    $(function () {
        // Load the Azur theme
        Galleria.loadTheme('{{url('/')}}/pms/'+'js/galleria.azur.js');

        // Initialize Galleria
        Galleria.run('.galleria');
    });
</script>
<script type="text/javascript">
    $(document).ready(function () {
        function countdown() {
            $('.clock').countdown('2020/10/03', function (event) {
                $(this).html(event.strftime(' <span> %H</span> Giời <span> :%M </span> Phút <span>:%S </span> Giây'));
            });
        }
        function checkRadido() {
            $('.lable__radio').click(function () {
                $(this).prevAll('.lable__radio').removeClass('active');
                $(this).nextAll('.lable__radio').removeClass('active');
                $(this).addClass('active');
            });
        }
        // hien thi danh sach turs 
        function onDisplay() {
            $('.fa-th-large').click(function () {
                $('.fa-th-list').removeClass('active');
                $(this).addClass('active');
                $('.onDisplay').addClass('active');
                $('.open-show__avata').removeClass('lg-33 md-33 sm-33 xs-50 ');
                $('.open-show__content').removeClass('lg-66 md-66 sm-33 xs-50');
                $('.open-show__avata').addClass('lg-100 md-100 sm-100 xs-100');
                $('.open-show__content').addClass('lg-100 md-100 sm-100 xs-100');
            })
            $('.fa-th-list').click(function () {
                $('.onDisplay').removeClass('active');
                $('.fa-th-large').removeClass('active');
                $(this).addClass('active');
                $('.open-show__avata').removeClass('lg-100 md-100 sm-100 xs-100');
                $('.open-show__content').removeClass('lg-100 md-100 sm-100 xs-100');
                $('.open-show__avata').addClass('lg-33 md-33 sm-33 xs-50');
                $('.open-show__content').addClass('lg-66 md-66 sm-33 xs-50');
            });
        }

        function selecOption() {
            $('.select .header-option').click(function () {
                $(this).next('.body-option').toggleClass('active');
                var header = $(this);
                $(this).next('.body-option').children('.item-option').click(function(){
                var html = $(this).html();
                header.html(html);
                $('.body-option').removeClass('active');
                });
                
            });

        }
        onDisplay();
        checkRadido();
        selecOption();
        countdown();
    });
</script>
<script>
    $(document).on('click', '.option-start-destination', function(e) {
        let text = $(this).text().trim();
        let id = $(this).attr('data-id');
        $(".add-text-destination").val(text);
        $(".start_destination").val(id);
        $(".item-grid__content").removeClass("active");
    });
    $(document).on('click', '.option-end-location', function(e) {
        let text = $(this).text().trim();
        let id = $(this).attr('data-id');
        $(".add-text-location").val(text);
        $(".end_location").val(id);
        $(".item-grid__content").removeClass("active");
    });
</script>
@endpush