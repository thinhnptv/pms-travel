@extends('public.master')
@section('content')
<main id="main">
    <section class="section-page">
        <div class="bs-container">
            <div class="bs-row">
                <div class="bs-col">
                    <div class="module module-page">
                        <div class="module-header">
                            <h2 class="title">
                                <span class="span__name black">
                                    Tìm kiếm tours du lịch
                                </span>
                                
                            </h2>
                            <div class="page__right">
                                <ul class="item">
                                    <li class="item-list">
                                        <a href="/" class="item-list__links">
                                            Trang chủ
                                        </a>
                                    </li>
                                    <li class="item-list">
                                        <span class="item-list__links">
                                            tours
                                        </span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="module-content">
                            <form action="{{ route('public.listTour') }}" method="GET">
                                <div class="grid">
                                    <div class="item-grid">
                                        <div class="item-grid__header  clickShow">
                                            <div class="item_icon">
                                                <img src="{{ url('/') }}/pms/images/icon_map.png" alt="icon_map.png">
                                            </div>
                                            <div class="item__content">
                                                <h3 class="title">
                                                    Nơi khởi hành
                                                </h3>
                                                <p class="p__text addText">
                                                    <input type="text" class="add-text-destination" placeholder="Bạn muốn đi đâu" readonly>
                                                    <input type="hidden" name="start_destination" class="start_destination">
                                                </p>
                                                <i class="fas fa-caret-down"></i>
                                            </div>
                                        </div>
                                        <div class="item-grid__content">
                                            <ul class="item">
                                                @foreach($listDestination as $item)
                                                    <li data-id="{{ $item->id }}" class="item-list option-start-destination">
                                                        <i class="fas fa-map-marker-alt"></i>
                                                        <span class="span__name ">
                                                            {{ $item->name }}
                                                        </span>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="item-grid">
                                        <div class="item-grid__header">
                                            <div class="item_icon">
                                                <img src="{{ url('/') }}/pms/images/icon_check.png" alt="icon_check.png">
                                            </div>
                                            <div class="item__content">
                                                <h3 class="title">
                                                    Check In - Check Out
                                                </h3>
                                                <p class="p__text acDates">

                                                    <input type="text" name="daterange" readonly>
                                                </p>
                                                <i class="fas fa-caret-down"></i>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="item-grid">
                                        <div class="item-grid__header clickShow">
                                            <div class="item_icon">
                                                <img src="{{ url('/') }}/pms/images/icon_us.png" alt="icon_us.png">
                                            </div>
                                            <div class="item__content">
                                                <h3 class="title">
                                                    Số lượng khách
                                                </h3>
                                                <p class="p__text">
                                                    <input type="text" class="text-number_people"  placeholder="2 Người lớn - 1 Trẻ em" readonly>
                                                    <input type="hidden" name="number_people" class="number_people">
                                                </p>
                                                <i class="fas fa-caret-down"></i>
                                            </div>
                                        </div>
                                        <div class="item-grid__content">
                                            <ul class="item">
                                                <li class="item-list">
                                                    <span class="span__name bold">
                                                        Người lớn
                                                    </span>
                                                    <div class="number">
                                                        <span class="span span__subtract bold" data-people="adults">
                                                            -
                                                        </span>
                                                        <span class="span span__show number_adult">
                                                            2
                                                        </span>
                                                        <span class="span span__add bold " data-people="adults">
                                                            +
                                                        </span>
                                                    </div>
                                                </li>
                                                <li class="item-list">
                                                    <span class="span__name bold">
                                                        Trẻ em
                                                        
                                                    </span>
                                                    <div class="number">
                                                        <span class="span span__subtract_childent bold" data-people="children">
                                                            -
                                                        </span>
                                                        <span class="span span__show number_children">
                                                           1
                                                        </span>
                                                        <span class="span span__add bold" data-people="children">
                                                            +
                                                        </span>
                                                    </div>
                                                </li>
                                                <li class="d-flex justify-content-end"><a href="javascript:void(0)" class="btn btn-sm btn-primary btn-apply-number-people">Apply</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="item-grid">
                                        <button class="button">
                                            <img src="{{ url('/') }}/pms/images/icon_search.png" alt="icon_search.png">
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="section-topDestination">
        <div class="bs-container">
            <div class="bs-row">
                <div class="bs-col">
                    <div class="module module-topDestination">
                        <div class="module-header">
                            <h2 class="title">
                                Điểm đến hàng đầu
                            </h2>
                        </div>
                        <div class="module-content">
                            <div class="bs-row row-lg-10 row-md-10 row-sm-10 row-xs-5">
                                {{-- @php
                                    dd($locationFeatured);
                                @endphp --}}
                                @foreach ($locationFeatured as $item)
                                    <div class="bs-col lg-33-10 md-33-10 sm-33-10 xs-50-5">
                                        <div class="container-box">
                                            @if (!empty($item->feature_image))
                                                <img src="{{ url('/') }}{{ $item->feature_image }}" alt="{{ $item->name }}">
                                            @else
                                                <img src="{{ url('/') }}/uploads/images/default.png" alt="{{ $item->name }}">
                                            @endif
                                            
                                            <div class="content">
                                                <h3 class="h3_title bold">
                                                    {{ $item->name }}
                                                </h3>
                                                <ul class="item">
                                                    <li class="item-list">
                                                        <a href="{{ route('public.listTour', ['end_location' => $item->id]) }}"><b>{{ sizeof($item->tour) }}</b> Tours</a>
                                                    </li>
                                                    <li class="item-list">
                                                        <a href="{{ route('hotel.ds', ['location_id' => $item->id]) }}"><b>{{ sizeof($item->hotel) }}</b> Hotels</a>
                                                    </li>
                                                    <li class="item-list">
                                                        <a href="{{ route('public.listCar', ['location_id' => $item->id]) }}"><b>{{ sizeof($item->car) }}</b> Cars</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="section-commitment">
        <div class="bs-container">
            <div class="bs-row">
                <div class="bs-col">
                    <div class="module module-commitment">
                        <div class="content">
                            <div class="bs-row">
                                <div class="bs-col lg-33 md-33 sm-33 xs-50">
                                    <div class="commitment ">
                                        <h3 class="title bold">
                                            Giá tour ưu đãi
                                        </h3>
                                        <p class="content__text">
                                            Nhiều khuyến mại, ưu đãi hấp dẫn, khách hàng hưởng nhiều dịch vụ giá tốt
                                            nhất.
                                        </p>
                                    </div>
                                </div>
                                <div class="bs-col lg-33 md-33 sm-33 xs-50">
                                    <div class="commitment">
                                        <h3 class="title bold">
                                            Uy tín & Chuẩn mực
                                        </h3>
                                        <p class="content__text">
                                            Nhiều khuyến mại, ưu đãi hấp dẫn, khách hàng hưởng nhiều dịch vụ giá tốt
                                            nhất.
                                        </p>
                                    </div>
                                </div>
                                <div class="bs-col lg-33 md-33 sm-33 xs-100">
                                    <div class="commitment">
                                        <h3 class="title bold">
                                            Nhiệt tình & Tận tâm
                                        </h3>
                                        <p class="content__text">
                                            Nhiều khuyến mại, ưu đãi hấp dẫn, khách hàng hưởng nhiều dịch vụ giá tốt
                                            nhất.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="section-advertisement">
        <div class="bs-container">
            <div class="bs-row">
                <div class="bs-col">
                    <div class="module-advertisement">
                        <div class="module-content">
                            <div class="grid">
                                <a href="#" class="grid__links">
                                    <img src="{{ url('/') }}/pms/images/banner_q1.png" alt="banner_q1.png">
                                </a>
                                <a href="#" class="grid__links">
                                    <img src="{{ url('/') }}/pms/images/banner_q2.png" alt="banner_q2.png">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="section-difference">
        <div class="bs-container">
            <div class="bs-row">
                <div class="bs-col">
                    <div class="module module-difference">
                        <div class="module-header">
                            <h2 class="title">
                                Khám phá nhiều điểm đến khác
                            </h2>
                        </div>
                        <div class="module-content">
                            <div class="bs-row row-lg-10 row-md-10 row-sm-5 row-xs-5">
                                @foreach ($locationMore as $item)
                                <div class="bs-col lg-25-10 col-md-25-10 sm-33-5 xs-50-5">
                                    <div class="container-box">
                                        @if (!empty($item->feature_image))
                                            <img src="{{ url('/') }}{{ $item->feature_image }}" alt="{{ $item->name }}">
                                        @else
                                            <img src="{{ url('/') }}/uploads/images/default.png" alt="{{ $item->name }}">
                                        @endif
                                        
                                        <div class="content">
                                            <h3 class="h3_title bold">
                                                {{ $item->name }}
                                            </h3>
                                            <ul class="item">
                                                <li class="item-list">
                                                    <a href="{{ route('public.listTour', ['end_location' => $item->id]) }}"><b>{{ sizeof($item->tour) }}</b> Tours </a>
                                                </li>
                                                <li class="item-list">
                                                    <a href="{{ route('hotel.ds', ['location_id' => $item->id]) }}"><b>{{ sizeof($item->hotel) }}</b> Hotels </a>
                                                </li>
                                                <li class="item-list">
                                                    <a href="{{ route('public.listCar', ['location_id' => $item->id]) }}"><b>{{ sizeof($item->car) }}</b> Cars </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
@endsection
@push('js')
    <script>
        $(document).on('click', '.option-start-destination', function(e) {
            let text = $(this).text().trim();
            let id = $(this).attr('data-id');
            $(".add-text-destination").val(text);
            $(".start_destination").val(id);
            $(this).parents(".item-grid__content").removeClass("active");
        });
        $(document).on('click', '.btn-apply-number-people', function(e) {
            e.preventDefault();
            var number_adult = parseInt($('.number_adult').html());
            var number_children = parseInt($('.number_children').html());
            var text = number_adult + ' Người lớn - '+ number_children +' Trẻ em';
            var total = number_adult + number_children;
            $('.text-number_people').val(text);
            $('.number_people').val(total);
            $(this).parents(".item-grid__content").removeClass("active");
        });

    </script>
@endpush