@extends('public.master')

@section('css-top')
<link href="{{ URL::asset('assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
@endsection
@section('content')

<main id="main">
    <section class="section-page section-page__2">
        <div class="bs-container">
            <div class="bs-row">
                <div class="bs-col">
                    <div class="module module-page">
                        <div class="module-header">
                            <h2 class="title">
                                <span class="span__name black"> Thuê xe ô tô cho hành trình của bạn</span>
                                <p class="p__info">
                                    Những trải nghiệm hàng đầu ở Huế để bạn bắt đầu
                                </p>
                            </h2>
                            <div class="page__right">
                                <ul class="item">
                                    <li class="item-list">
                                        <a href="index.html" class="item-list__links">
                                            Trang chủ
                                        </a>
                                    </li>
                                    <li class="item-list">
                                        <a href="car.html" class="item-list__links">
                                            Car
                                        </a>
                                    </li>
                                    <li class="item-list">
                                        <span class="item-list__links">
                                            Hà Nội
                                        </span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="section-rooms section-list section-list__car">
        <div class="container ">
            <form action="{{ route('tour.map') }}" method="get" class="form-filter-car">
                <div class="row ">
                    <div class="col-3 ">
                        <div class="form-group">
                            <label for="">Địa điểm đến</label>
                            <select name="end_location" id="" class="form-control submit-form">
                                <option value="">--Chọn địa điểm--</option>
                                @foreach ($listLocation as $item)
                                    <option value="{{ $item->id }}" @if(Request::get('end_location') == $item->id) selected @endif >{{ $item->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-3 ">
                        <div class="form-group">
                            <label for="">Địa điểm đi</label>
                            <select name="start_destination" id="" class="form-control submit-form">
                                <option value="">--Chọn địa điểm--</option>
                                @foreach ($listDestination as $item)
                                    <option value="{{ $item->id }}" @if(Request::get('start_destination') == $item->id) selected @endif >{{ $item->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-3">
                        <label for="">Chọn thời gian</label>
                        <div class="form-group">
                            <input type="text" class="form-control " name="daterange" value="{{ Request::get('daterange') }}" readonly>
                        </div>
                    </div>
                    {{-- <div class="col-3">
                        <div class="form-group">
                            <label for="">Chọn hình thức xe</label>
                            <select name="car_form" id="" class="form-control submit-form">
                                <option value="">--Chọn hình thức--</option>
                                <option value="2" @if(Request::get('car_form') == 2) selected @endif>Xe tự lái</option>
                                <option value="1" @if(Request::get('car_form') == 1) selected @endif>Xe có lái</option>
                            </select>
                        </div>
                    </div> --}}
                    <div class="col-3"><div class="form-group">
                        <label for="">Chọn khoảng giá</label>
                        <select name="price" id="" class="form-control submit-form">
                            <option value="">--Chọn khoảng giá--</option>
                            <option value="1" @if(Request::get('price') == 1) selected @endif>Từ 0 vnđ - 5 triệu vnđ</option>
                            <option value="2" @if(Request::get('price') == 2) selected @endif>5 triệu vnđ - 10 triệu vnđ</option>
                            <option value="3" @if(Request::get('price') == 3) selected @endif>10 triệu vnđ - 15 triệu vnđ</option>
                        </select>
                    </div></div>
                </div>
            </form>
        </div>
        <div class="bs-container">
            <div class="grid">
                <div class="filterMobile">
                    <i class="fas fa-filter"></i>
                    <i class="fas fa-times"></i>
                </div>
                <div class="filterShow grid-sidebar div-50">
                    <div class="row">
                        <div class="col-12">
                            <div class="form-group mb-3 ">
                                <div id="gmaps-basic" class="gmaps gmap-car"></div>
                                <i>Click onto map to place Location address</i>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="grid-main div-50 height-fix">
                    <div class="grid-main__content">
                        <div class="bs-row row-lg-15 row-md-10 row-sm-5 row-xs-5">
                            @foreach ($listTour as $item)
                            <div class="itemTours">
                                <div class="bs-row">
                                    <div class="open-show__avata bs-col lg-33 md-33 sm-33 xs-50 tn-100">
                                        <div class="avata">
                                            <a href="{{ route('public.detailTour', [$item->slug , $item->id]) }}" class="item__links"></a>
                                            <img src="{{url('/')}}{{ $item->thumnail }}" alt="{{ $item->title }}" class="img__links">
                                                @if($item->sale !== null && $item->sale < $item->price)
                                                    <span class="span__sale">
                                                        <span class="span__number">
                                                            {{ round((($item->price - $item->sale)/$item->price)*100) }} %
                                                        </span>
                                                    </span>
                                                @endif
                                            <a class="span___love" href="#">
                                                <i class="fas fa-heart"></i>
                                                {{ sizeof($item->wishListTour) }}
                                            </a>
                                            <div class="countdown">
                                                <span id="clock" class="clock"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="open-show__content bs-col lg-66 md-66 sm-66 xs-50 tn-100">
                                        <div class="content">
                                            <div class="content-header">
                                                <h2 class="title">
                                                    <a href="{{ route('public.detailTour', [$item->slug , $item->id]) }}" class="title__links">
                                                        <span class="span__name medium">
                                                            {{ $item->title }}
                                                        </span>
                                                        <p class="p__text">
                                                            {{ $item->description }}
                                                        </p>
                                                    </a>
                                                </h2>
                                                <p class="p__price">
                                                    GIÁ
                                                    <span class="span__price bold">
                                                        @if($item->sale !== null && $item->sale < $item->price)
                                                            {{ number_format($item->sale, 0, ",", ".") }} đ
                                                        @else
                                                            {{ number_format($item->price, 0, ",", ".") }} đ
                                                        @endif
                                                    </span>
                                                </p>
                                            </div>
                                            <div class="content-footer">
                                                <div class="text">
                                                    <p class="p__time">
                                                        <img src="{{url('/')}}/pms/images/icon_time.png" alt="icon_time.png" class="icon">
                                                        <span class="span__name medium">
                                                            Lịch trình:
                                                        </span>
                                                        {{ $item->duration }} ngày 
                                                    </p>
                                                </div>
                                                <div class="text">
                                                    <p class="p__time">
                                                        <img src="{{url('/')}}/pms/images/icon_check.png" alt="icon_check.png" class="icon">
                                                        <span class="span__name  medium">
                                                            Ngày đi:
                                                        </span>
                                                        @if(!$item->startDateTour->isEmpty())
                                                            {{ $item->startDateTour['0']['start_date'] }}
                                                        @endif
                                                    </p>
                                                </div>
                                                <div class="text">
                                                    <p class="p__time">
                                                        <img src="{{url('/')}}/pms/images/icon_map.png" alt="icon_map.png" class="icon">
                                                        <span class="span__name  medium">
                                                            Khởi hành: </span>
                                                            {{ optional($item->destination)->name }}
                                                    </p>
                                                </div>
                                                <div class="like-dvaluate">
                                                    <p class="p__like">
                                                        @for ($j = 0; $j < round($item->avgReview, 0); $j++)
                                                        <i class="fas fa-star"></i>
                                                        @endfor
                                                    </p>
                                                    <p class="p__like medium">
                                                        {{ sizeof($item->reviewTour) }} đánh giá
                                                    </p>
                                                </div>

                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                        </div>
                        <div class="fexPagintaion text-center d-flex justify-content-center clearfix">
                            {{ $listTour->render() }}
                        </div>
                    </div>
                </div>
            </div>
    </section>
    <input type="hidden" name="" class="list-car" value="{{ json_encode($listTour) }}">
    
</main>
@endsection
@push('js')
<script>
    var listTour = JSON.parse($('.list-car').val());
</script>
<script>
    var bookingCore = {
        url: 'http://sandbox.bookingcore.org',
        map_provider: 'gmap',
        map_gmap_key: '',
        csrf: 'JMrM5NwcxQy6HWOmuo7LQ2kHPh7pGwfbOPpXxkue'
    };
</script>

<script src="https://maps.google.com/maps/api/js?key=AIzaSyDsucrEdmswqYrw0f6ej3bf4M4suDeRgNA"></script>
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script>
{{-- <script type="text/javascript" src="https://raw.github.com/HPNeo/gmaps/master/gmaps.js"></script> --}}
<script src="{{ URL::asset('assets/js/map-engine.js') }}"></script>
<script>
    jQuery(function ($) {
        new BravoMapEngine('gmaps-basic', {
            fitBounds: true,
            center: [{{"21.008"}}, {{"105.858"}}],
            zoom:{{"8"}},
            ready: function (engineMap) {
                $.each(listTour['data'], function( index, value ) {
                    engineMap.addMarker([parseFloat(value.latitu), parseFloat(value.longtitu)], {
                        icon_options: {}
                    });
                });
                
            }
        });
    })
</script>
<script>
    $(document).on('change', '.submit-form', function() {
        $('.form-filter-car').submit();
    });
    $(document).on('click', '.applyBtn', function() {
        $('.form-filter-car').submit();
    });
</script>
@endpush
