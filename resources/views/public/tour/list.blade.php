@extends('public.master')
@section('content')
<style>
    .is-favourite{
        color: red;
    }
    .is-favourite-color{
        color: red;
    }
</style>
<main id="main">
    <section class="section-page section-page__2">
        <div class="bs-container">
            <div class="bs-row">
                <div class="bs-col">
                    <div class="module module-page">
                        <div class="module-header">
                            <h2 class="title">
                                <span class="span__name black">Được gợi ý ở Huế</span>
                                <p class="p__info">
                                    Những trải nghiệm hàng đầu ở Huế để bạn bắt đầu
                                </p>
                            </h2>
                            <div class="page__right">
                                <ul class="item">
                                    <li class="item-list">
                                        <a href="/" class="item-list__links">
                                            Trang chủ
                                        </a>
                                    </li>
                                    <li class="item-list">
                                        <a href="tour.html" class="item-list__links">
                                            tours
                                        </a>
                                    </li>
                                    <li class="item-list">
                                        <span class="item-list__links">
                                            Huế
                                        </span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="section-list__tours section-list">
        <div class="bs-container">
            <div class="grid">
                <div class="filterMobile">
                    <i class="fas fa-filter"></i>
                    <i class="fas fa-times"></i>
                </div>
                <div class="filterShow grid-sidebar">
                    
                    <div class="search">
                        <form id="form-tour-category" action="{{ route('public.listTour') }}" method="GET">
                            <div class="form-group">
                                <label class="lable__name bold" for="">Nơi khởi hành</label>
                                <div class="select">
                                    <input type="hidden" name="start_destination" class="start_destination" value="{{ Request::get('start_destination') }}">
                                    <div class="header-option header-option-destination">
                                        <img src="{{url('/')}}/pms/images/icon_map.png" class="img__option" alt="icon_map.png">
                                        <span class="span__option medium">
                                            @if(!empty(Request::get('start_destination')))
                                                @foreach($listDestination as $item)
                                                    @if ($item->id == Request::get('start_destination'))
                                                        {{ $item->name }}
                                                    @endif
                                                @endforeach
                                            @else
                                            --Chọn nơi khởi hành--
                                            @endif
                                        </span>
                                    </div>
                                    <div class="body-option">
                                        @foreach($listDestination as $item)
                                            <div data-id="{{ $item->id }}" class="item-option">
                                                <img src="{{url('/')}}/pms/images/icon_map.png" class="img__option" alt="icon_map.png">
                                                <span class="span__option medium">
                                                    {{ $item->name }}
                                                </span>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="lable__name bold" for="">Địa điểm đến</label>
                                <div class="select">
                                    <input type="hidden" name="end_location" class="end_location" value="{{ Request::get('end_location')}}">
                                    <div class="header-option header-option-location">
                                        <img src="{{url('/')}}/pms/images/icon_map.png" class="img__option" alt="icon_map.png">
                                        <span class="span__option medium">
                                            @if(!empty(Request::get('end_location')))
                                                @foreach($listLocation as $item)
                                                    @if ($item->id == Request::get('end_location'))
                                                    {{ $item->name }}
                                                    @endif
                                                @endforeach
                                            @else
                                            --Chọn địa điểm đến--
                                            @endif
                                            
                                        </span>
                                    </div>
                                    <div class="body-option">
                                        @foreach($listLocation as $item)
                                            <div data-id="{{ $item->id }}" class="item-option">
                                                <img src="{{url('/')}}/pms/images/icon_map.png" class="img__option" alt="icon_map.png">
                                                <span class="span__option medium">
                                                    {{ $item->name }}
                                                </span>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="lable__name bold">Thời gian đi</label>
                                <input type="text" class="form-control" name="daterange" readonly value="{{ Request::get('daterange')}}">
                            </div>
                            <button type="submit" class="button__search bold">
                                Tìm kiếm
                            </button>
                    </div>
                    <div class="onCheckRaido tour">
                        
                            @foreach ($tourCategory as $item)
                                <label  class="lable__radio label-category {{ Request::get('tour_category') == $item->id ? 'active' : '' }}">
                                    <span class="span__radio">
                                        <img src="{{url('/')}}/pms/images/links.png" alt="links.png">
                                        <span class="span__name">
                                            {{ $item->name }}
                                        </span>
                                    </span>
                                    <input type="radio" class="input__radio" name="tour_category" value="{{ $item->id }}" {{ Request::get('tour_category') == $item->id ? 'checked' : '' }}>
                                </label>
                            @endforeach
                       
                    </div>
                    <div class="evaluate">
                        <div class="onCheckRaido item">
                            <div class="item-header">
                                <h3 class="title bold">
                                    Điểm đánh giá
                                </h3>
                            </div>
                            <div class="item-content">
                                <label class="lable__radio label-category {{Request::get('star') == 1 ? 'active' : ''}}">
                                    <span class="span__radio">
                                        1 <br>
                                        Sao
                                    </span>
                                    <input type="radio" class="input__radio" name="star" value="1" {{Request::get('star') == 1 ? 'checked="checked"' : ''}}>
                                </label>
                                <label class="lable__radio label-category {{Request::get('star') == 2 ? 'active' : ''}}">
                                    <span class="span__radio">
                                        2 <br>
                                        Sao
                                    </span>
                                    <input type="radio" class="input__radio" name="star" value="2" {{Request::get('star') == 2 ? 'checked="checked"' : ''}}>
                                </label>
                                <label class="lable__radio label-category {{Request::get('star') == 3 ? 'active' : ''}}">
                                    <span class="span__radio">
                                        3 <br>
                                        Sao
                                    </span>
                                    <input type="radio" class="input__radio" name="star" value="3" {{Request::get('star') == 3 ? 'checked="checked"' : ''}}>
                                </label>
                                <label class="lable__radio label-category {{Request::get('star') == 4 ? 'active' : ''}}">
                                    <span class="span__radio">
                                        4 <br>
                                        Sao
                                    </span>
                                    <input type="radio" class="input__radio" name="star" value="4" {{Request::get('star') == 4 ? 'checked="checked"' : ''}}>
                                </label>
                                <label class="lable__radio label-category {{Request::get('star') == 5 ? 'active' : ''}}">
                                    <span class="span__radio">
                                        5 <br>
                                        Sao
                                    </span>
                                    <input type="radio" class="input__radio" name="star" value="5" {{Request::get('star') == 5 ? 'checked="checked"' : ''}}>
                                </label>
                            </div>
                        </div>
                   
                        <div class="onCheckRaido price">
                            <div class="item-header">
                                <h3 class="title bold">
                                    Chọn khoảng giá
                                </h3>
                            </div>
                            <div class="item-content">

                                    <label class="lable__radio label-category {{Request::get('price') == 1 ? 'active' : ''}}">
                                        <span class="span__radio">
                                            <b class="bold">0</b> vnđ - <b class="bold">5</b> triệu vnđ
                                        </span>
                                        <input type="radio" class="input__radio" name="price" value="1" {{Request::get('price') == 1 ? 'checked' : ''}}>
                                    </label>
                                    <label class="lable__radio label-category {{Request::get('price') == 2 ? 'active' : ''}}">
                                        <span class="span__radio ">
                                            <b class="bold">5</b> vnđ - <b class="bold">10</b> triệu vnđ
                                        </span>
                                        <input type="radio" class="input__radio" name="price" value="2" {{Request::get('price') == 2 ? 'checked' : ''}}>
                                    </label>
                                    <label class="lable__radio label-category {{Request::get('price') == 3 ? 'active' : ''}}">
                                        <span class="span__radio">
                                            <b class="bold">10</b> vnđ - <b class="bold">15</b> triệu vnđ
                                        </span>
                                        <input type="radio" class="input__radio" name="price" value="3" {{Request::get('price') == 3 ? 'checked' : ''}}>
                                    </label>

                            </div>
                        </div>
                        <div class="onCheckRaido tourismType">
                            <div class="item-header">
                                <h3 class="title bold">
                                    Loại hình du lịch
                                </h3>
                                <div class="item-content">
                                    @foreach($tourStyleCategory as $item)
                                        @if (!empty(Request::get('tourismType')) && in_array($item->id, Request::get('tourismType')))
                                            <label data-id="{{ $item->id }}" class="lable__radio lable_tour_style_category active bs-tooltip" tooltip-data="{{ $item->title }}">
                                                <span class="span__radio">
                                                    <img src="{{url('/')}}{{ $item->icon }}" alt="{{ $item->title }}" class="type">
                                                    <img src="{{url('/')}}{{ $item->icon_hover }}" alt="{{ $item->title }}" class="type_hover">
                                                </span>
                                                
                                            </label>
                                        @else
                                            <label data-id="{{ $item->id }}" class="lable__radio lable_tour_style_category bs-tooltip" tooltip-data="{{ $item->title }}">
                                                <span class="span__radio">
                                                    <img src="{{url('/')}}{{ $item->icon }}" alt="{{ $item->title }}" class="type">
                                                    <img src="{{url('/')}}{{ $item->icon_hover }}" alt="{{ $item->title }}" class="type_hover">
                                                </span>
                                                
                                            </label>
                                        @endif
                                    @endforeach
                                </div>
                                <div class="d-none wallet_input_tour_style_category">
                                    @foreach($tourStyleCategory as $item)
                                        @if (!empty(Request::get('tourismType')) && in_array($item->id, Request::get('tourismType')))
                                            <input type="checkbox" id="input__radio_{{ $item->id }}" class="input__radio " name="tourismType[]" value="{{ $item->id }}" checked>
                                        @else
                                        <input type="checkbox" id="input__radio_{{ $item->id }}" class="input__radio " name="tourismType[]" value="{{ $item->id }}" >
                                        @endif
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        <div class="onCheckRaido vehicle">
                            <div class="item-header">
                                <h3 class="title bold">
                                    Thông tin vận chuyển
                                </h3>
                                <div class="item-content">
                                    @foreach ($tourVehicleCategory as $item)
                                        @if (!empty(Request::get('vehicle')) && in_array($item->id, Request::get('vehicle')))
                                            <label data-id="{{ $item->id }}" class="lable__radio lable_tour_vehicle_category active">
                                                <span class="span__radio">
                                                    <img src="{{url('/')}}{{ $item->icon }}" alt="{{ $item->title }}">
                                                    <span class="span__name">
                                                        {{ $item->title }}
                                                    </span>
                                                </span>
                                            </label>
                                        @else
                                            <label data-id="{{ $item->id }}" class="lable__radio lable_tour_vehicle_category ">
                                                <span class="span__radio">
                                                    <img src="{{url('/')}}{{ $item->icon }}" alt="{{ $item->title }}">
                                                    <span class="span__name">
                                                        {{ $item->title }}
                                                    </span>
                                                </span>
                                            </label>
                                        @endif
                                    @endforeach
                                </div>
                                <div class="d-none wallet_input_tour_vehicle_category">
                                    @foreach($tourVehicleCategory as $item)
                                        @if (!empty(Request::get('vehicle')) && in_array($item->id, Request::get('vehicle')))
                                            <input type="checkbox" id="input__radio_vehicle_{{ $item->id }}" class="input__radio " name="vehicle[]" value="{{ $item->id }}" checked>
                                        @else
                                            <input type="checkbox" id="input__radio_vehicle_{{ $item->id }}" class="input__radio " name="vehicle[]" value="{{ $item->id }}" >
                                        @endif    
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        <div class="onCheckRaido price date">
                            <div class="item-header">
                                <h3 class="title bold">
                                    Thời gian du lịch
                                </h3>
                            </div>
                            <div class="item-content">
                                <label class="lable__radio label-category {{Request::get('date_number') == 1 ? 'active' : ''}}">
                                    <span class="span__radio">
                                        Dưới 3 ngày

                                    </span>
                                    <input type="radio" class="input__radio" value="1" name="date_number" {{Request::get('date_number') == 1 ? 'checked' : ''}}>
                                </label>
                                <label class="lable__radio label-category {{Request::get('date_number') == 2 ? 'active' : ''}}">
                                    <span class="span__radio">
                                        Từ 4 đến 7 ngày

                                    </span>
                                    <input type="radio" class="input__radio" value="2" name="date_number" {{Request::get('date_number') == 2 ? 'checked' : ''}}>
                                </label>
                                <label class="lable__radio label-category {{Request::get('date_number') == 3 ? 'active' : ''}}">
                                    <span class="span__radio">
                                        Từ 7 đến 10 ngày

                                    </span>
                                    <input type="radio" class="input__radio" value="3" name="date_number" {{Request::get('date_number') == 3 ? 'checked' : ''}}>
                                </label>
                                <label class="lable__radio label-category {{Request::get('date_number') == 4 ? 'active' : ''}}">
                                    <span class="span__radio">
                                        Trên 10 ngày
                                    </span>
                                    <input type="radio" class="input__radio" value="4" name="date_number" {{Request::get('date_number') == 4 ? 'checked' : ''}}>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="grid-main">
                    <div class="filter">
                        <div class="filter-price">
                            <div class="control-filter">
                                <span class="span__name bold">
                                    Xem theo:
                                </span>
                                <select class="select__text filter_price" name="filter_price" onchange="submitFormfilter()">
                                    <option value="">Giá tiền</option>
                                    <option value="1" {{Request::get('filter_price') == 1 ? 'selected' : ''}}>Thấp đến cao</option>
                                    <option value="2" {{Request::get('filter_price') == 2 ? 'selected' : ''}} >Cao đến thấp</option>
                                    <option value="3" {{Request::get('filter_price') == 3 ? 'selected' : ''}} >Khuyến mại</option>
                                </select>
                            </div></form>
                        </div>
                        <div class="filter-list">
                            <p class="p__text">
                                <b class="bold">{{ $listTour->firstItem().'-'. $listTour->lastItem() }}</b> trong tổng số <b class="bold">{{ $listTour->total() }}</b> tours
                            </p>
                            <i class="fas fa-th-large"></i>
                            <i class="fas fa-th-list active"></i>
                        </div>
                    </div>
                    <div class="grid-main__content onDisplay">
                        @php
                            // dd($listTour);
                            $i = 0;
                        @endphp
                        @foreach ($listTour as $item)
                            @php
                                $i ++;
                            @endphp
                            <div class="itemTours">
                                <div class="bs-row">
                                    <div class="open-show__avata bs-col lg-33 md-33 sm-33 xs-50 tn-100">
                                        <div class="avata">
                                            <a href="{{ route('public.detailTour', [$item->slug , $item->id]) }}" class="item__links"></a>
                                            <img src="{{url('/')}}{{ $item->thumnail }}" alt="{{ $item->title }}" class="img__links">
                                                @if($item->sale !== null && $item->sale < $item->price)
                                                    <span class="span__sale">
                                                        <span class="span__number">
                                                            {{ round((($item->price - $item->sale)/$item->price)*100) }} %
                                                        </span>
                                                    </span>
                                                @endif
                                            {{-- <a class="span___love" href="#">
                                                <i class="fas fa-heart"></i>
                                                {{ sizeof($item->wishListTour) }}
                                            </a> --}}
                                            <a class="span___love" onclick="wishListTour({{ $item->id }})">
                                                <i class="fas fa-heart {{ Auth::check() ? ($item->wishlistTourBy(auth()->user()) ? 'is-favourite' : ' ') : ' ' }}" {{ Auth::check() == false ? ' modal-show=show modal-data=#login ' : ' ' }} id="color{{ $item->id }}"></i>
                                                <span class="favourite {{ Auth::check() ? ($item->wishlistTourBy(auth()->user()) ? 'is-favourite-color' : ' '): ' ' }}" {{ Auth::check() == false ? ' modal-show=show modal-data=#login ' : ' ' }} id="{{ $item->id }}">{{ sizeOf($item->wishListTour) }}</span>
                                            </a>
                                            <div class="countdown">
                                                <span id="clock" class="clock"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="open-show__content bs-col lg-66 md-66 sm-66 xs-50 tn-100">
                                        <div class="content">
                                            <div class="content-header">
                                                <h2 class="title">
                                                    <a href="{{ route('public.detailTour', [$item->slug , $item->id]) }}" class="title__links">
                                                        <span class="span__name medium">
                                                            {{ $item->title }}
                                                        </span>
                                                        <p class="p__text">
                                                            {{ $item->description }}
                                                        </p>
                                                    </a>
                                                </h2>
                                                <p class="p__price">
                                                    GIÁ
                                                    <span class="span__price bold">
                                                        @if($item->sale !== null && $item->sale < $item->price)
                                                            {{ number_format($item->sale, 0, ",", ".") }} đ
                                                        @else
                                                            {{ number_format($item->price, 0, ",", ".") }} đ
                                                        @endif
                                                    </span>
                                                </p>
                                            </div>
                                            <div class="content-footer">
                                                <div class="text">
                                                    <p class="p__time">
                                                        <img src="{{url('/')}}/pms/images/icon_time.png" alt="icon_time.png" class="icon">
                                                        <span class="span__name medium">
                                                            Lịch trình:
                                                        </span>
                                                        {{ $item->duration }} ngày 
                                                    </p>
                                                </div>
                                                <div class="text">
                                                    <p class="p__time">
                                                        <img src="{{url('/')}}/pms/images/icon_check.png" alt="icon_check.png" class="icon">
                                                        <span class="span__name  medium">
                                                            Ngày đi:
                                                        </span>
                                                        @if(!$item->startDateTour->isEmpty())
                                                            {{ $item->startDateTour['0']['start_date'] }}
                                                        @endif
                                                    </p>
                                                </div>
                                                <div class="text">
                                                    <p class="p__time">
                                                        <img src="{{url('/')}}/pms/images/icon_map.png" alt="icon_map.png" class="icon">
                                                        <span class="span__name  medium">
                                                            Khởi hành: </span>
                                                            {{ optional($item->destination)->name }}
                                                    </p>
                                                </div>
                                                <div class="like-dvaluate">
                                                    <p class="p__like">
                                                        @for ($j = 0; $j < round($item->avgReview, 0); $j++)
                                                        <i class="fas fa-star"></i>
                                                        @endfor
                                                    </p>
                                                    <p class="p__like medium">
                                                        {{ sizeof($item->reviewTour) }} đánh giá
                                                    </p>
                                                </div>

                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            @if($i == 3)
                                <!-- banner advertisement -->
                                <a href="#" class="advertisement__links">
                                    <img src="{{url('/')}}/pms/images/banner_q2.png" alt="banner_q2.png">
                                </a>
                                <!-- // banner advertisement-->
                            @endif
                        @endforeach
                            @if($i < 3)
                                    <!-- banner advertisement -->
                                    <a href="#" class="advertisement__links">
                                        <img src="{{url('/')}}/pms/images/banner_q2.png" alt="banner_q2.png">
                                    </a>
                                    <!-- // banner advertisement-->
                            @endif

                        
                        <div class="fexPagintaion text-center clearfix">
                            {{$listTour->links()}}
                            
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="section-domestic">
    <div class="bs-container">
        <div class="bs-row">
            <div class="bs-col">
                <div class="module module-domestic">
                    <div class="module-header">
                        <h2 class="title">
                            Điểm đến yêu thích trong nước
                        </h2>
                        <p class="p__info">
                            Bao la khắp nước. Bốn bể là nhà
                        </p>
                    </div>
                    <div class="module-content">
                        <div class="grid">
                            @foreach ($locationFeatured as $item)
                                <div class="grid-item">
                                    <a href="#" class="item__links" width="100%"> 
                                        <div class="avata" width="100%">
                                            @if(!empty($item->feature_image))
                                                <img src="{{url('/')}}{{ $item->feature_image }}" alt="{{ $item->name }}" width="100%">
                                            @else
                                                <img src="{{ url('/') }}/uploads/images/default.png" alt="{{ $item->name }}" width="100%">
                                            @endif
                                        </div>
                                    </a>
                                    <div class="content">
                                        <h3 class="title">
                                            {{ $item->name }}
                                        </h3>
                                        <ul class="category">
                                            <li class="category-list">
                                                <a href="{{ route('public.listTour', ['end_location' => $item->id]) }}" class="category-list__links">
                                                    {{ sizeof($item->tour) }}
                                                    <span class="span_name">
                                                        Tours
                                                    </span>
                                                </a>
                                            </li>
                                            <li class="category-list">
                                                <a href="{{ route('hotel.ds', ['location_id' => $item->id]) }}" class="category-list__links">
                                                    {{ sizeof($item->hotel) }}
                                                    <span class="span_name">
                                                        Hotels
                                                    </span>

                                                </a>
                                            </li>
                                            <li class="category-list">
                                                <a href="{{ route('public.listCar', ['location_id' => $item->id]) }}" class="category-list__links">
                                                    {{ sizeof($item->car) }} 
                                                    <span class="span_name">
                                                        Cars
                                                    </span>
                                                </a>
                                            </li>

                                        </ul>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
</main>
@endsection
@push('js')
    <script type="text/javascript">
     function wishListTour(id) {
            $.ajax({
                type: "GET",
                url: "/tour-favorite/"+id ,
                success: function (response) {
                    $('#'+id).html(response.countFavourite).css('color',response.color);
                    $('#color'+id).css('color',response.color);
            }
        });
        }
        $(document).ready(function () {
            function countdown() {
                $('.clock').countdown('2020/10/03', function (event) {
                    $(this).html(event.strftime(' <span> %H</span> Giờ <span> :%M </span> Phút <span>:%S </span> Giây'));
                });
            }
            
            // hien thi danh sach turs 
            function onDisplay() {
                $('.fa-th-large').click(function () {
                    $('.fa-th-list').removeClass('active');
                    $(this).addClass('active');
                    $('.onDisplay').addClass('active');
                    $('.open-show__avata').removeClass('lg-33 md-33 sm-33 xs-50 ');
                    $('.open-show__content').removeClass('lg-66 md-66 sm-33 xs-50');
                    $('.open-show__avata').addClass('lg-100 md-100 sm-100 xs-100');
                    $('.open-show__content').addClass('lg-100 md-100 sm-100 xs-100');
                })
                $('.fa-th-list').click(function () {
                    $('.onDisplay').removeClass('active');
                    $('.fa-th-large').removeClass('active');
                    $(this).addClass('active');
                    $('.open-show__avata').removeClass('lg-100 md-100 sm-100 xs-100');
                    $('.open-show__content').removeClass('lg-100 md-100 sm-100 xs-100');
                    $('.open-show__avata').addClass('lg-33 md-33 sm-33 xs-50');
                    $('.open-show__content').addClass('lg-66 md-66 sm-33 xs-50');
                });
            }

            function selecOption() {
                $('.select .header-option-destination').click(function () {
                    $(this).next('.body-option').toggleClass('active');
                    var header = $(this);
                    $(this).next('.body-option').children('.item-option').click(function(){
                        var html = $(this).html();
                        var id = $(this).attr('data-id');
                        header.html(html);
                        header.parent('.select').children('.start_destination').val(id);
                        $(this).parents('.body-option').removeClass('active');
                    });
                    
                });
                $('.select .header-option-location').click(function () {
                    $(this).next('.body-option').toggleClass('active');
                    var header = $(this);
                    $(this).next('.body-option').children('.item-option').click(function(){
                        var html = $(this).html();
                        var id = $(this).attr('data-id');
                        header.html(html);
                        header.parent('.select').children('.end_location').val(id);
                        $(this).parents('.body-option').removeClass('active');
                    });
                    
                });

            }
            onDisplay();
            selecOption();
            countdown();
        });
        $(document).on('click', '.label-category', function(e){
            e.preventDefault();
                if($(this).hasClass('active')) {
                    $(this).removeClass('active');
                    $(this).prevAll('.label-category').removeClass('active');
                    $(this).nextAll('.label-category').removeClass('active');
                    console.log(1, $(this).children('.input__radio'));
                    
                    $(this).children('.input__radio').prop('checked', false);
                } else {
                    $(this).prevAll('.label-category').removeClass('active');
                    $(this).nextAll('.label-category').removeClass('active');
                    $(this).addClass('active');
                    console.log(2, $(this).children('.input__radio'));
                    $(this).children('.input__radio').prop('checked', true);
                }
                
                submitFormfilter();
            });
        function submitFormfilter(){
            $('#form-tour-category').submit();
        };
        $(document).on('click', '.lable_tour_style_category', function(e) {
            var id = $(this).attr('data-id');
            if($(this).hasClass('active')) {
                $(this).removeClass('active');
                $('#input__radio_'+id).prop('checked', false);
            }else{
                $(this).addClass('active');
                $('#input__radio_'+id).prop('checked', true);
            }
            submitFormfilter();
        });
        $(document).on('click', '.lable_tour_vehicle_category', function(e) {
            var id = $(this).attr('data-id');
            if($(this).hasClass('active')) {
                $(this).removeClass('active');
                $('#input__radio_vehicle_'+id).prop('checked', false);
            }else{
                $(this).addClass('active');
                $('#input__radio_vehicle_'+id).prop('checked', true);
            }
            submitFormfilter();
        });
    </script>
    
@endpush