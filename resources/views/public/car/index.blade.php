@extends('public.master')
@section('content')
<style>
    .is-favourite{
        color: red;
    }
    .is-favourite-color{
        color: red;
    }
</style>
<main id="main">
    <section class="section-page">
        <div class="bs-container">
            <div class="bs-row">
                <div class="bs-col">
                    <div class="module module-page">
                        <div class="module-header">
                            <h2 class="title">
                                <span class="span__name black">
                                    Thuê ô tô cho hành trình của bạn
                                </span>
                            </h2>
                            <div class="page__right">
                                <ul class="item">
                                    <li class="item-list">
                                        <a href="/" class="item-list__links">
                                            Trang chủ
                                        </a>
                                    </li>
                                    <li class="item-list">
                                        <span class="item-list__links">
                                            Car
                                        </span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="module-content">
                            <form action="{{ route('public.listCar') }}" method="GET">
                                <div class="grid">
                                    <div class="item-grid">
                                        <div class="item-grid__header  clickShow">
                                            <div class="item_icon">
                                                <img src="{{ url('/') }}/pms/images/icon_map.png" alt="icon_map.png">
                                            </div>
                                            <div class="item__content">
                                                <h3 class="title">
                                                    Nhận xe
                                                </h3>
                                                <p class="p__text addText">
                                                    <input type="text" class="text-location" placeholder="Bạn muốn nhận xe tại địa điểm nào" readonly>
                                                    <input type="hidden" name="location_id" class="location_id">
                                                </p>
                                                <i class="fas fa-caret-down"></i>
                                            </div>
                                        </div>
                                        <div class="item-grid__content">
                                            <ul class="item">
                                                @foreach ($listLocation as $item)
                                                    <li data-id="{{ $item->id }}" class="item-list item-list-location">
                                                        <i class="fas fa-map-marker-alt"></i>
                                                        <span class="span__name ">
                                                            {{ $item->name }}
                                                        </span>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="item-grid">
                                        <div class="item-grid__header">
                                            <div class="item_icon">
                                                <img src="{{ url('/') }}/pms/images/icon_check.png" alt="icon_check.png">
                                            </div>
                                            <div class="item__content">
                                                <h3 class="title">
                                                    Ngày thuê - Ngày trả
                                                </h3>
                                                <p class="p__text acDates">

                                                    <input type="text" name="daterange" readonly/>
                                                </p>
                                                <i class="fas fa-caret-down"></i>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="item-grid">
                                        <div class="item-grid__header clickShow">
                                            <div class="item_icon">
                                                <img src="{{ url('/') }}/pms/images/icon_us.png" alt="icon_us.png">
                                            </div>
                                            <div class="item__content">
                                                <h3 class="title">
                                                    Hình thức xe
                                                </h3>
                                                <p class="p__text">
                                                    <input type="text" class="text-car-form" placeholder="Chọn hình thức thuê xe bạn muốn" readonly>
                                                    <input type="hidden" name="car_form" class="car_form" >
                                                </p>
                                                <i class="fas fa-caret-down"></i>
                                            </div>
                                        </div>
                                        <div class="item-grid__content">
                                            <ul class="item">
                                                <li data-id="0" class="item-list item-list-car-form">
                                                    <i class="fas fa-map-marker-alt"></i>
                                                    <span class="span__name ">
                                                        Xe tự lái
                                                    </span>
                                                </li>
                                                <li data-id="1" class="item-list item-list-car-form">
                                                    <i class="fas fa-map-marker-alt"></i>
                                                    <span class="span__name ">
                                                        NV lái
                                                    </span>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="item-grid">
                                        <button class="button">
                                            <img src="{{ url('/') }}/pms/images/icon_search.png" alt="icon_search.png">
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="section-loving">
        <div class="bs-container">
            <div class="bs-row">
                <div class="bs-col">
                    <div class="module module-loving">
                        <div class="module-content">
                                <div class="slide-loving">
                                    @foreach ($listCarBrand as $item)
                                    <div class="item">
                                        <a href="{{ route('public.listCar','brand_id='.$item->id) }}" class="item__link" >
                                            <img src="{{ url('/').$item->image }}" alt="" width="130px" height="76px">
                                        </a>
                                    </div>
                                    @endforeach
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="section-receiveCar">
        <div class="bs-container">
            <div class="bs-row">
                <div class="bs-col">
                    <div class="moudle module-receiveCar">
                        <div class="module-header">
                            <h2 class="title">
                                Địa điểm nhận xe được đề xuất ở Hà Nội
                            </h2>
                        </div>
                        <div class="module-content">
                            <div class="bs-row row-lg-15 row-md-15 row-sm-5 row-xs">
                                <div class="bs-col lg-33-15 md-33-15 sm-50-5 xs-100">
                                    <div class="item">
                                        <div class="box">
                                            <h3 class="title medium">
                                                Thuê xe ô tô gần Hồ Tây, TP. Hà Nội
                                            </h3>
                                            <a href="#" class="item__link">
                                                Thuê ô tô tại địa điểm nhận xe này
                                                <i class="fas fa-caret-right"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="bs-col lg-33-15 md-33-15 sm-50-5 xs-100">
                                    <div class="item">
                                        <div class="box">
                                            <h3 class="title medium">
                                                Thuê xe ô tô gần Hồ Tây, TP. Hà Nội
                                            </h3>
                                            <a href="#" class="item__link">
                                                Thuê ô tô tại địa điểm nhận xe này
                                                <i class="fas fa-caret-right"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="bs-col lg-33-15 md-33-15 sm-50-5 xs-100">
                                    <div class="item">
                                        <div class="box">
                                            <h3 class="title medium">
                                                Thuê xe ô tô gần Hồ Tây, TP. Hà Nội
                                            </h3>
                                            <a href="#" class="item__link">
                                                Thuê ô tô tại địa điểm nhận xe này
                                                <i class="fas fa-caret-right"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="section-carlist">
        <div class="bs-container">
            <div class="bs-row">
                <div class="bs-col">
                    <div class="module module-carlist">
                        <div class="module-header">
                            <h2 class="title">
                                xe được ưa chuộng nhất
                            </h2>
                        </div>
                        <div class="module-content">
                            <div class="bs-row row-lg-15 row-md-10 row-sm-5 row-xs-5">
                                @foreach ($listCar as $item)
                                    <div class="bs-col lg-25-15 md-25-10 sm-33-5 xs-50-5 tn-100">
                                        <div class="carlist">
                                            <div class="avata">
                                                <a href="{{ route('public.detailCar', [$item->slug, $item->id]) }}" class="avata__links">
                                                    @if(!empty($item->thumnail))
                                                        <img src="{{ url('/') }}{{ $item->thumnail }}" alt="{{ $item->title }}" class="img_links">
                                                    @else
                                                        <img src="{{ url('/') }}/uploads/images/default.png" alt="{{ $item->title }}" class="img_links">
                                                    @endif
                                                    <div class="category">
                                                        <ul class="item">
                                                            <li class="item-list">
                                                                <b>{{ $item->passenger }}</b> chỗ
                                                            </li>
                                                            <li class="item-list">
                                                                <b>{{ $item->door }}</b> cửa
                                                            </li>
                                                            <li class="item-list">
                                                                {{ $item->gear_shift }}
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </a>
                                                <a class="span_love" onclick="Wishlistcar({{ $item->id }})">
                                                    <i class="fas fa-heart {{ Auth::check() ? ($item->wishlistCarBy(auth()->user()) ? 'is-favourite' : ' '): ' ' }}"
                                                    {{ Auth::check() == false ? ' modal-show=show modal-data=#login ' : ' ' }}
                                                    id="color{{ $item->id }}"></i>
                                                    <span class="favourite {{ Auth::check() ? ($item->wishlistCarBy(auth()->user()) ? 'is-favourite-color' : ' '): ' ' }}""
                                                    {{ Auth::check() == false ? ' modal-show=show modal-data=#login ' : ' ' }}
                                                    id="{{ $item->id }}">{{ sizeOf($item->wishlistCar) }}</span>
                                                </a>
                                            </div>
                                            <div class="content">
                                                <h3 class="title">
                                                    <a href="{{ route('public.detailCar', [$item->slug, $item->id]) }}" class="title__links">
                                                        {{ $item->title }}
                                                    </a>
                                                </h3>
                                                <p class="p__price">
                                                    <span class="span__price">
                                                        @if($item->sale !== null && $item->sale < $item->price)
                                                            {{ number_format($item->sale ).' '.'đ'}}
                                                        @else
                                                            {{ number_format($item->price).' '.'đ' }}
                                                        @endif
                                                    </span>
                                                    / ngày
                                                </p>
                                                <p class="p__map">
                                                    <img src="{{ url('/') }}/pms/images/icon_map.png" alt="icon_map.png">
                                                    {{ $item->location->name }}
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
@endsection
@push('js')
    <script type="text/javascript">
        function Wishlistcar(id) {
            $.ajax({
                type: "GET",
                url: "/car-favourite/"+id ,
                success: function (response) {
                    $('#'+id).html(response.countFavourite).css('color',response.color);
                    $('#color'+id).css('color',response.color);
                }
            });
        }

        $(document).ready(function () {
            $('.slide-loving').slick({
                slidesToShow: 7,
                slidesToScroll: 6,
                prevArrow: '<i class="icon_control fas fa-angle-left"></i>',
                nextArrow: '<i class="icon_control fas fa-angle-right"></i>',
                responsive: [
                    {
                        breakpoint: 1024,
                        settings: {
                            slidesToShow: 3,
                            slidesToScroll: 3,
                        }
                    }
                ]
            });
        });
        $(document).on('click', '.item-list-location', function(e) {
            let text = $(this).text().trim();
            let id = $(this).attr('data-id');
            $(".text-location").val(text);
            $(".location_id").val(id);
            $(this).closest('.item').closest('.item-grid__content').removeClass("active");
        });
        $(document).on('click', '.item-list-car-form', function(e) {
            let text = $(this).text().trim();
            let id = $(this).attr('data-id');
            $(".text-car-form").val(text);
            $(".car_form").val(id);
            $(this).closest('.item').closest('.item-grid__content').removeClass("active");
        });
    </script>

@endpush
