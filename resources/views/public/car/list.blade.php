@extends('public.master')
@section('content')
<style>
    .is-favourite{
        color: red;
    }
    .is-favourite-color{
        color: red;
    }
</style>
<main id="main">
    <section class="section-page section-page__2">
        <div class="bs-container">
            <div class="bs-row">
                <div class="bs-col">
                    <div class="module module-page">
                        <div class="module-header">
                            <h2 class="title">
                                <span class="span__name black"> Thuê xe ô tô cho hành trình của bạn</span>
                                <p class="p__info">
                                    Những trải nghiệm hàng đầu ở Huế để bạn bắt đầu
                                </p>


                            </h2>
                            <div class="page__right">
                                <ul class="item">
                                    <li class="item-list">
                                        <a href="/" class="item-list__links">
                                            Trang chủ
                                        </a>
                                    </li>
                                    <li class="item-list">
                                        <a href="car.html" class="item-list__links">
                                            Car
                                        </a>
                                    </li>
                                    <li class="item-list">
                                        <span class="item-list__links">
                                            Hà Nội
                                        </span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="section-rooms section-list section-list__car">
        <div class="bs-container">
            <form method="GET" id="form-filter-car" action="{{ route('public.listCar') }}">
            <div class="grid">
                <div class="filterMobile">
                    <i class="fas fa-filter"></i>
                    <i class="fas fa-times"></i>
                </div>
                <div class="filterShow grid-sidebar">
                    <div class="from__search">
                        <input type="text" name="title" placeholder="Nhập tên xe" class="input__search" value="{{ Request::get('title') }}">
                        <button class="btn__search">
                            <i class="fas fa-search"></i>
                        </button>
                    </div>
                    <div class="search">


                            <div class="form-group">
                                <label class="lable__name bold" for="">Nơi nhận xe</label>
                                <div class="select">
                                <input type="hidden" name="location_id" class="location_id" value='{{ Request::get('location_id') }}'>
                                    <div class="header-option header-option-location">
                                    <img src="{{ url('/') }}/pms/images/icon_map.png" class="img__option" alt="icon_map.png">
                                        <span class="span__option medium">
                                            @if(!empty(Request::get('location_id')))
                                                @foreach($listLocation as $item)
                                                    @if ($item->id == Request::get('location_id'))
                                                        {{ $item->name }}
                                                    @endif
                                                @endforeach
                                            @else
                                                --Chọn nơi nhận xe--
                                            @endif

                                        </span>
                                    </div>
                                    <div class="body-option">
                                        @foreach ($listLocation as $item)
                                            <div data-id="{{ $item->id }}" class="item-option">
                                                <img src="{{ url('/') }}/pms/images/icon_map.png" class="img__option" alt="icon_map.png">
                                                <span class="span__option medium">
                                                    {{ $item->name }}
                                                </span>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="lable__name bold">Ngày thuê / Ngày trả</label>
                                <div class="select">
                                    <div class="header-option">
                                        <img src="{{ url('/') }}/pms/images/icon_check.png" class="img__option" alt="icon_map.png">
                                        <input type="text" class="" name="daterange" value="{{ Request::get('daterange') }}" readonly>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="lable__name bold" for="">Hình thức xe</label>
                                <div class="select">
                                    <input type="hidden" name="car_form" class="car_form" value="{{ Request::get('car_form') }}">
                                    <div class="header-option header-option-car-form">
                                        <img src="{{ url('/') }}/pms/images/icon_us.png" class="img__option" alt="icon_us.png">
                                        <span class="span__option medium">
                                            @if (Request::get('car_form') == 2)
                                                Xe tự lái
                                            @elseif(Request::get('car_form') == 1)
                                                NV Lái
                                            @else
                                                --Chọn hình thức xe--
                                            @endif
                                        </span>
                                    </div>
                                    <div class="body-option">
                                        <div data-id="2" class="item-option">
                                            <img src="{{ url('/') }}/pms/images/icon_us.png" class="img__option" alt="icon_us.png">
                                            <span class="span__option medium">
                                                Xe tự lái
                                            </span>
                                        </div>
                                        <div data-id="1" class="item-option">
                                            <img src="{{ url('/') }}/pms/images/icon_us.png" class="img__option" alt="icon_us.png">
                                            <span class="span__option medium">
                                                NV Lái
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <button type="submit" class="button__search bold">
                                Tìm kiếm
                            </button>

                    </div>
                    <div class="evaluate">
                        <div class="onCheckRaido item">
                            <div class="item-header">
                                <h3 class="title bold">
                                    Điểm đánh giá
                                </h3>
                            </div>
                            <div class="item-content">
                                <label class="lable__radio label-category {{Request::get('star') == 1 ? 'active' : ''}}">
                                    <span class="span__radio">
                                        1 <br>
                                        Sao
                                    </span>
                                    <input type="radio" class="input__radio" name="star" value="1" {{Request::get('star') == 1 ? 'checked="checked"' : ''}}>
                                </label>
                                <label class="lable__radio label-category {{Request::get('star') == 2 ? 'active' : ''}}">
                                    <span class="span__radio">
                                        2 <br>
                                        Sao
                                    </span>
                                    <input type="radio" class="input__radio" name="star" value="2" {{Request::get('star') == 2 ? 'checked="checked"' : ''}}>
                                </label>
                                <label class="lable__radio label-category {{Request::get('star') == 3 ? 'active' : ''}}">
                                    <span class="span__radio">
                                        3 <br>
                                        Sao
                                    </span>
                                    <input type="radio" class="input__radio" name="star" value="3" {{Request::get('star') == 3 ? 'checked="checked"' : ''}}>
                                </label>
                                <label class="lable__radio label-category {{Request::get('star') == 4 ? 'active' : ''}}">
                                    <span class="span__radio">
                                        4 <br>
                                        Sao
                                    </span>
                                    <input type="radio" class="input__radio" name="star" value="4" {{Request::get('star') == 4 ? 'checked="checked"' : ''}}>
                                </label>
                                <label class="lable__radio label-category {{Request::get('star') == 5 ? 'active' : ''}}">
                                    <span class="span__radio">
                                        5 <br>
                                        Sao
                                    </span>
                                    <input type="radio" class="input__radio" name="star" value="5" {{Request::get('star') == 5 ? 'checked="checked"' : ''}}>
                                </label>
                            </div>
                        </div>
                        <div class="onCheckRaido price">
                            <div class="item-header">
                                <h3 class="title bold">
                                    Chọn khoảng giá
                                </h3>
                            </div>
                            <div class="item-content">

                                <label class="lable__radio label-category {{Request::get('price') == 1 ? 'active' : ''}}">
                                    <span class="span__radio">
                                        <b class="bold">0</b> vnđ - <b class="bold">5</b> triệu vnđ
                                    </span>
                                    <input type="radio" class="input__radio" name="price" value="1" {{Request::get('price') == 1 ? 'checked' : ''}}>
                                </label>
                                <label class="lable__radio label-category {{Request::get('price') == 2 ? 'active' : ''}}">
                                    <span class="span__radio ">
                                        <b class="bold">5</b> triệu vnđ - <b class="bold">10</b> triệu vnđ
                                    </span>
                                    <input type="radio" class="input__radio" name="price" value="2" {{Request::get('price') == 2 ? 'checked' : ''}}>
                                </label>
                                <label class="lable__radio label-category {{Request::get('price') == 3 ? 'active' : ''}}">
                                    <span class="span__radio">
                                        <b class="bold">10</b> triệu vnđ - <b class="bold">15</b> triệu vnđ
                                    </span>
                                    <input type="radio" class="input__radio" name="price" value="3" {{Request::get('price') == 3 ? 'checked' : ''}}>
                                </label>

                        </div>
                        </div>
                        <div class="onCheckRaido price date">
                            <div class="item-header">
                                <h3 class="title bold">
                                    Loại xe ô tô
                                </h3>
                            </div>
                            <div class="item-content">
                                @foreach($termType as $item)
                                    @if(!empty(Request::get('termType')) && in_array($item->id, Request::get('termType')))
                                        <label data-id="{{ $item->id }}" class="lable__radio lable-radio-term-type active">
                                            <span class="span__radio">
                                                {{ $item->name }}
                                            </span>
                                        </label>
                                    @else
                                        <label data-id="{{ $item->id }}" class="lable__radio lable-radio-term-type">
                                            <span class="span__radio">
                                                {{ $item->name }}
                                            </span>
                                        </label>
                                    @endif
                                @endforeach
                            </div>
                            <div>
                                @foreach($termType as $item)
                                    @if(!empty(Request::get('termType')) && in_array($item->id, Request::get('termType')))
                                        <input type="checkbox" id="termType-checkbox-{{ $item->id }}" class="input__radio" name="termType[]" value="{{ $item->id }}" checked>
                                    @else
                                        <input type="checkbox" id="termType-checkbox-{{ $item->id }}" class="input__radio" name="termType[]" value="{{ $item->id }}">
                                    @endif
                                @endforeach
                            </div>
                        </div>
                        <div class="onCheckRaido price date">
                            <div class="item-header">
                                <h3 class="title bold">
                                    tính năng xe
                                </h3>
                            </div>
                            <div class="item-content">
                            @foreach($termFeatures as $item)
                                @if(!empty(Request::get('termFeatures')) && in_array($item->id, Request::get('termFeatures')))
                                    <label data-id="{{ $item->id }}" class="lable__radio lable-radio-term-features active">
                                        <span class="span__radio">
                                            {{ $item->name }}
                                        </span>
                                    </label>
                                @else
                                    <label data-id="{{ $item->id }}" class="lable__radio lable-radio-term-features">
                                        <span class="span__radio">
                                            {{ $item->name }}
                                        </span>
                                    </label>
                                @endif
                            @endforeach
                            </div>
                            <div>
                                @foreach($termFeatures as $item)
                                    @if(!empty(Request::get('termFeatures')) && in_array($item->id, Request::get('termFeatures')))
                                        <input type="checkbox" id="termFeatures-checkbox-{{ $item->id }}" class="input__radio" name="termFeatures[]" value="{{ $item->id }}" checked>
                                    @else
                                        <input type="checkbox" id="termFeatures-checkbox-{{ $item->id }}" class="input__radio" name="termFeatures[]" value="{{ $item->id }}">
                                    @endif
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
                <div class="grid-main">
                    <div class="filter">
                        <div class="filter-price">
                            <div class="control-filter">
                                <span class="span__name bold">
                                    Xem theo:
                                </span>
                                <select class="select__text filter_price" name="filter_price" onchange="submitFormFilterCar()">
                                    <option value="">Giá tiền</option>
                                    <option value="1" {{Request::get('filter_price') == 1 ? 'selected' : ''}}>Thấp đến cao</option>
                                    <option value="2" {{Request::get('filter_price') == 2 ? 'selected' : ''}} >Cao đến thấp</option>
                                    <option value="3" {{Request::get('filter_price') == 3 ? 'selected' : ''}} >Khuyến mại</option>
                                </select>
                            </div>
                        </div></form>
                        <div class="filter-list">
                            <p class="p__text">
                                <b class="bold">{{ $listCar->firstItem().'-'. $listCar->lastItem() }}</b> trong tổng số <b class="bold">{{ $listCar->total() }}</b> xe
                            </p>
                        </div>
                    </div>
                    <div class="grid-main__content">
                        <div class="bs-row row-lg-15 row-md-10 row-sm-5 row-xs-5">
                            @foreach ($listCar as $item)
                                <div class="bs-col lg-33-15 md-33-10 sm-33-5 xs-50-5 tn-100">
                                    <div class="carlist">
                                        <div class="avata">
                                            <a href="{{ route('public.detailCar', [$item->slug, $item->id]) }}" class="avata__links">
                                                @if(!empty($item->thumnail))
                                                    <img src="{{ url('/') }}{{ $item->thumnail }}" alt="{{ $item->title }}" class="img_links">
                                                @else
                                                    <img src="{{ url('/') }}/uploads/images/default.png" alt="{{ $item->title }}" class="img_links">
                                                @endif
                                                <div class="category">
                                                    <ul class="item">
                                                        <li class="item-list">
                                                            <b>{{ $item->passenger }}</b> chỗ
                                                        </li>
                                                        <li class="item-list">
                                                            <b>{{ $item->door }}</b> cửa
                                                        </li>
                                                        <li class="item-list">
                                                            {{ $item->gear_shift }}
                                                        </li>
                                                    </ul>
                                                </div>
                                            </a>
                                            <a class="span_love" onclick="Wishlistcar({{ $item->id }})">
                                                <i class="fas fa-heart {{ Auth::check() ? ($item->wishlistCarBy(auth()->user()) ? 'is-favourite' : ' '): ' ' }}"
                                                {{ Auth::check() == false ? ' modal-show=show modal-data=#login ' : ' ' }}
                                                id="color{{ $item->id }}"></i>
                                                <span class="favourite {{ Auth::check() ? ($item->wishlistCarBy(auth()->user()) ? 'is-favourite-color' : ' '): ' ' }}""
                                                {{ Auth::check() == false ? ' modal-show=show modal-data=#login ' : ' ' }}
                                                id="{{ $item->id }}">{{ sizeOf($item->wishlistCar) }}</span>
                                            </a>
                                        </div>
                                        <div class="content">
                                            <h3 class="title">
                                                <a href="{{ route('public.detailCar', [$item->slug, $item->id]) }}" class="title__links">
                                                    {{ $item->title }}
                                                </a>
                                            </h3>
                                            <p class="p__price">
                                                <span class="span__price">
                                                    @if($item->sale !== null && $item->sale < $item->price)
                                                        {{ number_format($item->sale, 0, ",", ".") }} đ
                                                    @else
                                                        {{ number_format($item->price, 0, ",", ".") }} đ
                                                    @endif
                                                </span>
                                                / ngày
                                            </p>
                                            <p class="p__map">
                                                <img src="{{ url('/') }}/pms/images/icon_map.png" alt="icon_map.png">
                                                {{ optional($item->location)->name }}
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                        <div class="fexPagintaion text-center clearfix">
                            {{ $listCar->links() }}
                        </div>
                    </div>
                </div>
            </div>
    </section>
    <section class="section-domestic">
        <div class="bs-container">
            <div class="bs-row">
                <div class="bs-col">
                    <div class="module module-domestic">
                        <div class="module-header">
                            <h2 class="title">
                                Điểm đến yêu thích trong nước
                            </h2>
                            <p class="p__info">
                                Bao la khắp nước. Bốn bể là nhà
                            </p>
                        </div>
                        <div class="module-content">
                            <div class="grid">
                                @foreach ($locationFeatured as $item)
                                    <div class="grid-item">
                                        <a href="#" class="item__links" width="100%">
                                            <div class="avata" width="100%">
                                                @if(!empty($item->feature_image))
                                                    <img src="{{url('/')}}{{ $item->feature_image }}" alt="{{ $item->name }}" width="100%">
                                                @else
                                                    <img src="{{ url('/') }}/uploads/images/default.png" alt="{{ $item->name }}" width="100%">
                                                @endif
                                            </div>
                                        </a>
                                        <div class="content">
                                            <h3 class="title">
                                                {{ $item->name }}
                                            </h3>
                                            <ul class="category">
                                                <li class="category-list">
                                                    <a href="{{ route('public.listTour', ['end_location' => $item->id]) }}" class="category-list__links">
                                                        {{ sizeof($item->tour) }}
                                                        <span class="span_name">
                                                            Tours
                                                        </span>
                                                    </a>
                                                </li>
                                                <li class="category-list">
                                                    <a href="{{ route('hotel.ds', ['location_id' => $item->id]) }}" class="category-list__links">
                                                        {{ sizeof($item->hotel) }}
                                                        <span class="span_name">
                                                            Hotels
                                                        </span>

                                                    </a>
                                                </li>
                                                <li class="category-list">
                                                    <a href="{{ route('public.listCar', ['location_id' => $item->id]) }}" class="category-list__links">
                                                        {{ sizeof($item->car) }}
                                                        <span class="span_name">
                                                            Cars
                                                        </span>
                                                    </a>
                                                </li>

                                            </ul>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
@endsection
@push('js')
    <script type="text/javascript">
        function Wishlistcar(id) {
            $.ajax({
                type: "GET",
                url: "/car-favourite/"+id ,
                success: function (response) {
                    $('#'+id).html(response.countFavourite).css('color',response.color);
                    $('#color'+id).css('color',response.color);
                }
            });
        }

        $(document).ready(function () {


            // hien thi danh sach turs

            function selecOption() {
                $('.select .header-option-location').click(function () {
                    $(this).next('.body-option').toggleClass('active');
                    var header = $(this);
                    $(this).next('.body-option').children('.item-option').click(function () {
                        var html = $(this).html();
                        var id = $(this).attr('data-id');
                        header.html(html);
                        header.parent('.select').children('.location_id').val(id);
                        $('.body-option').removeClass('active');

                    });

                });
                $('.select .header-option-car-form').click(function () {
                    $(this).next('.body-option').toggleClass('active');
                    var header = $(this);
                    $(this).next('.body-option').children('.item-option').click(function () {
                        var html = $(this).html();
                        var id = $(this).attr('data-id');
                        header.html(html);
                        header.parent('.select').children('.car_form').val(id);
                        $('.body-option').removeClass('active');

                    });

                });

            }
            function slideProduct() {
                $('.slider-for').slick({
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    arrows: false,
                    fade: false,
                    asNavFor: '.slider-nav'
                });
                $('.slider-nav').slick({
                    slidesToShow: 4,
                    slidesToScroll: 1,
                    asNavFor: '.slider-for',
                    focusOnSelect: true,
                });
            }
            slideProduct();
            selecOption();

            $(document).on('click', '.lable-radio-term-type', function(e) {
                var id = $(this).attr('data-id');
                if($(this).hasClass('active')) {
                    $(this).removeClass('active');
                    $('#termType-checkbox-'+id).prop('checked', false);
                }else{
                    $(this).addClass('active');
                    $('#termType-checkbox-'+id).prop('checked', true);
                }
                submitFormFilterCar();
            });
            $(document).on('click', '.lable-radio-term-features', function(e) {
                var id = $(this).attr('data-id');
                if($(this).hasClass('active')) {
                    $(this).removeClass('active');
                    $('#termFeatures-checkbox-'+id).prop('checked', false);
                }else{
                    $(this).addClass('active');
                    $('#termFeatures-checkbox-'+id).prop('checked', true);
                }
                submitFormFilterCar();
            });
        });
        $(document).on('click', '.label-category', function(e){
            e.preventDefault();

                    if($(this).hasClass('active')) {
                        $(this).removeClass('active');
                        $(this).prevAll('.label-category').removeClass('active');
                        $(this).nextAll('.label-category').removeClass('active');
                        console.log(1, $(this).children('.input__radio'));

                        $(this).children('.input__radio').prop('checked', false);
                    } else {
                        $(this).prevAll('.label-category').removeClass('active');
                        $(this).nextAll('.label-category').removeClass('active');
                        $(this).addClass('active');
                        console.log(2, $(this).children('.input__radio'));
                        $(this).children('.input__radio').prop('checked', true);
                    }

                    submitFormFilterCar();
                });
        function submitFormFilterCar (){
                $('#form-filter-car').submit();
        }
    </script>

@endpush
