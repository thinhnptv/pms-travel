@extends('public.master')
@section('content')
<main id="main">
    <section class="section-page section-detail__page cart-detail">
        <div class="bs-container">
            <div class="bs-row">
                <div class="bs-col">
                    <div class="module module-page">
                        <div class="module-content">
                            <form action="{{ route('public.listCar') }}" method="GET">
                                <div class="grid">
                                    <div class="item-grid">
                                        <div class="item-grid__header  clickShow">
                                            <div class="item_icon">
                                                <img src="{{ url('/') }}/pms/images/icon_map.png" alt="icon_map.png">
                                            </div>
                                            <div class="item__content">
                                                <p class="p__text addText">
                                                    <input type="text" class="text-location" placeholder="Nơi nhận xe" readonly>
                                                    <input type="hidden" name="location_id" class="location_id">
                                                </p>
                                                <i class="fas fa-caret-down"></i>
                                            </div>
                                        </div>
                                        <div class="item-grid__content">
                                            <ul class="item">
                                                @foreach ($listLocation as $item)
                                                    <li data-id="{{ $item->id }}" class="item-list item-list-location">
                                                        <i class="fas fa-map-marker-alt"></i>
                                                        <span class="span__name ">
                                                            {{ $item->name }}
                                                        </span>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="item-grid">
                                        <div class="item-grid__header">
                                            <div class="item_icon">
                                                <img src="{{ url('/') }}/pms/images/icon_check.png" alt="icon_check.png">
                                            </div>
                                            <div class="item__content">
                                                <p class="p__text acDates">

                                                    <input type="text" name="daterange" readonly/>
                                                </p>
                                                <i class="fas fa-caret-down"></i>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="item-grid">
                                        <div class="item-grid__header  clickShow">
                                            <div class="item_icon">
                                                <img src="{{ url('/') }}/pms/images/icon_us.png" alt="icon_map.png">
                                            </div>
                                            <div class="item__content">
                                                <p class="p__text addText">
                                                    @if (Request::get('car_form') == 1)
                                                        <input type="text" class="text-car-form" placeholder="Hình thức (Tự lái, có lái)" value="Xe tự lái" readonly>
                                                    @elseif(Request::get('car_form') == 2)
                                                        <input type="text" class="text-car-form" placeholder="Hình thức (Tự lái, có lái)" value="NV lái" readonly>
                                                    @else
                                                        <input type="text" class="text-car-form" placeholder="Hình thức (Tự lái, có lái)"  readonly>
                                                    @endif
                                                    <input type="hidden" name="car_form" class="car_form" value="{{ Request::get('car_form') }}">
                                                </p>
                                                <i class="fas fa-caret-down"></i>
                                            </div>
                                        </div>
                                        <div class="item-grid__content">
                                            <ul class="item">
                                                <li data-id="1" class="item-list item-list-car-form">
                                                    <i class="fas fa-map-marker-alt"></i>
                                                    <span class="span__name ">
                                                        Xe tự lái
                                                    </span>
                                                </li>
                                                <li data-id="2" class="item-list item-list-car-form">
                                                    <i class="fas fa-map-marker-alt"></i>
                                                    <span class="span__name ">
                                                        NV lái
                                                    </span>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="item-grid">
                                        <button class="button black">
                                            Tìm kiếm
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="section-category">
        <div class="bs-container">
            <div class="bs-row">
                <div class="bs-col">
                    <div class="module module-category">
                        <div class="module-header">
                            <ul class="category">
                                <li class="category-list">
                                    <a href="/" class="category-list__link">
                                        Trang chủ
                                    </a>
                                </li>
                                <li class="category-list">
                                    <a href="{{ route('public.indexCar') }}" class="category-list__link">
                                        Car
                                    </a>
                                </li>
                                <li class="category-list">
                                    <span class="category-list__link">
                                        {{ $detailCar->title }}
                                    </span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="cart-detail section-detail ">
        <div class="bs-container">
            <div class="bs-row">
                <div class="bs-col">
                    <div class="module module-detail">
                        <div class="module-header clearfix">
                            <div class="header-info">
                                <h2 class="title bold">
                                    {{ $detailCar->title }}
                                </h2>
                                <p class="p__category">
                                    <span class="span__star">
                                        @for ($i = 0; $i < $detailCar->rating_star; $i++)
                                            <i class="fas fa-star"></i>
                                        @endfor
                                    </span>
                                    <span class="span__number bold">
                                        {{ $detailCar->rating_star }}/5
                                    </span>
                                    <span class="span__name bold">
                                        Tuyệt vời
                                    </span>
                                    <span class="span__text">
                                        | {{ sizeof($detailCar->reviewcar) }} đánh giá
                                    </span>
                                </p>
                                <p class="p__address">
                                    <img src="{{ url('/') }}/pms/images/icon_map.png" class="img__link" alt="icon_map.png">
                                    <span class="span__name">
                                        {{ $detailCar->address. ', ' .$detailCar->location->name }}
                                    </span>
                                </p>
                            </div>
                            <div class="header-buy">
                                <p class="p__name medium">
                                    Giá <span class="span__number black">
                                    @if($detailCar->sale !== null && $detailCar->sale < $detailCar->price)
                                        {{ number_format($detailCar->sale, 0, ",", ".") }} đ
                                    @else
                                        {{ number_format($detailCar->price, 0, ",", ".") }}
                                    @endif
                                    </span>
                                </p>
                                <a href="#" class="buy__link black">
                                    Thuê xe ngay
                                </a>
                            </div>
                        </div>
                        <div class="module-content">
                            <div class="slide-gallria">
                                <div class="galleria">
                                    @if (!empty($detailCar->album))
                                        @foreach (json_decode($detailCar->album) as $item)
                                            <a href="{{ url('/') }}{{ $item }}">
                                                <img class="img__links" src="{{ url('/') }}{{ $item }}"
                                                    data-big="{{ url('/') }}{{ $item }}">
                                            </a>
                                        @endforeach
                                    @else
                                        <a href="{{ url('/') }}/uploads/images/default.png">
                                            <img class="img__links" src="{{ url('/') }}/uploads/images/default.png"
                                                data-big="{{ url('/') }}/uploads/images/default.png">
                                        </a>
                                    @endif
                                </div>
                            </div>
                            <div class="content">
                                <h3 class="title__info">
                                    Mô tả xe
                                </h3>
                                <div class="p__text">
                                    {!! $detailCar->content !!}
                                </div>
                            </div>
                            <div class="basis">
                                <h3 class="title-basis">
                                    Tiện ích của xe {{ $detailCar->title }}
                                </h3>
                                <div class="item">
                                    <ul class="item-basis clearfix">
                                        @foreach ($detailCar->termfeature as $item)
                                            <li class="item-basis__list">
                                                <i class="fas fa-check"></i>
                                                {{ $item->name }}
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                            <div class="necessary">
                                <h3 class="title-necessary medium">
                                    Thông tin cần biết
                                </h3>
                                <p class="p__text">
                                    <span class="span__name medium">
                                        7:00 PM - 7:00 AM:
                                    </span>
                                    Tính chi phí 1 ngày
                                </p>
                                <p class="p__text">
                                    <span class="span__name medium">7:00 AM - 1:00 PM:</span> Tính chi phí 1/2 ngày
                                </p>
                                <p class="p__text">
                                    Các loại xe khác nhau có thể có chính sách hủy đặt xe và chính sách thanh toán
                                    trước khác nhau. Vui lòng kiểm tra chi tiết chính sách xe khi chọn xe ở phía
                                    trên
                                </p>
                            </div>
                            <div class="question">
                                <h3 class="title-question">
                                    Câu hỏi thường gặp
                                </h3>

                                <div class="panel-group" id="accordion">
                                    @foreach ($detailCar->faq as $item)
                                        <div class="panel">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" data-parent="#accordion" href="#question-{{ $item->id }}">
                                                        <img src="{{ url('/') }}/pms/images/icon_message.png" alt="icon_message.png"
                                                            class="icon icon__message">
                                                            {{ $item->title }}
                                                        <i class="fas fa-angle-down"></i>
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="question-{{ $item->id }}" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    {!! $item->content !!}
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>

                            </div>
                            <div class="evaluate">
                                <h3 class="title-evaluate">
                                    Đánh giá
                                </h3>
                                <div class="evaluate-container">
                                    <div class="bs-row row-lg-10 row-md-10 row-ms-5 row-xs">
                                        <div class="bs-col lg-25-10 md-25-10 sm-30-5 xs-100">
                                            <div class="item">
                                                <span class="span__number medium">
                                                {{ $detailCar->avgReview }}<sub>/5</sub>
                                                </span>
                                                <p class="p__text light">
                                                    Tuyệt vời
                                                </p>
                                                <p class="p__evaluate">
                                                    Dựa trên {{ sizeof($detailCar->reviewcar) }} đánh giá
                                                </p>
                                            </div>
                                        </div>
                                        <div class="bs-col lg-75-10 md-75-10 sm-70-5 xs-100">
                                            <div class="evaluate-content">
                                                <p class="p__evaluate active">
                                                    <span class="span__name">
                                                        Tuyệt vời
                                                    </span>
                                                    <span class="span__number">
                                                        3
                                                    </span>
                                                </p>
                                                <p class="p__evaluate">
                                                    <span class="span__name">
                                                        Rất tốt
                                                    </span>
                                                    <span class="span__number">
                                                        0
                                                    </span>
                                                </p>
                                                <p class="p__evaluate">
                                                    <span class="span__name">
                                                        Cũng được
                                                    </span>
                                                    <span class="span__number">
                                                        0
                                                    </span>
                                                </p>
                                                <p class="p__evaluate">
                                                    <span class="span__name">
                                                        Trung bình
                                                    </span>
                                                    <span class="span__number">
                                                        0
                                                    </span>
                                                </p>
                                                <p class="p__evaluate">
                                                    <span class="span__name">
                                                        Kém
                                                    </span>
                                                    <span class="span__number">
                                                        0
                                                    </span>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @php
                                // dd($detailCar);
                            @endphp
                            <div class="comment">
                                @foreach ($reviewcar as $item)
                                <div class="comment-container">
                                    <div class="avata">
                                        <div class="ImagesFrame">
                                            <div class="ImagesFrameCrop0">
                                                <img src="{{ url('/') }}{{ $item->user->avatar }}" alt="{{ $item->user->lastname }}">
                                            </div>
                                        </div>

                                    </div>
                                    <div class="content">

                                        <h3 class="title__name bold">
                                            {{ $item->user->firstname }} {{ $item->user->lastname }}
                                            <span class="span__star">
                                                @for ($stars = 0; $stars < $item->average; $stars++)
                                                    <i class="fas fa-star"></i>
                                                @endfor
                                            </span>
                                        </h3>
                                        <p class="p__day">
                                            {{ $item->created_at }}
                                        </p>
                                        <p class="p__content">
                                            {!! $item->content !!}
                                        </p>
                                    </div>
                                </div>
                                @endforeach
                                <div class="fexPagintaion text-center clearfix">
                                    {{ $reviewcar->links() }}
                                </div>
                                <div class="comment-footer">
                                    <p class="p__total">
                                        Hiển thị {{ $reviewcar->firstItem().'-'. $reviewcar->lastItem() }} trong tổng số {{ $reviewcar->total() }} đánh giá
                                    </p>
                                    @if (Auth::check())
                                        {{-- <p> {{ Auth::user()->firstname }}</p> --}}
                                    @else
                                        <p class="p__login">
                                            Bạn cần phải <a href="#" class="span__name">đăng nhập</a> để đánh giá chuyến đi
                                        </p>
                                    @endif

                                </div>
                            </div>
                            <div class="like">
                                <h3 class="title-like">
                                    Có thể bạn cũng thích
                                </h3>
                                <div class="bs-row row-lg-10 row-md-10 row-sm-5 row-xs-5">
                                    @foreach ($listCarInvolve as $item)
                                        <div class="bs-col lg-25-15 md-25-10 sm-33-5 xs-50-5 tn-100">
                                            <div class="carlist">
                                                <div class="avata">
                                                    <a href="{{ route('public.detailCar', [$item->slug, $item->id]) }}" class="avata__links">
                                                        @if(!empty($item->thumnail))
                                                            <img src="{{ url('/') }}{{ $item->thumnail }}" alt="{{ $item->title }}" class="img_links">
                                                        @else
                                                            <img src="{{ url('/') }}/uploads/images/default.png" alt="{{ $item->title }}" class="img_links">
                                                        @endif
                                                        <div class="category">
                                                            <ul class="item">
                                                                <li class="item-list">
                                                                    <b>{{ $item->passenger }}</b> chỗ
                                                                </li>
                                                                <li class="item-list">
                                                                    <b>{{ $item->door }}</b> cửa
                                                                </li>
                                                                <li class="item-list">
                                                                    {{ $item->gear_shift }}
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </a>
                                                    <a class="span_love" onclick="Wishlistcar({{ $item->id }})">
                                                        <i class="fas fa-heart {{ Auth::check() ? ($item->wishlistCarBy(auth()->user()) ? 'is-favourite' : ' '): ' ' }}"
                                                        {{ Auth::check() == false ? ' modal-show=show modal-data=#login ' : ' ' }}
                                                        id="color{{ $item->id }}"></i>
                                                        <span class="favourite {{ Auth::check() ? ($item->wishlistCarBy(auth()->user()) ? 'is-favourite-color' : ' '): ' ' }}""
                                                        {{ Auth::check() == false ? ' modal-show=show modal-data=#login ' : ' ' }}
                                                        id="{{ $item->id }}">{{ sizeOf($item->wishlistCar) }}</span>
                                                    </a>
                                                </div>
                                                <div class="content">
                                                    <h3 class="title">
                                                        <a href="{{ route('public.detailCar', [$item->slug, $item->id]) }}" class="title__links">
                                                            {{ $item->title }}
                                                        </a>
                                                    </h3>
                                                    <p class="p__price">
                                                        <span class="span__price">
                                                            @if($item->sale !== null && $item->sale < $item->price)
                                                                {{ number_format($item->sale, 0, ",", ".") }} đ
                                                            @else
                                                                {{ number_format($item->price, 0, ",", ".") }} đ
                                                            @endif
                                                        </span>
                                                        / ngày
                                                    </p>
                                                    <p class="p__map">
                                                        <img src="{{ url('/') }}/pms/images/icon_map.png" alt="icon_map.png">
                                                        {{ $item->location->name }}
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </section>
</main>
@endsection
@push('js')
    <script src="{{ url('/') }}/pms/js/galleria.js"></script>
    <script>

    function Wishlistcar(id) {
        $.ajax({
            type: "GET",
            url: "/car-favourite/"+id ,
            success: function (response) {
                $('#'+id).html(response.countFavourite).css('color',response.color);
                $('#color'+id).css('color',response.color);
            }
        });
    }

    $(function () {
        // Load the Azur theme
        Galleria.loadTheme('{{ url('/') }}/pms/js/galleria.azur.js');

        // Initialize Galleria
        Galleria.run('.galleria');
    });
    </script>
    <script type="text/javascript">
        $(document).on('click', '.item-list-location', function(e) {
            let text = $(this).text().trim();
            let id = $(this).attr('data-id');
            $(".text-location").val(text);
            $(".location_id").val(id);
            $(".item-grid__content").removeClass("active");
        });
        $(document).on('click', '.item-list-car-form', function(e) {
            let text = $(this).text().trim();
            let id = $(this).attr('data-id');
            $(".text-car-form").val(text);
            $(".car_form").val(id);
            $(".item-grid__content").removeClass("active");
        });
    </script>
@endpush
