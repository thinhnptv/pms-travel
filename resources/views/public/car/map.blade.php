@extends('public.master')

@section('css-top')
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
@endsection
@section('content')

<main id="main">
    <section class="section-page section-page__2">
        <div class="bs-container">
            <div class="bs-row">
                <div class="bs-col">
                    <div class="module module-page">
                        <div class="module-header">
                            <h2 class="title">
                                <span class="span__name black"> Thuê xe ô tô cho hành trình của bạn</span>
                                <p class="p__info">
                                    Những trải nghiệm hàng đầu ở Huế để bạn bắt đầu
                                </p>
                            </h2>
                            <div class="page__right">
                                <ul class="item">
                                    <li class="item-list">
                                        <a href="/" class="item-list__links">
                                            Trang chủ
                                        </a>
                                    </li>
                                    <li class="item-list">
                                        <a href="car.html" class="item-list__links">
                                            Car
                                        </a>
                                    </li>
                                    <li class="item-list">
                                        <span class="item-list__links">
                                            Hà Nội
                                        </span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="section-rooms section-list section-list__car">
        <div class="container ">
            <form action="{{ route('public.map.listCar') }}" method="get" class="form-filter-car">
                <div class="row ">
                    <div class="col-md-3 col-sm-3 col-lg-3">
                        <div class="form-group">
                            <label for="">Chọn địa điểm</label>
                            <select name="location_id" id="" class="form-control submit-form">
                                <option value="">--Chọn địa điểm--</option>
                                @foreach ($listLocation as $item)
                                    <option value="{{ $item->id }}" @if(Request::get('location_id') == $item->id) selected @endif >{{ $item->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-3 col-lg-3">
                        <label for="">Chọn thời gian</label>
                        <div class="form-group">
                            <input type="text" class="form-control " name="daterange" value="{{ Request::get('daterange') }}" readonly>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-3 col-lg-3">
                        <div class="form-group">
                            <label for="">Chọn hình thức xe</label>
                            <select name="car_form" id="" class="form-control submit-form">
                                <option value="">--Chọn hình thức--</option>
                                <option value="2" @if(Request::get('car_form') == 2) selected @endif>Xe tự lái</option>
                                <option value="1" @if(Request::get('car_form') == 1) selected @endif>Xe có lái</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-3 col-lg-3"><div class="form-group">
                        <label for="">Chọn khoảng giá</label>
                        <select name="price" id="" class="form-control submit-form">
                            <option value="">--Chọn khoảng giá--</option>
                            <option value="1" @if(Request::get('price') == 1) selected @endif>Từ 0 vnđ - 5 triệu vnđ</option>
                            <option value="2" @if(Request::get('price') == 2) selected @endif>5 triệu vnđ - 10 triệu vnđ</option>
                            <option value="3" @if(Request::get('price') == 3) selected @endif>10 triệu vnđ - 15 triệu vnđ</option>
                        </select>
                    </div></div>
                </div>
            </form>
        </div>
        <div class="bs-container">
            <div class="grid">
                <div class="filterMobile">
                    <i class="fas fa-filter"></i>
                    <i class="fas fa-times"></i>
                </div>
                <div class="filterShow grid-sidebar div-50">
                    <div class="row">
                        <div class="col-12">
                            <div class="form-group mb-3 ">
                                <div id="gmaps-basic" class="gmaps gmap-car"></div>
                                <i>Click onto map to place Location address</i>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="grid-main div-50 height-fix">
                    <div class="grid-main__content">
                        <div class="bs-row row-lg-15 row-md-10 row-sm-5 row-xs-5">
                            @foreach ($listCar as $item)
                            <div class="bs-col lg-50-15 md-50-10 sm-50-5 xs-50-5 tn-100">
                                <div class="carlist">
                                    <div class="avata">
                                        <a href="{{ route('public.detailCar', [$item->slug, $item->id]) }}" class="avata__links">
                                            <img src="{{url('/')}}{{ $item->thumnail }}" alt="{{ $item->title }}" class="img_links">
                                            <div class="category">
                                                <ul class="item">
                                                    <li class="item-list">
                                                        <b>{{ $item->passenger }}</b> chỗ
                                                    </li>
                                                    <li class="item-list">
                                                        <b>{{ $item->door }}</b> cửa
                                                    </li>
                                                    <li class="item-list">
                                                        {{ $item->gear_shift }}
                                                    </li>
                                                </ul>
                                            </div>
                                        </a>
                                        <a class="span_love" href="#">
                                            <i class="fas fa-heart"></i>
                                            {{ sizeof($item->reviewcar) }}
                                        </a>
                                    </div>
                                    <div class="content">
                                        <h3 class="title">
                                            <a href="{{ route('public.detailCar', [$item->slug, $item->id]) }}" class="title__links">
                                                {{ $item->title }}
                                            </a>
                                        </h3>
                                        <p class="p__price">
                                            <span class="span__price">
                                                @if($item->sale !== null && $item->sale < $item->price)
                                                    {{ number_format($item->sale, 0, ",", ".") }} đ
                                                @else
                                                    {{ number_format($item->price, 0, ",", ".") }} đ
                                                @endif
                                            </span>
                                            / ngày
                                        </p>
                                        <p class="p__map">
                                            <img src="{{url('/pms/images/icon_map.png')}}" alt="icon_map.png">
                                            {{ optional($item->location)->name }}
                                        </p>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                        <div class="fexPagintaion text-center d-flex justify-content-center clearfix">
                            {{ $listCar->render() }}
                        </div>
                    </div>
                </div>
            </div>
    </section>
    <input type="hidden" name="" class="list-car" value="{{ json_encode($listCar) }}">

</main>
@endsection
@push('js')
<script>
    var listCar = JSON.parse($('.list-car').val());
</script>
<script>
    var bookingCore = {
        url: 'http://sandbox.bookingcore.org',
        map_provider: 'gmap',
        map_gmap_key: '',
        csrf: 'JMrM5NwcxQy6HWOmuo7LQ2kHPh7pGwfbOPpXxkue'
    };
</script>

<script src="https://maps.google.com/maps/api/js?key=AIzaSyDsucrEdmswqYrw0f6ej3bf4M4suDeRgNA"></script>
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script>
<script src="{{ URL::asset('assets/libs/gmaps/gmaps.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/map-engine.js') }}"></script>
<script>
    jQuery(function ($) {
        new BravoMapEngine('gmaps-basic', {
            fitBounds: true,
            center: [{{"21.008"}}, {{"105.858"}}],
            zoom:{{"8"}},
            ready: function (engineMap) {
                $.each(listCar['data'], function( index, value ) {
                    engineMap.addMarker([parseFloat(value.latitu), parseFloat(value.longtitu)], {
                        icon_options: {}
                    });
                });

            }
        });
    })
</script>
<script>
    $(document).on('change', '.submit-form', function() {
        $('.form-filter-car').submit();
    });
    $(document).on('click', '.applyBtn', function() {
        $('.form-filter-car').submit();
    });
</script>
@endpush
