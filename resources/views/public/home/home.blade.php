@extends('public.master')
@section('content')
<style>
    .is-favourite{
        color: red;
    }
    .is-favourite-color{
        color: red;
    }
</style>
<main id="main">
    @include('public.banner')
    <section class="section-vacation">
        <div class="bs-container">
            <div class="bs-row">
                <div class="bs-col">
                    <div class="module module-vacation">
                        <div class="module-header">
                            <h2 class="title">
                                Giảm giá trong kỳ nghỉ
                            </h2>
                            <p class="p__info">
                                Được lựa chọn cẩn thận bởi các chuyên gia du lịch của chúng tôi<br />
                                cho các quyết định trong kỳ nghỉ của bạn
                            </p>
                        </div>
                        <div class="module-content">
                            <div class="slide-vacation">
                                @foreach ($carSaleHoliday as $item)
                                    <div class="item">
                                        <div class="vacation">
                                            <div class="avata">
                                                <a class="span_love" onclick="WishlistcarHoliday({{ $item->id }})">
                                                    <i class="fas fa-heart {{ Auth::check() ? ($item->wishlistCarBy(auth()->user()) ? 'is-favourite' : ' '): ' ' }}" {{ Auth::check() == false ? ' modal-show=show modal-data=#login ' : ' ' }} id="car-holiday-color{{ $item->id }}"></i>
                                                    <span class="favourite {{ Auth::check() ? ($item->wishlistCarBy(auth()->user()) ? 'is-favourite-color' : ' '): ' ' }}" {{ Auth::check() == false ? ' modal-show=show modal-data=#login ' : ' ' }} id="car-holiday-{{ $item->id }}">{{ sizeOf($item->wishlistCar) }}</span>
                                                </a>

                                                <a href="{{ route('public.detailCar', [$item->slug, $item->id]) }}" class="item-link">
                                                    <img src="{{url('/')}}{{ $item->thumnail }}" class="img__links" alt="{{ $item->title }}">
                                                        @if($item->sale !== null && $item->sale < $item->price)
                                                        <span class="span__sale">
                                                            <span class="number">
                                                                {{ round((($item->price - $item->sale)/$item->price)*100) }} %
                                                            </span>
                                                        </span>
                                                    @endif
                                                </a>
                                            </div>
                                            <div class="content">
                                                <h3 class="title">
                                                    <a href="#" class="title__links">
                                                        {{  $item->title }}
                                                    </a>
                                                </h3>
                                                <p class="p__text">
                                                    Giá chỉ từ
                                                    <span class="span__name">
                                                        @if($item->sale !== null && $item->sale < $item->price)
                                                            {{ number_format($item->sale, 0, ",", ".") }} đ
                                                        @else
                                                            {{ number_format($item->price, 0, ",", ".") }} đ
                                                        @endif
                                                    </span>
                                                    /ngày
                                                </p>
                                                <div class="content__footer">
                                                    <p class="p__left">
                                                        <img src="{{url('/')}}/pms/images/icon_map.png" alt="icon_map.png" class="map_img">
                                                        <span class="span__namee">
                                                            {{ optional($item->location)->name }}
                                                        </span>
                                                    </p>
                                                    <p class="p__right">
                                                        @for ($i = 0; $i < $item->rating_star; $i++)
                                                            <i class="fas fa-star"></i>
                                                        @endfor
                                                    <span class="span__number">{{ sizeof($item->reviewcar) }}</span> Đánh giá
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                                @foreach ($hotelSaleHoliday as $item)
                                    <div class="item">
                                        <div class="vacation">
                                            <div class="avata">
                                                {{-- <a class="span_love" href="{{ route('hotel.ct', [$item->slug, $item->id]) }}">
                                                    <i class="fas fa-heart"></i>
                                                    {{ sizeof($item->wishListHotel) }}
                                                </a> --}}
                                                <a class="span_love" onclick="WishlisthotelHoliday({{ $item->id }})">
                                                    <i class="fas fa-heart {{ Auth::check() ? ($item->wishlistHotelBy(auth()->user()) ? 'is-favourite' : ' ') : ' ' }}" {{ Auth::check() == false ? ' modal-show=show modal-data=#login ' : ' ' }} id="holiday-hotel-color{{ $item->id }}"></i>
                                                    <span class="favourite {{ Auth::check() ? ($item->wishlistHotelBy(auth()->user()) ? 'is-favourite-color' : ' '): ' ' }}" {{ Auth::check() == false ? ' modal-show=show modal-data=#login ' : ' ' }} id="holiday-hotel-{{ $item->id }}">{{ sizeOf($item->wishListHotel) }}</span>
                                                </a>
                                                <a href="{{ route('hotel.ct', [$item->slug, $item->id]) }}" class="item-link">
                                                    <img src="{{url('/')}}{{ $item->feature }}" class="img__links" alt="{{ $item->title }}">
                                                        @if($item->hotel_sale !== null && $item->hotel_sale < $item->price)
                                                        <span class="span__sale">
                                                            <span class="number">
                                                                {{ round((($item->price - $item->hotel_sale) / $item->price)*100) }} %
                                                            </span>
                                                        </span>
                                                    @endif
                                                </a>
                                            </div>
                                            <div class="content">
                                                <h3 class="title">
                                                    <a href="{{ route('hotel.ct', [$item->slug, $item->id]) }}" class="title__links">
                                                        {{  $item->name }}
                                                    </a>
                                                </h3>
                                                <p class="p__text">
                                                    Giá chỉ từ
                                                    <span class="span__name">
                                                        @if($item->hotel_sale !== null && $item->hotel_sale < $item->price)
                                                            {{ number_format($item->hotel_sale, 0, ",", ".") }} đ
                                                        @else
                                                            {{ number_format($item->price, 0, ",", ".") }} đ
                                                        @endif
                                                    </span>
                                                    /đêm
                                                </p>
                                                <div class="content__footer">
                                                    <p class="p__left">
                                                        <img src="{{url('/')}}/pms/images/icon_map.png" alt="icon_map.png" class="map_img">
                                                        <span class="span__namee">
                                                            {{ optional($item->location)->name }}
                                                        </span>
                                                    </p>
                                                    <p class="p__right">
                                                        @for ($i = 0; $i < $item->evaluate_star; $i++)
                                                            <i class="fas fa-star"></i>
                                                        @endfor
                                                    <span class="span__number">{{ sizeof($item->review) }}</span> Đánh giá
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                                @foreach ($tourSaleHoliday as $item)
                                    <div class="item">
                                        <div class="vacation">
                                            <div class="avata">
                                                <a class="span_love" onclick="wishListTourHoliday({{ $item->id }})">
                                                    <i class="fas fa-heart {{ Auth::check() ? ($item->wishlistTourBy(auth()->user()) ? 'is-favourite' : ' ') : ' ' }}" {{ Auth::check() == false ? ' modal-show=show modal-data=#login ' : ' ' }} id="holiday-tour-color{{ $item->id }}"></i>
                                                    <span class="favourite {{ Auth::check() ? ($item->wishlistTourBy(auth()->user()) ? 'is-favourite-color' : ' '): ' ' }}" {{ Auth::check() == false ? ' modal-show=show modal-data=#login ' : ' ' }} id="holiday-tour-{{ $item->id }}">{{ sizeOf($item->wishListTour) }}</span>
                                                </a>
                                                <a href="{{ route('public.detailTour', [$item->slug , $item->id]) }}" class="item-link">
                                                    <img src="{{url('/')}}{{ $item->thumnail }}" class="img__links" alt="{{ $item->title }}">
                                                        @if($item->sale !== null && $item->sale < $item->price)
                                                        <span class="span__sale">
                                                            <span class="number">
                                                                {{ round((($item->price - $item->sale)/$item->price)*100) }} %
                                                            </span>
                                                        </span>
                                                    @endif
                                                </a>
                                            </div>
                                            <div class="content">
                                                <h3 class="title">
                                                    <a href="#" class="title__links">
                                                        {{  $item->title }}
                                                    </a>
                                                </h3>
                                                <p class="p__text">
                                                    Giá chỉ từ
                                                    <span class="span__name">
                                                        @if($item->sale !== null && $item->sale < $item->price)
                                                            {{ number_format($item->sale, 0, ",", ".") }} đ
                                                        @else
                                                            {{ number_format($item->price, 0, ",", ".") }} đ
                                                        @endif
                                                    </span>
                                                </p>
                                                <div class="content__footer">
                                                    <p class="p__left">
                                                        <img src="{{url('/')}}/pms/images/icon_map.png" alt="icon_map.png" class="map_img">
                                                        <span class="span__namee">
                                                            {{ optional($item->location)->name }}
                                                        </span>
                                                    </p>
                                                    <p class="p__right">
                                                        @for ($i = 0; $i < $item->rating_star; $i++)
                                                            <i class="fas fa-star"></i>
                                                        @endfor
                                                    <span class="span__number">{{ sizeof($item->reviewTour) }}</span> Đánh giá
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                            <p class="p__click">
                                Để xem tất cả giảm giá trong các kỳ nghỉ, vui lòng
                                <a href="#" class="click__links">
                                    click vào đây
                                </a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="section-hot">
        <div class="bs-container">
            <div class="bs-row">
                <div class="bs-col">
                    <div class="module module-hot">
                        <div class="module-header">
                            <h2 class="title">
                                Khách sạn nổi bật
                            </h2>
                            <p class="p__info">
                                Khách sạn được đánh giá cao về thiết kế và phục vụ chu đáo, giá thành phải chăng
                            </p>
                        </div>
                        <div class="module-content">
                            <div class="slide-hot">
                                @foreach ($hotelFeatured as $item)
                                    <div class="item">
                                        <div class="hot">
                                            <a class="span_love" onclick="WishlisthotelFeature({{ $item->id }})">
                                                <i class="fas fa-heart {{ Auth::check() ? ($item->wishlistHotelBy(auth()->user()) ? 'is-favourite' : ' ') : ' ' }}" {{ Auth::check() == false ? ' modal-show=show modal-data=#login ' : ' ' }} id="feature-hotel-color{{ $item->id }}"></i>
                                                <span class="favourite {{ Auth::check() ? ($item->wishlistHotelBy(auth()->user()) ? 'is-favourite-color' : ' '): ' ' }}" {{ Auth::check() == false ? ' modal-show=show modal-data=#login ' : ' ' }} id="feature-hotel-{{ $item->id }}">{{ sizeOf($item->wishListHotel) }}</span>
                                            </a>
                                            <a href="#" class="hot__links">
                                                <div class="avata">
                                                    <img src="{{url('/')}}{{ $item->feature }}" alt="{{ $item->title }}">
                                                </div>
                                                <div class="content">
                                                    <div class="content__box">
                                                        <h3 class="title">
                                                            {{ $item->name }}
                                                        </h3>
                                                        <p class="p__pricer">
                                                            Từ <span class="span_number">
                                                                @if (!empty($item->hotel_sale) && $item->hotel_sale < $item->price)
                                                                    {{ number_format($item->hotel_sale, 0, ",", ".") }} đ
                                                                @else
                                                                    {{ number_format($item->price, 0, ",", ".") }} đ
                                                                @endif
                                                            </span> / đêm
                                                        </p>
                                                        <p class="p__like">
                                                            @for ($i = 0; $i < $item->evaluate_star; $i++)
                                                                <i class="fas fa-star"></i>
                                                            @endfor
                                                            <span class="span__number">{{ sizeof($item->review) }}</span> Đánh giá
                                                        </p>
                                                        <p class="p__fotter">
                                                            <img src="{{url('/pms/images/icon_map.png')}}" alt="icon_map.png"
                                                                class="map_img">
                                                            <span class="span__namee">
                                                                {{ optional($item->location)->name }}
                                                            </span>
                                                        </p>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="section-domestic">
    <div class="bs-container">
        <div class="bs-row">
            <div class="bs-col">
                <div class="module module-domestic">
                    <div class="module-header">
                        <h2 class="title">
                            Điểm đến yêu thích trong nước
                        </h2>
                        <p class="p__info">
                            Bao la khắp nước. Bốn bể là nhà
                        </p>
                    </div>
                    <div class="module-content">
                        <div class="grid">
                            @foreach ($locationFeatured as $item)
                                <div class="grid-item">
                                    <a href="#" class="item__links" width="100%">
                                        <div class="avata" width="100%">
                                            @if(!empty($item->feature_image))
                                                <img src="{{url('/')}}{{ $item->feature_image }}" alt="{{ $item->name }}" width="100%">
                                            @else
                                                <img src="{{ url('/') }}/uploads/images/default.png" alt="{{ $item->name }}" width="100%">
                                            @endif
                                        </div>
                                    </a>
                                    <div class="content">
                                        <h3 class="title">
                                            {{ $item->name }}
                                        </h3>
                                        <ul class="category">
                                            <li class="category-list">
                                                <a href="#" class="category-list__links">
                                                    {{ sizeof($item->tour) }}
                                                    <span class="span_name">
                                                        Tours
                                                    </span>
                                                </a>
                                            </li>
                                            <li class="category-list">
                                                <a href="#" class="category-list__links">
                                                    {{ sizeof($item->hotel) }}
                                                    <span class="span_name">
                                                        Hotels
                                                    </span>

                                                </a>
                                            </li>
                                            <li class="category-list">
                                                <a href="#" class="category-list__links">
                                                    {{ sizeof($item->car) }}
                                                    <span class="span_name">
                                                        Cars
                                                    </span>
                                                </a>
                                            </li>

                                        </ul>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
    <section class="section-promotion">
        <div class="bs-container">
            <div class="bs-row">
                <div class="bs-col">
                    <div class="module module-promotion">
                        <div class="module-header">
                            <h2 class="title">
                                Tour du lịch khuyến mãi
                            </h2>
                            <p class="p__info">
                                Được lựa chọn cẩn thận bởi các chuyên gia du lịch của chúng tôi<br />
                                cho các quyết định trong kỳ nghỉ của bạn
                            </p>
                        </div>
                        <div class="module-content">
                            <div class="bs-row row-lg-15 row-md-10 row-sm-10 row-xs-5">
                                @foreach ($tourFeatured as $item)
                                    <div class="bs-col lg-33-15 md-33-10 sm-50-10 xs-50-5">
                                        <div class="promotion">
                                            <div class="avata">
                                                <a href="{{ route('public.detailTour', [$item->slug , $item->id]) }}" class="avata__links" style="width:100%;" >
                                                    <img src="{{url('/')}}{{ $item->thumnail }}" alt="{{ $item->title }}"
                                                        class="img__links" width="100%">
                                                </a>
                                                <span class="span__sale">
                                                    <span class="number">
                                                        @if($item->sale !== null && $item->sale < $item->price)
                                                            {{ round((($item->price - $item->sale)/$item->price)*100) }} %
                                                        @endif
                                                    </span>
                                                </span>
                                                <a class="span_love" onclick="wishListTourFeature({{ $item->id }})">
                                                <i class="fas fa-heart {{ Auth::check() ? ($item->wishlistTourBy(auth()->user()) ? 'is-favourite' : ' ') : ' ' }}" {{ Auth::check() == false ? ' modal-show=show modal-data=#login ' : ' ' }} id="feature-tour-color{{ $item->id }}"></i>
                                                <span class="favourite {{ Auth::check() ? ($item->wishlistTourBy(auth()->user()) ? 'is-favourite-color' : ' '): ' ' }}" {{ Auth::check() == false ? ' modal-show=show modal-data=#login ' : ' ' }} id="feature-tour-{{ $item->id }}">{{ sizeOf($item->wishListTour) }}</span>
                                            </a>
                                            </div>
                                            <div class="content">
                                                <h2 class="title">
                                                    <a href="{{ route('public.detailTour', [$item->slug , $item->id]) }}" class="title__links">
                                                        {{ $item->title }}
                                                    </a>
                                                </h2>
                                                <p class="p__price">
                                                    Giá:
                                                    @if($item->sale !== null && $item->sale < $item->price)
                                                        <span class="span__price bold">
                                                            {{ number_format($item->sale, 0, ",", ".") }} đ
                                                        </span>
                                                        <span class="span__sale">
                                                            {{ number_format($item->price, 0, ",", ".") }} đ
                                                        </span>
                                                    @else
                                                        <span class="span__price bold">
                                                            {{ number_format($item->price, 0, ",", ".") }} đ
                                                        </span>
                                                    @endif

                                                </p>
                                                <div class="content-footer">
                                                    <div class="text">
                                                        <p class="p__time">
                                                            <img src="{{url('/pms/images/icon_time.png')}}" alt="icon_time.png"
                                                                class="icon">
                                                            <span class="span__name medium">
                                                                Lịch trình:
                                                            </span>
                                                            {{ $item->duration }} ngày
                                                        </p>
                                                        <p class="p__like">
                                                            @for ($j = 0; $j < $item->rating_star; $j++)
                                                            <i class="fas fa-star"></i>
                                                            @endfor
                                                        </p>
                                                    </div>
                                                    <div class="text">
                                                        <p class="p__time">
                                                            <img src="{{url('/pms/images/icon_check.png')}}" alt="icon_check.png"
                                                                class="icon">
                                                            <span class="span__name">
                                                                Ngày đi:
                                                            </span>
                                                            @if(!$item->startDateTour->isEmpty())
                                                                {{ $item->startDateTour['0']['start_date'] }}
                                                            @endif
                                                        </p>
                                                        <p class="p__like">
                                                            {{ sizeof($item->reviewTour) }} đánh giá
                                                        </p>
                                                    </div>
                                                    <div class="text">
                                                        <p class="p__time">
                                                            <img src="{{url('/pms/images/icon_map.png')}}" alt="icon_map.png" class="icon">
                                                            <span class="span__name">
                                                                Khởi hành: </span>
                                                                {{ optional($item->destination)->name }}
                                                        </p>
                                                    </div>

                                                </div>
                                                <div class="countdown">
                                                    Thời gian còn lại:
                                                    <span id="clock" class="clock"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="section-carlist">
        <div class="bs-container">
            <div class="bs-row">
                <div class="bs-col">
                    <div class="module module-carlist">
                        <div class="module-header">
                            <h2 class="title">
                                Danh sách xe
                            </h2>
                        </div>
                        <div class="module-content">
                            <div class="control">
                                <ul class="control-list control-list-flex">
                                        <li class="control-list__item active">
                                            <a href="{{ route('public.listCar') }}" class="control-list__links">
                                                Tất cả
                                            </a>
                                        </li>
                                    <form action="{{ route('public.listCar') }}" method="get" id="form-filter-car">
                                        @foreach ($carBrannd as $item)
                                            <li class="control-list__item">
                                                <a data-id="{{ $item->id }}" href="javascript:void(0)" class="control-list__links item-links-filter-car">
                                                    {{ $item->name }}
                                                </a>
                                            </li>
                                            <input type="checkbox" name="carBrand[]" value="{{ $item->id }}" id="carBrand-{{ $item->id }}" class="d-none">
                                        @endforeach
                                    </form>
                                    <li class="control-list__item">
                                        <a href="#" class="control-list__links">
                                            <i class="fas fa-bars"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <div class="bs-row row-lg-15 row-md-10 row-sm-5 row-xs-5">
                                @foreach ($carFeatured as $item)
                                    <div class="bs-col lg-25-15 md-25-10 sm-33-5 xs-50-5 tn-100">
                                        <div class="carlist">
                                            <div class="avata">
                                                <a href="{{ route('public.detailCar', [$item->slug, $item->id]) }}" class="avata__links">
                                                    <img src="{{url('/')}}{{ $item->thumnail }}" alt="{{ $item->title }}" class="img_links">
                                                    <div class="category">
                                                        <ul class="item">
                                                            <li class="item-list">
                                                                <b>{{ $item->passenger }}</b> chỗ
                                                            </li>
                                                            <li class="item-list">
                                                                <b>{{ $item->door }}</b> cửa
                                                            </li>
                                                            <li class="item-list">
                                                                {{ $item->gear_shift }}
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </a>
                                                <a class="span_love" onclick="Wishlistcar({{ $item->id }})">
                                                    <i class="fas fa-heart {{ Auth::check() ? ($item->wishlistCarBy(auth()->user()) ? 'is-favourite' : ' '): ' ' }}" {{ Auth::check() == false ? ' modal-show=show modal-data=#login ' : ' ' }} id="feature-car-color{{ $item->id }}"></i>
                                                    <span class="favourite {{ Auth::check() ? ($item->wishlistCarBy(auth()->user()) ? 'is-favourite-color' : ' '): ' ' }}" {{ Auth::check() == false ? ' modal-show=show modal-data=#login ' : ' ' }} id="feature-car-{{ $item->id }}">{{ sizeOf($item->wishlistCar) }}</span>
                                                </a>
                                            </div>
                                            <div class="content">
                                                <h3 class="title">
                                                    <a href="{{ route('public.detailCar', [$item->slug, $item->id]) }}" class="title__links">
                                                        {{ $item->title }}
                                                    </a>
                                                </h3>
                                                <p class="p__price">
                                                    <span class="span__price">
                                                        @if($item->sale !== null && $item->sale < $item->price)
                                                            {{ number_format($item->sale, 0, ",", ".") }} đ
                                                        @else
                                                            {{ number_format($item->price, 0, ",", ".") }} đ
                                                        @endif
                                                    </span>
                                                    / ngày
                                                </p>
                                                <p class="p__map">
                                                    <img src="{{url('/pms/images/icon_map.png')}}" alt="icon_map.png">
                                                    {{ optional($item->location)->name }}
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="section-review">
        <div class="bs-container">
            <div class="bs-row">
                <div class="bs-col">
                    <div class="module module-review">
                        <div class="module-header">
                            <h2 class="title">
                                Ý kiến Khách hàng
                            </h2>
                        </div>
                        <div class="module-content">
                            <div class="slide-review">
                                @foreach ($customerReview as $item)
                                    <div class="item">
                                        <div class="review">
                                            <div class="review-header">
                                                <div class="avata">
                                                <img class="img__links" src="{{url('/')}}{{ $item->avatar }}" alt="{{ $item->name }}">

                                                </div>
                                                <div class="content">
                                                    <h3 class="title medium">
                                                        {{ $item->name }}
                                                    </h3>
                                                    <ul class="category">
                                                        <li class="category-list">
                                                            <a href="#" class="category-linst__links">
                                                                {{ optional($item->location)->name }}
                                                            </a>
                                                        </li>
                                                    </ul>
                                                    <ul class="item-ul">
                                                        <li class="item-list">
                                                            <a href="#" class="item-list__links">
                                                                <i class="fab fa-facebook-f"></i>
                                                            </a>
                                                        </li>
                                                        <li class="item-list">
                                                            <a href="#" class="item-list__links">
                                                                <i class="fab fa-twitter"></i>
                                                            </a>
                                                        </li>
                                                        <li class="item-list">
                                                            <a href="#" class="item-list__links">
                                                                <i class="fab fa-linkedin-in"></i>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="review-content">
                                                <div class="p__text">
                                                    {!! $item->content !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="section-new">
        <div class="bs-container">
            <div class="bs-row">
                <div class="bs-col">
                    <div class="module module-new">
                        <div class="module-header">
                            <h2 class="title">
                                Tin mới nhất
                            </h2>
                            <p class="p__info">
                                Được lựa chọn cẩn thận bởi các chuyên gia du lịch của chúng tôi<br />
                                cho các quyết định trong kỳ nghỉ của bạn
                            </p>
                        </div>
                        <div class="module-content">
                            <div class="bs-row row-lg-15 row-md-10 row-sm-5 row-xs-5">
                                {{-- @php
                                    dd($postFeatured);
                                @endphp --}}
                                @foreach ($postFeatured as $item)
                                    <div class="bs-col lg-33-15 md-33-10 sm-33-5 xs-50-5">
                                        <div class="new">
                                            <div class="avata-new">
                                                <a href="#" class="new__links" style="width: 100%;">
                                                    <img src="{{url('/')}}{{ $item->image }}" alt="{{ $item->title }}"  class="img__links">
                                                    <p class="p__date">
                                                        <span class="span__day">
                                                            {{ $item->created_at->format('d') }}
                                                        </span>
                                                        <span class="span__month">
                                                            Tháng {{ $item->created_at->format('m') }}
                                                        </span>
                                                    </p>
                                                </a>
                                            </div>
                                            <div class="content">
                                                <h3 class="title">
                                                    <a href="#" class="title__links medium">
                                                        {{ $item->title }}
                                                    </a>
                                                </h3>
                                            </div>
                                        </div>

                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="section-newletter">
        <div class="bs-row">
            <div class="bs-col">
                <div class="module module-newletter">
                    <div class="module-content">
                        <div class="bs-row">
                            <div class="bs-col lg-50 md-50 sm-50 xs-100">
                                <div class="newletter">
                                    <div class="newletter-container">
                                        <div class="newletter-header">
                                            <div class="newletter-icon">
                                                <img src="{{url('/pms/images/icon__menu_2.png')}}" alt="icon__menu_2.png">
                                            </div>
                                            <div class="newletter-text">
                                                <h3 class="title bold">
                                                    Newletter
                                                </h3>
                                                <p class="p__info medium">
                                                    Đăng ký nhận tin để lên kế
                                                    hoạch cho kỳ nghỉ tới ngay từ bây giờ
                                                </p>
                                            </div>
                                        </div>
                                        {{-- <form action="" method="POST" role="form"> --}}
                                            <div class="form-group">
                                                <input type="email" class="form-control input__text" id="email-newletter" name=email
                                                    placeholder="Nhập email của bạn" required>
                                                <button type="submit" class="btn button__text bold submit-newletter">Gửi
                                                    ngay</button>
                                            </div>
                                        {{-- </form> --}}

                                    </div>
                                </div>
                            </div>
                            <div class="bs-col lg-50 md-50 sm-50 xs-100">
                                <div class="shar">
                                    <div class="shear-container">
                                        <div class="shar-header">
                                            <h3 class="title bold">
                                                Chia sẻ kinh nghiệm du lịch
                                            </h3>
                                            <p class="p__info">
                                                Lời khuyên từ các chuyên gia du lịch của chúng tôi để làm cho chuyến đi
                                                tiếp theo của bạn thậm chí tốt hơn.
                                            </p>
                                        </div>
                                        <div class="shar-content">
                                            <a href="#" class="shar__links medium">
                                                Tham khảo ngay
                                            </a>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>
</main>
@endsection
@push('js')
<script type="text/javascript">
    function slideHome() {
        $('.slide-home').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            infinite: true,
            adaptiveHeight: true,
            dots: true,
            arrows: false,
            autoplay: true
        });
    }



    function slideVacation() {
        $(".slide-vacation").slick({
            slidesToShow: 3,
            slidesToScroll: 2,
            infinite: true,
            prevArrow: '<i class="icon_control fas fa-angle-left"></i>',
            nextArrow: '<i class="icon_control fas fa-angle-right"></i>',
            arrows: true,
            responsive: [{

                breakpoint: 991.9,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 2,

                    infinite: false,
                }
            },


            {
                breakpoint: 767.9,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
            },

            {
                breakpoint: 479.9,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    infinite: false,
                }
            }
            ]
        });
    }
    function slideHot() {
        $(".slide-hot").slick({
            slidesToShow: 4,
            slidesToScroll: 3,
            infinite: true,

            prevArrow: '<i class="icon_control fas fa-angle-left"></i>',
            nextArrow: '<i class="icon_control fas fa-angle-right"></i>',
            arrows: true,
            responsive: [{

                breakpoint: 991.9,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1,
                }
            },
            {
                breakpoint: 479,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    infinite: false,
                    adaptiveHeight: true,
                }
            }
            ]
        });
    }

    function WishlisthotelHoliday(id) {
            $.ajax({
                type: "GET",
                url: "/hotel-favourite/"+id ,
                success: function (response) {
                    $('#holiday-hotel-'+id).html(response.countFavourite).css('color',response.color);
                    $('#holiday-hotel-color'+id).css('color',response.color);
            }
        });
    }
    function WishlisthotelFeature(id) {
            $.ajax({
                type: "GET",
                url: "/hotel-favourite/"+id ,
                success: function (response) {
                    $('#feature-hotel-'+id).html(response.countFavourite).css('color',response.color);
                    $('#feature-hotel-color'+id).css('color',response.color);
            }
        });
    }
    function Wishlistcar(id) {
            $.ajax({
                type: "GET",
                url: "/car-favourite/"+id ,
                success: function (response) {
                    $('#feature-car-'+id).html(response.countFavourite).css('color',response.color);
                    $('#feature-car-color'+id).css('color',response.color);
                }
            });
        }
    function WishlistcarHoliday(id) {
            $.ajax({
                type: "GET",
                url: "/car-favourite/"+id ,
                success: function (response) {
                    $('#car-holiday-'+id).html(response.countFavourite).css('color',response.color);
                    $('#car-holiday-color'+id).css('color',response.color);
                }
            });
        }
    function wishListTourHoliday(id) {
        $.ajax({
            type: "GET",
            url: "/tour-favorite/"+id ,
            success: function (response) {
                $('#holiday-tour-'+id).html(response.countFavourite).css('color',response.color);
                $('#holiday-tour-color'+id).css('color',response.color);
        }
    });
    }
    function wishListTourFeature(id) {
        $.ajax({
            type: "GET",
            url: "/tour-favorite/"+id ,
            success: function (response) {
                $('#feature-tour-'+id).html(response.countFavourite).css('color',response.color);
                $('#feature-tour-color'+id).css('color',response.color);
        }
    });
    }
    function countdown() {
        $('.clock').countdown('2020/10/03', function (event) {
            $(this).html(event.strftime(' <span> %H</span> Giờ <span> :%M </span> Phút <span>:%S </span> Giây'));
        });
    }
    function slideReview() {
        $('.slide-review').slick({
            slidesToShow: 3,
            slidesToScroll: 2,
            prevArrow: '<i class="icon_control fas fa-chevron-left"></i>',
            nextArrow: '<i class="icon_control fas fa-chevron-right"></i>',
            adaptiveHeight: true,
            responsive: [
                {
                    breakpoint: 991.9,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1,
                        infinite: false
                    }
                },
                {
                    breakpoint: 479.9,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        infinite: false
                    }
                }
            ]

        });
    }
    countdown();
    slideHome();
    slideHot();
    slideVacation();
    slideReview();

    $(document).on('click', '.item-links-filter-car', function(e) {
        e.preventDefault();
        var id = $(this).attr('data-id');
        $('#termType-'+id).prop('checked', true);
        $('#form-filter-car').submit();
    })

    $(document).on('click', '.item-links-filter-car', function(e) {
        e.preventDefault();
        var id = $(this).attr('data-id');
        $('#carBrand-'+id).prop('checked', true);
        $('#form-filter-car').submit();
    })

    $(document).on('click', '.submit-newletter', function(e) {
        e.preventDefault();
        toastr.clear();
        toastr.options = {
            "closeButton": true,
            "timeOut": "5000",
            "positionClass": "toast-bottom-left"
        }
        const __this = this;
        $(__this).prop('disabled', true);
        const __token = $('meta[name="csrf-token"]').attr('content');
        var email = $('#email-newletter').val();
        if(email == ''){
            toastr.warning('Email không được để trống');
            $(__this).prop('disabled', false);
            return false;
        }
        data_ = {
            email: email,
            _token: __token
        }

        var request = $.ajax({
            url: '{{route('public.add_newletter')}}',
            type: 'POST',
            data: data_,
            dataType: "json"
        });
        request.done(function (msg) {
            if(msg.type == 1) {
                toastr.success(msg.mess);
                $('#email-newletter').val('');
                $(__this).prop('disabled', false);
            }
            return false;
        });
        request.fail(function (errors) {
            var is_error =Object.values(errors.responseJSON.errors) ;
            toastr.error(is_error[0][0]);
            $(__this).prop('disabled', false);
            return false;
        });
    })

</script>
<script>
    $(document).on('click', '.option-start-destination', function(e) {
        let text = $(this).text().trim();
        let id = $(this).attr('data-id');
        $(".add-text-destination").val(text);
        $(".start_destination").val(id);
        $(".item-grid__content").removeClass("active");
    });
    $(document).on('click', '.btn-apply-number-people', function(e) {
        e.preventDefault();
        var number_adult = parseInt($('.number_adult').html());
        var number_children = parseInt($('.number_children').html());
        var text = number_adult + ' Người lớn - '+ number_children +' Trẻ em';
        var total = number_adult + number_children;
        $('.text-number_people').val(text);
        $('.number_people').val(total);
        $(".item-grid__content").removeClass("active");
    });
    $(document).on('click', '.item-list-location_car', function(e) {
        let text = $(this).text().trim();
        let id = $(this).attr('data-id');
        $("#text-location_car").val(text);
        $("#location_id_car").val(id);
        $(".item-grid__content").removeClass("active");
    });
    $(document).on('click', '.item-list-car-form', function(e) {
        let text = $(this).text().trim();
        let id = $(this).attr('data-id');
        $(".text-car-form").val(text);
        $(".car_form").val(id);
        $(".item-grid__content").removeClass("active");
    });
    $(document).on('click', '.item-option-location', function() {
        let text = $(this).text().trim();
        let id = $(this).attr('data-id');
        $(".text-location_hotel").val(text);
        $("#location_id_hotel").val(id);
        $(".item-grid__content").removeClass("active");
    })
    $(document).on('click', '.item-list-location-space', function(e) {
        let text = $(this).text().trim();
        let id = $(this).attr('data-id');
        $("#text-location_id-place").val(text);
        $("#location_id-space").val(id);
        $(".item-grid__content").removeClass("active");
    });

    $(document).on('click','.span__add_space',function(){
        var t = $(this).prev(".span__show_space").text().trim();
        Number(t++), $(this).prev(".span__show_space").text(t);
        var text = 'Số lượng khách: ' + t;
        $(".text-space-form").val(text);
        $('.number_guests').val(t);
    });

    $(document).on('click','.span__subtract_space',function(){
        var t = $(this).next(".span__show_space").text().trim();
        t = Number(--t);
        if(t == 0 ){
            t = 1;
        }
        $(this).next(".span__show_space").text(t);
        var text = 'Số lượng khách: ' + t;
        $(".text-space-form").val(text);
        $('.number_guests').val(t);
    });
</script>
@endpush
