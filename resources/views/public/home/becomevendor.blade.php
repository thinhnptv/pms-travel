@extends('public.master')
@section('content')

<main id="main">
    <section class="section-become">
        <div class="bs-container">
            <div class="module module-become">
                <div class="bs-row row-xs-5 row-sm-10 row-md-15 row-lg-15">
                    <div class="bs-col xs-100-5 sm-50-10 md-40-15 lg-40-15">
                        <div class="module-header">
                            <h2 class="title bold">
                                Become a vendor
                            </h2>
                            <p class="p__info">
                                Join our community to unlock your greatest asset and welcome paying guests into your home.
                            </p>
                        </div>
                        <div class="module-content">
                        <form class="form-become" action="{{ route('becomevendor') }}" method="POST" role="form">
                                @csrf
                                <div class="form-group">
                                    <input type="text" class="form-control input__control" name="firstname" placeholder="First Name" >
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control input__control" name="lastname" placeholder="Last Name" >
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control input__control" name="businessname" placeholder="Business Name">
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control input__control" name="phone" placeholder="Phone" >
                                </div>
                                <div class="form-group">
                                    <input type="email" class="form-control input__control" name="email" placeholder="Email" >
                                </div>
                                <div class="form-group">
                                    <input type="password" class="form-control input__control" name="password" placeholder="Password" >
                                </div>
                                <div class="form-group">
                                    <input class="custome__checkbox" id="term" type="checkbox" name="checkbox" >
                                    <label for="term" class="">
                                        I have read and accept the <a href="" target="_blank">Terms and Privacy Policy</a>
                                    </label>
                                </div>
                                <div class="form-group">
                                <button type="submit" class="btn btn-primary btn__submit">Sign Up</button>
                                </div>
                                @include('public.errors')
                            </form>
                        </div>
                    </div>
                    <div class="bs-col xs-100-5 sm-50-10 md-50-15 lg-60-15">
                        <div class="module-content">
                            <div class="video">
                                <div class="bg-video" data-toggle="modal" data-target="#myModal">
                                    <img class="img__video" src="{{url('/pms/images/bg-video.gif')}}" alt="bg-video.gif">
                                    <i class="fas fa-play"></i>
                                </div>
                                <!-- modal video -->
                                <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">
                                            <div class="modal-body">
                                                <iframe width="100%" height="500" src="https://www.youtube.com/embed/AmZ0WrEaf34" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </section>
    <section class="section-work">
        <div class="bs-container">
            <div class="bs-row">
                <div class="bs-col">
                    <div class="module module-work">
                        <div class="module-header">
                            <h2 class="title bold">
                                How does it work?
                            </h2>
                        </div>
                        <div class="module-content">
                            <div class="bs-row row-xs-5 row-sm-10 row-md-15 row-lg-15 ">
                                <div class="bs-col xs-50-5 sm-33-10 md-33-15 lg-33-15">
                                    <div class="item">
                                        <div class="number">
                                            1
                                        </div>
                                        <div class="content">
                                            <h3 class="item__title bold">
                                                Sign up
                                            </h3>
                                            <p class="p__desc">
                                                Click edit button to change this text to change this text
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="bs-col xs-50-5 sm-33-10 md-33-15 lg-33-15">
                                    <div class="item">
                                        <div class="number">
                                            2
                                        </div>
                                        <div class="content">
                                            <h3 class="item__title bold">
                                                Add your services
                                            </h3>
                                            <p class="p__desc">
                                                Click edit button to change this text to change this text
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="bs-col xs-50-5 sm-33-10 md-33-15 lg-33-15">
                                    <div class="item">
                                        <div class="number">
                                            3
                                        </div>
                                        <div class="content">
                                            <h3 class="item__title bold">
                                                Get bookings
                                            </h3>
                                            <p class="p__desc">
                                                Click edit button to change this text to change this text
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="bs-col col-xs-100">
                                    <div class="video">
                                        <div class="bg-img" data-toggle="modal" data-target="#myModal2">
                                            <img src="{{url('/pms/images/Bg-video.gif')}}" alt="Bg-video.gif">
                                            <i class="fas fa-play"></i>
                                        </div>
                                        <!-- modal video -->
                                        <div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                            <div class="modal-dialog modal-lg">
                                                <div class="modal-content">
                                                    <div class="modal-body">
                                                        <iframe width="100%" height="500" src="https://www.youtube.com/embed/hHUbLv4ThOo" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="section-expert">
        <div class="bs-container">
            <div class="bs-row">
                <div class="bs-col">
                    <div class="module-header">
                        <h2 class="title bold">
                            Why be a Local Expert
                        </h2>
                    </div>
                </div>
            </div>
            <div class="module-content">
                <div class=" bs-row row-xs-5 row-sm-10 row-md-15 row-lg-15">
                    <div class="bs-col xs-100-5 sm-33-10 md-33-15 lg-33-15">
                        <div class="item">
                            <div class="icon">
                                <img src="{{url('/pms/images/ico_piggy-bank_1.svg')}}" alt="ico_piggy-bank_1.svg">
                            </div>
                            <div class="content">
                                <h3 class="title bold">
                                    Earn an additional income
                                </h3>
                                <p class="p__text">
                                    Ut elit elit, luctus nec ullamcorper mattis
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="bs-col xs-100-5 sm-33-10 md-33-15 lg-33-15">
                        <div class="item">
                            <div class="icon">
                                <img src="{{url('/pms/images/ico_friendship_1.svg')}}" alt="ico_friendship_1.svg">
                            </div>
                            <div class="content">
                                <h3 class="title bold">
                                    Open your network
                                </h3>
                                <p class="p__text">
                                    Ut elit elit, luctus nec ullamcorper mattis
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="bs-col xs-100-5 sm-33-10 md-33-15 lg-33-15">
                        <div class="item">
                            <div class="icon">
                                <img src="{{url('/pms/images/ico_chat_1.svg')}}" alt="ico_chat_1.svg">
                            </div>
                            <div class="content">
                                <h3 class="title bold">
                                    Practice your language
                                </h3>
                                <p class="p__text">
                                    Ut elit elit, luctus nec ullamcorper mattis
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="section-faqs">
        <div class="bs-container">
            <div class="bs-row">
                <div class="bs-col">
                    <div class="module module-faqs">
                        <div class="module-header">
                            <h2 class="title bold">
                                FAQs
                            </h2>
                        </div>
                        <div class="module-content">
                            <div class="bs-row row-xs-5 row-sm-10 row-md-15 row-lg-15">
                                <div class="bs-col col-xs-100 sm-50-10 md-50-15 lg-50-15">
                                    <div class="faqs">
                                        <h3 class="faqs-title">
                                            <img src="{{url('/pms/images/ico_quest.png')}}" alt="ico_quest.png">
                                            <span class="span__title bold">
                                                How will I receive my payment?
                                            </span>
                                        </h3>
                                        <div class="content">
                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit elit, luctus nec ullamcorper mattis, dapibus leo pulvinar.
                                        </div>
                                    </div>
                                </div>
                                <div class="bs-col col-xs-100 sm-50-10 md-50-15 lg-50-15">
                                    <div class="faqs">
                                        <h3 class="faqs-title">
                                            <img src="{{url('/pms/images/ico_quest.png')}}" alt="ico_quest.png">
                                            <span class="span__title bold">
                                                How do I upload products?
                                            </span>
                                        </h3>
                                        <div class="content">
                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit elit, luctus nec ullamcorper mattis, dapibus leo pulvinar.
                                        </div>
                                    </div>
                                </div>
                                <div class="bs-col col-xs-100 sm-50-10 md-50-15 lg-50-15">
                                    <div class="faqs">
                                        <h3 class="faqs-title">
                                            <img src="{{url('/pms/images/ico_quest.png')}}" alt="ico_quest.png">
                                            <span class="span__title bold">
                                                How do I update or extend my availabilities?
                                            </span>
                                        </h3>
                                        <div class="content">
                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit elit, luctus nec ullamcorper mattis, dapibus leo pulvinar.
                                        </div>
                                    </div>
                                </div>
                                <div class="bs-col col-xs-100 sm-50-10 md-50-15 lg-50-15">
                                    <div class="faqs">
                                        <h3 class="faqs-title">
                                            <img src="{{url('/pms/images/ico_quest.png')}}" alt="ico_quest.png">
                                            <span class="span__title bold">
                                                How do I increase conversion rate?
                                            </span>
                                        </h3>
                                        <div class="content">
                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit elit, luctus nec ullamcorper mattis, dapibus leo pulvinar.
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="section-updatesMore">
        <div class="bs-container">
            <div class="module module-updatesMore">
                <div class="bs-row row-xs-5 row-sm-10 row-md-15 row-lg-15">
                    <div class="bs-col xs-100-5 sm-50-15 md-50 lg-40-15">
                        <div class="module-header">
                            <img class="icon__svg" src="{{url('/pms/images/island-alt.svg')}}" alt="island-alt.svg">
                            <div class="text-header">
                                <h2 class="title bold">
                                    Get Updates & More
                                </h2>
                                <p class="p__info">
                                    Thoughtful thoughts to your inbox
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="bs-col xs-100-5 sm-50-10 md-50-15 lg-50-15">
                        <div class="module-content">
                            <form class="form" action="" method="POST" role="form">
                                <div class="form-group">
                                    <input type="email" class="form-control" placeholder="Email">
                                    <button type="submit" class="btn-submit">
                                        Subscribe
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
@endsection