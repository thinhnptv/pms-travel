@extends('public.master')
@section('content')
<main id="main">
    <section class="section-list__new">
        <div class="bs-container">
            <div class="bs-row">
                <div class="bs-col">
                    <div class="module module-list__new">
                        <div class="module-content">
                            <div class="bs-row row-lg-15">
                                <div class="bs-col lg-100-15">
                                    <ul class="page">
                                        <li class="page-list">
                                            <a href="new-detail.html" class="page-list__link">
                                                Trang chủ
                                            </a>
                                        </li>
                                        <li class="page-list">
                                            <span href="#" class="page-list__link">
                                                Giới thiệu về Traveling
                                                </span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="section-about">
        <div class="bs-container">
            <div class="bs-row">
                <div class="bs-col">
                    <div class="module module-about">
                        <div class="module-header">
                            <div class="banner_about">
                               @if (!empty( $pageAbout))
                                <img src="{{ $pageAbout->avatar }}" alt="">
                               @endif
                            </div>
                        </div>
                        <div class="module-content">
                            @if (!empty( $pageAbout))
                                {!! $pageAbout->content !!}
                            @endif
                           
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>

@endsection
@push('js')
    <script type="text/javascript">
     
    </script>
    
@endpush