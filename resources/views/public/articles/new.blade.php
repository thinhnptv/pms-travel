@extends('public.master')
@section('css')
@endsection
@section('content')
<!DOCTYPE html>
<html lang="en">

<div class="bs-modal" id="singUp">
    <div class="modal-frame">
        <div class="content-modal">
            <div class="header-modal">
                <h3 class="modal__title">Đăng ký</h3>
                <span title="close" modal-show="close" class="close__modal"><img src="images/ico_close.svg" alt=""></span>
            </div>
            <div class="body-modal">
                <form action="" method="POST" role="form">
                    <form class="form" method="post">
                        <div class="row">
                            <div class="col-lg-6 col-md-12">
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Tên đầu tiên">
                                    <i class="input-icon field-icon fa">
                                        <img src="images/ico_fullname_signup.svg">
                                    </i>

                                </div>
                            </div>
                            <div class="col-lg-6 col-md-12">
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Họ">
                                    <i class="input-icon"><img src="/images/ico_fullname_signup.svg">
                                    </i>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <input type="email" class="form-control" placeholder="Địa chỉ email">
                            <i class="input-icon">
                                <img src="images/ico_email_login_form.svg">
                            </i>

                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control" name="password" placeholder="Mật khẩu">
                            <i class="input-icon">
                                <img src="images/ico_pass_login_form.svg">
                            </i>
                        </div>
                        <div class="form-group">
                            <label class="ferm_checkbox">
                                <input type="checkbox">
                                Tôi đã đọc và chấp nhận <span class="span__name">Điều khoản và Chính sách bảo mật</span>

                            </label>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary form-submit">
                                Đăng ký
                            </button>
                        </div>
                        <div class="advanced">
                            <p class="text-center">
                                hoặc tiếp tục với
                            </p>
                            <div class="row">
                                <div class="col-xs-12 text-center">
                                    <a href="#" class="btn btn-facebok">
                                        <i class="fab fa-facebook-f"></i>
                                        Facebook
                                    </a>
                                    <a href="#" class="btn btn-google">
                                        <i class="fab fa-google-plus-g "></i>
                                        Google
                                    </a>
                                </div>
                            </div>
                        </div>
                    </form>
                </form>

            </div>
        </div>
    </div>
</div>
<div class="bs-modal" id="login">
    <div class="modal-frame">
        <div class="content-modal">
            <div class="header-modal">
                <h3 class="modal__title">Đăng nhập</h3>
                <span title="close" modal-show="close" class="close__modal"><img src="images/ico_close.svg" alt=""></span>
            </div>
            <div class="body-modal">
                <form action="" method="POST" role="form">
                    <form class="form" method="post">
                        <div class="form-group">
                            <input type="email" class="form-control" placeholder="Địa chỉ email">
                            <i class="input-icon">
                                <img src="images/ico_email_login_form.svg">
                            </i>

                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control" name="password" placeholder="Mật khẩu">
                            <i class="input-icon">
                                <i class="fas fa-lock"></i>
                            </i>
                        </div>
                        <div class="form-group">
                            <label class="ferm_checkbox">
                                <input type="checkbox">
                                Lưu mật khẩu <a href="#" class="span__name">Quyên mật khẩu</a>

                            </label>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary form-submit">
                                Đăng nhập
                            </button>
                        </div>
                        <div class="advanced">
                            <p class="text-center">
                                hoặc tiếp tục với
                            </p>
                            <div class="row">
                                <div class="col-xs-12 text-center">
                                    <a href="#" class="btn btn-facebok">
                                        <i class="fab fa-facebook-f"></i>
                                        Facebook
                                    </a>
                                    <a href="#" class="btn btn-google">
                                        <i class="fab fa-google-plus-g "></i>
                                        Google
                                    </a>
                                </div>
                            </div>
                        </div>
                    </form>
                </form>

            </div>
        </div>
    </div>
</div>

<main id="main">
    <section class="section-list__new">
        <div class="bs-container">
            <div class="bs-row">
                <div class="bs-col">
                    <div class="module module-list__new">
                        <div class="module-content">
                            <div class="bs-row row-lg-15">
                                <div class="bs-col lg-100-15">
                                    <ul class="page">
                                        <li class="page-list">
                                            <a href="{{ route('post.home') }}" class="page-list__link">
                                                Trang chủ
                                            </a>
                                        </li>
                                        <li class="page-list">
                                            <a href="{{ route('post') }}" class="page-list__link">
                                                Tin tức
                                                </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="bs-row row-lg-15 row-md-10 row-ms row-xs">
                                <div class="bs-col lg-75-15 md-75-10 ms-100 xs-100">
                                    @if(isset($articles))
                                    @foreach($articles as $key => $val)
                                    <div class="new">
                                        <div class="avata">
                                            <a href="{{ route('post.detail', [$val->slug, $val->id]) }}" class="avata__link"></a>
                                            <div class="ImagesFrame">
                                                <div class="ImagesFrameCrop0">
                                                    <img src="{{ $val->image }}" alt="">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="content">
                                            <h3 class="title medium">
                                                <a href="{{ route('post.detail', [$val->slug, $val->id]) }}" class="title__link">
                                                    {{ $val->title }}
                                                </a>
                                            </h3>
                                            <div class="category">
                                                <span class="sapn__sign">
                                                    <i class="fas fa-dollar-sign"></i>
                                                </span>
                                                by {{ optional($val->articleUser)->full_name }} - Ngày đăng {{ $val->updated_at->format('d-m-Y') }}
                                            </div>
                                            <a href="{{ route('post.detail', [$val->slug, $val->id]) }}" class="view__link black">
                                                Xem chi tiết
                                            </a>
                                        </div>
                                    </div>
                                    @endforeach
                                    @endif
                                    <div style="text-align: center;">
                                        {{ $articles->links('vendor.pagination.bootstrap-4') }}
                                    </div>
                                </div>
                                <div class="bs-col lg-25-15 md-25-10 ms-100 xs-100">
                                    <div class="sidebar">
                                        <div class="content">
                                            <!-- Another variation with a button -->
                                            <form action="{{ route('post.search') }}" method="get" role="search" class="input-group">
                                                <input type="text" name="search" class="form-control" placeholder="Search">
                                                <div class="input-group-append">
                                                    <button style="position: absolute;" class="btn btn-secondary" type="submit">
                                                        <i class="fa fa-search"></i>
                                                    </button>
                                                </div>
                                            </form>
                                        </div>
                                        <h1 class="title medium">
                                            tin tức mới nhất
                                        </h1>
                                        <div class="content">
                                            @if(isset($newArticle))
                                            @foreach($newArticle as $key => $val)
                                            <div class="list-new">
                                                <div class="avata">
                                                    <a href="{{ route('post.detail', [$val->slug, $val->id]) }}" class="item__link"></a>
                                                    <div class='ImagesFrame'>
                                                        <div class='ImagesFrameCrop0'>
                                                            <img src='{{ $val->image }}' alt='new_1.jpg'>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="content-text">
                                                    <h3 class="title__text medium">
                                                        <a href="{{ route('post.detail', [$val->slug, $val->id]) }}" class="title__link">
                                                            {{ $val->title }}
                                                        </a>
                                                    </h3>
                                                </div>
                                            </div>
                                            @endforeach
                                            @endif
                                        </div>
                                        <h1 class="title medium">
                                            Tin tức ngẫu nhiên
                                        </h1>
                                        <div class="content">
                                            @if(isset($highlightArticle))
                                                @foreach($highlightArticle as $key => $val)
                                                <div class="list-new">
                                                    <div class="avata">
                                                        <a href="{{ route('post.detail', [$val->slug, $val->id]) }}" class="item__link"></a>
                                                        <div class='ImagesFrame'>
                                                            <div class='ImagesFrameCrop0'>
                                                                <img src='{{ $val->image }}' alt='new_1.jpg'>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="content-text">
                                                        <h3 class="title__text medium">
                                                            <a href="{{ route('post.detail', [$val->slug, $val->id]) }}" class="title__link">
                                                                {{ $val->title }}
                                                            </a>
                                                        </h3>
                                                    </div>
                                                </div>
                                                @endforeach
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>

<script src="js/tool.min.js"></script>

<script type="text/javascript" src="js/moment.min.js"></script>
<script type="text/javascript" src="js/daterangepicker.min.js"></script>
<script src="js/jquery.countdown.min.js"></script>
<script src="js/main.min.js"></script>


</body>

</html>
@endsection
