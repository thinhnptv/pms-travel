@extends('public.master')
@section('content')
<style>
   .edit-commment {
       display: none;
   }
</style>
<main id="main">
    <section class="section-list__new">
        <div class="bs-container">
            <div class="bs-row">
                <div class="bs-col">
                    <div class="module module-list__new">
                        <div class="module-content">
                            <div class="bs-row row-lg-15">
                                <div class="bs-col lg-100-15">
                                    <ul class="page">
                                        <li class="page-list">
                                            <a href="#" class="page-list__link">
                                                Trang chủ
                                            </a>
                                        </li>
                                        <li class="page-list">
                                            <a href="{{ route('post') }}" class="page-list__link">
                                                Tin tức
                                            </a>
                                        </li>

                                        <li class="page-list">
                                            <span class="page-list__link">
                                                {{ $articleDetail->title }}
                                            </span>
                                        </li>

                                    </ul>
                                </div>
                            </div>
                            <div class="bs-row row-lg-15 row-md-10 row-ms row-xs">
                                <div class="bs-col lg-75-15 md-75-10 ms-100 xs-100">
                                    @if(isset($articleDetail))
                                    <div class="new new-detail">
                                        <img src="{{ $articleDetail->image }}" alt="new_1.jpg">
                                        <div class="content">
                                            <h3 class="title medium">
                                                <p class="title__link">
                                                    {{ $articleDetail->title }}
                                                </p>
                                            </h3>
                                            <div class="category">
                                                <span class="sapn__sign">
                                                    <i class="fas fa-dollar-sign"></i>
                                                </span>
                                                by {{ optional($articleDetail->articleUser)->full_name }} - Ngày đăng {{ $articleDetail->created_at->format('d-m-Y') }}
                                            </div>

                                            <p class="p__text">
                                                {!! $articleDetail->content !!}
                                            </p>
                                        </div>
                                    </div>
                                    @endif

                                    @if(Auth::check())
                                    <div class="well">
                                        @if(session('success'))
                                            {{ session('success') }}
                                        @endif
                                        <h4>Bình luận của bạn <span class="glyphicon glyphicon-pencil"></span></h4>
                                        <form action="{{ route('post.comment', $articleDetail->id) }}" method="post">
                                            @csrf
                                            <div class="form-group">
                                                <textarea name="comment" class="form-control" rows="5"></textarea>
                                            </div>
                                            <button type="submit" class="btn btn-primary">Gửi</button>
                                        </form>
                                    </div>
                                    @endif
                                    @if (Auth::check() == false)
                                        <h1>Bình luận</h1>
                                        @foreach($commentArticle as $key => $val)
                                        <div class="content">
                                            <h3 class="title__name bold">
                                                {{ optional($val->user)->full_name }}
                                            </h3>

                                            <p class="p__day">
                                                {{ $val->created_at->format('d/m/Y H:i') }}
                                            </p>
                                            <p class="p__content">
                                                {{ $val->content }}
                                            </p>
                                        </div>
                                        <hr>
                                        @endforeach
                                    @else
                                        @if(isset($commentArticle) && count($commentArticle))
                                            @foreach($commentArticle as $key => $val)
                                            <div class="content">
                                                <h3 class="title__name bold">
                                                    {{ optional($val->user)->full_name }}
                                                </h3>

                                                <p class="p__day">
                                                    {{ $val->created_at->format('d/m/Y H:i') }}
                                                </p>
                                                <p class="p__content">
                                                    {{ $val->content }}
                                                </p>
                                                @if (Auth::id() == $val->user_id)
                                                <div data-id="{{ $val->id }}" id='edit-commment{{ $val->id }}' class="edit-commment" >
                                                <div class="well">
                                                    <h4>Chỉnh sửa bình luận <span class="glyphicon glyphicon-pencil"></span></h4>
                                                    <form action="{{ route('post.comment-update', $val->id) }}" method="post">
                                                        @csrf
                                                        <div class="form-group">
                                                            <textarea name="comment" class="form-control" rows="5"></textarea>
                                                        </div>
                                                        <button type="submit" class="btn btn-primary">Gửi</button>
                                                    </form>
                                                </div>
                                                </div>
                                                <a data-id="{{ $val->id }}" class="btn w-sm btn-success waves-effect waves-light" id="edit"><i class="fa fa-pencil-square-o" style="font-size:15px"></i></a>
                                                <a href="{{ route('post.comment-delete',$val->id) }}"><button type="button" class="btn w-sm btn-danger waves-effect waves-light"><i class="fa fa-trash-o" style="font-size:15px"></i></button></a>
                                                @endif
                                            </div>
                                            <hr>
                                            @endforeach
                                        @endif
                                    @endif
                                    @if (!empty($articleDetail->articleTag))
                                        <span>Tag: </span>
                                        @foreach ($articleDetail->articleTag as $item)
                                            <a href="{{ route('post.search', 'searchtag='.$item->tag_id) }}" >
                                                @foreach ($tags as $tag)
                                                        {{ ($item->tag_id == $tag->id) ? '#'.$tag->name : ' '  }}
                                                @endforeach
                                            </a>
                                        @endforeach
                                    @endif
                                    <div class="new-footer">
                                        <span class="span__name">
                                            Chia sẻ trên
                                        </span>
                                        <a href="" class="item-list__link">
                                            <i class="fab fa-facebook-f"></i>
                                        </a>
                                        <a href="" class="item-list__link">
                                            <i class="fab fa-twitter"></i>
                                        </a>
                                        <a href="" class="item-list__link">
                                            <i class="fab fa-youtube"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="bs-col lg-25-15 md-25-10 ms-100 xs-100">
                                    <div class="sidebar">

                                        <div class="content">
                                            <!-- Another variation with a button -->
                                            <form action="{{ route('post.search') }}" method="get" role="search" class="input-group">
                                                <input type="text" name="search" class="form-control" placeholder="Search">
                                                <div class="input-group-append">
                                                    <button style="position: absolute;" class="btn btn-secondary" type="submit">
                                                        <i class="fa fa-search"></i>
                                                    </button>
                                                </div>
                                            </form>
                                        </div>

                                        <h1 class="title medium">
                                            tin tức mới nhất
                                        </h1>
                                        <div class="content">
                                            @if(isset($newArticle))
                                                @foreach($newArticle as $key => $val)
                                                <div class="list-new">
                                                    <div class="avata">
                                                        <a href="{{ route('post.detail', [$val->slug, $val->id]) }}" class="item__link"></a>
                                                        <div class='ImagesFrame'>
                                                            <div class='ImagesFrameCrop0'>
                                                                <img src='{{ $val->image }}' alt='new_1.jpg'>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="content-text">
                                                        <h3 class="title__text medium">
                                                            <a href="{{ route('post.detail', [$val->slug, $val->id]) }}" class="title__link">
                                                                {{ $val->title }}
                                                            </a>
                                                        </h3>
                                                    </div>
                                                </div>
                                                @endforeach
                                            @endif
                                        </div>
                                        <h1 class="title medium">
                                            Phổ biến trên traveling
                                        </h1>
                                        <div class="content">
                                            @if(isset($highlightArticle) && count($highlightArticle))
                                                @foreach($highlightArticle as $key => $val)
                                                <div class="list-new">
                                                    <div class="avata">
                                                        <a href="{{ route('post.detail', [$val->slug, $val->id]) }}" class="item__link"></a>
                                                        <div class='ImagesFrame'>
                                                            <div class='ImagesFrameCrop0'>
                                                                <img src='{{ $val->image }}' alt='new_1.jpg'>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="content-text">
                                                        <h3 class="title__text medium">
                                                            <a href="{{ route('post.detail', [$val->slug, $val->id]) }}" class="title__link">
                                                                {{ $val->title }}
                                                            </a>
                                                        </h3>
                                                    </div>
                                                </div>
                                                @endforeach
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>

<script src="js/tool.min.js"></script>

<script type="text/javascript" src="js/moment.min.js"></script>
<script type="text/javascript" src="js/daterangepicker.min.js"></script>
<script src="js/jquery.countdown.min.js"></script>
<script src="js/main.min.js"></script>

<script>
    $(document).ready(function(){
        $(document).on('click', '#edit', function () {
            var id = $(this).attr("data-id");
            $("#edit-commment" + id).show()
        });
    });
</script>
@endsection
