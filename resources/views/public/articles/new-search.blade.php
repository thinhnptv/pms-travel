@extends('public.master')
@section('css')
@endsection
@section('content')

<main id="main">
    <section class="section-list__new">
        <div class="bs-container">
            <div class="bs-row">
                <div class="bs-col">
                    <div class="module module-list__new">
                        <div class="module-content">
                            <div class="bs-row row-lg-15">
                                <div class="bs-col lg-100-15">
                                    <ul class="page">
                                        <li class="page-list">
                                            <a href="{{ route('post.home') }}" class="page-list__link">
                                                Trang chủ
                                            </a>
                                        </li>
                                        <li class="page-list">
                                            <a href="{{ route('post') }}" class="page-list__link">
                                                Tin tức
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="bs-row row-lg-15 row-md-10 row-ms row-xs">
                                <div class="bs-col lg-75-15 md-75-10 ms-100 xs-100">
                                    @if(isset($searchArticle))
                                        @foreach($searchArticle as $key => $val)
                                            <div class="new">
                                                <div class="avata">
                                                    <a href="{{ route('post.detail', [$val->slug, $val->id]) }}" class="avata__link"></a>
                                                    <div class="ImagesFrame">
                                                        <div class="ImagesFrameCrop0">
                                                            <img src="{{ $val->image }}" alt="">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="content">
                                                    <h3 class="title medium">
                                                        <a href="{{ route('post.detail', [$val->slug, $val->id]) }}" class="title__link">
                                                            {{ $val->title }}
                                                        </a>
                                                    </h3>
                                                    <div class="category">
                                                <span class="sapn__sign">
                                                    <i class="fas fa-dollar-sign"></i>
                                                </span>
                                                        by {{ optional($val->articleUser)->full_name }} - Ngày đăng {{ $val->updated_at->format('d-m-Y') }}
                                                    </div>
                                                    <p class="descript">
                                                        {{ $val->description }}
                                                    </p>
                                                    <a href="{{ route('post.detail', [$val->slug, $val->id]) }}" class="view__link black">
                                                        Xem chi tiết
                                                    </a>
                                                </div>
                                            </div>
                                        @endforeach
                                    @endif
                                    <div style="text-align: center;">
                                        {{ $searchArticle->links('vendor.pagination.bootstrap-4') }}
                                    </div>

                                    {{--<div class="fexPagintaion text-center clearfix">--}}
                                    {{--<ul class="pagination">--}}
                                    {{--<li><a href="new-detail.html">«</a></li>--}}
                                    {{--<li class="active"><a href="new-detail.html">1</a></li>--}}
                                    {{--<li><a href="new-detail.html">2</a></li>--}}
                                    {{--<li><a href="new-detail.html">3</a></li>--}}
                                    {{--<li><a href="new-detail.html">...</a></li>--}}
                                    {{--<li><a href="new-detail.html">6</a></li>--}}
                                    {{--<li><a href="new-detail.html">»</a></li>--}}
                                    {{--</ul>--}}
                                    {{--</div>--}}
                                </div>
                                <div class="bs-col lg-25-15 md-25-10 ms-100 xs-100">
                                    <div class="sidebar">
                                        <div class="content">
                                            <!-- Another variation with a button -->
                                            <form action="{{ route('post.search') }}" method="get" class="input-group">
                                                <input type="text" name="search" value="{{ Request::get("search") }}" class="form-control" placeholder="Search">
                                                <div class="input-group-append">
                                                    <button style="position: absolute;" class="btn btn-secondary" type="submit">
                                                        <i class="fa fa-search"></i>
                                                    </button>
                                                </div>
                                            </form>
                                        </div>
                                        <h1 class="title medium">
                                            tin tức mới nhất
                                        </h1>
                                        <div class="content">
                                            @if(isset($newArticle))
                                                @foreach($newArticle as $key => $val)
                                                    <div class="list-new">
                                                        <div class="avata">
                                                            <a href="{{ route('post.detail', [$val->slug, $val->id]) }}" class="item__link"></a>
                                                            <div class='ImagesFrame'>
                                                                <div class='ImagesFrameCrop0'>
                                                                    <img src='{{ $val->image }}' alt='new_1.jpg'>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="content-text">
                                                            <h3 class="title__text medium">
                                                                <a href="{{ route('post.detail', [$val->slug, $val->id]) }}" class="title__link">
                                                                    {{ $val->title }}
                                                                </a>
                                                            </h3>
                                                        </div>
                                                    </div>
                                                @endforeach
                                            @endif
                                        </div>
                                        <h1 class="title medium">
                                            Phổ biến trên traveling
                                        </h1>
                                        <div class="content">
                                            @if(isset($highlightArticle))
                                                @foreach($highlightArticle as $key => $val)
                                                    <div class="list-new">
                                                        <div class="avata">
                                                            <a href="{{ route('post.detail', [$val->slug, $val->id]) }}" class="item__link"></a>
                                                            <div class='ImagesFrame'>
                                                                <div class='ImagesFrameCrop0'>
                                                                    <img src='{{ $val->image }}' alt='new_1.jpg'>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="content-text">
                                                            <h3 class="title__text medium">
                                                                <a href="{{ route('post.detail', [$val->slug, $val->id]) }}" class="title__link">
                                                                    {{ $val->title }}
                                                                </a>
                                                            </h3>
                                                        </div>
                                                    </div>
                                                @endforeach
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>

<script src="js/tool.min.js"></script>

<script type="text/javascript" src="js/moment.min.js"></script>
<script type="text/javascript" src="js/daterangepicker.min.js"></script>
<script src="js/jquery.countdown.min.js"></script>
<script src="js/main.min.js"></script>

@endsection
