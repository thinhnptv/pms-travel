<footer id="footer">
    <div class="bs-container">
        <div class="bs-row">
            <div class="bs-col">
                <div class="footer-top">
                    <div class="menu-container">
                        <div class="menu-header">
                            <h2 class="title medium">
                                Tours theo Tỉnh Thành
                            </h2>
                            <a href="#" class="view__links" title="Xem tours tại tất cả các tỉnh">
                                Xem tours tại tất cả các tỉnh
                            </a>
                        </div>
                        <div class="menu-content">
                            <ul class="item">
                                <li class="item-list">
                                    <a href="#" class="item-list__links">
                                        Tour An Giang
                                    </a>
                                </li>
                                <li class="item-list">
                                    <a href="#" class="item-list__links">
                                        Tour Bà Rịa - Vũng Tàu
                                    </a>
                                </li>
                                <li class="item-list">
                                    <a href="#" class="item-list__links">
                                        Tour Bắc Giang
                                    </a>
                                </li>
                                <li class="item-list">
                                    <a href="#" class="item-list__links">
                                        Tour Bến Tre
                                    </a>
                                </li>
                                <li class="item-list">
                                    <a href="#" class="item-list__links">
                                        Tour Bình Định
                                    </a>
                                </li>
                                <li class="item-list">
                                    <a href="#" class="item-list__links">
                                        Tour Bình Dương
                                    </a>
                                </li>
                            </ul>
                            <ul class="item">
                                <li class="item-list">
                                    <a href="#" class="item-list__links">
                                        Tour Bình Thuận
                                    </a>
                                </li>
                                <li class="item-list">
                                    <a href="#" class="item-list__links">
                                        Tour Cần Thơ
                                    </a>
                                </li>
                                <li class="item-list">
                                    <a href="#" class="item-list__links">
                                        Tour Đà Nẵng
                                    </a>
                                </li>
                                <li class="item-list">
                                    <a href="#" class="item-list__links">
                                        Tour Đắk Lắk
                                    </a>
                                </li>
                                <li class="item-list">
                                    <a href="#" class="item-list__links">
                                        Tour Điện Biên
                                    </a>
                                </li>
                                <li class="item-list">
                                    <a href="#" class="item-list__links">
                                        Tour Đồng Nai
                                    </a>
                                </li>
                            </ul>
                            <ul class="item">
                                <li class="item-list">
                                    <a href="#" class="item-list__links">
                                        Tour Hà Nội

                                    </a>
                                </li>
                                <li class="item-list">
                                    <a href="#" class="item-list__links">
                                        Tour Hà Tĩnh

                                    </a>
                                </li>
                                <li class="item-list">
                                    <a href="#" class="item-list__links">
                                        Tour Hải Phòng

                                    </a>
                                </li>
                                <li class="item-list">
                                    <a href="#" class="item-list__links">
                                        Tour Hòa Bình

                                    </a>
                                </li>
                                <li class="item-list">
                                    <a href="#" class="item-list__links">
                                        Tour Khánh Hòa

                                    </a>
                                </li>
                                <li class="item-list">
                                    <a href="#" class="item-list__links">
                                        Tour Kiên Giang
                                    </a>
                                </li>

                            </ul>
                            <ul class="item">
                                <li class="item-list">
                                    <a href="#" class="item-list__links">
                                        Tour Lai Châu

                                    </a>
                                </li>
                                <li class="item-list">
                                    <a href="#" class="item-list__links">
                                        Tour Lâm Đồng

                                    </a>
                                </li>
                                <li class="item-list">
                                    <a href="#" class="item-list__links">
                                        Tour Lạng Sơn

                                    </a>
                                </li>
                                <li class="item-list">
                                    <a href="#" class="item-list__links">
                                        Tour Lào Cai

                                    </a>
                                </li>
                                <li class="item-list">
                                    <a href="#" class="item-list__links">
                                        Tour Nghệ An

                                    </a>
                                </li>
                                <li class="item-list">
                                    <a href="#" class="item-list__links">
                                        Tour Ninh Bình
                                    </a>
                                </li>
                            </ul>
                            <ul class="item">
                                <li class="item-list">
                                    <a href="#" class="item-list__links">
                                        Tour Ninh Thuận

                                    </a>
                                </li>
                                <li class="item-list">
                                    <a href="#" class="item-list__links">
                                        Tour Phú Yên

                                    </a>
                                </li>
                                <li class="item-list">
                                    <a href="#" class="item-list__links">
                                        Tour Quảng Bình

                                    </a>
                                </li>
                                <li class="item-list">
                                    <a href="#" class="item-list__links">
                                        Tour Quảng Nam

                                    </a>
                                </li>
                                <li class="item-list">
                                    <a href="#" class="item-list__links">
                                        Tour Quảng Ngãi

                                    </a>
                                </li>
                                <li class="item-list">
                                    <a href="#" class="item-list__links">
                                        Tour Quảng Ninh
                                    </a>
                                </li>

                            </ul>
                            <ul class="item">
                                <li class="item-list">
                                    <a href="#" class="item-list__links">
                                        Tour Quảng Trị

                                    </a>
                                </li>
                                <li class="item-list">
                                    <a href="#" class="item-list__links">
                                        Tour Thái Nguyên

                                    </a>
                                </li>
                                <li class="item-list">
                                    <a href="#" class="item-list__links">
                                        Tour Thanh Hóa

                                    </a>
                                </li>
                                <li class="item-list">
                                    <a href="#" class="item-list__links">
                                        Tour Thừa Thiên Huế

                                    </a>
                                </li>
                                <li class="item-list">
                                    <a href="#" class="item-list__links">
                                        Tour Tiền Giang

                                    </a>
                                </li>
                                <li class="item-list">
                                    <a href="#" class="item-list__links">
                                        Tour TP Hồ Chí Minh

                                    </a>
                                </li>
                                <li class="item-list">
                                    <a href="#" class="item-list__links">
                                        Tour Vĩnh PhúcTour An Giang
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="menu-container">
                        <div class="menu-header">
                            <h2 class="title medium">
                                Khách sạn theo Tỉnh Thành
                            </h2>
                            <a href="#" class="view__links" title="Xem khách sạn tại tất cả các tỉnh">
                                Xem khách sạn tại tất cả các tỉnh
                            </a>
                        </div>
                        <div class="menu-content">
                            <ul class="item">
                                <li class="item-list">
                                    <a href="#" class="item-list__links">
                                        Khách sạn An Giang

                                    </a>
                                </li>
                                <li class="item-list">
                                    <a href="#" class="item-list__links">
                                        Khách sạn Bà Rịa - Vũng Tàu Khách sạn Bắc Giang

                                    </a>
                                </li>
                                <li class="item-list">
                                    <a href="#" class="item-list__links">

                                        Khách sạn Bắc Giang



                                    </a>
                                </li>
                                <li class="item-list">
                                    <a href="#" class="item-list__links">
                                        Khách sạn Bến Tre


                                    </a>
                                </li>
                                <li class="item-list">
                                    <a href="#" class="item-list__links">
                                        Khách sạn Bình Định

                                    </a>
                                </li>
                                <li class="item-list">
                                    <a href="#" class="item-list__links">
                                        Khách sạn Bình Dương
                                    </a>
                                </li>
                            </ul>
                            <ul class="item">
                                <li class="item-list">
                                    <a href="#" class="item-list__links">
                                        Khách sạn Bình Thuận

                                    </a>
                                </li>
                                <li class="item-list">
                                    <a href="#" class="item-list__links">
                                        Khách sạn Cần Thơ

                                    </a>
                                </li>
                                <li class="item-list">
                                    <a href="#" class="item-list__links">
                                        Khách sạn Đà Nẵng

                                    </a>
                                </li>
                                <li class="item-list">
                                    <a href="#" class="item-list__links">
                                        Khách sạn Đắk Lắk

                                    </a>
                                </li>
                                <li class="item-list">
                                    <a href="#" class="item-list__links">
                                        Khách sạn Điện Biên

                                    </a>
                                </li>
                                <li class="item-list">
                                    <a href="#" class="item-list__links">
                                        Khách sạn Đồng Nai
                                    </a>
                                </li>
                            </ul>
                            <ul class="item">
                                <li class="item-list">
                                    <a href="#" class="item-list__links">
                                        Khách sạn Hà Nội

                                    </a>
                                </li>
                                <li class="item-list">
                                    <a href="#" class="item-list__links">
                                        Khách sạn Hà Tĩnh

                                    </a>
                                </li>
                                <li class="item-list">
                                    <a href="#" class="item-list__links">
                                        Khách sạn Hải Phòng

                                    </a>
                                </li>
                                <li class="item-list">
                                    <a href="#" class="item-list__links">
                                        Khách sạn Hòa Bình

                                    </a>
                                </li>
                                <li class="item-list">
                                    <a href="#" class="item-list__links">
                                        Khách sạn Khánh Hòa

                                    </a>
                                </li>
                                <li class="item-list">
                                    <a href="#" class="item-list__links">
                                        Khách sạn Kiên Giang
                                    </a>
                                </li>
                            </ul>
                            <ul class="item">
                                <li class="item-list">
                                    <a href="#" class="item-list__links">
                                        Khách sạn Lai Châu

                                    </a>
                                </li>
                                <li class="item-list">
                                    <a href="#" class="item-list__links">
                                        Khách sạn Lâm Đồng

                                    </a>
                                </li>
                                <li class="item-list">
                                    <a href="#" class="item-list__links">
                                        Khách sạn Lạng Sơn

                                    </a>
                                </li>
                                <li class="item-list">
                                    <a href="#" class="item-list__links">
                                        Khách sạn Lào Cai

                                    </a>
                                </li>
                                <li class="item-list">
                                    <a href="#" class="item-list__links">

                                        Khách sạn Nghệ An

                                    </a>
                                </li>
                                <li class="item-list">
                                    <a href="#" class="item-list__links">
                                        Khách sạn Ninh Bình
                                    </a>
                                </li>

                            </ul>
                            <ul class="item">
                                <li class="item-list">
                                    <a href="#" class="item-list__links">
                                        Khách sạn Ninh Thuận

                                    </a>
                                </li>
                                <li class="item-list">
                                    <a href="#" class="item-list__links">
                                        Khách sạn Phú Yên

                                    </a>
                                </li>
                                <li class="item-list">
                                    <a href="#" class="item-list__links">
                                        Khách sạn Quảng Bình

                                    </a>
                                </li>
                                <li class="item-list">
                                    <a href="#" class="item-list__links">
                                        Khách sạn Quảng Nam

                                    </a>
                                </li>
                                <li class="item-list">
                                    <a href="#" class="item-list__links">
                                        Khách sạn Quảng Ngãi

                                    </a>
                                </li>
                                <li class="item-list">
                                    <a href="#" class="item-list__links">
                                        Khách sạn Quảng Ninh
                                    </a>
                                </li>

                            </ul>
                            <ul class="item">
                                <li class="item-list">
                                    <a href="#" class="item-list__links">
                                        Khách sạn Quảng Trị

                                    </a>
                                </li>
                                <li class="item-list">
                                    <a href="#" class="item-list__links">
                                        Khách sạn Thái Nguyên

                                    </a>
                                </li>
                                <li class="item-list">
                                    <a href="#" class="item-list__links">
                                        Khách sạn Thanh Hóa
                                    </a>
                                </li>
                                <li class="item-list">
                                    <a href="#" class="item-list__links">

                                        Khách sạn Thừa Thiên Huế
                                    </a>
                                </li>
                                <li class="item-list">
                                    <a href="#" class="item-list__links">

                                        Khách sạn Tiền Giang
                                    </a>
                                </li>
                                <li class="item-list">
                                    <a href="#" class="item-list__links">

                                        Khách sạn TP Hồ Chí Minh

                                    </a>
                                </li>
                                <li class="item-list">
                                    <a href="#" class="item-list__links">
                                        Khách sạn Vĩnh Phúc
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="menu-container">
                        <div class="menu-header">
                            <h2 class="title medium">
                                Xe hơi theo Tỉnh Thành
                            </h2>
                            <a href="#" class="view__links" title="Xem xe hơi tại tất cả các tỉnh">
                                Xem xe hơi tại tất cả các tỉnh
                            </a>
                        </div>
                        <div class="menu-content">
                            <ul class="item">
                                <li class="item-list">
                                    <a href="#" class="item-list__links">
                                        Xe hơi An Giang

                                    </a>
                                </li>
                                <li class="item-list">
                                    <a href="#" class="item-list__links">
                                        Xe hơi Bà Rịa - Vũng Tàu

                                    </a>
                                </li>
                                <li class="item-list">
                                    <a href="#" class="item-list__links">
                                        Xe hơi Bắc Giang

                                    </a>
                                </li>
                                <li class="item-list">
                                    <a href="#" class="item-list__links">
                                        Xe hơi Bến Tre

                                    </a>
                                </li>
                                <li class="item-list">
                                    <a href="#" class="item-list__links">
                                        Xe hơi Bình Định

                                    </a>
                                </li>
                                <li class="item-list">
                                    <a href="#" class="item-list__links">
                                        Xe hơi Bình Dương
                                    </a>
                                </li>

                            </ul>
                            <ul class="item">
                                <li class="item-list">
                                    <a href="#" class="item-list__links">
                                        Xe hơi Bình Thuận

                                    </a>
                                </li>
                                <li class="item-list">
                                    <a href="#" class="item-list__links">
                                        Xe hơi Cần Thơ

                                    </a>
                                </li>
                                <li class="item-list">
                                    <a href="#" class="item-list__links">
                                        Xe hơi Đà Nẵng

                                    </a>
                                </li>
                                <li class="item-list">
                                    <a href="#" class="item-list__links">
                                        Xe hơi Đắk Lắk

                                    </a>
                                </li>
                                <li class="item-list">
                                    <a href="#" class="item-list__links">
                                        Xe hơi Điện Biên

                                    </a>
                                </li>
                                <li class="item-list">
                                    <a href="#" class="item-list__links">
                                        Xe hơi Đồng Nai
                                    </a>
                                </li>
                            </ul>
                            <ul class="item">
                                <li class="item-list">
                                    <a href="#" class="item-list__links">
                                        Xe hơi Hà Nội

                                    </a>
                                </li>
                                <li class="item-list">
                                    <a href="#" class="item-list__links">
                                        Xe hơi Hà Tĩnh

                                    </a>
                                </li>
                                <li class="item-list">
                                    <a href="#" class="item-list__links">
                                        Xe hơi Hải Phòng

                                    </a>
                                </li>
                                <li class="item-list">
                                    <a href="#" class="item-list__links">
                                        Xe hơi Hòa Bình

                                    </a>
                                </li>
                                <li class="item-list">
                                    <a href="#" class="item-list__links">
                                        Xe hơi Khánh Hòa

                                    </a>
                                </li>
                                <li class="item-list">
                                    <a href="#" class="item-list__links">
                                        Xe hơi Kiên Giang
                                    </a>
                                </li>
                            </ul>
                            <ul class="item">
                                <li class="item-list">
                                    <a href="#" class="item-list__links">
                                        Xe hơi Lai Châu

                                    </a>
                                </li>
                                <li class="item-list">
                                    <a href="#" class="item-list__links">
                                        Xe hơi Lâm Đồng

                                    </a>
                                </li>
                                <li class="item-list">
                                    <a href="#" class="item-list__links">
                                        Xe hơi Lạng Sơn

                                    </a>
                                </li>
                                <li class="item-list">
                                    <a href="#" class="item-list__links">
                                        Xe hơi Lào Cai

                                    </a>
                                </li>
                                <li class="item-list">
                                    <a href="#" class="item-list__links">
                                        Xe hơi Nghệ An

                                    </a>
                                </li>
                                <li class="item-list">
                                    <a href="#" class="item-list__links">
                                        Xe hơi Ninh Bình
                                    </a>
                                </li>

                            </ul>
                            <ul class="item">
                                <li class="item-list">
                                    <a href="#" class="item-list__links">
                                        Xe hơi Ninh Thuận

                                    </a>
                                </li>
                                <li class="item-list">
                                    <a href="#" class="item-list__links">
                                        Xe hơi Phú Yên

                                    </a>
                                </li>
                                <li class="item-list">
                                    <a href="#" class="item-list__links">
                                        Xe hơi Quảng Bình

                                    </a>
                                </li>
                                <li class="item-list">
                                    <a href="#" class="item-list__links">
                                        Xe hơi Quảng Nam

                                    </a>
                                </li>
                                <li class="item-list">
                                    <a href="#" class="item-list__links">
                                        Xe hơi Quảng Ngãi

                                    </a>
                                </li>
                                <li class="item-list">
                                    <a href="#" class="item-list__links">
                                        Xe hơi Quảng Ninh
                                    </a>
                                </li>

                            </ul>
                            <ul class="item">
                                <li class="item-list">
                                    <a href="#" class="item-list__links">
                                        Xe hơi Quảng Trị

                                    </a>
                                </li>
                                <li class="item-list">
                                    <a href="#" class="item-list__links">
                                        Xe hơi Thái Nguyên

                                    </a>
                                </li>
                                <li class="item-list">
                                    <a href="#" class="item-list__links">
                                        Xe hơi Thanh Hóa

                                    </a>
                                </li>
                                <li class="item-list">
                                    <a href="#" class="item-list__links">
                                        Xe hơi Thừa Thiên Huế

                                    </a>
                                </li>
                                <li class="item-list">
                                    <a href="#" class="item-list__links">
                                        Xe hơi Tiền Giang

                                    </a>
                                </li>
                                <li class="item-list">
                                    <a href="#" class="item-list__links">
                                        Xe hơi TP Hồ Chí Minh

                                    </a>
                                </li>
                                <li class="item-list">
                                    <a href="#" class="item-list__links">
                                        Xe hơi Vĩnh Phúc
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="footer-banner clearfix">
                    <div class="div__left">
                        <p class="p__text">
                            <span class="span__title medium">
                                Support
                            </span>
                            <span class="span__text light">
                                0123.456.789
                            </span>
                        </p>
                        <p class="p__text">
                            <span class="span__title medium">
                                email
                            </span>
                            <span class="span__text light">
                                support@travel.vn
                            </span>
                        </p>
                    </div>
                    <div class="div__right">
                        <div class="div__shar">
                            <a href="#" class="icon__links icon__facebook medium">
                                <i class="fab fa-facebook-f"></i> facebook
                            </a>
                            <a href="#" class="icon__links icon__twitter medium">
                                <i class="fab fa-twitter"></i> twitter
                            </a>
                            <a href="#" class="icon__links icon__youtube medium">
                                <i class="fab fa-youtube"></i> youtube
                            </a>
                        </div>
                        <div class="div__menu">
                            <ul class="item">
                                <li class="item-list">
                                    <a href="#" class="item-list__links">
                                        Về Travel
                                    </a>
                                </li>
                                <li class="item-list">
                                    <a href="#" class="item-list__links">
                                        Liên hệ
                                    </a>
                                </li>
                                <li class="item-list">
                                    <a href="#" class="item-list__links">
                                        Điều khoản sử dụng
                                    </a>
                                </li>
                                <li class="item-list">
                                    <a href="#" class="item-list__links">
                                        Chính sách bảo mật
                                    </a>
                                </li>
                                <li class="item-list">
                                    <a href="#" class="item-list__links">
                                        Tuyển dụng
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="copyRight">
        <div class="bs-container">
            <div class="bs-row">
                <div class="bs-col">
                    <div class="div__left">
                        <h3 class="h3__text bold">
                            Công ty CPTT Thiên Việt
                        </h3>
                        <p class="p__text">
                            <b>Trụ sở:</b> Số 199 Phùng Khoang, P. Trung Văn, Q. Nam Từ Liêm, TP. Hà Nội
                        </p>
                        <p class="p__text">
                            <b>TTDVKH:</b> P2222 Tầng 22 CT6A, P Kiến Hưng, Q. Hà Đông, TP. Hà Nội
                        </p>
                    </div>
                    <div class="div__right">
                        <p class="p__text">
                            ĐKKD số: 1234567890
                        </p>
                        <p class="p__text">
                            Ngày cấp ĐKKD: 9/5/2016
                        </p>
                        <p class="p__text">
                            Bản quyền © 2020 Vntrip.vn
                        </p>



                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>