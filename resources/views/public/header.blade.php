
<body class="index">
    <header id="header">
    <div class="header-top">
        <div class="bs-container">
            <div class="bs-row row-lg-15 row-md-10 row-sm-5 row-xs">
                <div class="bs-col lg-50-15 md-50-10 sm-50-5 xs-80 tn-80">
                    <ul class="item">
                        <li class="item-list">
                            <a href="#" class="item-list__link">
                                <i class="fab fa-facebook-f"></i>
                            </a>
                        </li>
                        <li class="item-list">
                            <a href="" class="item-list__link">
                                <i class="fab fa-linkedin-in"></i>
                            </a>
                        </li>
                        <li class="item-list">
                            <a href="" class="item-list__link">
                                <i class="fab fa-google-plus-g"></i>
                            </a>
                        </li>
                        <li class="item-list">
                            <a href="" class="item-list__link">
                                support@traveling.vn
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="bs-col lg-50-15 md-50-10 sm-50-5 xs-20 tn-20">

                    <div class="right">
                        <i class="fas fa-ellipsis-v openOnlickMobile"></i>
                        <ul class="item">
                            @if (Auth::check())
                                <li class="item-list dropdown ">
                                    <a class="item-list__link" href="#" data-toggle="dropdown" class="login" aria-expanded="true">
                                        {{ Auth::user()->firstname }} {{ Auth::user()->lastname }}
                                        <i class="fa fa-user-circle" aria-hidden="true"></i>
                                        <i class="fa fa-angle-down"></i>
                                    </a>
                                    <ul class="dropdown-menu text-left " x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(18px, 17px, 0px);">
                                        <li>
                                            <a href="#">
                                                <i class="fa fa-user-secret" aria-hidden="true"></i>
                                                Bảng điều khiển nhà cung cấp
                                            </a>
                                        </li>
                                        <li class="menu-hr">
                                            <a href="#">
                                                <i class="fa fa-file" aria-hidden="true"></i>
                                                Thông tin của tôi
                                            </a>
                                        </li>
                                        <li class="menu-hr">
                                            <a href="#">
                                                <i class="fa fa-align-justify" aria-hidden="true"></i>
                                                Lịch sử đặt
                                            </a>
                                        </li>
                                        <li class="menu-hr">
                                            <a href="#">
                                                <i class="fas fa-lock"></i>
                                                Thay đổi mật khẩu
                                            </a>
                                        </li>
                                        <li class="menu-hr">
                                            <a href="#">
                                                <i class="fas fa-user-cog"></i>
                                                Bảng điều khiển admin
                                            </a>
                                        </li>
                                        <li class="menu-hr">
                                            <a href="{{route('destroy')}}" modal-data="#logout">
                                                <i class="fas fa-power-off"></i>
                                                Đăng xuất
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                                @else
                                    <li class="item-list">
                                        <a href="javascript:void(0)" class="item-list__link"  modal-show="show"
                                        modal-data="#login">
                                        Đăng nhập
                                        <i class="fas fa-sign-in-alt"></i>
                                        </a>
                                    </li>
                                    <li class="item-list">
                                        <a href="javascript:void(0)" class="item-list__link" modal-show="show"
                                            modal-data="#singUp">
                                            Đăng ký
                                            <i class="fa fa-user-plus" aria-hidden="true"></i>
                                        </a>
                                    </li>
                                @endif
                            <li class="item-list">
                                <a href="#" class="item-list__link">
                                    USD
                                    <i class="fas fa-angle-down"></i>
                                </a>
                                <ul class="sub">
                                    <li>
                                        <a href="#">
                                           VN
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="item-list">
                                <a href="#" class="item-list__link">
                                    <img src="{{url('/pms/images/icon_vn.png')}}" class="icon_lang" alt="icon_vn.png">
                                    <span class="span__name">
                                        Tiếng Việt
                                    </span>
                                    <i class="fas fa-angle-down"></i>
                                </a>
                                <ul class="sub">
                                    <li>
                                        <a href="#">
                                            <img src="{{url('/pms/images/icon_vn.png')}}" class="icon_lang" alt="icon_vn.png">
                                            <span class="span__name">
                                                Tiếng Việt
                                            </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <img src="{{url('/pms/images/icon_vn.png')}}" class="icon_lang" alt="icon_vn.png">
                                            <span class="span__name">
                                                Tiếng Việt
                                            </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <img src="{{url('/pms/images/icon_vn.png')}}" class="icon_lang" alt="icon_vn.png">
                                            <span class="span__name">
                                                Tiếng Việt
                                            </span>

                                        </a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="header-banner">
        <div class="bs-container">
            <div class="bs-row row-lg-15 row-md-10 row-sm row-xs">
                <div class="bs-col lg-20-15 md-20-10 sm-100 xs-50 tn-50">
                    <h1 class="logo">
                        <a href="/" class="logo__links">
                            <img src="{{url('/pms/images/logo.png')}}" class="img__links" alt="logo.png">
                        </a>
                    </h1>
                </div>
                <div class="bs-col lg-80-15 md-80-10 sm-100 xs-50 tn-50">
                    <div class="banner-right">
                        <ul class="menu menuAdd">
                            <li class="mobilde">
                                <a href="/" class="logoMobile">
                                    <img src="{{url('/pms/images/logo.png')}}" class="img__logo" alt="">
                                </a>
                                <i class="fas fa-window-close"></i>
                            </li>
                            <li class="menu-list active">
                                <a href="/" class="menu-list__link">
                                    Trang chủ
                                </a>
                            </li>
                            <li class="menu-list">
                                <a href="tour.html" class="menu-list__link">
                                    tours
                                </a>
                                <ul class="menu-sub">
                                    <li class="sub-list">
                                        <a href="{{ route('public.listTour') }}" class="sub-list__link">
                                            Tour List
                                        </a>
                                    </li>
                                    <li class="sub-list">
                                        <a href="{{ route('tour.map') }}" class="sub-list__link">
                                            Tour map
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="menu-list">
                                <a href="{{ route('hotel.tc') }}" class="menu-list__link" >
                                    Hotels
                                </a>
                                <ul class="menu-sub">
                                    <li class="sub-list">
                                        <a href="{{ route('hotel.ds') }}" class="sub-list__link">
                                            Hotel List
                                        </a>
                                    </li>
                                    <li class="sub-list">
                                        <a href="{{ route('hotel.map') }}" class="sub-list__link">
                                            Hotel map
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="menu-list">
                                <a href="{{ route('public.indexCar') }}" class="menu-list__link">
                                    car
                                </a>
                                <ul class="menu-sub">
                                    <li class="sub-list">
                                        <a href="{{ route('public.listCar') }}" class="sub-list__link">
                                            Car List
                                        </a>
                                    </li>
                                    <li class="sub-list">
                                        <a href="{{ route('public.map.listCar') }}" class="sub-list__link">
                                            Car map
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="menu-list">
                                <a href="{{ route('space') }}" class="menu-list__link">
                                    space
                                </a>
                                <ul class="menu-sub">
                                    <li class="sub-list">
                                    <a href="{{ route('space.list') }}" class="sub-list__link">
                                            Danh sách tất cả space
                                        </a>
                                    </li>
                                    <li class="sub-list">
                                        <a href="{{ route('space.spaceMap') }}" class="sub-list__link">
                                            Space map
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="menu-list">
                                <a href="{{ route('post') }}" class="menu-list__link">
                                    Tin tức
                                </a>
                                <ul class="menu-sub">
                                    <li class="sub-list">
                                    <a href="{{ route('auth.becomevendor') }}" class="sub-list__link">
                                            become a vendor
                                        </a>
                                    </li>

                                    {{-- <li class="sub-list">
                                        <a href="become.html" class="sub-list__link">
                                            become a vendor
                                        </a>
                                    </li>
                                    <li class="sub-list">
                                        <a href="become.html" class="sub-list__link">
                                            become a vendor
                                        </a>
                                    </li>
                                    <li class="sub-list">
                                        <a href="become.html" class="sub-list__link">
                                            become a vendor
                                        </a>
                                    </li> --}}
                                </ul>
                            </li>
                            <li class="menu-list">
                                <a href="about.html" class="menu-list__link">
                                    Về chúng tôi
                                </a>
                            </li>
                            <li class="menu-list">
                                <a href="{{ route('lienhe') }}" class="menu-list__link">
                                    liên hệ
                                </a>
                            </li>
                        </ul>
                        <span class="openMenu">
                            <i class="fas fa-bars"></i>
                        </span>
                        <div class="phone">
                            <a class="phone__links">
                                0945 501 890
                            </a>
                            <p class="p__text">
                                <i class="far fa-clock"></i>
                                <span class="span__name">
                                    8h30p - 18h
                                </span>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
<div class="bs-modal" id="login">
    <div class="modal-frame">
        <div class="content-modal">
            <div class="header-modal">
                <h3 class="modal__title">Đăng nhập</h3>
                <span title="close" modal-show="close" class="close__modal"><img src="{{url('/pms/images/ico_close.svg')}}" alt=""></span>
            </div>
            <div class="body-modal">
            <form action="{{route('auth.login')}}" method="POST" role="form" class="form">
                        @csrf
                        <div class="form-group">
                            <input type="email" class="form-control email-dn" placeholder="Địa chỉ email " name="email" required>
                            <i class="input-icon">
                                <img src="{{url('/pms/images/ico_email_login_form.svg')}}">
                            </i>

                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control password-dn" name="password" placeholder="Mật khẩu" required>
                            <i class="input-icon">
                                <i class="fas fa-lock"></i>
                            </i>
                        </div>
                        <div class="form-group">
                            <label class="ferm_checkbox">
                                <input type="checkbox">
                                Lưu mật khẩu <a href="#" class="span__name">quên mật khẩu</a>

                            </label>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary form-submit btn-dangnhap">
                                Đăng nhập
                            </button>
                        </div>
                        <div class="advanced">
                            <p class="text-center">
                                hoặc tiếp tục với
                            </p>
                            <div class="row">
                                <div class="col-xs-12 text-center">
                                    <a href="#" class="btn btn-facebok">
                                        <i class="fab fa-facebook-f"></i>
                                        Facebook
                                    </a>
                                    <a href="#" class="btn btn-google">
                                        <i class="fab fa-google-plus-g "></i>
                                        Google
                                    </a>
                                </div>
                            </div>
                        </div>
                        {{-- #login --}}
                        {{-- @include('public.errors') --}}
                </form>
            </div>
        </div>
    </div>
</div>
<div class="bs-modal" id="singUp">
    <div class="modal-frame">
        <div class="content-modal">
            <div class="header-modal">
                <h3 class="modal__title">Đăng ký</h3>
                {{-- @include('public.errors') --}}
                <span title="close" modal-show="close" class="close__modal"><img src="{{url('/pms/images/ico_close.svg')}}" alt=""></span>
            </div>
            <div class="body-modal">
            <form action="{{route('auth.register')}}" method="POST" role="form" class="form">
                        @csrf
                        <div class="row">
                            <div class="col-lg-6 col-md-12">
                                <div class="form-group">
                                    <input type="text" class="form-control firstname" name="firstname" placeholder="Tên" >
                                    <i class="input-icon field-icon fa">
                                        <img src="{{url('/pms/images/ico_fullname_signup.svg')}}">
                                    </i>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-12">
                                <div class="form-group">
                                    <input type="text" class="form-control lastname" placeholder="Họ" name="lastname" >
                                    <i class="input-icon"><img src="{{url('/pms/images/ico_fullname_signup.svg')}}">
                                    </i>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <input type="email" class="form-control email" name="email" placeholder="Địa chỉ email" >
                            <i class="input-icon">
                                <img src="{{url('/pms/images/ico_email_login_form.svg')}}">
                            </i>

                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control password" name="password" placeholder="Mật khẩu" >
                            <i class="input-icon">
                                <img src="{{url('/pms/images/ico_pass_login_form.svg')}}">
                            </i>
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control password_confirmation" placeholder="Nhập Lại Mật khẩu" id="password_confirmation" name="password_confirmation" >
                            <i class="input-icon">
                                <img src="{{url('/pms/images/ico_pass_login_form.svg')}}">
                            </i>
                        </div>
                        <div class="form-group">
                            <label class="ferm_checkbox">
                                <input type="checkbox" name="checkbox" required class="approved">
                                Tôi đã đọc và chấp nhận <span class="span__name"><a href="{{ route('termandprivacypolicy.index') }}">Điều khoản và Chính sách bảo mật</a></span>
                            </label>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary form-submit btn-dangky">
                                Đăng ký
                            </button>
                        </div>
                        <div class="advanced">
                            <p class="text-center">
                                hoặc tiếp tục với
                            </p>
                            <div class="row">
                                <div class="col-xs-12 text-center">
                                    <a href="#" class="btn btn-facebok">
                                        <i class="fab fa-facebook-f"></i>
                                        Facebook
                                    </a>
                                    <a href="#" class="btn btn-google">
                                        <i class="fab fa-google-plus-g "></i>
                                        Google
                                    </a>
                                </div>
                            </div>
                        </div>
                        {{-- #singUp --}}

                </form>
            </div>
        </div>
    </div>
</div>


