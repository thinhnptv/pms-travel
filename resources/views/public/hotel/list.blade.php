@extends('public.master')
@section('content')
    <style>
        .is-favourite{
            color: red;
        }
        .is-favourite-color{
            color: red;
        }
    </style>
    <main id="main">
        <section class="section-page section-page__2">
            <div class="bs-container">
                <div class="bs-row">
                    <div class="bs-col">
                        <div class="module module-page">
                            <div class="module-header">
                                <h2 class="title">
                                    <span class="span__name black">Được gợi ý ở Hà Nội</span>
                                    <p class="p__info">
                                        Những trải nghiệm hàng đầu ở Hà Nội để bạn bắt đầu
                                    </p>
                                </h2>
                                <div class="page__right">
                                    <ul class="item">
                                        <li class="item-list">
                                            <a href="/" class="item-list__links">
                                                Trang chủ
                                            </a>
                                        </li>
                                        <li class="item-list">
                                            <a href="tours.html" class="item-list__links">
                                                Khách sạn
                                            </a>
                                        </li>
                                        <li class="item-list">
                                            <span class="item-list__links">
                                                Hà Nội
                                            </span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    <form class="form-filter-hotel" action="{{ route('hotel.ds') }}" method="GET">
        <section class="section-rooms section-list">
            <div class="bs-container">
                <div class="grid">
                    <div class="filterMobile">
                        <i class="fas fa-filter"></i>
                        <i class="fas fa-times"></i>
                    </div>

                        <div class="filterShow grid-sidebar">
                            <div class="from__search">
                                <input type="text" placeholder="Nhập tên nhà nghỉ, khách sạn ..." class="input__search" name="title" value="{{ Request::get('title') }}">
                                <button class="btn__search">
                                    <i class="fas fa-search"></i>
                                </button>
                            </div>
                            <div class="search">



                                    <div class="form-group">
                                        <label class="lable__name bold" for="">Nơi đến</label>
                                        <div class="select">
                                            <input type="hidden" name="location_id" class="location_id" value="{{ Request::get('location_id') }}">
                                            <div class="header-option header-option-location">
                                                <img src="{{url('/pms/images/icon_map.png')}}" class="img__option" alt="icon_map.png">
                                                <span class="span__option medium">
                                                    @if (!empty(Request::get('location_id')))
                                                        @foreach ($location as $item)
                                                            @if($item->id == Request::get('location_id'))
                                                                {{ $item->name }}
                                                            @endif
                                                        @endforeach
                                                    @else
                                                        --Chọn nơi đến--
                                                    @endif

                                                </span>
                                            </div>
                                            <div class="body-option">
                                                @foreach ($location as $item)
                                                    <div data-id="{{ $item->id }}" class="item-option">
                                                        <img src="{{url('/pms/images/icon_map.png')}}" class="img__option" alt="icon_map.png">
                                                        <span class="span__option medium">
                                                            {{ $item->name }}
                                                        </span>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="lable__name bold">Check in / Check out</label>
                                        <div class="select">
                                            <div class="header-option">
                                                <img src="{{url('/pms/images/icon_check.png')}}" class="img__option" alt="icon_map.png">
                                                <input type="text" class="" name="daterange"  value="{{ Request::get('daterange')}}" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    {{-- <div class="form-group">
                                        <label class="lable__name bold" for="">Số lượng khách</label>
                                        <div class="select">
                                            <input type="hidden" name="people" class="people" value="{{ Request::get('people') }}">
                                            <div class="header-option">
                                                <img src="{{url('/pms/images/icon_map.png')}}" class="img__option" alt="icon_map.png">
                                                <span class="span__option medium">
                                                    TP.Huế
                                                </span>
                                            </div>
                                        </div>
                                    </div> --}}

                                    <button type="submit" class="button__search bold">
                                        Tìm kiếm
                                    </button>

                            </div>
                            <div class="evaluate">
                                <div class="onCheckRaido item">
                                    <div class="item-header">
                                        <h3 class="title bold">
                                            Điểm đánh giá
                                        </h3>
                                    </div>
                                    <div class="item-content">
                                            <label class="lable__radio lable_category {{ Request::get('star') == 1 ? "active" : ''}}">
                                                <span class="span__radio">
                                                    1 </br>
                                                    Sao
                                                </span>
                                                <input type="radio" class="input__radio" name="star" value="1" {{ Request::get('star') == 1 ? "checked" : ''}}>
                                            </label>
                                            <label class="lable__radio lable_category {{ Request::get('star') == 2 ? "active" : ''}}">
                                                <span class="span__radio">
                                                    2 </br>
                                                    Sao
                                                </span>
                                                <input type="radio" class="input__radio" name="star" value="2" {{ Request::get('star') == 2 ? "checked" : ''}}>
                                            </label>
                                            <label class="lable__radio lable_category {{ Request::get('star') == 3 ? "active" : ''}}">
                                                <span class="span__radio">
                                                    3 </br>
                                                    Sao
                                                </span>
                                                <input type="radio" class="input__radio" name="star" value="3" {{ Request::get('star') == 3 ? "checked" : ''}}>
                                            </label>
                                            <label class="lable__radio lable_category {{ Request::get('star') == 4 ? "active" : ''}}">
                                                <span class="span__radio">
                                                    4 </br>
                                                    Sao
                                                </span>
                                                <input type="radio" class="input__radio" name="star" value="4" {{ Request::get('star') == 4 ? "checked" : ''}}>
                                            </label>
                                            <label class="lable__radio lable_category {{ Request::get('star') == 5 ? "active" : ''}}">
                                                <span class="span__radio">
                                                    5 </br>
                                                    Sao
                                                </span>
                                                <input type="radio" class="input__radio" name="star" value="5" {{ Request::get('star') == 5 ? "checked" : ''}}>
                                            </label>
                                    </div>
                                </div>
                                <div class="onCheckRaido price">
                                    <div class="item-header">
                                        <h3 class="title bold">
                                            Chọn khoảng giá
                                        </h3>
                                    </div>
                                    <div class="item-content">

                                            <label class="lable__radio lable_category {{ Request::get('price_range') == 1 ? 'active' : ''}}">
                                                <span class="span__radio">
                                                    <b class="bold">0</b> vnđ - <b class="bold">5</b> triệu vnđ
                                                </span>
                                                <input type="radio" class="input__radio" value="1" name="price_range" {{ Request::get('price_range') == 1 ? 'checked' : ''}}>
                                            </label>
                                            <label class="lable__radio lable_category {{ Request::get('price_range') == 2 ? 'active' : ''}}">
                                                <span class="span__radio">
                                                    <b class="bold">5</b> triệu vnđ - <b class="bold">10</b> triệu vnđ
                                                </span>
                                                <input type="radio" class="input__radio" value="2" name="price_range" {{ Request::get('price_range') == 2 ? 'checked' : ''}}>
                                            </label>
                                            <label class="lable__radio lable_category {{ Request::get('price_range') == 3 ? 'active' : ''}}">
                                                <span class="span__radio">
                                                    <b class="bold">10</b> triệu vnđ - <b class="bold">15</b> triệu vnđ
                                                </span>
                                                <input type="radio" class="input__radio" value="3" name="price_range" {{ Request::get('price_range') == 3 ? 'checked' : ''}}>
                                            </label>

                                    </div>
                                </div>
                                <div class="onCheckRaido price date">
                                    <div class="item-header">
                                        <h3 class="title bold">
                                                    Property type
                                            <i class="fas fa-chevron-up"></i>
                                        </h3>
                                    </div>
                                    <div class="item-content">
                                            @foreach ($property as $item)
                                                @if (!empty(Request::get('property')) && in_array($item->id, Request::get('property')))
                                                    <label data-id="{{ $item->id }}" class="lable__radio property-checkbox active">
                                                        <span class="span__radio">
                                                            {{ $item->name }}
                                                        </span>

                                                    </label>
                                                @else
                                                    <label data-id="{{ $item->id }}" class="lable__radio property-checkbox">
                                                        <span class="span__radio">
                                                            {{ $item->name }}
                                                        </span>

                                                    </label>
                                                @endif
                                            @endforeach

                                    </div>
                                    <div>
                                        @foreach ($property as $item)
                                            @if (!empty(Request::get('property')) && in_array($item->id, Request::get('property')))
                                                <input type="checkbox" id="property-checkbox-{{ $item->id }}" class="input__radio" name="property[]" value="{{ $item->id }}" checked>
                                            @else
                                                <input type="checkbox" id="property-checkbox-{{ $item->id }}" class="input__radio" name="property[]" value="{{ $item->id }}">
                                            @endif
                                        @endforeach
                                    </div>
                                </div>
                                <div class="onCheckRaido price date">
                                    <div class="item-header">
                                        <h3 class="title bold">
                                            Facilities
                                            <i class="fas fa-chevron-up"></i>
                                        </h3>
                                    </div>
                                    <div class="item-content">
                                        @foreach ($facility as $item)
                                            @if (!empty(Request::get('facility')) && in_array($item->id, Request::get('facility')))
                                                <label data-id="{{ $item->id }}" class="lable__radio facility-checkbox active">
                                                    <span class="span__radio">
                                                        {{ $item->name }}
                                                    </span>

                                                </label>
                                            @else
                                                <label data-id="{{ $item->id }}" class="lable__radio facility-checkbox">
                                                    <span class="span__radio">
                                                        {{ $item->name }}
                                                    </span>

                                                </label>
                                            @endif
                                        @endforeach
                                    </div>
                                    <div>
                                        @foreach ($facility as $item)
                                            @if (!empty(Request::get('facility')) && in_array($item->id, Request::get('facility')))
                                                <input type="checkbox" id="facility-checkbox-{{ $item->id }}" class="input__radio" name="facility[]" value="{{ $item->id }}" checked>
                                            @else
                                                <input type="checkbox" id="facility-checkbox-{{ $item->id }}" class="input__radio" name="facility[]" value="{{ $item->id }}">
                                            @endif
                                        @endforeach
                                    </div>
                                </div>
                                <div class="onCheckRaido price date">
                                    <div class="item-header">
                                        <h3 class="title bold">
                                                Hotel Service
                                            <i class="fas fa-chevron-up"></i>
                                        </h3>
                                    </div>
                                    <div class="item-content">
                                        @foreach ($service as $item)
                                            @if (!empty(Request::get('service')) && in_array($item->id, Request::get('service')))
                                                    <label data-id="{{ $item->id }}" class="lable__radio service-checkbox active">
                                                        <span class="span__radio">
                                                            {{ $item->name }}
                                                        </span>

                                                    </label>
                                            @else
                                                    <label data-id="{{ $item->id }}" class="lable__radio service-checkbox">
                                                        <span class="span__radio">
                                                            {{ $item->name }}
                                                        </span>

                                                    </label>
                                            @endif
                                        @endforeach
                                    </div>
                                    <div>
                                        @foreach ($service as $item)
                                            @if (!empty(Request::get('service')) && in_array($item->id, Request::get('service')))
                                                <input type="checkbox" id="service-checkbox-{{ $item->id }}" class="input__radio" name="service[]" value="{{ $item->id }}" checked>
                                            @else
                                                <input type="checkbox" id="service-checkbox-{{ $item->id }}" class="input__radio" name="service[]" value="{{ $item->id }}">
                                            @endif
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="grid-main">
                            <div class="filter">
                                <div class="filter-price">
                                    <div class="control-filter">
                                        <span class="span__name bold">
                                            Xem theo:
                                        </span>
                                        {{-- <select class="select__text" name="location_id_1"  onchange="submitForm()">
                                            <option value="">--Chọn khu vực--</option>
                                            @foreach ($location as $item)
                                                    <option value="{{ $item->id }}" {{ Request::get('location_id_1') == $item->id ? 'selected' : '' }}>{{ $item->name }}</option>
                                            @endforeach
                                        </select> --}}
                                        <select class="select__text " name="price"  onchange="submitForm()">
                                            <option value="">Chọn giá tiền</option>
                                            <option value="1" {{ Request::get('price') == 1 ? 'selected' : '' }}>Thấp đến cao</option>
                                            <option value="2" {{ Request::get('price') == 2 ? 'selected' : '' }}>Cao đến thấp</option>
                                            <option value="3" {{ Request::get('price') == 3 ? 'selected' : '' }}>Khuyến mãi</option>
                                        </select>
                                    </div>
                                </div>
    </form>
                   {{-- @php
                       dd($listHotel);
                   @endphp --}}
                            <div class="filter-list">
                                <p class="p__text">
                                    <b class="bold"> {{ $listHotel->firstItem().'-'. $listHotel->lastItem() }} </b>  trong tổng số <b class="bold">{{ $listHotel->total() }}</b> khách sạn
                                </p>
                                <i class="fas fa-th-large"></i>
                                <i class="fas fa-th-list active"></i>
                            </div>
                        </div>
                        <div class="grid-main__content onDisplay">
                            {{-- @php
                                dd($listHotel);
                            @endphp --}}
                            @foreach ($listHotel as $item)
                                <div class="itemTours">
                                    <div class="bs-row">
                                        <div class="open-show__avata bs-col lg-33 md-33 sm-33 xs-50 tn-100">
                                            <div class="avata">
                                                <div class="slider-for">
                                                    @foreach (json_decode($item->album) as $items)
                                                        <div class="item">
                                                            <a href="{{ route('hotel.ct', [$item->slug, $item->id]) }}" class="item__links"></a>
                                                            <img src="{{url('/')}}{{ $items }}" alt="images/rooms_1.jpg"
                                                                class="img__links">
                                                        </div>
                                                    @endforeach
                                                </div>
                                                <span class="span__sale">
                                                    <span class="span__number">
                                                        @if (!empty($item->hotel_sale) && $item->hotel_sale < $item->price)
                                                                {{ round(($item->hotel_sale / $item->price) *100) }} %
                                                        @endif
                                                    </span>
                                                </span>
                                                <a class="span___love" onclick="Wishlisthotel({{ $item->id }})">
                                                    <i class="fas fa-heart {{ Auth::check() ? ($item->wishlistHotelBy(auth()->user()) ? 'is-favourite' : ' ') : ' ' }}" {{ Auth::check() == false ? ' modal-show=show modal-data=#login ' : ' ' }}id="color{{ $item->id }}"></i>
                                                    <span class="favourite {{ Auth::check() ? ($item->wishlistHotelBy(auth()->user()) ? 'is-favourite-color' : ' '): ' ' }}" {{ Auth::check() == false ? ' modal-show=show modal-data=#login ' : ' ' }} id="{{ $item->id }}">{{ sizeOf($item->wishListHotel) }}</span>
                                                </a>
                                            </div>
                                            <div class="slider-nav">
                                                @foreach (json_decode($item->album) as $items)
                                                    <div class="item">
                                                        <img src="{{url('/')}}{{ $items }}" alt="{{url('/')}}{{ $items }}" class="img__links">
                                                    </div>
                                                @endforeach


                                            </div>
                                        </div>
                                        <div class="open-show__content bs-col lg-66 md-66 sm-66 xs-50 tn-100">
                                            <div class="content">
                                                <div class="content-header">
                                                    <h2 class="title">
                                                        <a href="{{ route('hotel.ct', [$item->slug, $item->id]) }}" class="title__links">
                                                            <span class="span__name medium">
                                                                {{ $item->name }}
                                                            </span>
                                                            <p class="p__text">
                                                                <img class="img__link" src="{{url('/pms/images/icon_map.png')}}"
                                                                    alt="images/icon_map.png">
                                                                    {{ $item->location->name }}
                                                            </p>
                                                        </a>
                                                    </h2>
                                                    <p class="p__price">
                                                        <span class="span__name bold">
                                                            Gía chỉ từ:
                                                        </span>
                                                        <span class="span__price bold">
                                                            @if (!$item->room->isEmpty())
                                                            {{ number_format($item->room[0]['price']) }}₫<sub class="sub__text">/đêm</sub>
                                                            @endif
                                                            {{-- @if (!empty($item->hotel_sale) && $item->hotel_sale < $item->price)
                                                                {{ number_format($item->hotel_sale) }}₫<sub class="sub__text">/đêm</sub>
                                                            @else
                                                                {{ number_format($item->price) }}₫<sub class="sub__text">/đêm</sub> --}}
                                                            {{-- @endif --}}
                                                        </span>
                                                    </p>
                                                </div>
                                                <div class="content-footer">
                                                    @if (!$item->room->isEmpty())

                                                    <div class="text text__hidden">
                                                        <p class="p__time">
                                                            <span class="span__name medium">
                                                                {{ $item->room[0]['name'] }}
                                                            </span>
                                                            <img src="{{url('/pms/images/icon_user_2.png')}}" alt="icon_user_2s.png"
                                                                class="icon">
                                                        </p>
                                                    </div>
                                                    <div class="text text__hidden">
                                                        <p class="p__time">
                                                            {{ $item->room[0]['number_of_beds'] + $item->room[0]['number_of_bed'] }} giường ({{ $item->room[0]['number_of_beds'] }} giường đôi lớn, {{ $item->room[0]['number_of_bed'] }} giường đơn)
                                                        </p>
                                                    </div>
                                                    <div class="text text__hidden">
                                                        <p class="p__text">
                                                            Chỉ còn {{ $item->room[0]['number_of_room'] }} phòng như này
                                                        </p>
                                                    </div>
                                                    @endif

                                                    <div class="like-dvaluate">
                                                        <p class="p__like">
                                                            <span class="span__star">
                                                                @for ($i = 0; $i < $item->avgReview; $i++)
                                                                    <i class="fas fa-star"></i>
                                                                @endfor
                                                            </span>
                                                        </p>
                                                        <p class="p__like medium">
                                                            {{ sizeof($item->review) }} đánh giá
                                                        </p>
                                                    </div>

                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                            <div class="fexPagintaion text-center clearfix">
                                {{ $listHotel->links() }}
                            </div>
                        </div>
                    </div>
                </div>
        </section>
        <section class="section-domestic">
        <div class="bs-container">
            <div class="bs-row">
                <div class="bs-col">
                    <div class="module module-domestic">
                        <div class="module-header">
                            <h2 class="title">
                                Điểm đến yêu thích trong nước
                            </h2>
                            <p class="p__info">
                                Bao la khắp nước. Bốn bể là nhà
                            </p>
                        </div>
                        <div class="module-content">
                            <div class="grid">
                                <div class="grid-item">
                                    <a href="#" class="item__links">
                                        <div class="avata">
                                            <img src="{{url('/pms/images/domestic_1.png')}}" alt="domestic_1.png">
                                        </div>
                                    </a>
                                    <div class="content">
                                        <h3 class="title">
                                            SAPA
                                        </h3>
                                        <ul class="category">
                                            <li class="category-list">
                                                <a href="#" class="category-list__links">
                                                    340
                                                    <span class="span_name">
                                                        Tours
                                                    </span>
                                                </a>
                                            </li>
                                            <li class="category-list">
                                                <a href="#" class="category-list__links">
                                                    2.500
                                                    <span class="span_name">
                                                        Hotels
                                                    </span>

                                                </a>
                                            </li>
                                            <li class="category-list">
                                                <a href="#" class="category-list__links">
                                                    230 <span class="span_name">Cars</span>
                                                </a>
                                            </li>

                                        </ul>
                                    </div>
                                </div>
                                <div class="grid-item">
                                    <a href="#" class="item__links">
                                        <div class="avata">
                                            <img src="{{url('/pms/images/domestic_2.png')}}" alt="domestic_2.png">
                                        </div>

                                    </a>
                                    <div class="content">
                                        <h3 class="title">
                                            Vịnh hạ long
                                        </h3>
                                        <ul class="category">
                                            <li class="category-list">
                                                <a href="#" class="category-list__links">
                                                    340
                                                    <span class="span_name">
                                                        Tours
                                                    </span>
                                                </a>
                                            </li>
                                            <li class="category-list">
                                                <a href="#" class="category-list__links">
                                                    2.500
                                                    <span class="span_name">
                                                        Hotels
                                                    </span>

                                                </a>
                                            </li>
                                            <li class="category-list">
                                                <a href="#" class="category-list__links">
                                                    230 <span class="span_name">Cars</span>
                                                </a>
                                            </li>

                                        </ul>
                                    </div>
                                </div>
                                <div class="grid-item">
                                    <a href="#" class="item__links">
                                        <div class="avata">
                                            <img src="{{url('/pms/images/domestic_3.png')}}" alt="domestic_3.png">
                                        </div>

                                    </a>
                                    <div class="content">
                                        <h3 class="title">
                                            Đà Nẵng
                                        </h3>
                                        <ul class="category">
                                            <li class="category-list">
                                                <a href="#" class="category-list__links">
                                                    340
                                                    <span class="span_name">
                                                        Tours
                                                    </span>
                                                </a>
                                            </li>
                                            <li class="category-list">
                                                <a href="#" class="category-list__links">
                                                    2.500
                                                    <span class="span_name">
                                                        Hotels
                                                    </span>

                                                </a>
                                            </li>
                                            <li class="category-list">
                                                <a href="#" class="category-list__links">
                                                    230 <span class="span_name">Cars</span>
                                                </a>
                                            </li>

                                        </ul>
                                    </div>
                                </div>
                                <div class="grid-item">
                                    <a href="#" class="item__links">
                                        <div class="avata">
                                            <img src="{{url('/pms/images/domestic_4.png')}}" alt="domestic_4.png">
                                        </div>

                                    </a>
                                    <div class="content">
                                        <h3 class="title">
                                            Đà Lạt
                                        </h3>
                                        <ul class="category">
                                            <li class="category-list">
                                                <a href="#" class="category-list__links">
                                                    340
                                                    <span class="span_name">
                                                        Tours
                                                    </span>
                                                </a>
                                            </li>
                                            <li class="category-list">
                                                <a href="#" class="category-list__links">
                                                    2.500
                                                    <span class="span_name">
                                                        Hotels
                                                    </span>

                                                </a>
                                            </li>
                                            <li class="category-list">
                                                <a href="#" class="category-list__links">
                                                    230 <span class="span_name">Cars</span>
                                                </a>
                                            </li>

                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    </main>
    <script type="text/javascript">
        function Wishlisthotel(id) {
            $.ajax({
                type: "GET",
                url: "/hotel-favourite/"+id ,
                success: function (response) {
                    $('#'+id).html(response.countFavourite).css('color',response.color);
                    $('#color'+id).css('color',response.color);
            }
        });
        }
        $(document).ready(function () {
            function countdown() {
                $('.clock').countdown('2020/10/03', function (event) {
                    $(this).html(event.strftime(' <span> %H</span> Giờ <span> :%M </span> Phút <span>:%S </span> Giây'));
                });
            }
            function checkRadido() {
                $('.lable_category').click(function () {
                    $(this).prevAll('.lable_category').removeClass('active');
                    $(this).nextAll('.lable_category').removeClass('active');
                    $(this).addClass('active');
                    submitForm()
                });

            }
            // hien thi danh sach turs
           
                
                
            // hien thi danh sach turs 
            function onDisplay() {
                $('.fa-th-large').click(function () {
                    $('.fa-th-list').removeClass('active');
                    $(this).addClass('active');
                    $('.onDisplay').addClass('active');
                    $('.open-show__avata').removeClass('lg-33 md-33 sm-33 xs-50 ');
                    $('.open-show__content').removeClass('lg-66 md-66 sm-33 xs-50');
                    $('.open-show__avata').addClass('lg-100 md-100 sm-100 xs-100');
                    $('.open-show__content').addClass('lg-100 md-100 sm-100 xs-100');
                })
                $('.fa-th-list').click(function () {
                    $('.onDisplay').removeClass('active');
                    $('.fa-th-large').removeClass('active');
                    $(this).addClass('active');
                    $('.open-show__avata').removeClass('lg-100 md-100 sm-100 xs-100');
                    $('.open-show__content').removeClass('lg-100 md-100 sm-100 xs-100');
                    $('.open-show__avata').addClass('lg-33 md-33 sm-33 xs-50');
                    $('.open-show__content').addClass('lg-66 md-66 sm-33 xs-50');
                });
            }

            function selecOption() {
                $('.select .header-option-location').click(function () {
                    $(this).next('.body-option').toggleClass('active');
                    var header = $(this);
                    $(this).next('.body-option').children('.item-option').click(function () {
                        var html = $(this).html();
                        var id = $(this).attr('data-id');
                        header.html(html);
                        header.parent('.select').children('.location_id').val(id);
                        $('.body-option').removeClass('active');
                    });

                });

                // $('.select .header-option').click(function () {
                //     $(this).next('.body-option').toggleClass('active');
                //     var header = $(this);
                //     $(this).next('.body-option').children('.item-option').click(function () {
                //         var html = $(this).html();
                //         header.html(html);
                //         $('.body-option').removeClass('active');
                //     });

                // });
            }
            function slideProduct() {
                $('.slider-for').slick({
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    arrows: false,
                    fade: false,
                    asNavFor: '.slider-nav'
                });
                $('.slider-nav').slick({
                    slidesToShow: 4,
                    slidesToScroll: 1,
                    asNavFor: '.slider-for',
                    focusOnSelect: true,
                });
            }
            slideProduct();
            onDisplay();
            selecOption();
            countdown();

        });
        $(document).on('click', '.lable_category', function(e){
            e.preventDefault();
                    
                    if($(this).hasClass('active')) {
                        $(this).removeClass('active');
                        $(this).prevAll('.lable_category').removeClass('active');
                        $(this).nextAll('.lable_category').removeClass('active');
                        console.log(1, $(this).children('.input__radio'));
                        
                        $(this).children('.input__radio').prop('checked', false);
                    } else {
                        $(this).prevAll('.lable_category').removeClass('active');
                        $(this).nextAll('.lable_category').removeClass('active');
                        $(this).addClass('active');
                        console.log(2, $(this).children('.input__radio'));
                        $(this).children('.input__radio').prop('checked', true);
                    }
                    
                    submitForm()
                });
        function submitForm(){
                $('.form-filter-hotel').submit();
            };
        $(document).on('click', '.property-checkbox', function(e) {
            var id = $(this).attr('data-id');
            if($(this).hasClass('active')) {
                $(this).removeClass('active');
                $('#property-checkbox-'+id).prop('checked', false);
            }else{
                $(this).addClass('active');
                $('#property-checkbox-'+id).prop('checked', true);
            }
            submitForm()
        })
        $(document).on('click', '.facility-checkbox', function(e) {
            var id = $(this).attr('data-id');
            if($(this).hasClass('active')) {
                $(this).removeClass('active');
                $('#facility-checkbox-'+id).prop('checked', false);
            }else{
                $(this).addClass('active');
                $('#facility-checkbox-'+id).prop('checked', true);
            }
            submitForm()
        })
        $(document).on('click', '.service-checkbox', function(e) {
            var id = $(this).attr('data-id');
            if($(this).hasClass('active')) {
                $(this).removeClass('active');
                $('#service-checkbox-'+id).prop('checked', false);
            }else{
                $(this).addClass('active');
                $('#service-checkbox-'+id).prop('checked', true);
            }
            submitForm()
        })
    </script>
@endsection

