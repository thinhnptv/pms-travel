@extends('public.master')
@section('content')
<style>
    .is-favourite{
        color: red;
    }
    .is-favourite-color{
        color: red;
    }
</style>

<main id="main">
    <section class="section-page">
        <div class="bs-container">
            <div class="bs-row">
                <div class="bs-col">
                    <div class="module module-page">
                        <div class="module-header">
                            <h2 class="title">
                                <span class="span__name black">
                                    Tìm kiếm ưu đãi khách sạn, chỗ nghỉ & nhiều hơn nữa...
                                </span>

                            </h2>
                            <div class="page__right">
                                <ul class="item">
                                    <li class="item-list">
                                        <a href="/" class="item-list__links">
                                            Trang chủ
                                        </a>
                                    </li>
                                    <li class="item-list">
                                        <span class="item-list__links">
                                            Rooms
                                        </span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="module-content">
                            <form class="form-filter-hotel" action="{{ route('hotel.ds') }}" method="GET">
                                <div class="grid">
                                    <div class="item-grid">
                                        <div class="item-grid__header  clickShow">
                                            <div class="item_icon">
                                                <img src="{{url('/pms/images/icon_map.png')}}" alt="icon_map.png">
                                            </div>
                                            <div class="item__content">
                                                <h3 class="title">
                                                    Nơi đến
                                                </h3>
                                                <p class="p__text addText">
                                                    <input type="text" class="text-location" placeholder="Bạn muốn đi đâu" readonly>
                                                    <input type="hidden" name="location_id" class="location_id" >
                                                </p>
                                                <i class="fas fa-caret-down"></i>
                                            </div>
                                        </div>
                                        <div class="item-grid__content">

                                            <ul class="item">
                                                @foreach ($location as $item)
                                                    <div data-id="{{ $item->id }}" class="item-option item-option-location">
                                                        <li class="item-list clickDestination">
                                                            <i class="fas fa-map-marker-alt"></i>
                                                            <span class="span__name ">
                                                                {{ $item->name }}
                                                            </span>
                                                        </li>
                                                    </div>
                                                @endforeach
                                        </div>
                                    </div>
                                    <div class="item-grid">
                                        <div class="item-grid__header">
                                            <div class="item_icon">
                                                <img src="{{url('/pms/images/icon_check.png')}}" alt="icon_check.png">
                                            </div>
                                            <div class="item__content">
                                                <h3 class="title">
                                                    Check In - Check Out
                                                </h3>
                                                <p class="p__text acDates">

                                                    <input type="text" name="daterange" readonly/>
                                                </p>
                                                <i class="fas fa-caret-down"></i>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="item-grid">
                                        {{-- <div class="item-grid__header clickShow">
                                            <div class="item_icon">
                                                <img src="{{url('/pms/images/icon_us.png')}}" alt="icon_us.png">
                                            </div>
                                            <div class="item__content">
                                                <h3 class="title">
                                                    Số lượng khách
                                                </h3>
                                                <p class="p__text">
                                                    <input type="text" placeholder="2 Người lớn - 1 Trẻ em">
                                                </p>
                                                <i class="fas fa-caret-down"></i>
                                            </div>
                                        </div>
                                        <div class="item-grid__content">
                                            <ul class="item">
                                                <li class="item-list">
                                                    <span class="span__name bold ">
                                                        Phòng
                                                    </span>
                                                    <div class="number">
                                                        <span class="span span__subtract bold">
                                                            -
                                                        </span>
                                                        <span class="span span__show">
                                                            1
                                                        </span>
                                                        <span class="span span__add bold">
                                                            +
                                                        </span>
                                                    </div>
                                                </li>
                                                <li class="item-list">
                                                    <span class="span__name bold">
                                                        Người lớn
                                                    </span>
                                                    <div class="number">
                                                        <span class="span span__subtract bold" data-people="adults">
                                                            -
                                                        </span>
                                                        <span class="span span__show">
                                                            1
                                                        </span>
                                                        <span class="span span__add bold " data-people="adults">
                                                            +
                                                        </span>
                                                    </div>
                                                </li>
                                                <li class="item-list">
                                                    <span class="span__name bold">
                                                        Trẻ em

                                                    </span>
                                                    <div class="number">
                                                        <span class="span span__subtract bold" data-people="children">
                                                            -
                                                        </span>
                                                        <span class="span span__show">
                                                            0
                                                        </span>
                                                        <span class="span span__add bold" data-people="children">
                                                            +
                                                        </span>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div> --}}
                                    </div>
                                    <div class="item-grid">
                                        <button class="button">
                                            <img src="{{url('/pms/images/icon_search.png')}}" alt="icon_search.png">
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="section-resort">
        <div class="bs-container">
            <div class="bs-row">
                <div class="bs-col">
                    <div class="module module-resort">
                        <div class="module-header">
                            <h2 class="title">
                                loại hình nghỉ dưỡng
                            </h2>
                        </div>
                        <div class="module-content">
                            <div class="slide-resort">
                                @foreach ($listType as $item)

                                    <div class="item">
                                        <div class="resort">
                                            <a href="#" class="link"></a>
                                            <div class="avata">
                                                <img src="{{ $item->avatar }}" alt="resort_1.jpg">
                                            </div>
                                            <div class="content">
                                                <h3 class="h3__title black">
                                                    {{ $item->name }}
                                                </h3>
                                                <p class="p__text black">
                                                    {{ sizeof($item->hotel) }} khách sạn
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </section>
    <section class="section-favourite">
        <div class="bs-container">
            <div class="bs-row">
                <div class="bs-col">
                    <div class="module module-favourite">
                        <div class="module-header">
                            <h2 class="title">
                                Nơi ở mà khách yêu thích
                            </h2>
                            {{-- <p class="p__click">
                                Để xem tất cả nơi ở được yêu thích, vui lòng
                                <a href="#" class="click__links">
                                    click vào đây
                                </a>
                            </p> --}}
                        </div>
                        <div class="module-content">
                            <div class="bs-row row-lg-10 row-md-10 row-sm-5 row-xs-5 row-tn">
                                @foreach ($listLike as $item)

                                    <div class="bs-col lg-25-10 md-25-10 sm-33-5 xs-50-5 tn-100">
                                        <div class="favourite">
                                            <a href="{{ route('hotel.ct', [$item->slug, $item->id]) }}" class="item__links">
                                            <div class="avata">
                                                <a class="span__love" onclick="Wishlisthotel({{ $item->id }})">
                                                    <i class="fas fa-heart {{ Auth::check() ? ($item->wishlistHotelBy(auth()->user()) ? 'is-favourite' : ' '): ' ' }}" {{ Auth::check() == false ? ' modal-show=show modal-data=#login ' : ' ' }} id="color{{ $item->id }}"></i>
                                                    <span class="favourite {{ Auth::check() ? ($item->wishlistHotelBy(auth()->user()) ? 'is-favourite-color' : ' '): ' ' }}" {{ Auth::check() == false ? ' modal-show=show modal-data=#login ' : ' ' }} id="{{ $item->id }}">{{ sizeOf($item->wishListHotel) }}</span>
                                                </a>
                                                <span class="span__sale">
                                                    <span class="number">
                                                        @if (!empty($item->hotel_sale) && $item->hotel_sale < $item->price)
                                                            {{ round((($item->price - $item->hotel_sale) / $item->price) *100) }} %
                                                        @endif
                                                    </span>
                                                </span>

                                                
                                                <img src="{{ $item->feature }}" class="img__links" alt="favourite_1">
                                            </div>
                                        </a>
                                            <div class="content">
                                                <h3 class="title">
                                                    <a href="{{ route('hotel.ct', [$item->slug, $item->id]) }}" class="title__links medium">
                                                        {{ $item->name }}
                                                    </a>
                                                </h3>
                                                <p class="p__text">
                                                    Giá chỉ từ
                                                    <span class="span__name">
                                                        @if (!empty($item->hotel_sale) && $item->hotel_sale < $item->price)
                                                            {{ number_format($item->hotel_sale) }}₫
                                                        @else
                                                            {{ number_format($item->price) }}₫
                                                        @endif

                                                    </span>
                                                    /đêm
                                                </p>
                                                {{-- @php
                                                    dd($item->evaluate_star);
                                                @endphp --}}
                                                <div class="content__footer">
                                                    <p class="p__left">
                                                        <img src="{{url('/pms/images/icon_map.png')}}" alt="icon_map.png" class="map_img">
                                                        <span class="span__namee">
                                                            {{ $item->location->name }}
                                                        </span>
                                                    </p>
                                                    <p class="p__right medium">
                                                        <span class="span__star">
                                                            @for ($i = 0; $i < $item->evaluate_star; $i++)
                                                                <i class="fas fa-star"></i>
                                                            @endfor
                                                        </span>
                                                        <span class="span__number">{{ sizeof($item->review) }}</span> Đánh giá
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="section-domestic">
    <div class="bs-container">
        <div class="bs-row">
            <div class="bs-col">
                <div class="module module-domestic">
                    <div class="module-header">
                        <h2 class="title">
                            Điểm đến yêu thích trong nước
                        </h2>
                        <p class="p__info">
                            Bao la khắp nước. Bốn bể là nhà
                        </p>
                    </div>
                    <div class="module-content">
                        <div class="grid">
                            <div class="grid-item">
                                <a href="#" class="item__links">
                                    <div class="avata">
                                        <img src="{{url('/pms/images/domestic_1.png')}}" alt="domestic_1.png">
                                    </div>
                                </a>
                                <div class="content">
                                    <h3 class="title">
                                        SAPA
                                    </h3>
                                    <ul class="category">
                                        <li class="category-list">
                                            <a href="#" class="category-list__links">
                                                340
                                                <span class="span_name">
                                                    Tours
                                                </span>
                                            </a>
                                        </li>
                                        <li class="category-list">
                                            <a href="#" class="category-list__links">
                                                2.500
                                                <span class="span_name">
                                                    Hotels
                                                </span>

                                            </a>
                                        </li>
                                        <li class="category-list">
                                            <a href="#" class="category-list__links">
                                                230 <span class="span_name">Cars</span>
                                            </a>
                                        </li>

                                    </ul>
                                </div>
                            </div>
                            <div class="grid-item">
                                <a href="#" class="item__links">
                                    <div class="avata">
                                        <img src="{{url('/pms/images/domestic_2.png')}}" alt="domestic_2.png">
                                    </div>

                                </a>
                                <div class="content">
                                    <h3 class="title">
                                        Vịnh hạ long
                                    </h3>
                                    <ul class="category">
                                        <li class="category-list">
                                            <a href="#" class="category-list__links">
                                                340
                                                <span class="span_name">
                                                    Tours
                                                </span>
                                            </a>
                                        </li>
                                        <li class="category-list">
                                            <a href="#" class="category-list__links">
                                                2.500
                                                <span class="span_name">
                                                    Hotels
                                                </span>

                                            </a>
                                        </li>
                                        <li class="category-list">
                                            <a href="#" class="category-list__links">
                                                230 <span class="span_name">Cars</span>
                                            </a>
                                        </li>

                                    </ul>
                                </div>
                            </div>
                            <div class="grid-item">
                                <a href="#" class="item__links">
                                    <div class="avata">
                                        <img src="{{url('/pms/images/domestic_3.png')}}" alt="domestic_3.png">
                                    </div>

                                </a>
                                <div class="content">
                                    <h3 class="title">
                                        Đà Nẵng
                                    </h3>
                                    <ul class="category">
                                        <li class="category-list">
                                            <a href="#" class="category-list__links">
                                                340
                                                <span class="span_name">
                                                    Tours
                                                </span>
                                            </a>
                                        </li>
                                        <li class="category-list">
                                            <a href="#" class="category-list__links">
                                                2.500
                                                <span class="span_name">
                                                    Hotels
                                                </span>

                                            </a>
                                        </li>
                                        <li class="category-list">
                                            <a href="#" class="category-list__links">
                                                230 <span class="span_name">Cars</span>
                                            </a>
                                        </li>

                                    </ul>
                                </div>
                            </div>
                            <div class="grid-item">
                                <a href="#" class="item__links">
                                    <div class="avata">
                                        <img src="{{url('/pms/images/domestic_4.png')}}" alt="domestic_4.png">
                                    </div>

                                </a>
                                <div class="content">
                                    <h3 class="title">
                                        Đà Lạt
                                    </h3>
                                    <ul class="category">
                                        <li class="category-list">
                                            <a href="#" class="category-list__links">
                                                340
                                                <span class="span_name">
                                                    Tours
                                                </span>
                                            </a>
                                        </li>
                                        <li class="category-list">
                                            <a href="#" class="category-list__links">
                                                2.500
                                                <span class="span_name">
                                                    Hotels
                                                </span>

                                            </a>
                                        </li>
                                        <li class="category-list">
                                            <a href="#" class="category-list__links">
                                                230 <span class="span_name">Cars</span>
                                            </a>
                                        </li>

                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
</main>



<script type="text/javascript">
    function Wishlisthotel(id) {
            $.ajax({
                type: "GET",
                url: "/hotel-favourite/"+id ,
                success: function (response) {
                    $('#'+id).html(response.countFavourite).css('color',response.color);
                    $('#color'+id).css('color',response.color);
            }
        });
    }
    $(document).ready(function () {
        function slideResort() {
            $(".slide-resort").slick({
                slidesToShow: 3,
                slidesToScroll: 3,
                adaptiveHeight: false,
                prevArrow: '<i class="icon_control fas fa-angle-left"></i>',
                nextArrow: '<i class="icon_control fas fa-angle-right"></i>',
                arrows: true,
                infinite: false,
                rows: 2,
                responsive: [{

                    breakpoint: 991.9,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1,
                        adaptiveHeight: false,
                        infinite: true,
                        row:2,
                    }
                },

                {

                    breakpoint: 479.9,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        adaptiveHeight: true,
                        infinite: true
                    }
                }

                ]
            });
        }
        slideResort();
    });
    $(document).on('click', '.item-option-location', function() {
        let text = $(this).text().trim();
        let id = $(this).attr('data-id');
        $(".text-location").val(text);
        $(".location_id").val(id);
        $(".item-grid__content").removeClass("active");
    })
</script>

@endsection
