@extends('public.master')
@section('content')

<link rel="stylesheet" href="{{ url('/pms/css/web.css') }}">
<style>
    .is-favourite{
        color: red;
    }
    .is-favourite-color{
        color: red;
    }
</style>


<main id="main">
    <section class="section-page section-detail__page">
        <div class="bs-container">
            <div class="bs-row">
                <div class="bs-col">
                    <div class="module module-page">
                        <div class="module-content">
                            <form class="form-filter-hotel" action="{{ route('hotel.ds') }}" method="GET">
                                <div class="grid">
                                    <div class="item-grid">
                                        <div class="item-grid__header  clickShow">
                                            <div class="item_icon">
                                                <img src="{{url('/pms/images/icon_map.png')}}" alt="icon_map.png">
                                            </div>
                                            <div class="item__content">
                                                <p class="p__text addText">
                                                    <input type="text" class="text-location" placeholder="Bạn muốn đi đâu" readonly>
                                                    <input type="hidden" name="location_id" class="location_id" >
                                                </p>
                                                <i class="fas fa-caret-down"></i>
                                            </div>
                                        </div>
                                        <div class="item-grid__content">
                                            <ul class="item">
                                                @foreach ($location as $item)
                                                    
                                                    <li data-id="{{ $item->id }}" class="item-list item-option-location">
                                                        <i class="fas fa-map-marker-alt"></i>
                                                        <span class="span__name ">
                                                            {{ $item->name }}
                                                        </span>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="item-grid">
                                        {{-- <div class="item-grid__header  clickShow">
                                            <div class="item_icon">
                                                <img src="{{url('/pms/images/icon_map.png')}}" alt="icon_map.png">
                                            </div>
                                            <div class="item__content">
                                                <p class="p__text addText">
                                                    <input type="text" placeholder="Điểm đến">
                                                </p>
                                                <i class="fas fa-caret-down"></i>
                                            </div>
                                        </div>
                                        <div class="item-grid__content">
                                            <ul class="item">
                                                <li class="item-list clickDestination">
                                                    <i class="fas fa-map-marker-alt"></i>
                                                    <span class="span__name ">
                                                        Paris
                                                    </span>
                                                </li>
                                                <li class="item-list clickDestination">
                                                    <i class="fas fa-map-marker-alt"></i>
                                                    <span class="span__name">
                                                        New York, United States
                                                    </span>
                                                </li>
                                                <li class="item-list clickDestination">
                                                    <i class="fas fa-map-marker-alt"></i>
                                                    <span class="span__name">
                                                        California
                                                    </span>
                                                </li>
                                                <li class="item-list clickDestination">
                                                    <i class="fas fa-map-marker-alt"></i>
                                                    <span class="span__name">
                                                        United States
                                                    </span>
                                                </li>
                                                <li class="item-list clickDestination">
                                                    <i class="fas fa-map-marker-alt"></i>
                                                    <span class="span__name">
                                                        Los Angeles
                                                    </span>
                                                </li>
                                            </ul>
                                        </div> --}}
                                    </div>
                                    <div class="item-grid">
                                        <div class="item-grid__header">
                                            <div class="item_icon">
                                                <img src="{{url('/pms/images/icon_check.png')}}" alt="icon_check.png">
                                            </div>
                                            <div class="item__content">
                                                <p class="p__text acDates">

                                                    <input type="text" name="daterange" readonly/>
                                                </p>
                                                <i class="fas fa-caret-down"></i>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="item-grid">
                                        <button class="button black">
                                            Tìm kiếm
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="section-category">
        <div class="bs-container">
            <div class="bs-row">
                <div class="bs-col">
                    <div class="module module-category">
                        <div class="module-header">
                            <ul class="category">
                                <li class="category-list">
                                    <a href="/" class="category-list__link">
                                        Trang chủ
                                    </a>
                                </li>
                                <li class="category-list">
                                    <a href="toru.html" class="category-list__link">
                                        Khách sạn
                                    </a>
                                </li>
                                <li class="category-list">
                                    <a href="toru.html" class="category-list__link">
                                        {{ $detailHotel->location->name }}
                                    </a>
                                </li>
                                <li class="category-list">
                                    <span class="category-list__link">
                                        {{ $detailHotel->name }}
                                    </span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="section-detail">
        <div class="bs-container">
            <div class="bs-row">
                <div class="bs-col">
                    <div class="module module-detail">
                        <div class="module-header clearfix">
                            <div class="header-info">
                                <h2 class="title bold">
                                    {{ $detailHotel->name }}
                                </h2>
                                <p class="p__category">
                                    <span class="span__star">
                                        @for ($i = 0; $i < $detailHotel->avgReview; $i++)
                                            <i class="fas fa-star"></i>
                                        @endfor
                                    </span>
                                    <span class="span__number bold">
                                        {{ $detailHotel->avgReview }}/5
                                    </span>
                                    <span class="span__name bold">
                                        Tuyệt vời
                                    </span>
                                    <span class="span__text">
                                        | {{ sizeof($detailHotel->review) }} đánh giá
                                    </span>
                                </p>
                                <p class="p__address">
                                    <img src="{{url('/pms/images/icon_map.png')}}" class="img__link" alt="icon_map.png">
                                    <span class="span__name">
                                        {{ $detailHotel->location->name }}, {{ $detailHotel->address }}
                                    </span>
                                </p>
                            </div>
                            {{-- @php
                                dd($detailHotel);
                            @endphp --}}
                            <div class="header-buy">
                                <p class="p__name medium">
                                    @if (!empty($detailHotel->hotel_sale) && $detailHotel->hotel_sale < $detailHotel->price)
                                    {{-- {{ number_format($detailHotel->hotel_sale) }}₫<sub class="sub__text">/đêm</sub> --}}
                                    Giá khách sạn:  <span class="span__number black"> {{ number_format($detailHotel->hotel_sale) }}</span> đ
                                    @else 
                                    {{-- {{ number_format($detailHotel->price) }}₫<sub class="sub__text">/đêm</sub> --}}
                                    Giá khách sạn:  <span class="span__number black"> {{ number_format($detailHotel->price) }}</span> đ
                                    @endif
                                </p>
                                <a href="#" class="buy__link black">
                                    Đặt phòng ngay
                                </a>
                            </div>
                        </div>
                        <div class="module-content">
                            <div class="grayimg">
                                <div class="bs-row">
                                    @foreach (json_decode($detailHotel->album) as $item)
                                        <div class="bs-col lg-20 md-20 sm-20 xs-100">
                                                <a href="" class="item__links"></a>
                                                <img src="{{url('/')}}{{ $item }}" alt="images/rooms_1.jpg"
                                                    class="img__links">
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                            <div class="content">
                                <h3 class="title__info">
                                    Mô tả khách sạn
                                </h3>
                                {!! $detailHotel->content !!}
                            </div>
                            <div class="basis">
                                <h3 class="title-basis">
                                    tiện ích của khách sạn
                                </h3>
                                <div class="item">
                                    <ul class="item-basis clearfix">
                                        @foreach ($detailHotel->utilities as $item)
                                            <li class="item-basis__list">
                                                <i class="fas fa-check"></i>
                                                {{ $item->title }}

                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                            <div class="necessary">
                                <h3 class="title-necessary medium">
                                    thông tin cần biết
                                </h3>
                                <p class="p__text">
                                    <span class="span__name medium">
                                        7:00 PM - 7:00 AM:
                                    </span>
                                    Tính chi phí 1 ngày
                                </p>
                                <p class="p__text">
                                    <span class="span__name medium">7:00 AM - 1:00 PM:</span> Tính chi phí 1/2 ngày
                                </p>
                                <p class="p__text">
                                    Các loại phòng khác nhau có thể có chính sách hủy đặt phòng và chính sách thanh toán
                                    trước khác nhau. Vui lòng kiểm tra chi tiết chính sách phòng khi chọn phòng ở phía
                                    trên
                                </p>
                            </div>
                            <div class="question">
                                <h3 class="title-question">
                                    Câu hỏi thường gặp
                                </h3>

                                <div class="panel-group" id="accordion">
                                    @foreach ($detailHotel->policy as $item)
                                        <div class="panel">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" data-parent="#accordion" href="#question-{{ $item->id }}">
                                                        <img src="{{url('/pms/images/icon_message.png')}}" alt="icon_message.png"
                                                            class="icon icon__message">
                                                                {{ $item->title }}
                                                        <i class="fas fa-angle-down"></i>
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="question-{{ $item->id }}" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    {{ $item->content }}
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>

                            </div>
                            <div class="evaluate">
                                <h3 class="title-evaluate">
                                    Đánh giá
                                </h3>
                                <div class="evaluate-container">
                                    <div class="bs-row row-lg-10 row-md-10 row-ms-5 row-xs">
                                        <div class="bs-col lg-25-10 md-25-10 sm-30-5 xs-100">
                                            <div class="item">
                                                <span class="span__number medium">
                                                    5.0<sub>/5</sub>
                                                </span>
                                                <p class="p__text light">
                                                    Tuyệt vời
                                                </p>
                                                <p class="p__evaluate">
                                                    Dựa trên 3 đánh giá
                                                </p>
                                            </div>
                                        </div>
                                        <div class="bs-col lg-75-10 md-75-10 sm-70-5 xs-100">
                                            <div class="evaluate-content">
                                                <p class="p__evaluate active">
                                                    <span class="span__name">
                                                        Tuyệt vời
                                                    </span>
                                                    <span class="span__number">
                                                        3
                                                    </span>
                                                </p>
                                                <p class="p__evaluate">
                                                    <span class="span__name">
                                                        Rất tốt
                                                    </span>
                                                    <span class="span__number">
                                                        0
                                                    </span>
                                                </p>
                                                <p class="p__evaluate">
                                                    <span class="span__name">
                                                        Cũng được
                                                    </span>
                                                    <span class="span__number">
                                                        0
                                                    </span>
                                                </p>
                                                <p class="p__evaluate">
                                                    <span class="span__name">
                                                        Trung bình
                                                    </span>
                                                    <span class="span__number">
                                                        0
                                                    </span>
                                                </p>
                                                <p class="p__evaluate">
                                                    <span class="span__name">
                                                        Kém
                                                    </span>
                                                    <span class="span__number">
                                                        0
                                                    </span>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="comment">
                                @foreach ($review as $item)
                                    <div class="comment-container">
                                        <div class="avata">
                                            <div class="ImagesFrame">
                                                <div class="ImagesFrameCrop0">
                                                    <img src="{{url('/')}}{{ optional($item->user)->avatar }}" alt="">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="content">
                                            <h3 class="title__name bold">
                                                <span class="span__star">
                                                        @for ($i = 0; $i < $item->average; $i++)
                                                            <i class="fas fa-star"></i>
                                                        @endfor
                                                </span>
                                            </h3>
                                            <p class="p__day">
                                                {{ $item->created_at }}
                                            </p>
                                            <p class="p__content">
                                                <b>{{ $item->title }}</b> <br>
                                                {{ $item->content }}
                                            </p>
                                        </div>
                                    </div>
                                @endforeach
                                <div class="fexPagintaion text-center clearfix">
                                    {{ $review->links() }}
                                    
                                </div>
                                <div class="comment-footer">
                                    <p class="p__total">
                                        {{ $review->firstItem().'-'. $review->lastItem() }} Hiển thị tổng số  {{ $review->total() }}  đánh giá
                                    </p>
                                    <p class="p__login">
                                        Bạn cần phải <a href=" {{ route('home') }}" class="span__name">đăng nhập</a> để đánh giá khách sạn
                                    </p>
                                </div>
                            </div>
                            <div class="like">
                                <h3 class="title-like">
                                    Danh sách phòng
                                </h3>
                                @php
                                    // dd($detailHotel->room);
                                @endphp
                                <div class="bs-row row-lg-10 row-md-10 row-sm-5 row-xs-5">
                                    @foreach ($detailHotel->room as $item)
                                    @php
                                        // dd(json_decode($item->album));
                                    @endphp
                                    <div class="bs-col lg-25-10 md-25-10 sm-33-5 xs-50-5 tn-100">
                                        <div class="itemTours">
                                            <div class="avata">
                                                <div class="slider-for">
                                                  @if (json_decode($item->album) != null)
                                                    @foreach (json_decode($item->album) as $items)
                                                        <div class="item">
                                                            <a href="{{ route('hotel.ct', [$item->slug, $item->id]) }}" class="item__links"></a>
                                                            <img src="{{url('/')}}{{ $items }}" alt="images/rooms_1.jpg"
                                                                class="img__links">
                                                        </div>
                                                    @endforeach
                                                  @endif
                                                   
                                                </div>
                                                <span class="span__sale">
                                                    <span class="span__number">
                                                        @if (!empty($item->price_sale) && $item->price_sale < $item->price)
                                                            {{ round((($item->price - $item->price_sale) / $item->price) *100) }} %
                                                        @endif
                                                    </span>
                                                </span>
                                                {{-- <a class="span___love" href="#">
                                                    <i class="fas fa-heart"></i>
                                                    {{ sizeof($item->wishListHotel) }}
                                                </a> --}}
                                            </div>
                                            <div class="slider-nav">
                                                @if (json_decode($item->album) != null)
                                                    @foreach (json_decode($item->album) as $items)
                                                        <div class="item">
                                                            <img src="{{url('/')}}{{ $items }}" alt="images/rooms_1.jpg"
                                                                class="img__links">
                                                        </div>
                                                    @endforeach
                                                @endif
                                            </div>

                                            <div class="content">
                                                <div class="content-header">
                                                    <h2 class="title">
                                                        <a href="#" class="title__links medium b">
                                                            {{ $item->name }}
                                                        </a>
                                                    </h2><hr>
                                                    <p class="p__price">
                                                        <span class="span__name medium">
                                                            Giá chỉ từ:
                                                        </span>
                                                        <span class="span__price bold">
                                                            @if (!empty($item->price_sale) && $item->price_sale < $item->price)
                                                            {{ number_format($item->price_sale) }}₫<sub class="sub__text">/đêm</sub>
                                                            @else 
                                                                {{ number_format($item->price) }}₫<sub class="sub__text">/đêm</sub>
                                                            @endif
                                                        </span>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                                <h3 class="title-like">
                                    Có thể bạn cũng thích
                                </h3>
                                <div class="bs-row row-lg-10 row-md-10 row-sm-5 row-xs-5">
                                    @foreach ($listHotel as $item)
                                    {{-- @php
                                        dd($item);
                                    @endphp --}}
                                    <div class="bs-col lg-25-10 md-25-10 sm-33-5 xs-50-5 tn-100">
                                        <div class="itemTours">
                                            <div class="avata">
                                                <div class="slider-for">
                                                    @foreach (json_decode($item->album) as $items)
                                                        <div class="item">
                                                            <a href="{{ route('hotel.ct', [$item->slug, $item->id]) }}" class="item__links"></a>
                                                            <img src="{{url('/')}}{{ $items }}" alt="images/rooms_1.jpg"
                                                                class="img__links">
                                                        </div>
                                                    @endforeach
                                                </div>
                                                @if (!empty($item->hotel_sale) && $item->hotel_sale < $item->price)
                                                    <span class="span__sale">
                                                        <span class="span__number">
                                                            {{ round((($item->price - $item->hotel_sale) / $item->price) *100) }} %
                                                        </span>
                                                    </span>
                                                @endif
                                                <a class="span___love" onclick="Wishlisthotel({{ $item->id }})">
                                                    <i class="fas fa-heart {{ Auth::check() ? ($item->wishlistHotelBy(auth()->user()) ? 'is-favourite' : ' '): ' ' }}" {{ Auth::check() == false ? ' modal-show=show modal-data=#login ' : ' ' }} id="color{{ $item->id }}"></i>
                                                    <span class="favourite {{ Auth::check() ? ($item->wishlistHotelBy(auth()->user()) ? 'is-favourite-color' : ' '): ' ' }}" {{ Auth::check() == false ? ' modal-show=show modal-data=#login ' : ' ' }} id="{{ $item->id }}">{{ sizeOf($item->wishListHotel) }}</span>
                                                </a>
                                            </div>
                                            <div class="slider-nav">
                                                @foreach (json_decode($item->album) as $items)
                                                    <div class="item">
                                                        <img src="{{url('/')}}{{ $items }}" alt="images/rooms_1.jpg"
                                                            class="img__links">
                                                    </div>
                                                @endforeach
                                            </div>
                                            <div class="content">
                                                <div class="content-header">
                                                    <h2 class="title">
                                                        <a href="{{ route('hotel.ct', [$item->slug, $item->id]) }}" class="title__links medium">
                                                            {{ $item->name }}
                                                        </a>
                                                    </h2>
                                                    <p class="p__price">
                                                        <span class="span__name medium">
                                                            Giá chỉ từ:
                                                        </span>
                                                        <span class="span__price bold">
                                                            {{-- @if (!empty($item->hotel_sale) && $item->hotel_sale < $item->price)
                                                            {{ number_format($item->hotel_sale) }}₫<sub class="sub__text">/đêm</sub>
                                                            @else 
                                                                {{ number_format($item->price) }}₫<sub class="sub__text">/đêm</sub>
                                                            @endif --}}
                                                            @if (!$item->room->isEmpty())
                                                            {{ number_format($item->room[0]['price']) }}₫<sub class="sub__text">/đêm</sub>
                                                            @endif
                                                        </span>
                                                    </p>
                                                    <p class="p__text medium">
                                                        <img class="img__link" src="{{url('/pms/images/icon_map.png')}}"
                                                            alt="images/icon_map.png">
                                                            {{ optional($item->location)->name}}
                                                    </p>
                                                </div>
                                                <div class="content-footer">
                                                    <div class="like-dvaluate">
                                                        <p class="p__like">
                                                            @for ($i = 0; $i < $item->avgReview; $i++)
                                                                <i class="fas fa-star"></i>
                                                            @endfor
                                                        </p>
                                                        <p class="p__like medium">
                                                            {{ sizeof($item->review) }} đánh giá
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </section>
</main>

<script>
    function Wishlisthotel(id) {
            $.ajax({
                type: "GET",
                url: "/hotel-favourite/"+id ,
                success: function (response) {
                    $('#'+id).html(response.countFavourite).css('color',response.color);
                    $('#color'+id).css('color',response.color);
            }
        });
    }
    $(document).ready(function () {
        function slideProduct() {
            $('.slider-for').slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: false,
                fade: false,
                asNavFor: '.slider-nav'
            });
            $('.slider-nav').slick({
                slidesToShow: 4,
                slidesToScroll: 1,
                asNavFor: '.slider-for',
                focusOnSelect: true,
            });
        }
        slideProduct();
    })
    $(document).on('click', '.item-option-location', function() {
        let text = $(this).text().trim();
        let id = $(this).attr('data-id');
        $(".text-location").val(text);
        $(".location_id").val(id);
        $(".item-grid__content").removeClass("active");
    })
</script>
@endsection