@extends('public.master')

@section('css-top')
<link href="{{ URL::asset('assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
@endsection
@section('content')

<main id="main">
    <section class="section-page section-page__2">
        <div class="bs-container">
            <div class="bs-row">
                <div class="bs-col">
                    <div class="module module-page">
                        <div class="module-header">
                            <h2 class="title">
                                <span class="span__name black"> Khách sạn mà bạn muốn tới, căn phòng mà bạn muốn ở </span>
                                <p class="p__info">
                                    Những trải nghiệm hàng đầu để bạn có thể bắt đầu
                                </p>
                            </h2>
                            <div class="page__right">
                                <ul class="item">
                                    <li class="item-list">
                                        <a href="index.html" class="item-list__links">
                                            Trang chủ
                                        </a>
                                    </li>
                                    <li class="item-list">
                                        <a href="car.html" class="item-list__links">
                                            Hotel
                                        </a>
                                    </li>
                                    <li class="item-list">
                                        <span class="item-list__links">
                                            Hà Nội
                                        </span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="section-rooms section-list section-list__car">
        <div class="container ">
            <form action="{{ route('hotel.map') }}" method="get" class="form-filter-car">
                <div class="row ">
                    <div class="col-3 ">
                        <div class="form-group">
                            <label for="">Chọn địa điểm</label>
                            <select name="location_id" id="" class="form-control submit-form">
                                <option value="">--Chọn địa điểm--</option>
                                @foreach ($listLocation as $item)
                                    <option value="{{ $item->id }}" @if(Request::get('location_id') == $item->id) selected @endif >{{ $item->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-3">
                        <label for="">Chọn thời gian</label>
                        <div class="form-group">
                            <input type="text" class="form-control " name="daterange" value="{{ Request::get('daterange') }}" readonly>
                        </div>
                    </div>
                    {{-- <div class="col-3">
                        <div class="form-group">
                            <label for="">Chọn hình thức xe</label>
                            <select name="car_form" id="" class="form-control submit-form">
                                <option value="">--Chọn hình thức--</option>
                                <option value="2" @if(Request::get('car_form') == 2) selected @endif>Xe tự lái</option>
                                <option value="1" @if(Request::get('car_form') == 1) selected @endif>Xe có lái</option>
                            </select>
                        </div>
                    </div> --}}
                    <div class="col-3"><div class="form-group">
                        <label for="">Chọn khoảng giá</label>
                        <select name="price_range" id="" class="form-control submit-form">
                            <option value="">--Chọn khoảng giá--</option>
                            <option value="1" @if(Request::get('price_range') == 1) selected @endif>Từ 0 vnđ - 5 triệu vnđ</option>
                            <option value="2" @if(Request::get('price_range') == 2) selected @endif>5 triệu vnđ - 10 triệu vnđ</option>
                            <option value="3" @if(Request::get('price_range') == 3) selected @endif>10 triệu vnđ - 15 triệu vnđ</option>
                        </select>
                    </div></div>
                </div>
            </form>
        </div>
        <div class="bs-container">
            <div class="grid">
                <div class="filterMobile">
                    <i class="fas fa-filter"></i>
                    <i class="fas fa-times"></i>
                </div>
                <div class="filterShow grid-sidebar div-50">
                    <div class="row">
                        <div class="col-12">
                            <div class="form-group mb-3 ">
                                <div id="gmaps-basic" class="gmaps gmap-car"></div>
                                <i>Click onto map to place Location address</i>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="grid-main div-50 height-fix">
                    <div class="grid-main__content">
                        <div class="bs-row row-lg-15 row-md-10 row-sm-5 row-xs-5">
                            @foreach ($listHotel as $item)
                                <div class="itemTours">
                                    <div class="bs-row">
                                        <div class="open-show__avata bs-col lg-33 md-33 sm-33 xs-50 tn-100">
                                            <div class="avata">
                                                <div class="slider-for">
                                                    @foreach (json_decode($item->album) as $items)
                                                        <div class="item">
                                                            <a href="{{ route('hotel.ct', [$item->slug, $item->id]) }}" class="item__links"></a>
                                                            <img src="{{url('/')}}{{ $items }}" alt="images/rooms_1.jpg"
                                                                class="img__links">
                                                        </div>
                                                    @endforeach
                                                </div>
                                                <span class="span__sale">
                                                    <span class="span__number">
                                                        @if (!empty($item->hotel_sale) && $item->hotel_sale < $item->price)
                                                                {{ round(($item->hotel_sale / $item->price) *100) }} %
                                                        @endif
                                                    </span>
                                                </span>
                                                <a class="span___love" onclick="Wishlisthotel({{ $item->id }})">
                                                    <i class="fas fa-heart {{ Auth::check() ? ($item->wishlistHotelBy(auth()->user()) ? 'is-favourite' : ' ') : ' ' }}" id="color{{ $item->id }}"></i>
                                                    <span class="favourite {{ Auth::check() ? ($item->wishlistHotelBy(auth()->user()) ? 'is-favourite-color' : ' '): ' ' }}" id="{{ $item->id }}">{{ sizeOf($item->wishListHotel) }}</span>
                                                    {{-- @if (!empty($item->hotel_sale) && $item->hotel_sale < $item->price)
                                                        <span class="span__sale">
                                                            <span class="span__number">
                                                                {{ round((($item->price - $item->hotel_sale) / $item->price) *100) }} %
                                                            </span>
                                                        </span>
                                                    @endif --}}
                                                <a class="span___love" href="#">
                                                    <i class="fas fa-heart"></i>
                                                    {{ sizeof($item->wishListHotel) }}
                                                </a>
                                            </div>
                                            <div class="slider-nav">
                                                @foreach (json_decode($item->album) as $items)
                                                    <div class="item">
                                                        <img src="{{url('/')}}{{ $items }}" alt="{{url('/')}}{{ $items }}" class="img__links">
                                                    </div>
                                                @endforeach


                                            </div>
                                        </div>
                                        <div class="open-show__content bs-col lg-66 md-66 sm-66 xs-50 tn-100">
                                            <div class="content">
                                                <div class="content-header">
                                                    <h2 class="title">
                                                        <a href="{{ route('hotel.ct', [$item->slug, $item->id]) }}" class="title__links">
                                                            <span class="span__name medium">
                                                                {{ $item->name }}
                                                            </span>
                                                            <p class="p__text">
                                                                <img class="img__link" src="{{url('/pms/images/icon_map.png')}}"
                                                                    alt="images/icon_map.png">
                                                                    {{ $item->location->name }}
                                                            </p>
                                                        </a>
                                                    </h2>
                                                    <p class="p__price">
                                                        <span class="span__name bold">
                                                            Gía chỉ từ:
                                                        </span>
                                                        <span class="span__price bold">
                                                            @if (!$item->room->isEmpty())
                                                            {{ number_format($item->room[0]['price']) }}₫<sub class="sub__text">/đêm</sub>
                                                            @endif
                                                            {{-- @if (!empty($item->hotel_sale) && $item->hotel_sale < $item->price)
                                                                {{ number_format($item->hotel_sale) }}₫<sub class="sub__text">/đêm</sub>
                                                            @else
                                                                {{ number_format($item->price) }}₫<sub class="sub__text">/đêm</sub> --}}
                                                            {{-- @endif --}}
                                                        </span>
                                                    </p>
                                                </div>
                                                <div class="content-footer">
                                                    @if (!$item->room->isEmpty())

                                                    <div class="text text__hidden">
                                                        <p class="p__time">
                                                            <span class="span__name medium">
                                                                {{ $item->room[0]['name'] }}
                                                            </span>
                                                            <img src="{{url('/pms/images/icon_user_2.png')}}" alt="icon_user_2s.png"
                                                                class="icon">
                                                        </p>
                                                    </div>
                                                    <div class="text text__hidden">
                                                        <p class="p__time">
                                                            {{ $item->room[0]['number_of_beds'] + $item->room[0]['number_of_bed'] }} giường ({{ $item->room[0]['number_of_beds'] }} giường đôi lớn, {{ $item->room[0]['number_of_bed'] }} giường đơn)
                                                        </p>
                                                    </div>
                                                    <div class="text text__hidden">
                                                        <p class="p__text">
                                                            Chỉ còn {{ $item->room[0]['number_of_room'] }} phòng như này
                                                        </p>
                                                    </div>
                                                    @endif

                                                    <div class="like-dvaluate">
                                                        <p class="p__like">
                                                            <span class="span__star">
                                                                @for ($i = 0; $i < $item->avgReview; $i++)
                                                                    <i class="fas fa-star"></i>
                                                                @endfor
                                                            </span>
                                                        </p>
                                                        <p class="p__like medium">
                                                            {{ sizeof($item->review) }} đánh giá
                                                        </p>
                                                    </div>

                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                        <div class="fexPagintaion text-center d-flex justify-content-center clearfix">
                            {{ $listHotel->render() }}
                        </div>
                    </div>
                </div>
            </div>
    </section>
    <input type="hidden" name="" class="list-car" value="{{ json_encode($listHotel) }}">
    
</main>
@endsection
@push('js')
<script>
    var listHotel = JSON.parse($('.list-car').val());
</script>
<script>
    var bookingCore = {
        url: 'http://sandbox.bookingcore.org',
        map_provider: 'gmap',
        map_gmap_key: '',
        csrf: 'JMrM5NwcxQy6HWOmuo7LQ2kHPh7pGwfbOPpXxkue'
    };
</script>

<script src="https://maps.google.com/maps/api/js?key=AIzaSyDsucrEdmswqYrw0f6ej3bf4M4suDeRgNA"></script>
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script>
{{-- <script type="text/javascript" src="https://raw.github.com/HPNeo/gmaps/master/gmaps.js"></script> --}}
<script src="{{ URL::asset('assets/js/map-engine.js') }}"></script>
<script>
    jQuery(function ($) {
        new BravoMapEngine('gmaps-basic', {
            fitBounds: true,
            center: [{{"21.008"}}, {{"105.858"}}],
            zoom:{{"8"}},
            ready: function (engineMap) {
                $.each(listHotel['data'], function( index, value ) {
                    engineMap.addMarker([parseFloat(value.latitu), parseFloat(value.longtitu)], {
                        icon_options: {}
                    });
                });
                
            }
        });
    })
</script>
<script>
    $(document).on('change', '.submit-form', function() {
        $('.form-filter-car').submit();
    });
    $(document).on('click', '.applyBtn', function() {
        $('.form-filter-car').submit();
    });
</script>
@endpush
