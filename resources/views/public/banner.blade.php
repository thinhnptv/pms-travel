<section class="section-slide">
    <div class="module module-slide">
        <div class="bs-row">
            <div class="bs-col">
                <div class="module-content">
                    <div class="slide-home">
                        <div class="item">
                            <img src="{{url('/pms/images/slide.png')}}" alt="" class="img__links">
                            <div class="bs-container">
                                <div class="content">
                                    <div class="content-header">
                                        <h2 class="title">
                                            Trải nghiệm kỳ nghỉ tuyệt vời
                                        </h2>
                                        <p class="info">
                                            đưa đón sân bay giá tốt nhất
                                        </p>
                                    </div>
                                    <div class="layout__1">
                                        <img src="{{url('/pms/images/vali.png')}}" alt="vali.png">
                                    </div>
                                </div>

                            </div>

                        </div>
                        <div class="item">
                            <img src="{{url('/pms/images/slide.png')}}" alt="" class="img__links">
                            <div class="bs-container">
                                <div class="content">
                                    <div class="content-header">
                                        <h2 class="title">
                                            Trải nghiệm kỳ nghỉ tuyệt vời
                                        </h2>
                                        <p class="info">
                                            đưa đón sân bay giá tốt nhất
                                        </p>
                                    </div>
                                    <div class="layout__1">
                                        <img src="{{url('/pms/images/vali.png')}}" alt="vali.png">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="bs-container">
                        <div class="bs-table">
                            <div class="bs-tab">
                                <div class="tab-container">
                                    <div class="tab-control">
                                        <span class="control__show">Hotel</span>
                                        <ul class="control-list">
                                            <li class="control-list__item active" tab-show="#tab1">Hotel</li>
                                            <li class="control-list__item" tab-show="#tab2">Tour</li>
                                            <li class="control-list__item" tab-show="#tab3">Car</li>
                                            <li class="control-list__item" tab-show="#tab4">Space</li>
                                        </ul>
                                    </div>
                                    <div class="tab-content">
                                        <div class="tab-item active" id="tab1">
                                            <form class="form-filter-hotel" action="{{ route('hotel.ds') }}" method="GET">
                                                <div class="grid">
                                                    <div class="item-grid">
                                                        <div class="item-grid__header  clickShow">
                                                            <div class="item_icon">
                                                                <img src="{{url('/pms/images/icon_map.png')}}" alt="icon_map.png">
                                                            </div>
                                                            <div class="item__content">
                                                                <h3 class="title">
                                                                    Nơi đến
                                                                </h3>
                                                                <p class="p__text addText">
                                                                    <input type="text" class="text-location_hotel" id="text-location_hotel" placeholder="Bạn muốn đi đâu" readonly>
                                                                    <input type="hidden" name="location_id" class="location_id" id="location_id_hotel">
                                                                </p>
                                                                <i class="fas fa-caret-down"></i>
                                                            </div>
                                                        </div>
                                                        <div class="item-grid__content">
                                                            
                                                            <ul class="item">
                                                                @foreach ($listLocation as $item)
                                                                    <div data-id="{{ $item->id }}" class="item-option item-option-location">
                                                                        <li class="item-list clickDestination">
                                                                            <i class="fas fa-map-marker-alt"></i>
                                                                            <span class="span__name ">
                                                                                {{ $item->name }}
                                                                            </span>
                                                                        </li>
                                                                    </div>
                                                                @endforeach
                                                        </div>
                                                    </div>
                                                    <div class="item-grid">
                                                        <div class="item-grid__header">
                                                            <div class="item_icon">
                                                                <img src="{{url('/pms/images/icon_check.png')}}" alt="icon_check.png">
                                                            </div>
                                                            <div class="item__content">
                                                                <h3 class="title">
                                                                    Check In - Check Out
                                                                </h3>
                                                                <p class="p__text acDates">
                
                                                                    <input type="text" name="daterange" readonly/>
                                                                </p>
                                                                <i class="fas fa-caret-down"></i>
                
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="item-grid">
                                                       
                                                    </div>
                                                    <div class="item-grid">
                                                        <button class="button">
                                                            <img src="{{url('/pms/images/icon_search.png')}}" alt="icon_search.png">
                                                        </button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="tab-item" id="tab2">
                                            <form action="{{ route('public.listTour') }}" method="GET">
                                                <div class="grid">
                                                    <div class="item-grid">
                                                        <div class="item-grid__header  clickShow">
                                                            <div class="item_icon">
                                                                <img src="{{ url('/') }}/pms/images/icon_map.png" alt="icon_map.png">
                                                            </div>
                                                            <div class="item__content">
                                                                <h3 class="title">
                                                                    Nơi khởi hành
                                                                </h3>
                                                                <p class="p__text addText">
                                                                    <input type="text" class="add-text-destination" placeholder="Bạn muốn đi đâu" readonly>
                                                                    <input type="hidden" name="start_destination" class="start_destination">
                                                                </p>
                                                                <i class="fas fa-caret-down"></i>
                                                            </div>
                                                        </div>
                                                        <div class="item-grid__content">
                                                            <ul class="item">
                                                                @foreach($listDestination as $item)
                                                                    <li data-id="{{ $item->id }}" class="item-list option-start-destination">
                                                                        <i class="fas fa-map-marker-alt"></i>
                                                                        <span class="span__name ">
                                                                            {{ $item->name }}
                                                                        </span>
                                                                    </li>
                                                                @endforeach
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div class="item-grid">
                                                        <div class="item-grid__header">
                                                            <div class="item_icon">
                                                                <img src="{{ url('/') }}/pms/images/icon_check.png" alt="icon_check.png">
                                                            </div>
                                                            <div class="item__content">
                                                                <h3 class="title">
                                                                    Check In - Check Out
                                                                </h3>
                                                                <p class="p__text acDates">
                
                                                                    <input type="text" name="daterange" readonly/>
                                                                </p>
                                                                <i class="fas fa-caret-down"></i>
                
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="item-grid">
                                                        <div class="item-grid__header clickShow">
                                                            <div class="item_icon">
                                                                <img src="{{ url('/') }}/pms/images/icon_us.png" alt="icon_us.png">
                                                            </div>
                                                            <div class="item__content">
                                                                <h3 class="title">
                                                                    Số lượng khách
                                                                </h3>
                                                                <p class="p__text">
                                                                    <input type="text" class="text-number_people"  placeholder="2 Người lớn - 1 Trẻ em" readonly>
                                                                    <input type="hidden" name="number_people" class="number_people">
                                                                </p>
                                                                <i class="fas fa-caret-down"></i>
                                                            </div>
                                                        </div>
                                                        <div class="item-grid__content">
                                                            <ul class="item">
                                                                <li class="item-list">
                                                                    <span class="span__name bold">
                                                                        Người lớn
                                                                    </span>
                                                                    <div class="number">
                                                                        <span class="span span__subtract bold" data-people="adults">
                                                                            -
                                                                        </span>
                                                                        <span class="span span__show number_adult">
                                                                            2
                                                                        </span>
                                                                        <span class="span span__add bold " data-people="adults">
                                                                            +
                                                                        </span>
                                                                    </div>
                                                                </li>
                                                                <li class="item-list">
                                                                    <span class="span__name bold">
                                                                        Trẻ em
                                                                        
                                                                    </span>
                                                                    <div class="number">
                                                                        <span class="span span__subtract_childent bold" data-people="children">
                                                                            -
                                                                        </span>
                                                                        <span class="span span__show number_children">
                                                                           1
                                                                        </span>
                                                                        <span class="span span__add bold" data-people="children">
                                                                            +
                                                                        </span>
                                                                    </div>
                                                                </li>
                                                                <li class="d-flex justify-content-end"><a href="javascript:void(0)" class="btn btn-sm btn-primary btn-apply-number-people">Apply</a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div class="item-grid">
                                                        <button class="button">
                                                            <img src="{{ url('/') }}/pms/images/icon_search.png" alt="icon_search.png">
                                                        </button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="tab-item" id="tab3">
                                            <form action="{{ route('public.listCar') }}" method="GET">
                                                <div class="grid">
                                                    <div class="item-grid">
                                                        <div class="item-grid__header  clickShow">
                                                            <div class="item_icon">
                                                                <img src="{{ url('/') }}/pms/images/icon_map.png" alt="icon_map.png">
                                                            </div>
                                                            <div class="item__content">
                                                                <h3 class="title">
                                                                    Nhận xe
                                                                </h3>
                                                                <p class="p__text addText">
                                                                    <input type="text" class="text-location" id="text-location_car" placeholder="Bạn muốn nhận xe tại địa điểm nào" readonly>
                                                                    <input type="hidden" name="location_id" class="location_id" id="location_id_car">
                                                                </p>
                                                                <i class="fas fa-caret-down"></i>
                                                            </div>
                                                        </div>
                                                        <div class="item-grid__content">
                                                            <ul class="item">
                                                                @foreach ($listLocation as $item)
                                                                    <li data-id="{{ $item->id }}" class="item-list item-list-location_car">
                                                                        <i class="fas fa-map-marker-alt"></i>
                                                                        <span class="span__name ">
                                                                            {{ $item->name }}
                                                                        </span>
                                                                    </li>
                                                                @endforeach
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div class="item-grid">
                                                        <div class="item-grid__header">
                                                            <div class="item_icon">
                                                                <img src="{{ url('/') }}/pms/images/icon_check.png" alt="icon_check.png">
                                                            </div>
                                                            <div class="item__content">
                                                                <h3 class="title">
                                                                    Ngày thuê - Ngày trả
                                                                </h3>
                                                                <p class="p__text acDates">
                
                                                                    <input type="text" name="daterange" readonly/>
                                                                </p>
                                                                <i class="fas fa-caret-down"></i>
                
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="item-grid">
                                                        <div class="item-grid__header clickShow">
                                                            <div class="item_icon">
                                                                <img src="{{ url('/') }}/pms/images/icon_us.png" alt="icon_us.png">
                                                            </div>
                                                            <div class="item__content">
                                                                <h3 class="title">
                                                                    Hình thức xe
                                                                </h3>
                                                                <p class="p__text">
                                                                    <input type="text" class="text-car-form" placeholder="Chọn hình thức thuê xe bạn muốn" readonly>
                                                                    <input type="hidden" name="car_form" class="car_form" >
                                                                </p>
                                                                <i class="fas fa-caret-down"></i>
                                                            </div>
                                                        </div>
                                                        <div class="item-grid__content">
                                                            <ul class="item">
                                                                <li data-id="1" class="item-list item-list-car-form">
                                                                    <i class="fas fa-map-marker-alt"></i>
                                                                    <span class="span__name ">
                                                                        Xe tự lái
                                                                    </span>
                                                                </li>
                                                                <li data-id="2" class="item-list item-list-car-form">
                                                                    <i class="fas fa-map-marker-alt"></i>
                                                                    <span class="span__name ">
                                                                        NV lái
                                                                    </span>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div class="item-grid">
                                                        <button class="button">
                                                            <img src="{{ url('/') }}/pms/images/icon_search.png" alt="icon_search.png">
                                                        </button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="tab-item" id="tab4">
                                            <form action="{{ route('space.list')}}" method="GET">
                                                <div class="grid">
                                                    <div class="item-grid">
                                                        <div class="item-grid__header  clickShow">
                                                            <div class="item_icon">
                                                                <img src="{{ url('/') }}/pms/images/icon_map.png" alt="icon_map.png">
                                                            </div>
                                                            <div class="item__content">
                                                                <h3 class="title">
                                                                    Nơi khởi hành
                                                                </h3>
                                                                <p class="p__text addText">
                                                                    <input type="text" placeholder="Bạn muốn đi đâu" id="text-location_id-place" readonly>
                                                                    <input type="hidden" name="location_id" class="location_id" id="location_id-space">
                                                                </p>
                                                                <i class="fas fa-caret-down"></i>
                                                            </div>
                                                        </div>
                                                        <div class="item-grid__content">
                                                            <ul class="item">
                                                                @foreach ($listLocation as $item)
                                                                <li data-id="{{ $item->id }}" class="item-list item-list-location-space">
                                                                    <i class="fas fa-map-marker-alt"></i>
                                                                    <span class="span__name ">
                                                                        {{ $item->name }}
                                                                    </span>
                                                                </li>
                                                                @endforeach
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div class="item-grid">
                                                        <div class="item-grid__header">
                                                            <div class="item_icon">
                                                                <img src="{{ url('/') }}/pms/images/icon_check.png" alt="icon_check.png">
                                                            </div>
                                                            <div class="item__content">
                                                                <h3 class="title">
                                                                    Check In - Check Out
                                                                </h3>
                                                                <p class="p__text acDates">
                
                                                                    <input type="text" name="daterange" readonly/>
                                                                </p>
                                                                <i class="fas fa-caret-down"></i>
                
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="item-grid">
                                                        <div class="item-grid__header clickShow">
                                                            <div class="item_icon">
                                                                <img src="{{ url('/') }}/pms/images/icon_us.png" alt="icon_us.png">
                                                            </div>
                                                            <div class="item__content">
                                                                <h3 class="title">
                                                                    Số lượng khách tối đa
                                                                </h3>
                                                                <p class="p__text">
                                                                    <input type="text" class="text-space-form" placeholder="Số lượng khách: 1" readonly>
                                                                    <input type="hidden" name="numbeguests" class="number_guests" placeholder="Số lượng khách" value="1">
                                                                </p>
                                                                <i class="fas fa-caret-down"></i>
                                                            </div>
                                                        </div>
                                                        <div class="item-grid__content">
                                                            <ul class="item">
                                                                <li data-id=1 class="item-list">
                                                                    <span class="span__name bold ">
                                                                        Số lượng khách: <span class="guests"></span>
                                                                    </span>
                                                                    <div class="number">
                                                                        <span class="span span__subtract_space bold">
                                                                            -
                                                                        </span>
                                                                        <span class="span span__show_space">
                                                                            1
                                                                        </span>
                                                                        <span class="span span__add_space bold">
                                                                            +
                                                                        </span>
                                                                    </div>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div class="item-grid">
                                                        <button class="button">
                                                            <img src="{{ url('/') }}/pms/images/icon_search.png" alt="icon_search.png">
                                                        </button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>