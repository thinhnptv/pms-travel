@extends('public.master')

@section('content')
<style>
    .is-favourite{
        color: red;
    }
    .is-favourite-color{
        color: red;
    }
</style>
<main id="main">
    <section class="section-page section-detail__page">
        <div class="bs-container">
            <div class="bs-row">
                <div class="bs-col">
                    <div class="module module-page">
                        <div class="module-content">
                            <form action="{{ route('space.list') }}">
                                <div class="grid">
                                    <div class="item-grid">
                                        <div class="item-grid__header clickShow">
                                            <div class="item_icon">
                                                <img src="{{ url('/') }}/images/icon_map.png" alt="icon_map.png" />
                                            </div>
                                            <div class="item__content">
                                                <p class="p__text addText">
                                                    <input type="text" class="text-location" placeholder="Điểm đến"  readonly/>
                                                    <input type="hidden" name="location_id" class="location_id">
                                                </p>
                                                <i class="fas fa-caret-down"></i>
                                            </div>
                                        </div>
                                        <div class="item-grid__content">
                                            <ul class="item">
                                                    @foreach ($listLocation as $item)
                                                    <li data-id="{{ $item->id }}" class="item-list clickDestination">
                                                        <i class="fas fa-map-marker-alt"></i>
                                                        <span class="span__name ">
                                                            {{ $item->name }}
                                                        </span>
                                                    </li>
                                                    @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="item-grid">
                                        <div class="item-grid__header">
                                            <div class="item_icon">
                                                <img src="{{ url('/') }}/images/icon_check.png" alt="icon_check.png" />
                                            </div>
                                            <div class="item__content">
                                                <p class="p__text acDates">
                                                    <input type="text" name="daterange" />
                                                </p>
                                                <i class="fas fa-caret-down"></i>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item-grid">
                                        <button class="button black">
                                            Tìm kiếm
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="section-category">
        <div class="bs-container">
            <div class="bs-row">
                <div class="bs-col">
                    <div class="module module-category">
                        <div class="module-header">
                            <ul class="category">
                                <li class="category-list">
                                    <a href="/" class="category-list__link">
                                        Trang chủ
                                    </a>
                                </li>
                                <li class="category-list">
                                    <a href="{{ route('space.list') }}" class="category-list__link">
                                        Spaces
                                    </a>
                                </li>
                                <li class="category-list">
                                    <a class="category-list__link">
                                        Hà Nội
                                    </a>
                                </li>
                                <li class="category-list">
                                    <span class="category-list__link">
                                        {{ $spaceDetail->name }}
                                    </span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="section-detail">
        <div class="bs-container">
            <div class="bs-row">
                <div class="bs-col">
                    <div class="module module-detail">
                        <div class="module-header clearfix">
                            <div class="header-info">
                                <h2 class="title bold">
                                    {{ $spaceDetail->name }}
                                </h2>
                                <p class="p__category">
                                    <span class="span__star">
                                        @if(!empty($spaceDetail->rating_star))
                                        {!! rating_star($spaceDetail->rating_star) !!}
                                        @endif
                                    </span>
                                    @if(!empty($spaceDetail->rating_star))
                                    <span class="span__number bold">
                                        {{ $spaceDetail->rating_star }} /5
                                    </span>
                                    <span class="span__name bold">
                                        Tuyệt vời
                                    </span>
                                    <span class="span__text">
                                        | 3 đánh giá
                                    </span>
                                    @endif
                                </p>
                                <p class="p__address">
                                    <img src="{{ config('app.url') }}/images/icon_map.png" class="img__link" alt="icon_map.png" />
                                    <span class="span__name">
                                        {!! ucwords($spaceDetail->address.' - '.optional($spaceDetail->locationSpace)->name) !!}
                                    </span>
                                </p>
                            </div>
                            <div class="header-buy">
                                <p class="p__name medium">Giá space <span class="span__number black">
                                    {{ !empty($spaceDetail->sale_price) ? number_format($spaceDetail->sale_price).' '.'đ' : number_format($spaceDetail->price). ' '. 'đ' }}
                                </span></p>
                                <a href="#" class="buy__link black">
                                    Đặt phòng ngay
                                </a>
                            </div>
                        </div>
                        <div class="module-content">
                            <div class="grayimg">
                                @if (($arr_album_value) != ' ')
                                <div class="bs-row">
                                    <div class="bs-col lg-20 md-20 sm-20 xs-100">
                                        <a href="#" class="grayimg__links">
                                            <img src="{{ $arr_album_value[0] }}" alt="" width="234px" height="234px" />
                                        </a>
                                        <a href="#" class="grayimg__links">
                                            <img src="{{ $arr_album_value[1] }}" alt="" width="234px" height="234px" />
                                        </a>
                                    </div>
                                    <div class="bs-col lg-40 md-40 sm-40 xs-100">
                                        <a href="#" class="grayimg__links">
                                            <img src="{{ $arr_album_value[2] }}" alt="" width="468px" height="468px" />
                                        </a>
                                    </div>
                                    <div class="bs-col lg-40 md-40 sm-40 xs-100">
                                        <a href="#" class="grayimg__links">
                                            <img src="{{ $arr_album_value[3] }}" alt="" width="234px" height="234px" />
                                        </a>
                                        <a href="#" class="grayimg__links">
                                            <img src="{{ $arr_album_value[4] }}" alt="" width="234px" height="234px" />
                                        </a>
                                        <a href="#" class="grayimg__links">
                                            <img src="{{ $arr_album_value[5] }}" alt="" width="234px" height="234px" />
                                        </a>
                                        <a href="#" class="grayimg__links">
                                            <img src="{{ $arr_album_value[6] }}" alt="" width="234px" height="234px" />
                                        </a>
                                    </div>
                                </div>
                                @endif
                            </div>
                            <div class="content">
                                <h3 class="title__info">
                                    Mô tả space
                                </h3>
                                {!! $spaceDetail->content !!}
                            </div>
                            <div class="basis">
                                <h3 class="title-basis">
                                    tiện ích của space
                                </h3>
                                <div class="item">
                                    <ul class="item-basis clearfix">
                                        @foreach ($spaceDetail->spaceTypes as $item)
                                        <li class="item-basis__list">
                                            <i class="fas fa-check"></i>
                                            {!! $item->name !!}
                                        </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                            <div class="necessary">
                                <h3 class="title-necessary medium">
                                    thông tin cần biết
                                </h3>
                                <p class="p__text">
                                    <span class="span__name medium">
                                        7:00 PM - 7:00 AM:
                                    </span>
                                    Tính chi phí 1 ngày
                                </p>
                                <p class="p__text"><span class="span__name medium">7:00 AM - 1:00 PM:</span> Tính chi phí 1/2 ngày</p>
                                <p class="p__text">
                                    Các loại phòng khác nhau có thể có chính sách hủy đặt phòng và chính sách thanh toán trước khác nhau. Vui lòng kiểm tra chi tiết chính sách phòng khi chọn phòng ở phía trên
                                </p>
                            </div>
                            <div class="question">
                                <h3 class="title-question">
                                    Câu hỏi thường gặp
                                </h3>
                                <div class="panel-group" id="accordion">
                                    @foreach ($spaceDetail->faq as $faq)
                                    <div class="panel">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion" href="{{ '#'.$faq->id }}">
                                                    <img src="{{ '/images/icon_message.png' }}" alt="icon_message.png" class="icon icon__message" />
                                                    {{ $faq->title }}
                                                    <i class="fas fa-angle-down"></i>
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="{{ $faq->id }}" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                {{ $faq->content }}
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                            <div class="evaluate">
                                <h3 class="title-evaluate">
                                    Đánh giá
                                </h3>
                                <div class="evaluate-container">
                                    <div class="bs-row row-lg-10 row-md-10 row-ms-5 row-xs">
                                        <div class="bs-col lg-25-10 md-25-10 sm-30-5 xs-100">
                                            <div class="item">
                                                <span class="span__number medium"> 5.0<sub>/5</sub> </span>
                                                <p class="p__text light">
                                                    Tuyệt vời
                                                </p>
                                                <p class="p__evaluate">
                                                    Dựa trên 3 đánh giá
                                                </p>
                                            </div>
                                        </div>
                                        <div class="bs-col lg-75-10 md-75-10 sm-70-5 xs-100">
                                            <div class="evaluate-content">
                                                <p class="p__evaluate active">
                                                    <span class="span__name">
                                                        Tuyệt vời
                                                    </span>
                                                    <span class="span__number">
                                                        3
                                                    </span>
                                                </p>
                                                <p class="p__evaluate">
                                                    <span class="span__name">
                                                        Rất tốt
                                                    </span>
                                                    <span class="span__number">
                                                        0
                                                    </span>
                                                </p>
                                                <p class="p__evaluate">
                                                    <span class="span__name">
                                                        Cũng được
                                                    </span>
                                                    <span class="span__number">
                                                        0
                                                    </span>
                                                </p>
                                                <p class="p__evaluate">
                                                    <span class="span__name">
                                                        Trung bình
                                                    </span>
                                                    <span class="span__number">
                                                        0
                                                    </span>
                                                </p>
                                                <p class="p__evaluate">
                                                    <span class="span__name">
                                                        Kém
                                                    </span>
                                                    <span class="span__number">
                                                        0
                                                    </span>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="comment">
                                <div class="comment-container">
                                    <div class="avata">
                                        <div class="ImagesFrame">
                                            <div class="ImagesFrameCrop0">
                                                <img src="images/avata-1.png" alt="" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="content">
                                        <h3 class="title__name bold">
                                            Trần Lệ Ngân
                                            <span class="span__star">
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                            </span>
                                        </h3>
                                        <p class="p__day">
                                            03/12/2020 02:30
                                        </p>
                                        <p class="p__content">
                                            Sed pharetra rhoncus venenatis. Quisque tempus arcu sed lorem tincidunt laoreet. Maecenas nec porttitor eros. Aenean ut facilisis dolor. Nunc ut purus purus.
                                        </p>
                                    </div>
                                </div>
                                <div class="comment-container">
                                    <div class="avata">
                                        <div class="ImagesFrame">
                                            <div class="ImagesFrameCrop0">
                                                <img src="images/avata-2.png" alt="" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="content">
                                        <h3 class="title__name bold">
                                            Phạm Thanh Trang
                                            <span class="span__star">
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                            </span>
                                        </h3>
                                        <p class="p__day">
                                            03/12/2020 02:30
                                        </p>
                                        <p class="p__content">
                                            Sed pharetra rhoncus venenatis. Quisque tempus arcu sed lorem tincidunt laoreet. Maecenas nec porttitor eros. Aenean ut facilisis dolor. Nunc ut purus purus.
                                        </p>
                                    </div>
                                </div>
                                <div class="comment-container">
                                    <div class="avata">
                                        <div class="ImagesFrame">
                                            <div class="ImagesFrameCrop0">
                                                <img src="images/avata-3.png" alt="" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="content">
                                        <h3 class="title__name bold">
                                            Nguyễn Hải Ninh
                                            <span class="span__star">
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                            </span>
                                        </h3>
                                        <p class="p__day">
                                            03/12/2020 02:30
                                        </p>
                                        <p class="p__content">
                                            Sed pharetra rhoncus venenatis. Quisque tempus arcu sed lorem tincidunt laoreet. Maecenas nec porttitor eros. Aenean ut facilisis dolor. Nunc ut purus purus.
                                        </p>
                                    </div>
                                </div>
                                <div class="comment-footer">
                                    <p class="p__total">
                                        Hiển thị 1 - 3 trong tổng số 3 đánh giá
                                    </p>
                                    <p class="p__login">Bạn cần phải <a href="#" class="span__name">đăng nhập</a> để đánh giá khách sạn</p>
                                </div>
                            </div>
                            <div class="like">
                                <h3 class="title-like">
                                    Có thể bạn cũng thích
                                </h3>

                                <div class="bs-row row-lg-10 row-md-10 row-sm-5 row-xs-5">
                                    @foreach ($spacesFavourite as $item)
                                    <div class="bs-col lg-25-10 md-25-10 sm-33-5 xs-50-5 tn-100">
                                        <div class="itemTours">
                                            <div class="avata">
                                                <div class="slider-for">
                                                    <div class="item">
                                                        <a href="{{ route('space.detail',[$item->slug,$item->id]) }}" class="item__links"></a>
                                                        <img src="{{ config('app.url') }}{{ $item->banner }}" alt="images/rooms_1.jpg" class="img__links" />
                                                    </div>
                                                </div>
                                                @if (!empty($item->sale_price))
                                                <span class="span__sale">
                                                    <span class="span__number">
                                                        {{ '-'.floor((($item->price - $item->sale_price)/$item->price)*100).'%' }}
                                                    </span>
                                                </span>
                                                @endif
                                                <a class="span___love" onclick="favourite({{ $item->id }})">
                                                    <i class="fas fa-heart {{ Auth::check() ? ($item->isFavouritesBy(auth()->user()) ? 'is-favourite' : ' '): ' ' }}"
                                                        {{ Auth::check() == false ? ' modal-show=show modal-data=#login ' : ' ' }}
                                                    id="color{{ $item->id }}"></i>
                                                    <span class="favourite {{ Auth::check() ? ($item->isFavouritesBy(auth()->user()) ? 'is-favourite-color' : ' '): ' ' }}"
                                                        {{ Auth::check() == false ? ' modal-show=show modal-data=#login ' : ' ' }}
                                                    id="{{ $item->id }}">{{ sizeOf($item->spaceFavourites) }}</span>
                                                </a>
                                            </div>

                                            <div class="content">
                                                <div class="content-header">
                                                    <h2 class="title">
                                                        <a href="{{ route('space.detail', [$item->slug, $item->id]) }}" class="title__links medium">
                                                            {{ $item->name }}
                                                        </a>
                                                    </h2>
                                                    <p class="p__price">
                                                        <span class="span__name medium">
                                                            Giá chỉ từ:
                                                        </span>
                                                        <span class="span__price bold">
                                                            {{ (!empty($item->sale_price) ? number_format($item->sale_price) : number_format($item->price)).' '.'đ' }}
                                                        </span>
                                                    </p>
                                                    <p class="p__text medium">
                                                        <img class="img__link" src="{{ config('app.url') }}/images/icon_map.png" alt="images/icon_map.png" />
                                                        {{ ucwords($item->address. ' - ' .$item->locationSpace->name) }}
                                                    </p>
                                                </div>
                                                <div class="content-footer">
                                                    <div class="like-dvaluate">
                                                        <p class="p__like">
                                                            @if (!empty($item->rating_star) )
                                                            {!! rating_star($item->rating_star) !!}
                                                            @endif
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
<script type="text/javascript">
    function favourite(id) {
        $.ajax({
            type: "GET",
            url: "/space-favourite/"+id ,
            success: function (response) {
                $('#'+id).html(response.countFavourite).css('color',response.color);
                $('#color'+id).css('color',response.color);
            }
        });
    }

    $(document).ready(function () {
        function slideResort() {
            $(".slide-resort").slick({
                slidesToShow: 3,
                slidesToScroll: 3,
                adaptiveHeight: false,
                prevArrow: '<i class="icon_control fas fa-angle-left"></i>',
                nextArrow: '<i class="icon_control fas fa-angle-right"></i>',
                arrows: true,
                infinite: false,
                rows: 2,
                responsive: [{

                    breakpoint: 991.9,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1,
                        adaptiveHeight: false,
                        infinite: true,
                        row:2,
                    }
                },

                {

                    breakpoint: 479.9,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        adaptiveHeight: true,
                        infinite: true
                    }
                }

                ]
            });
        }
        slideResort();
    });
    $(document).on('click', '.clickDestination', function(e) {
        let text = $(this).text().trim();
        let id = $(this).attr('data-id');
        $(".text-location").val(text);
        $(".location_id").val(id);
        $(".item-grid__content").removeClass("active");
    });

    $(document).on('click','.span__add',function(){
        var number = $('.span__show').html();
        let guests = $('.guests').html(number);
        let text = 'Số lượng khách: ' + number;

        $(".text-space-form").val(text);
        $('.number_guests').val(number);

    });

    $(document).on('click','.span__subtract',function(){
        var number = $('.span__show').html();
        let guests = $('.guests').html(number);
        let text = 'Số lượng khách: ' + number;

        $(".text-space-form").val(text);
        $('.number_guests').val(guests);
    });

</script>
@endsection

