@extends('public.master')
@section('content')
<style>
    .is-favourite{
        color: red;
    }
    .is-favourite-color{
        color: red;
    }
</style>
<main id="main">
    <section class="section-page section-page__2">
        <div class="bs-container">
            <div class="bs-row">
                <div class="bs-col">
                    <div class="module module-page">
                        <div class="module-header">
                            <h2 class="title">
                                <span class="span__name black">Danh sách Space</span>
                                <p class="p__info">
                                    Những trải nghiệm hàng đầu ở Huế để bạn bắt đầu
                                </p>


                            </h2>
                            <div class="page__right">
                                <ul class="item">
                                    <li class="item-list">
                                        <a href="/" class="item-list__links">
                                            Trang chủ
                                        </a>
                                    </li>
                                    <li class="item-list">
                                        <a href="car.html" class="item-list__links">
                                            Space
                                        </a>
                                    </li>
                                    <li class="item-list">
                                        <span class="item-list__links">
                                            Hà Nội
                                        </span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="section-rooms section-list section-list__car">
        <div class="bs-container">
            <form method="GET" id="form-filter-space" action="">
            <div class="grid">
                <div class="filterMobile">
                    <i class="fas fa-filter"></i>
                    <i class="fas fa-times"></i>
                </div>
                <div class="filterShow grid-sidebar">
                    <div class="from__search">
                        <input type="text" name="spaceSearch" placeholder="Nhập tên space" class="input__search" >
                        <button class="btn__search">
                            <i class="fas fa-search"></i>
                        </button>
                    </div>
                    <div class="search">

                        <div class="form-group">
                            <label class="lable__name bold" for="">Nơi đến</label>
                            <div class="select">
                            <input type="hidden" name="location_id" class="location_id" value='{{ Request::get('location_id') }}'>
                                <div class="header-option header-option-location">
                                <img src="{{ url('/') }}/pms/images/icon_map.png" class="img__option" alt="icon_map.png">
                                    <span class="span__option medium">
                                        @if(!empty(Request::get('location_id')))
                                            @foreach($location as $item)
                                                @if ($item->id == Request::get('location_id'))
                                                    {{ $item->name }}
                                                @endif
                                            @endforeach
                                        @else
                                            --Chọn nơi đến--
                                        @endif
                                    </span>
                                </div>
                                <div class="body-option">
                                    @foreach ($location as $item)
                                        <div data-id="{{ $item->id }}" class="item-option">
                                            <img src="{{ url('/') }}/pms/images/icon_map.png" class="img__option" alt="icon_map.png">
                                            <span class="span__option medium">
                                                {{ $item->name }}
                                            </span>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                            <div class="form-group">
                                <label class="lable__name bold">CheckIn / CheckOut</label>
                                <div class="select">
                                    <div class="header-option">
                                        <img src="{{ url('/') }}/pms/images/icon_check.png" class="img__option" alt="icon_map.png">
                                        <input type="text" class="" name="daterange" >
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="lable__name bold" for="">Số lượng người</label>
                                <div class="select">
                                    <div class="header-option header-option-car-form">
                                        <img src="{{ url('/') }}/pms/images/icon_us.png" class="img__option" alt="icon_us.png">
                                        <span class="span__option medium">
                                                --Chọn số lượng người--
                                            <input type="hidden" name="numbeguests" class="number_guests" placeholder="Số lượng khách">
                                        </span>
                                    </div>
                                    <div class="body-option">
                                        <div class="item-grid__content">
                                            <ul class="item">
                                                <li data-id=1 class="item-list">
                                                    <span class="span__name bold ">
                                                        Số lượng khách: <span class="guests"></span>
                                                    </span>
                                                    <span class="number">
                                                        <span class="span span__subtract bold">
                                                            -
                                                        </span>
                                                        <span class="span span__show">
                                                            1
                                                        </span>
                                                        <span class="span span__add bold">
                                                            +
                                                        </span>
                                                    </span>

                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <button type="submit" class="button__search bold">
                                Tìm kiếm
                            </button>

                    </div>
                    <div class="evaluate">
                        <div class="onCheckRaido item">
                            <div class="item-header">
                                <h3 class="title bold">
                                    Điểm đánh giá
                                </h3>
                            </div>
                            <div class="item-content">
                                <label class="lable__radio label-category {{Request::get('star') == 1 ? 'active' : ''}}">
                                    <span class="span__radio">
                                        1 <br>
                                        Sao
                                    </span>
                                    <input type="radio" class="input__radio" name="star" value="1" {{Request::get('star') == 1 ? 'checked="checked"' : ''}}>
                                </label>
                                <label class="lable__radio label-category {{Request::get('star') == 2 ? 'active' : ''}}">
                                    <span class="span__radio">
                                        2 <br>
                                        Sao
                                    </span>
                                    <input type="radio" class="input__radio" name="star" value="2" {{Request::get('star') == 2 ? 'checked="checked"' : ''}}>
                                </label>
                                <label class="lable__radio label-category {{Request::get('star') == 3 ? 'active' : ''}}">
                                    <span class="span__radio">
                                        3 <br>
                                        Sao
                                    </span>
                                    <input type="radio" class="input__radio" name="star" value="3" {{Request::get('star') == 3 ? 'checked="checked"' : ''}}>
                                </label>
                                <label class="lable__radio label-category {{Request::get('star') == 4 ? 'active' : ''}}">
                                    <span class="span__radio">
                                        4 <br>
                                        Sao
                                    </span>
                                    <input type="radio" class="input__radio" name="star" value="4" {{Request::get('star') == 4 ? 'checked="checked"' : ''}}>
                                </label>
                                <label class="lable__radio label-category {{Request::get('star') == 5 ? 'active' : ''}}">
                                    <span class="span__radio">
                                        5 <br>
                                        Sao
                                    </span>
                                    <input type="radio" class="input__radio" name="star" value="5" {{Request::get('star') == 5 ? 'checked="checked"' : ''}}>
                                </label>
                            </div>
                        </div>
                        <div class="onCheckRaido price">
                            <div class="item-header">
                                <h3 class="title bold">
                                    Chọn khoảng giá
                                </h3>
                            </div>
                            <div class="item-content">
                                    <label class="lable__radio {{ (Request::get('price')==1) ? 'active' : ' ' }}">
                                        <span class="span__radio">
                                            <b class="bold">0</b> vnđ - <b class="bold">5</b> triệu vnđ
                                        </span>
                                        <input type="radio" class="input__radio" name="price" value="1" onchange="this.form.submit()"  {{ (Request::get('price')==1)?'checked':' ' }}>
                                    </label>
                                    <label class="lable__radio {{ (Request::get('price')==2) ? 'active' : ' ' }}">
                                        <span class="span__radio">
                                            <b class="bold">5</b> vnđ - <b class="bold">10</b> triệu vnđ
                                        </span>
                                        <input type="radio" class="input__radio" name="price"  value="2"onchange="this.form.submit()" {{ (Request::get('price')==2)?'checked':' ' }}>
                                    </label>
                                    <label class="lable__radio {{ (Request::get('price')==3) ? 'active' : ' ' }}">
                                        <span class="span__radio">
                                            <b class="bold">10</b> vnđ - <b class="bold">15</b> triệu vnđ
                                        </span>
                                        <input type="radio" class="input__radio" name="price" value="3" onchange="this.form.submit()" {{ (Request::get('price')==3)?'checked':' ' }}>
                                    </label>
                            </div>
                        </div>
                        {{-- @foreach ($spaceAttribute as $item)
                        <div class="onCheckRaido price date">
                            <div class="item-header">
                                <h3 class="title bold">
                                    {{ $item->name }}
                                </h3>
                            </div>
                            @foreach ($spacetype as $key => $val)
                            @if ($item->id == $key)
                            @foreach($val as $key => $val)
                            <div class="item-content">

                                    @if(!empty(Request::get('termType-{{ $item->id }}')) && in_array($val->id, Request::get('termType-{{ $item->id }}')))
                                        <label data-id="{{ $val->id }}" class="lable__radio lable-radio-term-type active">
                                            <span class="span__radio">
                                                {{ $val->name }}
                                            </span>
                                        </label>
                                    @else
                                        <label data-id="{{ $val->id }}" class="lable__radio lable-radio-term-type">
                                            <span class="span__radio">
                                                {{ $val->name }}
                                            </span>
                                        </label>
                                    @endif

                            </div>
                            <div>
                                    @if(!empty(Request::get('termType-{{ $item->id }}')) && in_array($val->id, Request::get('termType-{{ $item->id }}')))
                                        <input type="checkbox" id="termType-checkbox-{{ $val->id }}" class="input__radio" name="termType-{{ $item->id }}[]" value="{{ $val->id }}" checked>
                                    @else
                                        <input type="checkbox" id="termType-checkbox-{{ $val->id }}" class="input__radio" name="termType-{{ $item->id }}[]" value="{{ $val->id }}">
                                    @endif
                            </div>
                            @endforeach
                            @endif
                            @endforeach
                        </div>
                        @endforeach --}}
                        <div class="onCheckRaido price date">
                            <div class="item-header">
                                <h3 class="title bold">
                                    Loại Space
                                </h3>
                            </div>
                            <div class="item-content">
                                @foreach($spaceType as $item)
                                    @if(!empty(Request::get('termType')) && in_array($item->id, Request::get('termType')))
                                        <label data-id="{{ $item->id }}" class="lable__radio lable-radio-term-type active">
                                            <span class="span__radio">
                                                {{ $item->name }}
                                            </span>
                                        </label>
                                    @else
                                        <label data-id="{{ $item->id }}" class="lable__radio lable-radio-term-type">
                                            <span class="span__radio">
                                                {{ $item->name }}
                                            </span>
                                        </label>
                                    @endif
                                @endforeach
                            </div>
                            <div>
                                @foreach($spaceType as $item)
                                    @if(!empty(Request::get('termType')) && in_array($item->id, Request::get('termType')))
                                        <input type="checkbox" id="termType-checkbox-{{ $item->id }}" class="input__radio" name="termType[]" value="{{ $item->id }}" checked>
                                    @else
                                        <input type="checkbox" id="termType-checkbox-{{ $item->id }}" class="input__radio" name="termType[]" value="{{ $item->id }}">
                                    @endif
                                @endforeach
                            </div>
                        </div>
                        <div class="onCheckRaido price date">
                            <div class="item-header">
                                <h3 class="title bold">
                                    Space Amenity
                                </h3>
                            </div>
                            <div class="item-content">
                            @foreach($spaceAmenity as $item)
                                @if(!empty(Request::get('spaceAmenity')) && in_array($item->id, Request::get('spaceAmenity')))
                                    <label data-id="{{ $item->id }}" class="lable__radio lable-radio-term-features active">
                                        <span class="span__radio">
                                            {{ $item->name }}
                                        </span>
                                    </label>
                                @else
                                    <label data-id="{{ $item->id }}" class="lable__radio lable-radio-term-features">
                                        <span class="span__radio">
                                            {{ $item->name }}
                                        </span>
                                    </label>
                                @endif
                            @endforeach
                            </div>
                            <div>
                                @foreach($spaceAmenity as $item)
                                    @if(!empty(Request::get('spaceAmenity')) && in_array($item->id, Request::get('spaceAmenity')))
                                        <input type="checkbox" id="spaceAmenity-checkbox-{{ $item->id }}" class="input__radio" name="spaceAmenity[]" value="{{ $item->id }}" checked>
                                    @else
                                        <input type="checkbox" id="spaceAmenity-checkbox-{{ $item->id }}" class="input__radio" name="spaceAmenity[]" value="{{ $item->id }}">
                                    @endif
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
                <div class="grid-main">
                    <div class="filter">
                        <div class="filter-price">
                            <div class="control-filter">
                                <span class="span__name bold">
                                    Xem theo:
                                </span>
                                <select class="select__text filter_price" name="filter_price" onchange="this.form.submit()">
                                    <option value="">Giá tiền</option>
                                    <option value="1" {{Request::get('filter_price') == 1 ? 'selected' : ''}}>Thấp đến cao</option>
                                    <option value="2" {{Request::get('filter_price') == 2 ? 'selected' : ''}} >Cao đến thấp</option>
                                    <option value="3" {{Request::get('filter_price') == 3 ? 'selected' : ''}} >Khuyến mại</option>
                                </select>
                            </div>
                        </div>
            </form>
                    <div class="filter-list">
                        <p class="p__text">
                            <b class="bold">{{ $listSpaces->firstItem().'-'. $listSpaces->lastItem() }}</b> trong tổng số <b class="bold">{{  $listSpaces->total() }}</b> Space
                        </p>
                        <i class="fas fa-th-large"></i>
                        <i class="fas fa-th-list active"></i>
                    </div>
                    </div>
                    <div class="grid-main__content">
                        <div class="grid-main__content onDisplay">
                            @foreach ($listSpaces as $item)
                            <div class="itemTours">
                                <div class="bs-row">
                                    <div class="open-show__avata bs-col lg-33 md-33 sm-33 xs-50 tn-100">
                                        <div class="avata">
                                            <div class="slider-for">
                                            @foreach (json_decode($item->gallery) as $items)
                                                <div class="item">
                                                    <a href="{{ route('space.detail',[$item->slug,$item->id]) }}" class="item__links"></a>
                                                    <img src="{{url('/')}}{{ $items }}" alt="images/rooms_1.jpg"
                                                        class="img__links">
                                                </div>
                                            @endforeach
                                                {{-- <div class="item">
                                                    <a href="{{ route('space.detail',[$item->slug,$item->id]) }}" class="item__links"></a>
                                                    <img src="{{ config('app.url') }}{{ $item->banner }}" alt="images/rooms_1.jpg"
                                                        class="img__links">
                                                </div> --}}
                                            </div>
                                            @if (isset($item->sale_price))
                                            <span class="span__sale">
                                                <span class="span__number">
                                                   {{ '-'.floor((($item->price-$item->sale_price)/($item->price))*100).'%' }}
                                                </span>
                                            </span>
                                            @endif
                                            <a class="span___love" onclick="favourite({{ $item->id }})">
                                                <i class="fas fa-heart {{ Auth::check() ? ($item->isFavouritesBy(auth()->user()) ? 'is-favourite' : ' '): ' ' }}"
                                                    {{ Auth::check() == false ? ' modal-show=show modal-data=#login ' : ' ' }}
                                                id="color{{ $item->id }}" ></i>
                                                <span class="favourite {{ Auth::check() ? ($item->isFavouritesBy(auth()->user()) ? 'is-favourite-color' : ' '): ' ' }}"
                                                    {{ Auth::check() == false ? ' modal-show=show modal-data=#login ' : ' ' }}
                                                id="{{ $item->id }}">{{ sizeOf($item->spaceFavourites) }}</span>
                                            </a>
                                        </div>
                                        <div class="slider-nav">
                                            @foreach (json_decode($item->gallery) as $items)
                                                <div class="item">
                                                    <img src="{{url('/')}}{{ $items }}" alt="{{url('/')}}{{ $items }}" class="img__links">
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                    <div class="open-show__content bs-col lg-66 md-66 sm-66 xs-50 tn-100">
                                        <div class="content">
                                            <div class="content-header">
                                                <h2 class="title">
                                                    <a href="{{ route('space.detail',[$item->slug,$item->id]) }}" class="title__links">
                                                        <span class="span__name medium">
                                                            {{ $item->name }}
                                                        </span>
                                                        <p class="p__text">
                                                            <img class="img__link" src="images/icon_map.png"
                                                                alt="images/icon_map.png">
                                                            {{ ucwords($item->locationSpace->name) }}
                                                        </p>
                                                    </a>
                                                </h2>
                                                <p class="p__price">
                                                    <span class="span__name bold">
                                                        Giá chỉ từ:
                                                    </span>
                                                    <span class="span__price bold">
                                                        @if ($item->spaceAvailability->min('price'))
                                                            {{  number_format($item->spaceAvailability->min('price')). 'đ'  }}
                                                        @else
                                                            {{ !empty($item->sale_price) ? number_format($item->sale_price).'đ' : number_format($item->price).'đ'}}
                                                        @endif
                                                    </span>
                                                </p>
                                            </div>
                                            <div class="content-footer">
                                                <div class="text text__hidden">
                                                    <p class="p__time">
                                                        <span class="span__name medium">
                                                            Số giường ngủ: {{ $item->number_bed }}
                                                        </span>
                                                        <img src="images/icon_user_2.png" alt="icon_user_2s.png"
                                                            class="icon">
                                                    </p>
                                                </div>
                                                <div class="text text__hidden">
                                                    <p class="p__time">
                                                        <span class="span__name medium">
                                                            Số người tối đa: {{ $item->max_guests }}
                                                        </span>
                                                        <img src="images/icon_user_2.png" alt="icon_user_2s.png"
                                                            class="icon">
                                                    </p>
                                                </div>
                                                <div class="like-dvaluate">
                                                    <p class="p__like medium">
                                                        @if (!empty($item->rating_star))
                                                        {!! rating_star($item->rating_star) !!}
                                                        @endif
                                                    </p>
                                                </div>

                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                            <div class="fexPagintaion text-center clearfix">
                                {{ $listSpaces->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </section>
    <section class="section-domestic">
        <div class="bs-container">
            <div class="bs-row">
                <div class="bs-col">
                    <div class="module module-domestic">
                        <div class="module-header">
                            <h2 class="title">
                                Điểm đến yêu thích trong nước
                            </h2>
                            <p class="p__info">
                                Bao la khắp nước. Bốn bể là nhà
                            </p>
                        </div>
                        <div class="module-content">
                            <div class="grid">
                                @foreach ($listLocation as $item)
                                    <div class="grid-item">
                                        <a href="#" class="item__links" width="100%">
                                            <div class="avata" width="100%">
                                                @if(!empty($item->feature_image))
                                                    <img src="{{config('app.url')}}{{ $item->feature_image }}" alt="{{ $item->name }}" width="100%">
                                                @else
                                                    <img src="{{config('app.url')}}/uploads/images/default.png" alt="{{ $item->name }}" width="100%">
                                                @endif
                                            </div>
                                        </a>
                                        <div class="content">
                                            <h3 class="title">
                                                {{ $item->name }}
                                            </h3>
                                            <ul class="category">
                                                <li class="category-list">
                                                    <a href="#" class="category-list__links">
                                                        {{ sizeof($item->tour) }}
                                                        <span class="span_name">
                                                            Tours
                                                        </span>
                                                    </a>
                                                </li>
                                                <li class="category-list">
                                                    <a href="#" class="category-list__links">
                                                        {{ sizeof($item->hotel) }}
                                                        <span class="span_name">
                                                            Hotels
                                                        </span>

                                                    </a>
                                                </li>
                                                <li class="category-list">
                                                    <a href="#" class="category-list__links">
                                                        {{ sizeof($item->space) }}
                                                        <span class="span_name">
                                                            Spaces
                                                        </span>
                                                    </a>
                                                </li>
                                                 <li class="category-list">
                                                    <a href="#" class="category-list__links">
                                                        {{ sizeof($item->car) }}
                                                        <span class="span_name">
                                                            Cars
                                                        </span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
<script type="text/javascript">
        function favourite(id) {
            $.ajax({
                type: "GET",
                url: "/space-favourite/"+id ,
                success: function (response) {
                    $('#'+id).html(response.countFavourite).css('color',response.color);
                    $('#color'+id).css('color',response.color);
                }
            });
        }
        $(document).ready(function () {
        function checkRadido() {
            $('.label-category').click(function () {
                $(this).prevAll('.label-category').removeClass('active');
                $(this).nextAll('.label-category').removeClass('active');
                $(this).addClass('active');
                submitFormFilterSpace();
            });
        }

        function onDisplay() {
            $('.fa-th-large').click(function() {
                $('.fa-th-list').removeClass('active');
                $(this).addClass('active');
                $('.onDisplay').addClass('active');
                $('.open-show__avata').removeClass('lg-33 md-33 sm-33 xs-50 ');
                $('.open-show__content').removeClass('lg-66 md-66 sm-33 xs-50');
                $('.open-show__avata').addClass('lg-100 md-100 sm-100 xs-100');
                $('.open-show__content').addClass('lg-100 md-100 sm-100 xs-100');
            })
            $('.fa-th-list').click(function() {
                $('.onDisplay').removeClass('active');
                $('.fa-th-large').removeClass('active');
                $(this).addClass('active');
                $('.open-show__avata').removeClass('lg-100 md-100 sm-100 xs-100');
                $('.open-show__content').removeClass('lg-100 md-100 sm-100 xs-100');
                $('.open-show__avata').addClass('lg-33 md-33 sm-33 xs-50');
                $('.open-show__content').addClass('lg-66 md-66 sm-33 xs-50');
            });
        }

        function selecOption() {
            $('.select .header-option-location').click(function () {
                $(this).next('.body-option').toggleClass('active');
                var header = $(this);
                $(this).next('.body-option').children('.item-option').click(function () {
                    var html = $(this).html();
                    var id = $(this).attr('data-id');
                    header.html(html);
                    header.parent('.select').children('.location_id').val(id);
                    $('.body-option').removeClass('active');

                });
            });
            $('.select .header-option-car-form').click(function () {
                $(this).next('.body-option').toggleClass('active');
                var header = $(this);
                $(this).next('.body-option').children('.item-option').click(function () {
                    var html = $(this).html();
                    var id = $(this).attr('data-id');
                    header.html(html);
                    header.parent('.select').children('.car_form').val(id);
                    $('.body-option').removeClass('active');

                });
            });

        }
        function slideProduct() {
            $('.slider-for').slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: false,
                fade: false,
                asNavFor: '.slider-nav'
            });
            $('.slider-nav').slick({
                slidesToShow: 4,
                slidesToScroll: 1,
                asNavFor: '.slider-for',
                focusOnSelect: true,
            });
        }
        onDisplay();
        slideProduct();
        checkRadido();
        selecOption();

        $(document).on('click','.span__add',function(){
            var number = $('.span__show').html();
            let guests = $('.guests').html(number);
            let text = 'Số lượng khách: ' + number;

            $(".text-space-form").val(text);
            $('.number_guests').val(number);

        });

    $(document).on('click','.span__subtract',function(){
        var number = $('.span__show').html();
        let guests = $('.guests').html(number);
        let text = 'Số lượng khách: ' + number;

        $(".text-space-form").val(text);
        $('.number_guests').val(guests);
    });

        $(document).on('click', '.lable-radio-term-type', function(e) {
            var id = $(this).attr('data-id');
            if($(this).hasClass('active')) {
                $(this).removeClass('active');
                $('#termType-checkbox-'+id).prop('checked', false);
            }else{
                $(this).addClass('active');
                $('#termType-checkbox-'+id).prop('checked', true);
            }
            submitFormFilterSpace();
        });
        $(document).on('click', '.lable-radio-term-features', function(e) {
            var id = $(this).attr('data-id');
            if($(this).hasClass('active')) {
                $(this).removeClass('active');
                $('#spaceAmenity-checkbox-'+id).prop('checked', false);
            }else{
                $(this).addClass('active');
                $('#spaceAmenity-checkbox-'+id).prop('checked', true);
            }
            submitFormFilterSpace();
        });
        function submitFormFilterSpace (){
            $('#form-filter-space').submit();
        }
    });

</script>

@endsection

