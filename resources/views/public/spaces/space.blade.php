@extends('public.master')
@section('css')
@endsection
@section('content')
<style>
    .is-favourite{
        color: red;
    }
    .is-favourite-color{
        color: red;
    }
</style>
<main id="main">
    <section class="section-page">
        <div class="bs-container">
            <div class="bs-row">
                <div class="bs-col">
                    <div class="module module-page">
                        <div class="module-header">
                            <h2 class="title">
                                <span class="span__name black">
                                    Tìm kiếm ưu đãi khách sạn, chỗ nghỉ & nhiều hơn nữa...
                                </span>
                            </h2>
                            <div class="page__right">
                                <ul class="item">
                                    <li class="item-list">
                                        <a href="/" class="item-list__links">
                                            Trang chủ
                                        </a>
                                    </li>
                                    <li class="item-list">
                                        <span class="item-list__links">
                                            Space
                                        </span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="module-content">
                            <form action="{{ route('space.list') }}">
                                <div class="grid">
                                    <div class="item-grid">
                                        <div class="item-grid__header  clickShow">
                                            <div class="item_icon">
                                                <img src="images/icon_map.png" alt="icon_map.png">
                                            </div>
                                            <div class="item__content">
                                                <h3 class="title">
                                                    Điểm đến
                                                </h3>
                                                <p class="p__text addText">
                                                    <input type="text" class="text-location" placeholder="Nơi bạn muốn đến" readonly>
                                                    <input type="hidden" name="location_id" class="location_id">
                                                </p>
                                                <i class="fas fa-caret-down"></i>
                                            </div>
                                        </div>
                                        <div class="item-grid__content">
                                            <ul class="item">
                                                @foreach ($location as $item)
                                                <li data-id="{{ $item->id }}" class="item-list clickDestination">
                                                    <i class="fas fa-map-marker-alt"></i>
                                                    <span class="span__name ">
                                                        {{ $item->name }}
                                                    </span>
                                                </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="item-grid">
                                        <div class="item-grid__header">
                                            <div class="item_icon">
                                                <img src="images/icon_check.png" alt="icon_check.png">
                                            </div>
                                            <div class="item__content">
                                                <h3 class="title">
                                                    Check In - Check Out
                                                </h3>
                                                <p class="p__text acDates">

                                                    <input type="text" name="daterange" />
                                                </p>
                                                <i class="fas fa-caret-down"></i>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="item-grid">
                                        <div class="item-grid__header clickShow">
                                            <div class="item_icon">
                                                <img src="images/icon_us.png" alt="icon_us.png">
                                            </div>
                                            <div class="item__content">
                                                <h3 class="title">
                                                    Số lượng khách tối đa
                                                </h3>
                                                <p class="p__text">
                                                    <input type="text" class="text-space-form" placeholder="Số lượng khách" readonly>
                                                    <input type="hidden" name="numbeguests" class="number_guests" placeholder="Số lượng khách">
                                                </p>
                                                <i class="fas fa-caret-down"></i>
                                            </div>
                                        </div>
                                        <div class="item-grid__content">
                                            <ul class="item">
                                                <li data-id=1 class="item-list">
                                                    <span class="span__name bold ">
                                                        Số lượng khách: <span class="guests"></span>
                                                    </span>
                                                    <div class="number">
                                                        <span class="span span__subtract bold">
                                                            -
                                                        </span>
                                                        <span class="span span__show">
                                                            1
                                                        </span>
                                                        <span class="span span__add bold">
                                                            +
                                                        </span>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="item-grid">
                                        <button class="button">
                                            <img src="/images/icon_search.png" alt="icon_search.png">
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="section-resort">
        <div class="bs-container">
            <div class="bs-row">
                <div class="bs-col">
                    <div class="module module-resort ">
                        <div class="module-header">
                            <h2 class="title">
                                loại hình nghỉ dưỡng
                            </h2>
                            <p class="p__click">
                                Để xem tất cả space, vui lòng
                                <a href="{{ route('space.list') }}" class="click__links">
                                    click vào đây
                                </a>
                            </p>
                        </div>
                        <div class="module-content">
                            <div class="slide-resort">
                                @foreach ($spaces as $space)
                                <div class="item">
                                    <div class="resort">
                                        <a href="{{ route('space.detail',[$space->slug,$space->id]) }}" class="link"></a>
                                        <div class="avata">
                                            <img src="{{ config('app.url') }}{{ $space->banner }}" alt="resort_1.jpg">
                                        </div>
                                        <div class="content">
                                            <h3 class="h3__title black">
                                                {{ $space->name }}
                                            </h3>
                                            {{-- <p class="p__text black">
                                                432,222 {{ $space->name }}
                                            </p> --}}
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </section>
    <section class="section-favourite">
        <div class="bs-container">
            <div class="bs-row">
                <div class="bs-col">
                    <div class="module module-favourite">
                        <div class="module-header">
                            <h2 class="title">
                                Nơi ở mà khách yêu thích
                            </h2>
                            {{-- <p class="p__click">
                                Để xem tất cả nơi ở được yêu thích, vui lòng
                                <a href="#" class="click__links">
                                    click vào đây
                                </a>
                            </p> --}}
                        </div>
                        <div class="module-content">
                            <div class="bs-row row-lg-10 row-md-10 row-sm-5 row-xs-5 row-tn">
                            @foreach ($spacesFavourite as $space)
                                <div class="bs-col lg-25-10 md-25-10 sm-33-5 xs-50-5 tn-100">
                                    <div class="favourite">
                                        <div class="avata">
                                            <a class="span__love" onclick="favourite({{ $space->id }})">
                                                <i class="fas fa-heart {{ Auth::check() ? ($space->isFavouritesBy(auth()->user()) ? 'is-favourite' : ' ') : ' ' }}"
                                                    {{ Auth::check() == false ? ' modal-show=show modal-data=#login ' : ' ' }}
                                                id="color{{ $space->id }}"></i>
                                                <span class="favourite {{ Auth::check() ? ($space->isFavouritesBy(auth()->user()) ? 'is-favourite-color' : ' '): ' ' }}" id="{{ $space->id }}"
                                                    {{ Auth::check() == false ? ' modal-show=show modal-data=#login ' : ' ' }}
                                                >{{ sizeOf($space->spaceFavourites) }}</span>
                                            </a>
                                            @if(isset($space->sale_price))
                                            <span class="span__sale">
                                                <span class="number">
                                                    {{
                                                        '-'.floor((($space->price - $space->sale_price)/$space->price)*100).'%'
                                                    }}
                                                </span>
                                            </span>
                                            @endif
                                            <a href="{{ route('space.detail',[$space->slug, $space->id]) }}" class="item__links"></a>
                                            <img src="{{ config('app.url') }}{{ $space->banner }}" class="img__links" alt="favourite_1">
                                        </div>
                                        <div class="content">
                                            <h3 class="title">
                                                <a href="{{ route('space.detail',[$space->slug, $space->id]) }}" class="title__links medium">
                                                   {{ $space->name }}
                                                </a>
                                            </h3>
                                            <p class="p__text">
                                                Giá chỉ từ
                                                @if (isset($space->sale_price))
                                                    <span class="span__name">
                                                        {{ number_format($space->sale_price).' '.'đ' }}
                                                    </span>
                                                @else
                                                <span class="span__name">
                                                    {{ number_format($space->price).' '.'đ' }}
                                                </span>
                                                @endif
                                                {{-- /đêm --}}
                                            </p>
                                            <div class="content__footer">
                                                <p class="p__left">
                                                    <img src="images/icon_map.png" alt="icon_map.png" class="map_img">
                                                    <span class="span__namee">
                                                        @if (isset($space->locationSpace->name))
                                                        {{ ucwords($space->locationSpace->name)}}
                                                        @endif
                                                    </span>
                                                </p>
                                                <p class="p__right medium">
                                                    @if (isset($space->rating_star))
                                                    {!! rating_star($space->rating_star) !!}
                                                    @endif

                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="section-domestic">
    <div class="bs-container">
        <div class="bs-row">
            <div class="bs-col">
                <div class="module module-domestic">
                    <div class="module-header">
                        <h2 class="title">
                            Điểm đến yêu thích trong nước
                        </h2>
                        <p class="p__info">
                            Bao la khắp nước. Bốn bể là nhà
                        </p>
                    </div>
                    {{-- @foreach ($highlightSpace as $space) --}}
                    <div class="module-content">
                        <div class="grid">
                            @foreach ($highlightSpace as $location)
                            <div class="grid-item">
                                {{-- {{ config('app.url') }}{{  }} --}}
                                <a href="#" class="item__links">
                                    <div class="avata">
                                        <img src="{{ config('app.url') }}{{ $location->feature_image }}" alt="domestic_1.png">
                                    </div>
                                </a>
                                <div class="content">
                                    <h3 class="title">
                                        {{ $location->name }}
                                    </h3>
                                    <ul class="category">
                                        <li class="category-list">
                                            <a href="#" class="category-list__links">
                                                {{ sizeof($location->tour) }}
                                                <span class="span_name">
                                                    Tours
                                                </span>
                                            </a>
                                        </li>
                                        <li class="category-list">
                                            <a href="#" class="category-list__links">
                                                {{ sizeof($location->space) }}
                                                <span class="span_name">
                                                    Spaces
                                                </span>
                                            </a>
                                        </li>
                                        <li class="category-list">
                                            <a href="#" class="category-list__links">
                                                {{ sizeof($location->hotel) }}
                                                <span class="span_name">
                                                    Hotels
                                                </span>

                                            </a>
                                        </li>
                                        <li class="category-list">
                                            <a href="#" class="category-list__links">
                                                {{ sizeof($location->car) }}
                                                <span class="span_name">
                                                    Cars
                                                </span>
                                            </a>
                                        </li>

                                    </ul>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
</main>

    <script src="js/tool.min.js"></script>
    <script type="text/javascript" src="js/moment.min.js"></script>
    <script type="text/javascript" src="js/daterangepicker.min.js"></script>
    <script src="js/jquery.countdown.min.js"></script>
    <script src="js/main.min.js"></script>
    <script type="text/javascript">

    function favourite(id) {
        $.ajax({
            type: "GET",
            url: "/space-favourite/"+id ,
            success: function (response) {
                $('#'+id).html(response.countFavourite).css('color',response.color);
                $('#color'+id).css('color',response.color);
            }
        });
    }

    $(document).ready(function () {
        function slideResort() {
            $(".slide-resort").slick({
                slidesToShow: 3,
                slidesToScroll: 3,
                adaptiveHeight: false,
                prevArrow: '<i class="icon_control fas fa-angle-left"></i>',
                nextArrow: '<i class="icon_control fas fa-angle-right"></i>',
                arrows: true,
                infinite: false,
                rows: 2,
                responsive: [{

                    breakpoint: 991.9,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1,
                        adaptiveHeight: false,
                        infinite: true,
                        row:2,
                    }
                },

                {

                    breakpoint: 479.9,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        adaptiveHeight: true,
                        infinite: true
                    }
                }

                ]
            });
        }
        slideResort();
    });
    $(document).on('click', '.clickDestination', function(e) {
        let text = $(this).text().trim();
        let id = $(this).attr('data-id');
        $(".text-location").val(text);
        $(".location_id").val(id);
        $(".item-grid__content").removeClass("active");
    });

    $(document).on('click','.span__add',function(){
        var number = $('.span__show').html();
        let guests = $('.guests').html(number);
        let text = 'Số lượng khách: ' + number;

        $(".text-space-form").val(text);
        $('.number_guests').val(number);

    });

    $(document).on('click','.span__subtract',function(){
        var number = $('.span__show').html();
        let guests = $('.guests').html(number);
        let text = 'Số lượng khách: ' + number;

        $(".text-space-form").val(text);
        $('.number_guests').val(guests);
    });

</script>

@endsection
