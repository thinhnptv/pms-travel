<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}" >
    <title>Traveling </title>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="icon" href="images/favicon.ico" type="image/png">

    <link rel="stylesheet" type="text/css" href="{{url('/pms/css/tool.min.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{url('/pms/css/main.min.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{url('/pms/css/upgrade.min.css')}}" />
    <link rel="stylesheet" href="{{url('/pms/css/main.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('/pms/css/daterangepicker.css')}}" />
    {{-- <link rel="stylesheet" href="{{url('/assets/libs/jquery-toast/jquery.toast.css')}}"> --}}
    <link rel="stylesheet" href="{{url('/pms/css/web.css')}}">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.0/jquery.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="{{ URL::asset('assets/libs/toastrnew/toastr.min.css')}}" rel="stylesheet" type="text/css" />
    @yield('css-top')
</head>

<body class="index">

   @include('public.header')
   {{-- @include('public.banner') --}}
    @yield('content')
   @include('public.footer')






    <script src="{{url('/pms/js/tool.min.js')}}"></script>

    <script type="text/javascript" src="{{url('/pms/js/moment.min.js')}}"></script>
    <script type="text/javascript" src="{{url('/pms/js/daterangepicker.min.js')}}"></script>
    <script src="{{url('/pms/js/jquery.countdown.min.js')}}"></script>
    <script src="{{url('/pms/js/main.min.js')}}"></script>
    {{-- <script src="{{url('/assets/libs/jquery-toast/jquery.toast.js')}}"></script> --}}
    <script src="{{ URL::asset('assets/libs/toastrnew/toastr.min.js')}}"></script>


{{--
@if (session('message_error'))
<script>
    $(document).ready(function() {
        $.toast({
            heading: 'Error',
            text: "{{ session('message_error') }}",
            showHideTransition: 'fade',
            icon: 'error'
        });
    });
</script>
@endif --}}
{{--
@if (session('login_error'))
<script>
    $(document).ready(function() {
        $('#login').modal('show');
    });
</script>
@endif --}}

{{-- @if (session('message_success'))
<script>
    $(document).ready(function() {
        $.toast({
            heading: 'Success',
            text: "{{ session('message_success') }}",
            showHideTransition: 'fade',
            icon: 'success'
        });
    });
</script>
@endif --}}

<script type="text/javascript">
    $(document).ready(function() {
        $('.custome__checkbox').click(function() {
            $(this).toggleClass('active');
        });
    });
    </script>
    @yield('script')
    @stack('js')
    <script>
        $(document).on('click', '.btn-dangky', function(e){
            e.preventDefault();
            const __this = this;
            $(__this).prop('disabled', true);

            var firstName = $('.firstname').val();
            var lastName = $('.lastname').val();
            var email = $('.email').val();
            var password = $('.password').val();
            var passwordConfirmation = $('.password_confirmation').val();
            var approved;
            if($('.approved').is(':checked',true))
            {
                approved = 1;
            } else {
                approved = null;
            }
            if(firstName == '') {
                toastr.error('Tên không được để trống');
                $(__this).prop('disabled', false);
                return false;
            }
            if(firstName.length < 2) {
                toastr.error('Tên không được nhỏ hơn 2 ký tự');
                $(__this).prop('disabled', false);
                return false;
            }
            if(firstName.length > 15) {
                toastr.error('Tên không được lớn hơn 15 ký tự');
                $(__this).prop('disabled', false);
                return false;
            }
            if(lastName == "") {
                toastr.error('Họ không được để trống');
                $(__this).prop('disabled', false);
                return false;
            }
            if(lastName.length < 2) {
                toastr.error('Họ không được nhỏ hơn 2 ký tự');
                $(__this).prop('disabled', false);
                return false;
            }
            if(lastName.length > 15) {
                toastr.error('Họ không được lớn hơn 15 ký tự');
                $(__this).prop('disabled', false);
                return false;
            }
            if(email == "") {
                toastr.error('Email không được để trống');
                $(__this).prop('disabled', false);
                return false;
            }
            var re = /[a-z][a-z0-9_\.]{3,32}@[a-z0-9]{2,}(\.[a-z0-9]{2,4}){1,2}(\.[0-9]{2,4}){0,2}/igm;
            if (!re.test(email))
            {
                toastr.error('Email bạn nhập không đúng định dạng');
                $(__this).prop('disabled', false);
                return false;
            }
            if(password == "") {
                toastr.error('Mật khẩu không được để trống');
                $(__this).prop('disabled', false);
                return false;
            }
            if(password.length < 6) {
                toastr.error('Mật khẩu phải từ 6 ký tự trở lên');
                $(__this).prop('disabled', false);
                return false;
            }
            if(password.length > 15) {
                toastr.error('Mật khẩu phải dưới 15 ký tự ');
                $(__this).prop('disabled', false);
                return false;
            }
            if(passwordConfirmation == "") {
                toastr.error('Mật khẩu nhập lại không được để trống');
                $(__this).prop('disabled', false);
                return false;
            }
            if(passwordConfirmation  != password) {
                toastr.error('Mật khẩu nhập lại phải trùng mật khẩu cũ');
                $(__this).prop('disabled', false);
                return false;
            }
            if(approved  == null) {
                toastr.error('Bạn phải đồng ý các điều khoản trước khi đăng ký!');
                $(__this).prop('disabled', false);
                return false;
            }

            const __token = $('meta[name="csrf-token"]').attr('content');
            data_ = {
                firstname: firstName,
                lastname: lastName,
                email: email,
                password: password,
                password_confirmation: passwordConfirmation,
                checkbox: approved,
                _token: __token
            }
            var request = $.ajax({
                url: '{{route('auth.register')}}',
                type: 'POST',
                data: data_,
                dataType: "json"
            });
            request.done(function (msg) {
                if(msg.type == 1) {
                    toastr.success(msg.mess);
                    $(__this).prop('disabled', false);
                    location.reload();
                }else {
                    toastr.error(msg.mess);
                    $(__this).prop('disabled', false);
                }

                return false;
            });
            request.fail(function (errors) {

                var is_error =Object.values(errors.responseJSON.errors);
                toastr.error(is_error[0][0]);
                $(__this).prop('disabled', false);
                return false;
            });
        })
    </script>
    <script>
        $(document).on('click', '.btn-dangnhap', function(e){
            e.preventDefault();
            const __this = this;
            $(__this).prop('disabled', true);

            var email = $('.email-dn').val();
            var password = $('.password-dn').val();
            if(email == "") {
                toastr.error('Email không được để trống');
                $(__this).prop('disabled', false);
                return false;
            }
            var re = /^[a-z][a-z0-9_\.]{3,32}@[a-z0-9]{2,}(\.[a-z0-9]{2,4}){1,2}(\.[0-9]{2,4}){0,2}$/igm;
            if (!re.test(email))
            {
                toastr.error('Email bạn nhập không đúng định dạng');
                $(__this).prop('disabled', false);
                return false;
            }
            if(password == "") {
                toastr.error('Mật khẩu không được để trống');
                $(__this).prop('disabled', false);
                return false;
            }
            if(password.length < 6) {
                toastr.error('Mật khẩu phải từ 6 ký tự trở lên');
                $(__this).prop('disabled', false);
                return false;
            }
            const __token = $('meta[name="csrf-token"]').attr('content');
            data_ = {
                email: email,
                password: password,
                _token: __token
            }
            var request = $.ajax({
                url: '{{route('auth.login')}}',
                type: 'POST',
                data: data_,
                dataType: "json"
            });
            request.done(function (msg) {
                if(msg.type == 1) {
                    toastr.success(msg.mess);
                    $(__this).prop('disabled', false);
                    location.reload();
                }else if(msg.type == 3){
                    toastr.error(msg.mess);
                    $(__this).prop('disabled', false);
                }

                return false;
            });
            request.fail(function (errors) {
                var is_error =Object.values(errors.responseJSON.errors) ;
                var t_error = is_error.map(function(item){
                    return item;
                });
                toastr.error(t_error[0][0]);
                $(__this).prop('disabled', false);
                return false;
            });
        })
    </script>
</body>

</html>
