@extends('public.master')
@section('content')
<link rel="stylesheet" href="{{ url('/pms/css/web.css') }}">
<link rel="stylesheet" href="{{ url('/assets/css/webt.css') }}">

<main id="main">
    <section class="section-list__new">
        <div class="bs-container">
            <div class="bs-row">
                <div class="bs-col">
                    <div class="module module-list__new">
                        <div class="module-content">
                            <div class="bs-row row-lg-15">
                                <div class="bs-col lg-100-15">
                                    <ul class="page">
                                        <li class="page-list">
                                            <a href="new-detail.html" class="page-list__link">
                                                Trang chủ
                                            </a>
                                        </li>
                                        <li class="page-list">
                                            <sapn href="#" class="page-list__link">
                                                Liên hệ với Traveling
                                                </span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    {{-- @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @if (session('errorsss'))
        <div class="alert alert-danger">
            {{ session('errorsss') }}
        </div>
    @endif --}}
    <div class="notification">
        @if (session('message_success'))
            <div class="alert alert-success">
                {{ session('message_success') }}
            </div>
        @endif
    </div>
    <section class="section-contact">
        <div class="bs-container">
            <div class="bs-row">
                <div class="bs-col">
                    <div class="module module-contact">
                        <div class="module">
                            <div class="goole-map" style="width:100%">
                                {{-- <iframe
                                    src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3725.798009964506!2d105.7982509145082!3d20.960625295477634!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135ad21667e0557%3A0xb8e1dd08a4c200e7!2sNguyenquyen!5e0!3m2!1svi!2s!4v1584433140523!5m2!1svi!2s"
                                    width="100%" height="362" frameborder="0" style="border:0;" allowfullscreen=""
                                    aria-hidden="false" tabindex="0"></iframe> --}}
                                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d14896.366655290045!2d105.8350796697754!3d21.029018000000015!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135ab964dc6ec95%3A0x349d350204c693dc!2zS2jDoWNoIFPhuqFuIEhhbm9pIFN0cmVldA!5e0!3m2!1svi!2s!4v1591085539763!5m2!1svi!2s" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                            </div>
                            <div class="contact-text">
                                {{-- @php
                                    dd($contactSetting);
                                @endphp --}}
                                <h3 class="title medium">
                                    MỌI THÔNG TIN CHI TIẾT VUI LÒNG LIÊN HỆ
                                </h3>
                                <ul class="item">
                                    <li class="item-list">
                                        <span class="span__name">
                                            Văn phòng công ty:
                                        </span>
                                        <span class="span__info light">
                                            {{ $contactSetting->company_office }}
                                        </span>
                                    </li>
                                    <li class="item-list">
                                        <span class="span__name">
                                            Tel:
                                        </span>
                                        <span class="span__info bold red">
                                            {{ $contactSetting->tel }}
                                        </span>
                                    </li>
                                    <li class="item-list">
                                        <span class="span__name">
                                            Fax:
                                        </span>
                                        <span class="span__info light">
                                            {{ $contactSetting->fax }}
                                        </span>
                                    </li>
                                    <li class="item-list">
                                        <span class="span__name">
                                            Email:
                                        </span>
                                        <span class="span__info light">
                                            {{ $contactSetting->email }}
                                        </span>
                                    </li>
                                </ul>
                            </div>
                            <div class="contact-form">
                                <h3 class="title">
                                    {{ $contactSetting->title }}
                                </h3>
                                <p class="p__info">
                                    {{ $contactSetting->sub_title }}
                                </p>

                                <form action="{{ route('post.lienhe') }}" method="POST" role="form">
                                    @csrf
                                    <div class="form-group">
                                        <div class="bs-row row-lg-10 row-md-10 row-sm-5 row-xs-5">
                                            <div class="bs-col lg-33-10 md-33-10 sm-33-5 xs-33-5 tn-100">
                                                <label for="full_name">Họ & Tên <span class="text-danger">*</span></label>
                                                <div class="conainer-box">
                                                    <input type="text" class="form-control input__text" name="full_name" placeholder="Họ & Tên " value="{{ old('full_name') }}">
                                                    <p class="help is-danger">{{ $errors->first('full_name') }}</p>
                                                </div>
                                            </div>
                                            <div class="bs-col lg-33-10 md-33-10 sm-33-5 xs-33-5 tn-100">
                                                <label for="mail">Địa chỉ email <span class="text-danger">*</span></label>
                                                <div class="conainer-box">
                                                    <input type="text" class="form-control input__text" name="mail" placeholder="Địa chỉ email" value="{{ old('mail') }}">
                                                    <p class="help is-danger">{{ $errors->first('mail') }}</p>
                                                </div>
                                            </div>
                                            <div class="bs-col lg-33-10 md-33-10 sm-33-5 xs-33-5 tn-100">
                                                <label for="phone">Điện thoại <span class="text-danger">*</span></label>
                                                <div class="conainer-box">
                                                    <input type="text" class="form-control input__text" name="phone" placeholder="Điện thoại " value="{{ old('phone') }}">
                                                    <p class="help is-danger">{{ $errors->first('phone') }}</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="bs-row row-lg-10 row-md-10 row-sm-5 row-xs">
                                            <div class="bs-col lg-100-10 md-100-10 sm-100-5 xs-100">
                                                <label for="title">Tiêu đề </label>
                                                <div class="conainer-box">
                                                    
                                                    <input type="text" class="form-control" name="title" placeholder="Tiêu đề " value="{{ old('title') }}">
                                                    {{-- <p class="help is-danger">{{ $errors->first('title') }}</p> --}}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="bs-row row-lg-10 row-md-10 row-sm-5 row-xs">
                                            <div class="bs-col lg-100-10 md-100-10 sm-100-5 xs-100">
                                                <label for="content">Nội dung </label>
                                                <div class="conainer-box textarea ">
                                                    
                                                    <textarea class="form-control" name="content" placeholder="Nội dung">{{ old('content') }}</textarea>
                                                    {{-- <p class="help is-danger">{{ $errors->first('content') }}</p> --}}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    {{-- <div class="form-group">
                                        <div class="bs-row row-lg-10 row-md-10 row-sm-5 row-xs">
                                            <div class="bs-col lg-100-10 md-100-10 sm-100-5 xs-100">
                                                <div class="conainer-box">
                                                    <label for="">Mã bảo vệ</label>
                                                    <input type="text" class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                    </div> --}}
                                    <button type="submit" class="btn btn__sen bold">Gửi thông tin</button>
                                    <button type="reset" class="btn btn__reset bold">Làm mới</button>
                                    {{-- @include('public.errors') --}}
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
<script>
    $(document).ready(function () {
        function onChageInput() {
            $('.conainer-box  .form-control').click(function () {
                $(this).prev('label').addClass('active');
                $(this).blur(function () {
                   var hasText= $(this).val();
                   if(hasText ==""){
                    $(this).prev('label').removeClass('active');
                   }

                });

            })
        }
        onChageInput();
    });
    $( document ).ready(function() {
            var checkNotification =  $('.notification').find('.alert-success').length;
            if(checkNotification != 0) {
                    setTimeout(function(){
                        $('.notification').empty();
                    }, 3000);
            }
        });
</script>
@endsection