@extends('public.master')
@section('content')

<link rel="stylesheet" href="{{ url('/pms/css/web.css') }}">

<div class="legal_doc_title">
    @if (!empty( $termAndPrivacyPolicy))
        <h1>{{ $termAndPrivacyPolicy->title }}</h1>
    @endif
</div><br>
<div class="container" style="margin-top: 150px;">
    @if (!empty( $termAndPrivacyPolicy))
        {!! $termAndPrivacyPolicy->content !!}
    @endif
</div>


@endsection