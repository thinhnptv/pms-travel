@extends('layouts.master-without-nav')

@section('body')
    <body class="authentication-bg authentication-bg-pattern">
@endsection

@section('content')

        <div class="account-pages mt-5 mb-5">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-8 col-lg-6 col-xl-5">
                        <div class="card bg-pattern">
                            <div class="card-body p-4">
                                <div class="text-center w-75 m-auto">
                                    <a href="index">
                                        <span><img src="assets/images/logo-dark.png" alt="" height="22"></span>
                                    </a>
                                    <p class="text-muted mb-4 mt-3">Enter your email address and password to access admin panel.</p>
                                </div>
                            <form action="{{ route('post.admin_login') }}" method="POST">
                                    @csrf
                                    <div class="notification">
                                        @if (session('fail'))
                                            <div class="alert alert-danger">
                                                {{ session('fail') }}
                                            </div>
                                        @endif
                                    </div>
                                    <div class="form-group mb-3">
                                        <label for="emailaddress">Email address</label>
                                        <input class="form-control email" type="email" name="email" required="" placeholder="Enter your email">
                                    </div>
                                    <div class="form-group mb-3">
                                        <label for="password">Password</label>
                                        <input class="form-control password" type="password" required="" name="password" placeholder="Enter your password">
                                    </div>

                                    {{-- <div class="form-group mb-3">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="checkbox-signin" checked>
                                            <label class="custom-control-label" for="checkbox-signin">Remember me</label>
                                        </div>
                                    </div> --}}

                                    <div class="form-group mb-0 text-center">
                                    <button class="btn btn-primary btn-block btn-login-admin" type="submit" data-urltrue="{{ route('base.index') }}"> Log In </button>
                                    </div>

                                </form>
                                {{-- @if(Auth::check())
                                    <a href="{{ route('get.admin_logout') }}" class="btn btn-primary">Logout</a>
                                @endif --}}
                                {{-- <div class="text-center">
                                    <h5 class="mt-3 text-muted">Sign in with</h5>
                                    <ul class="social-list list-inline mt-3 mb-0">
                                        <li class="list-inline-item">
                                            <a href="javascript: void(0);" class="social-list-item border-primary text-primary"><i class="mdi mdi-facebook"></i></a>
                                        </li>
                                        <li class="list-inline-item">
                                            <a href="javascript: void(0);" class="social-list-item border-danger text-danger"><i class="mdi mdi-google"></i></a>
                                        </li>
                                        <li class="list-inline-item">
                                            <a href="javascript: void(0);" class="social-list-item border-info text-info"><i class="mdi mdi-twitter"></i></a>
                                        </li>
                                        <li class="list-inline-item">
                                            <a href="javascript: void(0);" class="social-list-item border-secondary text-secondary"><i class="mdi mdi-github-circle"></i></a>
                                        </li>
                                    </ul>
                                </div> --}}

                            </div> <!-- end card-body -->
                        </div>
                        <!-- end card -->

                        {{-- <div class="row mt-3">
                            <div class="col-12 text-center">
                                <p> <a href="pages-recoverpw" class="text-white-50 ml-1">Forgot your password?</a></p>
                                <p class="text-white-50">Don't have an account? <a href="pages-register" class="text-white ml-1"><b>Sign Up</b></a></p>
                            </div> <!-- end col -->
                        </div> --}}
                        <!-- end row -->

                    </div> <!-- end col -->
                </div>
                <!-- end row -->
            </div>
            <!-- end container -->
        </div>
        <!-- end page -->


        <footer class="footer footer-alt">
            2015 - {{date('Y')}} &copy; UBold theme by <a href="" class="text-white-50">Coderthemes</a> 
        </footer>
@endsection
@section('script-bottom')
    <script type="text/javascript">
     $(document).on('click', '.btn-login-admin', function (e) {
        e.preventDefault();
        const __this = this;
        $(__this).prop('disabled', true);
        var email = $('.email').val();
        var password = $('.password').val();
        var __token = $('input[name="_token"]').val();
        if(email == "") {
            toastr.warning('Email không được để trống!');
            $(__this).prop('disabled', false);
            return false;
        }
        if(password == '') {
            toastr.warning('Password không được để trống!');
            $(__this).prop('disabled', false);
            return false;
        }
        data_ = {
                email: email,
                password: password,
                _token: __token
            }
        var request = $.ajax({
            url: '{{route('post.admin_login')}}',
            type: 'POST',
            data: data_,
            dataType: "json"
        });
        request.done(function (msg) {
            if(msg.type == 1) {
                var urlTrue = $(__this).attr('data-urltrue');
                toastr.success(msg.mess);
                location.href = urlTrue;
                $(__this).prop('disabled', false);
            }else if(msg.type == 2) {
                toastr.error(msg.mess);
                $(__this).prop('disabled', false);
            }
            
            return false;
        });
        request.fail(function (errors) {
            var is_error =Object.values(errors.responseJSON.errors) ;
            toastr.error(is_error[0][0]);
            $(__this).prop('disabled', false);
            return false;
        });
    })
    </script>
    
@endsection