<?php


if (!function_exists('check_size_image_default')) {
    function check_size_image_default($url)
    {
        if (file_exists($url)) {
            $infor_image = getimagesize($url);
            $width = (int)$infor_image[0];
            $height = (int)$infor_image[1];
            $mime = $infor_image['mime'];
            $mime_origin = ['image/jpeg', 'image/jpg', 'image/png'];
            if ($width < 3000 && in_array($mime, $mime_origin)) {
                return true;
            }
        } else {
        return false;
        }

    }
}
if (!function_exists('checkImage')) {
    function checkImage($param)
    {
        $file_url = parse_url($param);
        $url = public_path($file_url['path']);
        return $file_url['path'];
    }
}
if (!function_exists('checkMultipleImage')) {
    function checkMultipleImage($param)
    {
        $str_request_album = json_decode($param, true);
        $str_album = "[\"";
        foreach($str_request_album as $item) {
            $file_url = parse_url($item);
            $url = public_path($file_url['path']);
            $str_album .= $file_url['path'] . '","';
        }
        $str_album = substr($str_album, 0, -2);
        $str_album .= "]";
        return $str_album;
    }
}

if (!function_exists('getNumberDateFromStartAndEndDate')) {
    function getNumberDateFromStartAndEndDate($start_date, $end_date)
    {
        $num_date = ((strtotime($end_date) - strtotime($start_date)) / 86400); //số ngày
        return $num_date;
    }
}

if (!function_exists('getTotalBookingRoom')) {
    function getTotalBookingRoom($param, $service_fee = 100) //tính giá
    {

        $num_date = getNumberDateFromStartAndEndDate($param->start_date, $param->end_date);
        $total = $param->price * $param->number * $num_date;
        foreach($param->extraPriceBooked as $item) {
            $total += $item->price;
        }
        $total += $service_fee;
        return $total;
    }
}

if (!function_exists('getNumberDateFromStartAndEndDateBookCar')) {
    function getNumberDateFromStartAndEndDateBookCar($start_date, $end_date)
    {
        $num_date = ((strtotime($end_date) - strtotime($start_date)) / 86400) + 1;
        return $num_date;
    }
}
if (!function_exists('getTotalBookingCar')) {
    function getTotalBookingCar($param, $equipment_fee = 100, $facility_fee = 200)
    {

        $num_date = getNumberDateFromStartAndEndDateBookCar($param->start_date, $param->end_date);
        $total = $param->price * $param->number * $num_date;
        foreach($param->extraPriceBooked as $item) {
            $total += $item->price;
        }
        $total += $equipment_fee;
        $total += $facility_fee;
        return $total;
    }
}
if (!function_exists('getTotalBookingTour')) {
    function getTotalBookingTour($param, $service_fee = 100)
    {
        $total = 0;
        if(!$param->extraPriceBooked->isEmpty()) {
            foreach($param->extraPriceBooked as $item) {
                $total += $item->price;
            }
        }
        if($param->is_person_type == 1 && !$param->personTypeBooked->isEmpty()) {
            foreach($param->personTypeBooked as $item) {
                $total += $item->number * $item->price;
            }
        }else {
            $total += $param->number * $param->price;
        }

        $total += $service_fee;
        return $total;
    }
}
if (!function_exists('showStatusSpace')) {
    function showStatusSpace($param)
    {
        $arrStatus = [
            'Approved',
            'Pending',
            'Spam',
            'Trash',
        ];
        return $arrStatus[$param];
    }
}

if (!function_exists('showStatusBooking')) {
    function showStatusBooking($param)
    {
        $arrStatus = [
            'Completed',
            'Processing',
            'Confirmed',
            'Cancelled',
            'Paid',
            'Unpaid',
            'Partial Payment'
        ];
        return $arrStatus[$param];
    }
}

if (!function_exists('showPaymentMethod')) {
    function showPaymentMethod($param)
    {
        $arrPaymentMethod = [
            'Offline Payment'
        ];
        return $arrPaymentMethod[$param];
    }
}

if (!function_exists('getTotalBookingSpace')) {
    function getTotalBookingSpace($param, $cleaning_fee = 100, $service_fee = 200)
    {
        $total = 0;
        foreach($param->spaceExtraPriceBooking as $key => $val) {
            $total += $val->price;
        }
        $total += $cleaning_fee;
        $total += $service_fee;
        return $total;
    }
}
if (!function_exists('hour_of_day')) {
    function hour_of_day($value = '')
    {
        $strOpition = '';
        $arrHour = [
            '00:00',
            '01:00',
            '02:00',
            '03:00',
            '04:00',
            '05:00',
            '06:00',
            '07:00',
            '08:00',
            '09:00',
            '10:00',
            '11:00',
            '12:00',
            '13:00',
            '14:00',
            '15:00',
            '16:00',
            '17:00',
            '18:00',
            '19:00',
            '20:00',
            '21:00',
            '22:00',
            '23:00'
        ];
        foreach($arrHour as $item) {
            if($value == $item) {
                $strOpition .= '<option selected value="'. $item .'">'. $item .'</option>';
            }else {
                $strOpition .= '<option value="'. $item .'">'. $item .'</option>';
            }
        }
        return $strOpition;
    }
}
if (!function_exists('day_of_week')) {
    function day_of_week($value)
    {
        $arrDay = [
            1 => 'Monday',
            2 => 'Tuesday',
            3 => 'Wednesday',
            4 => 'Thursday',
            5 => 'Friday',
            6 => 'Saturday',
            0 => 'Sunday',
        ];

        return $arrDay[$value];
    }
}

if (!function_exists('rating_star')){
    function rating_star($value = 0)
    {
        $array = [
            0 => '',
            1 => '<i class="fas fa-star"></i>',
            2 => ' <i class="fas fa-star"></i><i class="fas fa-star"></i>',
            3 => '<i class="fas fa-star"></i>
                <i class="fas fa-star"></i>
                <i class="fas fa-star"></i>',
            4 => '<i class="fas fa-star"></i>
                <i class="fas fa-star"></i>
                <i class="fas fa-star"></i>
                <i class="fas fa-star"></i>',
            5 => ' <i class="fas fa-star"></i>
            <i class="fas fa-star"></i>
            <i class="fas fa-star"></i>
            <i class="fas fa-star"></i>
            <i class="fas fa-star"></i>'
        ];
        return $array[$value];
    }
}

