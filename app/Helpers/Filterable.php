<?php
namespace App\Helpers;

use Illuminate\Support\Str;

trait Filterable
{

    /**
     * Trường hợp filter theo fields
     * @var array
     */
//    protected $filterable = ['user_name'];

    /**
     * Trường hợp cấu hình sẵn key và value cần filter
     */
    // protected $filterable = [
    //  'user_name' => 'ducpanda'
    // ];

    public function scopeFilter($query, $param)
    {
        foreach ($param as $field => $value) {
            $method = 'filter' . Str::studly($field);

            if ($value === '') {
                continue;
            }

            if (method_exists($this, $method)) {
                $this->{$method}($query, $value);
                continue;
            }

            if (empty($this->filterable) || !is_array($this->filterable)) {
                continue;
            }

            if (in_array($field, $this->filterable)) {
                $query->where($this->table . '.' . $field, $value);
                continue;
            }

            if (key_exists($field, $this->filterable)) {
                $query->where($this->table . '.' . $this->filterable[$field], $value);
                continue;
            }
        }

        return $query;
    }

    /**
     * option filter đặc biệt với field là keyword
     * @param $query
     * @param $param
     * @return mixed
     */
    public function filterKeyword($query, $param)
    {
        return $query->where(function ($q) use ($param) {
            $q->where('name', 'LIKE', '%' . $param . '%')->orwhere('name', 'LIKE', '%' . str_slug($param, ' ') . '%');
            return $q;
        });
    }

    public function filterSortBy($query, $param)
    {
        return $query->orderBy($this->table . '.' . $param, (request()->get('sortDesc') == 'true') ? 'DESC' : 'ASC');
    }
}
