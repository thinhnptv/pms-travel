<?php 
namespace App\Helpers;

use Illuminate\Http\Request;
/**
 * summary
 */
class HelperClass
{
    /**
     * summary
     */
    private $radio = '';

    public function showCategoriesTour($categories, $select = 0, $parent_id = 0, $char = '')
    {
        foreach ($categories as $key => $item)
        {
            if ($item['parent_id'] == $parent_id)
            {
                if($select != 0 && $select == $item['id']){
                    $this->radio .='<option value="'.$item['id'].'" selected="selected">'.$char.$item['name'].'</option>';
                }else{
                    $this->radio .='<option value="'.$item['id'].'">'.$char.$item['name'].'</option>';
                }
                    
                unset($categories[$key]);
                self::showCategoriesTour($categories, $select, $item['id'], $char.'-');
            }
        }
        return  $this->radio;
    }
}
 ?>