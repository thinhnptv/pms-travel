<?php

namespace App\Http\Request;

use App\User;
use App\Mail\Email;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Hash;
use Validator;

class LoginForm extends FormRequest
{
      /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
                'email' => 'required|regex:/^[a-z][a-z0-9_\.]{3,32}@[a-z0-9]{2,}(\.[a-z0-9]{2,4}){1,2}(\.[0-9]{2,4}){0,2}$/',
                'password' => 'required|min:6',
        ];
    }
    public function messages(){
        return [
            'email.required' => "Email không được để trống!",
            'email.regex' => "Email bạn nhập không đúng định dạng!",
            // 'email.unique' => "Email đã tồn tại!",
            'password.required' => "Mật Khẩu không được để trống!",
            'password.min' => "Mật khẩu phải lớn hơn 6 ký tự!",
        ];
    }
     public function persist()
     {
        $password = Hash::make(request('password'));
        // dd(array_merge(request(['name','email']), ['password' => $password]));
                //create and save the user
                    // $user = User::create(array_merge(request(['name','email']), ['password' => $password]));
                    // dd(array_merge($this->only(['name','email']), ['password'=>$password]));
                    $user = User::create(array_merge($this->only([ 'email']), ['password' => $password]));
        
                // sign them in
                    // auth()->login($user);
                    // \Mail::to($user)->send(new Email($user)) ;
     }
}