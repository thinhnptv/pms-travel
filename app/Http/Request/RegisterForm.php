<?php

namespace App\Http\Request;

use App\User;
use App\Mail\Email;
use App\VendorRequest;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Symfony\Component\HttpFoundation\Request;
use Mail;

class RegisterForm extends FormRequest
{
   
      /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'firstname' => 'required|min:2|max:15',
            'lastname' => 'required|min:2|max:15',
            'email' => 'required|regex:/^[a-z][a-z0-9_\.]{3,32}@[a-z0-9]{2,}(\.[a-z0-9]{2,4}){1,2}(\.[0-9]{2,4}){0,2}$/|unique:users,email',
            'password' => 'min:6|max:15|required|confirmed',
            'checkbox' => 'required',
        ];
    }
    public function messages(){
        return [
            'firstname.required' => "Tên không được để trống!",
            'lastname.required' => "Họ không được để trống!",
            'email.required' => "Email không được để trống!",
            'email.regex' => "Email bạn nhập không đúng định dạng!",
            'email.unique' => "Email đã tồn tại!",
            'password.required' => "Mật Khẩu không được để trống!",
            'password.min' => "Mật khẩu phải lớn hơn 6 ký tự!",
            'password.max' => "Mật khẩu phải nhỏ hơn 15 ký tự!",
            'checkbox.required' => "Bạn phải đồng ý các điều khoản trước khi đăng ký!",
        ];
    }
    
    
}