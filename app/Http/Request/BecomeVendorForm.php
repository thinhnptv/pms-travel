<?php

namespace App\Http\Request;

use App\User;
use App\Mail\Email;
use App\VendorRequest;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;

class BecomeVendorForm extends FormRequest
{
    // protected $attributes = [
    //     'status' => self::STATUS_UNCONFIRMED,
    // ];
      /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
             //validate the form
            //  $this->validate(request(),[
                'firstname' => 'required',
                'lastname' => 'required',
                'email' => 'required|email|unique:users,email',
                'password' => 'min:6|required',
                'checkbox' => 'required',
                // ]);
        ];
    }
    public function messages(){
        return [
            'firstname.required' => "Họ không được để trống!",
            'lastname.required' => "Họ không được để trống!",
            'email.required' => "Email không được để trống!",
            'email.email' => "Email bạn nhập không đúng định dạng!",
            'email.unique' => "Email đã tồn tại!",
            'password.required' => "Mật Khẩu không được để trống!",
            'password.min' => "Mật khẩu phải có ít nhất 6 ký tự!",
            'checkbox.required' => "Bạn phải đòng ý các điều khoản trước khi đăng ký!",
        ];
    }

    public function persist()
    {
        $password = Hash::make(request('password'));
        // dd(array_merge(request(['name','email']), ['password' => $password]));
                //create and save the user
                    // $user = User::create(array_merge(request(['name','email']), ['password' => $password]));
                    // dd(array_merge($this->only(['name','email']), ['password'=>$password]));
                    $user = User::create(array_merge($this->only(['firstname', 'lastname', 'email']), ['password' => $password], ['status' => 1] ));
                    $vendor = VendorRequest::create(array_merge($this->only(['firstname', 'lastname', 'email']), ['password' => $password], ['user_id' => $user->id]));
                // sign them in
                    // auth()->login($user);
                    // \Mail::to($user)->send(new Email($user)) ;
     }
}