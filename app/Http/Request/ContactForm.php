<?php

namespace App\Http\Request;

use App\User;
use App\VendorRequest;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Modules\Contact\Entities\Contact;
use Symfony\Component\HttpFoundation\Request;
use Mail;

class ContactForm extends FormRequest
{
    // protected $attributes = [
    //     'status' => self::STATUS_UNCONFIRMED,
    // ];
      /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
             //validate the form
            //  $this->validate(request(),[
                'full_name' => 'required|min:2',
                'mail' => 'required|regex:/^[a-z][a-z0-9_\.]{3,32}@[a-z0-9]{2,}(\.[a-z0-9]{2,4}){1,2}(\.[0-9]{2,4}){0,2}$/',
                'phone' => 'required|regex:/^0([1-9]{1})([0-9]{8,9})$/',
                // 'title' => 'required',
                // 'content' =>'required',
                // ]);
        ];
    }
    public function messages(){
        return [
            'full_name.required' => "Họ và tên không được để trống!",
            // 'mail.regex ' => "Email không được có khoảng trắng ở đầu!",
            'mail.required' => "Email không được để trống!",
            'mail.regex' => "Email bạn nhập không đúng định dạng!",
            // 'phone.regex ' => "Số điện thoại không được để khoảng trắng ở đầu",
            'phone.required' => "Số điện thoại không được để trống",
            'phone.regex' => "Số điện thoại không đúng định dạng!",
            // 'title.required' => "Bạn hãy điền tiêu đề vào đây!",
            // 'content.required' => "Bạn hãy điền nội dung vào đây!",
        ];
    }
     public function persist()
     {
        $data = $this->only(['full_name', 'phone', 'mail', 'title', 'content']);
        $contact = Contact::create(array_merge($this->only(['full_name', 'phone', 'mail', 'title', 'content'])));
        Mail::send('public.mail.mail',['data' => $data], function($message) use ($data) {
            $message->to('maint@thienvietjsc.net', 'Visitor')->subject('Visitor Feedback!');
        });
     }
}