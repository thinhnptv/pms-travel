<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class CheckActiveUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
         
        if (Auth::check() && Auth::user()->status == 0) {
            Auth::logout();
            return redirect()->route('home')->with('message_error','Tfai khoản của bạn đã bị khóa');
        }
        return $next($request);
    }
}
