<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class AddminLoginMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check()) {
            if (Auth::user()->role_id == 2) {
                return $next($request);
            } else{
                return redirect()->route('get.admin_login')->with('fail','Bạn không phải admin');
            }  
        } else{
            return redirect()->route('get.admin_login');
        } 
    }
}
