<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Modules\Tour\Entities\Tour;
use Modules\Category\Entities\Location;
use Modules\Tour\Entities\Destination;
use Modules\Category\Entities\TourCategory;
use Modules\Tour\Entities\TourStyleCategory;
use Modules\Tour\Entities\TourVehicleCategory;
use Modules\Tour\Entities\TourWishList;

class TourController extends Controller
{
    protected $modelTour;
    protected $modelLocation;
    protected $modelDestination;
    protected $modelTourCategory;
    protected $modelTourStyleCategory;
    protected $modelTourVehicleCategory;
    protected $modelWishListTour;

    public function __construct(
        TourStyleCategory $modelTourStyleCategory, 
        TourCategory $modelTourCategory, 
        Location $modelLocation, 
        Tour $modelTour, 
        Destination $modelDestination,
        TourVehicleCategory $modelTourVehicleCategory,
        TourWishList $modelWishListTour
    )
    {
        $this->modelTour = $modelTour;
        $this->modelLocation = $modelLocation;
        $this->modelDestination = $modelDestination;
        $this->modelTourCategory = $modelTourCategory;
        $this->modelTourStyleCategory = $modelTourStyleCategory;
        $this->modelTourVehicleCategory = $modelTourVehicleCategory;
        $this->modelWishListTour = $modelWishListTour;
    }
    public function favorite($tourId)
    {
        $favorite = $this->modelWishListTour;
        $tour = $this->modelTour->find($tourId);
        if($tour->wishlistTourBy(auth()->user())){
            $id = $this->modelWishListTour->select('id')->where('tour_id', $tourId)->where('user_id', auth()->id())->first();
            $color = 'white';
            $this->modelWishListTour->destroy($id->id);
        }else{
            $favorite->user_id = auth()->id();
            $favorite->tour_id = $tourId;
            $favorite->save();
            $color = 'red';
        }
        $countWishlisttour = $this->modelWishListTour->where('tour_id', $tourId)->count();
        return ['countFavourite' => $countWishlisttour, 'color' => $color];
    }
    public function listTour(Request $request)
    {
        // dd($request->all());
        $listTour = $this->modelTour->query()->where('status', 1)->with([
            'user',
            'location',
            'destination',
            'reviewTour'=> function($query) {
                $query->where('status', 1);
            },
            'wishListTour',
            'startDateTour'=> function($query) {
                $query->orderBy('start_date');
            }
        ])->select(['tours.*']);
        if(!empty($request->start_destination)) {
            $idDestination = $request->start_destination;
            $listTour->where('destination_id', $idDestination);
        }
        if(!empty($request->end_location)) {
            $idLocation = $request->end_location;
            $listTour->where('location_id', $idLocation);
        }
        if(!empty($request->number_people)) {
            $numberPeople = $request->number_people;
            $listTour->where('min_people', '<', $numberPeople)->where('max_people', '>', $numberPeople);
        }
        if(!empty($request->tour_category)) {
            $tourCategory = $request->tour_category;
            $listTour->where('category_id', $tourCategory );
        }
        if(!empty($request->star)) {
            $star = $request->star;
            $listTour->where('rating_star', $star );
        }
        
        if($request->filled('tourismType')) {
            $arrtourismType = $request->tourismType;
            $listTour->whereHas('tourismType',function($query) use($arrtourismType) {
                $query->whereIn('tour_style_categories.id', $arrtourismType);
            } );
        }
        if($request->filled('vehicle')) {
            $arrVehicle = $request->vehicle;
            $listTour->whereHas('tourVehicle',function($query) use($arrVehicle) {
                $query->whereIn('tour_vehicle_categories.id', $arrVehicle);
            } );
        }
        if(!empty($request->price)) {
            $price = $request->price;
            if ($price == 1) {
                $listTour->where('price', '<', 5000000 );
            } elseif($price == 2) {
                $listTour->whereBetween('price', [5000000, 10000000] );
            }elseif($price == 3){
                $listTour->whereBetween('price', [10000000, 15000000] );
            }
        }
        if(!empty($request->date_number)) {
            $dateNumber = $request->date_number;
            if ($dateNumber == 1) {
                $listTour->where('duration','<=', 3 );
            } elseif($dateNumber == 2) {
                $listTour->whereBetween('duration', [4, 7] );
            }elseif($dateNumber == 3){
                $listTour->whereBetween('duration', [7, 10] );
            }elseif($dateNumber == 4){
                $listTour->where('duration', '>' , 10 );
            }
        }
        if(!empty($request->filter_price)) {
            $filterPrice = $request->filter_price;
            if($filterPrice == 1){
                $listTour->orderBy('price');
            }elseif($filterPrice == 2){
                $listTour->orderBy('price', 'DESC');
            }elseif($filterPrice == 3) {
                $listTour->where('sale', '>=', 0)->whereRaw('sale < price');
            }
        }
        $listTour = $listTour->paginate(15);
        foreach($listTour as $item) {
            $item->avgReview = 5;
            if(!$item->reviewTour->isEmpty()) {
                $totalReview = 0;
                foreach($item->reviewTour as $items) {
                    $totalReview += $items->average;
                }
                $item->avgReview = round($totalReview / sizeof($item->reviewTour), 1);
            }
        }
        $listDestination = $this->modelDestination::query()->where('status', 1)->get();
        $listLocation = $this->modelLocation::query()->where('status', 1)->get();
        $locationFeatured = $this->modelLocation->with([
            'hotel',
            'car',
            'tour',
            'bookingTour',
        ])->where('status', 1)->where('is_featured', 1)->limit(4)->get();
        $tourCategory = $this->modelTourCategory::query()->where('status', 1)->get();
        $tourStyleCategory = $this->modelTourStyleCategory::all();
        $tourVehicleCategory = $this->modelTourVehicleCategory::all();
        return view('public.tour.list', compact('listTour', 'listDestination', 'listLocation', 'locationFeatured', 'tourCategory', 'tourStyleCategory', 'tourVehicleCategory'));
    }
    
    public function detailTour($slug, $id)
    {
        $detailTour = $this->modelTour::query()->with([
            'user',
            'location',
            'destination',
            'reviewTour' => function($query){
                $query->where('status', 1)->with('user');
            },
            'wishListTour',
            'itinerary',
            'include',
            'faq',
            'tourCategory',
            'termstyle',
            'startDateTour'=> function($query){
                $query->orderBy('start_date');
            }
        ])->findOrfail($id);
        $detailTour->avgReview = 5;
        if(!$detailTour->reviewTour->isEmpty()) {
            $totalReview = 0;
            foreach($detailTour->reviewTour as $items) {
                $totalReview += $items->average;
            }
            $detailTour->avgReview = round($totalReview / sizeof($detailTour->reviewTour), 1);
        }
        // dd($detailTour);
        $listTourInvolve = $this->modelTour::query()->with([
            'user',
            'location',
            'destination',
            'reviewTour',
            'wishListTour',
            'tourCategory',
            'startDateTour'=> function($query){
                $query->orderBy('start_date');
            }
        ])->where('id', '<>', $detailTour->id)->where('status', 1)->where(function ($query) use ($detailTour)  {
            $query->where('category_id', $detailTour->category_id)
                    ->orWhere('location_id', $detailTour->location_id)
                    ->orWhere('destination_id', $detailTour->destination_id);
        })->limit(4)->get();
        foreach($listTourInvolve as $item) {
            $item->avgReview = 5;
            if(!$item->reviewTour->isEmpty()) {
                $totalReview = 0;
                foreach($item->reviewTour as $items) {
                    $totalReview += $items->average;
                }
                $item->avgReview = round($totalReview / sizeof($item->reviewTour), 1);
            }
        }
        $listDestination = $this->modelDestination::query()->where('status', 1)->get();
        $listLocation = $this->modelLocation::query()->where('status', 1)->get();
        $reviewTour = $detailTour->reviewTour()->where('status', 1)->with('user')->paginate(2);
        return view('public.tour.detail', compact('detailTour', 
            'listTourInvolve', 
            'reviewTour', 
            'listDestination', 
            'listLocation'
        ));
    }
    public function indexTour ()
    {
        $locationFeatured = $this->modelLocation->with([
            'hotel',
            'car',
            'tour',
            'bookingTour',
        ])->where('status', 1)->where('is_featured', 1)->limit(3)->get();
        $locationMore = $this->modelLocation->with([
            'hotel',
            'car',
            'tour',
            'bookingTour',
        ])->where('status', 1)->limit(12)->get();
        $listDestination = $this->modelDestination::query()->where('status', 1)->get();
        return view('public.tour.index', compact('locationFeatured', 'locationMore', 'listDestination'));
    }
    public function map(Request $request)
    {
        $listTour = $this->modelTour->query()->where('status', 1)->with([
            'user',
            'location',
            'destination',
            'reviewTour'=> function($query) {
                $query->where('status', 1);
            },
            'wishListTour',
            'startDateTour'=> function($query) {
                $query->orderBy('start_date');
            }
        ])->select(['tours.*']);
        if(!empty($request->start_destination)) {
            $idDestination = $request->start_destination;
            $listTour->where('destination_id', $idDestination);
        }
        if(!empty($request->end_location)) {
            $idLocation = $request->end_location;
            $listTour->where('location_id', $idLocation);
        }
        if(!empty($request->price)) {
            $price = $request->price;
            if ($price == 1) {
                $listTour->where('price', '<', 5000000 );
            } elseif($price == 2) {
                $listTour->whereBetween('price', [5000000, 10000000] );
            }elseif($price == 3){
                $listTour->whereBetween('price', [10000000, 15000000] );
            }
        }
        $listTour = $listTour->paginate(4);
        $listDestination = $this->modelDestination::query()->where('status', 1)->get();
        $listLocation = $this->modelLocation::query()->where('status', 1)->get();
        return view('public.tour.map', compact(
            'listTour',
            'listLocation',
            'listDestination'
        ));
    }
    
}
