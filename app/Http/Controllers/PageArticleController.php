<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Modules\Comment\Entities\Comment;
use Modules\Post\Entities\Article;
use Carbon\Carbon;
use Modules\Post\Entities\ReviewArticle;
use Illuminate\Support\Arr;
use Modules\Post\Entities\Tag;
use Modules\Post\Entities\TagArticle;

class PageArticleController extends Controller
{



    public function listHome()
    {
        $newHome = Article::where('status', 1)->orderBy('id', 'desc')->take(6)->get();
        $days = Carbon::now()->day;
        $months = Carbon::now()->month;
        $data = [
            'newHome' => $newHome,
            'days' => $days,
            'months' => $months,
        ];
        return view('public.home.home', isset($data) ? $data : '');
    }

    public function listArticle()
    {
        $articles = Article::where('status', 1)
            ->orderByDesc('id')
            ->with('articleUser')
            ->paginate(5);
        $newArticle = Article::where('status', 1)
            ->orderBy('id', 'desc')
            ->take(5)
            ->get();
        $highlightArticle = Article::where('status', 1)
            ->select('id', 'title', 'image', 'slug')
            ->with('articleReview')
            ->withCount('articleReview')
            ->orderByDesc('article_review_count')
            ->get();
        // dd(sizeof($highlightArticle));
        if(sizeof($highlightArticle) <= 5) {
            $highlightArticle = $highlightArticle->take(5);
        }else {
            $highlightArticle = $highlightArticle->random(5);
        }

        $data = [
            'articles' => $articles,
            'newArticle' => $newArticle,
            'highlightArticle' => $highlightArticle,
        ];
        return view('public.articles.new', isset($data) ? $data : '');
    }

    public function articleDetail(Request $request)
    {
        //dd(1);
        $arrayUrl = preg_split("/(-)/i", $request->segment(2));
        $url = array_pop($arrayUrl);
        $idHtml = preg_split("/\./", $url);
        $id = array_shift($idHtml);
        // Lấy id trong chuỗi slug

        $articleDetail = Article::where('status', 1)
            ->with('articleUser', 'articleComment', 'articleTag')
            ->find($id);

        $newArticle = Article::where('status', 1)
            ->with('articleComment')
            ->orderByDesc('id')
            ->take(5)
            ->get();

        $highlightArticle = Article::where('status', 1)
            ->select('id', 'title', 'image', 'slug')
            ->with('articleReview')
            ->withCount('articleReview')
            ->orderByDesc('article_review_count')
            ->take(5)
            ->get();
        // Lấy số lượt review của article

        $commentArticle = Comment::where('article_id', $id)
            ->with('user', 'article')
            ->orderByDesc('created_at')
            ->take(5)
            ->get();

        $tags = Tag::where('status', 1)->get();;
        $data = [
            'articleDetail' => $articleDetail,
            'tags' => $tags,
            'newArticle' => $newArticle,
            'highlightArticle' => $highlightArticle,
            'commentArticle' => $commentArticle,
        ];
        return view('public.articles.new-detail', isset($data) ? $data : '');
    }


    public function articleSearch(Request $request)
    {
        $searchs = $request->search;

        $newArticle = Article::where('status', 1)
            ->orderByDesc('id')
            ->take(5)
            ->get();

        $highlightArticle = Article::where('status', 1)
            ->select('id', 'title', 'image', 'slug')
            ->with('articleReview', 'articleUser')
            ->withCount('articleReview')
            ->orderByDesc('article_review_count')
            ->take(5)
            ->get();
        // Lấy số lượt review của article

        $articleId = TagArticle::select('article_id')->where('tag_id', $request->searchtag)->get();

        if(!empty($request->searchtag)) {
            $searchArticle = Article::whereIn('id', $articleId)
            ->with('articleUser')
            ->orderbyDesc('id')
            ->paginate(5);
        }else {
            $searchArticle = Article::where('title', 'like', '%'.$searchs.'%')
            ->with('articleUser')
            ->orderbyDesc('id')
            ->paginate(5);
        }


        $data = [
            'newArticle' => $newArticle,
            'highlightArticle' => $highlightArticle,
            'searchArticle' => $searchArticle,
        ];


        return view('public.articles.new-search', isset($data) ? $data : '');
    }

    public function articleComment(Request $request, $id)
    {
        $article_id = $id;
        $comment = new Comment();
        $comment->article_id = $article_id;
        $comment->user_id = Auth::user()->id;
        $comment->content = $request->comment;
        $comment->save();

        return redirect()->back()->with([
            'success' => 'Bình luận thành công'
        ]);

    }

    public function commentDelete($id)
    {
        Comment::destroy($id);
        return back();
    }

    public function commentUpdate($id)
    {
        $comment = Comment::find($id);
        $comment->content = request()->comment;
        $comment->save();
        return back();
    }


}
