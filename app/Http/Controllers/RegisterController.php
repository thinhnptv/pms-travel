<?php

namespace App\Http\Controllers;

use App\Http\Request\RegisterForm;
use Illuminate\Http\Request;
use App\User;
use Validator;
use Illuminate\Support\Facades\Hash;
use Mail;

class RegisterController extends Controller
{
    public function store(RegisterForm $request)
    {
        $addUser = new User;
        $addUser->firstname = $request->firstname;
        $addUser->lastname = $request->lastname;
        $addUser->email = $request->email;
        $addUser->password = Hash::make($request->password);
        $addUser->status = 0;
        $addUser->save();
        ! auth()->attempt(request(
            [
                'firstname',
                'lastname',
                'email',
                'password',
            ]
            ));
        Mail::send('public.mail.add-user',['data' => $addUser], function($message) {
            $message->to('maint@thienvietjsc.net', 'Visitor')->subject('Visitor Feedback!');
        });
        return response()->json([
            'type' => 1,
            'mess' => "Đăng ký thành công!"
        ]);
        
    }
}
