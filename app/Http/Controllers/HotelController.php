<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Modules\Category\Entities\Location;
use Modules\Category\Entities\Term;
use Modules\Hotel\Entities\Hotel;
use Modules\Hotel\Entities\HotelType;
use Carbon\Carbon;
use Modules\Hotel\Entities\HotelWishList;
use Modules\Hotel\Entities\Room;
use Modules\Hotel\Entities\RoomAvavailability;

class HotelController extends Controller
{
    protected $modelHotel;
    protected $modelTerm;
    protected $modelLocation;
    protected $modelType;
    protected $modelRoomAvavailability;
    protected $modelWishlisthotel;
    protected $modelRoom;

    public function __construct(Hotel $modelHotel, Term $modelTerm, Location $modelLocation, HotelType $modelType, RoomAvavailability $modelRoomAvavailability,HotelWishList $modelWishlisthotel, Room $modelRoom)
    {
        $this->modelHotel = $modelHotel;
        $this->modelTerm = $modelTerm;
        $this->modelLocation = $modelLocation;
        $this->modelType = $modelType;
        $this->modelRoomAvavailability = $modelRoomAvavailability;
        $this->modelWishlisthotel = $modelWishlisthotel;
        $this->modelRoom = $modelRoom;
    }

    public function favourite($hotelId)
    {
        $favourite = new $this->modelWishlisthotel;
        $hotel = $this->modelHotel->find($hotelId);
        if( $hotel->wishlistHotelBy(auth()->user()) ){
            $id = $this->modelWishlisthotel->select('id')->where('hotel_id', $hotelId)->where('user_id', auth()->id())->first();
            $color = 'white';
            $this->modelWishlisthotel->destroy($id->id);
        }else{
            $favourite->user_id = auth()->id();
            $favourite->hotel_id = $hotelId;
            $favourite->save();
            $color = 'red';
        }
        $countWishlisthotel = $this->modelWishlisthotel->where('hotel_id', $hotelId)->count();
        return ['countFavourite' => $countWishlisthotel, 'color' => $color];
    }

    public function index(Request $request)
    {
        // dd($request->all());
        $listRoom = $this->modelRoom::query()->get();
        $property = $this->modelTerm::query()->where('attribute_id', 4)->get();
        $facility = $this->modelTerm::query()->where('attribute_id', 5)->get();
        $service = $this->modelTerm::query()->where('attribute_id', 3)->get();
        $location = $this->modelLocation::query()->where('status', 1)->get();
        $listHotel = $this->modelHotel::query()->where('status', 1)->with([
            'location',
            'property',
            'facility',
            'review',
            'wishListHotel',
            'service',
            'room'=>function($query){
                $query->where('status', 1)->orderBy('price');
            }
        ]);
        if(!empty($request->daterange)) {
            $daterange = $request->daterange;

            $startDate = substr( $daterange, 0, 10);
            $endDate = substr( $daterange, -10);
            $start_time = Carbon::createFromFormat('Y/m/d', $startDate)->format('Y-m-d');
            $end_time = Carbon::createFromFormat('Y/m/d', $endDate)->format('Y-m-d');
            $roomavalability = $this->modelRoomAvavailability->whereBetween('date', [$start_time, $end_time])->where('remaining', 0)->with([
                'room'
            ])->get()->toArray();
            //lấy ra những khách sạn có phòng bị full lịch trong khoảng ngày chọn,
            $arrIdHotel = array_map(function($roomavalabiliti)  {
                return $roomavalabiliti['room']['hotel_id'];
            }, $roomavalability);
            //laays ra taats cả các phòng bị full lịch trong khoảng ngày chọn
            $arrIdRoomFull  = array_map(function($roomavalabiliti)  {
                return $roomavalabiliti['room_id'];
            }, $roomavalability);
            //loại bỏ những id trùng nhau
            $arrIdHotel = array_unique($arrIdHotel);
            $arrIdHotelFullRoom = [];
            foreach($arrIdHotel as $item) {
                $hotelItem = $this->modelHotel->with('room')->find($item)->toArray();
                $arrRoomItem = array_map(function($roomItems) {
                    return $roomItems['id'];
                }, $hotelItem['room']);
                if(count(array_diff($arrRoomItem,$arrIdRoomFull)) == 0) {
                    $arrIdHotelFullRoom[] = $item;
                }
            }
            $listHotel->whereNotIn('id', $arrIdHotelFullRoom);
        }
        if(!empty($request->location_id)) {
            $locationId = $request->location_id;
            $listHotel->where('location_id', $locationId);
        }
        if(!empty($request->location_id_1)) {
            $locationId1 = $request->location_id_1;
            $listHotel->where('location_id', $locationId1);
        }
        if(!empty($request->star)) {
            $star = $request->star;
            $listHotel->where('evaluate_star', $star);
        }
        if(!empty($request->price)) {
            $priceId = $request->price;
            if ($priceId == 2){
                $listHotel->orderBy('price', 'desc');
            }elseif ($priceId == 1) {
                $listHotel->orderBy('price', 'asc');
            } elseif ($priceId == 3) {
                $listHotel->where('hotel_sale', '>', 0)->whereRaw('hotel_sale < price');
            }
        }
        if(!empty($request->price_range)){
            $price = $request->price_range;
            switch ($price){
                case '1':
                    $listHotel->where('price', '<', 5000000);
                break;
                case '2':
                    $listHotel->whereBetween('price', [5000000, 10000000]);
                break;
                case '3':
                    $listHotel->whereBetween('price', [10000000, 15000000]);
                break;
            }
        }
        if($request->filled('property')){
            $arrProperty = $request->property;
            $listHotel->whereHas('property', function($query) use($arrProperty){
                $query->whereIn('terms.id', $arrProperty);
            });
        }
        if($request->filled('facility')){
            $arrFacility = $request->facility;
            $listHotel->whereHas('facility', function($query) use($arrFacility){
                $query->whereIn('terms.id', $arrFacility);
            });
        }
        if($request->filled('service')){
            $arrService = $request->service;
            $listHotel->whereHas('service', function($query) use($arrService){
                $query->whereIn('terms.id', $arrService);
            });
        }
        if(!empty($request->title)){
            $title = $request->title;
            $listHotel->where('name', 'like', '%' .$title. '%');
        }
        $listHotel = $listHotel->paginate(15);
        foreach($listHotel as $item) {
            $item->avgReview = 5;
            if(!$item->review->isEmpty()) {
                $totalReview = 0;
                foreach($item->review as $items) {
                    $totalReview += $items->average;
                }
                $item->avgReview = round($totalReview / sizeof($item->review), 0);
            }
        }
        // dd($listHotel);
        return view('public.hotel.list', compact('listHotel', 'property', 'facility', 'service', 'location'));
    }
    public function detail($slug, $id, Request $request)
    {
        $location = $this->modelLocation::query()->where('status', 1)->get();

        $detailHotel = $this->modelHotel::query()->with([
            'location',
            'utilities',
            'wishListHotel',
            'policy',
            'room'=>function($query){
                $query->where('status', 1)->orderBy('price');
            },
            'review'=>function($query){
                $query->with('user');
            }
            ])->findOrFail($id);
        $detailHotel->avgReview = 5;
                if(!$detailHotel->review->isEmpty()) {
                    $totalReview = 0;
                    foreach($detailHotel->review as $items) {
                        $totalReview += $items->average;
                    }
                    $detailHotel->avgReview = round($totalReview / sizeof($detailHotel->review), 0);
                }
        $listHotel = $this->modelHotel::query()->where('status', 1)->with([
            'location',
            'property',
            'facility',
            'service',
            'review',
            'room'=>function($query){
                $query->orderBy('price');
            }
            ])->where('location_id',  $detailHotel->location_id)->paginate(15);

            foreach($listHotel as $item) {
                $item->avgReview = 5;
                if(!$item->review->isEmpty()) {
                    $totalReview = 0;
                    foreach($item->review as $items) {
                        $totalReview += $items->average;
                    }
                    $item->avgReview = round($totalReview / sizeof($item->review), 0);
                }
            }
            $review = $detailHotel->review()->where('status', 1)->with('user')->paginate(2);
        // dd($detailHotel);
        return view('public.hotel.detail-hotel', compact('detailHotel', 'listHotel', 'location', 'review'));
    }
    public function home(Request $request)
    {
        $listType = $this->modelType::query()->where('status', 1)->with('hotel')->get();
        $listLike = $this->modelHotel::query()->where('availability', 1)->with('location', 'review')->limit(8)->get();
        // dd($listLike);
        $listHotel = $this->modelHotel::query()->where('status', 1)->where('availability', 1)->with(
            'location',
            'wishListHotel'
            )->get();
        if(!empty($request->location_id)) {
            $locationId = $request->location_id;
            $listHotel->where('location_id', $locationId);
        }
        $location = $this->modelLocation::query()->where('status', 1)->get();

        return view('public.hotel.index', compact('listType', 'listLike', 'listHotel', 'location'));
    }
    public function map(Request $request)
    { 
        $listHotel = $this->modelHotel::query()->where('status', 1)->with([
            'location',
            'property',
            'facility',
            'review',
            'wishListHotel',
            'service',
            'room'=>function($query){
                $query->where('status', 1)->orderBy('price');
            }
        ]);
        if(!empty($request->daterange)) {
            $daterange = $request->daterange;

            $startDate = substr( $daterange, 0, 10);
            $endDate = substr( $daterange, -10);
            $start_time = Carbon::createFromFormat('Y/m/d', $startDate)->format('Y-m-d');
            $end_time = Carbon::createFromFormat('Y/m/d', $endDate)->format('Y-m-d');
            $roomavalability = $this->modelRoomAvavailability->whereBetween('date', [$start_time, $end_time])->where('remaining', 0)->with([
                'room'
            ])->get()->toArray();
            //lấy ra những khách sạn có phòng bị full lịch trong khoảng ngày chọn,
            $arrIdHotel = array_map(function($roomavalabiliti)  {
                return $roomavalabiliti['room']['hotel_id'];
            }, $roomavalability);
            //laays ra taats cả các phòng bị full lịch trong khoảng ngày chọn
            $arrIdRoomFull  = array_map(function($roomavalabiliti)  {
                return $roomavalabiliti['room_id'];
            }, $roomavalability);
            //loại bỏ những id trùng nhau
            $arrIdHotel = array_unique($arrIdHotel);
            $arrIdHotelFullRoom = [];
            foreach($arrIdHotel as $item) {
                $hotelItem = $this->modelHotel->with('room')->find($item)->toArray();
                $arrRoomItem = array_map(function($roomItems) {
                    return $roomItems['id'];
                }, $hotelItem['room']);
                if(count(array_diff($arrRoomItem,$arrIdRoomFull)) == 0) {
                    $arrIdHotelFullRoom[] = $item;
                }
            }
            $listHotel->whereNotIn('id', $arrIdHotelFullRoom);
        }
        if(!empty($request->location_id)) {
            $locationId = $request->location_id;
            $listHotel->where('location_id', $locationId);
        }
        if(!empty($request->price_range)){
            $price = $request->price_range;
            switch ($price){
                case '1':
                    $listHotel->where('price', '<', 5000000);
                break;
                case '2':
                    $listHotel->whereBetween('price', [5000000, 10000000]);
                break;
                case '3':
                    $listHotel->whereBetween('price', [10000000, 15000000]);
                break;
            }
        }
        $listHotel = $listHotel->paginate(4);
        $listLocation = $this->modelLocation->where('status', 1)->get();
        return view('public.hotel.map', compact(
            'listHotel',
            'listLocation'
        ));
    }
}
