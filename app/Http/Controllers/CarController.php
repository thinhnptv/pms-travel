<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Modules\Car\Entities\Car;
use Modules\Category\Entities\CarTerm;
use Modules\Category\Entities\Location;
use Carbon\Carbon;
use Modules\Car\Entities\CarAvailability;
use Modules\Car\Entities\CarBrand;
use Modules\Car\Entities\Wishlistcar;

class CarController extends Controller
{
    protected $modelCar;
    protected $modelLocation;
    protected $modelCarTerm;
    protected $modelCarAvailability;
    protected $modelCarBrand;
    protected $modelWishlistcar;

    public function __construct(
        Car $modelCar,
        Location $modelLocation,
        CarTerm $modelCarTerm,
        CarAvailability $modelCarAvailability,
        CarBrand $modelCarBrand,
        Wishlistcar $modelWishlistcar
    )
    {
        $this->modelCar = $modelCar;
        $this->modelLocation = $modelLocation;
        $this->modelCarTerm = $modelCarTerm;
        $this->modelCarAvailability = $modelCarAvailability;
        $this->modelCarBrand = $modelCarBrand;
        $this->modelWishlistcar = $modelWishlistcar;
    }

    public function favourite($carId)
    {
        $favourite = new $this->modelWishlistcar;
        $car = $this->modelCar->find($carId);
        if( $car->wishlistCarBy(auth()->user()) ){
            $id = $this->modelWishlistcar->select('id')->where('car_id', $carId)->where('user_id', auth()->id())->first();
            $color = 'white';
            $this->modelWishlistcar->destroy($id->id);
        }else{
            $favourite->user_id = auth()->id();
            $favourite->car_id = $carId;
            $favourite->save();
            $color = 'red';
        }
        $countWishlistcar = $this->modelWishlistcar->where('car_id', $carId)->count();
        return ['countFavourite'=>$countWishlistcar, 'color'=>$color];
    }

    public function indexCar()
    {
        $listLocation = $this->modelLocation->where('status', 1)->get();
        $listCar = $this->modelCar::query()->where('featured', 1)->where('status', 1)->with([
            'location',
            'wishlistCar'
        ])->orderByDesc('id')->limit(8)->get();
        $listCarBrand = $this->modelCarBrand::query()->orderByDesc('id')->get();
        return view('public.car.index', compact(
            'listLocation',
            'listCar',
            'listCarBrand'
        ));
    }
    public function listCar(Request $request)
    {
        // dd($request->all());
        $listCar = $this->modelCar::query()->where('status', 1)->with([
            'reviewcar',
            'location',
            'wishlistCar',
            'carBrand'
        ]);
        if(!empty($request->title)) {
            $title = $request->title;
            $listCar->where('title',  'like', '%' . $title . '%');
        }
        if(!empty($request->daterange)) {
            $daterange = $request->daterange;
            $startDate = substr( $daterange, 0, 10);
            $endDate = substr( $daterange, -10);
            $start_time = Carbon::createFromFormat('Y/m/d', $startDate);
            $end_time = Carbon::createFromFormat('Y/m/d', $endDate);
            $start_time =  $start_time->format('Y-m-d');
            $end_time =  $end_time->format('Y-m-d');
            $avaibility = $this->modelCarAvailability::query()->whereBetween('date', [$start_time, $end_time])->where('remaining', 0)->get()->toArray();
            $arrIdCar = array_map(function($avaibiliti)  {
                return $avaibiliti['car_id'];
            }, $avaibility);
            $listCar->whereNotIn('id', $arrIdCar);
        }
        if(isset($request->car_form)) {
            $carForm = $request->car_form;
            $listCar->where('car_form', $carForm);
        }
        if(!empty($request->location_id)) {
            $locationId = $request->location_id;
            $listCar->where('location_id', $locationId);
        }
        if(!empty($request->star)) {
            $ratingStar = $request->star;
            $listCar->where('rating_star', $ratingStar);
        }
        if($request->filled('termType')) {
            $arrtermType = $request->termType;
            $listCar->whereHas('termtype',function($query) use($arrtermType) {
                $query->whereIn('car_terms.id', $arrtermType);
            } );
        }
        if($request->filled('termFeatures')) {
            $arrtermFeatures = $request->termFeatures;
            $listCar->whereHas('termfeature',function($query) use($arrtermFeatures) {
                $query->whereIn('car_terms.id', $arrtermFeatures);
            } );
        }
        if(!empty($request->price)) {
            $price = $request->price;
            if ($price == 1) {
                $listCar->where('price', '<', 5000000 );
            } elseif($price == 2) {
                $listCar->whereBetween('price', [5000000, 10000000] );
            }elseif($price == 3){
                $listCar->whereBetween('price', [10000000, 15000000] );
            }
        }
        if(!empty($request->filter_price)) {
            $filterPrice = $request->filter_price;
            if($filterPrice == 1){
                $listCar->orderBy('price');
            }elseif($filterPrice == 2){
                $listCar->orderBy('price', 'DESC');
            }elseif($filterPrice == 3){

                $listCar->where('sale', '>', 0 );
            }
        }
        if(!empty($request->brand_id)){
            $brandId = $request->brand_id;
            $listCar->where('brand_id', $request->brand_id);
        }
        if(!empty($request->carBrand)) {
            $listCar->whereIn('brand_id',$request->carBrand);
        }

        if(empty($request->filter_price)) {
            $listCar = $listCar->orderbyDesc('id')->paginate(15);
        }else {
            $listCar = $listCar->paginate(15);
        }
        $locationFeatured = $this->modelLocation->with([
            'hotel',
            'car',
            'tour',
            'bookingTour',
        ])->where('status', 1)->where('is_featured', 1)->limit(4)->get();
        $listLocation = $this->modelLocation->where('status', 1)->get();
        $termType = $this->modelCarTerm::query()->where('attribute_id', 1)->get();
        $termFeatures = $this->modelCarTerm::query()->where('attribute_id', 2)->get();
        $carBrand = $this->modelCarBrand::all();
        return view('public.car.list', compact(
            'carBrand',
            'listCar',
            'locationFeatured',
            'listLocation',
            'termFeatures',
            'termType'
        ));
    }
    public function detailCar($slug, $id)
    {
        $detailCar = $this->modelCar::query()->with([
            'reviewcar' => function($query){
                $query->where('status', 1)->with('user')->paginate(1);
            },
            'location',
            'termfeature',
            'faq'
        ])->findOrfail($id);
        $detailCar->avgReview = 5;
        if(!$detailCar->reviewcar->isEmpty()) {
            $totalReview = 0;
            foreach($detailCar->reviewcar as $items) {
                $totalReview += $items->average;
            }
            $detailCar->avgReview = round($totalReview / sizeof($detailCar->reviewcar), 1);
        }
        $listLocation = $this->modelLocation->where('status', 1)->get();

        $count = $listCar = $this->modelCar::query()->where('status', 1)->get();

        if(sizeof($count) > 8) {
            $listCarInvolve = $listCar = $this->modelCar::query()->where('status', 1)->with([
                'reviewcar',
                'location',
                'wishlistCar'
            ])->where('id', '<>', $id)->orderByDesc('id')->get()->random(8);
        }else {
            $listCarInvolve = $listCar = $this->modelCar::query()->where('status', 1)->with([
                'reviewcar',
                'location',
                'wishlistCar'
            ])->where('id', '<>', $id)->orderByDesc('id')->limit(8)->get();
        }
        $reviewcar = $detailCar->reviewcar()->where('status', 1)->with('user')->paginate(2);
        return view('public.car.detail', compact(
            'detailCar',
            'listCarInvolve',
            'listLocation',
            'reviewcar'
        ));
    }
    public function listMapCar(Request $request) {
        $listCar = $this->modelCar::query()->where('status', 1)->with([
            'reviewcar',
            'location',
            'wishlistCar',
            'carBrand'
        ]);
        if(!empty($request->location_id)) {
            $locationId = $request->location_id;
            $listCar->where('location_id', $locationId);
        }
        if(!empty($request->daterange)) {
            $daterange = $request->daterange;
            $startDate = substr( $daterange, 0, 10);
            $endDate = substr( $daterange, -10);
            $start_time = Carbon::createFromFormat('Y/m/d', $startDate);
            $end_time = Carbon::createFromFormat('Y/m/d', $endDate);
            $start_time =  $start_time->format('Y-m-d');
            $end_time =  $end_time->format('Y-m-d');
            $avaibility = $this->modelCarAvailability::query()->whereBetween('date', [$start_time, $end_time])->where('remaining', 0)->get()->toArray();
            $arrIdCar = array_map(function($avaibiliti)  {
                return $avaibiliti['car_id'];
            }, $avaibility);
            $listCar->whereNotIn('id', $arrIdCar);
        }
        if(!empty($request->car_form)) {
            $carForm = $request->car_form;
            $listCar->where('car_form', $carForm);
        }
        if(!empty($request->price)) {
            $price = $request->price;
            if ($price == 1) {
                $listCar->where('price', '<', 5000000 );
            } elseif($price == 2) {
                $listCar->whereBetween('price', [5000000, 10000000] );
            }elseif($price == 3){
                $listCar->whereBetween('price', [10000000, 15000000] );
            }
        }
        $listCar = $listCar->paginate(4);
        $listLocation = $this->modelLocation->where('status', 1)->get();
        return view('public.car.map', compact(
            'listCar',
            'listLocation'
        ));
    }
}
