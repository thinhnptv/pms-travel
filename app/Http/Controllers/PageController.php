<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Modules\Page\Entities\Page;

class PageController extends Controller
{
    protected $modelPage;

    public function __construct(
        Page $modelPage
    )
    {
        $this->modelPage = $modelPage;
    }

    public function about()
    {
      $pageAbout = $this->modelPage::query()->where('status', 1)->where('style', 1)->first();
    //   dd($pageAbout);
      return view('public.page.about', compact('pageAbout'));
    }
}
