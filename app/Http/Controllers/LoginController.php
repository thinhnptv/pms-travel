<?php

namespace App\Http\Controllers;

use App\Http\Request\LoginForm;
use App\Http\Requests\LoginAdminRequest;
use App\User;
use Illuminate\Http\Request;
use Validator;
use Auth;

class LoginController extends Controller
{
    // public function __construct()
    // {
    //     $this->middleware('guest')->except(['destroy']);
    // }

    public function store(Request $request)
    {
        // $validator = Validator::make($request->all(), [
        //     'email' => 'required|email',
        //     'password' => 'required|min:6',
        // ],[
        //     'email.required' => "Email không được để trống!",
        //     'email.email' => "Email bạn nhập không đúng định dạng!",
        //     'password.required' => "Mật Khẩu không được để trống!",
        //     'password.min' => "Mật khẩu phải lớn hơn 6 ký tự!",
        // ]);
        // if ($validator->fails()) {
        //     return response()->json([
        //         'type' => 2,
        //         'mess' => $validator->errors()->first()
        //     ]);
        // }else{
            if (! auth()->attempt(array_merge(request(['email', 'password']), ['status' => 1]))) {
                return response()->json([
                    'type' => 3,
                    'mess' => "Tài khoản bị khóa hoặc tài khoản, mật khẩu không đúng"
                ]);
            } else {
                return response()->json([
                    'type' => 1,
                    'mess' => "Đăng nhập thành công"
                ]);
            }
        // }
      
    }
    public function destroy()
    {
        auth()->logout();
        return redirect()->route('home')->with('message_success', 'Đăng xuất thành công');
    }
    public function getAdminLogin() {
        return view('public.login.login');
    }
    public function postAdminLogin(LoginAdminRequest $request) {
        $login=array('email'=>$request->email,'password'=>$request->password, 'status'=>1);
        if(Auth::attempt($login)){
            // dd(Auth::attempt($login), Auth::user());
            // return redirect()->route('base.index')->with('success','Đăng nhập thành công');
            return response()->json([
                'type' => 1,
                'mess' => "Đăng nhập thành công!"
            ]);
        }
        else {
            return response()->json([
                'type' => 2,
                'mess' => "Tài khoản bị khoá hoặc sai tên tài khoản, mật khẩu!"
            ]);
            // return redirect()->route('get.admin_login')->with('fail','Tài khoản bị khoá hoặc sai tên tài khoản hoặc mật khẩu'); 
        }
    }
    public function logoutAdmin() {
        Auth::logout();
        return redirect()->route('get.admin_login')->with('success','Đăng xuất thành công'); 
    }
}
