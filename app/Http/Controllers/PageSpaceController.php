<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Modules\Space\Entities\Space;
use Modules\Category\Entities\Location;
use Modules\Category\Entities\SpaceAttribute;
use Modules\Category\Entities\SpaceTerm;
use Modules\Space\Entities\SpaceAvailability;
use Modules\Space\Entities\SpaceFavourite;
use Modules\Space\Entities\SpaceType;

class PageSpaceController extends Controller
{
    protected $modelCar;
    protected $modelLocation;
    protected $modelCarTerm;


    public function __construct(
        Space $modelSpace,
        Location $modelLocation
    )
    {
        $this->modelSpace = $modelSpace;
        $this->modelLocation = $modelLocation;
    }

    public function spaceFavourite($idspace)
    {
        $favourite = new SpaceFavourite();
        $space = Space::find($idspace);
        if( $space->isFavouritesBy(auth()->user()) ){
            $id = SpaceFavourite::select('id')->where('space_id', $idspace)->where('user_id', auth()->id())->first();
            $color = 'white';
            SpaceFavourite::destroy($id->id);
        }else{
            $favourite = new SpaceFavourite();
            $favourite->user_id = auth()->id();
            $favourite->space_id = $idspace;
            $favourite->save();
            $color = 'red';
        }
        $countFavourite = SpaceFavourite::where('space_id', $idspace)->count();
        return ['countFavourite'=>$countFavourite, 'color'=>$color];
    }

    public function listSpace(Request $request)
    {
        $listSpaces = $this->modelSpace::query()->where('status', 1)->with([
            'locationSpace', 'spaceTypes', 'spaceAvailability', 'spaceFavourites', 'spaceAvailability'
        ]);

        if(!empty($request->filled('spaceSearch'))){
           $listSpaces->where('name', 'like', '%'.$request->spaceSearch.'%');
        }
        if(!empty($request->location_id)){
            $listSpaces->where('location_id', $request->location_id);
        }
        if(!empty($request->numbeguests)){
            $listSpaces->where('max_guests', '>=', $request->numbeguests);
        }
        if(!empty($request->filter_price == 1)) {
            $listSpaces->orderBy('price');
        }
        if(!empty($request->filter_price == 2)) {
            $listSpaces->orderByDesc('price');
        }
        if(!empty($request->filter_price == 3)) {
            $listSpaces->where('sale_price', '>', 0);
        }
        if($request->filled('termType')) {
            $arrtermType = $request->termType;
            $listSpaces->whereHas('spaceTypes', function($query) use($arrtermType) {
                $query->whereIn('space_terms.id', $arrtermType);
            });
        }
        if($request->filled('spaceAmenity')) {
            $arrspaceAmenity = $request->spaceAmenity;
            $listSpaces->whereHas('spaceAmenities', function($query) use($arrspaceAmenity) {
                $query->whereIn('space_terms.id', $arrspaceAmenity);
            });
        }
        if(!empty($request->star)) {
            $ratingStar = $request->star;
            $listSpaces->where('rating_star', $ratingStar);
        }

        if(isset($request->price)){
            $price = $request->price;
            if($price == 1){
                $listSpaces->where('price', '<', 5000000);
            }elseif($price == 2){
                $listSpaces->whereBetween('price', [5000000, 10000000]);
            }elseif($price == 3){
                $listSpaces->whereBetween('price', [10000000, 15000000]);
            }
        }
        if(!empty($request->daterange)){
            $daterange = $request->daterange;
            $startDate = substr($daterange, 0, 10);
            $endDate = substr($daterange, -10);
            $start_time = Carbon::createFromFormat('Y/m/d', $startDate);
            $end_time = Carbon::createFromFormat('Y/m/d', $endDate);
            $start_time =  $start_time->format('Y-m-d');
            $end_time =  $end_time->format('Y-m-d');
            $avaibility = SpaceAvailability::query()->whereBetween('date', [$start_time, $end_time])->where('remaining', 0)->get()->toArray();
            $arrSpace = array_map(function($avaibiliti)  {
                return $avaibiliti['space_id'];
            }, $avaibility);
            $listSpaces->whereNotIn('id', $arrSpace);
        }
        if(empty($request->filter_price)) {
            $listSpaces = $listSpaces->orderByDesc('id')->paginate(15);
        }else{
            $listSpaces = $listSpaces->paginate(15);
        }

        $location = Location::where('status', 1)->with(['hotel', 'space', 'hotel', 'car', 'tour'])->get();

        $countLoction = Location::where('status', 1)->get();

        if(sizeof($countLoction) > 4){
            $listLocation = Location::where('status', 1)->with(['hotel', 'space', 'hotel', 'car', 'tour'])->get()->random(4);
        }else{
            $listLocation = Location::where('status', 1)->with(['hotel', 'space', 'hotel', 'car', 'tour'])->limit(4)->get();
        }
        $spaceType = SpaceTerm::where('attribute_id', 1)->get();
        $spaceAmenity = SpaceTerm::where('attribute_id', 2)->get();
        $numberGuests = Space::select('max_guests')->groupBy('max_guests')->get();


        return view('public.spaces.list-space',[
            'spaceType' => $spaceType,
            'listSpaces' => $listSpaces,
            'spaceAmenity' => $spaceAmenity,
            'location' => $location,
            'listLocation' => $listLocation,
            'numberGuests' => $numberGuests
        ]);

    }

    public function spaceDetail(Request $request)
    {
        $arrayUrl = preg_split("/(-)/i", $request->segment(2));
        $url = array_pop($arrayUrl);
        $idHtml = preg_split("/\./", $url);
        $id = array_shift($idHtml);

        $spaceDetail = Space::where('status', 1)
            ->with('locationSpace', 'spaceTypes')
            ->find($id);

        if(!empty($spaceDetail->gallery)) {
            $arr_album_selected = json_decode($spaceDetail->gallery, true);
            foreach($arr_album_selected as $key => $val) {
                $arr_album_value[] = config('app.url').$val;
            }
        }
        $listLocation = Location::where('status', 1)->with(['hotel', 'space'])->get();
        $countSpace = Space::where('status', 1)
            ->with('locationSpace')
            ->orderByDesc('created_at')->get();

        if(sizeof($countSpace) > 4){
            $spacesFavourite = Space::where('status', 1)
            ->with('locationSpace')
            ->orderByDesc('created_at')
            ->get()->random(4);
        }else{
            $spacesFavourite = Space::where('status', 1)
            ->with('locationSpace')
            ->orderByDesc('created_at')
            ->limit(4)
            ->get();
        }

        return view('public.spaces.space-detail', [
            'spacesFavourite' => $spacesFavourite,
            'listLocation' => $listLocation,
            'spaceDetail' => $spaceDetail,
            'arr_album_value' => !empty($spaceDetail->gallery)?$arr_album_value:' '
            ]);
    }

    public function space()
    {
        $spaces = Space::where('status', 1)
            ->with(['locationSpace', 'spaceFavourites'])
            ->orderByDesc('created_at')
            ->limit(6)
            ->get();
        $countSpace = Space::where('status', 1)
            ->with('locationSpace')
            ->orderByDesc('created_at')->get();
        if(sizeof($countSpace) < 8) {
            $spacesFavourite = Space::where('status', 1)
            ->with('locationSpace')
            ->orderByDesc('created_at')
            ->limit(8)
            ->get();
        }else {
            $spacesFavourite = Space::where('status', 1)
            ->with('locationSpace')
            ->get()
            ->random(8);
        }
        $location = Location::where('status', 1)->with(['hotel', 'space', 'hotel', 'car', 'tour'])->get();

        $listLocation = Location::where('status', 1)->with(['hotel', 'space'])->orderByDesc('id')->limit(4)->get();
        $numberGuests = $numberGuests = Space::select('max_guests')->groupBy('max_guests')->get();
        $countLocation = Location::where('status', 1)->get();
        if(sizeof($countLocation) <=4 ) {
            $highlightSpace = Location::where('status', 1)->get();
        }else {
            $highlightSpace = Location::where('status', 1)->get()->random(4);
        }

        $data = [
            'spacesFavourite' => $spacesFavourite,
            'spaces' => $spaces,
            'numberGuests' => $numberGuests,
            'listLocation' => $listLocation,
            'location' => $location,
            'highlightSpace' => $highlightSpace
        ];
        return view('public.spaces.space', isset($data) ? $data : '');
    }

    public function listMapSpace(Request $request)
    {
        $listSpaces = $this->modelSpace::query()->where('status', 1)->with([
            'locationSpace', 'spaceTypes', 'spaceAvailability', 'spaceFavourites', 'spaceAvailability'
        ]);


        if(!empty($request->location_id)){
            $listSpaces->where('location_id', $request->location_id);
        }
        if(!empty($request->numbeguests)){
            $listSpaces->where('max_guests', '>=', $request->numbeguests);
        }


        if(isset($request->price)){
            $price = $request->price;
            if($price == 1){
                $listSpaces->where('price', '<', 5000000);
            }elseif($price == 2){
                $listSpaces->whereBetween('price', [5000000, 10000000]);
            }elseif($price == 3){
                $listSpaces->whereBetween('price', [10000000, 15000000]);
            }
        }
        if(!empty($request->daterange)){
            $daterange = $request->daterange;
            $startDate = substr($daterange, 0, 10);
            $endDate = substr($daterange, -10);
            $start_time = Carbon::createFromFormat('Y/m/d', $startDate);
            $end_time = Carbon::createFromFormat('Y/m/d', $endDate);
            $start_time =  $start_time->format('Y-m-d');
            $end_time =  $end_time->format('Y-m-d');
            $avaibility = SpaceAvailability::query()->whereBetween('date', [$start_time, $end_time])->where('remaining', 0)->get()->toArray();
            $arrSpace = array_map(function($avaibiliti)  {
                return $avaibiliti['space_id'];
            }, $avaibility);
            $listSpaces->whereNotIn('id', $arrSpace);
        }
        if(empty($request->filter_price)) {
            $listSpaces = $listSpaces->orderByDesc('id')->paginate(4);
        }else{
            $listSpaces = $listSpaces->paginate(4);
        }
        $listLocation = Location::where('status', 1)->with(['hotel', 'space', 'hotel', 'car', 'tour'])->get()->random(4);
        $spaceAttribute = SpaceAttribute::where('status', 1)->get();
        $numberGuests = Space::select('max_guests')->groupBy('max_guests')->get();
        $spacetype = SpaceTerm::all()->groupBy('attribute_id');

        return view('public.spaces.space-map',[
            'spacetype' => $spacetype,
            'spaceAttribute' => $spaceAttribute,
            'listSpaces' => $listSpaces,
            'listLocation' => $listLocation,
            'numberGuests' => $numberGuests
        ]);
    }
}
