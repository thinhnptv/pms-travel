<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Modules\Tour\Entities\Tour;
use Modules\Car\Entities\Car;
use Modules\Hotel\Entities\Hotel;
use Modules\Category\Entities\Location;
use Modules\Category\Entities\CarTerm;
use Modules\Contact\Entities\Subscriber;
use Modules\Tour\Entities\Destination;
use App\Http\Requests\SubscriberRequest;
use Modules\Car\Entities\CarBrand;
use Validator;
use Modules\User\Entities\CustomerReview;
use Modules\Post\Entities\Article;

class IndexController extends Controller
{
    protected $modelTour;
    protected $modelCar;
    protected $modelLocation;
    protected $modelCarTerm;
    protected $modelSubscriber;
    protected $modelHotel;
    protected $modelDestination;
    protected $modelCustomerReview;
    protected $modelArticle;
    protected $modelCarBrand;


    public function __construct(
        Tour $modelTour,
        Car $modelCar,
        Location $modelLocation,
        CarTerm $modelCarTerm,
        Hotel $modelHotel,
        Destination $modelDestination,
        CustomerReview $modelCustomerReview,
        Article $modelArticle,
        Subscriber $modelSubscriber,
        CarBrand $modelCarBrand
    )
    {
        $this->modelTour = $modelTour;
        $this->modelCar = $modelCar;
        $this->modelLocation = $modelLocation;
        $this->modelCarTerm = $modelCarTerm;
        $this->modelHotel = $modelHotel;
        $this->modelDestination = $modelDestination;
        $this->modelSubscriber = $modelSubscriber;
        $this->modelCustomerReview = $modelCustomerReview;
        $this->modelArticle = $modelArticle;
        $this->modelCarBrand = $modelCarBrand;
    }
    public function index(){
        $tourFeatured = $this->modelTour::query()->where('is_featured', 1)->where('status', 1)->with([
            'location',
            'reviewTour',
            'wishListTour',
            'startDateTour'=> function($query) {
                $query->orderBy('start_date');
            }
        ])->limit(8)->get();
        $tourSaleHoliday = $this->modelTour::query()->where('sale_holiday', 1)->where('status', 1)->with([
            'location',
            'reviewTour',
            'wishListTour',
            'startDateTour'=> function($query) {
                $query->orderBy('start_date');
            }
        ])->get();
        $locationFeatured = $this->modelLocation->with([
            'hotel',
            'car',
            'tour'
        ])->where('status', 1)->where('is_featured', 1)->limit(4)->get();
        $carFeatured = $this->modelCar::query()->where('featured', 1)->where('status', 1)->with([
            'reviewcar',
            'wishlistCar',
            'location'
        ])->limit(8)->get();
        $carSaleHoliday = $this->modelCar::query()->where('sale_holiday', 1)->where('status', 1)->with([
            'reviewcar',
            'wishlistCar',
            'location'
        ])->get();
        $termType = $this->modelCarTerm::query()->where('attribute_id', 1)->get();
        $hotelFeatured = $this->modelHotel::query()->where('availability', 1)->where('status', 1)->with([
            'wishListHotel',
            'location',
            'review'
        ])->get();
        $hotelSaleHoliday = $this->modelHotel::query()->where('sale_holiday', 1)->where('status', 1)->with([
            'wishListHotel',
            'location',
            'review'
        ])->get();
        $carBrannd = $this->modelCarBrand::all();
        $customerReview = $this->modelCustomerReview::query()->with('location')->get();
        $listDestination = $this->modelDestination::query()->where('status', 1)->get();
        $listLocation = $this->modelLocation->where('status', 1)->get();
        $postFeatured = $this->modelArticle->query()->where('status', 1)->orderByDesc('created_at')->limit(6)->get();
        return view('public.home.home', compact(
            'carBrannd',
            'tourFeatured',
            'carFeatured',
            'termType',
            'hotelFeatured',
            'customerReview',
            'listDestination',
            'listLocation',
            'postFeatured',
            'locationFeatured',
            'tourSaleHoliday',
            'hotelSaleHoliday',
            'carSaleHoliday'
        ));
    }
    public function addNewletter(SubscriberRequest $request)
    {
        $email = $request->email;
        $addSubscriber = new $this->modelSubscriber;
        $addSubscriber->email = $email;
        $addSubscriber->save();
        return response()->json([
            'type' => 1,
            'mess' => "Đăng ký thành công!"
        ]);
    }
}
