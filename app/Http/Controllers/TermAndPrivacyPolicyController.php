<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Modules\Page\Entities\Page;

class TermAndPrivacyPolicyController extends Controller
{
    protected $modelPage;

    public function __construct(Page $modelPage)
    {
        $this->modelPage = $modelPage;
    }
    public function index()
    {
        $termAndPrivacyPolicy = $this->modelPage::query()->where('style', 2)->where('status', 1)->first();
        return view('public.Terms-and-Privacy-Policy.index', compact('termAndPrivacyPolicy'));
    }
}
