<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SubscriberRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email'=>'required|regex:/^[a-z][a-z0-9_\.]{3,32}@[a-z0-9]{2,}(\.[a-z0-9]{2,4}){1,2}(\.[0-9]{2,4}){0,2}$/|unique:subscribers, email,'
        ];
    }
    public function messages(){
        return [
            'email.required' => "Email không được để trống!" ,
            'email.regex' => "Email bạn nhập không đúng định dạng!" ,
            'email.unique' => "Email đã được đăng ký, mời bạn nhập email khác!"
        ];
    }
}
