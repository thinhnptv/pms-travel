<?php

namespace Modules\Post\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Comment\Entities\Comment;
use Modules\User\Entities\User;

class Article extends Model
{
    protected $table = 'articles';
    protected $fillable = [
        'id',
        'title',
        'description',
        'mete_description',
        'content',
        'user_id',
        'seo_description',
        'seo_title',
        'image',
        'slug',
        'created_at',
        'updated_at'
    ];

    public function articleUser()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function articleReview()
    {
        return $this->hasMany(ReviewArticle::class, 'article_id', 'id');
    }

    public function articleComment()
    {
        return $this->hasMany(Comment::class, 'article_id', 'id');
    }

    public function articleTag()
    {
        return $this->hasMany(TagArticle::class, 'article_id', 'id');
    }
}
