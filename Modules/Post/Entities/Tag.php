<?php

namespace Modules\Post\Entities;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    protected $table = 'tags';
    protected $fillable = [
        'name', 'slug', 'status'
    ];

    public function tagArticle()
    {
        return $this->hasMany(TagArticle::class, 'tag_id', 'id');
    }
}
