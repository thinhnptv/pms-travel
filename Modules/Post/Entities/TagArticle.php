<?php

namespace Modules\Post\Entities;

use Illuminate\Database\Eloquent\Model;

class TagArticle extends Model
{
    protected $table = "article_tag";
    protected $fillable = [];
}
