<?php

namespace Modules\Post\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\User\Entities\User;

class ReviewArticle extends Model
{
    protected $table = 'review_article';
    protected $fillable = [];

    public function reviewArticle()
    {
        return $this->belongsTo(Article::class, 'article_id', 'id');
    }

    public function reviewUser()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
