@extends('base::layouts.master')
@section('css')
    <!-- Bootstrap Tables css -->
    <link href="{{ URL::asset('assets/libs/bootstrap-table/bootstrap-table.min.css')}}" rel="stylesheet"
          type="text/css"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection
@section('content')
    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">UBold</a></li>
                            <li class="breadcrumb-item"><a href="javascript: void(0);">News</a></li>
                            <li class="breadcrumb-item active">Category</li>
                        </ol>
                    </div>
                    <h4 class="page-title">New Tags</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->
        <div class="row">
            <div class="container-fluid">

                <!-- start page title -->
                <!-- end page title -->


                <div class="row">
                    <div class="col-lg-4">
                        <div class="card-box">
                            <h4 class="page-title">Add Tags</h4>
                                <form action="{{ route('tags.create_tag') }}" method="post">
                                    @csrf
                                    <div class="form-group mb-3">
                                        <label for="product-name">Name</label>
                                        <input type="text" id="tag-name" name="name" class="form-control"
                                               placeholder="Tag name">
                                        @if($errors->has('name'))
                                            <p class="alert alert-danger">
                                                {{$errors->first('name')}}
                                            </p>
                                        @endif
                                    </div>
                                    <div class="form-group mb-3">
                                        <label for="product-reference">Slug</label>
                                        <input type="text" id="tag-slug" name="slug" class="form-control">
                                    </div>

                                    <div class="form-group mb-3">
                                        <label  class="mb-2">Status</label>
                                        <br/>
                                        <select class="form-control" id="" name="status">
                                                <option value="0">Offline</option>
                                                <option value="1">Online</option>
                                        </select>
                                    </div>
                                    <div class="mb-3">
                                        <button type="submit" class="btn w-sm btn-success waves-effect waves-light">Add new</button>
                                    </div>
                                </form>
                        </div> <!-- end card-box -->
                    </div> <!-- end col -->

                    <div class="col-lg-8">

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card-box">
                                    <div class="columns-left" style="float: left">
                                        <button style="margin-bottom: 10px" id="delete-checkbox-all" class="delete-all btn btn-primary">Delete</button>
                                    </div>
                                    <div style="float: right">
                                            <div class="form-group mx-sm-3 mb-2">
                                                <input type="text" class="form-control" name="search" id="search" placeholder="Search">
                                            </div> 
                                    </div>
                                    @php
                                        $stt = 1;
                                        $start = ($tags->currentPage()-1)*$tags->perPage() +1;
                                    @endphp
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th width="50px"><input type="checkbox" id="customCheckAll"></th>
                                                 <th width="50px">STT</th>
                                                <th >Name</th>
                                                <th>Slug</th>
                                                <th>Date</th>
                                                <th>Status</th>
                                                <th style="width: 82px;">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @if(isset($tags))
                                                @foreach($tags as $tag)
                                            <tr id="tr-{{ $tag->id }}">
                                                <td>
                                                    <input type="checkbox" class="checkbox" id="customCheck-{{ $tag->id }}" data-value="{{ $tag->id }}">
                                                </td>
                                                <td>{{ $start++ }}</td>
                                                <td class="td-name">
                                                    {{ $tag->name }}
                                                </td>
                                                <td>
                                                    {{ $tag->slug }}
                                                </td>
                                                <td>
                                                    {{ $tag->created_at->format('d-m-Y') }}
                                                </td>
                                                <td class="td-status">
                                                    @if($tag->status == 1)
                                                        {{ "Online" }}
                                                    @else
                                                        {{ "Offline" }}
                                                    @endif
                                                </td>
                                                <td width="20%">
                                                    <a href="{{ route('tags.edit_tag', $tag->id) }}" class="action-icon"> <i class="mdi mdi-square-edit-outline"></i></a>
                                                    <a data-id="{{ $tag->id }}" data-toggle="modal" data-target="#delete" type="button" href="" class="action-icon delete"> <i class="mdi mdi-delete"></i></a>
                                                </td>
                                            </tr>
                                                @endforeach
                                            @endif
                                        </tbody>
                                    </table>
                                    <div class="fexPagintaion text-center clearfix">{{ $tags->render() }}</div>
                                </div> <!-- end card-box-->
                            </div> <!-- end col-->
                        </div>
                    </div> <!-- end col-->
                </div>
                <!-- end row -->
            </div> <!-- container -->
        </div>
    </div>

    {{--<div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">--}}
        {{--<div class="modal-dialog" role="document">--}}
            {{--<div class="modal-content">--}}
                {{--<div class="modal-header">--}}
                    {{--<h3 class="modal-title" id="exampleModalLabel">Edit Tags</h3>--}}
                    {{--<button class="close" type="button" data-dismiss="modal" aria-label="Close">--}}
                        {{--<span aria-hidden="true">×</span>--}}
                    {{--</button>--}}
                {{--</div>--}}
                {{--<div class="modal-body">--}}
                    {{--<div class="row" style="margin: 5px">--}}
                        {{--<div class="col-lg-12">--}}
                            {{--<form>--}}
                                {{--<div class="form-group mb-3">--}}
                                    {{--<label for="name">Name</label>--}}
                                    {{--<input type="text" id="name"  name="name" class="form-control name"--}}
                                           {{--placeholder="Tag name">--}}
                                {{--</div>--}}
                                {{--<div class="form-group mb-3">--}}
                                    {{--<label>Status</label>--}}
                                    {{--<select class="form-control" name="status">--}}
                                        {{--<option value="1" class="online">Online</option>--}}
                                        {{--<option value="" class="offline">Offline</option>--}}
                                    {{--</select>--}}
                                {{--</div>--}}
                            {{--</form>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="modal-footer">--}}
                    {{--<button type="button" class="btn btn-success update">Save</button>--}}
                    {{--<button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
    <!-- delete Modal-->
    <div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="exampleModalLabel">Bạn có muốn xóa ?</h4>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body" style="margin-left: 183px;">
                    <button type="button" class="btn btn-success del">Có</button>
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Không</button>
                    <div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop

@section('script-bottom')

    <script src="{{ URL::asset('assets/libs/toastr/toastr.min.js')}}"></script>
    <script type="text/javascript">
        @if(session('success'))
        toastr.success('{{ session('success') }}', 'Thông báo', {timeOut: 5000});
        @endif

        $(document).ready(function () {
            $(document).on('keyup','#search',function(){
                var query = $(this).val();
                search_tag(query);
            });
            function search_tag(query = ' '){
                $.ajax({
                    type: "GET",
                    url: "{{ route('tags.search_tag') }}",
                    data: {query:query},
                    //dataType: "json",
                    success: function (data) {
                        $('tbody').html(data)
                    }
                });
            }


            $(document).on("keyup", "#tag-name", function () {
                let $this = $(this);
                let slug;
                let tag_name = $this.val();

                // Đổi chữ hoa thành chữ thường
                slug = tag_name.toLowerCase();

                //Đổi ký tự có dấu thành không dấu
                slug = slug.replace(/á|à|ả|ạ|ã|ă|ắ|ằ|ẳ|ẵ|ặ|â|ấ|ầ|ẩ|ẫ|ậ/gi, 'a');
                slug = slug.replace(/é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ/gi, 'e');
                slug = slug.replace(/i|í|ì|ỉ|ĩ|ị/gi, 'i');
                slug = slug.replace(/ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ/gi, 'o');
                slug = slug.replace(/ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự/gi, 'u');
                slug = slug.replace(/ý|ỳ|ỷ|ỹ|ỵ/gi, 'y');
                slug = slug.replace(/đ/gi, 'd');
                //Xóa các ký tự đặt biệt
                slug = slug.replace(/\`|\~|\!|\@|\#|\||\$|\%|\^|\&|\*|\(|\)|\+|\=|\,|\.|\/|\?|\>|\<|\'|\"|\:|\;|_/gi, '');
                //Đổi khoảng trắng thành ký tự gạch ngang
                slug = slug.replace(/ /gi, "-");
                //Đổi nhiều ký tự gạch ngang liên tiếp thành 1 ký tự gạch ngang
                //Phòng trường hợp người nhập vào quá nhiều ký tự trắng
                slug = slug.replace(/\-\-\-\-\-/gi, '-');
                slug = slug.replace(/\-\-\-\-/gi, '-');
                slug = slug.replace(/\-\-\-/gi, '-');
                slug = slug.replace(/\-\-/gi, '-');
                //Xóa các ký tự gạch ngang ở đầu và cuối
                slug = '@' + slug + '@';
                slug = slug.replace(/\@\-|\-\@|\@/gi, '');
                $('#tag-slug').val(slug);
            });

            // $('.edit').click(function () {
            //     let id = $(this).data('id'); // lấy data-id của thằng edit
            //     $('.update').attr('data-id', id); // gán data-id của edit cho .update
            //     // Edit
            //     $.ajax({
            //         url : 'tags/update/'+id,
            //         headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            //         dataType : 'json',
            //         type : 'get',
            //         success : function (result) {
            //             console.log(result);
            //             $('.name').val(result.name);
            //             $('.status').val(result.status);
            //             if(result.status == 1){
            //                 $('.online').attr('selected', 'selected');
            //             }
            //             else {
            //                 $('.offline').attr('selected', 'selected');
            //             }
            //         }
            //     });
            // });

            // $('.update').click(function () {
            //     let id = $(this).data('id');
            //     let name = $('.name').val();
            //     let status = $('.status').val();
            //     $.ajax({
            //         url : 'tags/update/'+id,
            //         data : {
            //             name : name,
            //             status : status,
            //         },
            //         headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            //         dataType : 'json',
            //         type : 'put',
            //         success : function (result) {
            //             console.log(result);
            //             toastr.success(result.success, 'Thông báo', {timeOut: 5000});
            //             $('#edit').modal('hide');
            //             $('#tr-'+id).find('.td-name').html(name);
            //             $('#tr-'+id).find('.td-status').html(status);
            //         }
            //     });
            // });

            $('.delete').click(function () {
                let $this = $(this);
                let id = $this.data('id'); // lấy data-id của thằng .delete
                $('.del').attr('data-id', id); // truyền data-id của delete cho .del
            });

            $('.del').click(function () {
                let $this = $(this);
                let id = $this.data('id'); // data-id của thằng .del
                $.ajax({
                    url : 'tags/delete/'+id,
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    dataType : 'json',
                    type : 'get',
                    cache : false,
                    success : function (result)
                    {
                        toastr.success(result.success, 'Thông báo', {timeOut: 5000});
                        $('#delete').modal('hide');
                        // location.reload();
                        // $('#tr-'+id).remove();
                        // $('.delete[data-id ='+id+']').parents('tr').remove();
                    }
                });
            });

            $(document).on("click", "#customCheckAll", function () {
                $('input:checkbox').not(this).prop('checked', this.checked);
            });

            $(document).on("click", "#delete-checkbox-all", function () {
                let value_id = [];
                $('.checkbox:checked').each(function () {
                    value_id.push($(this).data('value'));
                    console.log(value_id);
                });

                if (value_id.length > 0) {
                    $.ajax({
                        url : 'tags/deleteAll',
                        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                        dataType : 'json',
                        type : 'delete',
                        data: {
                            ids : value_id,
                        },
                        success: function (result) {
                            alert("Bạn chắc chắn muốn xóa");
                            toastr.success(result.success, 'Thông báo', {timeOut: 5000});
                            $.each(value_id, function (key, value) {
                                console.log(value);
                                $('#tr-'+value).remove();
                            })
                        }
                    });
                } else {
                    toastr.error("Error", "Bạn cần chọn hàng cần xóa", "error", 1000);
                };
            });
        });

    </script>
    <!-- Init js -->
    <script src="{{ URL::asset('assets/libs/bootstrap-table/bootstrap-table.min.js')}}"></script>
    <!-- Bootstrap Tables js -->
    <script src="{{ URL::asset('assets/js/pages/bootstrap-tables.init.js')}}"></script>
@endsection
