@if(isset($tags))
    @foreach($tags as $tag)
    <tr id="tr-{{ $tag->id }}">
        <td>
            <input type="checkbox" class="checkbox" id="customCheck-{{ $tag->id }}" data-value="{{ $tag->id }}">
        </td>
        <td class="td-name">
            {{ $tag->name }}
        </td>
        <td>
            {{ $tag->slug }}
        </td>
        <td>
            {{ $tag->created_at->format('d-m-Y') }}
        </td>
        <td class="td-status">
            @if($tag->status == 1)
            {{ "Online" }}
            @else
            {{ "Offline" }}
            @endif
        </td>
        <td width="20%">
            <a href="{{ route('tags.edit_tag', $tag->id) }}" class="action-icon"> <i class="mdi mdi-square-edit-outline"></i></a>
            <a data-id="{{ $tag->id }}" data-toggle="modal" data-target="#delete" type="button" href="" class="action-icon delete"> <i class="mdi mdi-delete"></i></a>
        </td>
    </tr>
    @endforeach
@endif