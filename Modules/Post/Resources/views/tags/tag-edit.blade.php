@extends('base::layouts.master')
@section('css')
    <!-- Bootstrap Tables css -->
    <link href="{{ URL::asset('assets/libs/bootstrap-table/bootstrap-table.min.css')}}" rel="stylesheet"
          type="text/css"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection
@section('content')
    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">UBold</a></li>
                            <li class="breadcrumb-item"><a href="javascript: void(0);">News</a></li>
                            <li class="breadcrumb-item active">Category</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Edit Tags</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->
        <div class="row">
            <div class="container-fluid">

                <!-- start page title -->
                <!-- end page title -->


                <div class="row">
                    <div class="col-lg-4">
                        <div class="card-box">
                            <h4 class="page-title">Edit Tags</h4>
                            <form action="{{ route('tags.update_tag', $tags->id) }}" method="post">
                                @csrf
                                <div class="form-group mb-3">
                                    <label for="product-name">Name</label>
                                    <input type="text" id="tag-name"  value="{{ $tags->name }}" name="name" class="form-control"
                                           placeholder="Tag name">
                                    @if($errors->has('name'))
                                        <p class="alert alert-danger">
                                            {{ $errors->first('name') }}
                                        </p>
                                    @endif
                                </div>
                                <div class="form-group mb-3">
                                    <label for="product-reference">Slug</label>
                                    <input value="{{ $tags->slug }}" type="text" id="tag-slug" name="slug" class="form-control">
                                </div>

                                <div class="form-group mb-3">
                                    <select class="form-control" id="" name="status">
                                        <label  class="mb-2">Status</label>
                                        <br/>
                                        @if($tags->status == 0)
                                            <option value="0">Offline</option>
                                            <option value="1">Online</option>
                                        @else
                                            <option value="1">Online</option>
                                            <option value="0">Offline</option>
                                        @endif
                                    </select>
                                </div>
                                <div class="mb-3">
                                    <button type="submit" class="btn w-sm btn-success waves-effect waves-light">Add new</button>
                                </div>
                            </form>
                        </div> <!-- end card-box -->
                    </div> <!-- end col -->

                    <div class="col-lg-8">

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card-box">
                                    <div class="columns-left" style="float: left">
                                        <button style="margin-bottom: 10px" id="delete-checkbox-all" class="delete-all btn btn-primary">Delete</button>
                                    </div>
                                    <div style="float: right">
                                        <form class="form-inline">
                                            <div class="form-group mx-sm-3 mb-2">
                                                <input type="text" class="form-control" id="inputPassword2" placeholder="Search">
                                            </div>
                                            <button type="submit" class="btn btn-primary mb-2">Search Tag</button>
                                        </form>
                                    </div>

                                    <table class="table table-bordered">
                                        <thead>
                                        <tr>
                                            <th width="50px"><input type="checkbox" id="customCheckAll"></th>
                                            <th >Name</th>
                                            <th>Slug</th>
                                            <th>Date</th>
                                            <th>Status</th>
                                            <th style="width: 82px;">Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if(isset($tags))
                                            <tr id="tr">
                                                <td>
                                                    <input type="checkbox" class="checkbox" id="customCheck" data-value="">
                                                </td>
                                                <td class="td-name">
                                                    {{ $tags->name }}
                                                </td>
                                                <td>
                                                    {{ $tags->slug }}
                                                </td>
                                                <td>
                                                    {{ $tags->created_at->format('d-m-Y') }}
                                                </td>
                                                <td class="td-status">
                                                    @if($tags->status == 1)
                                                        {{ "Online" }}
                                                    @else
                                                        {{ "Offline" }}
                                                    @endif
                                                </td>
                                                <td width="20%">
                                                    <a href="" class="action-icon"> <i class="mdi mdi-square-edit-outline"></i></a>
                                                    <a data-id="" data-toggle="modal" data-target="#delete" type="button" href="" class="action-icon delete"> <i class="mdi mdi-delete"></i></a>
                                                </td>
                                            </tr>
                                        @endif
                                        </tbody>
                                    </table>
                                </div> <!-- end card-box-->
                            </div> <!-- end col-->
                        </div>
                    </div> <!-- end col-->
                </div>
                <!-- end row -->
            </div> <!-- container -->
        </div>
    </div>
@stop


@section('script-bottom')
<script>
    $(document).ready(function () {

        // $(document).on('keyup','#search',function(){
        //         var query = $(this).val();
        //         search_space(query);
        //     });
        //     function search_space(query = ' '){
        //         $.ajax({
        //             type: "GET",
        //             url: "{{ route('space.searchspace') }}",
        //             data: {query:query},
        //             //dataType: "json",
        //             success: function (data) {
        //                 $('tbody').html(data)
        //             }
        //         });
        //     }

        $(document).on("keyup", "#tag-name", function () {
            let $this = $(this);
            let slug;
            let tag_name = $this.val();

            // Đổi chữ hoa thành chữ thường
            slug = tag_name.toLowerCase();

            //Đổi ký tự có dấu thành không dấu
            slug = slug.replace(/á|à|ả|ạ|ã|ă|ắ|ằ|ẳ|ẵ|ặ|â|ấ|ầ|ẩ|ẫ|ậ/gi, 'a');
            slug = slug.replace(/é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ/gi, 'e');
            slug = slug.replace(/i|í|ì|ỉ|ĩ|ị/gi, 'i');
            slug = slug.replace(/ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ/gi, 'o');
            slug = slug.replace(/ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự/gi, 'u');
            slug = slug.replace(/ý|ỳ|ỷ|ỹ|ỵ/gi, 'y');
            slug = slug.replace(/đ/gi, 'd');
            //Xóa các ký tự đặt biệt
            slug = slug.replace(/\`|\~|\!|\@|\#|\||\$|\%|\^|\&|\*|\(|\)|\+|\=|\,|\.|\/|\?|\>|\<|\'|\"|\:|\;|_/gi, '');
            //Đổi khoảng trắng thành ký tự gạch ngang
            slug = slug.replace(/ /gi, "-");
            //Đổi nhiều ký tự gạch ngang liên tiếp thành 1 ký tự gạch ngang
            //Phòng trường hợp người nhập vào quá nhiều ký tự trắng
            slug = slug.replace(/\-\-\-\-\-/gi, '-');
            slug = slug.replace(/\-\-\-\-/gi, '-');
            slug = slug.replace(/\-\-\-/gi, '-');
            slug = slug.replace(/\-\-/gi, '-');
            //Xóa các ký tự gạch ngang ở đầu và cuối
            slug = '@' + slug + '@';
            slug = slug.replace(/\@\-|\-\@|\@/gi, '');
            $('#tag-slug').val(slug);
        });
    });
</script>
@endsection
