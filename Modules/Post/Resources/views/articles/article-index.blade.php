@extends('base::layouts.master')
@section('css')
    <link href="{{ URL::asset('assets/libs/bootstrap-table/bootstrap-table.min.css')}}" rel="stylesheet"
          type="text/css"/>
    <link href="{{ URL::asset('assets/libs/toastr/toastr.min.css')}}" rel="stylesheet" type="text/css" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection
@section('content')
    <!-- Start Content-->
    <div class="container-fluid">
        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">UBold</a></li>
                            <li class="breadcrumb-item"><a href="javascript: void(0);">News</a></li>
                            <li class="breadcrumb-item active">All</li>
                        </ol>
                    </div>
                    <h4 class="page-title">All News</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row mb-2">
                            <div class="col-md-6">
                                <form class="form-inline">
                                    <div class="form-group mb-2">
                                        <label for="inputPassword2" class="sr-only">Search</label>
                                        <input type="search" name="search" id="search" class="form-control" id="inputPassword2" placeholder="Search...">
                                    </div>
                                </form>
                            </div>
                            <div class="col-md-6">
                                <div class="text-md-right">
                                    <a href="{{ route('articles.create_article') }}" style="color: #fff"><button type="button" class="btn w-sm btn-success waves-effect waves-light mb-2 mr-2">Add New</button></a>
                                    <button type="button" id="delete-checkbox-all" class="delete-all btn btn-danger waves-effect waves-light mb-2 mr-2">Delete All</button>
                                </div>
                            </div><!-- end col-->
                        </div>

                        <div class="table-responsive">
                            <table class="table table-centered table-borderless table-hover mb-0">
                                <thead class="thead-light">
                                <tr>
                                    <th style="width: 20px;">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="customCheckAll">
                                            <label class="custom-control-label" for="customCheckAll">&nbsp;</label>
                                        </div>
                                    </th>
                                    <th>STT</th>
                                    <th>Title</th>
                                    <th>Date</th>
                                    <th>Status</th>
                                    <th style="width: 82px;">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                    @php
                                        $stt = 1;
                                        $start = ($articles->currentPage()-1)*$articles->perPage() +1;
                                    @endphp
                                    @if (isset($articles))
                                        @foreach($articles as $article)
                                            <tr class="tr-article-{{ $article->id }}">
                                                <td>
                                                    <div class="custom-control custom-checkbox">
                                                        <input type="checkbox" class="checkbox custom-control-input" id="customCheck-{{ $article->id }}" data-value="{{ $article->id }}">
                                                        <label class="custom-control-label" for="customCheck-{{ $article->id }}">&nbsp;</label>
                                                    </div>
                                                </td>
                                                <th>{{ $start ++}}</th>
                                                <td>
                                                    {{ $article->title }}
                                                </td>
                                                <td>
                                                    {{ $article->created_at->format('d-m-Y') }}
                                                </td>
                                                <td>
                                                    @if($article->status == 1)
                                                        {{ "Online" }}
                                                    @else
                                                        {{ "Offline" }}
                                                    @endif
                                                </td>
                                                <td>
                                                    <a href="{{ route('articles.edit_article', $article->id) }}" class="action-icon"> <i class="mdi mdi-square-edit-outline"></i></a>
                                                    <a data-id="{{ $article->id }}" data-toggle="modal" data-target="#delete" type="button" href="" class="action-icon delete"> <i class="mdi mdi-delete"></i></a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                            <div class="fexPagintaion text-center clearfix">{{ $articles->render() }}</div>
                        </div>
                    </div> <!-- end card-body-->
                </div> <!-- end card-->
            </div> <!-- end col -->
        </div>
    </div>

    <div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Bạn có muốn xóa ?</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body" style="margin-left: 183px;">
                    <button type="button" class="btn btn-success del">Có</button>
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Không</button>
                    <div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('script-bottom')

    <script src="{{ URL::asset('assets/libs/toastr/toastr.min.js')}}"></script>
    <script type="text/javascript">

        @if(session('success'))
            toastr.success('{{ session('success') }}', 'Thông báo', {timeOut: 5000});
        @endif

        $(document).ready(function ()
        {
            $(document).on('keyup','#search',function(){
                var query = $(this).val();
                search_article(query);
            });
            function search_article(query = ' '){
                $.ajax({
                    type: "GET",
                    url: "{{ route('articles.search_article') }}",
                    data: {query:query},
                    //dataType: "json",
                    success: function (data) {
                        $('tbody').html(data)
                    }
                });
            }

            $(document).on("click", "#customCheckAll", function () {
                $('input:checkbox').not(this).prop('checked', this.checked);
            });
            $(document).on("click", "#delete-checkbox-all", function () {
                let value_id = [];
                $('.checkbox:checked').each(function () {
                    value_id.push($(this).data('value'));
                    console.log(value_id);
                });

                if (value_id.length > 0) {
                    $.ajax({
                        url : 'articles/deleteAll',
                        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                        dataType : 'json',
                        type : 'delete',
                        data: {
                            ids : value_id,
                        },
                        success: function (result) {
                            alert("Bạn chắc chắn muốn xóa");
                            toastr.success(result.success, 'Thông báo', {timeOut: 5000});
                            $.each(value_id, function (key, value) {
                                console.log(value);
                                $('.tr-article-'+value).remove();
                            })
                        }
                    });
                } else {
                    toastr.error("Error", "Bạn cần chọn hàng cần xóa", "error", 1000);
                };
            });


            $(document).on("click", ".delete", function () {
                let $this = $(this);
                let id = $this.data('id'); // lấy data-id của thằng .delete
                $('.del').attr('data-id', id); // truyền data-id của delete cho .del
            });

            $(document).on("click", ".del", function () {
                let $this = $(this);
                let id = $this.data('id');  // data-id của thằng .del
                $.ajax({
                    url : 'articles/delete/'+id,
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    dataType : 'json',
                    type : 'delete',
                    cache : false,
                    success : function (result)
                    {
                        toastr.success(result.success, 'Thông báo', {timeOut: 5000});
                        $('#delete').modal('hide');
                        location.reload();

                        // $('.tr-article-'+id).remove();
                    }
                });
            });
        });
    </script>

    <!-- Init js -->

    <script src="{{ URL::asset('assets/libs/bootstrap-table/bootstrap-table.min.js')}}"></script>

    <!-- Bootstrap Tables js -->

    <script src="{{ URL::asset('assets/js/pages/bootstrap-tables.init.js')}}"></script>
@endsection




