    @if (isset($articles))
        @foreach($articles as $article)
            <tr class="tr-article-{{ $article->id }}">
                <td>
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="checkbox custom-control-input" id="customCheck-{{ $article->id }}" data-value="{{ $article->id }}">
                        <label class="custom-control-label" for="customCheck-{{ $article->id }}">&nbsp;</label>
                    </div>
                </td>
                <td>
                    {{ $article->title }}
                </td>
                <td>
                    {{ $article->created_at->format('d-m-Y') }}
                </td>
                <td>
                    @if($article->status == 1)
                        {{ "Online" }}
                    @else
                        {{ "Offline" }}
                    @endif
                </td>
                <td>
                    <a href="{{ route('articles.edit_article', $article->id) }}" class="action-icon"> <i class="mdi mdi-square-edit-outline"></i></a>
                    <a data-id="{{ $article->id }}" data-toggle="modal" data-target="#delete" type="button" href="" class="action-icon delete"> <i class="mdi mdi-delete"></i></a>
                </td>
            </tr>
        @endforeach
    @endif

