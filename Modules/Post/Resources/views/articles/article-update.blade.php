@extends('base::layouts.master')
@section('css')
    <!-- Plugins css-->
    <link href="{{ URL::asset('assets/libs/select2/select2.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ URL::asset('assets/libs/summernote/summernote.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ URL::asset('assets/libs/dropzone/dropzone.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ URL::asset('assets/css/webt.css')}}" rel="stylesheet" type="text/css"/>
    <style>
    .select2-container .select2-selection--multiple {
        min-width: 500px;
    }
    .select2-container--open .select2-dropdown--below {
        min-width: 500px;
    }
    .select2-dropdown--above {
        min-width: 500px;
    }
    </style>
@endsection
@section('content')
    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">UBold</a></li>
                            <li class="breadcrumb-item"><a href="javascript: void(0);">eCommerce</a></li>
                            <li class="breadcrumb-item active">Product Edit</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Edit new</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->

        <form action="" method="post">
            @csrf
            <div class="row">
                <div class="col-lg-6">
                    <div class="card-box">
                        <div class="form-group mb-3">
                            <label for="product-name">Title <span class="text-danger">*</span></label>
                            <input type="text" id="product-name" value="{{ $articles->title }}" class="form-control" placeholder="" name="title">
                            @if($errors->has('title'))
                                <div class="error-text">
                                    {{$errors->first('title')}}
                                </div>
                            @endif
                        </div>

                        <div class="form-group mb-3">
                            <label for="product-description">Content <span class="text-danger">*</span></label>
                            <textarea class="form-control" id="product-description" rows="5" name="content" placeholder="Please enter description">{{ $articles->content }}</textarea>
                            @if($errors->has('content'))
                                <div class="error-text">
                                    {{$errors->first('content')}}
                                </div>
                            @endif
                        </div>

                        <div class="form-group mb-3">
                            <h5 class="text-uppercase mt-0 mb-3 bg-light p-2">Seo Manager</h5>

                            <div class="form-group mb-3">
                                <label for="product-meta-title">Seo title</label>
                                <input value="{{ $articles->seo_title }}" name="seo_title" type="text" class="form-control" id="product-meta-title" placeholder="Enter title">
                            </div>
                            <div class="form-group mb-0">
                                <label for="product-meta-description">Seo Description </label>
                                <textarea name="seo_description" class="form-control" rows="5" id="product-meta-description" placeholder="Enter description">{{ $articles->seo_description }}</textarea>
                            </div>
                        </div>

                        <div class="form-group mb-3">
                            <label  class="mb-2">Status</label>
                            <br/>
                            <select class="form-control" id="" name="status">
                                @if($articles->status == 0)
                                    <option value="0">Offline</option>
                                    <option value="1">Online</option>
                                @else
                                    <option value="1">Online</option>
                                    <option value="0">Offline</option>
                                @endif
                            </select>
                        </div>

                    </div> <!-- end card-box -->
                </div> <!-- end col -->

                <div class="col-lg-6">
                    <div class="card-box">
                        <h5 class="text-uppercase mt-0 mb-3 bg-light p-2">Images</h5>
                        <div class="card-box text-center">
                            <span class="input-group-btn">
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bd-example-modal-image">Upload image</button>
                                <input type="hidden" name="avatar" id="avatar" value="{{ $articles->image }}">
                            </span>
                            <div id="preview_avatar" class="mt-3">
                                @if(!empty($articles->image))
                                    <div class="box_imgg position-relative">
                                        <img src="{{ config('app.url') }}{{ $articles->image }}" id='show-img-avatar' style="width:100%;">
                                        <i class="mdi mdi-close-circle-outline style_icon_remove style_icon_remove_avatar" title="delete"></i>
                                    </div>
                                @endif
                            </div>
                        </div>

                    </div> <!-- end col-->

                    <div class="card-box">
                        <h5 class="text-uppercase mt-0 mb-3 bg-light p-2">Tag</h5>
                        <div class="form-group mb-0">
                            <label for="product-meta-description">Tag: </label>
                                <select class="js-example-basic-multiple" name="tag[]" multiple="multiple" >
                                    @foreach ($tags as $tag)
                                    <option value="{{ $tag->id }}"
                                        @foreach ($articles->articleTag as $item)
                                        {{ ($item->tag_id == $tag->id) ? 'selected' : ' '  }}
                                        @endforeach
                                        >{{ $tag->name }}</option>
                                    @endforeach
                                </select>
                        </div>
                    </div> <!-- end card-box -->
                </div> <!-- end col-->
            </div>
            <!-- end row -->

            <div class="row">
                <div class="col-12">
                    <div class="text-center mb-3">
                        <button type="submit" class="btn w-sm btn-success waves-effect waves-light">Save</button>
                        <a href="{{ route('articles.list_article') }}"><button type="button" class="btn w-sm btn-danger waves-effect waves-light">Cancel</button></a>
                    </div>
                </div> <!-- end col -->
            </div>
        </form>

        <!-- end row -->
    </div>
    <!-- container -->



@stop
@section('js')
    <div class="modal fade bd-example-modal-image" id="modal-file" tabindex="-1" role="dialog" data-backdrop="false" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Manger Image</h5>
                    <button type="button" class="close" data-dismiss="modal"><i class="fas fa-times"></i></button>
                </div>
                <div class="modal-body">
                    <iframe src="{{ config('app.url') }}/filemanager/dialog.php?akey=4Ans6pfTs6A9DzHj8AZq1NVGKoYxpyoI8ivCN1bOow&field_id=avatar&multiple=0" frameborder="0" style="width: 100%; height: 500px; overflow-y: auto"></iframe>
                </div>
            </div>
        </div>
    </div>

    {{--<div class="modal fade bd-example-modal-banner" id="modal-file-banner" tabindex="-1" role="dialog" data-backdrop="false" aria-hidden="true">--}}
        {{--<div class="modal-dialog modal-lg">--}}
            {{--<div class="modal-content">--}}
                {{--<div class="modal-header">--}}
                    {{--<h5 class="modal-title" id="exampleModalLabel">Manger Image</h5>--}}
                    {{--<button type="button" class="close" data-dismiss="modal"><i class="fas fa-times"></i></button>--}}
                {{--</div>--}}
                {{--<div class="modal-body">--}}
                    {{--<iframe src="{{ config('app.url') }}/filemanager/dialog.php?akey=4Ans6pfTs6A9DzHj8AZq1NVGKoYxpyoI8ivCN1bOow&field_id=banner&multiple=0" frameborder="0" style="width: 100%; height: 500px; overflow-y: auto"></iframe>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}

    {{--<div class="modal fade bd-example-modal-album" id="modal-file" tabindex="-1" role="dialog" data-backdrop="false" aria-hidden="true">--}}
    {{--<div class="modal-dialog modal-lg">--}}
    {{--<div class="modal-content">--}}
    {{--<div class="modal-header">--}}
    {{--<h5 class="modal-title" id="exampleModalLabel">Manger Image</h5>--}}
    {{--<button type="button" class="close" data-dismiss="modal"><i class="fas fa-times"></i></button>--}}
    {{--</div>--}}
    {{--<div class="modal-body">--}}
    {{--<iframe src="{{ config('app.url') }}/filemanager/dialog.php?akey=4Ans6pfTs6A9DzHj8AZq1NVGKoYxpyoI8ivCN1bOow&field_id=album&multiple=1" frameborder="0" style="width: 100%; height: 500px; overflow-y: auto"></iframe>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
@stop

@section('script')

    <!-- Summernote js -->
    <script src="{{ URL::asset('assets/libs/summernote/summernote.min.js')}}"></script>
    <!-- Select2 js-->
    <script src="{{ URL::asset('assets/libs/select2/select2.min.js')}}"></script>
    <!-- Dropzone file uploads-->
    <script src="{{ URL::asset('assets/libs/dropzone/dropzone.min.js')}}"></script>

    <!-- Init js -->
    <script src="{{ URL::asset('assets/js/pages/add-product.init.js')}}"></script>

    <script>
        $(document).ready(function () {
            $('.js-example-basic-multiple').select2();
            $(document).on('hide.bs.modal', '.bd-example-modal-image', function () {
                var _img = $('input#avatar').val();
                if (!_img.length) {
                    $('#preview_avatar').empty();
                } else {
                    $('#preview_avatar').empty();
                    $html = `
            <div class="box_imgg position-relative">
                <img src="" id='show-img-hotel' style="width:100%;">
                <i class="mdi mdi-close-circle-outline style_icon_remove style_icon_remove_avatar" title="delete"></i>
            </div>
        `;
                    $('#preview_avatar').append($html);
                    $('#show-img-hotel').attr('src', _img);
                }
            });

            $(document).on('click', '.style_icon_remove_avatar', function() {
                $(this).parent('.box_imgg').hide('slow', function () {
                    $(this).remove();
                    $('#avatar').val('');
                });
            });
        });
    </script>
@endsection
