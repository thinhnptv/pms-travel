<?php

//Route::group(['prefix' => 'laravel-filemanager'], function () {
//    \UniSharp\LaravelFilemanager\Lfm::routes();
//});

//Route::group(['prefix' => 'laravel-filemanager'], function () {
//    \UniSharp\LaravelFilemanager\Lfm::routes();
//});

use Illuminate\Support\Facades\Route;

Route::group(['middleware' => ['web', 'adminLogin'], 'prefix' => 'admin/articles', 'namespace' => 'Modules\Post\Http\Controllers'], function()
{
    Route::get('/', 'ArticleController@index')->name('articles.list_article');
    Route::get('/create', 'ArticleController@create')->name('articles.create_article');
    Route::post('/create', 'ArticleController@store');
    Route::get('/update/{id}', 'ArticleController@edit')->name('articles.edit_article');
    Route::post('/update/{id}', 'ArticleController@update');
    Route::delete('/delete/{id}', 'ArticleController@destroy')->name('articles.delete_article');
    Route::delete('/deleteAll', 'ArticleController@deleteMultiple')->name('articles.deleteAll_article');
    Route::get('/search','ArticleController@search')->name('articles.search_article');
});

Route::group(['middleware' => 'web', 'prefix' => 'tags', 'namespace' => 'Modules\Post\Http\Controllers'], function()
{
    Route::get('/', 'TagController@index')->name('tags.list_tag');
    Route::post('/create', 'TagController@store')->name('tags.create_tag');
    Route::get('/update/{id}', 'TagController@edit')->name('tags.edit_tag');
    Route::post('/update/{id}', 'TagController@update')->name('tags.update_tag');
    Route::get('/delete/{id}', 'TagController@destroy')->name('tags.delete_tag');
    Route::delete('/deleteAll', 'TagController@deleteMultiple');
    Route::get('/search','TagController@search')->name('tags.search_tag');
});
