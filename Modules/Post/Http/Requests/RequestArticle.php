<?php

namespace Modules\Post\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RequestArticle extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'content' => 'required|min:20|max:10000',
        ];
    }

    public function messages()
    {
        return [
            'title.required' => 'Tên không được để trống',
            'content.required' => 'Nội dung không được để trống',
            'content.min' => 'Độ dài nội dung quá ngắn phải trên 20 ký tự',
            'content.max' => 'Độ dài nội dung quá dài tối đa 10000 ký tự',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
