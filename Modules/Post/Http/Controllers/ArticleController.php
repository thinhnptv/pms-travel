<?php

namespace Modules\Post\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Post\Entities\Article;
use Modules\Post\Entities\Tag;
use Modules\Post\Entities\TagArticle;
use Modules\Post\Http\Requests\RequestArticle;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $articles = Article::query()->with('articleUser')->orderBy('id','desc')->paginate('15');
        return view('post::articles.article-index', compact('articles'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $tags = Tag::where('status', 1)->get();
        return view('post::articles.article-create',['tags' => $tags]);
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(RequestArticle $requestArticle)
    {
        $str_avatar = '';
        if ($requestArticle->input('avatar')) {
            $str_avatar = checkImage($requestArticle->input('avatar'));
        }
        $articles = new Article();
        $articles->title = $requestArticle->title;
        $articles->slug = str_slug($requestArticle->title);
        $articles->content = $requestArticle->content;
        $articles->seo_title = $requestArticle->seo_title;
        $articles->seo_description = $requestArticle->seo_description;
        $articles->status = $requestArticle->status;
        $articles->image = $str_avatar;
        $articles->save();
        $articles_id =  $articles->latest()->max('id');
        if(!empty($requestArticle->tag)){
            foreach($requestArticle->tag as $key=>$val){
                $article_tag = new TagArticle();

                $article_tag->article_id = $articles_id;
                $article_tag->tag_id = $val;
                $article_tag->save();
            }
        }
        return redirect()->route('articles.list_article')->with(['success' => 'Thêm mới thành công']);
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('articles::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {
        $tags = Tag::where('status', 1)->get();
        $articles = Article::query()->with(['articleTag'])->find($id);

        //dd($articles);
        return view('post::articles.article-update', compact('articles','tags'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(RequestArticle $requestArticle, $id)
    {
        $articles = Article::find($id);
        $articles_id =  $articles->id;
        $tagexist = TagArticle::select('id')->where('article_id',$articles_id)->get()->toArray();
        TagArticle::whereIn('id',$tagexist)->delete();
        // Article::destroy($id);

        $str_avatar = '';
        if ($requestArticle->input('avatar')) {
            $str_avatar = checkImage($requestArticle->input('avatar'));
        }

        $articles->title = $requestArticle->title;
        $articles->slug = str_slug($requestArticle->title);
        $articles->content = $requestArticle->content;
        $articles->seo_title = $requestArticle->seo_title;
        $articles->status = $requestArticle->status;
        $articles->image = $str_avatar;
        $articles->save();
        if(!empty($requestArticle->tag)){
            foreach($requestArticle->tag as $key=>$val){
                $article_tag = new TagArticle();
                $article_tag->article_id = $articles_id;
                $article_tag->tag_id = $val;
                $article_tag->save();
            }
        }
        return redirect()->route('articles.list_article')->with(['success' => 'Cập nhật thành công']);
    }
    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {
        $articles = Article::findOrFail($id);
        $articles->delete();
        return response()->json(['success' => 'Xóa thành công']);
    }
    public function deleteMultiple(Request $request){
        $ids = $request->ids;
        Article::whereIn('id', $ids)->delete();
        return response()->json(['success' => 'Xóa thành công']);
    }

    public function search(Request $request,Article $article)
    {
        if($request->ajax()){
            $query = $request->get('query');
            if($query != ' '){
                $data = Article::query()->where('title','like','%'.$query.'%')->get();
            }else{
                $data = Article::all();
            }
            if($data->count() > 0){
                $articles = $data;
                return view('post::articles.article-search', compact('articles'));
            }
            else{
                return view('post::articles.article-search-nodata');
            }
        }
    }


}
