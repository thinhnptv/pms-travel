<?php

namespace Modules\Post\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Post\Entities\Tag;
use Modules\Post\Http\Requests\RequestTag;

class TagController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $tags = Tag::paginate('2');
        return view('post::tags.tag-index', compact('tags'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('post::tags.tag-index');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(RequestTag $requestTag)
    {
        $tags = new Tag();
        $tags->name = $requestTag->name;
        $tags->slug = str_slug($requestTag->name);
        $tags->status = $requestTag->status;
        $tags->save();
        return redirect()->route('tags.list_tag')->with(['success' => 'Thêm mới thành công']);
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('post::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {
        $tags = Tag::find($id);
        return view('post::tags.tag-edit', compact('tags'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(RequestTag $requestTag, $id)
    {
        $tags = Tag::find($id);
        $tags->name = $requestTag->name;
        $tags->slug = str_slug($requestTag->name);
        $tags->status = $requestTag->status;
        $tags->save();
        return redirect()->route('tags.list_tag')->with(['success' => 'Cập nhật thành công']);
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {
        $tags = Tag::findOrFail($id);
        $tags->delete();
        return response()->json(['success' => 'Xóa thành công']);
    }
    public function deleteMultiple(Request $request){
        $ids = $request->ids;
        Tag::whereIn('id', $ids)->delete();
        return response()->json(['success' => 'Xóa thành công']);
    }
    public function search(Request $request,Tag $tag)
    {
        if($request->ajax()){
            $query = $request->get('query');
            if($query != ' '){
                $data = Tag::query()->where('name','like','%'.$query.'%')->get();
            }else{
                $data = Tag::all();
            }
            if($data->count() > 0){
                $tags = $data;
                return view('post::tags.tag-search', compact('tags'));
            }
            else{
                return view('post::tags.tag-search-nodata');
            }
        }
    }
}
