<?php

namespace Modules\Hotel\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Hotel\Entities\Booking;
use Modules\Hotel\Entities\ExtraPriceBooked;
use Modules\User\Entities\User;

class ReportsController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    protected $modelUser;
    protected $modelReport;
    protected $modelExtraPriceBooked;

    public function __construct(User $modelUser, Booking $modelReport, ExtraPriceBooked $modelExtraPriceBooked)
    {
        $this->modelUser = $modelUser;
        $this->modelReport = $modelReport;
        $this->modelExtraPriceBooked = $modelExtraPriceBooked;
    }
    public function index()
    {
        $list_booking_room = $this->modelReport::query() ->with([
            'user',
            'extraPriceBooked',
            'room' => function($query){
                $query->with(['hotel' => function($e){
                    $e->with('user');
                }]);
            }
            ])->paginate(15);
            foreach($list_booking_room as $item) {
                $item->total = getTotalBookingRoom($item);
            }
    
    // dd($list_booking_room);
        return view('hotel::rooms.list_room_booking', compact('list_booking_room'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('hotel::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('hotel::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('hotel::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
    public function deleteAll(Request $request)
    {
        $list_id = $request->list_id;
        foreach($list_id as $item){
            $delete_Booking_room = $this->modelReport->findOrFail($item);
            $delete_Booking_room->delete();
        }
        return response()->json([
            'type' => 1,
            'mess' => "Xóa thành công"
        ]);
    }
    public function changeStatus(Request $request)
    {
        $list_id = $request->list_id;
        $selected_action = $request->selected_action;
        foreach($list_id as $item){
            $change_status_booking_room = $this->modelReport->findOrFail($item);
            $change_status_booking_room->status = $selected_action;
            $change_status_booking_room->save();
        }
        return response()->json([
            'type' => 1,
            'status' => showStatusBoking($selected_action),
            'mess' => "Chuyển thành công"
        ]);
    }

}
