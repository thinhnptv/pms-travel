<?php

namespace Modules\Hotel\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Category\Entities\Location;
use Modules\Hotel\Entities\Hotel;
use Modules\User\Entities\User;
use Modules\User\Entities\VendorRequest;
use Modules\Category\Entities\HotelAttribute;
use Modules\Category\Entities\Term;
use Modules\Hotel\Entities\HotelType;
use Modules\Hotel\Entities\Policy;
use Modules\Hotel\Entities\Price;
use Modules\Hotel\Entities\Utility;
use Modules\Hotel\Http\Requests\CreateHotelRequest;

class HotelController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    protected $modelLocation;
    protected $modelHotel;
    protected $modelUser;
    protected $modelTerm;
    protected $modelPolicy;
    protected $modelPrice;
    protected $modelTypeHotel;
    protected $modelUtility;

    public function __construct(Hotel $modelHotel, Location $modelLocation, User $modelUser, Term $modelTerm, Policy $modelPolicy, Price $modelPrice, HotelType $modelTypeHotel, Utility $modelUtility)
    {
        $this->modelHotel = $modelHotel;
        $this->modelLocation = $modelLocation;
        $this->modelUser = $modelUser;
        $this->modelTerm = $modelTerm;
        $this->modelPolicy = $modelPolicy;
        $this->modelPrice = $modelPrice;
        $this->modelTypeHotel = $modelTypeHotel;
        $this->modelUtility = $modelUtility;
        
    }
    public function index(Request $request)
    {
        // dd($request);
        $all_location =  $this->modelLocation->all();
        $all_user =  $this->modelUser->all();
        $list_hotel = $this->modelHotel::query()->with('location')->with('review');
        if(!empty($request->name)){
            $name = $request->name;
            $list_hotel->where('name', 'like', '%' .$name. '%');
        }
        $list_hotel = $list_hotel->paginate(15);
        return view('hotel::hotels.list', compact('all_location', 'list_hotel', 'all_user'));
    }
    
    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $list_property = $this->modelTerm::query()->where('attribute_id', 4)->get();
        $list_facilities = $this->modelTerm::query()->where('attribute_id', 5)->get();
        $list_service = $this->modelTerm::query()->where('attribute_id', 3)->get();
        $vendor = $this->modelUser::query()->where('role_id', 3)->where('status', 1)->get();
        $location = $this->modelLocation::query()->where('status', 1)->get();
        $typeHotel = $this->modelTypeHotel::query()->where('status', 1)->get();
        return view('hotel::hotels.add', compact('vendor', 'location', 'list_property', 'list_facilities', 'list_service', 'typeHotel'));
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    // public static function check_size_image_default($url)
    // {
    //     if (file_exists($url)) {
    //         $infor_image = getimagesize($url);
    //         $width = (int)$infor_image[0];
    //         $height = (int)$infor_image[1];
    //         $mime = $infor_image['mime'];
    //         $mime_origin = ['image/jpeg', 'image/jpg', 'image/png'];
    //         if ($width < 3000 && in_array($mime, $mime_origin)) {
    //             return true;
    //         }
    //     } else {
    //     return false;
    //     }
        
    // }
    public function store(CreateHotelRequest $request)
    {
        
        $str_feature = '';
        $str_banner = '';
        $str_album = '';

        if ($request->input('banner')) {
            $file_url = parse_url($request->input('banner'));
            $url = public_path($file_url['path']);
            $str_banner = $file_url['path'];
        }
        if($request->input('album')){
            $str_request_album = json_decode($request->album, true);
            $str_album = "[\"";
            foreach($str_request_album as $item) {
                $file_url = parse_url($item);
                $url = public_path($file_url['path']);
                $str_album .= $file_url['path'] . '","';
            }
            $str_album = substr($str_album, 0, -2);
            $str_album .= "]";
        }
        if ($request->input('feature')) {
            $file_url = parse_url($request->input('feature'));
            $url = public_path($file_url['path']);
            // if ($this->check_size_image_default($url) == true) {
            //     $hotel_image_detail = $file_url['path'];
            // } else {
            //     return redirect()->back()->with('errorsss', "Max zize" ); //MAX_SIZE_IMAGE_HOTEL_DETAIL
            // }
            $str_feature = $file_url['path'];
        }
        $add_hotel = $this->modelHotel;
        $add_hotel->name = $request->name;
        $add_hotel->content = $request->content;
        $add_hotel->rating_star = $request->rating_star;
        $add_hotel->location_id = $request->location_id;
        $add_hotel->type_id = $request->type_id;
        $add_hotel->vendor_id = $request->vendor_id;
        $add_hotel->address = $request->address;
        $add_hotel->latitu = $request->latitu;
        $add_hotel->longtitu = $request->longtitu;
        $add_hotel->zoom = $request->zoom;
        $add_hotel->checkin = $request->checkin;
        $add_hotel->checkout = $request->checkout;
        $add_hotel->price = $request->price;
        $add_hotel->seo_title = $request->seo_title;
        $add_hotel->seo_description = $request->seo_description;
        $add_hotel->status = $request->status;
        $add_hotel->video = $request->video;
        $add_hotel->hotel_sale = $request->hotel_sale;
        $add_hotel->feature = $str_feature;
        $add_hotel->banner = $str_banner;
        $add_hotel->album = $str_album;
        $add_hotel->slug = str_slug($request->name);
        if(!empty($request->availability)) {
            $add_hotel->availability = $request->availability;
        }else {
            $add_hotel->availability = 0;
        }
        if(!empty($request->sale_holiday)) {
            $add_hotel->sale_holiday = $request->sale_holiday;
        }else {
            $add_hotel->sale_holiday = 0;
        }
        $add_hotel->save();

        if(!empty($request->policy)){
            foreach($request->policy as $items){
                if(!empty($items['title']) && !empty($items['content'])){
                    $new_policy = new $this->modelPolicy;
                    $new_policy->content = $items['content'];
                    $new_policy->title = $items['title'];
                    $new_policy->hotel_id = $add_hotel->id;
                    $new_policy->save();
                }
            }
        }
        if(!empty($request->utilities)){
            foreach($request->utilities as $item){
                if(!empty($item['title'])){
                    $new_utilities = new $this->modelUtility;
                    $new_utilities->title = $item['title'];
                    $new_utilities->hotel_id = $add_hotel->id;
                    $new_utilities->save();
                }
            }
        }

        if (!empty($request->extra_price)){
            foreach ($request->extra_price as $values){
                if (!empty($values['title']) && !empty($values['price'])) {
                    $new_price = new $this->modelPrice;
                    $new_price->title = $values['title'];
                    $new_price->price = $values['price'];
                    $new_price->type = $values['type'];
                    $new_price->hotel_id = $add_hotel->id;
                    $new_price->save();
                }
            }
        }

        if(!empty($request->property_term)){
            $add_hotel->property()->attach($request->property_term);
        }
        if(!empty($request->facility_term)){
            $add_hotel->facility()->attach($request->facility_term);
        }
        if(!empty($request->service_term)){
            $add_hotel->service()->attach($request->service_term);
        }
        
        return redirect()->route('hotel.list')->with('success', 'Thêm mới thành công');
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('hotel::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {
        $list_property = $this->modelTerm::query()->where('attribute_id', 4)->get();
        $list_facilities = $this->modelTerm::query()->where('attribute_id', 5)->get();
        $list_service = $this->modelTerm::query()->where('attribute_id', 3)->get();
        $vendor = $this->modelUser::query()->where('role_id', 3)->where('status', 1)->get();
        $location = $this->modelLocation::query()->where('status', 1)->get();
        $hotel = $this->modelHotel::query()->with('user')->findOrFail($id);
        // dd($hotel);
        $typeHotel = $this->modelTypeHotel::query()->where('status', 1)->get();
        $arr_property_selected = [];
        foreach($hotel->property as $item) {
            $arr_property_selected[] = $item->id;
        }
        $arr_facility_selected = [];
        foreach($hotel->facility as $item) {
            $arr_facility_selected[] = $item->id;
        }
        $arr_service_selected = [];
        foreach($hotel->service as $item) {
            $arr_service_selected[] = $item->id;
        }
        // dd($hotel);
        return view('hotel::hotels.edit', compact('hotel', 'location', 'list_property', 'list_facilities', 'list_service', 'vendor', 'arr_property_selected', 'arr_facility_selected', 'arr_service_selected', 'typeHotel'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update($id, CreateHotelRequest $request)
    {
        $str_feature = '';
        $str_banner = '';
        $str_album = '';

        if ($request->input('banner')) {
            $file_url = parse_url($request->input('banner'));
            $url = public_path($file_url['path']);
            // if ($this->check_size_image_default($url) == true) {
            //     $hotel_image_detail = $file_url['path'];
            // } else {
            //     return redirect()->back()->with('errorsss', "Max zize" ); //MAX_SIZE_IMAGE_HOTEL_DETAIL
            // }
            $str_banner = $file_url['path'];
        }
        if($request->input('album')){
            $str_request_album = json_decode($request->album, true);
            $str_album = "[\"";
            foreach($str_request_album as $item) {
                $file_url = parse_url($item);
                $url = public_path($file_url['path']);
                // if ($this->check_size_image_default($url) == true) {
                //     $hotel_image_detail = $file_url['path'];
                // } else {
                //     return redirect()->back()->with('errorsss', "Max zize" ); //MAX_SIZE_IMAGE_HOTEL_DETAIL
                // }
                $str_album .= $file_url['path'] . '","';
            }
            $str_album = substr($str_album, 0, -2);
            $str_album .= "]";
        }
        if ($request->input('feature')) {
            $file_url = parse_url($request->input('feature'));
            $url = public_path($file_url['path']);
            // if ($this->check_size_image_default($url) == true) {
            //     $hotel_image_detail = $file_url['path'];
            // } else {
            //     return redirect()->back()->with('errorsss', "Max zize" ); //MAX_SIZE_IMAGE_HOTEL_DETAIL
            // }
            $str_feature = $file_url['path'];
        }
        $update_hotel = $this->modelHotel->findOrFail($id);
        $update_hotel->name = $request->name;
        $update_hotel->content = $request->content;
        $update_hotel->rating_star = $request->rating_star;
        $update_hotel->location_id = $request->location_id;
        $update_hotel->type_id = $request->type_id;
        $update_hotel->vendor_id = $request->vendor_id;
        $update_hotel->address = $request->address;
        $update_hotel->latitu = $request->latitu;
        $update_hotel->longtitu = $request->longtitu;
        $update_hotel->zoom = $request->zoom;
        $update_hotel->checkin = $request->checkin;
        $update_hotel->checkout = $request->checkout;
        $update_hotel->price = $request->price;
        $update_hotel->seo_title = $request->seo_title;
        $update_hotel->seo_description = $request->seo_description;
        $update_hotel->status = $request->status;
        $update_hotel->video = $request->video;
        $update_hotel->hotel_sale = $request->hotel_sale;
        $update_hotel->feature = $str_feature;
        $update_hotel->banner = $str_banner;
        $update_hotel->album = $str_album;
        $update_hotel->slug = str_slug($request->name);
        if(!empty($request->availability)) {
            $update_hotel->availability = $request->availability;
        }else {
            $update_hotel->availability = 0;
        }
        if(!empty($request->sale_holiday)) {
            $update_hotel->sale_holiday = $request->sale_holiday;
        }else {
            $update_hotel->sale_holiday = 0;
        }
        $update_hotel->save();

        $delete_policy = $this->modelPolicy::query()->where('hotel_id', $id)->delete();
        if(!empty($request->policy)){
            foreach($request->policy as $items){
                if(!empty($items['title']) && !empty($items['content'])){
                    $new_policy = new $this->modelPolicy;
                    $new_policy->content = $items['content'];
                    $new_policy->title = $items['title'];
                    $new_policy->hotel_id = $update_hotel->id;
                    $new_policy->save();
                }
            }
        }
        $delete_utilities = $this->modelUtility::query()->where('hotel_id', $id)->delete();
        // dd($request->utilities);
        if(!empty($request->utilities)){
            foreach($request->utilities as $item){
                if(!empty($item['title'])){
                    $new_utilities = new $this->modelUtility;
                    $new_utilities->title = $item['title'];
                    $new_utilities->hotel_id = $update_hotel->id;
                    $new_utilities->save();
                }
            }
        }
        $delete_extra_price = $this->modelPrice::query()->where('hotel_id', $id)->delete();
        if (!empty($request->extra_price)){
            foreach ($request->extra_price as $values){
                if (!empty($values['title']) && !empty($values['price'])) {
                    $new_price = new $this->modelPrice;
                    $new_price->title = $values['title'];
                    $new_price->price = $values['price'];
                    $new_price->type = $values['type'];
                    $new_price->hotel_id = $update_hotel->id;
                    $new_price->save();
                }
            }
        }


        if(!empty($request->property_term)){
            $update_hotel->property()->sync($request->property_term);
        }else{
            $update_hotel->property()->detach();
        }
        if(!empty($request->facility_term)){
            $update_hotel->facility()->sync($request->facility_term);
        }else{
            $update_hotel->facility()->detach();
        }
        if(!empty($request->service_term)){
            $update_hotel->service()->sync($request->service_term);
        }else{
            $update_hotel->service()->detach();
        }
        return redirect()->route('hotel.list')->with('success', 'Update thành công');
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {
        $delete_hotel = $this->modelHotel->findorFail($id);
        $delete_hotel->property()->detach();
        $delete_hotel->facility()->detach();
        $delete_hotel->service()->detach();
        $delete_hotel->delete();
        return redirect()->route('hotel.list')->with('success', 'xóa thành công');
    }
    public function changeStatus($id)
    {
        $hotel = $this->modelHotel->findorFail($id);
        if ($hotel->status ==1) {
            $hotel->status = 0;
            $hotel->save();
            return response()->json([
                'type' => 1,
                'mess' => 'Chuyển thành công'
            ]);
        }else{
            $hotel->status =1;
            $hotel->save();
            return response()->json([
                'type' => 2,
                'mess' => 'Chuyển thành công'
            ]);
        }
    }
    public function term()
    {
        return view('category::terms.term-index');
    }
}
