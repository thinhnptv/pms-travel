<?php

namespace Modules\Hotel\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Category\Entities\RoomAttribute;
use Modules\Category\Entities\Roomterm;
use Modules\Hotel\Entities\Hotel;
use Modules\Hotel\Entities\Room;
use Modules\Hotel\Http\Requests\CreateRoomRequest;

class RoomsController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    protected $modelRoom;
    protected $modelRoomTerm;
    protected $modelRoomAttribute;
    protected $modelHotel;

    public function __construct(Hotel $modelHotel, Room $modelRoom, Roomterm $modelRoomTerm, RoomAttribute $modelRoomAttribute)
    {
        $this->modelHotel = $modelHotel;
        $this->modelRoom = $modelRoom;
        $this->modelRoomAttribute = $modelRoomAttribute;
        $this->modelRoomTerm = $modelRoomTerm;
    }
    public function index($id = "", Request $request)
    {
        $list_amenities = $this->modelRoomTerm::query()->where('attribute_id', 2)->get();
        $all_room = $this->modelRoom::query()->where('hotel_id', $id)->paginate(15);
        return view('hotel::rooms.list-manage-rooms', compact('all_room', 'id', 'list_amenities'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store($id, CreateRoomRequest $request)
    {
        $str_feature = '';
        $str_album = '';

        if($request->input('album')){
            $str_request_album = json_decode($request->album, true);
            $str_album = "[\"";
            foreach($str_request_album as $item) {
                $file_url = parse_url($item);
                $url = public_path($file_url['path']);
                
                $str_album .= $file_url['path'] . '","';
            }
            $str_album = substr($str_album, 0, -2);
            $str_album .= "]";
        }
        if ($request->input('feature')) {
            $file_url = parse_url($request->input('feature'));
            $url = public_path($file_url['path']);
            
            $str_feature = $file_url['path'];
        }
        // dd($request->all());
        $add_room = new $this->modelRoom;
        $add_room->name = $request->name;
        $add_room->hotel_id = $id;
        $add_room->feature = $str_feature;
        $add_room->album = $str_album;
        $add_room->price = $request->price;
        $add_room->number_of_room = $request->number_of_room;
        $add_room->number_of_beds = $request->number_of_beds;
        $add_room->number_of_bed = $request->number_of_bed;
        $add_room->room_size = $request->room_size;
        $add_room->max_adults = $request->max_adults;
        $add_room->max_children = $request->max_children;
        $add_room->import_url = $request->import_url;
        $add_room->status = $request->status;
        $add_room->save();

        if(!empty($request->amenities_room_term)){
            $add_room->amenities()->attach($request->amenities_room_term);
        }
        
        return redirect()->route('room.list', $id)->with('success', 'Thêm mới thành công');
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('hotel::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($idks, $idr)
    {
        $list_amenities = $this->modelRoomTerm::query()->where('attribute_id', 2)->get();
        $room = $this->modelRoom::query()->findOrFail($idr);
        $arr_amenities_selected = [];
        foreach($room->amenities as $item) {
            $arr_amenities_selected[] = $item->id;
        }
        return view('hotel::rooms.edit', compact('room', 'list_amenities', 'arr_amenities_selected', 'idks'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update($idks, $idr,  CreateRoomRequest $request)
    {
        $str_feature = '';
        $str_album = '';
        if($request->input('album')){
            $str_request_album = json_decode($request->album, true);
            $str_album = "[\"";
            foreach($str_request_album as $item) {
                $file_url = parse_url($item);
                $url = public_path($file_url['path']);
                
                $str_album .= $file_url['path'] . '","';
            }
            $str_album = substr($str_album, 0, -2);
            $str_album .= "]";
        }
        if ($request->input('feature')) {
            $file_url = parse_url($request->input('feature'));
            $url = public_path($file_url['path']);
            
            $str_feature = $file_url['path'];
        }
        // dd($request->all());
        $update_room =  $this->modelRoom->findOrFail($idr);
        $update_room->name = $request->name;
        $update_room->feature = $str_feature;
        $update_room->album = $str_album;
        $update_room->price = $request->price;
        $update_room->number_of_room = $request->number_of_room;
        $update_room->number_of_beds = $request->number_of_beds;
        $update_room->number_of_bed = $request->number_of_bed;
        $update_room->room_size = $request->room_size;
        $update_room->max_adults = $request->max_adults;
        $update_room->max_children = $request->max_children;
        $update_room->import_url = $request->import_url;
        $update_room->status = $request->status;
        $update_room->save();

        if(!empty($request->amenities_room_term)){
            $update_room->amenities()->sync($request->amenities_room_term);
        }
        
        return redirect()->route('room.list', $idks)->with('success', 'Cập nhật phòng thành công');
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
    public function changeStatus(Request $request)
    {
        $list_id = $request->list_id;
        $selected_action = $request->selected_action;
        foreach($list_id as $item){
            $list_room_id = $this->modelRoom->findOrFail($item);
            $list_room_id->status = $selected_action;
            $list_room_id->save();
        }
        switch ($selected_action) {
            case 1:
                return response()->json([
                    'type' => 1,
                    'mess' => "Chuyển thành công",
                ]);
                break;
            case 2:
                return response()->json([
                    'type' => 2,
                    'mess' => "Chuyển thành công",
                ]);
                break;
            case 3:
                return response()->json([
                    'type' => 3,
                    'mess' => "Chuyển thành công",
                ]);
                break;
        }
    }
    public function delete(Request $request)
    {
        $list_id = $request->list_id;
        $list_id = $this->modelRoom->whereIn('id', $list_id)->delete();
        return response()->json([
            'type' => 1,
            'mess' => "Xóa thành công!"
        ]);
    } 
}
