<?php

namespace Modules\Hotel\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Hotel\Entities\ReviewHotel;
use Modules\User\Entities\User;

class ReviewsController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    protected $modelUser;
    protected $modelReview;

    public function __construct(User $modelUser, ReviewHotel $modelReview)
    {
        $this->modelUser = $modelUser;
        $this->modelReview = $modelReview;
    }

    public function index(Request $request)
    {
        $list_reviews = $this->modelReview::query()->with('user');
        if(!empty($request->user_id)){
            $name = $request->user_id;
            $list_reviews->where('user_id', 'like', '%' .$name. '%')
            ->orWhere('hotel_id', $name);
        }
        $list_reviews = $list_reviews->paginate(15);
        return view('hotel::reviews.list', compact('list_reviews'));
    }
    public function listReviewForHotel($id)
    {
        $list_reviews = $this->modelReview::query()->where('hotel_id', $id)->with('user')->paginate(15);
        return view('hotel::reviews.list', compact('list_reviews'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('hotel::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('hotel::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('hotel::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
    public function changeStatus(Request $request)
    {
        $list_id = $request->list_id;
        $selected_action = $request->selected_action;
        foreach($list_id as $item){
            $list_reviews_id = $this->modelReview->findOrFail($item);
            $list_reviews_id->status = $selected_action;
            $list_reviews_id->save();
        }
        switch ($selected_action) {
            case 1:
                return response()->json([
                    'type' => 1,
                    'mess' => "Chuyển thành công",
                ]);
                break;
            case 2:
                return response()->json([
                    'type' => 2,
                    'mess' => "Chuyển thành công",
                ]);
                break;
            case 3:
                return response()->json([
                    'type' => 3,
                    'mess' => "Chuyển thành công",
                ]);
                break;
            case 4:
                return response()->json([
                    'type' => 4,
                    'mess' => "Chuyển thành công",
                ]);
                break;
        }
    }
    public function delete(Request $request)
    {
        $list_id = $request->list_id;
        $list_id = $this->modelReview->whereIn('id', $list_id)->delete();
        return response()->json([
            'type' => 1,
            'mess' => "Xóa thành công!"
        ]);
    } 
}
