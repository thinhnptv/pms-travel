<?php

namespace Modules\Hotel\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Hotel\Entities\HotelType;
use Modules\Hotel\Http\Requests\CreateTypeRequest;

class HotelTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    protected $modelTypeHotel;

    public function __construct(HotelType $modelTypeHotel)
    {
        $this->modelTypeHotel = $modelTypeHotel;
    }
    public function index(Request $request)
    {
        $listType = $this->modelTypeHotel::query();
        if(!empty($request->name)){
            $name = $request->name;
            $listType->where('name', 'like', '%' .$name. '%');
        }
        $listType = $listType->paginate(15);
        return view('hotel::type.list', compact('listType'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('hotel::type.add');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(CreateTypeRequest $request)
    {
        $str_avatar = '';
        if ($request->input('avatar')) {
            $file_url = parse_url($request->input('avatar'));
            $str_avatar = $file_url['path'];
        }
        $addtypeHotel = $this->modelTypeHotel;
        $addtypeHotel->name = $request->name;
        $addtypeHotel->status = $request->status;
        $addtypeHotel->avatar = $str_avatar;
        $addtypeHotel->save();
        return redirect()->route('type')->with('success', 'Thêm mới thành công');
        
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('hotel::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {
        $editTypeHotel = $this->modelTypeHotel::query()->findOrFail($id);
        return view('hotel::type.edit', compact('editTypeHotel'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update($id, Request $request)
    {
        $str_avatar = '';
        if ($request->input('avatar')) {
            $file_url = parse_url($request->input('avatar'));
            $str_avatar = $file_url['path'];
        }
        $updatetypeHotel = $this->modelTypeHotel->findorFail($id);
        $updatetypeHotel->name = $request->name;
        $updatetypeHotel->status = $request->status;
        $updatetypeHotel->avatar = $str_avatar;
        $updatetypeHotel->save();
        return redirect()->route('type')->with('success', 'Cập nhật thành công thành công');
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {
        $deleteTypeHotel = $this->modelTypeHotel::query()->findOrFail($id);
        $deleteTypeHotel->delete();
        return redirect()->route('type')->with('success', 'Xóa thành công thành công');
    }
    public function changeStatus($id)
    {
        $changeStatus = $this->modelTypeHotel::query()->findOrFail($id);
        if($changeStatus->status == 1) {
            $changeStatus->status = 0;
            $changeStatus->save();
            return response()->json([
                'type' => 1,
                'mess' => 'Chuyển thành công',
            ]);
        }else{
            $changeStatus->status = 1;
            $changeStatus->save();
            return response()->json([
                'type' => 2,
                'mess' => 'Chuyển thành công',
            ]);
        }
    }
}
