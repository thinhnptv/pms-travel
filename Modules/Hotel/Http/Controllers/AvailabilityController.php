<?php

namespace Modules\Hotel\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Hotel\Entities\RoomAvavailability;
use Modules\Hotel\Entities\Hotel;
use Modules\Hotel\Entities\Room;
use Carbon\Carbon;

class AvailabilityController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    protected $modelAvailability;
    protected $modelRoomAvavailability;
    protected $modelRoom;
    protected $modelHotel;

    public function __construct(RoomAvavailability $modelRoomAvavailability, Room $modelRoom, Hotel $modelHotel)
    {
        $this->modelRoomAvavailability = $modelRoomAvavailability;
        $this->modelRoom = $modelRoom;
        $this->modelHotel = $modelHotel;
    }
    public function index($id, Request $request)
    {
        // dd($this->modelRoom::query()->where('hotel_id', $id)->get());
        $room = $this->modelRoom::query()->findOrFail($id);
        return view('hotel::rooms.update-rooms-availability', compact(
            'room', 'id'
        ));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function get($id, Request $request)
    {
        // dd(1);
        $room = $this->modelRoom::query()->findOrFail($id);
        $start = Carbon::createFromFormat('Y-m-d', $request->start);
        $end = $start->copy()->addDays(41);

        $numberDay = $end->diffInDays($start);
        $arr = [];
        $arr[] = $start->format('Y-m-d');
        for ($i = 1; $i <= $numberDay ; $i++) {
            $day    = $start->addDays($i);
            $arr[] = $day->format('Y-m-d');
            $start->subDays($i);
        }

        $roomAvavailability = $this->modelRoomAvavailability->where('room_id', $id)->get();
        $arrDate = [];
        foreach ($roomAvavailability as $item) {
            $arrDate[] = $item->date;
        }
        
        $arrResult = array_map(function($date) use ($roomAvavailability, $arrDate, $room) {  
            if(in_array($date, $arrDate)) {
                foreach ($roomAvavailability as $items) {
                    if($date == $items->date) {
                        if($items->remaining == 0) {
                            $event = 'Full book';
                            $className = 'bg-danger';
                        }else{
                            $event = $items->remaining . " x " . $items->price;
                            $className = 'bg-success';
                        }
                        $remaining = $items->remaining;
                        $price = $items->price;
                        $roomId = $items->room_id;
                    }
                }
                $result = [
                    'start' => $date,
                    'roomId' => $roomId,
                    'remaining' => $remaining,
                    'price' => $price,
                    'className' => $className,
                    'title' => $event       
                ]; 
                
            }else{
                $event = $room->number_of_room . " x " . $room->price;
                $remaining = $room->number_of_room;
                $price = $room->price;
                $roomId = $room->id;
                $result = [
                    'start' => $date,
                    'roomId' => $roomId,
                    'remaining' => $remaining,
                    'price' => $price,
                    'className' => 'bg-warning',
                    'title' => $event
                ]; 
                
            }
            return $result;
        }, $arr);
        return response()->json($arrResult);
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function save(Request $request)
    {
        // dd($request->all());
        if($request->remaining <= 0 ){
            $remaining = 0;
        }else {
            $remaining = $request->remaining;
        }
        if($request->price <= 0 ){
            $price = 0;
        }else {
            $price = $request->price;
        }
        $roomAvailability = $this->modelRoomAvavailability->where('date', $request->day)->where('room_id', $request->roomId)->first();
        if($roomAvailability != null) {
            $availability = $this->modelRoomAvavailability->findOrFail($roomAvailability->id);
            $availability->remaining = $remaining;
            $availability->price = $price;
            $availability->save();
            
        }else {
            $availability = new $this->modelRoomAvavailability;
            $availability->remaining = $remaining;
            $availability->room_id = $request->roomId;
            $availability->date = $request->day;
            $availability->price = $price;
            $availability->save();
        }
        return Response()->json([
            'type' => 1,
            'mess' => 'Cập nhật thành công',
        ]);
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('hotel::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('hotel::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
}
