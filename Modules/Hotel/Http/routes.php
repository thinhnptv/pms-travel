<?php

Route::group(['middleware' => ['web', 'adminLogin'], 'prefix' => 'admin/hotel', 'namespace' => 'Modules\Hotel\Http\Controllers'], function()
{
    Route::get('/', 'HotelController@index')->name('hotel.list');
    Route::post('/change-status/{id}', 'HotelController@changeStatus')->name('hotel.change_status');
    Route::get('/delete/{id}', 'HotelController@destroy')->name('hotel.delete');
    Route::get('/add', 'HotelController@create')->name('hotel.get_add');
    Route::post('/add', 'HotelController@store')->name('hotel.post_add');
    Route::get('/term', 'HotelController@term')->name('hotel.manage-term');
    Route::get('/edit/{id}', 'HotelController@edit')->name('hotel.edit');
    Route::post('/edit/{id}', 'HotelController@update')->name('hotel.update'); 

    Route::get('/all-reviews', 'ReviewsController@index')->name('review.list');
    Route::get('/list-reviews-for-hotel/{id}', 'ReviewsController@listReviewForHotel')->name('review.list_reviews_for_hotel');
    Route::post('/delete-review', 'ReviewsController@delete')->name('review.delete');
    Route::post('/change-status-review', 'ReviewsController@changeStatus')->name('review.change_status');

    Route::get('/manage-rooms/{id}', 'RoomsController@index')->name('room.list');
    Route::post('/add-rooms/{id}', 'RoomsController@store')->name('room.post_add');
    Route::post('/change-status-room', 'RoomsController@changeStatus')->name('room.change_status');
    Route::post('/delete-room', 'RoomsController@delete')->name('room.delete');
    Route::get('/edit-room/{idks}/{idr}', 'RoomsController@edit')->name('room.edit');
    Route::post('/edit-room/{idks}/{idr}', 'RoomsController@update')->name('room.update');
    
    Route::get('/all-booking-rooms', 'ReportsController@index')->name('room.list_booking_rooms');
    Route::post('/change-status-booking-room', 'ReportsController@changeStatus')->name('room.booking_room_status_change');
    Route::post('/delete-booking-all', 'ReportsController@deleteAll')->name('room.booking_room_delete');

    Route::get('/manage-rooms-availability/{id}', 'AvailabilityController@index')->name('room.availability');
    Route::get('/get-rooms-availability/{id}', 'AvailabilityController@get')->name('room.availability.get');
    Route::post('/save-rooms-availability', 'AvailabilityController@save')->name('room.availability.save');

    Route::get('/all-type-of-hotel', 'HotelTypeController@index')->name('type');
    Route::get('/add-type-hotel', 'HotelTypeController@create')->name('type.get_add');
    Route::post('/add-type-hotel', 'HotelTypeController@store')->name('type.post_add');
    Route::get('/edit-type-hotel/{id}', 'HotelTypeController@edit')->name('type.edit');
    Route::post('/edit-type-hotel/{id}', 'HotelTypeController@update')->name('type.update');
    Route::get('/delete-type-hotel/{id}', 'HotelTypeController@destroy')->name('type.delete');
    Route::post('/change-status-type-hotel/{id}', 'HotelTypeController@changeStatus')->name('type.status');

    Route::get('/utility-hotel', 'UtilityController@index')->name('utility');
    Route::get('/add-utility', 'UtilityController@create')->name('utility.get_add');
    Route::post('/add-utility', 'UtilityController@store')->name('utility.post_add');
    Route::get('/edit-utility/{id}', 'UtilityController@edit')->name('utility.edit');
    Route::post('/edit-utility/{id}', 'UtilityController@update')->name('utility.update');
    Route::get('/delete-utility/{id}', 'UtilityController@destroy')->name('utility.delete');




});
