<?php

namespace Modules\Hotel\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Request;

class CreateRoomRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        // dd($request->utilities);
        return [
            'name'=>'required|min:2',
            'price'=>'required|numeric|min:1',
        ];
    }
    public function messages () 
    {
        return [
            'name.required' => "Tên phòng không được để trống!",
            'name.min' => "Tên phòng không được ít hơn 2 ký tự!",
            'price.required' => "Giá phòng không được để trống",
            'price.numeric' => "Giá phòng phải là số",
            'price.min' => "Giá phòng phải lớn hơn 0",
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
