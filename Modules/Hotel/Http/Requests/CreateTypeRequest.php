<?php

namespace Modules\Hotel\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Request;

class CreateTypeRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        // dd($request->utilities);
        return [
            'name'=>'required|unique:hotel_types,name',
        ];
    }
    public function messages () 
    {
        return [
            'name.unique' => "Tên khách sạn không được trùng!",
            'name.required' => "Tên khách sạn là bắt buộc!",
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
