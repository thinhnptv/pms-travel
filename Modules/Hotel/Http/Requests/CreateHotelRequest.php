<?php

namespace Modules\Hotel\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Request;

class CreateHotelRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // dd($request->utilities);
        return [
            'name'=>'required|min:3',
            'price'=>'required|numeric|min:1',
            'feature'=>'required',
            'album'=>'required',
            'rating_star'=>'required',
            'type_id'=>'required',
            'latitu' => 'required',
            'longtitu' => 'required',
            'hotel_sale'=>'nullable|numeric|max:'.(int)request()->price,
        ];
    }
    public function messages () 
    {
        return [
            'name.required' => "Tên khách sạn không được để trống!",
            'name.min' => "Tên khách sạn không được ít hơn 3 ký tự!",
            'price.required' => "Giá khách sạn không được để trống",
            'price.numeric' => "Giá khách sạn phải là số",
            'price.min' => "Giá khách sạn phải lớn hơn 0",
            'hotel_sale.required' => "Giá sale khách sạn không được để trống",
            'hotel_sale.numeric' => "Giá sale khách sạn phải là số",
            'hotel_sale.min' => "Giá sale khách sạn phải lớn hơn 0",
            'feature.required' => "Ảnh đại diện khách sạn không được để trống!",
            'album.required' => "Album khách sạn không được để trống!",
            'rating_star.required'=>'Số sao khách sạn không được để trống',
            'type_id.required'=>'Loại hình khách sạn không được để trống',
            'latitu.required'=>'Vĩ độ khách sạn không được để trống',
            'longtitu.required'=>'Kinh độ khách sạn không được để trống',
            'hotel_sale.max'=>'Giá sale không được lớn hơn giá gốc',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
