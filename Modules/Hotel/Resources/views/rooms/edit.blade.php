@extends('base::layouts.master')
@section('css')
        <!-- Plugins css-->
        <link href="{{ URL::asset('assets/libs/select2/select2.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ URL::asset('assets/libs/summernote/summernote.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ URL::asset('assets/libs/dropzone/dropzone.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ URL::asset('assets/css/webt.css')}}" rel="stylesheet" type="text/css" />
@endsection
@section('content')
<div class="container-fluid">

    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="javascript: void(0);">UBold</a></li>
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Apps</a></li>
                        <li class="breadcrumb-item active"> Edit: {{ $room->name }}</li>
                    </ol>
                </div>
                <h4 class="page-title">Edit: {{ $room->name }}</h4>
            </div>
        </div>
    </div>
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @endif
    @if (session('errorsss'))
            <div class="alert alert-danger">
                {{ session('errorsss') }}
            </div>
        @endif
    <!-- end page title -->
    <form action="{{ route('room.update', [$idks, $room->id]) }}" method="POST" enctype="multipart/form-data">
        <input type="hidden" name="_token" value="{{csrf_token()}}" /> 
        <div class="row">
            <div class="col-md-12 col-lg-9">
                <div class="card p-2">
                    <div class="row">
                        <div class="col-md-12 col-lg-12">
                            <h4>Room information</h4>
                            <hr>
                            <div class="form-group mb-3">
                                <label for="name">Room Name</label>
                                <span class="text-danger">*</span>
                                <input type="text" id="name" name="name" class="form-control" placeholder="Room name" value="{{ $room->name }}">
                            </div> 
                            <div class="form-group mb-3">
                                <label for="" >Feature Image</label>
                                <div class="card-box text-center">
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-warning" data-toggle="modal" data-target=".bd-example-modal-image-feature">Select Featured Image</button>
                                        @if (!empty($room->feature))
                                            <input type="hidden" name="feature" id="feature"  value="{{ config('app.url') }}{{ $room->feature }}">
                                        @else
                                            <input type="hidden" name="feature" id="feature"  value="">
                                        @endif
                                    </span>
                                    <div id="preview_feature" class="mt-3">
                                        @if (!empty($room->feature))
                                            <div class="box_imgg position-relative">
                                                <img src="{{ config('app.url') }}{{ $room->feature }}" id='show-img-hotel' style="width:100%;">
                                                <i class="mdi mdi-close-circle-outline style_icon_remove style_icon_remove_feature" title="delete"></i>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="form-group-image mb-3">
                                @php
                                $str_album = '';
                                @endphp
                                @if (!empty($room->album))
                                @php
                                    $array_album = json_decode($room->album, true);
                                    $str_album_array = [];
                                @endphp
                                @foreach ($array_album as $item)
                                @php
                                    $str_album_array[] = config('app.url').$item;
                                @endphp
                                @endforeach
                                @php
                                    $str_album .= "[\"";
                                    $str_album .= implode("\",\"", $str_album_array);
                                    $str_album .= "\"]";
                                @endphp
                                @endif
                                <label for="" >Gallery</label>
                                <div class="card-box text-center">
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-warning" data-toggle="modal" data-target=".bd-example-modal-image-album">Select Album Image</button>
                                        <input type="hidden" name="album" id="album"  value="{{  $str_album }}">
                                    </span>
                                    <div id="preview_album" class="mt-3">
                                        <div class="row row_preview_album" id="row_preview_album">
                                            @if (!empty($room->album))
                                                @foreach (json_decode($room->album) as $item)
                                                    <div class="col-3 mt-3">
                                                        <div class="box_imgg position-relative">
                                                            <img src="{{ config('app.url') }}{{ $item }}" class="img-height-110" style="width:100%; height=110px;">
                                                            <i class="mdi mdi-close-circle-outline style_icon_remove style_icons_remove_album" title="delete"></i>
                                                        </div>
                                                    </div>
                                                @endforeach
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group mb-3">
                                        <label for="price">Price</label>
                                        <span class="text-danger">*</span>
                                        <input type="number" id="price" name="price" class="form-control" placeholder="price" value="{{ $room->price }}">
                                    </div>
                                    <div class="form-group mb-3">
                                        <label for="number_of_beds">Number of beds</label>
                                        <input type="number" id="number_of_beds" name="number_of_beds" class="form-control" placeholder="1" value="{{ $room->number_of_beds }}">
                                    </div>
                                    <div class="form-group mb-3">
                                        <label for="number_of_bed">Number of bed</label>
                                        <input type="number" id="number_of_bed" name="number_of_bed" class="form-control" placeholder="1" value="{{ $room->number_of_bed }}">
                                    </div>
                                    <div class="form-group mb-3">
                                        <label for="max_adults">Max Adults</label>
                                        <input type="number" id="max_adults" name="max_adults" class="form-control" placeholder="1" value="{{ $room->max_adults }}">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group mb-3">
                                        <label for="number_of_room">Number of room</label>
                                        <input type="number" id="number_of_room" name="number_of_room" class="form-control" placeholder="1" value="{{ $room->number_of_room }}">
                                    </div>
                                    <div class="form-group mb-3">
                                        <label for="room_size">Room Size</label>
                                        <input type="text" id="room_size" name="room_size" class="form-control" placeholder="sqft" value="{{ $room->room_size }}">
                                    </div>
                                    <div class="form-group mb-3">
                                        <label for="max_children">Max Children</label>
                                        <input type="number" id="max_children" name="max_children" class="form-control" placeholder="0" value="{{ $room->max_children }}">
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <h4>Attribute: Room Amenities</h4>
                            <div class="form-group mb-3">
                                <div class="terms-scrollable">
                                    @foreach ($list_amenities as $item)
                                        @if (in_array($item->id, $arr_amenities_selected))
                                            <label class="term-item">
                                                <input type="checkbox" name="amenities_room_term[]" value="{{ $item->id }}" checked>
                                                <span class="term-name">{{ $item->name }}</span>
                                            </label>
                                        @else
                                            <label class="term-item">
                                                <input type="checkbox" name="amenities_room_term[]" value="{{ $item->id }}">
                                                <span class="term-name">{{ $item->name }}</span>
                                            </label>
                                        @endif
                                    @endforeach
                                </div>
                            </div> 
                            <hr>
                            <div class="form-group mb-3">
                                <label for="import_url">Import url</label>
                                <input type="text" id="import_url" name="import_url" class="form-control" placeholder="" {{ $room->import_url }}>
                            </div>
                            <div class="form-group mb-3">
                                <h4>status</h4>
                                <div class="form-group mb-3">
                                    <select name="status" id="" class="form-control">
                                        @if ($room->status == 1)
                                            <option value="1">Publish</option>
                                            <option value="2">Pending</option>
                                            <option value="3">Draft</option>
                                        @elseif($room->status == 2)
                                            <option value="2">Pending</option>
                                            <option value="1">Publish</option>
                                            <option value="3">Draft</option>
                                        @elseif($room->status ==3)
                                            <option value="3">Draft</option>
                                            <option value="2">Pending</option>
                                            <option value="1">Publish</option>
                                        @endif
                                    </select>
                                </div> 
                            </div>
                        </div>
                    </div> 
                    <div class="form-group mb-3 text-right">
                        <button type="submit" class="btn w-sm btn-success waves-effect waves-light">Save Changes</button>
                    </div>     
                </div>
            </div>
        </form>
           
        </div>
        
    <!-- end row -->
    
</div>



@section('js')
    <div class="modal fade bd-example-modal-image-album" id="modal-abbum" tabindex="-1" role="dialog" data-backdrop="false" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Manger Image</h5>
                    <button type="button" class="close" data-dismiss="modal"><i class="fas fa-times"></i></button>
                </div>
                <div class="modal-body">
                    <iframe src="{{ config('app.url') }}/filemanager/dialog.php?akey=4Ans6pfTs6A9DzHj8AZq1NVGKoYxpyoI8ivCN1bOow&field_id=album&multiple=1" frameborder="0" style="width: 100%; height: 500px; overflow-y: auto"></iframe>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade bd-example-modal-image-feature" id="modal-abbum" tabindex="-1" role="dialog" data-backdrop="false" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Manger Image</h5>
                    <button type="button" class="close" data-dismiss="modal"><i class="fas fa-times"></i></button>
                </div>
                <div class="modal-body">
                    <iframe src="{{ config('app.url') }}/filemanager/dialog.php?akey=4Ans6pfTs6A9DzHj8AZq1NVGKoYxpyoI8ivCN1bOow&field_id=feature&multiple=0" frameborder="0" style="width: 100%; height: 500px; overflow-y: auto"></iframe>
                </div>
            </div>
        </div>
    </div>
@stop
@endsection

@push('js')
    <!-- Summernote js -->
    <script src="{{ URL::asset('assets/libs/summernote/summernote.min.js') }}"></script>
    <!-- Select2 js-->
    <script src="{{ URL::asset('assets/libs/select2/select2.min.js') }}"></script>
    <!-- Dropzone file uploads-->
    <script src="{{ URL::asset('assets/libs/dropzone/dropzone.min.js') }}"></script>
    <!-- Init js -->
    <script>
        jQuery(document).ready(function(){
        $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
        });
        // Summernote
    
        $('#content').summernote({
            height: 180,                 // set editor height
            minHeight: null,             // set minimum height of editor
            maxHeight: null,             // set maximum height of editor
            focus: false                 // set focus to editable area after initializing summernote
        });
        
        // Select2
        $('.select2').select2();
    
        
    });
         $(document).on('click','.check_master', function(e) {
            if($(this).is(':checked',true))
            {
                $(".sub-chk").prop('checked', true);
            } else {
                $(".sub-chk").prop('checked',false);
            }
        });
        //status
        $(document).on('click', '.btn-apply', function(e){
            const __this = this;
            $(__this).prop('disable', true);
            var list_id = [];
            $('.sub-chk:checked').each(function(){
                var sub_id = parseInt($(this).attr('data-id'));
                list_id.push(sub_id);
            });
            
            var selected_action = parseInt($('.actions').val());
            if (list_id.length == 0) {
                console.log(1);
                toastr.error("Vui lòng chọn hàng cần xử lý");
                $(__this).prop('disable', false);
            } else {
                if (selected_action == 4) {
                    var check_sure = confirm("Bạn có chắc chắn muốn xóa không?");
                    if (check_sure == true) {
                        var __token = $('meta[name="csrf-token"]').attr('content');
                        data_ = {
                            list_id: list_id,
                            _token: __token
                        }
                        var request = $.ajax({
                            url: '{{ route('room.delete') }}',
                            type: 'POST',
                            data: data_,
                            dataType: "json"
                        });
                        request.done(function(msg){
                            if(msg.type == 1){
                                $('.alert-success').remove();
                                $('.sub-chk:checked').each(function(){
                                    $(this).parents("tr").remove();
                                });
                                toastr.success(msg.mess);
                            }
                            return false;
                        });
                        request.fail(function(jqXHR, textStatus){
                            alert("Không thể gửi yêu cầu mã lỗi: "+textStatus);
                        });
                    }
                    $(this).prop('disable', false);
                }else if(selected_action != 0){
                    var __token = $('meta[name="csrf-token"]').attr('content');
                    data__ = {
                        list_id: list_id,
                        _token: __token,
                        selected_action: selected_action
                    }
                    var request = $.ajax({
                        url: '{{ route('room.change_status') }}',
                        type: 'POST',
                        data: data__,
                        dataType: "json"
                    });
                    request.done(function(msg){
                        switch (msg.type) {
                            case 1:{
                                $('.alert-success').remove();
                                $('.sub-chk:checked').each(function(){
                                    $(this).parents("tr").children('td').children('.status_room').removeClass('btn-primary');
                                    $(this).parents("tr").children('td').children('.status_room').removeClass('btn-danger');
                                    $(this).parents("tr").children('td').children('.status_room').removeClass('btn-secondary');
                                    $(this).parents("tr").children('td').children('.status_room').addClass('btn-primary');
                                    $(this).parents("tr").children('td').children('.status_room').html('Publish');
                                });
                                toastr.success(msg.mess);
                                break;
                            }
                            case 2:{
                                $('.alert-success').remove();
                                $('.sub-chk:checked').each(function(){
                                    $(this).parents("tr").children('td').children('.status_room').removeClass('btn-primary');
                                    $(this).parents("tr").children('td').children('.status_room').removeClass('btn-danger');
                                    $(this).parents("tr").children('td').children('.status_room').removeClass('btn-secondary');
                                    $(this).parents("tr").children('td').children('.status_room').addClass('btn-danger');
                                    $(this).parents("tr").children('td').children('.status_room').html('Draft');
                                });
                                toastr.success(msg.mess);
                                break;
                            }
                            case 3:{
                                $('.alert-success').remove();
                                $('.sub-chk:checked').each(function(){
                                    $(this).parents("tr").children('td').children('.status_room').removeClass('btn-primary');
                                    $(this).parents("tr").children('td').children('.status_room').removeClass('btn-danger');
                                    $(this).parents("tr").children('td').children('.status_room').removeClass('btn-secondary');
                                    $(this).parents("tr").children('td').children('.status_room').addClass('btn-secondary');
                                    $(this).parents("tr").children('td').children('.status_room').html(' Pending');
                                });
                                toastr.success(msg.mess);
                                break;
                            }
                        }
                        return false;
                    });
                    request.fail(function (jqXHR, textStatus) {
                        alert("Không thể gửi yêu cầu mã lỗi : " + textStatus);
                    });
                }
            }
        });
        $(document).on('click','.page-link',function(e) {
            e.preventDefault();
            url_ = this.href;
            var url_string = location.href;
            var url = new URL(url_string);
            var role = url.searchParams.get("role");
            if(role != null){
                url_ = url_ + '&role='+role;
            }
            window.location.href=url_;
        })
// upload ảnh
        //album
        $(document).on('hide.bs.modal', '.bd-example-modal-image-album', function () {
            var _img = $('input#album').val();
            if (!_img.length) {
                $('#preview_album').empty();
            } else {
                if(_img[0] == '[') {
                    var array = JSON.parse(_img);
                    $('#row_preview_album').empty();
                    var html = '';
                    $.each(array, function( index, value ) {
                        html += `
                            <div class="col-3 mt-3">
                                <div class="box_imgg position-relative">
                                    <img src="${value}" class="img-height-110" style="width:100%; height=110px;">
                                    <i class="mdi mdi-close-circle-outline style_icon_remove style_icons_remove_album" title="delete"></i>
                                </div>
                            </div>
                        `;
                    });
                    $('#row_preview_album').append(html);

                }else{
                    
                    $html = `
                        <div class="col-3 mt-3">
                            <div class="box_imgg position-relative">
                                <img src="" id='show-img-album' style="width:100%;">
                                <i class="mdi mdi-close-circle-outline style_icon_remove style_icons_remove_album" title="delete"></i>
                            </div>
                        </div>
                    `;
                    $('#row_preview_album').empty();
                    $('#row_preview_album').append($html);
                    $('#show-img-album').attr('src', _img);
                    var str_src = '';  
                    str_src += "[\"";
                    str_src += _img;
                    str_src += "\"]";
                    $('#album').val(str_src);
                }
                
            }
        });
      
        $(document).on('click', '.style_icons_remove_album', function() {
            $(this).parents('.col-3').hide('slow', function () { 
                $(this).remove(); 
                var arr_image = [];
                var str_src = '';               
                $('.row_preview_album').find('.col-3').each(function () {
                    var str_image = '';
                    str_image += '"';
                    str_image += $(this).find('.img-height-110').attr('src');
                    str_image += '"';
                    arr_image.push(str_image);
                });
                str_src += "[";
                str_src += arr_image.toString();
                str_src += "]";
                if(arr_image.length == 0){
                    $('#album').val('');
                }else{
                    $('#album').val(str_src);
                }
            });
            });
        //feature
        $(document).on('hide.bs.modal', '.bd-example-modal-image-feature', function () {
            var _img = $('input#feature').val();
            if (!_img.length) {
                $('#preview_feature').empty();
            } else {
                $('#preview_feature').empty();
                $html = `
                    <div class="box_imgg position-relative">
                        <img src="" id='show-img-hotel' style="width:100%;">
                        <i class="mdi mdi-close-circle-outline style_icon_remove style_icon_remove_feature" title="delete"></i>
                    </div>
                `;
                $('#preview_feature').append($html);
                $('#show-img-hotel').attr('src', _img);
            }
        });
        $(document).on('click', '.style_icon_remove_feature', function() {
            $(this).parent('.box_imgg').hide('slow', function () { 
                $(this).remove(); 
                $('#banner').val('');
            });
        });
        
    </script>
    <script>
        $( document ).ready(function() {
            var url_string = location.href;
            var url = new URL(url_string);
            var role = url.searchParams.get("role");
            if(role != null){
                $('.filter_role').val(role);
            }
             
         });
     </script>
     {{-- ........................ --}}


      <script>
    var bookingCore = {
        url: 'http://sandbox.bookingcore.org',
        map_provider: 'gmap',
        map_gmap_key: '',
        csrf: 'JMrM5NwcxQy6HWOmuo7LQ2kHPh7pGwfbOPpXxkue'
    };
    var i18n = {
        warning: "Warning",
        success: "Success",
        confirm_delete: "Do you want to delete?",
        confirm: "Confirm",
        cancel: "Cancel",
    };
    </script>
    <script src="https://maps.google.com/maps/api/js?key=AIzaSyDsucrEdmswqYrw0f6ej3bf4M4suDeRgNA"></script>
    <script src="{{ URL::asset('assets/libs/gmaps/gmaps.min.js') }}"></script>
    <script src="{{ URL::asset('assets/js/pages/google-maps.init.js') }}"></script>
    <script src="{{ URL::asset('assets/js/map-engine.js') }}"></script>
    <script>
    jQuery(function ($) {
        new BravoMapEngine('gmaps-basic', {
            fitBounds: true,
            center: [{{"51.505"}}, {{"-0.09"}}],
            zoom:{{"8"}},
            ready: function (engineMap) {
                console.log(1);

                engineMap.on('click', function (dataLatLng) {
                    engineMap.clearMarkers();
                    engineMap.addMarker(dataLatLng, {
                        icon_options: {}
                    });
                    $("input[name=latitu]").attr("value", dataLatLng[0]);
                    $("input[name=longtitu]").attr("value", dataLatLng[1]);
                });
                engineMap.on('zoom_changed', function (zoom) {
                    $("input[name=zoom]").attr("value", zoom);
                })
            }
        });
    })
    </script>

@endpush
   