@extends('base::layouts.master')
@section('css')
        <!-- Plugins css-->
        <link href="{{ URL::asset('assets/libs/select2/select2.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ URL::asset('assets/libs/summernote/summernote.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ URL::asset('assets/libs/dropzone/dropzone.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ URL::asset('assets/css/webt.css')}}" rel="stylesheet" type="text/css" />
@endsection
@section('content')
    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">UBold</a></li>
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Apps</a></li>
                            <li class="breadcrumb-item active">All Reviews</li>
                        </ol>
                    </div>
                    <h4 class="page-title">All Reviews</h4>
                </div>
            </div>
        </div>
        @if (session('success'))
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
        @endif
        <!-- end page title -->
    <div class="row">
        <div class="col-12">
            <div class="card p-2">
                <div class="row mb-2">
                    <div class="col-sm-2">
                        <select name="role" id="" class="form-control actions">
                            <option value="0">Bulk Actions</option>
                            <option value="1">Approved</option>
                            <option value="2">Pending</option>
                            <option value="3">Spam</option>
                            <option value="4">Trash</option>
                            <option value="5">Delete</option>
                        </select>
                    </div>
                    <div class="col-sm-2">
                        <a href="javascript:void(0)" class="btn btn-primary waves-effect waves-light mb-2 btn-apply" >Apply</a>
                    </div>
                    <div class="col-sm-8">
                        <div class="text-sm-right">
                         
                        </div>
                        <form action="{{ route('review.list') }}" method="GET">
                            <div class="form-inline text-sm-right" style="float: right">
                                <input type="text" class="form-control" id="user_id" name="user_id"  placeholder="search by title" value="{{ Request::get('user_id') }}">
                                <button type="submit" class="btn btn-info ">Search</button>
                            </div>
                        </form>
                        
                    </div><!-- end col-->
                </div>
                <div class="table-responsive">
                    <table class="table table-bordered  mb-0">
                        <thead>
                            <tr>
                                <th width="50px"><input type="checkbox" id="master" class="check_master"></th>
                                <th class="text-center">STT</th>
                                <th class="text-center">Author</th>
                                <th class="text-center">Review Content</th>
                                <th class="text-center">Average Raiting</th>
                                <th class="text-center">In Response To</th>
                                <th class="text-center">Status</th>
                                <th class="text-center">Submitted On</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                                $stt = 1;
                            @endphp
                            @foreach ($list_reviews as $item)
                            <tr>
                                    <td><input type="checkbox" class="sub-chk" data-id="{{ $item->id }}"></td>
                                    <th class="text-center" scope="row">{{ $stt }}</th>
                                @if (!empty($item->user->id))
                                    <td> {{ optional($item->user)->firstname }} {{ optional($item->user)->lastname }}</td>
                                @else
                                    <td>[Author Deleted]</td>
                                @endif
                                    <td><h5>{{ $item->title }}</h5>{{ $item->content }}</td>
                                    <td>
                                        <table class="table table-borderless">
                                            <thead class="thead-light">
                                                <tr>
                                                    <th class="text-center">Average</th>
                                                    <th class="text-center">
                                                        <ul class="review-star d-flex flex-row ml-2">
                                                            @for ($i = 0; $i < $item->average; $i++)
                                                                <li><i class="fa fa-star"></i></li>
                                                            @endfor
                                                            @for ($i = 0; $i < 5-$item->average; $i++)
                                                                <li><i class="far fa-star"></i></li>
                                                            @endfor
                                                        </ul>
                                                    </th>
                                                </tr>
                                            </thead> 
                                            <tr>
                                                <td class="text-center">Service</td>
                                                <td class="text-center">
                                                    <ul class="review-star d-flex flex-row ml-2">
                                                        @for ($i = 0; $i < $item->service; $i++)
                                                            <li><i class="fa fa-star"></i></li>
                                                        @endfor
                                                        @for ($i = 0; $i < 5-$item->service; $i++)
                                                            <li><i class="far fa-star"></i></li>
                                                        @endfor
                                                    </ul>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="text-center">Organization</td>
                                                <td class="text-center">
                                                    <ul class="review-star d-flex flex-row ml-2">
                                                        @for ($i = 0; $i < $item->organization; $i++)
                                                            <li><i class="fa fa-star"></i></li>
                                                        @endfor
                                                        @for ($i = 0; $i < 5-$item->organization; $i++)
                                                            <li><i class="far fa-star"></i></li>
                                                        @endfor
                                                    </ul>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="text-center">Friendliness</td>
                                                <td class="text-center">
                                                    <ul class="review-star d-flex flex-row ml-2">
                                                        @for ($i = 0; $i < $item->friendliness; $i++)
                                                            <li><i class="fa fa-star"></i></li>
                                                        @endfor
                                                        @for ($i = 0; $i < 5-$item->friendliness; $i++)
                                                            <li><i class="far fa-star"></i></li>
                                                        @endfor
                                                    </ul>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="text-center">Area Expert</td>
                                                <td class="text-center">
                                                    <ul class="review-star d-flex flex-row ml-2">
                                                        @for ($i = 0; $i < $item->area_expert; $i++)
                                                            <li><i class="fa fa-star"></i></li>
                                                        @endfor
                                                        @for ($i = 0; $i < 5-$item->area_expert; $i++)
                                                            <li><i class="far fa-star"></i></li>
                                                        @endfor
                                                    </ul>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="text-center">Safety</td>
                                                <td class="text-center">
                                                    <ul class="review-star d-flex flex-row ml-2">
                                                        @for ($i = 0; $i < $item->safety; $i++)
                                                            <li><i class="fa fa-star"></i></li>
                                                        @endfor
                                                        @for ($i = 0; $i < 5-$item->safety; $i++)
                                                            <li><i class="far fa-star"></i></li>
                                                        @endfor
                                                    </ul>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        <a href="">{{ $item->hotel->name }}
                                         <br> 
                                         <i class="fe-arrow-right"></i>View Hotel
                                        </a>
                                    </td>
                                    <td class="text-center">
                                        @switch($item->status)
                                            @case(1)
                                                <a href="" class="btn status-review btn-success">Approved</a>
                                                @break
                                            @case(2)
                                                <a href="" class="btn status-review btn-warning">Pending</a>
                                                @break
                                            @case(3)
                                                <a href="" class="btn status-review btn-danger">Spam</a>
                                                @break
                                            @default
                                                <a href="" class="btn status-review btn-secondary">Trash</a>
                                        @endswitch
                                    </td>
                                    <td>{{ $item->created_at }}</td>
                            </tr>
                            @php
                                $stt++;
                            @endphp
                            @endforeach
                        </tbody>
                    </table>
                    <div class="mt-2">
                        {{-- phân trang --}}
                        {{ $list_reviews->render() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('js')
    <!-- Summernote js -->
    <script src="{{ URL::asset('assets/libs/summernote/summernote.min.js') }}"></script>
    <!-- Select2 js-->
    <script src="{{ URL::asset('assets/libs/select2/select2.min.js') }}"></script>
    <!-- Dropzone file uploads-->
    <script src="{{ URL::asset('assets/libs/dropzone/dropzone.min.js') }}"></script>
    <!-- Init js -->
    <script src="{{ URL::asset('assets/js/webt.js') }}"></script>
    
    <script>
         $(document).on('click', '.check_master', function(e) {
            if($(this).is(':checked',true))
            {
                $(".sub-chk").prop('checked', true);
            } else {
                $(".sub-chk").prop('checked', false);
            }
        });
        $(document).on('click', '.delete_all',function(e) {
            const __this = this;
            $(__this).prop('disabled', true);
            var list_id = [];
            $('.sub-chk:checked').each(function () {
                var sub_id =parseInt($(this).attr('data-id'));
                list_id.push(sub_id);
            });
            
            if(list_id.length == 0) {
                toastr.error("Vui lòng chọn hàng cần xóa");
            }else {
                var check_sure = confirm("Bạn chắc chắn muốn xóa?");
                if(check_sure == true){
                    const __token = $('meta[name="csrf-token"]').attr('content');
                    data_ = {
                        list_id: list_id,
                        _token: __token
                    }
                    var request = $.ajax({
                        url: '{{route('user.delete_all')}}',
                        type: 'POST',
                        data: data_,
                        dataType: "json"
                    });
                    request.done(function (msg) {
                        if (msg.type == 1) {
                            $('.alert-success').remove();
                            $('.sub-chk:checked').each(function () {
                                $(this).parents("tr").remove();
                            });
                            $(__this).prop('disabled', false);
                            toastr.success(msg.mess);
                        }
                        return false;
                    });
                    request.fail(function (jqXHR, textStatus) {
                        alert("Không thể gửi yêu cầu mã lỗi : " + textStatus);
                    });
                }
            }
        });
        $(document).on('click','.status_hotel', function(e) {
            e.preventDefault();
            toastr.clear();
            toastr.options = {
                "closeButton": true,
                "timeOut": "5000",
                "positionClass": "toast-top-right"
            }
            const __this = this;
            $(__this).prop('disabled', true); //ngừng bấm
            var id = $(__this).attr('data-id');
            const __token = $('meta[name="csrf-token"]').attr('content');
            data_ = {
                _token: __token
            }
            var url_ = $(__this).attr('data-url');
            var request = $.ajax({
                url: url_,
                type: 'POST',
                data: data_,
                dataType: "json"
            });
            request.done(function (msg) {
                if (msg.type == 1) {
                    $('.alert-success').remove();
                    $(__this).prop('disabled', false);
                    $(__this).html('Blocked');
                    $(__this).removeClass('btn-primary');
                    $(__this).addClass('btn-danger');
                    toastr.success(msg.mess);
                }else{
                    $('.alert-success').remove();
                    $(__this).prop('disabled', false);
                    $(__this).html('Publish');
                    $(__this).removeClass('btn-danger');
                    $(__this).addClass('btn-primary');
                    toastr.success(msg.mess);
                }
                return false;
            });
            request.fail(function (jqXHR, textStatus) {
                alert("Không thể gửi yêu cầu mã lỗi : " + textStatus);
            });
            
        });
        $(document).on('click', '.page-link', function(e) {
            e.preventDefault();
            url_ = this.href;
            var url_string = location.href;
            var url = new URL(url_string);
            var role = url.searchParams.get("role");
            if(role != null){
                url_ = url_ + '&role='+role;
            }
            window.location.href=url_;
        })
        $(document).on('click', '.btn-apply', function(e){
            const __this = this;
            $(__this).prop('disable', true);
            var list_id = [];
            $('.sub-chk:checked').each(function(){
                var sub_id = parseInt($(this).attr('data-id'));
                list_id.push(sub_id);
            });
            
            var selected_action = parseInt($('.actions').val());
            if (list_id.length == 0) {
                toastr.error("Vui lòng chọn hàng cần xóa");
                $(__this).prop('disable', false);
            } else {
                if (selected_action == 5) {
                    var check_sure = confirm("Bạn có chắc chắn muốn xóa không?");
                    if (check_sure == true) {
                        var __token = $('meta[name="csrf-token"]').attr('content');
                        data_ = {
                            list_id: list_id,
                            _token: __token
                        }
                        var request = $.ajax({
                            url: '{{ route('review.delete')}}',
                            type: 'POST',
                            data: data__,
                            dataType: "json"
                        });
                        request.done(function(msg){
                            if(msg.type == 1){
                                $('.alert-success').remove();
                                $('.sub-chk:checked').each(function(){
                                    $(this).parents("tr").remove();
                                });
                                toastr.success(msg.mess);
                            }
                            return false;
                        });
                        request.fail(function(jqXHR, textStatus){
                            alert("Không thể gửi yêu cầu mã lỗi: "+textStatus);
                        });
                    }
                    $(this).prop('disable', false);
                }else if(selected_action != 0){
                    var __token = $('meta[name="csrf-token"]').attr('content');
                    data__ = {
                        list_id: list_id,
                        _token: __token,
                        selected_action: selected_action
                    }
                    var request = $.ajax({
                        url: '{{ route('review.change_status') }}',
                        type: 'POST',
                        data: data__,
                        dataType: "json"
                    });
                    request.done(function(msg){
                        switch (msg.type) {
                            case 1:{
                                $('.alert-success').remove();
                                $('.sub-chk:checked').each(function(){
                                    $(this).parents("tr").children('td').children('.status-review').removeClass('btn-success');
                                    $(this).parents("tr").children('td').children('.status-review').removeClass('btn-warning');
                                    $(this).parents("tr").children('td').children('.status-review').removeClass('btn-danger');
                                    $(this).parents("tr").children('td').children('.status-review').removeClass('btn-secondary');
                                    $(this).parents("tr").children('td').children('.status-review').addClass('btn-success');
                                    $(this).parents("tr").children('td').children('.status-review').html('Approved');
                                });
                                toastr.success(msg.mess);
                                break;
                            }
                            case 2:{
                                $('.alert-success').remove();
                                $('.sub-chk:checked').each(function(){
                                    $(this).parents("tr").children('td').children('.status-review').removeClass('btn-success');
                                    $(this).parents("tr").children('td').children('.status-review').removeClass('btn-warning');
                                    $(this).parents("tr").children('td').children('.status-review').removeClass('btn-danger');
                                    $(this).parents("tr").children('td').children('.status-review').removeClass('btn-secondary');
                                    $(this).parents("tr").children('td').children('.status-review').addClass('btn-warning');
                                    $(this).parents("tr").children('td').children('.status-review').html('Pending');
                                });
                                toastr.success(msg.mess);
                                break;
                            }
                            case 3:{
                                $('.alert-success').remove();
                                $('.sub-chk:checked').each(function(){
                                    $(this).parents("tr").children('td').children('.status-review').removeClass('btn-success');
                                    $(this).parents("tr").children('td').children('.status-review').removeClass('btn-warning');
                                    $(this).parents("tr").children('td').children('.status-review').removeClass('btn-danger');
                                    $(this).parents("tr").children('td').children('.status-review').removeClass('btn-secondary');
                                    $(this).parents("tr").children('td').children('.status-review').addClass('btn-danger');
                                    $(this).parents("tr").children('td').children('.status-review').html('Spam');
                                });
                                toastr.success(msg.mess);
                                break;
                            }
                            case 4:{
                                $('.alert-success').remove();
                                $('.sub-chk:checked').each(function(){
                                    $(this).parents("tr").children('td').children('.status-review').removeClass('btn-success');
                                    $(this).parents("tr").children('td').children('.status-review').removeClass('btn-warning');
                                    $(this).parents("tr").children('td').children('.status-review').removeClass('btn-danger');
                                    $(this).parents("tr").children('td').children('.status-review').removeClass('btn-secondary');
                                    $(this).parents("tr").children('td').children('.status-review').addClass('btn-secondary');
                                    $(this).parents("tr").children('td').children('.status-review').html('Trash');
                                });
                                toastr.success(msg.mess);
                                break;
                            }
                        }
                        return false;
                    });
                    request.fail(function(jqXHR, textStatus){
                        alert("Không thể gửi yêu cầu mã lỗi: "+textStatus);
                    });
                }
            }
        })
    </script>
    <script>
        $( document ).ready(function() {
            var url_string = location.href;
            var url = new URL(url_string);
            var role = url.searchParams.get("role");
            if(role != null){
                $('.filter_role').val(role);
            }
             
         });
     </script>
@endpush
