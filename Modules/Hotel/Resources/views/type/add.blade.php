@extends('base::layouts.master')
@section('css')
        <!-- Plugins css-->
        <link href="{{ URL::asset('assets/libs/select2/select2.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ URL::asset('assets/libs/summernote/summernote.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ URL::asset('assets/libs/dropzone/dropzone.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ URL::asset('assets/css/webt.css')}}" rel="stylesheet" type="text/css" />
@endsection
@section('content')
<div class="container-fluid">

    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="javascript: void(0);">UBold</a></li>
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Apps</a></li>
                        <li class="breadcrumb-item active">Add Type</li>
                    </ol>
                </div>
                <h4 class="page-title">Add Type</h4>
            </div>
        </div>
    </div>
    {{-- <div class="notification">
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        
    @if (session('errorsss'))
    <div class="alert alert-danger">
        {{ session('errorsss') }}
    </div>
   @endif
        </div> --}}
    
    <!-- end page title -->
    <form action="{{ route('type.post_add') }}" method="POST" enctype="multipart/form-data">
        <input type="hidden" name="_token" value="{{csrf_token()}}" /> 
        <div class="row">
            <div class="col-md-12 col-lg-9">
                <div class="card p-2">
                    <div class="row">
                        <div class="col-md-12 col-lg-12">
                            <h4>Name of Type</h4>
                            <div class="form-group mb-3">
                                <label for="name">Title </label>
                                <input type="text" id="name" name="name" class="form-control" placeholder="Name of Type">
                                <p class="help is-danger">{{ $errors->first('name') }}</p>
                            </div> 
                        </div>
                    </div>     
                </div>
                <div class="form-group mb-3 text-right">
                    <button type="submit" class="btn w-sm btn-success waves-effect waves-light btn-type">Save Changes</button>
                    <a href="{{route('type')}}" class="btn w-sm btn-warning waves-effect waves-light">Cancel</a>
                </div> 
            </div>
            <div class="col-md-12 col-lg-3">
                <div class="card p-2">
                    <h4>Publish</h4>
                    <hr>
                    <div class="form-group mb-3">
                        <input type="radio" id="" name="status" value="1" checked>
                        <label for="">Publish </label>
                    </div> 
                    <div class="form-group mb-3">
                        <input type="radio" id="" name="status" value="0">
                        <label for="">Block </label>
                    </div> 
                    <hr>
                    <h4>Avatar</h4>
                    <div class="form-group mb-3">
                        <div class="card-box text-center">
                            <span class="input-group-btn">
                                <button type="button" class="btn btn-warning" data-toggle="modal" data-target=".bd-example-modal-image-avatar">Select Avatar Image</button>
                                <input type="hidden" name="avatar" id="avatar" required>
                            </span>
                            <div id="preview_avatar" class="mt-3">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <!-- end row -->
    </form>
</div>



@section('js')
    <div class="modal fade bd-example-modal-image-avatar" id="modal-abbum" tabindex="-1" role="dialog" data-backdrop="false" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Manger Image</h5>
                    <button type="button" class="close" data-dismiss="modal"><i class="fas fa-times"></i></button>
                </div>
                <div class="modal-body">
                    <iframe src="{{ config('app.url') }}/filemanager/dialog.php?akey=4Ans6pfTs6A9DzHj8AZq1NVGKoYxpyoI8ivCN1bOow&field_id=avatar&multiple=0" frameborder="0" style="width: 100%; height: 500px; overflow-y: auto"></iframe>
                </div>
            </div>
        </div>
    </div>
@stop
@endsection

@push('js')
    <!-- Summernote js -->
    <script src="{{ URL::asset('assets/libs/summernote/summernote.min.js') }}"></script>
    <!-- Select2 js-->
    <script src="{{ URL::asset('assets/libs/select2/select2.min.js') }}"></script>
    <!-- Dropzone file uploads-->
    <script src="{{ URL::asset('assets/libs/dropzone/dropzone.min.js') }}"></script>
    <!-- Init js -->
    <script>
        jQuery(document).ready(function(){
        $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
        });
        // Summernote
    
        $('#content').summernote({
            height: 180,                 // set editor height
            minHeight: null,             // set minimum height of editor
            maxHeight: null,             // set maximum height of editor
            focus: false                 // set focus to editable area after initializing summernote
        });
        
        // Select2
        $('.select2').select2();
    
        
    });
         $(document).on('click','.check_master', function(e) {
            if($(this).is(':checked',true))
            {
                $(".sub-chk").prop('checked', true);
            } else {
                $(".sub-chk").prop('checked',false);
            }
        });
        
        $(document).on('click','.delete_all',function(e) {
            const __this = this;
            $(__this).prop('disabled', true);
            var list_id = [];
            $('.sub-chk:checked').each(function () {
                var sub_id =parseInt($(this).attr('data-id'));
                list_id.push(sub_id);
            });
            
            if(list_id.length == 0) {
                toastr.error("Vui lòng chọn hàng cần xóa");
            }else {
                var check_sure = confirm("Bạn chắc chắn muốn xóa?");
                if(check_sure == true){
                    const __token = $('meta[name="csrf-token"]').attr('content');
                    data_ = {
                        list_id: list_id,
                        _token: __token
                    }
                    var request = $.ajax({
                        url: '{{route('user.delete_all')}}',
                        type: 'POST',
                        data: data_,
                        dataType: "json"
                    });
                    request.done(function (msg) {
                        if (msg.type == 1) {
                            $('.alert-success').remove();
                            $('.sub-chk:checked').each(function () {
                                $(this).parents("tr").remove();
                            });
                            $(__this).prop('disabled', false);
                            toastr.success(msg.mess);
                        }
                        return false;
                    });
                    request.fail(function (jqXHR, textStatus) {
                        alert("Không thể gửi yêu cầu mã lỗi : " + textStatus);
                    });
                }
            }
        });
        $(document).on('click','.status_hotel',function(e) {
            e.preventDefault();
            toastr.clear();
            toastr.options = {
                "closeButton": true,
                "timeOut": "5000",
                "positionClass": "toast-top-right"
            }
            const __this = this;
            $(__this).prop('disabled', true);
            var id = $(__this).attr('data-id');
            const __token = $('meta[name="csrf-token"]').attr('content');
            data_ = {
                _token: __token
            }
            var url_ = $(__this).attr('data-url');
            var request = $.ajax({
                url: url_,
                type: 'POST',
                data: data_,
                dataType: "json"
            });
            request.done(function (msg) {
                if (msg.type == 1) {
                    $('.alert-success').remove();
                    $(__this).prop('disabled', false);
                    $(__this).html('Blocked');
                    $(__this).removeClass('btn-primary');
                    $(__this).addClass('btn-danger');
                    toastr.success(msg.mess);
                }else{
                    $('.alert-success').remove();
                    $(__this).prop('disabled', false);
                    $(__this).html('Publish');
                    $(__this).removeClass('btn-danger');
                    $(__this).addClass('btn-primary');
                    toastr.success(msg.mess);
                }
                return false;
            });
            request.fail(function (jqXHR, textStatus) {
                alert("Không thể gửi yêu cầu mã lỗi : " + textStatus);
            });
            
        });
        $(document).on('click','.page-link',function(e) {
            e.preventDefault();
            url_ = this.href;
            var url_string = location.href;
            var url = new URL(url_string);
            var role = url.searchParams.get("role");
            if(role != null){
                url_ = url_ + '&role='+role;
            }
            window.location.href=url_;
        })
// upload ảnh
        //avatar
        $(document).on('hide.bs.modal', '.bd-example-modal-image-avatar', function () {
            var _img = $('input#avatar').val();
            if (!_img.length) {
                $('#preview_avatar').empty();
            } else {
                $('#preview_avatar').empty();
                $html = `
                    <div class="box_imgg position-relative">
                        <img src="" id='show-img-hotel' style="width:100%;">
                        <i class="mdi mdi-close-circle-outline style_icon_remove style_icon_remove_avatar" title="delete"></i>
                    </div>
                `;
                $('#preview_avatar').append($html);
                $('#show-img-hotel').attr('src', _img);
            }
        });
        $(document).on('click', '.style_icon_remove_avatar', function() {
            $(this).parent('.box_imgg').hide('slow', function () { 
                $(this).remove(); 
                $('#banner').val('');
            });
        });
        
    </script>
    <script>
        $( document ).ready(function() {
            var url_string = location.href;
            var url = new URL(url_string);
            var role = url.searchParams.get("role");
            if(role != null){
                $('.filter_role').val(role);
            }
             
         });
     </script>
     {{-- ........................ --}}


      <script>
    var bookingCore = {
        url: 'http://sandbox.bookingcore.org',
        map_provider: 'gmap',
        map_gmap_key: '',
        csrf: 'JMrM5NwcxQy6HWOmuo7LQ2kHPh7pGwfbOPpXxkue'
    };
    var i18n = {
        warning: "Warning",
        success: "Success",
        confirm_delete: "Do you want to delete?",
        confirm: "Confirm",
        cancel: "Cancel",
    };
    </script>
    <script src="https://maps.google.com/maps/api/js?key=AIzaSyDsucrEdmswqYrw0f6ej3bf4M4suDeRgNA"></script>
    <script src="{{ URL::asset('assets/libs/gmaps/gmaps.min.js') }}"></script>
    <script src="{{ URL::asset('assets/js/pages/google-maps.init.js') }}"></script>
    <script src="{{ URL::asset('assets/js/map-engine.js') }}"></script>
    <script>
    jQuery(function ($) {
        new BravoMapEngine('gmaps-basic', {
            fitBounds: true,
            center: [{{"51.505"}}, {{"-0.09"}}],
            zoom:{{"8"}},
            ready: function (engineMap) {
                console.log(1);

                engineMap.on('click', function (dataLatLng) {
                    engineMap.clearMarkers();
                    engineMap.addMarker(dataLatLng, {
                        icon_options: {}
                    });
                    $("input[name=latitu]").attr("value", dataLatLng[0]);
                    $("input[name=longtitu]").attr("value", dataLatLng[1]);
                });
                engineMap.on('zoom_changed', function (zoom) {
                    $("input[name=zoom]").attr("value", zoom);
                })
            }
        });
    })
    </script>
    <script>
        

@endpush
   