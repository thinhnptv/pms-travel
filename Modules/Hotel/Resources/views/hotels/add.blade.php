@extends('base::layouts.master')
@section('css')
        <!-- Plugins css-->
        <link href="{{ URL::asset('assets/libs/select2/select2.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ URL::asset('assets/libs/summernote/summernote.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ URL::asset('assets/libs/dropzone/dropzone.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ URL::asset('assets/css/webt.css')}}" rel="stylesheet" type="text/css" />
@endsection
@section('content')
<div class="container-fluid">

    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="javascript: void(0);">UBold</a></li>
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Apps</a></li>
                        <li class="breadcrumb-item active">Add Hotel</li>
                    </ol>
                </div>
                <h4 class="page-title">Add Hotel</h4>
            </div>
        </div>
    </div>
    {{-- @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @if (session('errorsss'))
            <div class="alert alert-danger">
                {{ session('errorsss') }}
            </div>
        @endif --}}
    <!-- end page title -->
    <form action="{{ route('hotel.post_add') }}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="row">
            <div class="col-md-12 col-lg-9">
                <div class="card p-2">
                    <div class="row">
                        <div class="col-md-12 col-lg-12">
                            <h4>Hotel Content</h4>
                            <div class="form-group mb-3">
                                <label for="name">Title </label>
                                <span class="text-danger">*</span>
                                <input type="text" id="name" name="name" class="form-control" placeholder="Name of Hotel" value="{{ old('name') }}">
                                <p class="help is-danger">{{ $errors->first('name') }}</p>
                            </div>
                            <div class="form-group mb-3">
                                <label for="content">Content</label>
                                <textarea class="form-control" id="content" name="content" rows="5" placeholder="Please enter content">{{ old('content') }}</textarea>
                            </div>
                            {{-- <div class="form-group mb-3">
                                <label for="video" class="control-label">Youtube Video</label>
                                <input type="text" id="video" name="video" class="form-control" placeholder="Youtube link video">
                            </div>  --}}
                            <div class="form-group-image mb-3">
                                <label for="" >Banner Image</label>
                                <div class="card-box text-center">
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-warning" data-toggle="modal" data-target=".bd-example-modal-image-banner">Select Banner</button>
                                        <input type="hidden" name="banner" id="banner" required value="{{ old('banner') }}">
                                    </span>
                                    <div id="preview_banner" class="mt-3">
                                    @if (!empty(old('banner')))
                                        <div class="box_imgg position-relative">
                                            <img src="{{ old('banner') }}" id='show-img-banner' style="width:100%;">
                                            <i class="mdi mdi-close-circle-outline style_icon_remove style_icon_remove_banner" title="delete"></i>
                                        </div>
                                    @endif
                                    </div>

                                </div>
                            </div>
                            <div class="form-group-image mb-3">
                                <label for="" >Album Image</label>
                                <span class="text-danger">*</span>
                                <div class="card-box text-center">
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-warning" data-toggle="modal" data-target=".bd-example-modal-image-album">Select Album Image</button>
                                        <input type="hidden" name="album" id="album" value="{{ old('album') }}">
                                        <p class="help is-danger">{{ $errors->first('album') }}</p>
                                    </span>
                                    <div id="preview_album" class="mt-3">
                                        <div class="row row_preview_album" id="row_preview_album">
                                            @if (!empty( old('album')))
                                                @foreach (json_decode( old('album')) as $item)
                                                        <div class="col-3 mt-3">
                                                            <div class="box_imgg position-relative">
                                                                <img src="{{ $item }}" class="img-height-110" style="width:100%; height=110px;">
                                                                <i class="mdi mdi-close-circle-outline style_icon_remove style_icons_remove_album" title="delete"></i>
                                                            </div>
                                                        </div>
                                                @endforeach
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <h4>Hotel Policy</h4>
                            <div class="form-group mb-3">
                                <label for="rating_star">Hotel rating standard</label>
                                <span class="text-danger">*</span>
                                <input type="number" id="rating_star" name="rating_star" class="form-control" placeholder="Eg: 5" value="{{ old('rating_star') }}">
                                <p class="help is-danger">{{ $errors->first('rating_star') }}</p>
                            </div>
                            <div class="form-group-item">
                                <label class="control-label">FAQ</label>
                                    <div class="g-items-header">
                                        <div class="row">
                                            <div class="col-md-4">Title</div>
                                            <div class="col-md-4">Content</div>
                                            <div class="col-md-1"></div>
                                        </div>
                                    </div>
                                    <div class="g-items tr_rating">
                                        @php
                                                $j = 0;
                                        @endphp
                                        @if (!empty(old('policy')))
                                            @foreach (old('policy') as $item)
                                            <div class="item" data-number="{{ $j }}">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <input type="text" name="policy[{{ $j }}][title]" class="form-control" placeholder="Eg: What kind of foowear is most suitable ?" value="{{ $item['title'] }}">
                                                    </div>
                                                    <div class="col-md-4">
                                                        <textarea name="policy[{{ $j }}][content]" class="form-control" placeholder="">{{ $item['content'] }}</textarea>
                                                    </div>
                                                    <div class="col-md-1">
                                                        <span class="btn btn-danger btn-sm btn-remove-item btn-remove-item-rating "><i class="fa fa-trash"></i></span>
                                                    </div>
                                                </div>
                                            </div>
                                            @php
                                                $j++;
                                            @endphp
                                            @endforeach
                                        @endif
                                    </div>
                                <div class="text-right">
                                    <span class="btn btn-info btn-sm btn-add-item add-items-raiting"><i class="icon ion-ios-add-circle-outline add-items-raiting"></i> Add item</span>
                                </div>
                            </div>
                            <div class="form-group-item">
                                <label class="control-label">Utilities</label>
                                    <div class="g-items-header">
                                        <div class="row">
                                            <div class="col-md-11">Title</div>
                                            <div class="col-md-1"></div>
                                        </div>
                                    </div>
                                    <div class="g-items g-items-utilities">
                                        @php
                                                $k = 0;
                                        @endphp
                                        @if (!empty(old('utilities')))
                                        @foreach (old('utilities') as $item)
                                        <div class="item" data-number="{{ $k }}">
                                            <div class="row">
                                                <div class="col-md-11">
                                                     <input type="text" name="utilities[{{ $k }}][title]" class="form-control" placeholder="Eg: What kind of foowear is most suitable ?" value="{{ $item['title'] }}">
                                                </div>
                                                <div class="col-md-1">
                                                    <span class="btn btn-danger btn-sm btn-remove-item btn-remove-item-utilities"><i class="fa fa-trash"></i></span>
                                                 </div>
                                            </div>
                                        </div>
                                        @php
                                            $k++;
                                        @endphp
                                        @endforeach
                                    @endif
                                    </div>
                                <div class="text-right">
                                    <span class="btn btn-info btn-sm btn-add-item add-items-utilities"><i class="icon ion-ios-add-circle-outline add-items-utilities"></i> Add item</span>
                                </div>
                            </div>
                            <hr>
                            <h4>Type of Hotel <span class="text-danger">*</span></h4>
                            <div class="form-group mb-3">
                                <label for="type_id">Hotel Type</label>
                                <select class="form-control" id="type_id" name="type_id">
                                    <option value="">--Please Select--</option>
                                        @foreach ($typeHotel as $item)
                                            <option value="{{ $item->id }}"
                                                @if (old('type_id') == $item->id)
                                                    selected
                                                @endif>
                                                {{ $item->name }}
                                            </option>
                                        @endforeach
                                </select>
                            </div>
                            <hr>
                            <h4>Locations</h4>
                            <div class="form-group mb-3">
                                <label for="location_id">Location</label>
                                <select class="form-control" id="location_id" name="location_id">
                                    {{-- <option value="">--Please Select--</option> --}}
                                        @foreach ($location as $item)
                                            <option value="{{ $item->id }}"
                                                @if (old('location_id') == $item->id)
                                                    selected
                                                @endif
                                                >
                                                {{ $item->name }}
                                            </option>
                                        @endforeach
                                </select>
                            </div>
                            <div class="form-group mb-3">
                                <label for="address">Real Address</label>
                                <input type="text" id="address" name="address" class="form-control" placeholder="Real Address" value="{{ old('address') }}">
                            </div>
                            <div class="row">
                            <div class="col-md-6 col-lg-6">
                            <div class="form-group mb-3">
                                <h4 class="header-title mb-3">The geographic coordinate</h4>
                                <div id="gmaps-basic" class="gmaps"></div>
                                <i>Click onto map to place Location address
                                </i>
                            </div>
                            </div>
                            <div class="col-md-6 col-lg-6">
                            <div class="form-group mb-3">
                                <label for="latitu">map_lat</label>
                                <span class="text-danger">*</span>
                                <input type="text" id="latitu" name="latitu" class="form-control"
                                       placeholder="Please enter " value="{{ old('latitu') }}">
                                <p class="help is-danger">{{ $errors->first('latitu') }}</p>
                            </div>
                            <div class="form-group mb-3">
                                <label for="longtitu">map_lng </label>
                                <span class="text-danger">*</span>
                                <input type="text" id="longtitu" name="longtitu" class="form-control"
                                       placeholder="Please enter " value="{{ old('longtitu') }}">
                                <p class="help is-danger">{{ $errors->first('longtitu') }}</p>
                            </div>
                            <div class="form-group mb-3">
                                <label for="zoom">map_zoom</label>
                                <input type="text" id="zoom" name="zoom" class="form-control"
                                       placeholder="Please enter " value="{{ old('zoom') }}">
                            </div>
                            </div>
                            </div>
                            <hr>
                            <h4>Check in/out time</h4>
                            <div class="row">
                                <div class="col-md-6 col-lg-6">
                                    <div class="form-group mb-3">
                                        <label for="checkin">Time for checkin </label>
                                        <input type="text" id="checkin" name="checkin" class="form-control" placeholder="Eg: 12:00 am" value="{{ old('checkin') }}">
                                    </div>
                                </div>
                                <div class="col-md-6 col-lg-6">
                                    <div class="form-group mb-3">
                                        <label for="checkout">Time for checkout </label>
                                        <input type="text" id="checkout" name="checkout" class="form-control" placeholder="Eg: 11:00 am" value="{{ old('checkout') }}">
                                    </div>
                                </div>
                                </div>
                            <hr>
                            <h4>Pricing</h4>
                            <div class="form-group mb-3">
                                <label for="price">Price </label>
                                <span class="text-danger">*</span>
                                <input type="number" id="price" name="price" class="form-control" placeholder="" value="{{ old('price') }}">
                                <p class="help is-danger">{{ $errors->first('price') }}</p>
                            </div>
                            <div class="form-group mb-3">
                                <label for="hotel_sale">Hotel Sale </label>
                                <input type="number" id="hotel_sale" name="hotel_sale" class="form-control" placeholder="" value="{{ old('hotel_sale') }}">
                                <p class="help is-danger">{{ $errors->first('hotel_sale') }}</p>
                            </div>
                            {{-- <div class="form-group-item " data-condition="enable_extra_price:is(1)" style="">
                                <label class="control-label">Extra Price</label>
                                    <div class="g-items-header">
                                        <div class="row">
                                            <div class="col-md-4">Name</div>
                                            <div class="col-md-2">Price</div>
                                            <div class="col-md-2">Type</div>
                                            <div class="col-md-1"></div>
                                        </div>
                                    </div>
                                <div class="g-items tr_price">

                                </div>
                                <div class="text-right">
                                    <span class="btn btn-info btn-sm btn-add-items add-items"><i class="icon ion-ios-add-circle-outline "></i> Add item</span>
                                </div>
                            </div> --}}
                            <hr>
                            <h4>Seo Manager</h4>
                            <div class="form-group mb-3">
                                <label for="seo_title">Seo Title </label>
                                <input type="text" id="seo_title" name="seo_title" class="form-control" placeholder="Leave blank to use service title" value="{{ old('seo_title') }}">
                            </div>
                            <div class="form-group mb-3">
                                <label for="seo_description">Seo Description</label>
                                <textarea class="form-control" id="seo_description" name="seo_description" rows="5" placeholder="Enter description...">{{ old('seo_description') }}</textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 col-lg-3">
                <div class="card p-2">
                    <h4>Status</h4>
                    <hr>
                    <div class="form-group mb-3">
                        <input type="radio" id="" name="status" value="1" checked value="{{ old('status') }}">
                        <label for="">Publish </label>
                    </div>
                    <div class="form-group mb-3">
                        <input type="radio" id="" name="status" value="0" value="{{ old('status') }}">
                        <label for="">Block </label>
                    </div>
                    <hr>
                    <h4>Author Setting</h4>
                    <div class="form-group mb-3">
                        <select class="form-control" id="vendor_id" name="vendor_id">
                            {{-- <option value="">--Select User---</option> --}}
                            @foreach ($vendor as $item)
                            <option value="{{ $item->id }}"
                                @if (old('vendor_id') == $item->id)
                                    selected
                                @endif
                                >{{ $item->firstname }} {{ $item->lastname }}</option>
                            @endforeach
                        </select>
                    </div>
                    <hr>
                    <h4>Availability</h4>
                    <div class="form-group mb-3">
                        <input type="checkbox" id="availability" name="availability" value="1"
                        @if (old('availability') == 1)
                            checked
                         @endif
                        >
                        <label for="availability"> Enable featured</label>
                    </div>
                    <h4>Sale Holiday</h4>
                    <div class="form-group mb-3">
                        <label for="sale_holiday">
                        <input type="checkbox" id="sale_holiday" name="sale_holiday" value="1"
                        @if (old('sale_holiday') == 1)
                            checked
                        @endif
                        >
                         Enable featured</label>
                    </div>
                    <hr>
                    <h4>Attribute: Property type</h4>
                    <div class="form-group mb-3">
                        <div class="terms-scrollable">
                            @foreach ($list_property as $item)
                                <label class="term-item">
                                    <input type="checkbox" name="property_term[]" value="{{ $item->id }}"
                                    @if (!empty(old('property_term')) && in_array($item->id, old('property_term')))
                                        checked
                                    @endif
                                    >
                                    <span class="term-name">{{ $item->name }}</span>
                                </label>
                            @endforeach
                        </div>
                    </div>
                    @php
                        // dd(old('property_term'));
                    @endphp
                    <hr>
                    <h4>Attribute: Facilities</h4>
                    <div class="form-group mb-3">
                        <div class="terms-scrollable">
                            @foreach ($list_facilities as $item)
                                <label class="term-item">
                                    <input type="checkbox" name="facility_term[]" value="{{ $item->id }}"
                                    @if (!empty(old('facility_term')) && in_array($item->id, old('facility_term')))
                                        checked
                                    @endif
                                    >
                                    <span class="term-name">{{ $item->name }}</span>
                                </label>
                            @endforeach
                        </div>
                    </div>
                    <hr>
                    <h4>Attribute: Hotel Service</h4>
                    <div class="form-group mb-3">
                        <div class="terms-scrollable">
                            @foreach ($list_service as $item)
                                <label class="term-item">
                                    <input type="checkbox" name="service_term[]" value="{{ $item->id }}"
                                    @if (!empty(old('service_term')) && in_array($item->id, old('service_term')))
                                        checked
                                    @endif
                                    >
                                    <span class="term-name">{{ $item->name }}</span>
                                </label>
                            @endforeach
                        </div>
                    </div>
                    <hr>
                    <h4>Feature Image <span class="text-danger">*</span></h4>
                    <div class="form-group mb-3">
                        <div class="card-box text-center">
                            <span class="input-group-btn">
                                <button type="button" class="btn btn-warning" data-toggle="modal" data-target=".bd-example-modal-image-feature">Select Featured Image</button>
                                <input type="hidden" name="feature" id="feature" required value="{{ old('feature') }}">
                                <p class="help is-danger">{{ $errors->first('feature') }}</p>
                            </span>
                            <div id="preview_feature" class="mt-3">
                                @if (!empty(old('feature')))
                                    <div class="box_imgg position-relative">
                                        <img src="{{ old('feature')}}" id='show-img-hotel' style="width:100%;">
                                        <i class="mdi mdi-close-circle-outline style_icon_remove style_icon_remove_feature" title="delete"></i>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group mb-3 text-right">
            <button type="submit" class="btn w-sm btn-success waves-effect waves-light">Save Changes</button>
            <a href="{{route('hotel.list')}}" class="btn w-sm btn-warning waves-effect waves-light">Cancel</a>
        </div>
    <!-- end row -->
    </form>
</div>



@section('js')
    <div class="modal fade bd-example-modal-image-banner" id="modal-abbum" tabindex="-1" role="dialog" data-backdrop="false" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Manger Image</h5>
                    <button type="button" class="close" data-dismiss="modal"><i class="fas fa-times"></i></button>
                </div>
                <div class="modal-body">
                    <iframe src="{{ config('app.url') }}/filemanager/dialog.php?akey=4Ans6pfTs6A9DzHj8AZq1NVGKoYxpyoI8ivCN1bOow&field_id=banner&multiple=0" frameborder="0" style="width: 100%; height: 500px; overflow-y: auto"></iframe>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade bd-example-modal-image-album" id="modal-abbum" tabindex="-1" role="dialog" data-backdrop="false" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Manger Image</h5>
                    <button type="button" class="close" data-dismiss="modal"><i class="fas fa-times"></i></button>
                </div>
                <div class="modal-body">
                    <iframe src="{{ config('app.url') }}/filemanager/dialog.php?akey=4Ans6pfTs6A9DzHj8AZq1NVGKoYxpyoI8ivCN1bOow&field_id=album&multiple=1" frameborder="0" style="width: 100%; height: 500px; overflow-y: auto"></iframe>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade bd-example-modal-image-feature" id="modal-abbum" tabindex="-1" role="dialog" data-backdrop="false" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Manger Image</h5>
                    <button type="button" class="close" data-dismiss="modal"><i class="fas fa-times"></i></button>
                </div>
                <div class="modal-body">
                    <iframe src="{{ config('app.url') }}/filemanager/dialog.php?akey=4Ans6pfTs6A9DzHj8AZq1NVGKoYxpyoI8ivCN1bOow&field_id=feature&multiple=0" frameborder="0" style="width: 100%; height: 500px; overflow-y: auto"></iframe>
                </div>
            </div>
        </div>
    </div>
@stop
@endsection

@push('js')
    <!-- Summernote js -->
    <script src="{{ URL::asset('assets/libs/summernote/summernote.min.js') }}"></script>
    <!-- Select2 js-->
    <script src="{{ URL::asset('assets/libs/select2/select2.min.js') }}"></script>
    <!-- Dropzone file uploads-->
    <script src="{{ URL::asset('assets/libs/dropzone/dropzone.min.js') }}"></script>
    <!-- Init js -->
    <script>
        jQuery(document).ready(function(){
        $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
        });
        // Summernote

        $('#content').summernote({
            height: 180,                 // set editor height
            minHeight: null,             // set minimum height of editor
            maxHeight: null,             // set maximum height of editor
            focus: false                 // set focus to editable area after initializing summernote
        });

        // Select2
        $('.select2').select2();


    });
         $(document).on('click','.check_master', function(e) {
            if($(this).is(':checked',true))
            {
                $(".sub-chk").prop('checked', true);
            } else {
                $(".sub-chk").prop('checked',false);
            }
        });
        $(document).on('click','.delete_all',function(e) {
            const __this = this;
            $(__this).prop('disabled', true);
            var list_id = [];
            $('.sub-chk:checked').each(function () {
                var sub_id =parseInt($(this).attr('data-id'));
                list_id.push(sub_id);
            });

            if(list_id.length == 0) {
                toastr.error("Vui lòng chọn hàng cần xóa");
            }else {
                var check_sure = confirm("Bạn chắc chắn muốn xóa?");
                if(check_sure == true){
                    const __token = $('meta[name="csrf-token"]').attr('content');
                    data_ = {
                        list_id: list_id,
                        _token: __token
                    }
                    var request = $.ajax({
                        url: '{{route('user.delete_all')}}',
                        type: 'POST',
                        data: data_,
                        dataType: "json"
                    });
                    request.done(function (msg) {
                        if (msg.type == 1) {
                            $('.alert-success').remove();
                            $('.sub-chk:checked').each(function () {
                                $(this).parents("tr").remove();
                            });
                            $(__this).prop('disabled', false);
                            toastr.success(msg.mess);
                        }
                        return false;
                    });
                    request.fail(function (jqXHR, textStatus) {
                        alert("Không thể gửi yêu cầu mã lỗi : " + textStatus);
                    });
                }
            }
        });
        $(document).on('click','.status_hotel',function(e) {
            e.preventDefault();
            toastr.clear();
            toastr.options = {
                "closeButton": true,
                "timeOut": "5000",
                "positionClass": "toast-top-right"
            }
            const __this = this;
            $(__this).prop('disabled', true);
            var id = $(__this).attr('data-id');
            const __token = $('meta[name="csrf-token"]').attr('content');
            data_ = {
                _token: __token
            }
            var url_ = $(__this).attr('data-url');
            var request = $.ajax({
                url: url_,
                type: 'POST',
                data: data_,
                dataType: "json"
            });
            request.done(function (msg) {
                if (msg.type == 1) {
                    $('.alert-success').remove();
                    $(__this).prop('disabled', false);
                    $(__this).html('Blocked');
                    $(__this).removeClass('btn-primary');
                    $(__this).addClass('btn-danger');
                    toastr.success(msg.mess);
                }else{
                    $('.alert-success').remove();
                    $(__this).prop('disabled', false);
                    $(__this).html('Publish');
                    $(__this).removeClass('btn-danger');
                    $(__this).addClass('btn-primary');
                    toastr.success(msg.mess);
                }
                return false;
            });
            request.fail(function (jqXHR, textStatus) {
                alert("Không thể gửi yêu cầu mã lỗi : " + textStatus);
            });

        });
        $(document).on('click','.page-link',function(e) {
            e.preventDefault();
            url_ = this.href;
            var url_string = location.href;
            var url = new URL(url_string);
            var role = url.searchParams.get("role");
            if(role != null){
                url_ = url_ + '&role='+role;
            }
            window.location.href=url_;
        })
        var i=0;
        $(document).on('click', '.add-items', function(){

            var html = ` <div class="item" data-number="${i}">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <input type="text" name="extra_price[${i}][title]" class="form-control" placeholder="Extra price name" >
                                            </div>
                                            <div class="col-md-2">
                                                <input type="number" min="0" name="extra_price[${i}][price]" class="form-control" >
                                            </div>
                                            <div class="col-md-2">
                                                <select name="extra_price[${i}][type]" class="form-control">
                                                    <option value="0">One-time</option>
                                                    <option value="1">Per day</option>
                                                </select>
                                            </div>
                                            <div class="col-md-1">
                                                <span class="btn btn-danger btn-sm btn-remove-item btn-remove-item-price"><i class="fa fa-trash"></i></span>
                                            </div>
                                         </div>
                                    </div> `;
            $('.tr_price').append(html);
            i++;
        })
            var j= {{ $j }};
            $(document).on('click', '.add-items-raiting', function(){
            var html = ` <div class="item" data-number="${j}">
                                            <div class="row">
                                                <div class="col-md-4">
                                                     <input type="text" name="policy[${j}][title]" class="form-control" placeholder="Eg: What kind of foowear is most suitable ?">
                                                </div>
                                                <div class="col-md-4">
                                                    <textarea name="policy[${j}][content]" class="form-control" placeholder=""></textarea>
                                                 </div>
                                                <div class="col-md-1">
                                                    <span class="btn btn-danger btn-sm btn-remove-item btn-remove-item-rating "><i class="fa fa-trash"></i></span>
                                                 </div>
                                            </div>
                                        </div>`;
            $('.tr_rating').append(html);
            j++;
        })
        var k = {{ $k }};
            $(document).on('click', '.add-items-utilities', function(){
            var html = ` <div class="item" data-number="${k}">
                                            <div class="row">
                                                <div class="col-md-11">
                                                     <input type="text" name="utilities[${k}][title]" class="form-control" placeholder="Eg: What kind of foowear is most suitable ?" >
                                                </div>
                                                <div class="col-md-1">
                                                    <span class="btn btn-danger btn-sm btn-remove-item btn-remove-item-utilities"><i class="fa fa-trash"></i></span>
                                                 </div>
                                            </div>
                                        </div>`;
            $('.g-items-utilities').append(html);
            k++;
        })
        $(document).on('click', '.btn-remove-item-rating', function(){
            // console.log(1);
            $(this).parents('.item').remove();
        })
        $(document).on('click', '.btn-remove-item-price', function(){
            $(this).parents('.item').remove();
        })
        $(document).on('click', '.btn-remove-item-utilities', function(){
            $(this).parents('.item').remove();
        })
// upload ảnh
        //banner
        $(document).on('hide.bs.modal', '.bd-example-modal-image-banner', function(){
            var _img = $('input#banner').val();
            console.log(_img);
            if (!_img.length) {
                $('#preview_banner').empty();
            } else {
                $('#preview_banner').empty();
                $html = `<div class="box_imgg position-relative">
                        <img src="" id='show-img-banner' style="width:100%;">
                        <i class="mdi mdi-close-circle-outline style_icon_remove style_icon_remove_banner" title="delete"></i>
                    </div>
                        `;
                $('#preview_banner').append($html);
                $('#show-img-banner').attr('src', _img);
            }
        });
        $(document).on('click', '.style_icon_remove_banner', function() {
            $(this).parent('.box_imgg').hide('slow', function () {
                $(this).remove();
                $('#banner').val('');
            });
        });
        //album
        $(document).on('hide.bs.modal', '.bd-example-modal-image-album', function () {
            var _img = $('input#album').val();
            if (!_img.length) {
                $('#preview_album').empty();
            } else {
                if(_img[0] == '[') {
                    var array = JSON.parse(_img);
                    $('#row_preview_album').empty();
                    var html = '';
                    $.each(array, function( index, value ) {
                        html += `
                            <div class="col-3 mt-3">
                                <div class="box_imgg position-relative">
                                    <img src="${value}" class="img-height-110" style="width:100%; height=110px;">
                                    <i class="mdi mdi-close-circle-outline style_icon_remove style_icons_remove_album" title="delete"></i>
                                </div>
                            </div>
                        `;
                    });
                    $('#row_preview_album').append(html);

                }else{

                    $html = `
                        <div class="col-3 mt-3">
                            <div class="box_imgg position-relative">
                                <img src="" id='show-img-album' style="width:100%;">
                                <i class="mdi mdi-close-circle-outline style_icon_remove style_icons_remove_album" title="delete"></i>
                            </div>
                        </div>
                    `;
                    $('#row_preview_album').empty();
                    $('#row_preview_album').append($html);
                    $('#show-img-album').attr('src', _img);
                    var str_src = '';
                    str_src += "[\"";
                    str_src += _img;
                    str_src += "\"]";
                    $('#album').val(str_src);
                }

            }
        });

        $(document).on('click', '.style_icons_remove_album', function() {
            $(this).parents('.col-3').hide('slow', function () {
                $(this).remove();
                var arr_image = [];
                var str_src = '';
                $('.row_preview_album').find('.col-3').each(function () {
                    var str_image = '';
                    str_image += '"';
                    str_image += $(this).find('.img-height-110').attr('src');
                    str_image += '"';
                    arr_image.push(str_image);
                });
                str_src += "[";
                str_src += arr_image.toString();
                str_src += "]";
                if(arr_image.length == 0){
                    $('#album').val('');
                }else{
                    $('#album').val(str_src);
                }
            });
            });
        //feature
        $(document).on('hide.bs.modal', '.bd-example-modal-image-feature', function () {
            var _img = $('input#feature').val();
            if (!_img.length) {
                $('#preview_feature').empty();
            } else {
                $('#preview_feature').empty();
                $html = `
                    <div class="box_imgg position-relative">
                        <img src="" id='show-img-hotel' style="width:100%;">
                        <i class="mdi mdi-close-circle-outline style_icon_remove style_icon_remove_feature" title="delete"></i>
                    </div>
                `;
                $('#preview_feature').append($html);
                $('#show-img-hotel').attr('src', _img);
            }
        });
        $(document).on('click', '.style_icon_remove_feature', function() {
            $(this).parent('.box_imgg').hide('slow', function () {
                $(this).remove();
                $('#banner').val('');
            });
        });

    </script>
    <script>
        $( document ).ready(function() {
            var url_string = location.href;
            var url = new URL(url_string);
            var role = url.searchParams.get("role");
            if(role != null){
                $('.filter_role').val(role);
            }

         });
     </script>
     {{-- ........................ --}}


      <script>
    var bookingCore = {
        url: 'http://sandbox.bookingcore.org',
        map_provider: 'gmap',
        map_gmap_key: '',
        csrf: 'JMrM5NwcxQy6HWOmuo7LQ2kHPh7pGwfbOPpXxkue'
    };
    var i18n = {
        warning: "Warning",
        success: "Success",
        confirm_delete: "Do you want to delete?",
        confirm: "Confirm",
        cancel: "Cancel",
    };
    </script>
    <script src="https://maps.google.com/maps/api/js?key=AIzaSyDsucrEdmswqYrw0f6ej3bf4M4suDeRgNA"></script>
    <script src="{{ URL::asset('assets/libs/gmaps/gmaps.min.js') }}"></script>
    <script src="{{ URL::asset('assets/js/pages/google-maps.init.js') }}"></script>
    <script src="{{ URL::asset('assets/js/map-engine.js') }}"></script>
    <script>
    jQuery(function ($) {
        new BravoMapEngine('gmaps-basic', {
            fitBounds: true,
            center: [{{"51.505"}}, {{"-0.09"}}],
            zoom:{{"8"}},
            ready: function (engineMap) {
                console.log(1);

                engineMap.on('click', function (dataLatLng) {
                    engineMap.clearMarkers();
                    engineMap.addMarker(dataLatLng, {
                        icon_options: {}
                    });
                    $("input[name=latitu]").attr("value", dataLatLng[0]);
                    $("input[name=longtitu]").attr("value", dataLatLng[1]);
                });
                engineMap.on('zoom_changed', function (zoom) {
                    $("input[name=zoom]").attr("value", zoom);
                })
            }
        });
    })
    </script>

@endpush
