<?php

namespace Modules\Hotel\Entities;

use Illuminate\Database\Eloquent\Model;

class Price extends Model
{
    protected $table = 'prices';
    protected $fillable = ['title', 'price', 'type', 'hotel_id', 'created_at', 'updated_at'];
}
