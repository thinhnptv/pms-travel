<?php

namespace Modules\Hotel\Entities;

use Illuminate\Database\Eloquent\Model;

class ExtraPriceBooked extends Model
{
    protected $table = 'extra_price_booked';
    protected $fillable = ['title', 'price', 'booking_room_id', 'created_at', 'updated_at'];
}
