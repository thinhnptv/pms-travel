<?php

namespace Modules\Hotel\Entities;

use Illuminate\Database\Eloquent\Model;

class HotelType extends Model
{
    protected $table = 'hotel_types';
    protected $fillable = ['id', 'name', 'avatar'];
    public function hotel()
    {
        return $this->hasMany(Hotel::class, 'type_id', 'id');
    }
}
