<?php

namespace Modules\Hotel\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Category\Entities\Location;
use Modules\Category\Entities\Term;
use Modules\User\Entities\User;
use Modules\User\Entities\VendorRequest;
use Modules\Hotel\Entities\HotelWishList;

class Hotel extends Model
{
    protected $table= 'hotels';
    protected $fillable = ['id', 'utilities', 'type_id', 'zoom', 'video', 'location_id', 'vendor_id', 'name', 'status', 'address', 'checkin', 'checkout', 'description', 'content', 'title', 'latitu', 'longtitu', 'seo_description', 'seo_title', 'price', 'rating_star', 'created_at', 'updated_at', 'availability', 'evaluate_star', 'hotel_sale', 'sale_holiday', 'slug'];

    public function location()
    {
        return $this->belongsTo(Location::class, 'location_id', 'id');
    }
    public function user()
    {
        return $this->belongsTo(User::class, 'vendor_id', 'id');
    }
    public function review()
    {
        return $this->hasMany(ReviewHotel::class, 'hotel_id', 'id');
    }
    public function policy()
    {
        return $this->hasMany(Policy::class, 'hotel_id', 'id');
    }
    public function utilities()
    {
        return $this->hasMany(Utility::class, 'hotel_id', 'id');
    }
    public function extra_price()
    {
        return $this->hasMany(Price::class, 'hotel_id', 'id');
    }
    public function property()
    {
        return $this->belongsToMany(Term::class, 'property_term', 'hotel_id', 'term_id');
    }
    public function facility()
    {
        return $this->belongsToMany(Term::class, 'facility_term', 'hotel_id', 'term_id');
    }
    public function service()
    {
        return $this->belongsToMany(Term::class, 'service_term', 'hotel_id', 'term_id');
    }

    public function room()
    {
        return $this->hasMany(Room::class, 'hotel_id', 'id');
    }
    public function type()
    {
        return $this->belongsTo(HotelType::class, 'type_id', 'id');
    }
    public function wishListHotel()
    {
        return $this->hasMany(HotelWishList::class, 'hotel_id', 'id');
    }

    public function wishlistHotelBy($user)
    {
        return $this->wishListHotel()->where('user_id',$user->id)->exists();
    }
}
