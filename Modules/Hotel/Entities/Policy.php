<?php

namespace Modules\Hotel\Entities;

use Illuminate\Database\Eloquent\Model;

class Policy extends Model
{
    protected $table = 'policies';
    protected $fillable = ['title', 'content', 'hotel_id', 'created_at', 'updated_at'];
}
