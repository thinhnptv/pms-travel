<?php

namespace Modules\Hotel\Entities;

use Illuminate\Database\Eloquent\Model;

class Utility extends Model
{
    protected $table = 'utilities';
    protected $fillable = ['id', 'title', 'hotel_id', 'created_at', 'updated_at'];
}
