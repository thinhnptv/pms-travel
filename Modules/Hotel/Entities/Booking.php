<?php

namespace Modules\Hotel\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\User\Entities\User;

class Booking extends Model
{
    protected $table = 'booking_rooms';
    protected $fillable = ['id', 'room_id', 'user_id', 'status', 'payment_method', 'start_date', 'end_date', 'price', 'number', 'paid', 'special_requirements', 'created_at', 'updated_at'];
    
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
    public function room()
    {
        return $this->belongsTo(Room::class, 'room_id', 'id');
    }
    public function extraPriceBooked()
    {
        return $this->hasMany(ExtraPriceBooked::class, 'booking_room_id', 'id');
    }
}
