<?php

namespace Modules\Hotel\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Category\Entities\Roomterm;

class Room extends Model
{
    protected $table = 'rooms';
    protected $fillable = ['id', 'hotel_id', 'name', 'feature', 'album', 'price', 'number_of_room', 'number_of_beds', 'number_of_bed', 'room_size', 'max_adults', 'max_children', 'import_url', 'status', 'created_at', 'updated_at'];

    public function amenities()
    {
        return $this->belongsToMany(Roomterm::class, 'amenities_room_term', 'room_id', 'room_term_id');
    }
    public function hotel()
    {
        return $this->belongsTo(Hotel::class, 'hotel_id', 'id');
    }
    public function roomAvailability()
    {
        return $this->hasMany(RoomAvavailability::class, 'room_id', 'id');
    }

}
