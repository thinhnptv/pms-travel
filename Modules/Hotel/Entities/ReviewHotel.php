<?php

namespace Modules\Hotel\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Hotel\Entities\Hotel;
use Modules\User\Entities\User;

class ReviewHotel extends Model
{
    protected $table = 'review_hotels';
    protected $fillable = ['id', 'title', 'content', 'status', 'service', 'organization', 'friendliness', 'area_expert', 'safety', 'average', 'hotel_id', 'user_id', 'created_at', 'updated_at'];
    public function hotel()
    {
        return $this->belongsTo(Hotel::class, 'hotel_id', 'id');
    }
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
    public function policy()
    {
        return $this->hasMany(Policy::class, 'user_id', 'id');
    }
    
}
