<?php

namespace Modules\Hotel\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Hotel\Entities\Room;

class RoomAvavailability extends Model
{
    protected $fillable = [];

    public function room()
    {
        return $this->belongsTo(Room::class, 'room_id', 'id');
    }
}
