<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rooms', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('hotel_id')->unsigned()->nullable();
            $table->foreign('hotel_id')->references('id')->on('hotels')->onDelete('cascade');
            $table->string('name')->nullable();
            $table->string('feature')->nullable();
            $table->longText('album')->nullable();
            $table->integer('price')->nullable();
            $table->integer('number_of_room')->nullable();
            $table->integer('number_of_beds')->nullable();//giường đôi
            $table->integer('number_of_bed')->nullable();//giường đơn
            $table->integer('room_size')->nullable();
            $table->integer('max_adults')->nullable();
            $table->integer('max_children')->nullable();
            $table->string('import_url')->nullable();
            $table->tinyInteger('status')->default(1);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rooms');
    }
}
