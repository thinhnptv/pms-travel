<?php

namespace Modules\User\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Hotel\Entities\Hotel;
use Modules\User\Entities\Role;

class User extends Model
{
    // protected $dateFormat = "U";
    protected $table= 'users';
    protected $fillable= [ 'id', 'firstname', 'lastname', 'phone', 'address', 'biographica', 'avatar', 'vendor_commission_type', 'vendor_commission_value', 'status', 'business_name', 'birthday', 'email', 'email_verified_at', 'password', 'role_id', 'remember_token', 'created_at', 'updated_at' ];

    public function role()
    {
        return $this->belongsTo(Role::class, 'role_id', 'id');
    }
    public function getFullNameAttribute(){
        return $this->firstname. ' '.$this->lastname;
    }
}
