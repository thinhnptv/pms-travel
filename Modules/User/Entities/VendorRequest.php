<?php

namespace Modules\User\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\User\Entities\User;

class VendorRequest extends Model

{
    protected $table= 'vendor_requests';
    protected $fillable = ['id', 'firstname', 'lastname', 'approved_id', 'user_id', 'status', 'date_approve', 'created_at', 'updated_at'];

    public function userVendorUser()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
    public function userVendorApproved()
    {
        return $this->belongsTo(User::class, 'approved_id', 'id');
    }
}
