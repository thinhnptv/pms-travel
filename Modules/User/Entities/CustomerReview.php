<?php

namespace Modules\User\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Category\Entities\Location;

class CustomerReview extends Model
{
    protected $fillable = [];

    public function location() 
    {
        return $this->belongsTo(Location::class, 'location_id', 'id');
    }
}
