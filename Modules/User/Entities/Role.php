<?php

namespace Modules\User\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\User\Entities\User;

class Role extends Model
{
    protected $table= 'roles';
    protected $fillable = ['id', 'name', 'created_at', 'updated_at'];

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'role_id');
    }
}

