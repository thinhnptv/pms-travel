<?php

namespace Modules\User\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\User\Entities\Role;
use Modules\User\Http\Requests\AddRoleRequest;

class RoleController extends Controller
{
    protected $modelRole;

    public function __construct(Role $modelRole)
    {
        $this->modelRole = $modelRole;
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $listRole = $this->modelRole->paginate(15);
        return view('user::role.list', compact('listRole'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function addRole(AddRoleRequest $request)
    {
        $addRole = new $this->modelRole;
        $addRole->name = $request->name;
        $addRole->save();
        return redirect()->route('role.list')->with('success', 'Thêm mới thành công');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('user::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function deleteAll(Request $request){
        $list_id = $request->list_id;
        $this->modelRole->whereIn('id', $list_id)->delete();
        return response()->json([
            'type'=>1,
            'mess'=>"Xóa thành công"
            ]);
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function editRole($id)
    {
        $editRole = $this->modelRole->findOrFail($id);
        return view('user::role.edit', compact('editRole'));
    }
    public function updateRole($id, AddRoleRequest $request)
    {
        $editRole = $this->modelRole->findOrFail($id);
        $editRole->name = $request->name;
        $editRole->save();
        return redirect()->route('role.list')->with('success', 'Sửa thành công');
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function deleteRole($id)
    {
        $deleteRole = $this->modelRole->findOrFail($id);
        $deleteRole->delete();
        return redirect()->route('role.list')->with('success', 'Xoá thành công');
    }
}
