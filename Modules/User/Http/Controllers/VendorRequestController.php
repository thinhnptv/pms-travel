<?php

namespace Modules\User\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\User\Entities\User;
use Modules\User\Entities\Role;
use Modules\User\Entities\VendorRequest;

class VendorRequestController extends Controller
{

    protected $modelRole;
    protected $modelUser;
    protected $modelVendorRequest;

    public function __construct(User $modelUser, Role $modelRole, VendorRequest $modelVendorRequest){
        $this->modelUser = $modelUser;
        $this->modelRole = $modelRole;
        $this->modelVendorRequest = $modelVendorRequest;
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $list_vendor_request = $this->modelVendorRequest->with('userVendorUser')->with('userVendorApproved')->paginate(15);
        return view('user::vendor_request.list', compact('list_vendor_request'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('user::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('user::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('user::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
    public function approVendor($id, Request $request)
    {
        // dd($request->all());
        $user_id = $request->id_user;
        $vendor_request = $this->modelVendorRequest->findOrFail($id); // lấy ra từ bàng vendor requesst thằng id = $id
        $user = $this->modelUser->findOrFail($user_id);
        $vendor_request->status = 1;
        $vendor_request->date_approve= date("Y-m-d H:i:s");
        $vendor_request->approved_id= $request->id_approve;
        $user->role_id = 3;
        $vendor_request->save();
        $user->save();
        $name_approve = $vendor_request->userVendorApproved->firstname . " " . $vendor_request->userVendorApproved->lastname;
        return response()->json([
            'type'=>1,
            'mess'=>"Approve thành công",
            'vendor_request' => $vendor_request,
            'name_approve' => $name_approve
            ]);
    }
}
