<?php

namespace Modules\User\Http\Controllers;

// use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Modules\User\Http\Requests\CreateUserRequest;
use Modules\User\Http\Requests\EditUserRequest;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\User\Entities\User;
use Modules\User\Entities\Role;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    protected $modelRole;
    protected $modelUser;

    public function __construct(User $modelUser, Role $modelRole)
    {
        $this->modelUser = $modelUser;
        $this->modelRole = $modelRole;
    }

    public function index(Request $request)
    {
        $all_role =  $this->modelRole->all();
        $list_user = $this->modelUser::with('role');
        if($request->has('role') &&  $request->role != 0){
            $role_id = $request->role;
            $list_user->where('role_id', $role_id);
        }
        $list_user = $list_user->paginate(15);
        return view('user::user.list',compact('list_user','all_role'));
    }
    public function listVendor()
    {
        $list_user = $this->modelUser::with('role')->where('role_id',3)->paginate(15);
        return view('user::vendor.list', compact('list_user'));
    }
    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $all_role = $this->modelRole->all();
        return view('user::user.add', compact('all_role'));
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(CreateUserRequest $request)
    {
        $str_avatar = '';
        if ($request->input('avatar')) {
            $str_avatar = checkImage($request->input('avatar'));
        }
       
        $add_user = new $this->modelUser;
        $add_user->business_name = $request->business_name;
        $add_user->firstname = $request->firstname;
        $add_user->phone = $request->phone;
        $add_user->address = $request->address;
        $add_user->vendor_commission_type = $request->vendor_commission_type;
        $add_user->biographical = $request->biographical;
        $add_user->email = $request->email;
        $add_user->lastname = $request->lastname;
        $add_user->birthday = $request->birthday;
        $add_user->password = bcrypt($request->password);
        $add_user->vendor_commission_value = $request->vendor_commission_value;
        $add_user->avatar = $str_avatar;
        $add_user->status = $request->status;
        $add_user->role_id = $request->role_id;
        $add_user->save();
        return redirect()->route('user.list')->with('success', 'Thêm mới thành công');
    }

    
    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('user::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {
        $all_role = $this->modelRole->all();
        $user = $this->modelUser::with('role')->findOrFail($id);
        return view('user::user.edit', compact('user', 'all_role'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update($id, EditUserRequest $request)
    {
        if ($request->input('avatar')) {
            $str_avatar = checkImage($request->input('avatar'));
        }
        
        $edit_user = $this->modelUser->findOrFail($id);
        $edit_user->business_name = $request->business_name;
        $edit_user->firstname = $request->firstname;
        $edit_user->phone = $request->phone;
        $edit_user->address = $request->address;
        $edit_user->vendor_commission_type = $request->vendor_commission_type;
        $edit_user->biographical = $request->biographical;
        $edit_user->email = $edit_user->email;
        $edit_user->lastname = $request->lastname;
        $edit_user->birthday = $request->birthday;
        if(!empty($request->password)) {
            $edit_user->password = bcrypt($request->password);
        }else {
            $edit_user->password = $edit_user->password;
        }
        $edit_user->vendor_commission_value = $request->vendor_commission_value;
        $edit_user->avatar = $str_avatar;
        $edit_user->status = $request->status;
        $edit_user->role_id = $request->role_id;
        $edit_user->save();
        return redirect()->route('user.list')->with('success', 'Sửa thành công');

    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {
        $delete_user = $this->modelUser->findOrFail($id);
        $delete_user->delete();
        return redirect()->route('user.list')->with('success', 'Xóa thành công');
    }
    public function deleteAll(Request $request){
        $list_id = $request->list_id;
        $this->modelUser->whereIn('id', $list_id)->delete();
        return response()->json([
            'type'=>1,
            'mess'=>"Xóa thành công"
            ]);
    }
    public function changeStatus($id)
    {
        $user = $this->modelUser->findOrFail($id);
        if($user->status ==1){
            $user->status = 0;
            $user->save();
            return response()->json([
                'type'=>1,
                'mess'=>"Chuyển thành công"
            ]);
        }else{
            $user->status = 1;
            $user->save();
            return response()->json([
                'type'=>2,
                'mess'=>"Chuyển thành công"
            ]);
        }
    }
}
