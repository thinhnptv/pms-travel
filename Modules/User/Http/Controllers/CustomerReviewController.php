<?php

namespace Modules\User\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Category\Entities\Location;
use Modules\User\Entities\CustomerReview;
use Modules\User\Http\Requests\CreateCustomerReviewRequest;

class CustomerReviewController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    protected $modelCustomerReview;
    protected $modelLocation;

    public function __construct(CustomerReview $modelCustomerReview, Location $modelLocation)
    {
        $this->modelCustomerReview = $modelCustomerReview;
        $this->modelLocation = $modelLocation;
    }
    public function index()
    {
        $listCustomerReview = $this->modelCustomerReview::query();
        $listCustomerReview = $listCustomerReview->paginate(15);
        return view('user::customer_review.list', compact('listCustomerReview'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $listLocation = $this->modelLocation::query()->where('status', 1)->get();
        return view('user::customer_review.add', compact('listLocation'));
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(CreateCustomerReviewRequest $request)
    {
        $str_avatar = '';
        if ($request->input('avatar')) {
            $str_avatar = checkImage($request->input('avatar'));
        }
        $addCustomerReview = new $this->modelCustomerReview;
        $addCustomerReview->name = $request->name;
        $addCustomerReview->content = $request->biographical;
        $addCustomerReview->avatar = $str_avatar;
        $addCustomerReview->location_id = $request->location_id;
        $addCustomerReview->save();
        return redirect()->route('customer_review.list')->with('success', 'Thêm mới thành công');
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('user::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {
        $listLocation = $this->modelLocation::query()->where('status', 1)->get();
        $editCustomerReview = $this->modelCustomerReview::query()->findOrFail($id);
        return view('user::customer_review.edit', compact('listLocation', 'editCustomerReview'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update($id, CreateCustomerReviewRequest $request)
    {
        $str_avatar = '';
        if ($request->input('avatar')) {
            $str_avatar = checkImage($request->input('avatar'));
        }
        $updateCustomerReview = $this->modelCustomerReview::query()->findOrFail($id);
        $updateCustomerReview->name = $request->name;
        $updateCustomerReview->content = $request->biographical;
        $updateCustomerReview->location_id = $request->location_id;
        $updateCustomerReview->avatar = $str_avatar;
        $updateCustomerReview->save();
        return redirect()->route('customer_review.list')->with('success', 'Cập nhật thành công');
     }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {
        $deleteCustomerReview = $this->modelCustomerReview::query()->findOrFail($id);
        $deleteCustomerReview->delete();
        return redirect()->route('customer_review.list')->with('success', 'Xóa thành công');
    }
}
