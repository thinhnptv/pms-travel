<?php

namespace Modules\User\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class EditUserRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    public function rules(Request $request)
    {
        if(!empty($request->phone)){
            if(!empty($request->password)){
                return [
                    'password'=>'required|min:6|max:32',
                    'phone' => 'regex:/^0([1-9]{1})([0-9]{8})$/'
                ];
            }else{
                return [
                    'phone' => 'regex:/^0([1-9]{1})([0-9]{8})$/'
                ];
            }
        }else {
            if(!empty($request->password)){
                return [
                    'password'=>'required|min:6|max:32'
                ];
            }else{
                return [];
            }
        }
    }

    public function messages () 
    {
        return [
            'password.required' => "Mật Khẩu không được để trống!" ,
            'password.min' => "Mật khẩu phải có ít nhất 3 ký tự!" ,
            'password.max' => "Mật khẩu có nhiều nhất 32 ký tự!" ,
            'phone.regex' => "Số điện thoại không đúng định dạng!" 
        ];
    }
}
