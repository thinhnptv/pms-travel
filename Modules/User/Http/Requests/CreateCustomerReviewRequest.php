<?php

namespace Modules\User\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class CreateCustomerReviewRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules(Request $request)
    {
        return [
                'name'=>'required',
                'biographical'=>'required',
                'avatar' => 'required',
            ];
    }

    public function messages () 
    {
        return [
           'name.required' => 'Tên không được để trống',
           'biographical.required' => 'Nội dung không được để trống',
           'avatar.required' => 'Ảnh đại diện không được để trống',
        ];
    }
}
