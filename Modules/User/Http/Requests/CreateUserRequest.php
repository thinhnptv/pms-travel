<?php

namespace Modules\User\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class CreateUserRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules(Request $request)
    {
        if(!empty($request->phone)){
            return [
                'email'=>'required|email|unique:users,email',
                'password'=>'required|min:3|max:32',
                'phone' => 'regex:/^0([1-9]{1})([0-9]{8,9})$/'
            ];
        }else{
            return [
                'email'=>'required|email|unique:users,email',
                'password'=>'required|min:3|max:32'
            ];
        }
            
        
    }

    public function messages () 
    {
        return [
            'email.required' => "Email không được để trống!" ,
            'email.email' => "Email bạn nhập không đúng định dạng!" ,
            'email.unique' => "Email đã tồn tại!" ,
            'password.required' => "Mật Khẩu không được để trống!" ,
            'password.min' => "Mật khẩu phải có ít nhất 3 ký tự!" ,
            'password.max' => "Mật khẩu có nhiều nhất 32 ký tự!" ,
            'phone.regex' => "Số điện thoại không đúng định dạng!" ,
        ];
    }
}
