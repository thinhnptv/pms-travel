<?php

Route::group(['middleware' => ['web', 'adminLogin'], 'prefix' => 'admin/user', 'namespace' => 'Modules\User\Http\Controllers'], function()
{
    Route::get('/', 'UserController@index')->name('user.list');
    Route::get('/list-vendor', 'UserController@listVendor')->name('user.list_vendor');
    Route::get('/add', 'UserController@create')->name('user.get_add');
    Route::post('/add', 'UserController@store')->name('user.post_add');
    Route::get('/edit/{id}', 'UserController@edit')->name('user.edit');
    Route::post('/edit/{id}', 'UserController@update')->name('user.update');
    Route::get('/delete/{id}', 'UserController@destroy')->name('user.delete');
    Route::post('/delete-all', 'UserController@deleteAll')->name('user.delete_all');
    Route::post('/change-status/{id}', 'UserController@changeStatus')->name('user.change_status');
    Route::get('/listvendorrequest', 'VendorRequestController@index')->name('vendorrequest.list');
    Route::post('/approvevendor/{id}', 'VendorRequestController@approVendor')->name('vendorrequest.approve');

    Route::get('/customer-review', 'CustomerReviewController@index')->name('customer_review.list');
    Route::get('/add-customer-review', 'CustomerReviewController@create')->name('customer_review.create');
    Route::post('/add-customer-review', 'CustomerReviewController@store')->name('customer_review.store');
    Route::get('/edit-customer-review/{id}', 'CustomerReviewController@edit')->name('customer_review.edit');
    Route::post('/edit-customer-review/{id}', 'CustomerReviewController@update')->name('customer_review.update');
    Route::get('/delete-customer-review/{id}', 'CustomerReviewController@destroy')->name('customer_review.destroy');

    Route::get('/list-role', 'RoleController@index')->name('role.list');
    Route::get('/edit-role/{id}', 'RoleController@editRole')->name('role.edit');
    Route::get('/delete-role/{id}', 'RoleController@deleteRole')->name('role.delete');
    Route::post('/edit-role/{id}', 'RoleController@updateRole')->name('role.update');
    Route::post('/add-role', 'RoleController@addRole')->name('role.post_add');
    Route::post('/delete-all-role', 'RoleController@deleteAll')->name('role.deleteAll');
});
