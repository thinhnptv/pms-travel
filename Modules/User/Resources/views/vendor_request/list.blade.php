@extends('base::layouts.master')
@section('content')
    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">UBold</a></li>
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Apps</a></li>
                            <li class="breadcrumb-item active">List Vendor Request</li>
                        </ol>
                    </div>
                    <h4 class="page-title">List Vendor Request</h4>
                </div>
            </div>
        </div>
        @if (session('success'))
            <div class="alert alert-success">
                {{ session('success')  }}
            </div>
        @endif
        <!-- end page title -->
    <div class="row">
        <div class="col-12">
            <div class="card p-2">
                <div class="row mb-2">
                    <div class="col-sm-4">
                        
                    </div>
                    <div class="col-sm-8">
                        <div class="text-sm-right">
                            {{-- <a href="javascript:void(0)" class="btn btn-danger waves-effect waves-light mb-2 " ><i class="mdi mdi-delete mr-1"></i> Delete All</a> --}}
                        </div>
                    </div><!-- end col-->
                </div>
                <div class="table-responsive">
                    <table class="table table-bordered  mb-0">
                        <thead>
                            <tr>
                                <th width="50px"><input type="checkbox" id="master" class="check_master"></th>
                                <th class="text-center">ID</th>
                                <th class="text-center">Name</th>
                                <th class="text-center">Approved By</th>
                                <th class="text-center">Role request</th>
                                <th class="text-center">Email</th>
                                <th class="text-center">Date request</th>
                                <th class="text-center">Date approved</th>
                                <th class="text-center">Status</th>
                                <th class="text-center"></th>
                            </tr>
                        </thead>
                        <tbody>


                            @foreach ($list_vendor_request as $item)
                            <tr>
                                <td><input type="checkbox" class="sub-chk" data-id="{{ $item->id  }}"></td>
                                <th class="text-center" scope="row">{{ $item->id }}</th>
                                <td>{{ $item->firstname }} {{ $item->lastname }}</td>
                                @if(!empty($item->userVendorApproved->id))
                                    <td class="name_approved">{{ $item->userVendorApproved->firstname }} {{ $item->userVendorApproved->lastname }}</td>
                                @else
                                    <td class="name_approved"></td>
                                @endif
                                <td>Vendor</td>
                                <td>{{ $item->email }}</td>
                                <td class="text-center">{{ $item->created_at }}</td>
                                <td class="text-center date_approved">{{ $item->date_approve }}</td>
                                @if($item->status == 1)
                                <td class="text-center status_approved"><a href="#" class="btn btn-primary">Approved</a></td>
                                <td class="text-center "></td>
                                @else
                                <td class="text-center status_approved"><a href="#" class="btn btn-danger">Pending</a></td>
                                {{-- sau này sửa cái data-idapprove thành Auth->user->id --}}
                                <td class="text-center"><a href="javasctipr:void(0)" class="btn btn-info approve_vendor" data-url="{{ route('vendorrequest.approve', $item->id) }}" data-idapprove="1" data-iduser="{{ $item->userVendorUser->id }}" data-id="{{ $item->id }}">Approve</a></td>  
                                    @endif
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div class="mt-2">
                        {{ $list_vendor_request->render() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('js')
    <!-- Summernote js -->
    <script src="{{ URL::asset('assets/libs/summernote/summernote.min.js') }}"></script>
    <!-- Select2 js-->
    <script src="{{ URL::asset('assets/libs/select2/select2.min.js') }}"></script>
    <!-- Dropzone file uploads-->
    <script src="{{ URL::asset('assets/libs/dropzone/dropzone.min.js') }}"></script>
    <!-- Init js -->
    <script src="{{ URL::asset('assets/js/webt.js') }}"></script>
   <script>
       $(document).on('click','.approve_vendor',function(e) {
            e.preventDefault();
            var id = $(this).attr('data-id');
            var id_user = $(this).attr('data-iduser');
            var id_approve = $(this).attr('data-idapprove');
            const __this = this;
            $(__this).prop('disabled', true);
            const __token = $('meta[name="__token"]').attr('content');
            data_ = {
                id_user: id_user,
                id_approve: id_approve,
                _token: __token
            }
            url_ = $(this).attr('data-url');
            
            var request = $.ajax({
                url: url_,
                type: 'POST',
                data: data_,
                dataType: "json"
            });
            request.done(function (msg) {
               if(msg.type == 1){
                $('.name_approved').html(msg.name_approve);
                $('.date_approved').html(msg.vendor_request.date_approve);
                $('.status_approved').children('a').removeClass('btn-danger');
                $('.status_approved').children('a').addClass('btn-primary');
                $('.status_approved').children('a').html('Approved');
                $(__this).parent('td').html('');
                toastr.success(msg.mess);
               }
                return false;
            });
            request.fail(function (jqXHR, textStatus) {
                alert("Không thể gửi yêu cầu mã lỗi : " + textStatus);
            });
       });
   </script>
@endpush
