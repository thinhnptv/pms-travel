@extends('base::layouts.master')
@section('css')
        <!-- Plugins css-->
        <link href="{{ URL::asset('assets/libs/select2/select2.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ URL::asset('assets/libs/summernote/summernote.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ URL::asset('assets/libs/dropzone/dropzone.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ URL::asset('assets/css/webt.css')}}" rel="stylesheet" type="text/css" />
@endsection
@section('content')
    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">UBold</a></li>
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Apps</a></li>
                            <li class="breadcrumb-item active">Add Customer Review</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Add Customer Review</h4>
                </div>
            </div>
        </div>
        {{-- @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif --}}
        <!-- end page title -->
        <form action="{{ route('customer_review.store') }}" method="post" enctype="multipart/form-data">
            <input type="hidden" name="_token" value="{{csrf_token()}}" /> 
            <div class="row">
                <div class="col-md-12 col-lg-9">
                    <div class="card p-2">
                        <div class="row">
                            <div class="col-md-12 col-lg-6">
                                <div class="form-group mb-3">
                                    <label for="name">Name</label>
                                    <span class="text-danger">*</span>
                                    <input type="text" id="name" name="name" class="form-control" placeholder="Please enter name" value="{{ old('name') }}">
                                    <p class="help is-danger">{{ $errors->first('name') }}</p>
                                </div> 
                                <div class="form-group mb-3">
                                    <label for="biographical">Content</label>
                                    <span class="text-danger">*</span>
                                    <textarea name="biographical" id="biographical" cols="30" rows="10">{{ old('biographical') }}</textarea>
                                    <p class="help is-danger">{{ $errors->first('biographical') }}</p>
                                </div> 
                            </div>
                        </div>     
                    </div>
                </div>
                <div class="col-md-12 col-lg-3">
                    <div class="card p-2">
                        <div class="card-box text-center">
                            <label for="">Avatar <span class="text-danger">*</span></label> <br/>
                            <span class="input-group-btn">
                                <button type="button" class="btn btn-warning" data-toggle="modal" data-target=".bd-example-modal-image">Select avatar</button>
                                <input type="hidden" name="avatar" id="avatar" required value="{{ old('avatar') }}">
                            </span>
                            <div id="preview_avatar" class="mt-3">
                                       
                            </div>
                            <p class="help is-danger">{{ $errors->first('avatar') }}</p>
                        </div>
                        <div class="form-group mb-3">
                            <label for="location_id">Location</label>
                            <select class="form-control" id="location_id" name="location_id">
                                @foreach ($listLocation as $item)
                                    <option value="{{ $item->id }}"
                                        @if (old('location_id') == $item->id)
                                            selected
                                        @endif
                                        >{{ $item->name }}</option>
                                @endforeach
                            </select>
                        </div> 
                    </div>
                </div>
            </div>
            
            <div class="row">
                <div class="col-12">
                    <div class="text-center mb-3">
                        <button type="submit" class="btn w-sm btn-success waves-effect waves-light">Save</button>
                        <a href="{{ route('customer_review.list') }}" class="btn w-sm btn-warning waves-effect waves-light">Cancel</a>
                    </div>
                </div> <!-- end col -->
            </div>
        <!-- end row -->
        </form>
    </div>
@section('js')
    <div class="modal fade bd-example-modal-image" id="modal-file" tabindex="-1" role="dialog" data-backdrop="false" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Manger Image</h5>
                    <button type="button" class="close" data-dismiss="modal"><i class="fas fa-times"></i></button>
                </div>
                <div class="modal-body">
                    <iframe src="{{ config('app.url') }}/filemanager/dialog.php?akey=4Ans6pfTs6A9DzHj8AZq1NVGKoYxpyoI8ivCN1bOow&field_id=avatar&multiple=0" frameborder="0" style="width: 100%; height: 500px; overflow-y: auto"></iframe>
                </div>
            </div>
        </div>
    </div>
@stop
@endsection

@push('js')
    <!-- Summernote js -->
    <script src="{{ URL::asset('assets/libs/summernote/summernote.min.js') }}"></script>
    <!-- Select2 js-->
    <script src="{{ URL::asset('assets/libs/select2/select2.min.js') }}"></script>
    <!-- Dropzone file uploads-->
    <script src="{{ URL::asset('assets/libs/dropzone/dropzone.min.js') }}"></script>
    <!-- Init js -->
    <script src="{{ URL::asset('assets/js/webt.js') }}"></script>

    <script>
        $(document).on('click', '.style_icon_remove', function() {
            $(this).parent('.box_imgg').hide('slow', function () { 
                $(this).remove(); 
                $('#avatar').val('');
            });
        });
       $(document).on('hide.bs.modal', '.bd-example-modal-image', function () {
            var _img = $('input#avatar').val();
            if (!_img.length) {
                $('#preview_avatar').empty();
            } else {
                $('#preview_avatar').empty();
                $html = `
                    <div class="box_imgg position-relative">
                        <img src="" id='show-img-avatar' style="width:100%;">
                        <i class="fas fa-times-circle style_icon_remove style_icon_remove_avatar" title="delete"></i>
                    </div>
                `;
                $('#preview_avatar').append($html);
                $('#show-img-avatar').attr('src', _img);
            }
        });
    </script>
@endpush

