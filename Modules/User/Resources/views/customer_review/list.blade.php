@extends('base::layouts.master')
@section('css')
        <!-- Plugins css-->
        <link href="{{ URL::asset('assets/libs/select2/select2.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ URL::asset('assets/libs/summernote/summernote.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ URL::asset('assets/libs/dropzone/dropzone.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ URL::asset('assets/css/webt.css')}}" rel="stylesheet" type="text/css" />
@endsection
@section('content')
    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">UBold</a></li>
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Apps</a></li>
                            <li class="breadcrumb-item active">List Customer Review</li>
                        </ol>
                    </div>
                    <h4 class="page-title">List Customer Review</h4>
                </div>
            </div>
        </div>
        <div class="notification">
            @if (session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif
        </div>
       
        <!-- end page title -->
    <div class="row">
        <div class="col-12">
            <div class="card p-2">
                <div class="row mb-2">
                    <div class="col-sm-4">
                        {{-- <select name="role" id="" class="form-control filter_role" onchange="search(this)">
                            <option value="0">chọn vai trò</option>
                            @foreach($all_role as $item)
                                <option value="{{ $item->id }}">{{ $item->name }}</option>
                            @endforeach
                           
                        </select> --}}
                    </div>
                    <div class="col-sm-8">
                        <div class="text-sm-right">
                            
                            {{-- <a href="javascript:void(0)" class="btn btn-danger waves-effect waves-light mb-2 delete_all" ><i class="mdi mdi-delete mr-1"></i> Delete All</a> --}}
                            <a href="{{ route('customer_review.create') }}" class="btn btn-primary waves-effect waves-light mb-2" ><i class="mdi mdi-plus-circle mr-1"></i> Add</a>
                        </div>
                    </div><!-- end col-->
                </div>
                <div class="table-responsive">
                    <table class="table table-bordered  mb-0">
                        <thead>
                            <tr>
                                <th width="50px"><input type="checkbox" id="master" class="check_master"></th>
                                <th class="text-center">STT</th>
                                <th class="text-center">Name</th>
                                <th class="text-center">Avatar</th>
                                <th class="text-center">#</th>
                                <th class="text-center">#</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                                $stt = 1;
                            @endphp
                            @foreach ($listCustomerReview as $item)
                            <tr>
                                <td><input type="checkbox" class="sub-chk" data-id="{{ $item->id }}"></td>
                                <th class="text-center" scope="row">{{ $stt }}</th>
                                <td>{{ $item->name }}</td>
                                <td><img src="{{ config('app.url') }}{{ $item->avatar }}" alt="" width="100px"></td>
                                <td class="text-center">
                                    <a href="{{ route('customer_review.edit', $item->id) }}" class="btn btn-info">Edit</a>
                                </td>
                                <td class="text-center">
                                <a href="{{ route('customer_review.destroy', $item->id) }}" class="btn btn-danger" onclick="return confirm('Bạn chắc chắn muốn xoá?')">Delete</a>
                                </td>
                            </tr>
                            @php
                                $stt++;
                            @endphp
                            @endforeach
                        </tbody>
                    </table>
                    <div class="mt-2">
                        {{ $listCustomerReview->render() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('js')
    <!-- Summernote js -->
    <script src="{{ URL::asset('assets/libs/summernote/summernote.min.js') }}"></script>
    <!-- Select2 js-->
    <script src="{{ URL::asset('assets/libs/select2/select2.min.js') }}"></script>
    <!-- Dropzone file uploads-->
    <script src="{{ URL::asset('assets/libs/dropzone/dropzone.min.js') }}"></script>
    <!-- Init js -->
    <script src="{{ URL::asset('assets/js/webt.js') }}"></script>
    
    <script>
        $( document ).ready(function() {
            var checkNotification =  $('.notification').find('.alert-success').length;
            if(checkNotification != 0) {
                    setTimeout(function(){
                        $('.notification').empty();
                    }, 3000);
            }
        });
         $(document).on('click','.check_master', function(e) {
            if($(this).is(':checked',true))
            {
                $(".sub-chk").prop('checked', true);
            } else {
                $(".sub-chk").prop('checked',false);
            }
        });
        
        $(document).on('click','.page-link',function(e) {
            e.preventDefault();
            url_ = this.href;
            var url_string = location.href;
            var url = new URL(url_string);
            var role = url.searchParams.get("role");
            if(role != null){
                url_ = url_ + '&role='+role;
            }
            window.location.href=url_;
        })
    </script>
    <script>
        $( document ).ready(function() {
            var url_string = location.href;
            var url = new URL(url_string);
            var role = url.searchParams.get("role");
            if(role != null){
                $('.filter_role').val(role);
            }
             
         });
     </script>
@endpush
