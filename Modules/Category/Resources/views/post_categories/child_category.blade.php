<li>{{ $child_category->name }}</li>
@if ($child_category->subcategories)
    <ul>
        @foreach ($child_category->subcategories as $subchild)
            @include('child_category', ['child_category' => $subchild])
        @endforeach
    </ul>
@endif
