@extends('base::layouts.master')
@section('content')
    <!-- Start Content-->
    <div class="container-fluid">
        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">UBold</a></li>
                            <li class="breadcrumb-item"><a href="javascript: void(0);">News</a></li>
                            <li class="breadcrumb-item active">Category</li>
                        </ol>
                    </div>
                    <h4 class="page-title">News Categories</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->
        <div class="row">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="card-box">
                            <form method="post" action="{{ route('postCategories.store') }}">
                                @csrf
                                <div class="form-group mb-3">
                                    <label for="name"> Name <span class="text-danger">*</span></label>
                                    <input type="text" id="name" name="name" class="form-control"
                                           placeholder="Category name">
                                    @if($errors->has('name'))
                                        <p class="alert alert-danger">{{ $errors->first('name') }}</p>
                                    @endif
                                </div>
                                <div class="form-group mb-3">
                                    <label for="product-reference">Parent</label>
                                    <select type="text" id="parent_id" name="parent_id" class="form-control cate">
                                        <option value="">-- Select --</option>
                                        {{ category_parent($postCategories) }}
                                    </select>
                                </div>
                                <div class="form-group mb-3">
                                    <label for="name"> Slug </label>
                                    <input type="text" id="slug" name="slug" class="form-control"
                                           placeholder="Category name">
                                </div>
                                <div class="mb-3">
                                    <button type="submit" class="btn w-sm btn-primary waves-effect waves-light">Add
                                        new
                                    </button>
                                </div>
                            </form>
                        </div> <!-- end card-box -->
                    </div> <!-- end col -->
                    <div class="col-lg-8">
                        <form class="form-delete" action="{{ route('postCategories.delete_multiple') }}" method="post">
                            @csrf
                            @method('DELETE')
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="card-box">
                                        <h4 class="header-title">Custom Toolbar</h4>
                                        <p class="sub-header">
                                            Example of custom toolbar.
                                        </p>
                                        <div class="columns-left" style="float: left">
                                            <button style="margin-bottom: 10px" class="btn btn-danger delete_all">
                                                Delete
                                            </button>
                                        </div>
                                        <div class="columns-left" style="float: right">
                                            <form>
                                                <div class="form-inline">
                                                    <label for="inputPassword2" class="sr-only">Password</label>
                                                    <input type="text" class="form-control" id="inputPassword2"
                                                           placeholder="search">
                                                    <button type="submit" class="btn btn-info ">Search Category</button>
                                                </div>
                                            </form>
                                        </div>
                                        <table class="table table-bordered">
                                            <tr>
                                                <th width="50px"><input type="checkbox" class="selectall" id="master">
                                                </th>
                                                <th>Name</th>
                                                <th>Slug</th>
                                                <th>Status</th>
                                                <th>Date</th>
                                            </tr>
                                            {{--@if(isset($postCategories))--}}
                                                {{--@foreach($postCategories as $key => $val)--}}
                                                {{--<tr>--}}
                                                    {{--<td>--}}
                                                        {{--<input type='checkbox' class='sub-chk' name='ids[]' value='{{ $val->id }}'>--}}
                                                    {{--</td>--}}

                                                    {{--<td>--}}
                                                        {{--<a href='{{ route('postCategories.edit', $val->id) }}'>{{ $val->name }}</a>--}}
                                                        {{--@foreach ($val->subcategories as $childCategory)--}}
                                                            {{--<ul>--}}
                                                            {{--<li>--}}
                                                                {{--<a href='{{ route('postCategories.edit', $childCategory->id) }}'>{{ $childCategory->name }}</a>--}}
                                                            {{--</li>--}}
                                                        {{--<ul>--}}
                                                            {{--@foreach($childCategory->childrenCategories as $child)--}}
                                                                {{--@include('module-category::child_category', ['child_category' => $child])--}}
                                                            {{--@endforeach--}}
                                                        {{--</ul>--}}
                                                            {{--</ul>--}}
                                                        {{--@endforeach--}}
                                                    {{--</td>--}}

                                                    {{--<td>--}}
                                                        {{--{{ $val->slug }}--}}
                                                    {{--</td>--}}
                                                    {{--@if($val->status == 1)--}}
                                                    {{--<td>--}}
                                                        {{--<span class='badge badge-success'>Publish</span>--}}
                                                    {{--</td>--}}
                                                    {{--@else--}}
                                                    {{--<td>--}}
                                                        {{--<span class='badge badge-dark'>Draft</span>--}}
                                                    {{--</td>--}}
                                                    {{--@endif--}}
                                                    {{--<td>--}}
                                                        {{--{{ $val->created_at->format('d-m-Y') }}--}}
                                                    {{--</td>--}}
                                                {{--</tr>--}}
                                                {{--@endforeach--}}
                                            {{--@endif--}}
                                            @if($postCategories->count())
                                                {{ showPostCategories($postCategories) }}
                                            @endif
                                        </table>
                                    </div> <!-- end card-box-->
                                </div> <!-- end col-->
                            </div>
                        </form>
                    </div> <!-- end col-->
                </div>
                <!-- end row -->
            </div> <!-- container -->
        </div>
    </div>
    <div class="modal justify-content-center" id="confirm">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <p>Bạn có chắc chắn muốn xóa</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-sm btn-primary" id="delete-btn">Delete</button>
                    <button type="button" class="btn btn-sm btn-dark" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>

@stop
@section('script-bottom')
    {!! Toastr::message() !!}
    <script>

        $(document).ready(function () {

            $(document).on("keyup", "#name", function () {
                let $this = $(this);
                let slug;
                let post_name = $this.val();

                // Đổi chữ hoa thành chữ thường
                slug = post_name.toLowerCase();

                //Đổi ký tự có dấu thành không dấu
                slug = slug.replace(/á|à|ả|ạ|ã|ă|ắ|ằ|ẳ|ẵ|ặ|â|ấ|ầ|ẩ|ẫ|ậ/gi, 'a');
                slug = slug.replace(/é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ/gi, 'e');
                slug = slug.replace(/i|í|ì|ỉ|ĩ|ị/gi, 'i');
                slug = slug.replace(/ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ/gi, 'o');
                slug = slug.replace(/ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự/gi, 'u');
                slug = slug.replace(/ý|ỳ|ỷ|ỹ|ỵ/gi, 'y');
                slug = slug.replace(/đ/gi, 'd');
                //Xóa các ký tự đặt biệt
                slug = slug.replace(/\`|\~|\!|\@|\#|\||\$|\%|\^|\&|\*|\(|\)|\+|\=|\,|\.|\/|\?|\>|\<|\'|\"|\:|\;|_/gi, '');
                //Đổi khoảng trắng thành ký tự gạch ngang
                slug = slug.replace(/ /gi, "-");
                //Đổi nhiều ký tự gạch ngang liên tiếp thành 1 ký tự gạch ngang
                //Phòng trường hợp người nhập vào quá nhiều ký tự trắng
                slug = slug.replace(/\-\-\-\-\-/gi, '-');
                slug = slug.replace(/\-\-\-\-/gi, '-');
                slug = slug.replace(/\-\-\-/gi, '-');
                slug = slug.replace(/\-\-/gi, '-');
                //Xóa các ký tự gạch ngang ở đầu và cuối
                slug = '@' + slug + '@';
                slug = slug.replace(/\@\-|\-\@|\@/gi, '');
                $('#slug').val(slug);
            });

            $(document).on('click', '.delete_all', function (e) {
                e.preventDefault();
                var $form = $('.form-delete');
                $('#confirm').modal({backdrop: 'static', keyboard: false})
                    .on('click', '#delete-btn', function () {
                        $form.submit();
                    });
            });

            $('.selectall').click(function () {
                $('.sub-chk').prop('checked', $(this).prop('checked'));
            });

            $('.sub-chk').change(function () {
                var total = $('.sub-chk').length;
                var number = $('.sub-chk:checked').length;
                if (total == number) {
                    $('.selectall').prop('checked', true);
                } else {
                    $('.selectall').prop('checked', false);
                }
            });

        });
    </script>










@endsection


