@foreach($spaceAttributes as $space_attribute)
    <tr>
        <td><input type='checkbox' class='sub-chk' name='ids[]'
                    value='{{ $space_attribute->id }}'></td>
        <td>
            <a href="{{ route('spaceAttributes.edit', $space_attribute->id) }}">{{ $space_attribute->name }}</a>
        </td>
        <td>
            <a href="{{ route('spaceAttributes.edit', $space_attribute->id) }}"
                class="btn btn-primary btn-sm">Edit</a>
            <a href="{{ route('spaceAttributes.show_all_term', $space_attribute->id) }}"
                class="btn btn-success btn-sm">Manage Terms</a>
        </td>
    </tr>
@endforeach
