
@extends('base::layouts.master')

@section('content')
    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">UBold</a></li>
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Space</a></li>
                            <li class="breadcrumb-item active">Attribute</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Spaces Attributes</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->
        <div class="row">
            <div class="container-fluid">

                <!-- start page title -->
                <!-- end page title -->


                <div class="row">
                    <div class="col-lg-4">
                        <div class="card-box">
                            <form method="post" action="{{ route('spaceAttributes.store') }}">
                                @csrf
                                <div class="form-group mb-3">
                                    <label for="product-name"> Name <span class="text-danger">*</span></label>
                                    <input type="text" id="slug-source" name="name" class="form-control"
                                           placeholder="Category name">
                                    @if($errors->has('name'))
                                        <p class="alert alert-danger">{{ $errors->first('name') }}</p>
                                    @endif
                                </div>

                                <div class="mb-3">
                                    <button type="submit" class="btn w-sm btn-primary waves-effect waves-light">
                                        Add new
                                    </button>
                                    <a href="{{ route('spaceAttributes.list') }}"><button type="button" id="button-hide" class="btn w-sm btn-danger waves-effect waves-light">
                                        Cancel
                                    </button></a>
                                </div>
                            </form>
                        </div> <!-- end card-box -->
                    </div> <!-- end col -->

                    <div class="col-lg-8">
                        <form class="form-delete" action="{{ route('spaceAttributes.delete_multiple') }}" method="post">
                            @csrf
                            @method('DELETE')
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="card-box">
                                        <h4 class="header-title">All Attributes</h4>
                                        <div class="columns-left" style="float: left">
                                            <button style="margin-bottom: 10px" class="btn btn-danger delete_all">
                                                Delete
                                            </button>
                                        </div>
                                        <div class="columns-left" style="float: right">
                                                <div class="form-inline">
                                                    <input type="text" class="form-control" name="search" id="search"
                                                           placeholder="Search name attibute ">
                                                </div>
                                        </div>
                                        <table class="table table-bordered" id="testing">
                                            <thead class="thead-light">
                                            <tr>
                                                <th width="50px"><input type="checkbox" class="selectall" id="master">
                                                </th>
                                                <th>Name</th>
                                                <th>Action</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @if($spaceAttributes->count() > 0)
                                                @foreach($spaceAttributes as $space_attribute)
                                                    <tr>
                                                        <td><input type='checkbox' class='sub-chk' name='ids[]'
                                                                   value='{{ $space_attribute->id }}'></td>
                                                        <td>
                                                            <a href="{{ route('spaceAttributes.edit', $space_attribute->id) }}">{{ $space_attribute->name }}</a>
                                                        </td>
                                                        <td>
                                                            <a href="{{ route('spaceAttributes.edit', $space_attribute->id) }}"
                                                               class="btn btn-primary btn-sm">Edit</a>
                                                            <a href="{{ route('spaceAttributes.show_all_term', $space_attribute->id) }}"
                                                               class="btn btn-success btn-sm">Manage Terms</a>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            @endif
                                            </tbody>
                                        </table>
                                    </div> <!-- end card-box-->
                                </div>
                                <!-- end col-->
                            </div>
                        </form>
                    </div> <!-- end col-->
                </div>
                <!-- end row -->
            </div> <!-- container -->
        </div>
    </div>
    <div class="modal justify-content-center" id="confirm">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <p>Are you sure you, want to delete?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-sm btn-primary" id="delete-btn">Delete</button>
                    <button type="button" class="btn btn-sm btn-dark" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
@stop

@section('script-bottom')

    {!! Toastr::message() !!}

    <script>
        $(document).on('keyup','#search',function(){
                var query = $(this).val();
                search_attribute(query);
            });
            function search_attribute(query = ' '){
                $.ajax({
                    type: "GET",
                    url: "{{ route('spaceAttributes.search_attribute') }}",
                    data: {query:query},
                    //dataType: "json",
                    success: function (data) {
                        $('tbody').html(data)
                    }
            });
        }

        $(document).on('click', '.delete_all', function (e) {
            e.preventDefault();
            var $form = $('.form-delete');
            $('#confirm').modal({backdrop: 'static', keyboard: false})
                .on('click', '#delete-btn', function () {
                    $form.submit();
                });
            // $('#confirmDeleteComment').modal('show');

            // do anything else you need here
        });

    </script>
    <script>
        $('.selectall').click(function () {
            $('.sub-chk').prop('checked', $(this).prop('checked'));
        })
        $('.sub-chk').change(function () {
            var total = $('.sub-chk').length;
            var number = $('.sub-chk:checked').length;
            if (total == number) {
                $('.selectall').prop('checked', true);
            } else {
                $('.selectall').prop('checked', false);
            }
        })
    </script>

@endsection

