@extends('base::layouts.master')
@section('css')
    <!-- Bootstrap Tables css -->
    <link href="{{ URL::asset('assets/libs/bootstrap-table/bootstrap-table.min.css')}}" rel="stylesheet"
          type="text/css"/>
@endsection
@section('content')
    <!-- Start Content-->
    <div class="container-fluid">
        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">UBold</a></li>
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Apps</a></li>
                            <li class="breadcrumb-item active">Kanban Board</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Edit: {{ $tourCategory->name }}</h4>
                    </p>
                </div>
            </div>
        </div>
        <!-- end page title -->
        <form action="{{ route('tourCategories.update', $tourCategory->id) }}" method="post">
            @csrf
            <div class="row">
                <div class="col-lg-9">
                    <div class="card-box">
                        <h5 class=" bg-light p-2 mt-0 mb-3">Category content</h5>
                        <div class="form-group mb-3">
                            <label for="product-name"> Name</label>
                            <input type="text" id="slug-source" name="name" class="form-control"
                                   value="{{ $tourCategory->name }}">
                            @if($errors->has('name'))
                                <p class="alert alert-danger">{{ $errors -> first('name') }}</p>
                            @endif
                        </div>
                        <div class="form-group mb-3">
                            <label for="product-category">Parent</label>
                            <select class="form-control select2" name="parent_id" id="product-category">
                                <option value="">-- Select --</option>
                                <?php  category_parent($parentCategories)?>
                            </select>
                        </div>
                        <div class="form-group mb-3">
                            <label for="product-name">Slug</label>
                            <input type="text" id="slug-target" name="slug" class="form-control"
                                   value="{{ $tourCategory->slug }}">
                            @if($errors->has('slug'))
                                <p class="alert alert-danger">{{ $errors->first('slug') }}</p>
                            @endif
                        </div>
                    </div> <!-- end card-box -->
                </div> <!-- end col -->
                <div class="col-lg-3">
                    <div class="card-box">
                        <div class="col-md-12">
                            <h4 class="header-title mt-5 mt-sm-0">Publish</h4>
                            <div class="mt-3">
                                <div class="custom-control custom-radio">
                                    {{--                                {{dd($postCategory->status)}}--}}
                                    <input type="radio" id="customRadio1" value="1" name="status"
                                           {{ ($tourCategory->status===1) ? "checked" : "" }} class="custom-control-input">
                                    <label class="custom-control-label" for="customRadio1">Publish</label>
                                </div>
                            </div>
                            <div class="mt-3">
                                <div class="custom-control custom-radio">
                                    <input type="radio" id="customRadio2" value="0" name="status"
                                           {{ ($tourCategory->status===0) ? "checked" : "" }} class="custom-control-input">
                                    <label class="custom-control-label" for="customRadio2">Draft</label>
                                </div>
                            </div>
                            <div class=" mt-3">
                                <button type="submit" class="btn w-sm btn-success waves-effect waves-light">Save
                                </button>
                            </div>
                        </div>
                    </div> <!-- end col -->
                </div> <!-- end col-->
            </div>
            <!-- end col-->
        </form>
    </div>
    <!-- end row -->
    <!-- end row -->
@stop

@section('script')
    <script>
        $(document).ready(function () {
            $('#slug-source').keyup(function () {
                var str = $(this).val();
                var trims = $.trim(str);
                var slug = trims.replace(/[^a-z0-9]/gi, '-').replace(/-+/g, '-').replace(/^-|-$/g, '')
                $('#slug-target').val(slug.toLowerCase())
            })
        });


    </script>
    <!-- Plugins js-->
    <script src="{{ URL::asset('assets/libs/twitter-bootstrap-wizard/twitter-bootstrap-wizard.min.js')}}"></script>

    <!-- Init js-->
    <script src="{{ URL::asset('assets/js/pages/form-wizard.init.js')}}"></script>

@endsection
