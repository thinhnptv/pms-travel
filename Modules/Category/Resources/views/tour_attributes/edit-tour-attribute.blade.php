@extends('base::layouts.master')
@section('css')
    <!-- Bootstrap Tables css -->
    <link href="{{ URL::asset('assets/libs/bootstrap-table/bootstrap-table.min.css')}}" rel="stylesheet"
          type="text/css"/>
@endsection
@section('content')
    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-lg-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">UBold</a></li>
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Apps</a></li>
                            <li class="breadcrumb-item active">Tour</li>
                            <li class="breadcrumb-item active">Attributes</li>
                            <li class="breadcrumb-item active">Attribute:{{ $tourAttribute->name }} </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-lg-6">
                <h2 class="page-title">Edit: {{ $tourAttribute->name }}</h2>
            </div>
        </div>
        <!-- end page title -->
        <form action="{{ route('tourAttributes.update', $tourAttribute->id) }}" method="post">
            @csrf
            <div class="row justify-content-center">
                <div class="col-lg-6">
                    <div class="card-box">
                        <div class="col-xl-12">
                            <div class="card">
                                <div class="card-body">
                                    <form>
                                        <div id="basicwizard">
                                            <ul class="nav nav-pills bg-light nav-justified form-wizard-header mb-4">
                                                <li class="nav-item">
                                                    <a href="#basictab1" data-toggle="tab"
                                                       class="nav-link rounded-0 pt-2 pb-2 activate">
                                                        <i class="mdi mdi-account-circle mr-1"></i>
                                                        <span class="d-none d-sm-inline">English</span>
                                                    </a>
                                                </li>
                                                <li class="nav-item">
                                                    <a href="#basictab2" data-toggle="tab"
                                                       class="nav-link rounded-0 pt-2 pb-2">
                                                        <i class="mdi mdi-face-profile mr-1"></i>
                                                        <span class="d-none d-sm-inline">Japanese</span>
                                                    </a>
                                                </li>
                                                <li class="nav-item">
                                                    <a href="#basictab3" data-toggle="tab"
                                                       class="nav-link rounded-0 pt-2 pb-2">
                                                        <i class="mdi mdi-checkbox-marked-circle-outline mr-1"></i>
                                                        <span class="d-none d-sm-inline">Egyptian</span>
                                                    </a>
                                                </li>
                                            </ul>
                                            <h5 class=" bg-light p-2 mt-0 mb-3 text-center">Attribute Content</h5>
                                            <div class="tab-content b-0 mb-0">
                                                <div class="tab-pane" id="basictab1">
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="form-group row mb-3">
                                                                <label class="col-md-3 col-form-label" for="userName">Name</label>
                                                                <div class="col-md-9">
                                                                    <input type="text" class="form-control" id="userName"
                                                                           name="name" value="{{ $tourAttribute->name }}">
                                                                    @if($errors->has('name'))
                                                                        <p class="alert alert-danger">{{ $errors->first('name') }}</p>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                        </div> <!-- end col -->
                                                    </div> <!-- end row -->
                                                </div>
                                                <div class="tab-pane" id="basictab2">
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="form-group row mb-3">
                                                                <label class="col-md-3 col-form-label" for="userName">Name</label>
                                                                <div class="col-md-9">
                                                                    <input type="text" class="form-control" id="userName"
                                                                           name="" value="{{ $tourAttribute->name }}">
                                                                </div>
                                                            </div>
                                                        </div> <!-- end col -->
                                                    </div> <!-- end row -->
                                                </div>
                                                <div class="tab-pane" id="basictab3">
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="form-group row mb-3">
                                                                <label class="col-md-3 col-form-label" for="userName">Name</label>
                                                                <div class="col-md-9">
                                                                    <input type="text" class="form-control" id="userName"
                                                                           name="" value="{{ $tourAttribute->name }}">
                                                                </div>
                                                            </div>
                                                        </div> <!-- end col -->
                                                    </div> <!-- end row -->
                                                </div>
                                            </div> <!-- tab-content -->
                                        </div> <!-- end #basicwizard-->
                                    </form>
                                </div> <!-- end card-body -->
                            </div> <!-- end card-->
                        </div> <!-- end col -->
                    </div>
                    <div style="margin-bottom: 20px; float: right">
                        <button  class="btn btn-primary" type="submit">Save Change</button>
                    </div>
                </div> <!-- end col -->
            </div>
            <!-- end col-->
        </form>
    </div>
    <!-- end row -->
    <!-- end row -->
@stop

@section('script-bottom')
    {!! Toastr::message() !!}

    <!-- Plugins js-->
    <script src="{{ URL::asset('assets/libs/twitter-bootstrap-wizard/twitter-bootstrap-wizard.min.js')}}"></script>
    <!-- Init js-->
    <script src="{{ URL::asset('assets/js/pages/form-wizard.init.js')}}"></script>

@endsection
