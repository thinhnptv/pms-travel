@extends('base::layouts.master')

@section('content')
    <!-- Start Content-->
    <div class="container-fluid">
        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">UBold</a></li>
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Room</a></li>
                            <li class="breadcrumb-item active">Attribute</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Room Attributes</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->
        <div class="row">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="card-box">
                            <form method="post" action="{{route('roomAttributes.store')}}">
                                @csrf
                                <h5>Add Attributes</h5>
                                <hr>
                                <div class="form-group mb-3">
                                    <label for="product-name"> Name <span class="text-danger">*</span></label>
                                    <input type="text" id="slug-source" name="name" class="form-control"
                                           placeholder="Attribute name">
                                    @if($errors->has('name'))
                                        <p class="alert alert-danger">{{ $errors -> first('name') }}</p>
                                    @endif
                                </div>
                                <div class="mb-3">
                                    <button type="submit" class="btn w-sm btn-primary waves-effect waves-light">Add new</button>
                                </div>
                            </form>
                        </div> <!-- end card-box -->
                    </div> <!-- end col -->
                    <div class="col-lg-8">
                        
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="card-box">
                                        <h4 class="header-title">All Attributes</h4>
                                        <div class="columns-left" style="float: left">
                                            <button style="margin-bottom: 10px" class="btn btn-danger delete_all">
                                                Delete
                                            </button>
                                        </div>
                                        <div class="columns-left" style="float: right">
                                            <form action="{{route('roomAttributes.list')}}" method="GET" >
                                                <div class="form-inline">
                                                    <input type="text" class="form-control" id="name" name="name" placeholder="search" value="{{Request::get('name')}}">
                                                    <button type="submit" class="btn btn-info ">Search Category</button>
                                                </div>
                                            </form>
                                        </div>
                                        {{--                                    <button id="demo-delete-row" class="btn btn-danger btn-sm" disabled><i class="mdi mdi-close mr-1"></i>Delete</button>--}}
                                        <form class="form-delete" action="{{route('roomAttributes.delete_multiple')}}" method="post">
                                            @csrf
                                            @method('DELETE')
                                        <table class="table table-bordered" id="testing">
                                            <tr>
                                                <th width="50px"><input type="checkbox" class="selectall" id="master">
                                                </th>
                                                <th>Name</th>
                                                <th>Action</th>
                                            </tr>
                                            @if($roomAttributes->count()>0)
                                                @foreach($roomAttributes as $room_attribute)
                                                    <tr>
                                                        <td><input type='checkbox' class='sub-chk' name='ids[]'
                                                                   value='{{$room_attribute->id}}'></td>
                                                        <td>
                                                            <a href="{{route('roomAttributes.edit',$room_attribute->id)}}">{{$room_attribute->name}}</a>
                                                        </td>
                                                        <td>
                                                            <a href="{{route('roomAttributes.edit',$room_attribute->id)}}"
                                                               class="btn btn-primary btn-sm">Edit</a>
                                                            <a href="{{ route('roomAttributes.show_all_term',$room_attribute->id) }}"
                                                               class="btn btn-success btn-sm">Manage Terms</a>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            @endif
                                        </table>
                                    </div> <!-- end card-box-->
                                </div>
                                <!-- end col-->
                            </div>
                        </form>
                    </div> <!-- end col-->
                </div>
                <!-- end row -->
            </div> <!-- container -->
        </div>
    </div>
    <div class="modal justify-content-center" id="confirm">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <p>Are you sure you, want to delete?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-sm btn-primary" id="delete-btn">Delete</button>
                    <button type="button" class="btn btn-sm btn-dark" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
@stop
@section('script-bottom')

    {!! Toastr::message() !!}

    <script>
        $(document).on('click', '.delete_all', function (e) {
            e.preventDefault();
            var $form = $('.form-delete');
            $('#confirm').modal({backdrop: 'static', keyboard: false})
                .on('click', '#delete-btn', function () {
                    $form.submit();
                });
            // $('#confirmDeleteComment').modal('show');

            // do anything else you need here
        });

    </script>
    <script>
        $('.selectall').click(function () {
            $('.sub-chk').prop('checked', $(this).prop('checked'));
        })
        $('.sub-chk').change(function () {
            var total = $('.sub-chk').length;
            var number = $('.sub-chk:checked').length;
            if (total == number) {
                $('.selectall').prop('checked', true);
            } else {
                $('.selectall').prop('checked', false);
            }
        })
    </script>
@endsection
