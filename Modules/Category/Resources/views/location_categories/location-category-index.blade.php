@extends('base::layouts.master')
@section('css')
    <!-- Bootstrap Tables css -->
    <link href="{{ URL::asset('assets/libs/bootstrap-table/bootstrap-table.min.css')}}" rel="stylesheet"
          type="text/css"/>
    <link href="{{ URL::asset('assets/libs/quill/quill.min.css')}}" rel="stylesheet" type="text/css"/>

    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection
@section('content')
    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">UBold</a></li>
                            <li class="breadcrumb-item"><a href="javascript: void(0);">News</a></li>
                            <li class="breadcrumb-item active">Category</li>
                        </ol>
                    </div>
                    <h4 class="page-title">News Locations</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->
        <div class="row">
            <div class="container-fluid">

                <!-- start page title -->
                <!-- end page title -->


                <div class="row">
                    <div class="col-lg-4">
                        <div class="card-box">
                            <form method="post" action="{{ route('locationCategories.store') }}">
                                @csrf
                                <div class="form-group mb-3">
                                    <label for="name"> Name <span class="text-danger">*</span></label>
                                    <input type="text" id="name" name="name" class="form-control"
                                           placeholder="Category name">
                                    @if($errors->has('name'))
                                        <p class="alert alert-danger">{{ $errors->first('name') }}</p>
                                    @endif
                                </div>


                                <div class="form-group mb-3">
                                    <label for="product-reference">Parent <span class="text-danger">*</span></label>
                                    <select type="text" id="parent_id" name="parent_id" class="form-control cate">
                                        <option value="">-- Select --</option>
                                        <?php  category_parent($locations)?>
                                    </select>

                                </div>
                                <div class="form-group mb-3">

                                    <h4 class="header-title">Desscription</h4>
                                    <div id="description" style="height: 300px;">

                                    </div>
                                    <textarea class="form-control" name="description" id="description-textarea"
                                              type="textarea" style="display: none" rows="5"></textarea>
                                </div>
                                <div class="form-group mb-3">
                                    <h4 class="header-title mb-3">Basic</h4>
                                    <div id="gmaps-basic" class="gmaps"></div>
                                    <i>Click onto map to place Location address
                                    </i>
                                </div>
                                <div class="form-group mb-3">
                                    <label for="map_lat">map_lat <span class="text-danger">*</span></label>
                                    <input type="text" id="map_lat" name="map_lat" class="form-control"
                                           placeholder="Please enter ">
                                </div>
                                <div class="form-group mb-3">
                                    <label for="map_lng">map_lng <span class="text-danger">*</span></label>
                                    <input type="text" id="map_lng" name="map_lng" class="form-control"
                                           placeholder="Please enter ">
                                </div>
                                <div class="form-group mb-3">
                                    <label for="map_zoom">map_zoom <span class="text-danger">*</span></label>
                                    <input type="text" id="map_zoom" name="map_zoom" class="form-control"
                                           placeholder="Please enter ">
                                </div>

                                <div class="mb-3">
                                    <button type="submit" class="btn w-sm btn-primary waves-effect waves-light">Add
                                        new
                                    </button>
                                </div>
                            </form>
                        </div> <!-- end card-box -->
                    </div> <!-- end col -->

                    <div class="col-lg-8">
                        <form class="form-delete" action="{{ route('locationCategories.delete_multiple') }}"
                              method="post">
                            @csrf
                            @method('DELETE')
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="card-box">
                                        <h4 class="header-title">Custom Toolbar</h4>
                                        <p class="sub-header">
                                            Example of custom toolbar.
                                        </p>
                                        <div class="columns-left" style="float: left">
                                            <button style="margin-bottom: 10px" class="btn btn-danger delete_all">Delete</button>
                                        </div>
                                        <div class="columns-left" style="float: right">
                                            <form>
                                                <div class="form-inline">
                                                    <label for="inputPassword2" class="sr-only">Password</label>
                                                    <input type="text" class="form-control" id="inputPassword2"
                                                           placeholder="search">
                                                    <button type="submit" class="btn btn-info ">Search Category</button>
                                                </div>
                                            </form>
                                        </div>
                                        <table class="table table-bordered">
                                            <tr>
                                                <th width="50px"><input type="checkbox" class="selectall" id="master">
                                                </th>
                                                <th>Name</th>
                                                <th>Slug</th>
                                                <th>Status</th>
                                                <th>Date</th>
                                            </tr>
                                            @if($locations->count())
                                                <?php showLocationCategories($locations); ?>
                                            @endif
                                        </table>
                                    </div> <!-- end card-box-->
                                </div> <!-- end col-->
                            </div>
                        </form>
                    </div> <!-- end col-->
                </div>
                <!-- end row -->
            </div> <!-- container -->
        </div>
    </div>
    <div class="modal justify-content-center" id="confirm">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <p>Bạn có chắc chắn muốn xóa</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-sm btn-primary" id="delete-btn">Delete</button>
                    <button type="button" class="btn btn-sm btn-dark" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>

@stop
@section('script-bottom')
    {!! Toastr::message() !!}

    <script>
        $(document).on('click', '.delete_all', function (e) {
            e.preventDefault();
            var $form = $('.form-delete');
            $('#confirm').modal({backdrop: 'static', keyboard: false})
                .on('click', '#delete-btn', function () {
                    $form.submit();
                });
        });

    </script>
    <script>
        $('.selectall').click(function () {
            $('.sub-chk').prop('checked', $(this).prop('checked'));
        })
        $('.sub-chk').change(function () {
            var total = $('.sub-chk').length;
            var number = $('.sub-chk:checked').length;
            if (total == number) {
                $('.selectall').prop('checked', true);
            } else {
                $('.selectall').prop('checked', false);
            }
        })
    </script>





    <script src="{{ URL::asset('assets/libs/quill/quill.min.js')}}"></script>

    <!-- Init js-->
    <script>
        $(document).ready(function () {
            var quill = new Quill('#description', {
                theme: 'snow'
            });
            quill.on('text-change', function (delta, oldDelta, source) {
                $('#description-textarea').text($(".ql-editor").html());
            });
        });
    </script>


    <script>
        var bookingCore = {
            url: 'http://sandbox.bookingcore.org',
            map_provider: 'gmap',
            map_gmap_key: '',
            csrf: 'JMrM5NwcxQy6HWOmuo7LQ2kHPh7pGwfbOPpXxkue'
        };
        var i18n = {
            warning: "Warning",
            success: "Success",
            confirm_delete: "Do you want to delete?",
            confirm: "Confirm",
            cancel: "Cancel",
        };
    </script>

    <script src="https://maps.google.com/maps/api/js?key=AIzaSyDsucrEdmswqYrw0f6ej3bf4M4suDeRgNA"></script>
    <script src="{{ URL::asset('assets/libs/gmaps/gmaps.min.js') }}"></script>
    <script src="{{ URL::asset('assets/js/pages/google-maps.init.js') }}"></script>
    <script src="{{ URL::asset('assets/js/map-engine.js') }}"></script>
    <script>
        jQuery(function ($) {
            new BravoMapEngine('gmaps-basic', {
                fitBounds: true,
                center: [{{"51.505"}}, {{"-0.09"}}],
                zoom:{{"8"}},
                ready: function (engineMap) {
                    console.log(1);

                    engineMap.on('click', function (dataLatLng) {
                        engineMap.clearMarkers();
                        engineMap.addMarker(dataLatLng, {
                            icon_options: {}
                        });
                        $("input[name=map_lat]").attr("value", dataLatLng[0]);
                        $("input[name=map_lng]").attr("value", dataLatLng[1]);
                    });
                    engineMap.on('zoom_changed', function (zoom) {
                        $("input[name=map_zoom]").attr("value", zoom);
                    })
                }
            });
        })
    </script>

@endsection

