@extends('base::layouts.master')
@section('css')
    <!-- Bootstrap Tables css -->
    <link href="{{ URL::asset('assets/libs/quill/quill.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{ URL::asset('assets/libs/dropzone/dropzone.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{ URL::asset('assets/libs/dropify/dropify.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{ URL::asset('assets/css/webt.css')}}" rel="stylesheet" type="text/css"/>
@endsection
@section('content')
    <!-- Start Content-->
    <div class="container-fluid">
        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">UBold</a></li>
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Apps</a></li>
                            <li class="breadcrumb-item active">Kanban Board</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Edit: {{$locations->name}}</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->
        <form action="{{ route('locationCategories.update', $locations->id) }}" method="post"
              enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-lg-9">
                    <div class="card-box">
                        <h5 class=" bg-light p-2 mt-0 mb-3">Category content</h5>
                        <div class="form-group mb-3">
                            <label for="product-name"> Name <span class="text-danger">*</span></label>
                            <input type="text" name="name" class="form-control"
                                   value="{{$locations->name}}">
                            @if($errors->has('name'))
                                <p class="alert alert-danger">{{ $errors->first('name') }}</p>
                            @endif
                        </div>
                        <div class="form-group mb-3">
                            <label for="product-category">Parent</label>
                            <select type="text" id="parent_id" name="parent_id" class="form-control cate">
                                <option value="">-- Select --</option>
                                <?php  category_parent($parentLocations)?>
                            </select>
                        </div>
                        <div class="form-group mb-3">
                            <label for="product-category">Description</label>
                            <textarea class="form-group description" type="textarea" name="description" rows="5"
                                      id="description">{{ $locations->description }}</textarea>
                        </div>
                        <div class="form-group mb-3">
                            <label for="banner">Banner Image</label>
                            <div style="" class="col-md-6">
                                <div class="input-group-btn">
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bd-example-modal-banner">Upload image</button>
                                        <input type="hidden" name="banner" id="banner" value="{{ $locations->banner_image }}">
                                    </span>
                                </div>
                                <div id="preview_banner" class="mt-3">
                                    @if(!empty($locations->banner_image))
                                        <div class="box_imgg position-relative">
                                            <img src="{{ config('app.url') }}{{ $locations->banner_image }}" class="show-img" id='show-img-banner' style="width:100%;">
                                            <i class="mdi mdi-close-circle-outline style_icon_remove style_icon_remove_banner" title="delete"></i>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="form-group mb-3">
                            <h4 class="header-title mb-3">The geographic coordinate</h4>
                            <div class="row">
                                <div class="col-lg-9">
                                    <div class="form-group mb-3">
                                        <div id="gmaps-basic" class="gmaps"></div>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="form-group mb-3">
                                        <div class="form-group mb-3">
                                            <label for="map_lat">map_lat <span class="text-danger">*</span></label>
                                            <input type="text" id="map_lat" name="map_lat" class="form-control"
                                                   placeholder="Please enter " value="{{ $locations->latitude }}">
                                        </div>
                                        <div class="form-group mb-3">
                                            <label for="map_lng">map_lng <span class="text-danger">*</span></label>
                                            <input type="text" id="map_lng" name="map_lng" class="form-control"
                                                   placeholder="Please enter " value="{{ $locations->longitude }}">
                                        </div>
                                        <div class="form-group mb-3">
                                            <label for="map_zoom">map_zoom <span class="text-danger">*</span></label>
                                            <input type="text" id="map_zoom" name="map_zoom" class="form-control"
                                                   placeholder="Please enter ">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> <!-- end card-box -->
                </div> <!-- end col -->
                <div class="col-lg-3">
                    <div class="card-box">
                        <div class="col-md-12">
                            <h4 class="header-title mt-5 mt-sm-0">Publish</h4>
                            <div class="mt-3">
                                <div class="custom-control custom-radio">
                                    <input type="radio" id="customRadio1" value="1" name="status"
                                           {{ ($locations->status===1)? "checked" : "" }} class="custom-control-input">
                                    <label class="custom-control-label" for="customRadio1">Publish</label>
                                </div>
                            </div>
                            <div class="mt-3">
                                <div class="custom-control custom-radio">
                                    <input type="radio" id="customRadio2" value="0" name="status"
                                           {{ ($locations->status===0)? "checked" : "" }} class="custom-control-input">
                                    <label class="custom-control-label" for="customRadio2">Draft</label>
                                </div>
                            </div>
                            <div class=" mt-3">
                                <button type="submit" class="btn w-sm btn-success waves-effect waves-light">Save
                                </button>
                            </div>
                        </div>
                    </div> <!-- end col -->
                    <div class="card-box">
                        <h5 class="mt-0 mb-3 bg-light p-2">Feature Image</h5>
                        <div class="card-box text-center">
                            <span class="input-group-btn">
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bd-example-modal-image">Upload image</button>
                                <input type="hidden" name="avatar" id="avatar" value="{{ $locations->feature_image }}">
                            </span>
                            <div id="preview_avatar" class="mt-3">
                                @if(!empty($locations->feature_image))
                                    <div class="box_imgg position-relative">
                                        <img src="{{ config('app.url') }}{{ $locations->feature_image }}" id='show-img-avatar' style="width:100%;">
                                        <i class="mdi mdi-close-circle-outline style_icon_remove style_icon_remove_avatar" title="delete"></i>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>

                </div> <!-- end col-->
            </div>
            <!-- end col-->
        </form>
    </div>
    <!-- end row -->
@stop

@section('js')
    <div class="modal fade bd-example-modal-image" id="modal-file" tabindex="-1" role="dialog" data-backdrop="false" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Manger Image</h5>
                    <button type="button" class="close" data-dismiss="modal"><i class="fas fa-times"></i></button>
                </div>
                <div class="modal-body">
                    <iframe src="{{ config('app.url') }}/filemanager/dialog.php?akey=4Ans6pfTs6A9DzHj8AZq1NVGKoYxpyoI8ivCN1bOow&field_id=avatar&multiple=0" frameborder="0" style="width: 100%; height: 500px; overflow-y: auto"></iframe>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade bd-example-modal-banner" id="modal-file-banner" tabindex="-1" role="dialog" data-backdrop="false" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Manger Image</h5>
                    <button type="button" class="close" data-dismiss="modal"><i class="fas fa-times"></i></button>
                </div>
                <div class="modal-body">
                    <iframe src="{{ config('app.url') }}/filemanager/dialog.php?akey=4Ans6pfTs6A9DzHj8AZq1NVGKoYxpyoI8ivCN1bOow&field_id=banner&multiple=0" frameborder="0" style="width: 100%; height: 500px; overflow-y: auto"></iframe>
                </div>
            </div>
        </div>
    </div>

    {{--<div class="modal fade bd-example-modal-album" id="modal-file" tabindex="-1" role="dialog" data-backdrop="false" aria-hidden="true">--}}
        {{--<div class="modal-dialog modal-lg">--}}
            {{--<div class="modal-content">--}}
                {{--<div class="modal-header">--}}
                    {{--<h5 class="modal-title" id="exampleModalLabel">Manger Image</h5>--}}
                    {{--<button type="button" class="close" data-dismiss="modal"><i class="fas fa-times"></i></button>--}}
                {{--</div>--}}
                {{--<div class="modal-body">--}}
                    {{--<iframe src="{{ config('app.url') }}/filemanager/dialog.php?akey=4Ans6pfTs6A9DzHj8AZq1NVGKoYxpyoI8ivCN1bOow&field_id=album&multiple=1" frameborder="0" style="width: 100%; height: 500px; overflow-y: auto"></iframe>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
@stop

@section('script')
    <script src="https://cloud.tinymce.com/stable/tinymce.min.js"></script>
    <script>
        tinymce.init({
            selector: '.description',
            theme: "modern",
            height: 250,
            plugins: [
                'advlist autolink lists link image charmap print preview hr anchor pagebreak',
                'searchreplace wordcount visualblocks visualchars code fullscreen',
                'insertdatetime media nonbreaking save table contextmenu directionality',
                'emoticons template paste textcolor colorpicker textpattern imagetools'
            ],
            toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
            toolbar2: 'print preview media | forecolor backcolor emoticons',
            image_advtab: true
        });
    </script>
    <script>
        var bookingCore = {
            url: 'http://sandbox.bookingcore.org',
            map_provider: 'gmap',
            map_gmap_key: '',
            csrf: 'JMrM5NwcxQy6HWOmuo7LQ2kHPh7pGwfbOPpXxkue'
        };
        var i18n = {
            warning: "Warning",
            success: "Success",
            confirm_delete: "Do you want to delete?",
            confirm: "Confirm",
            cancel: "Cancel",
        };
    </script>

    <script src="https://maps.google.com/maps/api/js?key=AIzaSyDsucrEdmswqYrw0f6ej3bf4M4suDeRgNA"></script>
    <script src="{{ URL::asset('assets/libs/gmaps/gmaps.min.js') }}"></script>
    <script src="{{ URL::asset('assets/js/pages/google-maps.init.js') }}"></script>
    <script src="{{ URL::asset('assets/js/map-engine.js') }}"></script>
    <script src="{{url('module/core/js/map-engine.js')}}"></script>
    <script>
        jQuery(function ($) {
            new BravoMapEngine('gmaps-basic', {
                fitBounds: true,
                center: [{{$locations->latitude ?? "51.505"}}, {{$locations->longitude ?? "-0.09"}}],
                zoom: {{ "8"}},
                ready: function (engineMap) {
                    @if($locations->latitude && $locations->longitude)
                    engineMap.addMarker([{{$locations->latitude}}, {{$locations->longitude}}], {
                        icon_options: {}
                    });
                    @endif
                    engineMap.on('click', function (dataLatLng) {
                        engineMap.clearMarkers();
                        engineMap.addMarker(dataLatLng, {
                            icon_options: {}
                        });
                        $("input[name=map_lat]").attr("value", dataLatLng[0]);
                        $("input[name=map_lng]").attr("value", dataLatLng[1]);
                    });
                    engineMap.on('zoom_changed', function (zoom) {
                        $("input[name=map_zoom]").attr("value", zoom);
                    })
                }
            });
        })
    </script>

    <script>

        $(document).on('hide.bs.modal', '.bd-example-modal-image', function () {
            var _img = $('input#avatar').val();
            if (!_img.length) {
                $('#preview_avatar').empty();
            } else {
                $('#preview_avatar').empty();
                $html = `
            <div class="box_imgg position-relative">
                <img src="" id='show-img-hotel' style="width:100%;">
                <i class="mdi mdi-close-circle-outline style_icon_remove style_icon_remove_avatar" title="delete"></i>
            </div>
        `;
                $('#preview_avatar').append($html);
                $('#show-img-hotel').attr('src', _img);
            }
        });

        $(document).on('click', '.style_icon_remove_avatar', function() {
            $(this).parent('.box_imgg').hide('slow', function () {
                $(this).remove();
                $('#avatar').val('');
            });
        });

        $(document).on('hide.bs.modal', '.bd-example-modal-banner', function () {
            var _img = $('input#banner').val();
            if (!_img.length) {
                $('#preview_banner').empty();
            } else {
                $('#preview_banner').empty();
                $html = `
            <div class="box_imgg position-relative">
                <img src="" id='show-img-banner' class="show-img" style="width:100%;">
                <i class="mdi mdi-close-circle-outline style_icon_remove style_icon_remove_banner" title="delete"></i>
            </div>
        `;
                $('#preview_banner').append($html);
                $('#show-img-banner').attr('src', _img);
            }
        });

        $(document).on('click', '.style_icon_remove_banner', function() {
            $(this).parent('.box_imgg').hide('slow', function () {
                $(this).remove();
                $('#banner').val('');
            });
        });
    </script>
    <script src="{{ URL::asset('assets/libs/twitter-bootstrap-wizard/twitter-bootstrap-wizard.min.js')}}"></script>

    <!-- Init js-->
    <script src="{{ URL::asset('assets/js/pages/form-wizard.init.js')}}"></script>
    <script src="{{ URL::asset('assets/libs/dropzone/dropzone.min.js')}}"></script>
    <script src="{{ URL::asset('assets/libs/dropify/dropify.min.js')}}"></script>

    <!-- Init js-->
    <script src="{{ URL::asset('assets/js/pages/form-fileuploads.init.js')}}"></script>

@endsection

