@foreach($terms as $term)
    <tr id="tr_{{ $term->id}}">
        <td><input type="checkbox" class="sub-chk" name='ids[]' value='{{ $term->id }}'></td>
        <td><a href="{{ route('spaceTerms.edit', $term->id) }}">{{ $term->name }}</a>
        </td>
        <td>{{ $term->created_at->format('d-m-Y') }}</td>
        <td>
            <a href="{{ route('spaceTerms.edit', $term->id) }}"
                class="btn btn-primary btn-sm">Edit</a></td>
    </tr>
@endforeach
