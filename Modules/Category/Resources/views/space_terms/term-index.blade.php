@extends('base::layouts.master')
@section('css')
    <!-- Bootstrap Tables css -->
    <link href="{{ URL::asset('assets/libs/bootstrap-table/bootstrap-table.min.css')}}" rel="stylesheet"
          type="text/css"/>
    <link href="{{ URL::asset('assets/libs/dropzone/dropzone.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{ URL::asset('assets/libs/dropify/dropify.min.css')}}" rel="stylesheet" type="text/css"/>

    <!-- App css -->
    <link href="{{ URL::asset('assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{ URL::asset('assets/css/icons.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{ URL::asset('assets/css/app.min.css')}}" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection
@section('content')
    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">UBold</a></li>
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Space</a></li>
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Attribute</a></li>
                            <li class="breadcrumb-item active">Attribute:{{ $spaceAttribute->name }}</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Attribute:{{ $spaceAttribute->name }}</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->
        <div class="row">
            <div class="container-fluid">

                <!-- start page title -->
                <!-- end page title -->


                <div class="row">
                    <div class="col-lg-4">
                        <div class="card-box">
                            <form method="post" action="{{ route('spaceTerms.store') }}" enctype="multipart/form-data">
                                @csrf
                                <input type="hidden" name="attribute_id" value="{{ $spaceAttribute->id }}">
                                <div class="form-group mb-3">
                                    <label for="product-name"> Name <span class="text-danger">*</span></label>
                                    <input type="text" id="product-name" name="name" class="form-control"
                                           placeholder="Term name">
                                    @if($errors->has('name'))
                                        <p class="alert alert-danger">{{ $errors->first('name') }}</p>
                                    @endif
                                </div>

                                <div class="mb-3">
                                    <button type="submit" class="btn w-sm btn-primary waves-effect waves-light">
                                        Add new
                                    </button>
                                    <a href="{{ route('spaceAttributes.list') }}"><button type="button" id="button-hide" class="btn w-sm btn-danger waves-effect waves-light">
                                        Cancel
                                    </button></a>
                                </div>
                            </form>
                        </div> <!-- end card-box -->
                    </div> <!-- end col -->

                    <div class="col-lg-8">
                        <form class="form-delete" action="{{ route('spaceTerms.delete_multiple') }}" method="post">
                            @csrf
                            @method('DELETE')
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="card-box">
                                        <h4 class="header-title">All Terms</h4>

                                        <div class="columns-left" style="float: left">
                                            <button style="margin-bottom: 10px" class="btn btn-primary delete_all">Delete</button>
                                        </div>
                                        <div class="columns-left" style="float: right">
                                                <div class="form-inline">
                                                    <input type="text" class="form-control" name="search" id="search"
                                                           placeholder="search">
                                                </div>
                                        </div>
                                        <table class="table table-bordered">
                                            <thead class="thead-light">
                                            <tr>
                                                <th width="50px"><input type="checkbox" class="selectall" id="master"></th>
                                                <th>Name</th>
                                                <th>Date</th>
                                                <th>Action</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @if($terms->count()>0)
                                                @foreach($terms as $term)
                                                    <tr id="tr_{{ $term->id}}">
                                                        <td><input type="checkbox" class="sub-chk" name='ids[]' value='{{ $term->id }}'></td>
                                                        <td><a href="{{ route('spaceTerms.edit', $term->id) }}">{{ $term->name }}</a>
                                                        </td>
                                                        <td>{{ $term->created_at->format('d-m-Y') }}</td>
                                                        <td>
                                                            <a href="{{ route('spaceTerms.edit', $term->id) }}"
                                                               class="btn btn-primary btn-sm">Edit</a></td>
                                                        </tr>
                                                @endforeach
                                            @endif
                                            </tbody>
                                        </table>
                                    </div> <!-- end card-box-->
                                </div> <!-- end col-->
                            </div>
                        </form>
                    </div> <!-- end col-->
                </div>
                <!-- end row -->
            </div> <!-- container -->
        </div>
    </div>
    <div class="modal justify-content-center" id="confirm">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <p>Bạn chắc chắn muốn xóa</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-sm btn-primary" id="delete-btn">Delete</button>
                    <button type="button" class="btn btn-sm btn-dark" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
@stop
@section('script-bottom')
    {!! Toastr::message() !!}

    <script>
         $(document).on('keyup','#search',function(){
                var query = $(this).val();
                search_article(query);
            });
            function search_article(query = ' '){
                $.ajax({
                    type: "GET",
                    url: "{{ route('spaceAttributes.search_attribut_term') }}",
                    data: {query:query},
                    //dataType: "json",
                    success: function (data) {
                        $('tbody').html(data)
                    }
            });
        }

        $(document).on('click', '.delete_all', function (e) {
            e.preventDefault();
            var $form = $('.form-delete');
            $('#confirm').modal({backdrop: 'static', keyboard: false})
                .on('click', '#delete-btn', function () {
                    $form.submit();
                });
            // $('#confirmDeleteComment').modal('show');

            // do anything else you need here
        });

    </script>
    <script>
        $('.selectall').click(function () {
            $('.sub-chk').prop('checked', $(this).prop('checked'));
        })
        $('.sub-chk').change(function () {
            var total = $('.sub-chk').length;
            var number = $('.sub-chk:checked').length;
            if (total == number) {
                $('.selectall').prop('checked', true);
            } else {
                $('.selectall').prop('checked', false);
            }
        })
    </script>

    <script src="{{ URL::asset('assets/libs/dropzone/dropzone.min.js')}}"></script>
    <script src="{{ URL::asset('assets/libs/dropify/dropify.min.js')}}"></script>
    <!-- Init js-->
    <script src="{{ URL::asset('assets/js/pages/form-fileuploads.init.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
@endsection


