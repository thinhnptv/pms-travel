<?php

use Illuminate\Support\Facades\Route;

Route::group(['middleware' => ['web', 'adminLogin'], 'prefix' => 'admin/category', 'namespace' => 'Modules\Category\Http\Controllers'], function () {
    Route::group(['prefix' => 'room'], function () {
        Route::get('/', 'RoomAttributeController@index')->name('roomAttributes.list');
        Route::get('/showRoomTerm/{id}', 'RoomAttributeController@getAllTerm')->name('roomAttributes.show_all_term');
        Route::post('/create', 'RoomAttributeController@store')->name('roomAttributes.store');
        Route::get('edit/{id}', 'RoomAttributeController@edit')->name('roomAttributes.edit');
        Route::post('update/{id}', 'RoomAttributeController@update')->name('roomAttributes.update');
        Route::delete('delete/{id}', 'RoomAttributeController@destroy')->name('roomAttributes.delete');
        Route::delete('deleteMultiple', 'RoomAttributeController@deleteMultiple')->name('roomAttributes.delete_multiple');

        Route::group(['prefix' => 'term'], function () {
            Route::post('/create', 'RoomTermController@store')->name('roomTerms.store');
            Route::get('edit/{id}', 'RoomTermController@edit')->name('roomTerms.edit');
            Route::post('update/{id}', 'RoomTermController@update')->name('roomTerms.update');
            Route::delete('delete/{id}', 'RoomTermController@destroy')->name('roomTerms.delete');
            Route::delete('deleteMultiple', 'RoomTermController@deleteMultiple')->name('roomTerms.delete_multiple');
        });
    });

    Route::group(['prefix' => 'car'], function () {
        Route::get('/', 'CarAttributeController@index')->name('carAttributes.list');
        Route::get('/showCarTerm/{id}', 'CarAttributeController@getAllTerm')->name('carAttributes.show_all_term');
        Route::post('/create', 'CarAttributeController@store')->name('carAttributes.store');
        Route::get('edit/{id}', 'CarAttributeController@edit')->name('carAttributes.edit');
        Route::post('update/{id}', 'CarAttributeController@update')->name('carAttributes.update');
        Route::delete('delete/{id}', 'CarAttributeController@destroy')->name('carAttributes.delete');
        Route::delete('deleteMultiple', 'CarAttributeController@deleteMultiple')->name('carAttributes.delete_multiple');

        Route::group(['prefix' => 'term'], function () {
            Route::post('/create', 'CarTermController@store')->name('carTerms.store');
            Route::get('edit/{id}', 'CarTermController@edit')->name('carTerms.edit');
            Route::post('update/{id}', 'CarTermController@update')->name('carTerms.update');
            Route::delete('delete/{id}', 'CarTermController@destroy')->name('carTerms.delete');
            Route::delete('deleteMultiple', 'CarTermController@deleteMultiple')->name('carTerms.delete_multiple');
        });
    });

    Route::group(['prefix' => 'space'], function () {
        Route::get('/', 'SpaceAttributeController@index')->name('spaceAttributes.list');
        Route::get('/showSpaceTerm/{id}', 'SpaceAttributeController@getAllTerm')->name('spaceAttributes.show_all_term');
        Route::post('/create', 'SpaceAttributeController@store')->name('spaceAttributes.store');
        Route::get('edit/{id}', 'SpaceAttributeController@edit')->name('spaceAttributes.edit');
        Route::post('update/{id}', 'SpaceAttributeController@update')->name('spaceAttributes.update');
        Route::delete('delete/{id}', 'SpaceAttributeController@destroy')->name('spaceAttributes.delete');
        Route::delete('deleteMultiple', 'SpaceAttributeController@deleteMultiple')->name('spaceAttributes.delete_multiple');
        Route::get('searchAttribute','SpaceAttributeController@searchAttribute')->name('spaceAttributes.search_attribute');
        Route::get('searchAttributeTerm','SpaceAttributeController@searchAttributeTerm')->name('spaceAttributes.search_attribut_term');

        Route::group(['prefix' => 'term'], function () {
            Route::post('/create', 'SpaceTermController@store')->name('spaceTerms.store');
            Route::get('edit/{id}', 'SpaceTermController@edit')->name('spaceTerms.edit');
            Route::post('update/{id}', 'SpaceTermController@update')->name('spaceTerms.update');
            Route::delete('delete/{id}', 'SpaceTermController@destroy')->name('spaceTerms.delete');
            Route::delete('deleteMultiple', 'SpaceTermController@deleteMultiple')->name('spaceTerms.delete_multiple');
        });

    });
    Route::group(['prefix' => 'hotel'], function () {
        Route::get('/', 'HotelAttributeController@index')->name('hotelAttributes.index');
        Route::get('/showHotelTerm/{id}', 'HotelAttributeController@getAllTerm')->name('hotelAttributes.show_all_term');
        Route::post('/create', 'HotelAttributeController@store')->name('hotelAttributes.store');
        Route::get('edit/{id}', 'HotelAttributeController@edit')->name('hotelAttributes.edit');
        Route::post('update/{id}', 'HotelAttributeController@update')->name('hotelAttributes.update');
        Route::delete('delete/{id}', 'HotelAttributeController@destroy')->name('hotelAttributes.delete');
        Route::delete('delete_multiple', 'HotelAttributeController@deleteMultiple')->name('hotelAttributes.delete_multiple');

        Route::group(['prefix' => 'term'], function () {
            Route::post('/create', 'TermController@store')->name('hotelTerms.store');
            Route::get('edit/{id}', 'TermController@edit')->name('hotelTerms.edit');
            Route::post('update/{id}', 'TermController@update')->name('hotelTerms.update');
            Route::delete('delete/{id}', 'TermController@destroy')->name('hotelTerms.delete');
            Route::delete('delete_multiple', 'TermController@deleteMultiple')->name('hotelTerms.delete_multiple');
        });
    });

    Route::group(['prefix' => 'location'], function () {
        Route::get('/', 'LocationController@index')->name('locationCategories.index');
        Route::post('/create', 'LocationController@store')->name('locationCategories.store');
        Route::get('edit/{id}', 'LocationController@edit')->name('locationCategories.edit');
        Route::post('update/{id}', 'LocationController@update')->name('locationCategories.update');
        Route::delete('delete/{id}', 'LocationController@destroy')->name('locationCategories.delete');
        Route::delete('deleteMultiple', 'LocationController@deleteMultiple')->name('locationCategories.delete_multiple');

    });

    Route::group(['prefix' => 'tour'], function (){
        Route::get('/', 'TourCategoryController@index')->name('tourCategories.list');
        Route::post('/create', 'TourCategoryController@store')->name('tourCategories.store');
        Route::get('edit/{id}', 'TourCategoryController@edit')->name('tourCategories.edit');
        Route::post('update/{id}', 'TourCategoryController@update')->name('tourCategories.update');
        Route::delete('delete/{id}', 'TourCategoryController@destroy')->name('tourCategories.delete');
        Route::delete('deleteMultiple', 'TourCategoryController@deleteMultiple')->name('tourCategories.delete_multiple');

        Route::group(['prefix' => 'atrribute'], function () {
            Route::get('/', 'TourAttributeController@index')->name('tourAttributes.index');
            Route::get('/showTerm/{id}', 'TourAttributeController@getAllTerm')->name('tourAttributes.show_all_term');
            Route::post('/create', 'TourAttributeController@store')->name('tourAttributes.store');
            Route::get('edit/{id}', 'TourAttributeController@edit')->name('tourAttributes.edit');
            Route::post('update/{id}', 'TourAttributeController@update')->name('tourAttributes.update');
            Route::delete('delete/{id}', 'TourAttributeController@destroy')->name('tourAttributes.delete');
            Route::delete('delete_multiple', 'TourAttributeController@deleteMultiple')->name('tourAttributes.delete_multiple');

            Route::group(['prefix' => 'term'], function () {
                Route::post('/create', 'TourTermController@store')->name('tourTerms.store');
                Route::get('edit/{id}', 'TourTermController@edit')->name('tourTerms.edit');
                Route::post('update/{id}', 'TourTermController@update')->name('tourTerms.update');
                Route::delete('delete/{id}', 'TourTermController@destroy')->name('tourTerms.delete');
                Route::delete('delete_multiple', 'TourTermController@deleteMultiple')->name('tourTerms.delete_multiple');
            });
        });
    });

    Route::group(['prefix' => 'post'], function (){
        Route::get('/', 'PostCategoryController@index')->name('postCategories.list');
        Route::post('/create', 'PostCategoryController@store')->name('postCategories.store');
        Route::get('edit/{id}', 'PostCategoryController@edit')->name('postCategories.edit');
        Route::post('update/{id}', 'PostCategoryController@update')->name('postCategories.update');
        Route::delete('delete/{id}', 'PostCategoryController@destroy')->name('postCategories.delete');
        Route::delete('deleteMultiple', 'PostCategoryController@deleteMultiple')->name('postCategories.delete_multiple');
    });


});
