<?php

namespace Modules\Category\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdatePostCategoryRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:3|max:255',
        ];

    }

    public function messages()
    {
        return [
            'name.required' => 'Tên danh mục không được để trống',
            'name.min' => 'Danh mục bài viết tối thiểu 3 kí tự',
            'name.max' => 'Danh mục bài viết tối đa 255 kí tự'
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
