<?php

namespace Modules\Category\Http\Controllers;

use Brian2694\Toastr\Facades\Toastr;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Modules\Category\Entities\Roomterm;
use Modules\Category\Http\Requests\CreateRoomTermRequest;
use Modules\Category\Http\Requests\UpdateRoomTermRequest;

class RoomTermController extends Controller
{
    public function index()
    {
        return view('category::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('category::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CreateRoomTermRequest $request)
    {
        $attribute_id = $request->attribute_id;
        $terms = new RoomTerm();
        $terms->attribute_id = $attribute_id;
        $terms->name = $request->name;
        $terms->icon = $request->icon;
//        $term->image =$imageName;
        $terms->save();
        Toastr::success('Thêm mới thành công');
        return redirect()->route('roomAttributes.show_all_term', compact('attribute_id'));
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('category::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $terms = RoomTerm::find($id);
        $roomAttribute = $terms->roomAttribute;
        return view('category::room_terms.edit-room-term', compact('terms','roomAttribute'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdateRoomTermRequest $request, $id)
    {

        $terms = RoomTerm::find($id);
        $attribute_id = $terms->attribute_id;
        $terms->name = $request->name;
        $terms->icon = $request->icon;
//        $term->image =$imageName;
        $terms->save();
        Toastr::success('Cập nhật thành công');
        return redirect()->route('roomAttributes.show_all_term', compact('attribute_id'));

    }

    /**
     * Remove the specified resource from storage.
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $terms = RoomTerm::findOrFail($id);
        $terms->delete();
        return response()->json(['success'=>"Xóa thành công", 'tr'=>'tr_'.$id]);
    }

    public function deleteMultiple(Request $request){
        $ids = $request->get('ids');
        if (isset($ids)) {
            Roomterm::whereIn('id', $ids)->delete();
            Toastr::success('Xóa thành công');
        } else {
            Toastr::warning('Bạn cần chọn hàng để xóa');
        }
        return redirect()->back();
    }

}
