<?php

namespace Modules\Category\Http\Controllers;

use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\Category\Entities\TourCategory;
use Modules\Category\Http\Requests\CreateTourCategoryRequest;
use Modules\Category\Http\Requests\UpdateTourCategoryRequest;

class TourCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $tourCategories = TourCategory::query();
        if (!empty($request->name)) {
           
            $name = $request->name; 
            $tourCategories->where('name', 'like', '%' .$name. '%');
        }
        $tourCategories = $tourCategories->get();
        return view('category::tour_categories.tour-category-index', compact('tourCategories'));
    }

    /**
     * Show the form for creating a new resource.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('category::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CreateTourCategoryRequest $request)
    {
        $postCategory = new TourCategory();
        $postCategory->name = $request->name;
        $postCategory->slug = $request->slug;
        $postCategory->parent_id = $request->parent_id;
        $postCategory->save();
        Toastr::success('Category created successfully');
        return redirect()->route('tourCategories.list');
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('category::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $tourCategory = TourCategory::findOrFail($id);
        $parentCategories = TourCategory::whereNull('parent_id')->where('id', '!=', $id)->get();
        return view('category::tour_categories.edit-tour-category', compact('tourCategory', 'parentCategories'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdateTourCategoryRequest $request, $id)
    {
        $postCategory = TourCategory::find($id);
        $postCategory->name = $request->name;
        $postCategory->status = $request->status;
        $postCategory->parent_id = $request->parent_id;
        $postCategory->slug = str_slug($request->name);
        $postCategory->save();
        Toastr::success("Category updated successfully");
        return redirect()->route('tourCategories.list');
    }

    /**
     * Remove the specified resource from storage.
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $postCategory = TourCategory::findOrFail($id);
        $postCategory->delete();
        return response()->json(['success' => "Product Deleted successfully.", 'tr' => 'tr_' . $id]);
    }

    public function deleteMultiple(Request $request)
    {
        $ids = $request->get('ids');
        if (isset($ids)) {
            TourCategory::WhereIn('id', $ids)->delete();
            Toastr::success('Category deleted successfully');
        } else {
            Toastr::warning('Please select at least one item!!!');
        }
        return redirect()->route('tourCategories.list');
    }

}
