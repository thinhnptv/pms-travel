<?php

namespace Modules\Category\Http\Controllers;

use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\Category\Entities\HotelAttribute;
use Modules\Category\Http\Requests\CreateHotelAttributeRequest;
use Modules\Category\Http\Requests\UpdateHotelAttributeRequest;

class HotelAttributeController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $hotelAttributes = HotelAttribute::query();
        if(!empty($request->name)){
            $name = $request->name;
            $hotelAttributes->where('name', 'like', '%' .$name. '%');
        }
        $hotelAttributes = $hotelAttributes->get();
        return view('category::hotel_attributes.hotel-attributes-index', compact('hotelAttributes'));
    }

    public function getAllTerm($id)
    {
        $hotelAttributes = HotelAttribute::find($id);
        $terms = $hotelAttributes->terms;
        return view('category::terms.term-index', compact('terms', 'hotelAttributes'));

    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('category::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CreateHotelAttributeRequest $request)
    {
        $hotelAttributes = new HotelAttribute();
        $hotelAttributes->name = $request->name;
        $hotelAttributes->save();
        Toastr::success('Thêm mới thành công');
        return redirect()->route('hotelAttributes.index');

    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('category::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $hotelAttributes = HotelAttribute::findOrFail($id);

        return view('category::hotel_attributes.edit-hotel-attribute', compact('hotelAttributes'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdateHotelAttributeRequest $request, $id)
    {
        $hotelAttributes = HotelAttribute::find($id);
        $hotelAttributes->name = $request->name;
        $hotelAttributes->save();
        Toastr::success('Cập nhật thành công');
        return redirect()->route('hotelAttributes.index');

    }

    /**
     * Remove the specified resource from storage.
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $hotelAttributes = HotelAttribute::findOrFail($id);
        $hotelAttributes->delete();
        return response()->json(['success' => "Xóa thành công", 'tr' => 'tr_' . $id]);
    }

    public function deleteMultiple(Request $request)
    {
        $ids = $request->get('ids');
        if (isset($ids)) {
            HotelAttribute::whereIn('id', $ids)->delete();
            Toastr::success('Xóa thành công');
        } else {
            Toastr::warning('Bạn cần chọn hàng để xóa');
        }
        return redirect()->route('hotelAttributes.index');

    }

}
