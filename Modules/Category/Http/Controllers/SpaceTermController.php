<?php

namespace Modules\Category\Http\Controllers;

use Brian2694\Toastr\Facades\Toastr;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Modules\Category\Entities\SpaceTerm;
use Modules\Category\Http\Requests\CreateSpaceTermRequest;
use Modules\Category\Http\Requests\UpdateSpaceTermRequest;

class SpaceTermController extends Controller
{
    public function index()
    {
        return view('category::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('category::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CreateSpaceTermRequest $request)
    {
//        $image = $request->file('image');
//        $slug = str_slug($request->name);
//        if (isset($image)) {
//            $currentDate = Carbon::now()->toDateString();
//            $imageName = $slug . '-' . $currentDate . '-' . uniqid() . '.' . $image->getClientOriginalExtension();
//            if (!Storage::disk('public')->exists('category')) {
//                Storage::disk('public')->makeDirectory('category');
//            }
//            $category = Image::make($image)->stream();
//            Storage::disk('public')->put('category/' . $imageName, $category);
//        } else {
//            $imageName = "default.png";
//        }
        $attribute_id = $request->attribute_id;
        $terms = new SpaceTerm();
        $terms->attribute_id = $attribute_id;
        $terms->name = $request->name;
        $terms->icon = $request->icon;
//        $term->image = $imageName;
        $terms->save();
        Toastr::success('Thêm mới thành công');
        return redirect()->route('spaceAttributes.show_all_term', compact('attribute_id'));
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('category::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $terms = SpaceTerm::findorFail($id);
        return view('category::space_terms.edit-space-term', compact('terms'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdateSpaceTermRequest $request, $id)
    {
//        $image = $request->file('image');
//        $slug = str_slug($request->name);
//        $term = SpaceTerm::find($id);
//        if (isset($image)) {
//            $currentDate = Carbon::now()->toDateString();
//            $imageName = $slug . '-' . $currentDate . '-' . uniqid() . '.' . $image->getClientOriginalExtension();
//            if (!Storage::disk('public')->exists('category')) {
//                Storage::disk('public')->makeDirectory('category');
//            }
//            if (Storage::disk('public')->exists('category/' . $term->image)) {
//                Storage::disk('public')->delete('category/' . $term->image);
//            }
//            $attribute = Image::make($image)->stream();
//            Storage::disk('public')->put('category/' . $imageName, $attribute);
//        } else {
//            $imageName = $term->image;
//        }
        $terms = SpaceTerm::findorFail($id);
        $attribute_id = $terms->attribute_id;
        $terms->name = $request->name;
        $terms->icon = $request->icon;
//        $term->image = $imageName;
        $terms->save();
        Toastr::success('Cập nhật thành công');
        return redirect()->route('spaceAttributes.show_all_term', compact('attribute_id'));

    }

    /**
     * Remove the specified resource from storage.
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $terms = SpaceTerm::findOrFail($id);
        $terms->delete();
        return response()->json(['success' => "Xóa thành công", 'tr' => 'tr_' . $id]);
    }

    public function deleteMultiple(Request $request)
    {
        $ids = $request->get('ids');
        if (isset($ids)) {
            SpaceTerm::whereIn('id', $ids)->delete();
            Toastr::success('Xóa thành công');
        } else {
            Toastr::warning('Bạn cần chọn hàng để xóa');
        }
        return redirect()->back();
    }
}
