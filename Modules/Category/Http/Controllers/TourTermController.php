<?php

namespace Modules\Category\Http\Controllers;

use Brian2694\Toastr\Facades\Toastr;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Modules\Category\Entities\TourTerm;
use Modules\Category\Http\Requests\CreateTourTermRequest;
use Modules\Category\Http\Requests\UpdateTourTermRequest;

class TourTermController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('category::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('category::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(CreateTourTermRequest $request)
    {
        $attribute_id = $request->attribute_id;
        $term = new TourTerm();
        $term->attribute_id = $attribute_id;
        $term->name = $request->name;
        $term->save();
        Toastr::success('Term created successfully');
        return redirect()->route('tourAttributes.show_all_term', compact('attribute_id'));
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('category::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {
        $term = TourTerm::find($id);

        return view('category::tour_terms.edit-tour-term', compact('term'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(UpdateTourTermRequest $request, $id)
    {
        
        $term = TourTerm::find($id);
       
        $attribute_id = $term->attribute_id;
        $term->name = $request->name;
        $term->save();
        Toastr::success('Term updated');
        return redirect()->route('tourAttributes.show_all_term', compact('attribute_id'));

    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {
        $term = TourTerm::findOrFail($id);
        $term->delete();
        return response()->json(['success' => "Term Deleted successfully.", 'tr' => 'tr_'.$id]);
    }
    public function deleteMultiple(Request $request)
    {
        $ids = $request->get('ids');
        if (isset($ids)) {
            TourTerm::whereIn('id', $ids)->delete();
            Toastr::success('Terms deleted successfully');
        } else {
            Toastr::warning('Please select at least one item!!!');
        }
        return redirect()->back();
    }
}
