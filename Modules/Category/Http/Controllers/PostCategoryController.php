<?php

namespace Modules\Category\Http\Controllers;

use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Category\Entities\PostCategory;
use Modules\Category\Http\Requests\CreatePostCategoryRequest;
use Modules\Category\Http\Requests\UpdatePostCategoryRequest;

class PostCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $postCategories = PostCategory::all();
        return view('category::post_categories.post-category-index', compact('postCategories'));
    }

    /**
     * Show the form for creating a new resource.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('category::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CreatePostCategoryRequest $request)
    {
        $postCategory = new PostCategory();
        $postCategory->name = $request->name;
        $postCategory->slug = $request->slug;
        $postCategory->parent_id = $request->parent_id;
        $postCategory->save();
        Toastr::success('Thêm mới thành công');
        return redirect()->route('postCategories.list');
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('category::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $postCategory = PostCategory::findOrFail($id);
        $parentCategories = PostCategory::where('id', '!=', $id)->get();
        return view('category::post_categories.edit-post-category', compact('postCategory', 'parentCategories'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdatePostCategoryRequest $request, $id)
    {
        $postCategory = PostCategory::find($id);
        $postCategory->name = $request->name;
        $postCategory->status = $request->status;
        $postCategory->parent_id = $request->parent_id;
        $postCategory->slug = str_slug($request->name);
        $postCategory->save();
        Toastr::success("Cập nhật thành công");
        return redirect()->route('postCategories.list');
    }

    /**
     * Remove the specified resource from storage.
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $postCategory = PostCategory::findOrFail($id);
        $postCategory->delete();
        return response()->json(['success' => "Xóa thành công", 'tr' => 'tr_' . $id]);
    }

    public function deleteMultiple(Request $request)
    {
        $ids = $request->get('ids');
        if (isset($ids)) {
            PostCategory::WhereIn('id', $ids)->delete();
            Toastr::success('Xóa thành công');
        } else {
            Toastr::warning('Bạn cần chọn hàng cần xóa');
        }
        return redirect()->route('postCategories.list');
    }
}
