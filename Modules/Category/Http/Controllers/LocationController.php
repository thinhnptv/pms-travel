<?php

namespace Modules\Category\Http\Controllers;

use Brian2694\Toastr\Facades\Toastr;
use Carbon\Carbon;
use GoogleMaps\GoogleMaps;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Modules\Category\Entities\Location;
use Modules\Category\Http\Requests\CreateLocationRequest;
use Modules\Category\Http\Requests\UpdateLocationRequest;

class LocationController extends Controller
{
    public function index()
    {
        $locations = Location::all();
        return view('category::location_categories.location-category-index', compact('locations'));
    }


    public function store(CreateLocationRequest $request)
    {
        $locations = new Location();
        $locations->name = $request->name;
        $locations->slug = str_slug($request->name);
        $locations->description = $request->description;
        $locations->parent_id = $request->parent_id;
        $locations->longitude = $request->map_lng;
        $locations->latitude = $request->map_lat;
        $locations->save();
        Toastr::success('Thêm mới thành công');
        return redirect()->back();

    }

    public function edit($id)
    {
        $locations = Location::findOrFail($id);
        $parentLocations = Location::where('id', '!=', $id)->get();

        return view('category::location_categories.location-category-edit', compact('locations', 'parentLocations'));
    }


    public function update(UpdateLocationRequest $request, $id)
    {
        $str_avatar = '';
        $str_banner = '';
//        $str_album = '';


        if ($request->input('avatar')) {
            $str_avatar = checkImage($request->input('avatar'));
        }
        if ($request->input('banner')) {
            $str_banner = checkImage($request->input('banner'));
        }
//        if($request->input('album')) {
//            $str_album = checkMultipleImage($request->input('album'));
//        }

        $locations = Location::findOrFail($id);
        $locations->name = $request->name;
        $locations->status = $request->status;
        $locations->description = $request->description;
        $locations->slug = str_slug($request->name);
        $locations->parent_id = $request->parent_id;
        $locations->longitude = $request->map_lng;
        $locations->latitude = $request->map_lat;
        $locations->banner_image = $str_banner;
        $locations->feature_image = $str_avatar;
        $locations->save();
        Toastr::success('Cập nhật thành công');
        return redirect()->route('locationCategories.index');
    }


    public function destroy($id)
    {
        $locations = Location::findOrFail($id);
        $locations->delete();
        return response()->json(['success' => "Xóa thành công.", 'tr' => 'tr_' . $id]);
    }

    public function deleteMultiple(Request $request)
    {
        $ids = $request->get('ids');
        if (isset($ids)) {
            Location::whereIn('id', $ids)->delete();
            Toastr::success('Xóa thành công');
        } else {
            Toastr::warning('Bạn cần chọn hàng để xóa');
        }
        return redirect()->route('locationCategories.index');

    }

}
