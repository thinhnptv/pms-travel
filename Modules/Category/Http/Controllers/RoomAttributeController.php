<?php

namespace Modules\Category\Http\Controllers;

use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\Category\Entities\RoomAttribute;
use Modules\Category\Http\Requests\CreateRoomAttributeRequest;
use Modules\Category\Http\Requests\UpdateRoomAttributeRequest;

class RoomAttributeController extends Controller
{
    public function index(Request $request)
    {
        $roomAttributes = RoomAttribute::query();
        if(!empty($request->name)){
            $name = $request->name;
            $roomAttributes->where('name', 'like', '%' .$name. '%');
        }
        $roomAttributes = $roomAttributes->get();
        return view('category::room_attributes.room-attributes-index', compact('roomAttributes'));
    }

    public function getAllTerm($id)
    {
        $roomAttribute = RoomAttribute::find($id);
        $roomTerms = $roomAttribute->roomTerms;
        return view('category::room_terms.room-term-index', compact('roomTerms', 'roomAttribute'));

    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('category::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CreateRoomAttributeRequest $request)
    {
        $roomAttribute = new RoomAttribute();
        $roomAttribute->name = $request->name;
        $roomAttribute->slug = $request->name;
        $roomAttribute->save();
        Toastr::success('Thêm mới thành công');
        return redirect()->route('roomAttributes.list');
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('category::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $roomAttribute = RoomAttribute::findOrFail($id);

        return view('category::room_attributes.edit-room-attribute', compact('roomAttribute'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdateRoomAttributeRequest $request, $id)
    {
        $roomAttribute = RoomAttribute::find($id);
        $roomAttribute->name = $request->name;
        $roomAttribute->save();
        Toastr::success('Cập nhật thành công');
        return redirect()->route('roomAttributes.list');

    }

    /**
     * Remove the specified resource from storage.
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $roomAttribute = RoomAttribute::findOrFail($id);
        $roomAttribute->delete();
        return response()->json(['success' => "Xóa thành công", 'tr' => 'tr_' . $id]);
    }

    public function deleteMultiple(Request $request)
    {
        $ids = $request->get('ids');
        if (isset($ids)) {
            RoomAttribute::whereIn('id', $ids)->delete();
            Toastr::success('Xóa thành công');
        } else {
            Toastr::warning('Bạn cần chọn hàng để xóa');
        }
        return redirect()->route('roomAttributes.list');
    }
}
