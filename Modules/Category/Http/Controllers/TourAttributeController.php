<?php

namespace Modules\Category\Http\Controllers;

use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Category\Entities\TourAttribute;
use Modules\Category\Http\Requests\CreateTourAttributeRequest;
use Modules\Category\Http\Requests\UpdateTourTermRequest;

class TourAttributeController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        $tourAttributes = TourAttribute::query();
        if (!empty($request->name)) {
            $name = $request->name;
            $tourAttributes->where('name', 'like', '%' .$name. '%');
        }
        $tourAttributes = $tourAttributes->get();
        return view('category::tour_attributes.tour-attributes-index', compact('tourAttributes'));
    }
    public function getAllTerm($id)
    {
        $tourAttribute = TourAttribute::find($id);
        $tourTerms = $tourAttribute->terms;
        return view('category::tour_terms.tour-term-index', compact('tourTerms', 'tourAttribute'));

    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('category::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(CreateTourAttributeRequest $request)
    {
        $slug = str_slug($request->name);
        
        $tourlAttribute = new TourAttribute();
        $tourlAttribute->name = $request->name;
        $tourlAttribute->slug = $slug;
        $tourlAttribute->save();
        Toastr::success('Attribute created successfully');
        return redirect()->route('tourAttributes.index');
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('category::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {
        $tourAttribute = TourAttribute::findOrFail($id);

        return view('category::tour_attributes.edit-tour-attribute', compact('tourAttribute'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(UpdateTourTermRequest $request, $id)
    {
        $tourAttribute = TourAttribute::find($id);
        $slug = str_slug($request->name);
        $tourAttribute->name = $request->name;
        $tourAttribute->slug = $slug;
        $tourAttribute->save();
        Toastr::success('Attribute updated successfully');
        return redirect()->route('tourAttributes.index');
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {
        $tourAttribute = TourAttribute::findOrFail($id);
        $tourAttribute->delete();
        return response()->json(['success' => "Attribute Deleted successfully.", 'tr' => 'tr_' . $id]);
    }
    public function deleteMultiple(Request $request)
    {
        $ids = $request->get('ids');
        if (isset($ids)) {
            TourAttribute::whereIn('id', $ids)->delete();
            Toastr::success('Category deleted successfully');
        } else {
            Toastr::warning('Please select at least one item!!!');
        }
        return redirect()->route('tourAttributes.index');

    }
}
