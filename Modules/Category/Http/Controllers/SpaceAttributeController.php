<?php

namespace Modules\Category\Http\Controllers;

use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\Category\Entities\SpaceAttribute;
use Modules\Category\Entities\SpaceTerm;
use Modules\Category\Http\Requests\CreateSpaceAttributeRequest;
use Modules\Category\Http\Requests\UpdateSpaceAttributeRequest;

class SpaceAttributeController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $spaceAttributes = SpaceAttribute::all();
        return view('category::space_attributes.space-attributes-index', compact('spaceAttributes'));
    }

    public function getAllTerm($id)
    {
        $spaceAttribute = SpaceAttribute::find($id);
        $terms = $spaceAttribute->spaceTerms;
        return view('category::space_terms.term-index', compact('terms', 'spaceAttribute'));

    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('category::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CreateSpaceAttributeRequest $request)
    {
        $spaceAttribute = new SpaceAttribute();
        $spaceAttribute->name = $request->name;
        $spaceAttribute->slug = $request->name;
        $spaceAttribute->save();
        Toastr::success('Thêm mới thành công');
        return redirect()->route('spaceAttributes.list');

    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('category::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $spaceAttribute = SpaceAttribute::findOrFail($id);

        return view('category::space_attributes.edit-space-attribute', compact('spaceAttribute'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdateSpaceAttributeRequest $request, $id)
    {
        $spaceAttribute = SpaceAttribute::find($id);
        $spaceAttribute->name = $request->name;
        $spaceAttribute->save();
        Toastr::success('Cập nhật thành công');
        return redirect()->route('spaceAttributes.list');

    }

    /**
     * Remove the specified resource from storage.
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $spaceAttribute = SpaceAttribute::findOrFail($id);
        $spaceAttribute->delete();
        return response()->json(['success' => "Xóa thành công", 'tr' => 'tr_' . $id]);
    }

    public function deleteMultiple(Request $request)
    {
        $ids = $request->get('ids');
        if (isset($ids)) {
            SpaceAttribute::whereIn('id', $ids)->delete();
            Toastr::success('Xóa thành công');
        } else {
            Toastr::warning('Bạn cần chọn hàng cần xóa');
        }
        return redirect()->route('spaceAttributes.list');
    }

    public function searchAttribute(Request $request)
    {
        if($request->ajax()){
            $query = $request->get('query');
            if($query != ' '){
                $data = SpaceAttribute::query()->where('name', 'like', '%'.$query.'%')->get();
            }else{
                $data = SpaceAttribute::all();
            }
            if($data->count() > 0){
                $spaceAttributes = $data;
                return view('category::space_attributes.space-attribute-search', compact('spaceAttributes'));
            }
            else{
                return view('category::space_attributes.space-attribute-search-nodata');
            }
        }
    }

    public function searchAttributeTerm(Request $request)
    {
        if($request->ajax()){
            $query = $request->get('query');
            if($query != ' '){
                $data = SpaceTerm::query()->where('name', 'like', '%'.$query.'%')->get();
            }else{
                $data = SpaceTerm::all();
            }
            if($data->count() > 0){
                $terms = $data;
                return view('category::space_terms.search-space-attribute-term', compact('terms'));
            }
            else{
                return view('category::space_attributes.space-attribute-search-nodata');
            }
        }
    }
}
