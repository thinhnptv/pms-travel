<?php

namespace Modules\Category\Http\Controllers;

use Brian2694\Toastr\Facades\Toastr;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Modules\Category\Entities\Term;
use Modules\Category\Http\Requests\CreateTermRequest;
use Modules\Category\Http\Requests\UpdateTermRequest;

class TermController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('category::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('category::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CreateTermRequest $request)
    {
        $attribute_id = $request->attribute_id;
        $terms = new Term();
        $terms->attribute_id = $attribute_id;
        $terms->name = $request->name;
        $terms->icon = $request->icon;
//        $term->image = $imageName;
        $terms->save();
        Toastr::success('Thêm mới thành công');
        return redirect()->route('hotelAttributes.show_all_term', compact('attribute_id'));
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('category::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $terms = Term::find($id);
        return view('category::terms.edit-term', compact('terms'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdateTermRequest $request, $id)
    {
        $terms = Term::findorFail($id);
        $attribute_id = $terms->attribute_id;
        $terms->name = $request->name;
        $terms->icon = $request->icon;
//        $term->image = $imageName;
        $terms->save();
        Toastr::success('Cập nhật thành công');
        return redirect()->route('hotelAttributes.show_all_term', compact('attribute_id'));

    }

    /**
     * Remove the specified resource from storage.
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $terms = Term::findOrFail($id);
        $terms->delete();
        return response()->json(['success' => "Xóa thành công", 'tr' => 'tr_' . $id]);
    }

    public function deleteMultiple(Request $request)
    {
        $ids = $request->get('ids');
        $id = $request->id;
        if (isset($ids)) {
            Term::whereIn('id', $ids)->delete();
            Toastr::success('Xóa thành công');
        } else {
            Toastr::warning('Bạn cần chọn hàng để xóa');
        }
        return redirect()->back();
    }
}
