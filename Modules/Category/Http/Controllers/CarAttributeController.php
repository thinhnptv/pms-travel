<?php

namespace Modules\Category\Http\Controllers;

use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\Category\Entities\CarAttribute;
use Modules\Category\Entities\CarTerm;
use Modules\Category\Http\Requests\CreateCarAttributeRequest;
use Modules\Category\Http\Requests\UpdateCarAttributeRequest;

class CarAttributeController extends Controller
{
    public function index(Request $request)
    {
        $carAttributes = CarAttribute::where('status', 1);

        if (!empty($request->filled('name'))) {
           $carAttributes->where('name', 'like', '%'.$request->name.'%');
        }

        $carAttributes = $carAttributes->get();

        return view('category::car_attributes.car-attributes-index', compact('carAttributes'));
    }

    public function getAllTerm(Request $request, $id)
    {
        $carAttributes = CarAttribute::find($id);
        $carTerms = $carAttributes->carTerms;

        if (!empty($request->filled('name'))) {
            //dd($request->name);
            $carTerms = CarTerm::where('name', 'like', '%'.$request->name.'%')->get();
        }

        return view('category::car_terms.car-term-index', compact('carTerms', 'carAttributes'));

    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('category::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CreateCarAttributeRequest $request)
    {
        $carAttributes = new CarAttribute();
        $carAttributes->name = $request->name;
        $carAttributes->slug = $request->name;
        $carAttributes->save();
        Toastr::success('Thêm mới thành công');
        return redirect()->route('carAttributes.list');

    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('category::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $carAttributes = CarAttribute::findOrFail($id);

        return view('category::car_attributes.edit-car-attribute', compact('carAttributes'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdateCarAttributeRequest $request, $id)
    {
        $carAttributes = CarAttribute::find($id);
        $carAttributes->name = $request->name;
        $carAttributes->save();
        Toastr::success('Cập nhật thành công');
        return redirect()->route('carAttributes.list');

    }

    /**
     * Remove the specified resource from storage.
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $carAttributes = CarAttribute::findOrFail($id);
        $carAttributes->delete();
        return response()->json(['success' => "Xóa thành công.", 'tr' => 'tr_' . $id]);
    }

    public function deleteMultiple(Request $request)
    {
        $ids = $request->get('ids');
        if (isset($ids)) {
            CarAttribute::whereIn('id', $ids)->delete();
            Toastr::success('Xóa thành công');
        } else {
            Toastr::warning('Bạn cần chọn hàng để xóa');
        }
        return redirect()->route('carAttributes.list');
    }
}
