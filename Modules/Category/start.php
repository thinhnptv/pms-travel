<?php

/*
|--------------------------------------------------------------------------
| Register Namespaces and Routes
|--------------------------------------------------------------------------
|
| When your module starts, this file is executed automatically. By default
| it will only load the module's route file. However, you can expand on
| it to load anything else from the module, such as a class or view.
|
*/

if (!app()->routesAreCached()) {
    require __DIR__ . '/Http/routes.php';
}

function showLocationCategories($param, $parent = 0, $str = '')
{
    if($param->count()>0){
        foreach ($param as $key =>  $val)
        {
            if ($val['parent_id'] == $parent)
            {
                $id=$val['id'];
                $parent_id=$val['parent_id'];
                $name=$val['name'];
                $slug=$val['slug'];
                $route = "location/edit/$id";
                $date = $val['created_at'];
                $status = $val['status'];
                $date = $val['created_at'];

                echo "<tr id=$id>";
                echo "<td><input type='checkbox' class='sub-chk' name='ids[]' value='$id'></td>";
                echo "<td><a href='$route'>$str $name</a></td>";
                echo "<td>$slug</td>";
                if($status == 1){
                    echo  "<td><span class='badge badge-success'>Publish</span></td>";
                }else{
                    echo "<td><span class='badge badge-dark'>Draft</span></td>";
                }
                echo "<td>$date</td>";
                echo '</tr>';
                showLocationCategories($param, $id, $str.'-');
            }
        }
    }
}

function showTourCategories($param, $parent = 0, $str = '')
{
    if($param->count()>0){
        foreach ($param as $key =>  $val)
        {
            if ($val['parent_id'] == $parent)
            {
                $id=$val['id'];
                $parent_id=$val['parent_id'];
                $name=$val['name'];
                $slug=$val['slug'];
                $route = "tour/edit/$id";
                $date = $val['created_at'];
                $status = $val['status'];
                $date = $val['created_at'];

                echo "<tr id=$id>";
                echo "<td><input type='checkbox' class='sub-chk' name='ids[]' value='$id'></td>";
                echo "<td><a href='$route'>$str $name</a></td>";
                echo "<td>$slug</td>";
                if($status == 1){
                    echo  "<td><span class='badge badge-success'>Publish</span></td>";
                }else{
                    echo "<td><span class='badge badge-dark'>Draft</span></td>";
                }
                echo "<td>$date</td>";
                echo '</tr>';
                showLocationCategories($param, $id, $str.'-');
            }
        }
    }
}

function showPostCategories($param, $parent = 0, $str = '')
{
    if($param->count()>0){
        foreach ($param as $key =>  $val)
        {
            if ($val['parent_id'] == $parent)
            {
                $id = $val['id'];
                $parent_id = $val['parent_id'];
                $name = $val['name'];
                $slug = $val['slug'];
                $route = "post/edit/$id";
                $date = $val['created_at']->format('d-m-Y');
                $status = $val['status'];
                $date = $val['created_at']->format('d-m-Y');
                echo "<tr id=$id>";
                echo "<td><input type='checkbox' class='sub-chk' name='ids[]' value='$id'></td>";
                echo "<td><a href='$route'>$str $name</a></td>";
                echo "<td>$slug</td>";
                if($status == 1){
                    echo  "<td><span class='badge badge-success'>Publish</span></td>";
                }else{
                    echo "<td><span class='badge badge-dark'>Draft</span></td>";
                }
                echo "<td>$date</td>";
                echo '</tr>';
                showPostCategories($param, $id, $str.'-');
            }
        }
    }
}

function category_parent($param, $parent = 0, $str = "", $select = 0)
{
    foreach ($param as $key => $val) {
        $id = $val['id'];
        $name = $val['name'];
        if ($val["parent_id"] == $parent) {
            if ($select != 0 && $id == $select) {
                echo "<option value='$id' selected='selected'>$str $name</option>";
            } else {
                echo "<option value='$id'>$str $name</option>";
            }
            category_parent($param, $id, $str . "-");
        }

    }
}
