<?php

namespace Modules\Category\Entities;

use Illuminate\Database\Eloquent\Model;

use Modules\Hotel\Entities\Hotel;
use Modules\Space\Entities\Space;
use Modules\Car\Entities\Car;
use Modules\Tour\Entities\Tour;
use Modules\Tour\Entities\BookingTour;

class Location extends Model
{
    protected $dateFormat = 'U';
    protected $table = 'locations';
    protected $fillable = ['name', 'slug', 'status', 'description'];

    public function hotel()
    {
        return $this->hasMany(Hotel::class, 'location_id', 'id');
    }
    public function car()
    {
        return $this->hasMany(Car::class, 'location_id', 'id');
    }
    public function tour()
    {
        return $this->hasMany(Tour::class, 'location_id', 'id');
    }
    public function bookingTour()
    {
        return $this->hasManyThrough(
            BookingTour::Class, Tour::class,
            'location_id', 'tour_id', 'id' , 'id'
        );
    }
    public function space()
    {
        return $this->hasMany(Space::class);
    }
}


