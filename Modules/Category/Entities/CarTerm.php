<?php

namespace Modules\Category\Entities;

use Illuminate\Database\Eloquent\Model;

class CarTerm extends Model
{
    protected $dateFormat = "U";
    protected $table = "car_terms";
    protected $fillable = ['name', 'image'];

    public function carAttribute()
    {
        return $this->belongsTo(CarAttribute::class);
    }
}
