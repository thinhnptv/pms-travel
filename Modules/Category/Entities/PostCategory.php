<?php

namespace Modules\Category\Entities;

use Illuminate\Database\Eloquent\Model;

class PostCategory extends Model
{
    protected $dateFormat = 'U';
    protected $table = 'post_categories';
    protected $fillable = ['name', 'status'];

    public function subcategories()
    {
        return $this->hasMany(self::class, 'parent_id');
    }
}
