<?php

namespace Modules\Category\Entities;

use Illuminate\Database\Eloquent\Model;

class HotelAttribute extends Model
{
    protected $table = 'hotel_attributes';
    protected $dateFormat = 'U';
    protected $fillable = ['name'];

    public function terms(){
        return $this->hasMany(Term::class,'attribute_id');
    }
}
