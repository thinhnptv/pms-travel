<?php

namespace Modules\Category\Entities;

use Illuminate\Database\Eloquent\Model;

class SpaceAttribute extends Model
{
    protected $dateFormat ="U";
    protected $table = 'space_attributes';
    protected $fillable = ['name','slug','status'];

    public function spaceTerms(){
        return $this->hasMany(SpaceTerm::class,'attribute_id');
    }
}
