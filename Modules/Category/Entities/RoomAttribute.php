<?php

namespace Modules\Category\Entities;

use Illuminate\Database\Eloquent\Model;

class RoomAttribute extends Model
{
    protected $dateFormat = "U";
    protected $fillable = ['name', 'slug'];
    protected $table = 'room_attributes';

    public function roomTerms()
    {
        return $this->hasMany(Roomterm::class, 'attribute_id');
    }
}
