<?php

namespace Modules\Category\Entities;

use Illuminate\Database\Eloquent\Model;

class CarAttribute extends Model
{
    protected $dateFormat = "U";
    protected $table = 'car_attributes';
    protected $fillable = ['name', 'slug', 'status'];

    public function carTerms()
    {
        return $this->hasMany(CarTerm::class, 'attribute_id');
    }
}
