<?php

namespace Modules\Category\Entities;

use Illuminate\Database\Eloquent\Model;

class TourTerm extends Model
{
    protected $table = 'tour_terms';
    // protected $dateFormat = "U";
    protected $fillable = ['attribute_id', 'name', 'icon', 'image', 'created_at', 'updated_at'];

    public function tourAttribute()
    {
        return $this->belongsTo(TourAttribute::class);
    }
}
