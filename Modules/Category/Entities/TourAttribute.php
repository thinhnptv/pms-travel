<?php

namespace Modules\Category\Entities;

use Illuminate\Database\Eloquent\Model;

class TourAttribute extends Model
{
    protected $table = 'tour_attributes';
    // protected $dateFormat = 'U';
    protected $fillable = ['name', 'slug', 'status', 'created_at', 'updated_at'];

    public function terms(){
        return $this->hasMany(TourTerm::class,'attribute_id');
    }
}
