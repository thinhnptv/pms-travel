<?php

namespace Modules\Category\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Space\Entities\Space;

class SpaceTerm extends Model
{
    protected $dateFormat ="U";
    protected $table = 'space_terms';
    protected $fillable = ['name','icon','iamge'];
    public function spaceAttribute(){
        return $this->belongsTo(SpaceAttribute::class);
    }
    public function typeSpaces()
    {
        return $this->belongsToMany(Space::class);
    }

    public function amenitySpaces()
    {
        return $this->belongsToMany(Space::class);
    }
}

