<?php

namespace Modules\Category\Entities;

use Illuminate\Database\Eloquent\Model;

class Roomterm extends Model
{
    protected $dateFormat = "U";
    protected $fillable = ['name', 'icon', 'image'];
    protected $table = 'room_terms';

    public function roomAttribute()
    {
        return $this->belongsTo(RoomAttribute::class);
    }
}
