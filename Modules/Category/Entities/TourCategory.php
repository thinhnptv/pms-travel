<?php

namespace Modules\Category\Entities;

use Illuminate\Database\Eloquent\Model;

class TourCategory extends Model
{
    protected $dateFormat = 'U';
    protected $table = 'tour_categories';
    protected $fillable = ['name', 'status'];

    public function subcategories()
    {
        return $this->hasMany(self::class, 'parent_id');
    }

}
