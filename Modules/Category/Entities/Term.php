<?php

namespace Modules\Category\Entities;

use Illuminate\Database\Eloquent\Model;

class Term extends Model
{
    protected $dateFormat= "U";
    protected $fillable = [];
    protected $table = 'terms';
    public function hotel_attributes(){
        return $this->belongsTo(HotelAttribute::class);
    }
}
