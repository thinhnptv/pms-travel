<?php

namespace Modules\Category\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Category\Entities\HotelAttribute;

class SeedHotelAttributesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $attributes = [
            [
                'name' => 'Property type',
                'created_at' => 230142020,
                'updated_at' => 230142020,
            ],
            [
                'name' => '	Facilities',
                'created_at' => 230142020,
                'updated_at' => 230142020,
            ],
            [
                'name' => 'Hotel Service',
                'created_at' => 230142020,
                'updated_at' => 230142020,
            ],
        ];
            $new_attribute = HotelAttribute::insert($attributes);
    }
}
