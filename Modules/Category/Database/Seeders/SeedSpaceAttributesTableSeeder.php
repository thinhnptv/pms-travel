<?php

namespace Modules\Category\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Category\Entities\SpaceAttribute;

class SeedSpaceAttributesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $attributes = [
            [
                'name' => 'Space Type',
                'status' => true,
                'slug' => 'space-type',
                'created_at' => 23042020,
                'updated_at' => 23042020,
            ],
            [
                'name' => 'Amenities',
                'status' => false,
                'slug' => 'amenities',
                'created_at' => 23042020,
                'updated_at' => 23042020,
            ],
        ];
        $newAttribute = SpaceAttribute::insert($attributes);
    }
}
