<?php

namespace Modules\Category\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Category\Entities\CarAttribute;

class SeedCarAttributesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $attributes = [
            [
                'name' => 'Car Type',
                'status' => true,
                'slug' => 'car-type',
                'created_at' => 23042020,
                'updated_at' => 23042020,
            ],
            [
                'name' => '	Car Features',
                'status' => false,
                'slug' => '	car-features',
                'created_at' => 23042020,
                'updated_at' => 23042020,
            ],
        ];
            $newAttribute = CarAttribute::insert($attributes);
    }
}
