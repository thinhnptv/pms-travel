<?php

namespace Modules\Category\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Category\Entities\Term;

class SeedTermsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $terms = [
            [
                'attribute_id' => 1,
                'name' => 'Apartments',
                'image' => 'Adventure Travel_image',
                'icon' => 'fa fa-facebook',
                'created_at' => 230142020,
                'updated_at' => 230142020,
            ],
            [
                'attribute_id' => 1,
                'name' => '	Hotels',
                'image' => 'City trips_iamge',
                'icon' => 'fa fa-google',
                'created_at' => 230142020,
                'updated_at' => 230142020,
            ],
            [
                'attribute_id' => 1,
                'name' => 'Homestays',
                'image' => 'Ecotourism_image',
                'icon' => 'fa fa-twitter',
                'created_at' => 230142020,
                'updated_at' => 230142020,
            ],
            [
                'attribute_id' => 1,
                'name' => 'Villas',
                'image' => 'Villas_image',
                'icon' => 'fa fa-twitter',
                'created_at' => 230142020,
                'updated_at' => 230142020,
            ],
        ];
        $new_attribute = Term::insert($terms);
    }
}
