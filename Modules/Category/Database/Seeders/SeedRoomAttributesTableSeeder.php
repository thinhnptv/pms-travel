<?php

namespace Modules\Category\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Category\Entities\RoomAttribute;

class SeedRoomAttributesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $attributes = [
            [
                'name' => 'Room Amenitiesl',
                'slug'=> 'adventure-travel',
                'status' => 1,
            ],

        ];
        foreach($attributes as $attribute){
            $new_attribute = RoomAttribute::create(['name' => $attribute['name'],'slug'=>$attribute['slug'],'status'=>$attribute['status']]);
        }


    }
}
