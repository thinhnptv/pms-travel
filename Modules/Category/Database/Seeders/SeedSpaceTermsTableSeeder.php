<?php

namespace Modules\Category\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Category\Entities\SpaceTerm;

class SeedSpaceTermsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        $terms = [
            [
                'attribute_id' => 1,
                'name' => 'Auditorium',
                'image' => 'Auditorium_image',
                'icon' => 'fa fa-facebook',
                'created_at' => 23042020,
                'updated_at' => 23042020,
            ],
            [
                'attribute_id' => 1,
                'name' => '	Bar',
                'image' => 'Bar_iamge',
                'icon' => 'fa fa-google',
                'created_at' => 23042020,
                'updated_at' => 23042020,
            ],
            [
                'attribute_id' => 1,
                'name' => 'Ballroom',
                'image' => 'Ballroom_image',
                'icon' => 'fa fa-twitter',
                'created_at' => 23042020,
                'updated_at' => 23042020,
            ],
            [
                'attribute_id' => 1,
                'name' => 'Dance Studio',
                'image' => 'Dance-studio_image',
                'icon' => 'fa fa-twitter',
                'created_at' => 23042020,
                'updated_at' => 23042020,
            ],
            [
                'attribute_id' => 1,
                'name' => 'Office',
                'image' => 'Office_image',
                'icon' => 'fa fa-twitter',
                'created_at' => 23042020,
                'updated_at' => 23042020,
            ],
        ];
        $newTerm = SpaceTerm::insert($terms);
    }
}
