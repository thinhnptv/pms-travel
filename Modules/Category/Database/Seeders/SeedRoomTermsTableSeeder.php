<?php

namespace Modules\Category\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Category\Entities\Roomterm;

class SeedRoomTermsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $terms = [
            [
                'attribute_id'=>1,
                'name' => '	Wake-up call',
                'image' => 'Adventure-Travel_image',
                'icon' =>'fa fa-facebook',
            ],
            [
                'attribute_id'=>1,
                'name' => '	Flat Tv',
                'image' => 'Flat-Tv.image',
                'icon' =>'fa fa-twiter',
            ],
            [
                'attribute_id'=>1,
                'name' => 'Internet – Wifi',
                'image' => 'Ecotourism_image',
                'icon' =>'fa fa-google',
            ],
            [
                'attribute_id'=>1,
                'name' => '	Coffee and tea',
                'image' => 'Coffee-and-tea-image',
                'icon' =>'fa fa-tourims',
            ],


        ];
        foreach($terms as $term){
            $new_attribute = RoomTerm::create(['attribute_id'=>$term['attribute_id'],'name' => $term['name'],'image'=>$term['image'],'icon' => $term['icon']]);
        }

    }
}
