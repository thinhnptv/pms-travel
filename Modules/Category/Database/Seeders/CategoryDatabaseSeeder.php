<?php

namespace Modules\Category\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class CategoryDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

         $this->call(SeedRoomAttributesTableSeeder::class);
         $this->call(SeedRoomTermsTableSeeder::class);
         $this->call(SeedCarAttributesTableSeeder::class);
         $this->call(SeedCarTermsTableSeeder::class);
         $this->call(SeedSpaceAttributesTableSeeder::class);
         $this->call(SeedSpaceTermsTableSeeder::class);
         $this->call(SeedHotelAttributesTableSeeder::class);
         $this->call(SeedTermsTableSeeder::class);
    }
    
}
