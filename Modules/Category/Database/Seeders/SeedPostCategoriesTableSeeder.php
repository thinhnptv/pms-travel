<?php

namespace Modules\Category\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Category\Entities\PostCategory;

class SeedPostCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $categories = [
            [
                'name' => 'Adventure Travel',
                'status' => 1,
                'slug' => 'adventure-travel',
                'parent_id' => null,
                'created_at' => 23042020,
                'updated_at' => 23042020,
            ],
            [
                'name' => 'City trips',
                'status' => 1,
                'slug' => 'city-trips',
                'parent_id' => 1,
                'created_at' => 23042020,
                'updated_at' => 23042020,
            ],
            [
                'name' => 'Ecotourism',
                'status' => 1,
                'slug' => 'ecotourism',
                'parent_id' => 1,
                'created_at' => 23042020,
                'updated_at' => 23042020,
            ],
            [
                'name' => 'Escorted Tour',
                'status' => 0,
                'slug' => 'escorted-tour',
                'parent_id' => 1,
                'created_at' => 23042020,
                'updated_at' => 23042020,

            ],
        ];
        $new_category = PostCategory::insert($categories);

    }
}
