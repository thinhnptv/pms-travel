<?php

namespace Modules\Category\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Category\Entities\CarTerm;

class SeedCarTermsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $terms = [
            [
                'attribute_id' => 1,
                'name' => '	Convertibles',
                'image' => 'Convertibles_image',
                'created_at' => 23042020,
                'updated_at' => 23042020,
            ],
            [
                'attribute_id' => 1,
                'name' => 'Coupes',
                'image' => 'Coupes_iamge',
                'created_at' => 23042020,
                'updated_at' => 23042020,
            ],
            [
                'attribute_id' => 1,
                'name' => 'Hatchbacks',
                'image' => 'Hatchbacks_image',
                'created_at' => 23042020,
                'updated_at' => 23042020,
            ],
            [
                'attribute_id' => 1,
                'name' => '	Minivans',
                'image' => 'Minivans_image',
                'created_at' => 23042020,
                'updated_at' => 23042020,
            ],
        ];
        foreach ($terms as $term) {
            $newTerm = CarTerm::insert($terms);
        }


    }
}

