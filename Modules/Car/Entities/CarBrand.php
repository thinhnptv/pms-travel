<?php

namespace Modules\Car\Entities;

use Illuminate\Database\Eloquent\Model;

class CarBrand extends Model
{
    protected $table = 'car_brands';
    protected $fillable = [];

    public function cars()
    {
        return $this->hasMany(Car::class, 'brand_id', 'id');
    }
}
