<?php

namespace Modules\Car\Entities;

use Illuminate\Database\Eloquent\Model;

class ExtraPriceBooked extends Model
{
    protected $table = 'extra_price_bookeds';
    protected $fillable = ['id', 'title', 'price', 'booking_car_id', 'created_at', 'updated_at'];
}
