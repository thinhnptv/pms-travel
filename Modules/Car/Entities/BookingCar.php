<?php

namespace Modules\Car\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\User\Entities\User;
use Modules\Car\Entities\Car;
use Modules\Car\Entities\ExtraPriceBooked;

class BookingCar extends Model
{
    protected $table = 'booking_cars';
    protected $fillable = ['id', 'car_id', 'user_id', 'status', 'payment_method', 'start_date', 'end_date', 'price', 'number', 'paid', 'special_requirements', 'created_at', 'updated_at'];

    public function user() 
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
    public function car() 
    {
        return $this->belongsTo(Car::class, 'car_id', 'id');
    }
    public function extraPriceBooked() 
    {
        return $this->hasMany(ExtraPriceBooked::class, 'booking_car_id', 'id');
    }

}
