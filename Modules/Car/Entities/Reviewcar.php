<?php

namespace Modules\Car\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\User\Entities\User;
use Modules\Car\Entities\Car;

class Reviewcar extends Model
{
    protected $table = 'reviewcars';
    protected $fillable = ['id', 'title', 'content', 'status', 'equipment', 'comfortable', 'climate_control', 'facility', 'aftercare', 'average', 'car_id', 'user_id', 'created_at', 'updated_at'];

    public function user() 
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
    public function car() 
    {
        return $this->belongsTo(Car::class, 'car_id', 'id');
    }
}
