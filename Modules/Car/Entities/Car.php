<?php

namespace Modules\Car\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\User\Entities\User;
use Modules\Category\Entities\Location;
use Modules\Car\Entities\Reviewcar;
use Modules\Car\Entities\Faqcar;
use Modules\Car\Entities\Extrapricecar;
use Modules\Car\Entities\Wishlistcar;
use Modules\Car\Entities\CarAvailability;
use Modules\Category\Entities\CarTerm;

class Car extends Model
{
    protected $table = 'cars';
    protected $fillable = ['id', 'title', 'content', 'video', 'thumnail', 'banner', 'album', 'passenger', 'gear_shift', 'baggage', 'quantily', 'price', 'sale', 'door', 'location_id', 'address', 'latitu', 'longtitu', 'mapzoom', 'seo_title', 'seo_description', 'slug', 'featured', 'deafault_state', 'status', 'vendor_id', 'created_at', 'updated_at'];

    public function user()
    {
        return $this->belongsTo(User::class, 'vendor_id', 'id');
    }
    public function location()
    {
        return $this->belongsTo(Location::class, 'location_id', 'id');
    }
    public function reviewcar()
    {
        return $this->hasMany(Reviewcar::class, 'car_id', 'id');
    }
    public function wishlistCar()
    {
        return $this->hasMany(Wishlistcar::class, 'car_id', 'id');
    }
    public function termtype()
    {
        return $this->belongsToMany(CarTerm::class, 'car_termtype', 'car_id', 'termtype_id');
    }
    public function termfeature()
    {
        return $this->belongsToMany(CarTerm::class, 'car_termfeature', 'car_id', 'termfeature_id');
    }
    public function faq()
    {
        return $this->hasMany(Faqcar::class, 'car_id', 'id');
    }
    public function extraPrice()
    {
        return $this->hasMany(Extrapricecar::class, 'car_id', 'id');
    }
    public function availability()
    {
        return $this->hasMany(CarAvailability::class, 'car_id', 'id');
    }

    public function carBrand()
    {
        return $this->belongsTo(CarBrand::class, 'brand_id', 'id');
    }

    public function wishlistCarBy($user)
    {
        return $this->wishlistCar()->where('user_id',$user->id)->exists();
    }
}
