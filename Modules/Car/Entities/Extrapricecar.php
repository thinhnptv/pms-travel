<?php

namespace Modules\Car\Entities;

use Illuminate\Database\Eloquent\Model;

class Extrapricecar extends Model
{
    protected $table = 'extrapricecars';
    protected $fillable = ['id', 'title', 'price', 'type', 'car_id', 'created_at', 'updated_at'];
}
