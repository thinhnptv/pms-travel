<?php

namespace Modules\Car\Entities;

use Illuminate\Database\Eloquent\Model;

class Faqcar extends Model
{
    protected $table = 'faqcars';
    protected $fillable = ['id', 'title', 'content', 'car_id', 'created_at', 'updated_at'];
}
