<?php

namespace Modules\Car\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RequestCarBrand extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if(!isset(request()->brandId)){
            return [
                'name' => 'required|unique:car_brands,name',
                'image' => 'required',
            ];
        }else{
            return [
                'name' => 'required|unique:car_brands,name,'.request()->brandId,
                'image' => 'required',
            ];
        }

    }

    public function messages ()
    {

        if(!isset(request()->brandId)){
            return [
                'name.required' => "Tên thương hiệu không được để trống!",
                'name.unique' => 'Tên thương hiệu đã tồn tại',
                'image.required' => 'Ảnh không được để trống',
            ];
        }else{
            return [
                'name.required' => "Tên thương hiệu không được để trống!",
                'name.unique' => 'Tên thương hiệu đã tồn tại',
                'image.required' => 'Ảnh không được để trống'
            ];
        }

    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
