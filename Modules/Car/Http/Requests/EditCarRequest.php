<?php

namespace Modules\Car\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EditCarRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'=>['required'],
            'content' => ['required'],
            'album' => ['required'],
            'address' => ['required'],
            'avatar' => ['required'],
            'passenger' => ['required'],
            'gear_shift' => ['required'],
            'baggage' => ['required'],
            'door' => ['required'],
            'latitu' => ['required'],
            'longtitu' => ['required'],
            'price' => ['required'],
            'quantily' => [ 'required'],
            'sale' => [function ($attribute, $value, $fail) {
                if($this->price < $value) {
                    return $fail('Giá sale phải nhỏ hơn giá thường');
                }
            }],
        ];
    }
    public function messages ()
    {
        return [
            'title.required' => "Tên xe không được để trống!",
            'content.required' => 'Nội dung không được để trống',
            'album.required' => "Ablum không được để trống!",
            'address.required' => "Địa chỉ không được để trống!",
            'passenger.required' => "Số người không được để trống",
            'gear_shift.required' => 'Cần số không được để trống',
            'avatar.required' => "Ảnh không được để trống!",
            'baggage.required' => 'Hành lý không được để trống',
            'door.required' => 'Số ghê không được để trống',
            'latitu.required' => 'Vĩ độ không được để trống',
            'longtitu.required' => 'Kinh độ không được để trống',
            'price.required' =>'Giá không được để trống',
            'quantily.required' => 'Số lượng xe không được để trống',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
