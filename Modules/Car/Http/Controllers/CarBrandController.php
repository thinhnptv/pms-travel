<?php

namespace Modules\Car\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Car\Entities\Car;
use Modules\Car\Entities\CarBrand;
use Modules\Car\Http\Requests\RequestCarBrand;

class CarBrandController extends Controller
{
    protected $modelCarBrand;
    protected $modelCar;

    public function __construct(CarBrand $modelCarBrand, Car $modelCar)
    {
        $this->modelCarBrand = $modelCarBrand;
        $this->modelCar = $modelCar;
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('car::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $list_car_brand = $this->modelCarBrand->orderByDesc('id');
        if(!empty(request()->name)){
            $name = request()->name;
            $list_car_brand->where('name', 'like', '%'.$name.'%');
        }
        $list_car_brand =  $list_car_brand->paginate(15);
        return view('car::brands.create-car-brand',compact('list_car_brand'));
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(RequestCarBrand $request)
    {
        $string_image = " ";

        if ($request->input('image')) {
            $string_image = checkImage($request->input('image'));
        }

        $brand = new $this->modelCarBrand;

        $brand->name = strtoupper($request->name);
        $brand->image = $string_image;
        $brand->save();

        return redirect()->route('car.brand-store')->with(['success' => 'Thêm mới thành công']);
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('car::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($brandId)
    {
        $carBrand = $this->modelCarBrand->find($brandId);

        return view('car::brands.edit-car-brand',compact('carBrand'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(RequestCarBrand $request,$brandId)
    {
        $carBrand = $this->modelCarBrand->find($brandId);

        $string_image = " ";

        if ($request->input('image')) {
            $string_image = checkImage($request->input('image'));
        }
        $carBrand->name = strtoupper($request->name);
        $carBrand->image = $string_image;
        $carBrand->save();

        return redirect()->route('car.brand-store')->with(['success' => 'Sửa mới thành công']);
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {

    }

    public function deleteAll(Request $request )
    {
        $listId = $request->ids;

        $this->modelCarBrand->whereIn('id', $listId)->delete();

        return back()->with(['success' => 'Xóa thành công']);
    }
}
