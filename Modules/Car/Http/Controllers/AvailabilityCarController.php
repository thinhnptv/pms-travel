<?php

namespace Modules\Car\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Car\Entities\Car;
use Modules\Car\Entities\BookingCar;
use Modules\Car\Entities\CarAvailability;
use Carbon\Carbon;


class AvailabilityCarController extends Controller
{
    protected $modelCar;
    protected $modelBookingCar;
    protected $modelCarAvailability;

    public function __construct(
        Car $modelCar,
        BookingCar $modelBookingCar,
        CarAvailability $modelCarAvailability
    )
    {
        $this->modelCar = $modelCar;
        $this->modelBookingCar = $modelBookingCar;
        $this->modelCarAvailability = $modelCarAvailability;
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index($id)
    {
        $car = $this->modelCar::query()->findOrFail($id);
        // dd($car);
        $startMonth = now()->startOfMonth();
        $start = $startMonth->startOfWeek();
        $end = $start->copy()->addDays(41);

        $numberDay = $end->diffInDays($start);
        $arr = [];
        $arr[] = $start->format('Y-m-d');
        for ($i = 1; $i <= $numberDay ; $i++) {
            $day    = $start->addDays($i);
            $arr[] = $day->format('Y-m-d');
            $start->subDays($i);
        }
        // $bookingCarById = $this->modelBookingCar::query()->where('car_id', $id)->with('car')->get();
        //     // dd($bookingCarById);
        // $arrDate = [];
        //     foreach($bookingCarById as $item) {
        //         $arrDateItem = [];
        //         $start_time = Carbon::createFromFormat('Y-m-d', $item->start_date);
        //         $end_time = Carbon::createFromFormat('Y-m-d', $item->end_date);
        //         $arrDateItem[] = $item->start_date;
        //         $numberDate = $end_time->diffInDays($start_time);
        //         for ($i = 1; $i <= $numberDate ; $i++) {
        //             $day    = $start_time->addDays($i);
        //             $arrDateItem[] = $day->format('Y-m-d');
        //             $start_time->subDays($i);
        //         }
        //         $arrDate = array_merge($arrDate, $arrDateItem);
        //     }
            $bookingCarArr = $this->modelCarAvailability->where('car_id', $id)->get();
            $arrDate = [];
            foreach ($bookingCarArr as $item) {
                $arrDate[] = $item->date;
            }
            
            $arrResult = array_map(function($date) use ($bookingCarArr, $arrDate, $car) {
               
                    if(in_array($date, $arrDate)) {
                        foreach ($bookingCarArr as $items) {
                            if($date == $items->date) {
                                $event = $items->remaining . " x " . $items->price;
                                $remaining = $items->remaining;
                                $price = $items->price;
                                $carId = $items->car_id;
                            }
                        }
                        $result = [
                            'start' => $date,
                            'carId' => $carId,
                            'remaining' => $remaining,
                            'price' => $price,
                            'className' => 'bg-success',
                            'title' => $event       
                        ]; 
                        
                    }else{
                        if(!empty($car->sale) && $car->sale < $car->price) {
                            $event = $car->quantily . " x " . $car->sale;
                            $remaining = $car->quantily;
                            $price = $car->sale;
                            $carId = $car->id;
                        }else {
                            $event = $car->quantily . " x " . $car->price;
                            $remaining = $car->quantily;
                            $price = $car->price;
                            $carId = $car->id;
                        }

                        $result = [
                            'start' => $date,
                            'carId' => $carId,
                            'remaining' => $remaining,
                            'price' => $price,
                            'className' => 'bg-warning',
                            'title' => $event
                        ]; 
                        
                    }
                    return $result;
            }, $arr);
        $jsonResult = json_encode($arrResult);
        return view('car::availability.availability', compact(
            'car',
            'id',
            'bookingCarArr',
            'bookingCarId',
            'jsonResult'
        ));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function get($id, Request $request)
    {
        $car = $this->modelCar::query()->findOrFail($id);
        $start = Carbon::createFromFormat('Y-m-d', $request->start);
        $end = $start->copy()->addDays(41);

        $numberDay = $end->diffInDays($start);
        $arr = [];
        $arr[] = $start->format('Y-m-d');
        for ($i = 1; $i <= $numberDay ; $i++) {
            $day    = $start->addDays($i);
            $arr[] = $day->format('Y-m-d');
            $start->subDays($i);
        }
        // $bookingCarById = $this->modelBookingCar::query()->where('car_id', $id)->with('car')->get();
        //     // dd($bookingCarById);
        // $arrDate = [];
        //     foreach($bookingCarById as $item) {
        //         $arrDateItem = [];
        //         $start_time = Carbon::createFromFormat('Y-m-d', $item->start_date);
        //         $end_time = Carbon::createFromFormat('Y-m-d', $item->end_date);
        //         $arrDateItem[] = $item->start_date;
        //         $numberDate = $end_time->diffInDays($start_time);
        //         for ($i = 1; $i <= $numberDate ; $i++) {
        //             $day    = $start_time->addDays($i);
        //             $arrDateItem[] = $day->format('Y-m-d');
        //             $start_time->subDays($i);
        //         }
        //         $arrDate = array_merge($arrDate, $arrDateItem);
        //     }
            $bookingCarArr = $this->modelCarAvailability->where('car_id', $id)->get();
            $arrDate = [];
            foreach ($bookingCarArr as $item) {
                $arrDate[] = $item->date;
            }
            
            $arrResult = array_map(function($date) use ($bookingCarArr, $arrDate, $car) {
               
                    if(in_array($date, $arrDate)) {
                        foreach ($bookingCarArr as $items) {
                            if($date == $items->date) {
                                if($items->remaining == 0) {
                                    $event = 'Full book';
                                    $className = 'bg-danger';
                                }else{
                                    $event = $items->remaining . " x " . $items->price;
                                    $className = 'bg-success';
                                }
                                $remaining = $items->remaining;
                                $price = $items->price;
                                $carId = $items->car_id;
                            }
                        }
                        $result = [
                            'start' => $date,
                            'carId' => $carId,
                            'remaining' => $remaining,
                            'price' => $price,
                            'className' => $className,
                            'title' => $event       
                        ]; 
                        
                    }else{
                        if(!empty($car->sale) && $car->sale < $car->price) {
                            $event = $car->quantily . " x " . $car->sale;
                            $remaining = $car->quantily;
                            $price = $car->sale;
                            $carId = $car->id;
                        }else {
                            $event = $car->quantily . " x " . $car->price;
                            $remaining = $car->quantily;
                            $price = $car->price;
                            $carId = $car->id;
                        }

                        $result = [
                            'start' => $date,
                            'carId' => $carId,
                            'remaining' => $remaining,
                            'price' => $price,
                            'className' => 'bg-warning',
                            'title' => $event
                        ]; 
                        
                    }
                    return $result;
            }, $arr);
        // $jsonResult = json_encode($arrResult);
        // dd($arrResult);
        return response()->json($arrResult);
    }
    public function create()
    {
        return view('car::create');
    }
    public function save(Request $request)
    {
        if($request->remaining <= 0 ){
            $remaining = 0;
        }else {
            $remaining = $request->remaining;
        }
        if($request->price <= 0 ){
            $price = 0;
        }else {
            $price = $request->price;
        }
        $carAvailability = $this->modelCarAvailability->where('date', $request->day)->where('car_id', $request->carId)->first();
        if($carAvailability != null) {
            $availability = $this->modelCarAvailability->findOrFail($carAvailability->id);
            $availability->remaining = $remaining;
            $availability->price = $price;
            $availability->save();
            
        }else {
            $availability = new $this->modelCarAvailability;
            $availability->remaining = $remaining;
            $availability->car_id = $request->carId;
            $availability->date = $request->day;
            $availability->price = $price;
            $availability->save();
        }
        return Response()->json([
            'type' => 1,
            'mess' => 'Cập nhật thành công',
        ]);
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('car::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('car::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
}
