<?php

namespace Modules\Car\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Car\Entities\Car;
use Modules\Car\Entities\CarBrand;
use Modules\Car\Entities\Faqcar;
use Modules\Car\Entities\Extrapricecar;
use Modules\User\Entities\User;
use Modules\Category\Entities\Location;
use Modules\Category\Entities\CarTerm;
use Modules\Car\Http\Requests\CreateCarRequest;
use Modules\Car\Http\Requests\EditCarRequest;

class CarController extends Controller
{
    protected $modelCar;
    protected $modelUser;
    protected $modelLocation;
    protected $modelCarTerm;
    protected $modelFaqcar;
    protected $modelExtrapricecar;
    protected $modelCarBrand;

    public function __construct(Car $modelCar, User $modelUser, Location $modelLocation, CarTerm $modelCarTerm,Faqcar $modelFaqcar, Extrapricecar $modelExtrapricecar, CarBrand $modelCarBrand)
    {
        $this->modelCar = $modelCar;
        $this->modelUser = $modelUser;
        $this->modelLocation = $modelLocation;
        $this->modelCarTerm = $modelCarTerm;
        $this->modelFaqcar = $modelFaqcar;
        $this->modelExtrapricecar = $modelExtrapricecar;
        $this->modelCarBrand = $modelCarBrand;
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $list_car = $this->modelCar::query()->with('user')->with('location')->with('reviewcar')->orderByDesc('id');
        if(!empty(request()->name)) {
            $list_car->where('title', 'like', '%'.request()->name.'%');
        }
        $list_car = $list_car->paginate(15);

        return view('car::car.list', compact('list_car'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $vendor = $this->modelUser::query()->where('status', 1)->where('role_id', 3)->get();
        $location = $this->modelLocation::query()->where('status', 1)->get();
        $car_type = $this->modelCarTerm::query()->where('attribute_id', 1)->get();
        $car_features = $this->modelCarTerm::query()->where('attribute_id', 2)->get();
        $car_brand = $this->modelCarBrand::query()->get();
        return view('car::car.add',compact('vendor', 'location', 'car_type', 'car_features','car_brand'));
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */

    public function store(CreateCarRequest $request)
    {
        $str_avatar = '';
        $str_album = '';

        if ($request->input('avatar')) {
            $str_avatar = checkImage($request->input('avatar'));
        }
        if($request->input('album')) {
            $str_album = checkMultipleImage($request->input('album'));
        }
        $new_car = new $this->modelCar;
        $new_car->title = $request->title;
        $new_car->content = $request->content;
        $new_car->thumnail = $str_avatar;
        $new_car->album = $str_album;
        $new_car->passenger = $request->passenger;
        $new_car->gear_shift = $request->gear_shift;
        $new_car->baggage = $request->baggage;
        $new_car->door = $request->door;
        $new_car->quantily = $request->quantily;
        $new_car->price = $request->price;
        if(!empty($request->sale)) {
            $new_car->sale = $request->sale;
        }
        $new_car->location_id = $request->location_id;
        $new_car->brand_id = $request->car_brand_id;
        $new_car->car_form = $request->car_form;
        $new_car->address = $request->address;
        $new_car->latitu = $request->latitu;
        $new_car->longtitu = $request->longtitu;
        $new_car->mapzoom = $request->mapzoom;
        $new_car->seo_title = $request->seo_title;
        $new_car->seo_description = $request->seo_description;
        $new_car->slug = str_slug($request->title);
        $new_car->seo_description = $request->seo_description;
        if(empty($request->featured)) {
            $new_car->featured = 0;
        } else {
            $new_car->featured = $request->featured;
        }
        $new_car->deafault_state = $request->deafault_state;
        $new_car->status = $request->status;
        $new_car->vendor_id = $request->vendor_id;
        $new_car->save();

        if(!empty($request->faqs)){
            foreach($request->faqs as $item) {
                if(!empty($item['title']) && !empty($item['content'])) {
                    $new_faq = new $this->modelFaqcar;
                    $new_faq->title = $item['title'];
                    $new_faq->content = $item['content'];
                    $new_faq->car_id = $new_car->id;
                    $new_faq->save();
                }
            }
        }
        if(!empty($request->extra_price)){
            foreach($request->extra_price as $item) {
                if(!empty($item['title']) && !empty($item['price'])) {
                    $new_extraprice = new $this->modelExtrapricecar;
                    $new_extraprice->title = $item['title'];
                    $new_extraprice->price = $item['price'];
                    $new_extraprice->type = $item['type'];
                    $new_extraprice->car_id = $new_car->id;
                    $new_extraprice->save();
                }
            }
        }
        if(!empty($request->cartype)){
            $new_car->termtype()->attach($request->cartype);
        }
        if(!empty($request->carfeatures)){
            $new_car->termfeature()->attach($request->carfeatures);
        }
        return redirect()->route('car.list')->with(['success' => 'Thêm mới thành công']);
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('car::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {
        $vendor = $this->modelUser::query()->where('status', 1)->where('role_id', 3)->get();
        $location = $this->modelLocation::query()->where('status', 1)->get();
        $car_type = $this->modelCarTerm::query()->where('attribute_id', 1)->get();
        $car_features = $this->modelCarTerm::query()->where('attribute_id', 2)->get();
        $edit_car = $this->modelCar::query()->with('extraPrice')->with('faq')->with('user')->with('location','carBrand')->findOrFail($id);
        $car_brand = $this->modelCarBrand::query()->get();
        $car_features_selected = [];
        foreach($edit_car->termfeature as $item) {
            $car_features_selected[] = $item->id;
        }
        $car_type_selected = [];
        foreach($edit_car->termtype as $item) {
            $car_type_selected[] = $item->id;
        }
        return view('car::car.edit',compact('vendor', 'location', 'car_type', 'car_features', 'edit_car', 'car_features_selected', 'car_type_selected','car_brand'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update($id, EditCarRequest $request)
    {
        $str_avatar = '';
        $str_album = '';
        if ($request->input('avatar')) {
            $str_avatar = checkImage($request->input('avatar'));
        }
        if($request->input('album')) {
            $str_album = checkMultipleImage($request->input('album'));
        }

        $update_car = $this->modelCar->findOrFail($id);
        $update_car->title = $request->title;
        $update_car->content = $request->content;
        $update_car->thumnail = $str_avatar;
        $update_car->album = $str_album;
        $update_car->passenger = $request->passenger;
        $update_car->gear_shift = $request->gear_shift;
        $update_car->baggage = $request->baggage;
        $update_car->door = $request->door;
        $update_car->quantily = $request->quantily;
        $update_car->price = $request->price;
        if(!empty($request->sale)) {
            $update_car->sale = $request->sale;
        }
        $update_car->location_id = $request->location_id;
        $update_car->brand_id = $request->car_brand_id;
        $update_car->car_form = $request->car_form;
        $update_car->address = $request->address;
        $update_car->latitu = $request->latitu;
        $update_car->longtitu = $request->longtitu;
        $update_car->mapzoom = $request->mapzoom;
        $update_car->seo_title = $request->seo_title;
        $update_car->seo_description = $request->seo_description;
        $update_car->slug = str_slug($request->title);
        $update_car->seo_description = $request->seo_description;
        if(empty($request->featured)) {
            $update_car->featured = 0;
        } else {
            $update_car->featured = $request->featured;
        }
        $update_car->deafault_state = $request->deafault_state;
        $update_car->status = $request->status;
        $update_car->vendor_id = $request->vendor_id;
        $update_car->save();

        $delete_faqs = $this->modelFaqcar::query()->where('car_id', $id)->delete();
        if(!empty($request->faqs)){
            foreach($request->faqs as $item) {
                if(!empty($item['title']) && !empty($item['content'])) {
                    $new_faq = new $this->modelFaqcar;
                    $new_faq->title = $item['title'];
                    $new_faq->content = $item['content'];
                    $new_faq->car_id = $update_car->id;
                    $new_faq->save();
                }
            }
        }
        $delete_extra_price = $this->modelExtrapricecar::query()->where('car_id', $id)->delete();
        if(!empty($request->extra_price)){
            foreach($request->extra_price as $item) {
                if(!empty($item['title']) && !empty($item['price'])) {
                    $new_extraprice = new $this->modelExtrapricecar;
                    $new_extraprice->title = $item['title'];
                    $new_extraprice->price = $item['price'];
                    $new_extraprice->type = $item['type'];
                    $new_extraprice->car_id = $update_car->id;
                    $new_extraprice->save();
                }
            }
        }
        if(!empty($request->cartype)){
            $update_car->termtype()->sync($request->cartype);
        }else {
            $update_car->termtype()->detach();
        }
        if(!empty($request->carfeatures)){
            $update_car->termfeature()->sync($request->carfeatures);
        }else {
            $update_car->termfeature()->detach();
        }
        return redirect()->route('car.list')->with(['success' => 'Sửa thành công']);
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {
        $delete_car = $this->modelCar->findOrFail($id);
        $delete_car->termtype()->detach();
        $delete_car->termfeature()->detach();
        $delete_car->delete();
        return redirect()->route('car.list')->with('success', 'Xóa thành công');
    }
    public function deleteAll(Request $request)
    {
        $list_id = $request->list_id;
        foreach($list_id as $item){
            $delete_car = $this->modelCar->findOrFail($item);
            $delete_car->termtype()->detach();
            $delete_car->termfeature()->detach();
            $delete_car->delete();
        }
        return response()->json([
            'type' => 1,
            'mess' => "Xóa thành công"
        ]);
    }
    public function changeStatus($id)
    {
        $car = $this->modelCar->findorFail($id);
        if($car->status == 1) {
            $car->status = 0;
            $car->save();
            return Response()->json([
                'type' => 1,
                'mess' => 'Chuyển thành công',
            ]);
        }else {
            $car->status = 1;
            $car->save();
            return Response()->json([
                'type' => 2,
                'mess' => 'Chuyển thành công',
            ]);
        }
    }

}
