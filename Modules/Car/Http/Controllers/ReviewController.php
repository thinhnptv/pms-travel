<?php

namespace Modules\Car\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Car\Entities\Reviewcar;

class ReviewController extends Controller
{
    protected $modelReviewcar;

    public function __construct(Reviewcar $modelReviewcar)
    {
        $this->modelReviewcar = $modelReviewcar;
        
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $list_review = $this->modelReviewcar::query()->with('user')->with('car')->paginate(15);
        return view('car::review.list', compact('list_review'));
    }
    public function listReviewForCarId($id)
    {
        $list_review = $this->modelReviewcar::query()->with('user')->with('car')->where('car_id',$id)->paginate(15);
        return view('car::review.list', compact('list_review'));
    }
    /**
     * Show the form for creating a new resource. 
     * @return Response
     */
    public function create()
    {
        return view('car::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('car::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('car::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }

    public function deleteAll(Request $request)
    {
        $list_id = $request->list_id;
        foreach($list_id as $item){
            $delete_review_car = $this->modelReviewcar->findOrFail($item);
            $delete_review_car->delete();
        }
        return response()->json([
            'type' => 1,
            'mess' => "Xóa thành công"
        ]);
    }
    public function changeStatus(Request $request)
    {
        $list_id = $request->list_id;
        $selected_action = $request->selected_action;
        foreach($list_id as $item){
            $change_status_review = $this->modelReviewcar->findOrFail($item);
            $change_status_review->status = $selected_action;
            $change_status_review->save();
        }
        if($selected_action == 1) {
            return response()->json([
                'type' => 1,
                'mess' => "Chuyển thành công thành Approved"
            ]);
        }else if($selected_action == 2) {
            return response()->json([
                'type' => 2,
                'mess' => "Chuyển thành công thành Pending"
            ]);
        }else if($selected_action == 3) {
            return response()->json([
                'type' => 3,
                'mess' => "Chuyển thành công thành Spam"
            ]);
        }else if($selected_action == 4) {
            return response()->json([
                'type' => 4,
                'mess' => "Chuyển thành công thành Trash"
            ]);
        }
        
    }
}
