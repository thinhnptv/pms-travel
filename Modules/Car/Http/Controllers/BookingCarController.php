<?php

namespace Modules\Car\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Car\Entities\BookingCar;

class BookingCarController extends Controller
{
    protected $modelBookingCar;
    

    public function __construct(BookingCar $modelBookingCar)
    {
        $this->modelBookingCar = $modelBookingCar;
        
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $listBookingCar = $this->modelBookingCar::query()
        ->with([
                'user',
                'extraPriceBooked',
                'car'=> function($query){
                    $query->with('user');
                }
                ])->paginate(15);
        foreach($listBookingCar as $item) {
            $item->total = getTotalBookingCar($item);
        }
        // dd($listBookingCar);
        return view('car::booking.list',compact('listBookingCar'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('car::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('car::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('car::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
    public function deleteAll(Request $request)
    {
        $listId = $request->listId;
        foreach($listId as $item){
            $deleteBookingCar = $this->modelBookingCar->findOrFail($item);
            $deleteBookingCar->delete();
        }
        return response()->json([
            'type' => 1,
            'mess' => "Xóa thành công"
        ]);
    }
    public function changeStatus(Request $request)
    {
        $listId = $request->listId;
        $selectedAction = $request->selectedAction;
        foreach($listId as $item){
            $changeStatusBookingCar = $this->modelBookingCar->findOrFail($item);
            $changeStatusBookingCar->status = $selectedAction;
            $changeStatusBookingCar->save();
        }
        return response()->json([
            'type' => 1,
            'status' => showStatusBooking($selectedAction),
            'mess' => "Chuyển thành công"
        ]);
    }
}
