<?php

use Illuminate\Support\Facades\Route;

Route::group(['middleware' => ['web', 'adminLogin'], 'prefix' => 'admin/car', 'namespace' => 'Modules\Car\Http\Controllers'], function()
{
    Route::get('/', 'CarController@index')->name('car.list');
    Route::post('/change-status/{id}', 'CarController@changeStatus')->name('car.change_status');
    Route::get('/delete/{id}', 'CarController@destroy')->name('car.delete');
    Route::post('/delete-all', 'CarController@deleteAll')->name('car.delete_all');
    Route::get('/add', 'CarController@create')->name('car.get_add');
    Route::post('/add', 'CarController@store')->name('car.post_add');
    Route::get('/edit/{id}', 'CarController@edit')->name('car.get_edit');
    Route::post('/edit/{id}', 'CarController@update')->name('car.post_edit');

    Route::get('/list-review', 'ReviewController@index')->name('car.review.list');
    Route::get('/list-review-for-car/{id}', 'ReviewController@listReviewForCarId')->name('car.review.list_for_car');
    Route::post('/delete-review-all', 'ReviewController@deleteAll')->name('car.review.delete_all');
    Route::post('/change-review-status', 'ReviewController@changeStatus')->name('car.review.change_status');

    Route::get('/list-booking-car', 'BookingCarController@index')->name('car.booking.list');
    Route::post('/delete-booking-car-all', 'BookingCarController@deleteAll')->name('car.booking.delete_all');
    Route::post('/change-booking-car-status', 'BookingCarController@changeStatus')->name('car.booking.change_status');

    Route::get('/availability-car/{id}', 'AvailabilityCarController@index')->name('car.availability');
    Route::post('/save-availability-car', 'AvailabilityCarController@save')->name('car.availabilit_save');
    Route::get('/get-availability-car/{id}', 'AvailabilityCarController@get')->name('car.availabilit_get');

    Route::get('/car-brand','CarBrandController@index');
    Route::get('/create-car-brand','CarBrandController@create')->name('car.brand-list');
    Route::post('/create-car-brand','CarBrandController@store')->name('car.brand-store');
    Route::get('/edit-car-brand/{brandId}','CarBrandController@edit')->name('car.brand-edit');
    Route::post('/update-car-brand/{brandId}','CarBrandController@update')->name('car.brand-update');
    Route::delete('/delete-car-brand','CarBrandController@deleteAll')->name('car.delet-car-brand');
});
