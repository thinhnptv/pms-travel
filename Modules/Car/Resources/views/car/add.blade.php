@extends('base::layouts.master')
@section('css')
        <!-- Plugins css-->
        <link href="{{ URL::asset('assets/libs/select2/select2.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ URL::asset('assets/libs/summernote/summernote.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ URL::asset('assets/libs/dropzone/dropzone.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ URL::asset('assets/css/webt.css')}}" rel="stylesheet" type="text/css" />
@endsection
@section('content')
    <!-- Start Content-->
    <div class="container-fluid">
        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">UBold</a></li>
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Apps</a></li>
                            <li class="breadcrumb-item active">Add Car</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Add Car</h4>
                </div>
            </div>
        </div>
        <form action="" method="post" enctype="multipart/form-data">
           @csrf
            <div class="row">
                <div class="col-md-12 col-lg-9">
                    <div class="card p-2">
                        <div class="row">
                            <div class="col-md-12 col-lg-12">
                                <div class="form-group mb-3">
                                    <label for="title">Title <span class="text-danger">*</span></label>
                                    <input type="text" id="title" name="title" value="{{ old('title') }}" class="form-control" placeholder="Please enter name of the car">
                                    @if ($errors->has('title'))
                                        <p class="help is-danger">{{ $errors->first('title') }}</p>
                                    @endif
                                </div>
                                <div class="form-group mb-3">
                                    <label for="content">Content <span class="text-danger">*</span></label>
                                    <textarea class="form-control" id="content" name="content" rows="5" placeholder="Please enter content">{{ old('content') }}</textarea>
                                    @if ($errors->has('content'))
                                    <p class="help is-danger">{{ $errors->first('content') }}</p>
                                    @endif
                                </div>
                                <div class="form-group form-group-item mb-3">
                                    <label class="control-label">FAQs</label>
                                    <div class="g-items-header">
                                        <div class="row">
                                            <div class="col-md-5">Title</div>
                                            <div class="col-md-5">Content</div>
                                            <div class="col-md-1"></div>
                                        </div>
                                    </div>
                                    <div class="g-items g-items-faq">
                                        @php
                                        $i = 0;
                                @endphp
                                @if (!empty(old('faqs')))
                                    @foreach (old('faqs') as $item)
                                    <div class="item" data-number="{{ $i }}">
                                        <div class="row">
                                            <div class="col-md-5">
                                                <input type="text" name="faqs[{{ $i }}][title]" class="form-control" value="{{ $item['title'] }}" placeholder="Eg: Can I bring my pet?">
                                                </div>
                                            <div class="col-md-6">
                                                <textarea name="faqs[{{ $i }}][content]" class="form-control" placeholder="">{{ $item['content'] }}</textarea>
                                            </div>
                                            <div class="col-md-1">
                                                <span class="btn btn-danger btn-sm btn-remove-item"><i class="fa fa-trash"></i></span>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                @php
                                    $i++;
                                @endphp
                                @endif
                                    </div>
                                    <div class="text-right">
                                        <span class="btn btn-info btn-sm btn-add-item btn-add-item-faq"><i class="fe-plus-circle"></i> Add item</span>
                                    </div>
                                </div>
                                <div class="form-group form-group-item mb-3">
                                    <label class="control-label">Extra Price</label>
                                    <div class="g-items-header">
                                        <div class="row">
                                            <div class="col-md-5">Title</div>
                                            <div class="col-md-3">Price</div>
                                            <div class="col-md-3">Type</div>
                                            <div class="col-md-1"></div>
                                        </div>
                                    </div>
                                    <div class="g-items g-items-extra-price">
                                @php
                                    $j = 0;
                                @endphp
                                @if (!empty(old('extra_price')))
                                    @foreach (old('extra_price') as $item)
                                    <div class="item" data-number="{{ $j }}">
                                        <div class="row">
                                            <div class="col-md-5">
                                                <input type="text" name="extra_price[{{ $j }}][title]" class="form-control" placeholder="Extra price name" value="{{ $item['title'] }}">
                                            </div>
                                            <div class="col-md-3">
                                                <input type="number" min="0" name="extra_price[{{ $j }}][price]" class="form-control" value="{{ $item['price'] }}">
                                            </div>
                                            <div class="col-md-3">
                                                <select name="extra_price[{{ $j }}][type]" class="form-control">
                                                    <option value="0" {{ $item['type'] == 0 ? 'selected' : ' ' }}>One-time</option>
                                                    <option value="1" {{ $item['type'] == 1 ? 'selected' : ' ' }}>Per day</option>
                                                </select>
                                            </div>
                                            <div class="col-md-1">
                                                <span class="btn btn-danger btn-sm btn-remove-item"><i class="fa fa-trash"></i></span>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                @php
                                        $i++;
                                @endphp
                                @endif
                                    </div>
                                    <div class="text-right">
                                        <span class="btn btn-info btn-sm btn-add-item btn-add-item-extra-price"><i class="fe-plus-circle"></i> Add item</span>
                                    </div>
                                </div>

                                <div class="form-group mb-3">
                                    <label for="banner">Album <span class="text-danger">*</span></label>
                                    <div class="row">
                                        <div class="col-3">
                                            <span class="input-group-btn">
                                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bd-example-modal-album">Select album</button>
                                                <input type="hidden" name="album" id="album" value="{{ old('album') }}" >
                                            </span>
                                        </div>
                                    </div>
                                    <div id="preview_album" class="mt-3">
                                        <div class="row row_preview_album" id="row_preview_album">
                                            @if ($errors->has('album'))
                                                <p class="help is-danger">{{ $errors->first('album') }}</p>
                                            @endif
                                            @if (!empty( old('album')))
                                                @foreach (json_decode( old('album')) as $item)
                                                    <div class="col-3 mt-3">
                                                        <div class="box_imgg position-relative">
                                                            <img src="{{ $item }}" class="img-height-110" style="width:100%; height=110px;">
                                                            <i class="mdi mdi-close-circle-outline style_icon_remove style_icons_remove_album" title="delete"></i>
                                                        </div>
                                                    </div>
                                                @endforeach
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="panel">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Passenger <span class="text-danger">*</span></label>
                                                <input type="number" min="0" value="{{ old('passenger') }}" placeholder="Example: 3" name="passenger" class="form-control">
                                                @if ($errors->has('passenger'))
                                                    <p class="help is-danger">{{ $errors->first('passenger') }}</p>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Gear Shift <span class="text-danger">*</span></label>
                                                <input type="text" value="{{ old('gear_shift') }}" placeholder="Example: Auto" name="gear_shift" class="form-control">
                                                @if ($errors->has('gear_shift'))
                                                    <p class="help is-danger">{{ $errors->first('gear_shift') }}</p>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Baggage <span class="text-danger">*</span></label>
                                                <input type="number" min="0" value="{{ old('baggage') }}" placeholder="Example: 5" name="baggage" class="form-control">
                                                @if ($errors->has('baggage'))
                                                    <p class="help is-danger">{{ $errors->first('baggage') }}</p>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Door <span class="text-danger">*</span></label>
                                                <input type="number" min="0" value="{{ old('door') }}" placeholder="Example: 4" name="door" class="form-control">
                                                 @if ($errors->has('door'))
                                                    <p class="help is-danger">{{ $errors->first('door') }}</p>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group mb-3">
                                    <label for="quantily">Quantily <span class="text-danger">*</span></label>
                                    <input type="number" id="quantily" min="0" name="quantily" value="{{ old('quantily') }}" class="form-control" placeholder="Please enter number of the car">
                                    <p class="help is-danger">{{ $errors->first('quantily') }}</p>
                                </div>
                                <div class="form-group mb-3">
                                    <div class="row">
                                        <div class="col-6">
                                            <label for="price">Price <span class="text-danger">*</span></label>
                                            <input type="number" id="price" min="0" name="price" value="{{ old('price') }}" class="form-control" placeholder="Car Price">
                                            <p class="help is-danger">{{ $errors->first('price') }}</p>
                                        </div>
                                        <div class="col-6">
                                            <label for="sale">Sale</label>
                                            <input type="number" id="sale" name="sale" class="form-control" value="{{ old('sale') }}" placeholder="Car Sale">
                                            <p class="help is-danger">{{ $errors->first('sale') }}</p>
                                            <span><i>If the regular price is less than the discount , it will show the regular price</i></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group mb-3">
                                    <label for="location_id">Location</label>
                                    <select class="form-control" id="location_id" name="location_id">
                                        @foreach ($location as $item)
                                            <option value="{{ $item->id }}" {{ (old('location_id') == $item->id ) ? 'selected' : ' ' }}>{{ $item->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group mb-3">
                                    <label for="address">Address <span class="text-danger">*</span></label>
                                    <input type="text" id="address" value="{{ old('address') }}" name="address" class="form-control" placeholder="Please enter address">
                                    @if ($errors->has('address'))
                                        <p class="help is-danger">{{ $errors->first('address') }}</p>
                                    @endif
                                </div>
                                <div class="form-group mb-3">
                                    <label for="brand_id">Car Brand </label>
                                    <select class="form-control" id="brand_id" name="car_brand_id">
                                        <option value="">--Select car brand--</option>
                                        @foreach ($car_brand as $item)
                                            <option value="{{ $item->id }}" {{ (old('car_brand_id') == $item->id ) ? 'selected' : ' ' }}>{{ $item->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group mb-3">
                                    <label for="brand_id">Car form</label>
                                    <select class="form-control" id="car_form" name="car_form">
                                        <option value="2">Tự lái</option>
                                        <option value="1">Nhân viên lái</option>
                                    </select>
                                </div>
                                <h4 class="header-title mb-3">The geographic coordinate</h4>
                                <div class="row">
                                    <div class="col-8">
                                        <div class="form-group mb-3">
                                            <div id="gmaps-basic" class="gmaps"></div>
                                            <i>Click onto map to place Location address</i>
                                        </div>
                                    </div>
                                    <div class="col-4 ">
                                        <div class="form-group mb-3">
                                            <label for="latitu">Map Latitude: </label>
                                            <span class="text-danger">*</span>
                                            <input type="text" id="latitu" name="latitu" class="form-control" value="{{ old('latitu') }}">
                                            <p class="help is-danger">{{ $errors->first('latitu') }}</p>
                                        </div>
                                        <div class="form-group mb-3">
                                            <label for="longtitu">Map Longitude: </label>
                                            <span class="text-danger">*</span>
                                            <input type="text" id="longtitu" name="longtitu" class="form-control" value="{{ old('longtitu') }}">
                                            <p class="help is-danger">{{ $errors->first('longtitu') }}</p>
                                        </div>
                                        <div class="form-group mb-3">
                                            <label for="mapzoom">Map Zoom: </label>
                                            <input type="text" id="mapzoom" name="mapzoom" class="form-control" value="{{ old('mapzoom') }}">
                                            <p class="help is-danger">{{ $errors->first('mapzoom') }}</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group mb-3">
                                    <label for="seo_title">Seo Title</label>
                                    <input type="text" id="seo_title" name="seo_title" value="{{ old('seo_title') }}" class="form-control" placeholder="Please enter seo_title">
                                </div>
                                <div class="form-group mb-3">
                                    <label for="seo_description">Seo Description</label>
                                    <input type="text" id="seo_description" name="seo_description" value="{{ old('seo_description') }}" class="form-control" placeholder="Please enter seo_title">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 col-lg-3">
                    <div class="card p-2">
                        <div class="card-box text-center">
                            <span class="input-group-btn">
                                <button type="button" class="btn btn-warning" data-toggle="modal" data-target=".bd-example-modal-avatar">Select avatar <span class="text-danger">*</span></button>
                                <input type="hidden" name="avatar" id="avatar" value="{{ old('avatar') }}">
                                @if (!empty(old('avatar')))
                                <div class="box_imgg position-relative">
                                    <img src="{{ old('avatar') }}" id='show-img-banner' style="width:100%;">
                                    <i class="mdi mdi-close-circle-outline style_icon_remove style_icon_remove_banner" title="delete"></i>
                                </div>
                                @endif
                                @if ($errors->has('avatar'))
                                <p class="help is-danger">{{ $errors->first('avatar') }}</p>
                                @endif
                            </span>
                            <div id="preview_avatar" class="mt-3">

                            </div>
                        </div>
                        <div class="form-group mb-3">
                            <label for="status_car">Status</label>
                            <select class="form-control" id="status_car" name="status">
                                <option value="0" {{ (old('status') == 0) ? 'selected' : ' ' }}>Blocked</option>
                                <option value="1" {{ (old('status') == 1) ? 'selected' : ' ' }}>Publish</option>
                            </select>
                        </div>
                        <div class="form-group mb-3">
                            <label for="vendor_id">Vendor</label>
                            <select class="form-control" id="vendor_id" name="vendor_id">
                                <option value="">--Select User--</option>
                                @foreach ($vendor as $item)
                                    <option value="{{ $item->id }}">{{ $item->firstname }} {{ $item->lastname }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group mb-3">
                            <label >Car Featured</label>
                            <br />
                            <label>
                                <input type="checkbox" name="featured" value="1" {{ (old('featured') == 1 ) ? 'checked' : ' ' }}> Enable featured
                            </label>
                        </div>
                        <div class="form-group mb-3">
                            <label for="">Default State</label>
                            <select class="form-control" id="deafault_state" name="deafault_state">
                                <option value="0">Always available</option>
                                <option value="1">Only available on specific dates</option>
                            </select>
                        </div>
                        <div class="form-group mb-3">
                            <label for="">Attribute: Car Type</label>
                            <div class="terms-scrollable">
                                @foreach ($car_type as $item)
                                    <label class="term-item">
                                        <input type="checkbox" name="cartype[]" value="{{ $item->id }}"
                                        {{ (!empty(old('cartype')) && in_array($item->id, old('cartype'))) ? 'checked' : ' ' }}
                                        >
                                        <span class="term-name">{{ $item->name }}</span>
                                    </label>
                                @endforeach
                            </div>
                        </div>
                        <div class="form-group mb-3">
                            <label for="">Attribute: Car Features</label>
                            <div class="terms-scrollable">
                                @foreach ($car_features as $item)
                                    <label class="term-item">
                                        <input type="checkbox" name="carfeatures[]" value="{{ $item->id }}"
                                        {{ (!empty(old('carfeatures')) && in_array($item->id, old('carfeatures'))) ? 'checked' : ' ' }}
                                        >
                                        <span class="term-name">{{ $item->name }}</span>
                                    </label>
                                @endforeach
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <div class="text-center mb-3">
                                    <button type="submit" class="btn w-sm btn-success waves-effect waves-light">Save</button>
                                    <a href="{{route('car.list')}}" class="btn w-sm btn-warning waves-effect waves-light">Cancel</a>
                                    {{-- <button type="reset" class="btn w-sm btn-danger waves-effect waves-light">Reset</button> --}}
                                </div>
                            </div> <!-- end col -->
                        </div>
                    </div>
                </div>
            </div>
        <!-- end row -->
        </form>
    </div>
@section('js')
    <div class="modal fade bd-example-modal-avatar" id="modal-file" tabindex="-1" role="dialog" data-backdrop="false" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Manger Image</h5>
                    <button type="button" class="close" data-dismiss="modal"><i class="fas fa-times"></i></button>
                </div>
                <div class="modal-body">
                    <iframe src="{{ url('/') }}/filemanager/dialog.php?akey=4Ans6pfTs6A9DzHj8AZq1NVGKoYxpyoI8ivCN1bOow&field_id=avatar&multiple=0" frameborder="0" style="width: 100%; height: 500px; overflow-y: auto"></iframe>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade bd-example-modal-banner" id="modal-file-banner" tabindex="-1" role="dialog" data-backdrop="false" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Manger Image</h5>
                    <button type="button" class="close" data-dismiss="modal"><i class="fas fa-times"></i></button>
                </div>
                <div class="modal-body">
                    <iframe src="{{ url('/') }}/filemanager/dialog.php?akey=4Ans6pfTs6A9DzHj8AZq1NVGKoYxpyoI8ivCN1bOow&field_id=banner&multiple=0" frameborder="0" style="width: 100%; height: 500px; overflow-y: auto"></iframe>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade bd-example-modal-album" id="modal-file-album" tabindex="-1" role="dialog" data-backdrop="false" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Manger Image</h5>
                    <button type="button" class="close" data-dismiss="modal"><i class="fas fa-times"></i></button>
                </div>
                <div class="modal-body">
                    <iframe src="{{ url('/') }}/filemanager/dialog.php?akey=4Ans6pfTs6A9DzHj8AZq1NVGKoYxpyoI8ivCN1bOow&field_id=album&multiple=1" frameborder="0" style="width: 100%; height: 500px; overflow-y: auto"></iframe>
                </div>
            </div>
        </div>
    </div>
@stop
@endsection

@push('js')
    <!-- Summernote js -->
    <script src="{{ URL::asset('assets/libs/summernote/summernote.min.js') }}"></script>
    <!-- Select2 js-->
    <script src="{{ URL::asset('assets/libs/select2/select2.min.js') }}"></script>
    <!-- Dropzone file uploads-->
    <script src="{{ URL::asset('assets/libs/dropzone/dropzone.min.js') }}"></script>
    <!-- Init js -->
    <script>
        jQuery(document).ready(function(){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        // Summernote
        $('#content').summernote({
            height: 180,                 // set editor height
            minHeight: null,             // set minimum height of editor
            maxHeight: null,             // set maximum height of editor
            focus: false                 // set focus to editable area after initializing summernote
        });
        

    });
    </script>
    <script>

       $(document).on('hide.bs.modal', '.bd-example-modal-avatar', function () {
            var _img = $('input#avatar').val();
            if (!_img.length) {
                $('#preview_avatar').empty();
            } else {
                $('#preview_avatar').empty();
                $html = `
                    <div class="box_imgg position-relative">
                        <img src="" id='show-img-avatar' style="width:100%;">
                        <i class="fas fa-times-circle style_icon_remove style_icon_remove_avatar" title="delete"></i>
                    </div>
                `;
                $('#preview_avatar').append($html);
                $('#show-img-avatar').attr('src', _img);
            }
        });
        $(document).on('click', '.style_icon_remove_avatar', function() {
            $(this).parent('.box_imgg').hide('slow', function () {
                $(this).remove();
                $('#banner').val('');
            });
        });
       $(document).on('hide.bs.modal', '.bd-example-modal-banner', function () {
            var _img = $('input#banner').val();
            if (!_img.length) {
                $('#preview_banner').empty();
            } else {
                $('#preview_banner').empty();
                $html = `
                    <div class="box_imgg position-relative">
                        <img src="" id='show-img-banner' style="width:100%;">
                        <i class="fas fa-times-circle style_icon_remove style_icon_remove_banner" title="delete"></i>
                    </div>
                `;
                $('#preview_banner').append($html);
                $('#show-img-banner').attr('src', _img);
            }
        });
        $(document).on('click', '.style_icon_remove_banner', function() {
            $(this).parent('.box_imgg').hide('slow', function () {
                $(this).remove();
                $('#banner').val('');
            });
        });
       $(document).on('hide.bs.modal', '.bd-example-modal-album', function () {
            var _img = $('input#album').val();
            if (!_img.length) {
                $('#preview_album').empty();
            } else {
                if(_img[0] == '[') {
                    var array = JSON.parse(_img);
                    $('#row_preview_album').empty();
                    var html = '';
                    $.each(array, function( index, value ) {
                        html += `
                            <div class="col-3 mt-3">
                                <div class="box_imgg position-relative">
                                    <img src="${value}" class="img-height-110" style="width:100%; height=110px;">
                                    <i class="fas fa-times-circle style_icon_remove style_icons_remove_album" title="delete"></i>
                                </div>
                            </div>
                        `;
                    });
                    $('#row_preview_album').append(html);

                }else{

                    $html = `
                        <div class="col-3 mt-3">
                            <div class="box_imgg position-relative">
                                <img src="" id='show-img-album' style="width:100%;">
                                <i class="fas fa-times-circle style_icon_remove style_icons_remove_album" title="delete"></i>
                            </div>
                        </div>
                    `;
                    $('#row_preview_album').empty();
                    $('#row_preview_album').append($html);
                    $('#show-img-album').attr('src', _img);
                    var str_src = '';
                    str_src += "[\"";
                    str_src += _img;
                    str_src += "\"]";
                    $('#album').val(str_src);
                }

            }
        });
        $(document).on('click', '.style_icons_remove_album', function() {
            $(this).parents('.col-3').hide('slow', function () {
                $(this).remove();
                var arr_image = [];
                var str_src = '';
                $('.row_preview_album').find('.col-3').each(function () {
                    var str_image = '';
                    str_image += '"';
                    str_image += $(this).find('.img-height-110').attr('src');
                    str_image += '"';
                    arr_image.push(str_image);
                });
                str_src += "[";
                str_src += arr_image.toString();
                str_src += "]";
                if(arr_image.length == 0){
                    $('#album').val('');
                }else{
                    $('#album').val(str_src);
                }
            });
        });
        var i = 0 , j=0;
        $(document).on('click', '.btn-add-item-faq', function() {
            var html = `
                <div class="item" data-number="${i}">
                    <div class="row">
                        <div class="col-md-5">
                            <input type="text" name="faqs[${i}][title]" class="form-control" placeholder="Eg: Can I bring my pet?" required>
                            </div>
                        <div class="col-md-6">
                            <textarea name="faqs[${i}][content]" class="form-control" placeholder="" required></textarea>
                        </div>
                        <div class="col-md-1">
                            <span class="btn btn-danger btn-sm btn-remove-item"><i class="fa fa-trash"></i></span>
                        </div>
                    </div>
                </div>
            `;
            $('.g-items-faq').append(html);
            i++;
        });
        $(document).on('click', '.btn-add-item-extra-price', function() {
            var html = `
                <div class="item" data-number="${j}">
                    <div class="row">
                        <div class="col-md-5">
                            <input type="text" name="extra_price[${j}][title]" class="form-control" placeholder="Extra price name" required>
                        </div>
                        <div class="col-md-3">
                            <input type="number" min="0" name="extra_price[${j}][price]" class="form-control" value="" required>
                        </div>
                        <div class="col-md-3">
                            <select name="extra_price[${j}][type]" class="form-control" >
                                <option value="0">One-time</option>
                                <option value="1">Per day</option>
                            </select>
                        </div>
                        <div class="col-md-1">
                            <span class="btn btn-danger btn-sm btn-remove-item"><i class="fa fa-trash"></i></span>
                        </div>
                    </div>
                </div>
            `;
            $('.g-items-extra-price').append(html);
            j++;
        });
        $(document).on('click', '.btn-remove-item', function(){
            $(this).parents('.item').remove();
        });
    </script>
    <script>
        var bookingCore = {
            url: 'http://sandbox.bookingcore.org',
            map_provider: 'gmap',
            map_gmap_key: '',
            csrf: 'JMrM5NwcxQy6HWOmuo7LQ2kHPh7pGwfbOPpXxkue'
        };
    </script>

    <script src="https://maps.google.com/maps/api/js?key=AIzaSyDsucrEdmswqYrw0f6ej3bf4M4suDeRgNA"></script>
    <script src="{{ URL::asset('assets/libs/gmaps/gmaps.min.js') }}"></script>
    <script src="{{ URL::asset('assets/js/pages/google-maps.init.js') }}"></script>
    <script src="{{ URL::asset('assets/js/map-engine.js') }}"></script>
    <script>
        jQuery(function ($) {
            new BravoMapEngine('gmaps-basic', {
                fitBounds: true,
                center: [{{"51.505"}}, {{"-0.09"}}],
                zoom:{{"8"}},
                ready: function (engineMap) {
                    engineMap.on('click', function (dataLatLng) {
                        engineMap.clearMarkers();
                        console.log(dataLatLng);
                        
                        engineMap.addMarker(dataLatLng, {
                            icon_options: {}
                        });
                        $("input[name=latitu]").attr("value", dataLatLng[0]);
                        $("input[name=longtitu]").attr("value", dataLatLng[1]);
                    });
                    engineMap.on('zoom_changed', function (zoom) {
                        $("input[name=mapzoom]").attr("value", zoom);
                    })
                }
            });
        })
    </script>
@endpush

