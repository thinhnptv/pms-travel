@extends('base::layouts.master')
@section('css')
        <!-- Plugins css-->
        <link href="{{ URL::asset('assets/libs/select2/select2.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ URL::asset('assets/libs/summernote/summernote.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ URL::asset('assets/libs/dropzone/dropzone.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ URL::asset('assets/css/webt.css')}}" rel="stylesheet" type="text/css" />
@endsection
@section('content')
    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">UBold</a></li>
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Apps</a></li>
                            <li class="breadcrumb-item active">List Reviews Cars</li>
                        </ol>
                    </div>
                    <h4 class="page-title">List Reviews Cars</h4>
                </div>
            </div>
        </div>
        @if (session('success'))
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
        @endif
        <!-- end page title -->
    <div class="row">
        <div class="col-12">
            <div class="card p-2">
                <div class="row mb-2">
                    <div class="col-sm-2">
                        <select class="form-control selected_action">
                            <option value="0">Bulk Actions</option>
                            <option value="1">Approved</option>
                            <option value="2">Pending</option>
                            <option value="3">Spam</option>
                            <option value="4">Trash</option>
                            <option value="5">Delete</option>
                        </select>
                    </div>
                    <div class="col-sm-2"><a href="javascript:void(0)" class="btn btn-primary waves-effect waves-light mb-2 btn_change_action" >Apply</a></div>
                    <div class="col-sm-8">
                        <div class="text-sm-right">
                            
                        </div>
                    </div><!-- end col-->
                </div>
                <div class="table-responsive">
                    <table class="table table-bordered  mb-0">
                        <thead>
                            <tr>
                                <th width="50px"><input type="checkbox" id="master" class="check_master"></th>
                                <th class="text-center">STT</th>
                                <th class="text-center">Author</th>
                                <th class="text-center">Review Content</th>
                                <th class="text-center">Average rating points</th>
                                <th class="text-center">In Response To</th>
                                <th class="text-center">Status</th>
                                <th class="text-center">Submitted</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                                   $stt = 1;
                            @endphp
                            @foreach ($list_review as $item)
                                <tr>
                                    <td><input type="checkbox" class="sub-chk" data-id="{{ $item->id }}"></td>
                                    <th class="text-center" scope="row">{{ $stt }}</th>
                                    @if(!empty($item->user->id))
                                        <td>{{ optional($item->user)->firstname }} {{ optional($item->user)->lastname }}</td>
                                    @else
                                        <td>[Author deleted]</td>
                                    @endif
                                    <td><h5>{{ $item->title }}</h5>{{$item->content}}</td>
                                    <td>
                                        <table class="table table-borderless">
                                            <thead class="thead-light">
                                                <tr>
                                                    <th>Average:</th>
                                                    <th>
                                                        @if (!empty($item->average))
                                                            <ul class="review-star d-flex flex-row ml-2">
                                                                @for ($i = 0; $i < $item->average; $i++)
                                                                    <li><i class="fa fa-star"></i></li>
                                                                @endfor
                                                                @for ($i = 0; $i < 5-$item->average; $i++)
                                                                    <li><i class="far fa-star"></i></li>
                                                                @endfor
                                                            </ul>
                                                        @endif
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>Equipment:</td>
                                                    <td>
                                                        @if (!empty($item->equipment))
                                                            <ul class="review-star d-flex flex-row  ml-2">
                                                                @for ($i = 0; $i < $item->equipment; $i++)
                                                                    <li><i class="fa fa-star"></i></li>
                                                                @endfor
                                                                @for ($i = 0; $i < 5-$item->equipment; $i++)
                                                                    <li><i class="far fa-star"></i></li>
                                                                @endfor
                                                            </ul>
                                                        @endif
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Comfortable:</td>
                                                    <td>
                                                        @if (!empty($item->comfortable))
                                                            <ul class="review-star d-flex flex-row  ml-2">
                                                                @for ($i = 0; $i < $item->comfortable; $i++)
                                                                    <li><i class="fa fa-star"></i></li>
                                                                @endfor
                                                                @for ($i = 0; $i < 5-$item->comfortable; $i++)
                                                                    <li><i class="far fa-star"></i></li>
                                                                @endfor
                                                            </ul>
                                                        @endif
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Climate_control:</td>
                                                    <td>
                                                        @if (!empty($item->climate_control))
                                                            <ul class="review-star d-flex flex-row  ml-2">
                                                                @for ($i = 0; $i < $item->climate_control; $i++)
                                                                    <li><i class="fa fa-star"></i></li>
                                                                @endfor
                                                                @for ($i = 0; $i < 5-$item->climate_control; $i++)
                                                                    <li><i class="far fa-star"></i></li>
                                                                @endfor
                                                            </ul>
                                                        @endif
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Facility:</td>
                                                    <td>
                                                        @if (!empty($item->facility))
                                                            <ul class="review-star d-flex flex-row  ml-2">
                                                                @for ($i = 0; $i < $item->facility; $i++)
                                                                    <li><i class="fa fa-star"></i></li>
                                                                @endfor
                                                                @for ($i = 0; $i < 5-$item->facility; $i++)
                                                                    <li><i class="far fa-star"></i></li>
                                                                @endfor
                                                            </ul>
                                                        @endif
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Aftercare:</td>
                                                    <td>
                                                        @if (!empty($item->aftercare))
                                                            <ul class="review-star d-flex flex-row  ml-2">
                                                                @for ($i = 0; $i < $item->aftercare; $i++)
                                                                    <li><i class="fa fa-star"></i></li>
                                                                @endfor
                                                                @for ($i = 0; $i < 5-$item->aftercare; $i++)
                                                                    <li><i class="far fa-star"></i></li>
                                                                @endfor
                                                            </ul>
                                                        @endif
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td>
                                        <a href="#">{{ optional($item->car)->title }}</a>
                                    </td>
                                    @if ($item->status == 4)
                                        <td class="text-center"><a href="" class="btn status-review-car btn-secondary">Trash</a></td>
                                    @elseif ($item->status == 1)
                                        <td class="text-center"><a href="" class="btn status-review-car btn-success">Approved</a></td>
                                    @elseif ($item->status == 2)
                                        <td class="text-center"><a href="" class="btn status-review-car btn-warning">Pending</a></td>
                                    @elseif ($item->status == 3)
                                        <td class="text-center"><a href="" class="btn status-review-car btn-danger">Spam</a></td>
                                    @endif
                                    <td>{{ $item->created_at }}</td>
                                </tr>
                                @php
                                   $stt++;
                                @endphp
                            @endforeach
                        </tbody>
                    </table>
                    <div class="mt-2">
                        {{ $list_review->render() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('js')
    <!-- Summernote js -->
    <script src="{{ URL::asset('assets/libs/summernote/summernote.min.js') }}"></script>
    <!-- Select2 js-->
    <script src="{{ URL::asset('assets/libs/select2/select2.min.js') }}"></script>
    <!-- Dropzone file uploads-->
    <script src="{{ URL::asset('assets/libs/dropzone/dropzone.min.js') }}"></script>
    <!-- Init js -->
    
    <script>
        $(document).on('click', '.check_master', function(e) {
            if($(this).is(':checked', true))
            {
                $(".sub-chk").prop('checked', true);
            } else {
                $(".sub-chk").prop('checked', false);
            }
        });
        $(document).on('click', '.btn_change_action', function() {
            const __this = this;
            $(__this).prop('disabled', true);
            var list_id = [];
            $('.sub-chk:checked').each(function () {
                var sub_id =parseInt($(this).attr('data-id'));
                list_id.push(sub_id);
            });
            var selected_action =parseInt($('.selected_action').val());
            if(list_id.length == 0) {
                toastr.error("Vui lòng chọn hàng cần xóa");
                $(__this).prop('disabled', false);
            }else {
                if(selected_action == 5) {
                    var check_sure = confirm("Bạn chắc chắn muốn xóa?");
                    if(check_sure == true){
                        const __token = $('meta[name="csrf-token"]').attr('content');
                        data_ = {
                            list_id: list_id,
                            _token: __token
                        }
                        var request = $.ajax({
                            url: '{{route('car.review.delete_all')}}',
                            type: 'POST',
                            data: data_,
                            dataType: "json"
                        });
                        request.done(function (msg) {
                            if (msg.type == 1) {
                                $('.alert-success').remove();
                                $('.sub-chk:checked').each(function () {
                                    $(this).parents("tr").remove();
                                });
                                toastr.success(msg.mess);
                            }
                            return false;
                        });
                        request.fail(function (jqXHR, textStatus) {
                            alert("Không thể gửi yêu cầu mã lỗi : " + textStatus);
                        });
                    }
                    $(__this).prop('disabled', false);
                }else if(selected_action != 0 ) {
                    const __token = $('meta[name="csrf-token"]').attr('content');
                    data_ = {
                        list_id: list_id,
                        selected_action: selected_action,
                        _token: __token
                    }
                    var request = $.ajax({
                        url: '{{route('car.review.change_status')}}',
                        type: 'POST',
                        data: data_,
                        dataType: "json"
                    });
                    request.done(function (msg) {
                        if (msg.type == 1) {
                            $('.alert-success').remove();
                            $('.sub-chk:checked').each(function () {
                                $(this).parents("tr").children("td").children('.status-review-car').removeClass("btn-secondary");
                                $(this).parents("tr").children("td").children('.status-review-car').removeClass("btn-success");
                                $(this).parents("tr").children("td").children('.status-review-car').removeClass("btn-warning");
                                $(this).parents("tr").children("td").children('.status-review-car').removeClass("btn-danger");
                                $(this).parents("tr").children("td").children('.status-review-car').addClass("btn-success");
                                $(this).parents("tr").children("td").children('.status-review-car').html("Approved");
                            });
                            toastr.success(msg.mess);
                        }else if(msg.type == 2) {
                            $('.alert-success').remove();
                            $('.sub-chk:checked').each(function () {
                                $(this).parents("tr").children("td").children('.status-review-car').removeClass("btn-secondary");
                                $(this).parents("tr").children("td").children('.status-review-car').removeClass("btn-success");
                                $(this).parents("tr").children("td").children('.status-review-car').removeClass("btn-warning");
                                $(this).parents("tr").children("td").children('.status-review-car').removeClass("btn-danger");
                                $(this).parents("tr").children("td").children('.status-review-car').addClass("btn-warning");
                                $(this).parents("tr").children("td").children('.status-review-car').html("Pending");
                            });
                            toastr.success(msg.mess);
                        }else if(msg.type == 3) {
                            $('.alert-success').remove();
                            $('.sub-chk:checked').each(function () {
                                $(this).parents("tr").children("td").children('.status-review-car').removeClass("btn-secondary");
                                $(this).parents("tr").children("td").children('.status-review-car').removeClass("btn-success");
                                $(this).parents("tr").children("td").children('.status-review-car').removeClass("btn-warning");
                                $(this).parents("tr").children("td").children('.status-review-car').removeClass("btn-danger");
                                $(this).parents("tr").children("td").children('.status-review-car').addClass("btn-danger");
                                $(this).parents("tr").children("td").children('.status-review-car').html("Spam");
                            });
                            toastr.success(msg.mess);
                        }else if(msg.type == 4) {
                            $('.alert-success').remove();
                            $('.sub-chk:checked').each(function () {
                                $(this).parents("tr").children("td").children('.status-review-car').removeClass("btn-secondary");
                                $(this).parents("tr").children("td").children('.status-review-car').removeClass("btn-success");
                                $(this).parents("tr").children("td").children('.status-review-car').removeClass("btn-warning");
                                $(this).parents("tr").children("td").children('.status-review-car').removeClass("btn-danger");
                                $(this).parents("tr").children("td").children('.status-review-car').addClass("btn-secondary");
                                $(this).parents("tr").children("td").children('.status-review-car').html("Trash");
                            });
                            toastr.success(msg.mess);
                        }
                        $(__this).prop('disabled', false);
                        return false;
                    });
                    request.fail(function (jqXHR, textStatus) {
                        alert("Không thể gửi yêu cầu mã lỗi : " + textStatus);
                    }); 
                }
            }
        });
    </script>
    
@endpush
