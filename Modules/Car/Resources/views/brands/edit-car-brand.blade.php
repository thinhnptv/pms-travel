@extends('base::layouts.master')
@section('css')
    <!-- Bootstrap Tables css -->
    <link href="{{ URL::asset('assets/libs/bootstrap-table/bootstrap-table.min.css')}}" rel="stylesheet"
          type="text/css"/>
@endsection
@section('content')
    <!-- Start Content-->
    <div class="container-fluid">
        <!-- start page title -->
        <div class="row">
            <div class="col-lg-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">UBold</a></li>
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Space</a></li>
                            <li class="breadcrumb-item active">Attribute</li>
                            <li class="breadcrumb-item active">Attribute: {{ $carBrand->name }}</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-lg-6">
                <h2 class="page-title">Edit: {{ $carBrand->name }}</h2>
            </div>
        </div>
        <!-- end page title -->
        <form action="{{ route('car.brand-update', $carBrand->id) }}" method="post">
            @csrf
            <div class="row justify-content-center">
                <div class="col-lg-6">
                    <div class="card-box">
                        <div class="col-xl-12">
                            <div class="card">
                                <div class="card-body">
                                    <form>
                                        <div id="basicwizard">
                                            <ul class="nav nav-pills bg-light nav-justified form-wizard-header mb-4">
                                                <li class="nav-item">
                                                    <a href="#basictab1" data-toggle="tab"
                                                       class="nav-link rounded-0 pt-2 pb-2 activate">
                                                        <i class="mdi mdi-account-circle mr-1"></i>
                                                        <span class="d-none d-sm-inline">English</span>
                                                    </a>
                                                </li>
                                                <li class="nav-item">
                                                    <a href="#basictab2" data-toggle="tab"
                                                       class="nav-link rounded-0 pt-2 pb-2">
                                                        <i class="mdi mdi-face-profile mr-1"></i>
                                                        <span class="d-none d-sm-inline">Japanese</span>
                                                    </a>
                                                </li>
                                                <li class="nav-item">
                                                    <a href="#basictab3" data-toggle="tab"
                                                       class="nav-link rounded-0 pt-2 pb-2">
                                                        <i class="mdi mdi-checkbox-marked-circle-outline mr-1"></i>
                                                        <span class="d-none d-sm-inline">Egyptian</span>
                                                    </a>
                                                </li>
                                            </ul>
                                            <h5 class=" bg-light p-2 mt-0 mb-3 text-center">Attribute Content</h5>
                                            <div class="tab-content b-0 mb-0">
                                                <div class="tab-pane" id="basictab1">
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="form-group row mb-3">
                                                                <label class="col-md-3 col-form-label" for="userName">Name <span class="text-danger">*</span></label>
                                                                <div class="col-md-9">
                                                                    <input type="text" class="form-control" id="userName"
                                                                           name="name" value="{{  $carBrand->name }}">
                                                                    @if($errors->has('name'))
                                                                        <p class="alert alert-danger">{{ $errors->first('name') }}</p>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                            <div class="form-group row mb-3">
                                                                <label class="col-md-3 col-form-label" for="userName">Logo Image <span class="text-danger">*</span></label>
                                                                <div class="col-md-9">
                                                                    <div class="input-group-btn">
                                                                        <span class="input-group-btn">
                                                                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bd-example-modal-banner">Upload image</button>
                                                                            <input type="hidden" name="image" id="banner" value="{{ $carBrand->image }}">
                                                                        </span>
                                                                    </div>
                                                                    <div id="preview_banner" class="mt-3">
                                                                        <img src="{{ url('/').$carBrand->image }}" alt="">
                                                                    </div>
                                                                    @if($errors->has('image'))
                                                                        <p class="alert alert-danger">{{ $errors->first('image') }}</p>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                        </div> <!-- end col -->
                                                    </div> <!-- end row -->
                                                </div>
                                                <div class="tab-pane" id="basictab2">
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="form-group row mb-3">
                                                                <label class="col-md-3 col-form-label" for="userName">Name</label>
                                                                <div class="col-md-9">
                                                                    <input type="text" class="form-control" id="userName"
                                                                           name="" value="{{ $carBrand->name }}">
                                                                </div>
                                                            </div>
                                                        </div> <!-- end col -->
                                                    </div> <!-- end row -->
                                                </div>
                                                <div class="tab-pane" id="basictab3">
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="form-group row mb-3">
                                                                <label class="col-md-3 col-form-label" for="userName">Name</label>
                                                                <div class="col-md-9">
                                                                    <input type="text" class="form-control" id="userName"
                                                                           name="" value="{{ $carBrand->name }}">
                                                                </div>
                                                            </div>
                                                        </div> <!-- end col -->
                                                    </div> <!-- end row -->
                                                </div>
                                            </div> <!-- tab-content -->
                                        </div> <!-- end #basicwizard-->
                                    </form>
                                </div> <!-- end card-body -->
                            </div> <!-- end card-->
                        </div> <!-- end col -->
                    </div>
                    <div style="margin-bottom: 20px; float: right">
                        <button  class="btn btn-primary" type="submit">Save Change</button>
                        <a href="{{ route('car.brand-list') }}"><button class="btn btn-danger delete_all" type="submit">Cancel</button></a>
                    </div>
                </div> <!-- end col -->
            </div>
            <!-- end col-->
        </form>
    </div>
    <!-- end row -->

@stop

@section('js')
    <div class="modal fade bd-example-modal-image" id="modal-file" tabindex="-1" role="dialog" data-backdrop="false" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Manger Image</h5>
                    <button type="button" class="close" data-dismiss="modal"><i class="fas fa-times"></i></button>
                </div>
                <div class="modal-body">
                    <iframe src="{{ config('app.url') }}/filemanager/dialog.php?akey=4Ans6pfTs6A9DzHj8AZq1NVGKoYxpyoI8ivCN1bOow&field_id=avatar&multiple=0" frameborder="0" style="width: 100%; height: 500px; overflow-y: auto"></iframe>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade bd-example-modal-banner" id="modal-file" tabindex="-1" role="dialog" data-backdrop="false" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Manger Image</h5>
                    <button type="button" class="close" data-dismiss="modal"><i class="fas fa-times"></i></button>
                </div>
                <div class="modal-body">
                    <iframe src="{{ config('app.url') }}/filemanager/dialog.php?akey=4Ans6pfTs6A9DzHj8AZq1NVGKoYxpyoI8ivCN1bOow&field_id=banner&multiple=0" frameborder="0" style="width: 100%; height: 500px; overflow-y: auto"></iframe>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade bd-example-modal-album" id="modal-file" tabindex="-1" role="dialog" data-backdrop="false" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Manger Image</h5>
                    <button type="button" class="close" data-dismiss="modal"><i class="fas fa-times"></i></button>
                </div>
                <div class="modal-body">
                    <iframe src="{{ config('app.url') }}/filemanager/dialog.php?akey=4Ans6pfTs6A9DzHj8AZq1NVGKoYxpyoI8ivCN1bOow&field_id=album&multiple=1" frameborder="0" style="width: 100%; height: 500px; overflow-y: auto"></iframe>
                </div>
            </div>
        </div>
    </div>
@stop

@section('script-bottom')
    {!! Toastr::message() !!}

    <script>
        $(document).on('hide.bs.modal', '.bd-example-modal-banner', function () {
            var _img = $('input#banner').val();
            if (!_img.length) {
                $('#preview_banner').empty();
            } else {
                $('#preview_banner').empty();
                $html = `
            <div class="box_imgg position-relative">
                <img src="" id='show-img-banner' style="width:100%;">
                <i class="mdi mdi-close-circle-outline style_icon_remove style_icon_remove_banner" title="delete"></i>
            </div>
        `;
                $('#preview_banner').append($html);
                $('#show-img-banner').attr('src', _img);
            }
        });

        $(document).on('hide.bs.modal', '.bd-example-modal-album', function () {
            var _img = $('input#album').val();
            if (!_img.length) {
                $('#preview_album').empty();
            } else {
                if(_img[0] == '[') {
                    var array = JSON.parse(_img);
                    $('#row_preview_album').empty();
                    var html = '';
                    $.each(array, function( index, value ) {
                        html += `
                            <div class="col-3">
                                <div class="box_imgg position-relative">
                                    <img src="${value}" class="img-height-110" style="width:100%; height=110px;">
                                    <i class="mdi mdi-close-circle-outline style_icon_remove style_icons_remove_album" title="delete"></i>
                                </div>
                            </div>
                        `;
                    });
                    $('#row_preview_album').append(html);

                }else{

                    $html = `
                        <div class="col-3">
                            <div class="box_imgg position-relative">
                                <img src="" id='show-img-album' style="width:100%;">
                                <i class="mdi mdi-close-circle-outline style_icon_remove style_icons_remove_album" title="delete"></i>
                            </div>
                        </div>
                    `;
                    $('#row_preview_album').empty();
                    $('#row_preview_album').append($html);
                    $('#show-img-album').attr('src', _img);
                    var str_src = '';
                    console.log(str_src);
                    str_src += "[\"";
                    str_src += _img;
                    str_src += "\"]";
                    $('#album').val(str_src);
                }
            }
        });

        $(document).on('click', '.btn-remove-item', function(){
            $(this).parents('.item').remove();
        });

        $(document).on('hide.bs.modal', '.bd-example-modal-image', function () {
            var _img = $('input#avatar').val();
            if (!_img.length) {
                $('#preview_avatar').empty();
            } else {
                $('#preview_avatar').empty();
                $html = `
            <div class="box_imgg position-relative">
                <img src="" id='show-img-hotel' style="width:100%;">
                <i class="mdi mdi-close-circle-outline style_icon_remove style_icon_remove_image" title="delete"></i>
            </div>
        `;
                $('#preview_avatar').append($html);
                $('#show-img-hotel').attr('src', _img);
            }
        });

        $(document).on('click', '.style_icon_remove_image', function() {
            $(this).parent('.box_imgg').hide('slow', function () {
                $(this).remove();
                $('#avatar').val('');
            });
        });

        $(document).on('hide.bs.modal', '.bd-example-modal-banner', function () {
            var _img = $('input#banner').val();
            if (!_img.length) {
                $('#preview_banner').empty();
            } else {
                $('#preview_banner').empty();
                $html = `
            <div class="box_imgg position-relative">
                <img src="" id='show-img-banner' style="width:100%;">
                <i class="mdi mdi-close-circle-outline style_icon_remove style_icon_remove_banner" title="delete"></i>
            </div>
        `;
                $('#preview_banner').append($html);
                $('#show-img-banner').attr('src', _img);
            }
        });

        $(document).on('click', '.style_icon_remove_banner', function() {
            $(this).parent('.box_imgg').hide('slow', function () {
                $(this).remove();
                $('#banner').val('');
            });
        });

        $(document).on('click', '.style_icons_remove_album', function() {
            $(this).parents('.col-3').hide('slow', function () {
                $(this).remove();
                var arr_image = [];
                var str_src = '';
                $('.row_preview_album').find('.col-3').each(function () {
                    arr_image.push($(this).find('.img-height-110').attr('src'));
                });
                str_src += "[\"";
                str_src += arr_image.toString();
                str_src += "\"]";
                if(arr_image.length == 0){
                    $('#album').val('');
                }else{
                    $('#album').val(str_src);
                }
            });
        });
    </script>
@stop

@section('script')
    </script>
    <!-- Plugins js-->
    <script src="{{ URL::asset('assets/libs/twitter-bootstrap-wizard/twitter-bootstrap-wizard.min.js')}}"></script>

    <!-- Init js-->
    <script src="{{ URL::asset('assets/js/pages/form-wizard.init.js')}}"></script>

@endsection
