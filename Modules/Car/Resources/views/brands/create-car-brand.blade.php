@extends('base::layouts.master')

@section('content')
    <!-- Start Content-->
    <div class="container-fluid">
        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">UBold</a></li>
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Car</a></li>
                            <li class="breadcrumb-item active">Attribute</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Car Brand</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->
        <div class="row">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="card-box">
                            <form method="post" action="{{ route('car.brand-store') }}">
                                @csrf
                                <div class="form-group mb-3">
                                    <label for="product-name"> Name <span class="text-danger">*</span></label>
                                    <input type="text" id="slug-source" name="name" class="form-control" value="{{ old('name') }}"
                                           placeholder="Category name">
                                    @if($errors->has('name'))
                                        <p class="alert alert-danger">{{ $errors->first('name') }}</p>
                                    @endif
                                </div>
                                <div class="form-group mb-3">
                                    <label for="banner">Logo Image <span class="text-danger">*</span></label>
                                    <div class="col-md-6">
                                        <div class="input-group-btn">
                                            <span class="input-group-btn">
                                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bd-example-modal-banner">Upload image</button>
                                                <input type="hidden" name="image" id="banner">
                                                @if (!empty(old('image')))
                                                <div class="box_imgg position-relative">
                                                    <img src="{{ old('image') }}" id='show-img-image' style="width:100%;">
                                                    <i class="mdi mdi-close-circle-outline style_icon_remove style_icon_remove_banner" title="delete"></i>
                                                </div>
                                                @endif
                                            </span>
                                        </div>
                                        <div id="preview_banner" class="mt-3">
                                        </div>
                                    </div>
                                    @if($errors->has('image'))
                                        <p class="alert alert-danger">{{ $errors->first('image') }}</p>
                                    @endif
                                </div>
                                <div class="mb-3">
                                    <button type="submit" class="btn btn-info ">
                                        Add new
                                    </button>
                                </div>
                            </form>
                        </div> <!-- end card-box -->
                    </div> <!-- end col -->
                    <div class="col-lg-8">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="card-box">
                                        <h4 class="header-title">Car Brand All</h4>
                                        <div class="columns-left" style="float: left">
                                            <button style="margin-bottom: 10px" class="btn btn-danger delete_all">
                                                Delete
                                            </button>
                                        </div>
                                        <div class="columns-left" style="float: right">
                                            <form action="{{ route('car.brand-store') }}" method="GET">
                                                <div class="form-inline">
                                                    <input type="text" class="form-control" name="name"
                                                           placeholder="search">
                                                    <button type="submit" class="btn btn-info ">Search</button>
                                                </div>
                                            </form>
                                        </div>
                            <form class="form-delete" action="{{ route('car.delet-car-brand') }}" method="post">
                                @csrf
                                @method('DELETE')
                                    <table class="table table-bordered" id="testing">
                                        <tr>
                                            <th width="50px"><input type="checkbox" class="selectall" id="master">
                                            </th>
                                            <th width="50px">STT</th>
                                            <th>Name</th>
                                            <th>Action</th>
                                        </tr>
                                        @php
                                            $start = ($list_car_brand->currentPage()-1)*$list_car_brand->perPage() +1;
                                        @endphp
                                            @foreach ($list_car_brand as $item)
                                                <tr>
                                                    <td><input type='checkbox' class='sub-chk' name='ids[]'
                                                                value='{{ $item->id }}'></td>
                                                    <td>{{$start++ }}</td>
                                                    <td>
                                                        <a href="{{ route('car.brand-edit',$item->id) }}">{{ $item->name }}</a>
                                                    </td>
                                                    <td>
                                                        <a href="{{ route('car.brand-edit',$item->id) }}"
                                                            class="btn btn-primary btn-sm">Edit</a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                    </table>
                                    </div> <!-- end card-box-->
                                </div>
                                <!-- end col-->
                            </form>
                        </div>
                        <div class="fexPagintaion text-center clearfix">
                            {{ $list_car_brand->links() }}
                        </div>
                    </div> <!-- end col-->
                </div>
                <!-- end row -->
            </div> <!-- container -->
        </div>
    </div>
    <div class="modal justify-content-center" id="confirm">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <p>Bạn chắc chắn muốn xóa</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-sm btn-primary" id="delete-btn">Delete</button>
                    <button type="button" class="btn btn-sm btn-dark" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
@stop

@section('js')
    <div class="modal fade bd-example-modal-image" id="modal-file" tabindex="-1" role="dialog" data-backdrop="false" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Manger Image</h5>
                    <button type="button" class="close" data-dismiss="modal"><i class="fas fa-times"></i></button>
                </div>
                <div class="modal-body">
                    <iframe src="{{ config('app.url') }}/filemanager/dialog.php?akey=4Ans6pfTs6A9DzHj8AZq1NVGKoYxpyoI8ivCN1bOow&field_id=avatar&multiple=0" frameborder="0" style="width: 100%; height: 500px; overflow-y: auto"></iframe>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade bd-example-modal-banner" id="modal-file" tabindex="-1" role="dialog" data-backdrop="false" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Manger Image</h5>
                    <button type="button" class="close" data-dismiss="modal"><i class="fas fa-times"></i></button>
                </div>
                <div class="modal-body">
                    <iframe src="{{ config('app.url') }}/filemanager/dialog.php?akey=4Ans6pfTs6A9DzHj8AZq1NVGKoYxpyoI8ivCN1bOow&field_id=banner&multiple=0" frameborder="0" style="width: 100%; height: 500px; overflow-y: auto"></iframe>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade bd-example-modal-album" id="modal-file" tabindex="-1" role="dialog" data-backdrop="false" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Manger Image</h5>
                    <button type="button" class="close" data-dismiss="modal"><i class="fas fa-times"></i></button>
                </div>
                <div class="modal-body">
                    <iframe src="{{ config('app.url') }}/filemanager/dialog.php?akey=4Ans6pfTs6A9DzHj8AZq1NVGKoYxpyoI8ivCN1bOow&field_id=album&multiple=1" frameborder="0" style="width: 100%; height: 500px; overflow-y: auto"></iframe>
                </div>
            </div>
        </div>
    </div>
@stop

@section('script-bottom')

    {!! Toastr::message() !!}

    <script>
        @if(session('success'))
        toastr.success('{{ session('success') }}', 'Thông báo', {timeOut: 5000});
        @endif
        $(document).on('click', '.delete_all', function (e) {
            e.preventDefault();
            var $form = $('.form-delete');
            $('#confirm').modal({backdrop: 'static', keyboard: false})
                .on('click', '#delete-btn', function () {
                    $form.submit();
                });
        });

        $(document).on('hide.bs.modal', '.bd-example-modal-banner', function () {
            var _img = $('input#banner').val();
            if (!_img.length) {
                $('#preview_banner').empty();
            } else {
                $('#preview_banner').empty();
                $html = `
            <div class="box_imgg position-relative">
                <img src="" id='show-img-banner' style="width:100%;">
                <i class="mdi mdi-close-circle-outline style_icon_remove style_icon_remove_banner" title="delete"></i>
            </div>
        `;
                $('#preview_banner').append($html);
                $('#show-img-banner').attr('src', _img);
            }
        });

        $(document).on('hide.bs.modal', '.bd-example-modal-album', function () {
            var _img = $('input#album').val();
            if (!_img.length) {
                $('#preview_album').empty();
            } else {
                if(_img[0] == '[') {
                    var array = JSON.parse(_img);
                    $('#row_preview_album').empty();
                    var html = '';
                    $.each(array, function( index, value ) {
                        html += `
                            <div class="col-3">
                                <div class="box_imgg position-relative">
                                    <img src="${value}" class="img-height-110" style="width:100%; height=110px;">
                                    <i class="mdi mdi-close-circle-outline style_icon_remove style_icons_remove_album" title="delete"></i>
                                </div>
                            </div>
                        `;
                    });
                    $('#row_preview_album').append(html);

                }else{

                    $html = `
                        <div class="col-3">
                            <div class="box_imgg position-relative">
                                <img src="" id='show-img-album' style="width:100%;">
                                <i class="mdi mdi-close-circle-outline style_icon_remove style_icons_remove_album" title="delete"></i>
                            </div>
                        </div>
                    `;
                    $('#row_preview_album').empty();
                    $('#row_preview_album').append($html);
                    $('#show-img-album').attr('src', _img);
                    var str_src = '';
                    console.log(str_src);
                    str_src += "[\"";
                    str_src += _img;
                    str_src += "\"]";
                    $('#album').val(str_src);
                }
            }
        });

        $(document).on('click', '.btn-remove-item', function(){
            $(this).parents('.item').remove();
        });

        $(document).on('hide.bs.modal', '.bd-example-modal-image', function () {
            var _img = $('input#avatar').val();
            if (!_img.length) {
                $('#preview_avatar').empty();
            } else {
                $('#preview_avatar').empty();
                $html = `
            <div class="box_imgg position-relative">
                <img src="" id='show-img-hotel' style="width:100%;">
                <i class="mdi mdi-close-circle-outline style_icon_remove style_icon_remove_image" title="delete"></i>
            </div>
        `;
                $('#preview_avatar').append($html);
                $('#show-img-hotel').attr('src', _img);
            }
        });

        $(document).on('click', '.style_icon_remove_image', function() {
            $(this).parent('.box_imgg').hide('slow', function () {
                $(this).remove();
                $('#avatar').val('');
            });
        });

        $(document).on('hide.bs.modal', '.bd-example-modal-banner', function () {
            var _img = $('input#banner').val();
            if (!_img.length) {
                $('#preview_banner').empty();
            } else {
                $('#preview_banner').empty();
                $html = `
            <div class="box_imgg position-relative">
                <img src="" id='show-img-banner' style="width:100%;">
                <i class="mdi mdi-close-circle-outline style_icon_remove style_icon_remove_banner" title="delete"></i>
            </div>
        `;
                $('#preview_banner').append($html);
                $('#show-img-banner').attr('src', _img);
            }
        });

        $(document).on('click', '.style_icon_remove_banner', function() {
            $(this).parent('.box_imgg').hide('slow', function () {
                $(this).remove();
                $('#banner').val('');
            });
        });

        $(document).on('click', '.style_icons_remove_album', function() {
            $(this).parents('.col-3').hide('slow', function () {
                $(this).remove();
                var arr_image = [];
                var str_src = '';
                $('.row_preview_album').find('.col-3').each(function () {
                    arr_image.push($(this).find('.img-height-110').attr('src'));
                });
                str_src += "[\"";
                str_src += arr_image.toString();
                str_src += "\"]";
                if(arr_image.length == 0){
                    $('#album').val('');
                }else{
                    $('#album').val(str_src);
                }
            });
        });
    </script>
    <script>
        $('.selectall').click(function () {
            $('.sub-chk').prop('checked', $(this).prop('checked'));
        })
        $('.sub-chk').change(function () {
            var total = $('.sub-chk').length;
            var number = $('.sub-chk:checked').length;
            if (total == number) {
                $('.selectall').prop('checked', true);
            } else {
                $('.selectall').prop('checked', false);
            }
        })
    </script>


@endsection

