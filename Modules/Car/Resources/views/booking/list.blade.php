@extends('base::layouts.master')
@section('css')
        <!-- Plugins css-->
        <link href="{{ URL::asset('assets/libs/select2/select2.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ URL::asset('assets/libs/summernote/summernote.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ URL::asset('assets/libs/dropzone/dropzone.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ URL::asset('assets/css/webt.css')}}" rel="stylesheet" type="text/css" />
@endsection
@section('content')
    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">UBold</a></li>
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Apps</a></li>
                            <li class="breadcrumb-item active">List Booking Car</li>
                        </ol>
                    </div>
                    <h4 class="page-title">List Booking Car</h4>
                </div>
            </div>
        </div>
        @if (session('success'))
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
        @endif
        <!-- end page title -->
    <div class="row">
        <div class="col-12">
            <div class="card p-2">
                <div class="row mb-2">
                    <div class="col-sm-2">
                        <select class="form-control selected_action">
                            <option value="">Bulk Actions</option>
                            <option value="0">Completed</option>
                            <option value="1">Processing</option>
                            <option value="2">Confirmed</option>
                            <option value="3">Cancelled</option>
                            <option value="4">Paid</option>
                            <option value="5">Unpaid</option>
                            <option value="6">Partial Payment</option>
                            <option value="7">Delete</option>
                        </select>
                    </div>
                    <div class="col-sm-2"><a href="javascript:void(0)" class="btn btn-primary waves-effect waves-light mb-2 btn_change_action" >Apply</a></div>
                    <div class="col-sm-8">
                        <div class="text-sm-right">
                            
                            
                        </div>
                    </div><!-- end col-->
                </div>
                <div class="table-responsive">
                    <table class="table table-bordered  mb-0">
                        <thead>
                            <tr>
                                <th width="50px"><input type="checkbox" id="master" class="check_master"></th>
                                <th class="text-center">STT</th>
                                {{-- <th class="text-center">ID</th> --}}
                                <th class="text-center">Service</th>
                                <th class="text-center">Customer</th>
                                <th class="text-center">Total</th>
                                <th class="text-center">Status</th>
                                <th class="text-center">Payment Method</th>
                                <th class="text-center">Created At</th>
                                <th class="text-center">#</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                                   $stt = 1;
                            @endphp
                            @foreach ($listBookingCar as $item)
                            <tr>
                                <td><input type="checkbox" class="sub-chk" data-id="{{ $item->id }}"></td>
                                <th class="text-center" scope="row">{{ $stt }}</th>
                                {{-- <th class="text-center" scope="row">{{ $item->id }}</th> --}}
                                @if (!empty($item->car->id))
                                    <td>
                                        <a href="">{{ $item->car->title }} </a>
                                        <br /> by 
                                        @if (!empty($item->car->user->id))
                                        <a href="">{{ $item->car->user->firstname}} (#{{ $item->car->user->id }})</a>
                                        @else
                                            [Vendor Deleted]
                                        @endif
                                        
                                    </td>
                                @else
                                    <td>[Car Deleted]</td>
                                @endif
                                @if (!empty($item->user->id))
                                    <td>
                                        <ul>
                                            <li>Name: {{ $item->user->firstname }} {{ $item->user->lastname }}</li>
                                            <li>Email: {{ $item->user->email }}</li>
                                            <li>Phone: {{ $item->user->phone }}</li>
                                            <li>Address: {{ $item->user->address }}</li>
                                            <li>Custom Requirement: <br /> {{ $item->special_requirements }}</li>
                                        </ul>
                                    </td>
                                @else
                                    <td>[Customer Deleted]</td>
                                @endif
                                
                                <td>${{ $item->total }}</td>
                                <td class="status-booing-car">{{ showStatusBooking($item->status)}}</td>
                                @if($item->payment_method == 0) 
                                    <td>Offline Payment</td>
                                @endif
                                <td>{{ $item->created_at }}</td>
                                @if (!empty($item->car->id))
                                <td class="action">
                                    <div class="dropdown">
                                        <button class="btn btn-secondary btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Actions
                                        </button>
                                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton" style="">
                                            <a class="dropdown-item" href="#" data-toggle="modal" data-target="#modal-booking-{{ $item->id }}">Detail</a>
                                            {{-- <a class="dropdown-item" href="http://sandbox.bookingcore.org/admin/module/report/booking/email_preview/13">Email Preview</a> --}}
                                        </div>
                                    </div>
                                    
                                    <div class="modal fade" id="modal-booking-{{ $item->id }}" style="display: none;" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-centered modal-lg">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="modal-title">Booking ID: #{{ $item->id }}</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <ul class="nav nav-tabs">
                                                        <li class="nav-item">
                                                            <a class="nav-link active" data-toggle="tab" href="#booking-detail-{{ $item->id }}">Booking Detail</a>
                                                        </li>
                                                        <li class="nav-item">
                                                            <a class="nav-link" data-toggle="tab" href="#booking-customer-{{ $item->id }}">
                                                        Customer Information
                                                        </a>
                                                        </li>
                                                    </ul>
                                                    <div class="tab-content">
                                                        <div id="booking-detail-{{ $item->id }}" class="tab-pane active"><br>
                                                            <div class="booking-review">
                                                                <div class="booking-review-content">
                                                                    <div class="review-section">
                                                                        <div class="info-form">
                                                                            <ul class="review-list">
                                                                                <li class="item-li-status-booking">
                                                                                    <div class="label">Booking Status</div>

                                                                                    <div class="val">{{ showStatusBooking($item->status)}}</div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="label">Booking Date</div>
                                                                                    <div class="val">{{ $item->created_at->format('d/m/Y')}}</div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="label">Payment Method</div>
                                                                                    <div class="val">{{ showPaymentMethod($item->payment_method)}}</div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="label">Service</div>
                                                                                    @if (!empty($item->car->id))
                                                                                        <div class="val"><a href="javastript:void(0)" target="_blank">{{ $item->car->title }}</a></div>
                                                                                    @else
                                                                                        <div class="val"></div>
                                                                                    @endif
                                                                                    
                                                                                </li>
                                                                                <li>
                                                                                    <div class="label">Vendor</div>
                                                                                    @if (!empty($item->car->user->id))
                                                                                        <div class="val"><a href="javastript:void(0)" target="_blank">{{ $item->car->user->firstname}} {{ $item->car->user->lastname}}</a></div>
                                                                                    @else
                                                                                        <div class="val">[Vendor deleted]</div>
                                                                                    @endif
                                                                                </li>
                                                                                <li>
                                                                                    <div class="label">Start date</div>
                                                                                    <div class="val">{{ date_format(date_create($item->start_date), 'd/m/Y') }}</div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="label">End date</div>
                                                                                    <div class="val">{{ date_format(date_create($item->end_date), 'd/m/Y') }}</div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="label">Days</div>
                                                                                    <div class="val">{{ getNumberDateFromStartAndEndDate($item->start_date, $item->end_date) }}</div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="label">Number</div>
                                                                                    <div class="val">{{ $item->number }}</div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="label">Price</div>
                                                                                    <div class="val">${{ $item->price }}</div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="label" title="({{ date_format(date_create($item->start_date), 'd/m/Y') }} -> {{ date_format(date_create($item->end_date), 'd/m/Y') }}) * ${{ $item->price}} * {{ $item->number }}">Rental price</div>
                                                                                    <div class="val" title="({{ date_format(date_create($item->start_date), 'd/m/Y') }} -> {{ date_format(date_create($item->end_date), 'd/m/Y') }}) * ${{ $item->price}} * {{ $item->number }}">${{ $item->price*$item->number*getNumberDateFromStartAndEndDate($item->start_date, $item->end_date) }}</div>
                                                                                </li>
                                                                                @if(sizeof($item->extraPriceBooked) > 0)
                                                                                    <li>
                                                                                        <div class="label"><strong>Extra Prices</strong></div>
                                                                                    </li>
                                                                                    <li class="no-flex">
                                                                                        <ul>
                                                                                            @foreach($item->extraPriceBooked as $value)
                                                                                                <li>
                                                                                                    <div class="label">{{ $value->title }}</div>
                                                                                                    <div class="val">${{ $value->price }}</div>
                                                                                                </li>
                                                                                            @endforeach
                                                                                        </ul>
                                                                                    </li>
                                                                                @else
                                                                                    
                                                                                @endif
                                                                                <li>
                                                                                    <div class="label">Equipment fee</div>
                                                                                    <div class="val">$100</div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="label">Facility fee</div>
                                                                                    <div class="val">$200</div>
                                                                                </li>
                                                                                <li class="final-total d-block">
                                                                                    <div class="d-flex justify-content-between">
                                                                                        <div class="label">Total:</div>
                                                                                        <div class="val">${{ $item->total }}</div>
                                                                                    </div>
                                                                                    <div class="d-flex justify-content-between">
                                                                                        <div class="label">Paid:</div>
                                                                                        <div class="val">${{ $item->paid }}</div>
                                                                                    </div>
                                                                                    <div class="d-flex justify-content-between">
                                                                                        <div class="label">Remain:</div>
                                                                                        <div class="val">${{ $item->total - $item->paid }}</div>
                                                                                    </div>
                                                                                </li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div id="booking-customer-{{ $item->id }}" class="tab-pane fade"><br>
                                                            <div class="booking-review">
                                                                <div class="booking-review">
                                                                    <div class="booking-review-content">
                                                                        <div class="review-section">
                                                                            <div class="info-form">
                                                                                @if(!empty($item->user->id))
                                                                                    <ul class="review-list">
                                                                                        <li>
                                                                                            <div class="label">First Name</div>
                                                                                            <div class="val">{{ optional($item->user)->firstname }}</div>
                                                                                        </li>
                                                                                        <li>
                                                                                            <div class="label">Last name</div>
                                                                                            <div class="val">{{ optional($item->user)->lastname }}</div>
                                                                                        </li>
                                                                                        <li>
                                                                                            <div class="label">Email</div>
                                                                                            <div class="val">{{ optional($item->user)->email }}</div>
                                                                                        </li>
                                                                                        <li>
                                                                                            <div class="label">Phone</div>
                                                                                            <div class="val">{{ optional($item->user)->phone }}</div>
                                                                                        </li>
                                                                                        <li>
                                                                                            <div class="label">Address</div>
                                                                                            <div class="val">{{ optional($item->user)->address }}</div>
                                                                                        </li>
                                                                                        <li>
                                                                                            <div class="label"><strong>Special Requirements</strong></div>
                                                                                        </li>
                                                                                        <li>
                                                                                            <div class="val p-3">{{ $item->special_requirements }}</div>
                                                                                        </li>
                                                                                    </ul>
                                                                                @else
                                                                                    [Customer Deleted]
                                                                                @endif
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <span class="btn btn-secondary" data-dismiss="modal">Close</span>
                                                    </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                @else
                                    <td></td>
                                @endif
                            </tr>
                            @php
                                   $stt++;
                            @endphp
                            @endforeach
                        </tbody>
                    </table>
                    <div class="mt-2">
                        {{ $listBookingCar->render() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('js')
    <!-- Summernote js -->
    <script src="{{ URL::asset('assets/libs/summernote/summernote.min.js') }}"></script>
    <!-- Select2 js-->
    <script src="{{ URL::asset('assets/libs/select2/select2.min.js') }}"></script>
    <!-- Dropzone file uploads-->
    <script src="{{ URL::asset('assets/libs/dropzone/dropzone.min.js') }}"></script>
    <!-- Init js -->
    <script src="{{ URL::asset('assets/js/webt.js') }}"></script>
    
    <script>
         $(document).on('click','.check_master', function(e) {
            if($(this).is(':checked',true))
            {
                $(".sub-chk").prop('checked', true);
            } else {
                $(".sub-chk").prop('checked',false);
            }
        });
        $(document).on('click', '.btn_change_action', function() {
            const __this = this;
            $(__this).prop('disabled', true);
            var listId = [];
            $('.sub-chk:checked').each(function () {
                var sub_id =parseInt($(this).attr('data-id'));
                listId.push(sub_id);
            });
            var selectedAction =parseInt($('.selected_action').val());
            if(listId.length == 0) {
                toastr.error("Vui lòng chọn hàng cần xóa");
                $(__this).prop('disabled', false);
            }else {
                if(selectedAction == 7) {
                    var check_sure = confirm("Bạn chắc chắn muốn xóa?");
                    if(check_sure == true){
                        const __token = $('meta[name="csrf-token"]').attr('content');
                        data_ = {
                            listId: listId,
                            _token: __token
                        }
                        var request = $.ajax({
                            url: '{{route('car.booking.delete_all')}}',
                            type: 'POST',
                            data: data_,
                            dataType: "json"
                        });
                        request.done(function (msg) {
                            if (msg.type == 1) {
                                $('.alert-success').remove();
                                $('.sub-chk:checked').each(function () {
                                    $(this).parents("tr").remove();
                                });
                                toastr.success(msg.mess);
                            }
                            return false;
                        });
                        request.fail(function (jqXHR, textStatus) {
                            alert("Không thể gửi yêu cầu mã lỗi : " + textStatus);
                        });
                    }
                    $(__this).prop('disabled', false);
                }else if(selectedAction != null) {
                    const __token = $('meta[name="csrf-token"]').attr('content');
                    data_ = {
                        listId: listId,
                        selectedAction: selectedAction,
                        _token: __token
                    }
                    var request = $.ajax({
                        url: '{{route('car.booking.change_status')}}',
                        type: 'POST',
                        data: data_,
                        dataType: "json"
                    });
                    request.done(function (msg) {
                        if (msg.type == 1) {
                            $('.alert-success').hide();
                            $('.sub-chk:checked').each(function () {
                                $(this).parents("tr").children('.status-booing-car').html(msg.status);
                                $(this).parents("tr").find('.item-li-status-booking').children('.val').html(msg.status);
                            });
                            toastr.success(msg.mess);
                        }
                        $(__this).prop('disabled', false);
                        return false;
                    });
                    request.fail(function (jqXHR, textStatus) {
                        alert("Không thể gửi yêu cầu mã lỗi : " + textStatus);
                    }); 
                }
            }
        });
    </script>
    
@endpush
