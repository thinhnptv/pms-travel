<?php

namespace Modules\Contact\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Contact\Entities\Contact;
use Modules\Contact\Entities\Subscriber;
use Modules\Contact\Http\Requests\AddSubscriberRequest;
use Modules\Contact\Http\Requests\EditSubscriberRequest;

class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    protected $modelContact;
    protected $modelSubscriber;

    public function __construct(Subscriber $modelSubscriber, Contact $modelContact)
    {
        $this->modelContact = $modelContact;
        $this->modelSubscriber = $modelSubscriber;
    }
    public function index(Request $request)
    {
        $listContact = $this->modelContact::query();
        if (!empty($request->full_name)) {
            $name = $request->full_name;
            $listContact->where('full_name', 'like', '%' .$name. '%')
            ->orWhere('mail', $name);
        }
        $listContact = $listContact->latest();
        $listContact = $listContact->paginate(15);
        return view('contact::layouts.list', compact('listContact'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('contact::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show($id)
    {
        $showContact = $this->modelContact::query()->findOrFail($id);
        return view('contact::layouts.show', compact('showContact'));
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function addSubscriber(AddSubscriberRequest $request)
    {
        // dd($request->all());
        $addSubscriber = new $this->modelSubscriber;
        $addSubscriber->firstname = $request->firstname;
        $addSubscriber->lastname = $request->lastname;
        $addSubscriber->email = $request->email;
        $addSubscriber->save();
        return redirect()->route('list-subscriber')->with('success', 'Thêm mới thành công');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {
        $deleteContact = $this->modelContact->findorFail($id);
        $deleteContact->delete();
        return redirect()->route('list_contact')->with('success', 'Xóa thành công');
    }
    public function deleteAll(Request $request)
    {
        $list_id = $request->list_id;
        foreach($list_id as $item){
            $deleteTour = $this->modelContact->findorFail($item);
            $deleteTour->delete();
        }
        return response()->json([
            'type' => 1,
            'mess' => "Xóa thành công"
        ]);
    }
    public function deleteSubscriberAll(Request $request)
    {
        $list_id = $request->list_id;
        foreach($list_id as $item){
            $deleteSubscriber = $this->modelSubscriber->findorFail($item);
            $deleteSubscriber->delete();
        }
        return response()->json([
            'type' => 1,
            'mess' => "Xóa thành công"
        ]);
    }
    public function listSubscriber() {
        $listSubscriber = $this->modelSubscriber::query()->paginate(15);
        return view('contact::subscriber.list', compact('listSubscriber'));
    }
    public function destroySubcriber($id)
    {
        $deleteSubscriber = $this->modelSubscriber->findorFail($id);
        $deleteSubscriber->delete();
        return redirect()->route('list-subscriber')->with('success', 'Xóa thành công');
    }
    public function getEditSubcriber($id) {
        $getEditSubcriber = $this->modelSubscriber->findorFail($id);
        return view('contact::subscriber.edit', compact('getEditSubcriber'));
    }
    public function postEditSubcriber($id, EditSubscriberRequest $request) {
        $postEditSubcriber = $this->modelSubscriber->findorFail($id);
        $postEditSubcriber->firstname = $request->firstname;
        $postEditSubcriber->lastname = $request->lastname;
        $postEditSubcriber->email = $request->email;
        $postEditSubcriber->save();
        return redirect()->route('list-subscriber')->with('success', 'Sửa thành công');
    }
}
