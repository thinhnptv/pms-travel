<?php

namespace Modules\Contact\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EditSubscriberRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|regex:/^[a-z][a-z0-9_\.]{3,32}@[a-z0-9]{2,}(\.[a-z0-9]{2,4}){1,2}(\.[0-9]{2,4}){0,2}$/|unique:subscribers,email,'.request()->id,
            'firstname' => 'required|min:2',
            'lastname' => 'required|min:2'
        ];
    }
    public function messages(){
        return [
            'firstname.required' => "Họ không được để trống!" ,
            'firstname.min' => "Họ không được ít hơn 2 ký tự" ,
            'lastname.required' => "Tên không được để trống!" ,
            'lastname.min' => "Tên không được ít hơn 2 ký tự!" ,
            'email.required' => "Email không được để trống!" ,
            'email.regex' => "Email bạn nhập không đúng định dạng!" ,
            'email.unique' => "Email đã được đăng ký, mời bạn nhập email khác!"
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
