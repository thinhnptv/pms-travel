<?php

Route::group(['middleware' => ['web', 'adminLogin'], 'prefix' => 'admin/contact', 'namespace' => 'Modules\Contact\Http\Controllers'], function()
{
    Route::get('/', 'ContactController@index')->name('list_contact');
    Route::get('/delete/{id}', 'ContactController@destroy')->name('contact.delete');
    Route::post('/delete-all-contact', 'ContactController@deleteAll')->name('contact.delete_all');
    Route::post('/delete-all-subscriber', 'ContactController@deleteSubscriberAll')->name('subscriber.delete_all');
    Route::get('/show-contact/{id}', 'ContactController@show')->name('contact.show');
    Route::get('/list-subscriber', 'ContactController@listSubscriber')->name('list-subscriber');
    Route::get('/delete-subscriber/{id}', 'ContactController@destroySubcriber')->name('delete.subscriber');
    Route::get('/edit-subscriber/{id}', 'ContactController@getEditSubcriber')->name('subscriber.get-edit');
    Route::post('/edit-subscriber/{id}', 'ContactController@postEditSubcriber')->name('subscriber.post-edit');
    Route::post('/add-subscriber', 'ContactController@addSubscriber')->name('subscriber.add');
});
