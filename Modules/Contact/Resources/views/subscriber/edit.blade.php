@extends('base::layouts.master')
@section('css')
        <link href="{{ URL::asset('assets/css/webt.css')}}" rel="stylesheet" type="text/css" />
@endsection
@section('content')
    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">UBold</a></li>
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Apps</a></li>
                            <li class="breadcrumb-item active">Edit Subscriber</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Edit Subscriber</h4>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <form action="{{ route('subscriber.post-edit', $getEditSubcriber->id) }}" method="post">
                    @csrf
                    
                    <div class="card p-2">
                        <div class="row">
                            <div class="col-md-12 col-lg-12">
                                <hr>
                                <div class="form-group mb-3">
                                    <label for="name">First Name</label>
                                    <input type="text" name="firstname" class="form-control" placeholder="Fist name" value="{{ $getEditSubcriber->firstname }}">
                                    <div class="notification">
                                        <p class="help is-danger">{{ $errors->first('firstname') }}</p>
                                    </div>
                                </div>
                                <div class="form-group mb-3">
                                    <label for="name">Last Name</label>
                                    <input type="text" name="lastname" class="form-control" placeholder="Last name" value="{{ $getEditSubcriber->lastname }}">
                                    <div class="notification">
                                        <p class="help is-danger">{{ $errors->first('lastname') }}</p>
                                    </div>
                                </div>
                                <div class="form-group mb-3">
                                    <label for="name">Email</label>
                                    <input type="text" name="email" class="form-control" placeholder="Email" value="{{ $getEditSubcriber->email }}">
                                    <div class="notification">
                                        <p class="help is-danger">{{ $errors->first('email') }}</p>
                                    </div>
                                </div>
                            </div> 
                        </div> 
                        <div class="form-group mb-3 text-right">
                            <button type="submit" class="btn w-sm btn-success waves-effect waves-light">Save Changes</button>
                            <button type="reset" class="btn w-sm btn-primary waves-effect waves-light">Reset</button>
                        </div>     
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection

@push('js')
    <script>
        jQuery(document).ready(function(){
            $(window).keydown(function(event){
                if(event.keyCode == 13) {
                event.preventDefault();
                return false;
                }
            });
            var checkNotificationSuccess =  $('.notification').find('.alert-success').length;
            var checkNotificationDanger =  $('.notification').find('.is-danger').length;
            if(checkNotificationSuccess != 0 || checkNotificationDanger != 0) {
                setTimeout(function(){
                    $('.notification').empty();
                }, 3000);
            }
        });
    </script>
    
@endpush

