@extends('base::layouts.master')
@section('css')
       
        <link href="{{ URL::asset('assets/css/webt.css')}}" rel="stylesheet" type="text/css" />
@endsection
@section('content')
    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">UBold</a></li>
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Apps</a></li>
                            <li class="breadcrumb-item active">All Subscriber</li>
                        </ol>
                    </div>
                    <h4 class="page-title">All Subscriber</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->
        <div class="notification">
            @if (session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif
        </div>  
        <div class="row">
            <div class="col-4">
                <form action="{{ route('subscriber.add') }}" method="post">
                    @csrf
                    
                    <div class="card p-2">
                        <div class="row">
                            <div class="col-md-12 col-lg-12">
                                <h4>Add Subscriber</h4>
                                <hr>
                                <div class="form-group mb-3">
                                    <label for="name">First Name</label>
                                    <input type="text" name="firstname" class="form-control" placeholder="Fist name">
                                    <div class="notification">
                                        <p class="help is-danger">{{ $errors->first('firstname') }}</p>
                                    </div>
                                </div>
                                <div class="form-group mb-3">
                                    <label for="name">Last Name</label>
                                    <input type="text" name="lastname" class="form-control" placeholder="Last name">
                                    <div class="notification">
                                        <p class="help is-danger">{{ $errors->first('lastname') }}</p>
                                    </div>
                                </div>
                                <div class="form-group mb-3">
                                    <label for="name">Email</label>
                                    <input type="text" name="email" class="form-control" placeholder="Email">
                                    <div class="notification">
                                        <p class="help is-danger">{{ $errors->first('email') }}</p>
                                    </div>
                                </div>
                            </div> 
                        </div> 
                        <div class="form-group mb-3 text-right">
                            <button type="submit" class="btn w-sm btn-success waves-effect waves-light">Save Changes</button>
                            <button type="reset" class="btn w-sm btn-primary waves-effect waves-light">Reset</button>
                        </div>     
                    </div>
                </form>
            </div>
            <div class="col-8">
                <div class="card p-2">
                    <div class="row mb-2">
                        <div class="col-sm-4">
                            <div class="text-sm-left">
                                <a href="javascript:void(0)" class="btn btn-danger waves-effect waves-light mb-2 delete_all" ><i class="mdi mdi-delete mr-1"></i> Delete All</a>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-bordered  mb-0">
                            <thead>
                                <tr>
                                    <th width="50px"><input type="checkbox" id="master" class="check_master"></th>
                                    <th class="text-center">STT</th>
                                    <th class="text-center">Frist name</th>
                                    <th class="text-center">Last name</th>
                                    <th class="text-center">Mail</th>
                                    <th class="text-center"></th>
                                    <th class="text-center"></th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $stt  = ($listSubscriber->currentPage()-1)*$listSubscriber->perPage() +1;
                                @endphp
                                @foreach ($listSubscriber as $item)
                                <tr>
                                    <td><input type="checkbox" class="sub-chk" data-id="{{ $item->id }}"></td>
                                    <th class="text-center" scope="row">{{ $stt++ }}</th>
                                    <td > {{ $item->firstname }}</td>
                                    <td > {{ $item->lastname }}</td>
                                    <td >{{ $item->email }}</td>
                                    <td class="text-center">
                                        <a href="{{ route('subscriber.get-edit', $item->id) }}" class="btn btn-success" >Edit</a>
                                    </td>
                                    <td class="text-center">
                                        <a href="{{ route('delete.subscriber', $item->id) }}" class="btn btn-danger" onclick="return confirm('Bạn chắc chắn muốn xoá?')">Delete</a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <div class="mt-2">
                            {{ $listSubscriber->render() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('js')
    <script>
        $( document ).ready(function() {
            var checkNotificationSuccess =  $('.notification').find('.alert-success').length;
            var checkNotificationDanger =  $('.notification').find('.is-danger').length;
            if(checkNotificationSuccess != 0 || checkNotificationDanger != 0) {
                setTimeout(function(){
                    $('.notification').empty();
                }, 3000);
            }
        });
        $(document).on('click','.check_master', function(e) {
            if($(this).is(':checked',true))
            {
                $(".sub-chk").prop('checked', true);
            } else {
                $(".sub-chk").prop('checked',false);
            }
        });
        $(document).on('click','.delete_all',function(e) {
            const __this = this;
            $(__this).prop('disabled', true);
            var list_id = [];
            $('.sub-chk:checked').each(function () {
                var sub_id =parseInt($(this).attr('data-id'));
                list_id.push(sub_id);
            });
            
            if(list_id.length == 0) {
                toastr.error("Vui lòng chọn hàng cần xóa");
            }else {
                var check_sure = confirm("Bạn chắc chắn muốn xóa?");
                if(check_sure == true){
                    const __token = $('meta[name="csrf-token"]').attr('content');
                    data_ = {
                        list_id: list_id,
                        _token: __token
                    }
                    var request = $.ajax({
                        url: '{{route('subscriber.delete_all')}}',
                        type: 'POST',
                        data: data_,
                        dataType: "json"
                    });
                    request.done(function (msg) {
                        if (msg.type == 1) {
                            $('.sub-chk:checked').each(function () {
                                $(this).parents("tr").remove();
                            });
                            $(__this).prop('disabled', false);
                            toastr.success(msg.mess);
                        }
                        return false;
                    });
                    request.fail(function (jqXHR, textStatus) {
                        alert("Không thể gửi yêu cầu mã lỗi : " + textStatus);
                    });
                }
            }
        });
    </script>
@endpush
