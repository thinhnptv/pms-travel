@extends('base::layouts.master')
@section('css')
        <!-- Plugins css-->
        <link href="{{ URL::asset('assets/libs/select2/select2.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ URL::asset('assets/libs/summernote/summernote.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ URL::asset('assets/libs/dropzone/dropzone.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ URL::asset('assets/css/webt.css')}}" rel="stylesheet" type="text/css" />
@endsection
@section('content')
    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">UBold</a></li>
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Apps</a></li>
                            <li class="breadcrumb-item active">All Contact</li>
                        </ol>
                    </div>
                    <h4 class="page-title">All Contact</h4>
                </div>
            </div>
        </div>
        <div class="notification">
            @if (session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif
        </div>
        
        <!-- end page title -->
    <div class="row">
        <div class="col-12">
            <div class="card p-2">
                <div class="row mb-2">
                    <div class="col-sm-4">
                        <div class="text-sm-left">
                            <a href="javascript:void(0)" class="btn btn-danger waves-effect waves-light mb-2 delete_all" ><i class="mdi mdi-delete mr-1"></i> Delete All</a>
                        </div>
                    </div>
                    <div class="col-sm-8">
                        <form action="{{ route('list_contact') }}" method="GET">
                            <div class="form-inline text-sm-right" style="float: right">
                                <input type="text" class="form-control" id="full_name" name="full_name" placeholder="search" value="{{ Request::get('full_name') }}">
                                <button type="submit" class="btn btn-info ">Search Contact</button>
                            </div>
                        </form>
                        
                    </div><!-- end col-->
                </div>
                <div class="table-responsive">
                    <table class="table table-bordered  mb-0">
                        <thead>
                            <tr>
                                <th width="50px"><input type="checkbox" id="master" class="check_master"></th>
                                <th class="text-center">STT</th>
                                {{-- <th class="text-center">ID</th> --}}
                                <th class="text-center">Full name</th>
                                {{-- <th class="text-center">Phone</th> --}}
                                <th class="text-center">Title</th>
                                {{-- <th class="text-center">Content</th> --}}
                                <th class="text-center">Mail</th>
                                <th class="text-center"></th>
                                <th class="text-center"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                                   $stt  = ($listContact->currentPage()-1)*$listContact->perPage() +1;
                            @endphp
                            @foreach ($listContact as $item)
                            <tr>
                                <td><input type="checkbox" class="sub-chk" data-id="{{ $item->id }}"></td>
                                <th class="text-center" scope="row">{{ $stt++ }}</th>
                                {{-- <th class="text-center" scope="row">{{ $item->id }}</th> --}}
                                <td class="text-center"> {{ $item->full_name }}</td>
                                {{-- <td class="text-center">{{ $item->phone }}</td> --}}
                                <td class="text-center">{{ $item->title }}</td>
                                {{-- <td class="text-center">{{ $item->content }}</td> --}}
                                <td class="text-center">{{ $item->mail }}</td>
                                <td>
                                    <a href="{{ route('contact.delete', $item->id) }}" class="btn btn-danger" onclick="return confirm('Bạn chắc chắn muốn xoá?')">Delete</a>
                                </td>
                                <td>
                                    <a href="{{ route('contact.show', $item->id) }}" class="btn btn-primary" >Show</a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div class="mt-2">
                        {{ $listContact->render() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('js')
    <!-- Summernote js -->
    <script src="{{ URL::asset('assets/libs/summernote/summernote.min.js') }}"></script>
    <!-- Select2 js-->
    <script src="{{ URL::asset('assets/libs/select2/select2.min.js') }}"></script>
    <!-- Dropzone file uploads-->
    <script src="{{ URL::asset('assets/libs/dropzone/dropzone.min.js') }}"></script>
    <!-- Init js -->
    <script src="{{ URL::asset('assets/js/webt.js') }}"></script>
    
    <script>
        $( document ).ready(function() {
            var checkNotification =  $('.notification').find('.alert-success').length;
            if(checkNotification != 0) {
                    setTimeout(function(){
                        $('.notification').empty();
                    }, 3000);
            }
        });
         $(document).on('click','.check_master', function(e) {
            if($(this).is(':checked',true))
            {
                $(".sub-chk").prop('checked', true);
            } else {
                $(".sub-chk").prop('checked',false);
            }
        });
       
        $(document).on('click','.page-link',function(e) {
            e.preventDefault();
            url_ = this.href;
            var url_string = location.href;
            var url = new URL(url_string);
            var role = url.searchParams.get("role");
            if(role != null){
                url_ = url_ + '&role='+role;
            }
            window.location.href=url_;
        })
        $(document).on('click','.delete_all',function(e) {
            const __this = this;
            $(__this).prop('disabled', true);
            var list_id = [];
            $('.sub-chk:checked').each(function () {
                var sub_id =parseInt($(this).attr('data-id'));
                list_id.push(sub_id);
            });
            
            if(list_id.length == 0) {
                toastr.error("Vui lòng chọn hàng cần xóa");
            }else {
                var check_sure = confirm("Bạn chắc chắn muốn xóa?");
                if(check_sure == true){
                    const __token = $('meta[name="csrf-token"]').attr('content');
                    data_ = {
                        list_id: list_id,
                        _token: __token
                    }
                    var request = $.ajax({
                        url: '{{route('contact.delete_all')}}',
                        type: 'POST',
                        data: data_,
                        dataType: "json"
                    });
                    request.done(function (msg) {
                        if (msg.type == 1) {
                            // $('.alert-success').remove();
                            $('.sub-chk:checked').each(function () {
                                $(this).parents("tr").remove();
                            });
                            $(__this).prop('disabled', false);
                            toastr.success(msg.mess);
                        }
                        return false;
                    });
                    request.fail(function (jqXHR, textStatus) {
                        alert("Không thể gửi yêu cầu mã lỗi : " + textStatus);
                    });
                }
            }
        });
    </script>
    <script>
        $( document ).ready(function() {
            var url_string = location.href;
            var url = new URL(url_string);
            var role = url.searchParams.get("role");
            if(role != null){
                $('.filter_role').val(role);
            }
             
         });
     </script>
@endpush
