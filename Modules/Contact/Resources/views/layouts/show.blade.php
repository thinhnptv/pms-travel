@extends('base::layouts.master')
@section('css')
        <!-- Plugins css-->
        <link href="{{ URL::asset('assets/libs/select2/select2.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ URL::asset('assets/libs/summernote/summernote.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ URL::asset('assets/libs/dropzone/dropzone.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ URL::asset('assets/css/webt.css')}}" rel="stylesheet" type="text/css" />
@endsection
@section('content')
    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">UBold</a></li>
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Apps</a></li>
                            <li class="breadcrumb-item active">Detail Their Contact</li>
                        </ol>
                    </div>
                    <h4 class="page-title">{{ $showContact->full_name}}: {{ $showContact->title}}</h4>
                </div>
            </div>
        </div>
        {{-- @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif --}}
        <!-- end page title -->
        <form action="" method="post" enctype="multipart/form-data">
            <input type="hidden" name="_token" value="{{csrf_token()}}" /> 
            <div class="row">
                <div class="col-md-12 col-lg-9">
                    <div class="card p-2">
                        <div class="row">
                            <div class="col-md-12 col-lg-12">
                                <div class="form-group mb-3">
                                    <h4>Full Name:</h4> <p>{{ $showContact->full_name}}</p>
                                </div> 
                                <div class="form-group mb-3">
                                    <h4>Phone:</h4> <p>{{ $showContact->phone}}</p>
                                </div>
                                <div class="form-group mb-3">
                                    <h4>Content:</h4> <p>{{ $showContact->content}}</p>
                                </div>
                                <div class="form-group mb-3">
                                    <h4>Mail:</h4> <p>{{ $showContact->mail}}</p>
                                </div>
                                <div class="form-group mb-3">
                                    <h4>Created at:</h4> <p>{{ $showContact->created_at}}</p>
                                </div>
                                <div class="form-group mb-3">
                                    <h4>Updated at:</h4> <p>{{ $showContact->updated_at}}</p>
                                </div>
                                <div class="text-center mb-3">
                                    <a href="{{route('list_contact')}}" class="btn w-sm btn-warning waves-effect waves-light">Cancel</a>
                                </div>
                            </div>
                        </div>     
                    </div>
                </div>
            </div>
        </form>
    </div>

@endsection

@push('js')
    <!-- Summernote js -->
    <script src="{{ URL::asset('assets/libs/summernote/summernote.min.js') }}"></script>
    <!-- Select2 js-->
    <script src="{{ URL::asset('assets/libs/select2/select2.min.js') }}"></script>
    <!-- Dropzone file uploads-->
    <script src="{{ URL::asset('assets/libs/dropzone/dropzone.min.js') }}"></script>
    <!-- Init js -->
    
    <script>
        jQuery(document).ready(function(){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        // Summernote
        $('#content').summernote({
            height: 180,                 // set editor height
            minHeight: null,             // set minimum height of editor
            maxHeight: null,             // set maximum height of editor
            focus: false                 // set focus to editable area after initializing summernote
        });
        
        
    });
    </script>
   

    
@endpush

