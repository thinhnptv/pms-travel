<?php

namespace Modules\Contact\Entities;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $table = 'contacts';
    protected $fillable = ['id', 'full_name', 'mail', 'phone', 'title', 'content', 'created_at', 'updated_at'];
}
