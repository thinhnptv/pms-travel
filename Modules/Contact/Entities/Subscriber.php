<?php

namespace Modules\Contact\Entities;

use Illuminate\Database\Eloquent\Model;

class Subscriber extends Model
{
    protected $table = 'subscribers';
}
