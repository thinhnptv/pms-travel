<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSpacesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('spaces', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('slug')->nullable();
            $table->longText('content')->nullable();
            $table->integer('size')->nullable();
            $table->string('banner')->nullable();
            $table->text('gallery')->nullable();
            $table->integer('number_bed')->nullable();
            $table->integer('number_bathroom')->nullable();
            $table->integer('max_guests')->nullable();
            $table->string('address')->nullable();
            $table->string('latitu')->nullable();
            $table->string('longtitu')->nullable();
            $table->string('mapzoom')->nullable();
            $table->string('seo_title')->nullable();
            $table->string('seo_description')->nullable();
            $table->string('price')->nullable();
            $table->string('sale_price')->nullable();
            $table->integer('state')->nullable();
            $table->integer('featured')->nullable();
            $table->string('status')->nullable()->default(1);
            $table->tinyInteger('rating_star')->default(5);
            $table->integer('location_id')->unsigned()->nullable();
            $table->foreign('location_id')->references('id')->on('locations')->onDelete('set null');
            $table->integer('vendor_id')->unsigned()->nullable();
            $table->foreign('vendor_id')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('spaces');
    }
}
