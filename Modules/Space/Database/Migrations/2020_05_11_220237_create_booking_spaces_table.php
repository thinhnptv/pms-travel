<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookingSpacesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('booking_spaces', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('total')->nullable();
            $table->string('payment_method')->nullable();
            $table->dateTime('start_date')->nullable();
            $table->dateTime('end_date')->nullable();
            $table->string('special_requirement')->nullable();
            $table->tinyInteger('status')->nullable();
            $table->integer('price')->nullable();
            $table->integer('rental_price')->nullable();
            $table->integer('paid')->nullable();
            $table->integer('remain')->nullable();
            $table->integer('space_id')->unsigned()->nullable();
            $table->foreign('space_id')->references('id')->on('spaces')->onDelete('set null');
            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('set null');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('booking_spaces');
    }
}
