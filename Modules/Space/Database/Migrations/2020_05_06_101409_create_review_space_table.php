<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReviewSpaceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('review_space', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->nullable();
            $table->longText('content')->nullable();
            $table->tinyInteger('sleep')->nullable();
            $table->tinyInteger('location')->nullable();
            $table->tinyInteger('service')->nullable();
            $table->tinyInteger('clearness')->nullable();
            $table->tinyInteger('room')->nullable();
            $table->tinyInteger('average')->nullable();
            $table->tinyInteger('status')->nullable();
            $table->integer('space_id')->unsigned()->nullable();
            $table->foreign('space_id')->references('id')->on('spaces')->onDelete('cascade');
            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('review_space');
    }
}
