<?php

namespace Modules\Space\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\User\Entities\User;

class ReviewSpace extends Model
{
    protected $table = 'review_space';
    protected $fillable = [];

    public function reviewSpace()
    {
        return $this->belongsTo(Space::class, 'space_id', 'id');
    }
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
