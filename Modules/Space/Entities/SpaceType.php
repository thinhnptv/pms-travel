<?php

namespace Modules\Space\Entities;

use Illuminate\Database\Eloquent\Model;

class SpaceType extends Model
{
    protected $fillable = [];
    protected $table = 'space_type';
}
