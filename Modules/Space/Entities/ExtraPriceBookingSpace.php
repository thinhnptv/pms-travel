<?php

namespace Modules\Space\Entities;

use Illuminate\Database\Eloquent\Model;

class ExtraPriceBookingSpace extends Model
{
    protected $table = 'extra_price_booking_spaces';
    protected $fillable = [];

    public function extraPriceBookingSpace()
    {
        return $this-$this->belongsTo(Booking::class, 'booking_space_id', 'id');
    }
}
