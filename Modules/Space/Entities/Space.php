<?php

namespace Modules\Space\Entities;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Modules\Category\Entities\Location;
use Modules\Category\Entities\SpaceTerm;
use Modules\User\Entities\User;


class Space extends Model
{
    protected $table = 'spaces';
    protected $fillable = [
        'location_id',
        'vendor_id',
    ];

    public function locationSpace()
    {
        return $this->belongsTo(Location::class, 'location_id', 'id');
    }
    public function user()
    {
        return $this->belongsTo(User::class,'vendor_id', 'id');
    }
    public function review()
    {
        return $this->hasMany(ReviewSpace::class, 'space_id', 'id');
    }
    public function faq()
    {
        return $this->hasMany(FaqSpace::class, 'space_id', 'id');
    }
    public function extra_price()
    {
        return $this->hasMany(ExtraPriceSpace::class, 'space_id', 'id');
    }
    public function spaceTypes()
    {
        return $this->belongsToMany(SpaceTerm::class, 'space_type', 'space_id', 'space_term_id');
    }
    public function spaceAmenities()
    {
        return $this->belongsToMany(SpaceTerm::class, 'space_amenity', 'space_id', 'space_term_id');
    }

    public function spaceBooking()
    {
        return $this->hasMany(Booking::class, 'space_id', 'id');
    }

    public function spaceAvailability()
    {
        return $this->hasMany(SpaceAvailability::class, 'space_id', 'id');
    }

    public function spaceFavourites()
    {
        return $this->hasMany(SpaceFavourite::class,'space_id','id');
    }

    public function isFavouritesBy($user)
    {
        return $this->spaceFavourites()->where('user_id',$user->id)->exists();
    }
}


