<?php

namespace Modules\Space\Entities;

use Illuminate\Database\Eloquent\Model;

class SpaceAvailability extends Model
{
    protected $table = 'space_availabilities';
    protected $fillable = [];

    public function availabilitySpaces()
    {
        return $this->belongsTo(Space::class, 'space_id', 'id');
    }
}
