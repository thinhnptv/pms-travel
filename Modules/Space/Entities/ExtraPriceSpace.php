<?php

namespace Modules\Space\Entities;

use Illuminate\Database\Eloquent\Model;

class ExtraPriceSpace extends Model
{
    protected $table = 'extraprice_space';
    protected $fillable = [];

    public function extrapriceSpace()
    {
        return $this->belongsTo(Space::class, 'space_id', 'id');
    }
}
