<?php

namespace Modules\Space\Entities;

use Illuminate\Database\Eloquent\Model;

class FaqSpace extends Model
{
    protected $table = 'faq_space';
    protected $fillable = [];

    public function faqSpace()
    {
        return $this->belongsTo(Space::class, 'space_id', 'id');
    }
}
