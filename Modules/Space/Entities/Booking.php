<?php

namespace Modules\Space\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\User\Entities\User;

class Booking extends Model
{
    protected $table = 'booking_spaces';
    protected $fillable = [];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function bookingSpace()
    {
        return $this->belongsTo(Space::class, 'space_id', 'id');
    }
    public function spaceExtraPriceBooking()
    {
        return $this->hasMany(ExtraPriceBookingSpace::class, 'booking_space_id', 'id');
    }
}
