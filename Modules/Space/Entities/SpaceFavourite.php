<?php

namespace Modules\Space\Entities;

use Illuminate\Database\Eloquent\Model;

class SpaceFavourite extends Model
{
    protected $table = 'space_favourite';
    protected $guarded = [];

}
