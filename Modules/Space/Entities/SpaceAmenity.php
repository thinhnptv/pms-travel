<?php

namespace Modules\Space\Entities;

use Illuminate\Database\Eloquent\Model;

class SpaceAmenity extends Model
{
    protected $fillable = [];
    protected $table = 'space_amenity';
}
