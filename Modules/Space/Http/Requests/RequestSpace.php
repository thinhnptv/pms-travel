<?php

namespace Modules\Space\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RequestSpace extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'content' => 'required',
            'album' =>'required',
            'location_id' =>'required',
            'price' => 'required',
            'max_guests' => 'required',
            'banner' => 'required',
            'address' => 'required',
            'bedroom' => 'required',
            'bathroom' => 'required',
            'square' => 'required',
            'map_lat' => 'required',
            'map_lng' => 'required',
            'sale_price' => [function ($attribute, $value, $fail) {
                if (($this->price) < $value) {
                    return $fail("Giá sale phải nhỏ hơn giá thường");
                }
            }],
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Tên không được để trống',
            'content.required' => 'Nội dung không được để trống',
            'album.required' => 'Ablum không được để trống',
            'location_id.required' => 'Địa điểm không được để trống',
            'price.required' => 'Giá không được để trống',
            'max_guests.required' => 'Số người không được để trống',
            'banner.required' => 'Ảnh không được để trống',
            'address.required' => 'Địa chỉ này không được để trống',
            'bedroom.required' => 'Số giường ngủ không được để trống',
            'bathroom.required' => 'Số phòng tắm không được để trống',
            'square.required' => 'Diện tích không được để trống',
            'map_lat.required' => 'Vĩ độ không được để trống',
            'map_lng.required' => 'Kinh độ không được để trống'
        ];
    }


    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
