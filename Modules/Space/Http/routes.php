<?php

Route::group(['middleware' => ['web', 'adminLogin'], 'prefix' => 'admin/spaces', 'namespace' => 'Modules\Space\Http\Controllers'], function()
{
    Route::get('/', 'SpaceController@index')->name('spaces.list');
    Route::get('/create', 'SpaceController@create')->name('spaces.create');
    Route::post('/create', 'SpaceController@store');
    Route::get('/update/{id}', 'SpaceController@edit')->name('spaces.edit');
    Route::post('/update/{id}', 'SpaceController@update');
    Route::get('/delete/{id}', 'SpaceController@destroy')->name('spaces.delete');
    Route::delete('/deleteAll', 'SpaceController@deleteAll')->name('spaces.deleteAll');
    Route::post('/change-status/{id}', 'SpaceController@changeStatus')->name('spaces.changeStatus');

    Route::get('/list-review', 'ReviewController@index')->name('spaces.review.list');
    Route::post('/booking-review-change', 'ReviewController@changeStatus')->name('spaces.review.change_status');

    Route::get('/booking-review', 'BookingController@index')->name('spaces.booking.list');
    Route::post('/booking-space-change', 'BookingController@changeStatus')->name('spaces.booking.change_status');

    Route::get('/availability/{id}', 'AvailabilityController@index')->name('spaces.availability.list');
    Route::get('/get-availability/{id}', 'AvailabilityController@get')->name('spaces.availability.get');
    Route::post('/save-availability', 'AvailabilityController@save')->name('spaces.availability_save');

    Route::get('/search', 'SpaceController@search')->name('space.searchspace');
});