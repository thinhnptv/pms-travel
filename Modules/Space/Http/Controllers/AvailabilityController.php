<?php

namespace Modules\Space\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Space\Entities\SpaceAvailability;
use Modules\Space\Entities\Space;
use Carbon\Carbon;

class AvailabilityController extends Controller
{
    protected $modelSpaceAvailability;
    protected $modelSpace;

    public function __construct(SpaceAvailability $modelSpaceAvailability, Space $modelSpace)
    {
        $this->modelSpaceAvailability = $modelSpaceAvailability;
        $this->modelSpace = $modelSpace;
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index($id)
    {
        return view('space::availability.index', compact('id') );
    }

    public function get($id, Request $request)
    {
        $space = $this->modelSpace::query()->findOrFail($id);
        $start = Carbon::createFromFormat('Y-m-d', $request->start);
        $end = $start->copy()->addDays(41);

        $numberDay = $end->diffInDays($start);
        $arr = [];
        $arr[] = $start->format('Y-m-d');
        for ($i = 1; $i <= $numberDay ; $i++) {
            $day    = $start->addDays($i);
            $arr[] = $day->format('Y-m-d');
            $start->subDays($i);
        }
        $spaceAvailability = $this->modelSpaceAvailability->where('space_id', $id)->get();
        $arrDate = [];
        foreach ($spaceAvailability as $item) {
            $arrDate[] = $item->date;
        }
        
        $arrResult = array_map(function($date) use ($spaceAvailability, $arrDate, $space) {
            
                if(in_array($date, $arrDate)) {
                    foreach ($spaceAvailability as $items) {
                        if($date == $items->date) {
                            if($items->remaining == 0) {
                                $event = 'Full book';
                                $className = 'bg-danger';
                            }else{
                                $event = $items->price;
                                $className = 'bg-success';
                            }
                            $remaining = $items->remaining;
                            $price = $items->price;
                            $spaceId = $items->space_id;
                        }
                    }
                    $result = [
                        'start' => $date,
                        'spaceId' => $spaceId,
                        'remaining' => $remaining,
                        'price' => $price,
                        'className' => $className,
                        'title' => $event       
                    ]; 
                    
                }else{
                    if(!empty($space->sale_price) && $space->sale_price < $space->price) {
                        $event = $space->sale_price;
                        $remaining = 1;
                        $price = $space->sale_price;
                        $spaceId = $space->id;
                    }else {
                        $event = $space->price;
                        $remaining = 1;
                        $price = $space->price;
                        $spaceId = $space->id;
                    }

                    $result = [
                        'start' => $date,
                        'spaceId' => $spaceId,
                        'remaining' => $remaining,
                        'price' => $price,
                        'className' => 'bg-warning',
                        'title' => $event
                    ]; 
                    
                }
                return $result;
            }, $arr);
        return response()->json($arrResult);
    }

    public function save(Request $request)
    {
        // dd($request->all());
        if($request->remaining <= 0 ){
            $remaining = 0;
        }else {
            $remaining = $request->remaining;
        }
        if($request->price <= 0 ){
            $price = 0;
        }else {
            $price = $request->price;
        }
        $spaceAvailability = $this->modelSpaceAvailability->where('date', $request->day)->where('space_id', $request->spaceId)->first();
        if($spaceAvailability != null) {
            $availability = $this->modelSpaceAvailability->findOrFail($spaceAvailability->id);
            $availability->remaining = $remaining;
            $availability->price = $price;
            $availability->save();
            
        }else {
            $availability = new $this->modelSpaceAvailability;
            $availability->remaining = $remaining;
            $availability->space_id = $request->spaceId;
            $availability->date = $request->day;
            $availability->price = $price;
            $availability->save();
        }
        return Response()->json([
            'type' => 1,
            'mess' => 'Cập nhật thành công',
        ]);
    }
    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('space::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('space::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('space::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
}
