<?php

namespace Modules\Space\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Space\Entities\ReviewSpace;

class ReviewController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $reviews = ReviewSpace::query()->with('reviewSpace', 'user')->get();
        return view('space::reviews.index', compact('reviews'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('space::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('space::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('space::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }


    public function changeStatus(Request $request)
    {
        $ids = $request->ids;
        $status = $request->status;
        foreach ($ids as $key => $val){
            $changeStatus = ReviewSpace::findOrFail($val);
            $changeStatus->status = $status;
            $changeStatus->update();
        }
        return response()->json([
            'type' => 1,
            'status' => showStatusSpace($status),
            'success' => 'Thay đổi trạng thái thành công'
        ]);
    }
}
