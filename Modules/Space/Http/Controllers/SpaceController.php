<?php

namespace Modules\Space\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Category\Entities\Location;
use Modules\Category\Entities\SpaceTerm;
use Modules\Space\Entities\ExtraPriceSpace;
use Modules\Space\Entities\FaqSpace;
use Modules\Space\Entities\Space;
use Modules\User\Entities\User;
use Carbon\Carbon;
use Modules\Category\Entities\SpaceAttribute;
use Modules\Space\Entities\SpaceAmenity;
use Modules\Space\Entities\SpaceType;
use Modules\Space\Http\Requests\RequestSpace;

class SpaceController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $spaces = Space::query()->with('locationSpace', 'user', 'review')->orderByDesc('id')->paginate('15');
        return view('space::spaces.index', compact('spaces'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $locations = Location::where('status', 1)->get();
        $users = User::all();
        // $spaceAttribute = SpaceAttribute::where('status', 1)->get();
        // $spaceAttribute = SpaceAttribute::where('status', 1)->get();
        $space_type = SpaceTerm::query()->where('attribute_id', 1)->get();
        $space_amenity = SpaceTerm::query()->where('attribute_id', 2)->get();
        $spaceType = SpaceTerm::all()->groupBy('attribute_id');
        return view('space::spaces.create', compact('locations', 'users', 'spaceType', 'space_amenity', 'space_type'));
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(RequestSpace $request)
    {
        $str_banner = '';
        $str_album = '';

        if ($request->input('banner')) {
            $str_banner = checkImage($request->input('banner'));
        }
        if($request->input('album')) {
            $str_album = checkMultipleImage($request->input('album'));
        }

        $spaces = new Space();
        $spaces->name = $request->name;
        $spaces->slug = str_slug($request->name);
        $spaces->content = $request->content;
        $spaces->banner = $str_banner;
        $spaces->gallery = $str_album;
        if($request->featured == 1){
            $spaces->featured = $request->featured;
        }else {
            $spaces->featured = 0;
        }
        $spaces->size = $request->square;
        $spaces->number_bed = $request->bedroom;
        $spaces->number_bathroom = $request->bathroom;
        $spaces->address = $request->address;
        $spaces->latitu = $request->map_lat;
        $spaces->longtitu = $request->map_lng;
        $spaces->seo_title = $request->seo_title;
        $spaces->seo_description = $request->seo_description;
        $spaces->price = $request->price;
        if(!empty($request->sale_price)) {
            if($request->price > $request->sale_price ) {
                $spaces->sale_price = $request->sale_price;
            }else {
                $spaces->price = $request->price;
            }
        }else {
            $spaces->sale_price = null;
        }
        $spaces->max_guests = $request->max_guests;
        $spaces->status = $request->status;
        $spaces->state = $request->state;
        $spaces->mapzoom = $request->map_zoom;
        $spaces->location_id = $request->location_id;
        $spaces->vendor_id = $request->vendor_id;
        $spaces->save();

        if (isset($request->faqs)){
            foreach ($request->faqs as $key => $val){
                if (isset($val['title']) && isset($val['content'])){
                    $faqs = new FaqSpace();
                    $faqs->title = $val['title'];
                    $faqs->content = $val['content'];
                    $faqs->space_id = $spaces->id;
                    $faqs->save();
                }
            }
        }

        if (isset($request->extra_price)){
            foreach ($request->extra_price as $key => $val){
                if (isset($val['title']) && isset($val['price']) && isset($val['type'])){
                    $extra_prices = new ExtraPriceSpace();
                    $extra_prices->name = $val['title'];
                    $extra_prices->price = $val['price'];
                    $extra_prices->type = $val['type'];
                    $extra_prices->space_id = $spaces->id;
                    $extra_prices->save();
                }

            }
        }
        if (isset($request->spacetype)){
            $spaces->spaceTypes()->attach($request->spacetype);
        }
        if (isset($request->spaceAmenity)){
            $spaces->spaceAmenities()->attach($request->spaceAmenity);
        }

        return redirect()->route('spaces.list')->with(['success' => 'Thêm mới thành công']);

    }


    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('space::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {
        $spaces = Space::query()->with('faq', 'extra_price')->findOrFail($id);
        $locations = Location::where('status', 1)->get();
        $users = User::all();
        $spaceAttribute = SpaceAttribute::where('status', 1)->get();
        $spaceType = SpaceTerm::all()->where('attribute_id', 1);
        $spaceAmenity = SpaceTerm::all()->where('attribute_id', 2);
        $space_amenity = [];
        $space_type = [];
        foreach ($spaces->spaceTypes as $key => $val){
            $space_type[] = $val->id;
        }
        foreach ($spaces->spaceAmenities as $key => $val){
            $space_amenity[] = $val->id;
        }
        return view('space::spaces.edit', compact('locations', 'users', 'spaceType','spaces', 'space_type', 'spaceAttribute', 'space_amenity', 'spaceAmenity'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(RequestSpace $request, $id)
    {
        $spaces = Space::findOrFail($id);
        $space_id = $spaces->id;
        $spaceTypeExist = SpaceType::select('id')->where('space_id', $space_id)->get()->toArray();
        SpaceType::whereIn('id', $spaceTypeExist)->delete();
        $spaceAmenityExist = SpaceAmenity::select('id')->where('space_id', $space_id)->get()->toArray();
        SpaceAmenity::whereIn('id', $spaceAmenityExist)->delete();
        $str_banner = '';
        $str_album = '';

        if ($request->input('banner')) {
            $str_banner = checkImage($request->input('banner'));
        }
        if($request->input('album')) {
            $str_album = checkMultipleImage($request->input('album'));
        }

        $spaces->name = $request->name;
        $spaces->slug = str_slug($request->name);
        $spaces->content = $request->content;
        $spaces->banner = $str_banner;
        $spaces->gallery = $str_album;
        $spaces->size = $request->square;
        $spaces->number_bed = $request->bedroom;
        $spaces->number_bathroom = $request->bathroom;
        $spaces->address = $request->address;
        $spaces->latitu = $request->map_lat;
        $spaces->longtitu = $request->map_lng;
        $spaces->seo_title = $request->seo_title;
        $spaces->seo_description = $request->seo_description;
        $spaces->price = $request->price;
        if(!empty($request->sale_price)) {
            if($request->price > $request->sale_price ) {
                $spaces->sale_price = $request->sale_price;
            }else {
                $spaces->price = $request->price;
                $spaces->sale_price = null;
            }
        }else {
            $spaces->sale_price = null;
        }
        if($request->featured == 1){
            $spaces->featured = $request->featured;
        }else {
            $spaces->featured = 0;
        }
        $spaces->max_guests = $request->max_guests;
        $spaces->status = $request->status;
        $spaces->state = $request->state;
        $spaces->mapzoom = $request->map_zoom;
        $spaces->location_id = $request->location_id;
        $spaces->vendor_id = $request->vendor_id;
        $spaces->save();

        $deleteFaq = FaqSpace::where('space_id', $id)->delete();
        if (isset($request->faqs)){
            foreach ($request->faqs as $key => $val){
                if (isset($val['title']) && isset($val['content'])){
                    $faqs = new FaqSpace();
                    $faqs->title = $val['title'];
                    $faqs->content = $val['content'];
                    $faqs->space_id = $spaces->id;
                    $faqs->save();
                }
            }
        }

        $deleteExtraPrice = ExtraPriceSpace::where('space_id', $id)->delete();
        if (isset($request->extra_price)){
            foreach ($request->extra_price as $key => $val){
                if (isset($val['title']) && isset($val['price']) && isset($val['type'])){
                    $extra_prices = new ExtraPriceSpace();
                    $extra_prices->name = $val['title'];
                    $extra_prices->price = $val['price'];
                    $extra_prices->type = $val['type'];
                    $extra_prices->space_id = $spaces->id;
                    $extra_prices->save();
                }
            }
        }

        if (isset($request->spacetype)){
            $spaces->spaceTypes()->syncWithoutDetaching($request->spacetype);
        }
        if (isset($request->spaceAmenity)){
            $spaces->spaceAmenities()->syncWithoutDetaching($request->spaceAmenity);
        }

        return redirect()->route('spaces.list')->with(['success' => 'Sửa thành công']);

    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {
        $spaces = Space::findOrFail($id);
        $spaces->spaceTypes()->detach();
        $spaces->spaceAmenities()->detach();
        $spaces->delete();
        return redirect()->route('spaces.list')->with(['success' => 'Xóa thành công']);
    }

    public function deleteAll(Request $request)
    {
        $ids = $request->ids;
        foreach ($ids as $key => $val){
            $spaces = Space::findorFail($val);
            $spaces->spaceTypes()->detach();
            $spaces->spaceAmenities()->detach();
            $spaces->delete();
        }
        return response()->json(['success' => 'Xóa thành công']);
    }

    public function changeStatus($id)
    {
        $changeStatus = Space::findorFail($id);
        if ($changeStatus->status == 1){
            $changeStatus->status = 0;
            $changeStatus->update();
            return response()->json([
                'status' => 0,
                'success' => 'Thay đổi trạng thái thành công'
            ]);
        }
        else{
            $changeStatus->status = 1;
            $changeStatus->update();
            return response()->json([
                'status' => 1,
                'success' => 'Thay đổi trạng thái thành công'
            ]);
        }
    }

    public function search(Request $request,Space $spaces)
    {
        if($request->ajax()){
            $query = $request->get('query');
            if($query != ' '){
                $data = Space::query()
                        ->where('spaces.name','like','%'.$query.'%')
                        ->with('locationSpace', 'user', 'review')
                        ->get();
            }else{
                $data = Space::query()->with('locationSpace', 'user', 'review')->get();
            }
            if($data->count() > 0){
                $spaces = $data;
                return view('space::spaces.space-search', compact('spaces'));
            }else{
                return view('space::spaces.space-search-nodata');
            }
        }
    }
}
