@extends('base::layouts.master')

@section('css')
    <!-- Plugin css -->
    <link href="{{ URL::asset('assets/libs/fullcalendar/fullcalendar.min.css')}}" rel="stylesheet" type="text/css" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

@section('content')

    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">UBold</a></li>
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Apps</a></li>
                            <li class="breadcrumb-item active">Calendar</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Calendar</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->


        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="panel">
                            <div class="panel-body">
                                <div class="filter-div d-flex justify-content-between">
                                    <div class="col-left">
                                        <form method="get" action="" class="filter-form filter-form-left d-flex flex-column flex-sm-row" role="search">
                                            <input type="text" name="s" value="" placeholder="Search by name" class="form-control" />
                                            <button style="margin-left: 15px" class="btn-info btn btn-icon btn_search" type="submit">Search</button>
                                        </form>
                                    </div>
                                    <div class="col-right">
                                        <span class="count-string">Showing 1 - 12 of 12 spaces</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            
                            <div class="col-lg-12">
                                <div class="response"></div>
                                <div id="calendar"></div>
                            </div> <!-- end col -->

                        </div>  <!-- end row -->
                    </div> <!-- end card body-->
                </div> <!-- end card -->

                <!-- Add New Event MODAL -->
                <div class="modal fade" id="event-modal" tabindex="-1">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title">Add New Event</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            </div>
                            <div class="modal-body p-3">
                            </div>
                            <div class="text-right p-3">
                                <button type="button" class="btn btn-light " data-dismiss="modal">Close</button>
                                <button type="button" class="btn btn-success save-event  ">Create event</button>
                                <button type="button" class="btn btn-danger delete-event  " data-dismiss="modal">Delete</button>
                            </div>
                        </div> <!-- end modal-content-->
                    </div> <!-- end modal dialog-->
                </div>
                <!-- end modal-->

                <!-- Modal Add Category -->
                <div class="modal fade" id="add-category" tabindex="-1">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title">Add a category</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            </div>
                            <div class="modal-body p-3">
                                <form>
                                    <div class="form-group">
                                        <label class="control-label">Category Name</label>
                                        <input class="form-control form-white" placeholder="Enter name" type="text" name="category-name"/>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Choose Category Color</label>
                                        <select class="form-control form-white" data-placeholder="Choose a color..." name="category-color">
                                            <option value="primary">Primary</option>
                                            <option value="success">Success</option>
                                            <option value="danger">Danger</option>
                                            <option value="info">Info</option>
                                            <option value="warning">Warning</option>
                                            <option value="dark">Dark</option>
                                        </select>
                                    </div>

                                </form>
                                <div class="text-right pt-2">
                                    <button type="button" class="btn btn-light " data-dismiss="modal">Close</button>
                                    <button type="button" class="btn btn-primary ml-1   save-category" data-dismiss="modal">Save</button>
                                </div>

                            </div> <!-- end modal-body-->
                        </div> <!-- end modal-content-->
                    </div> <!-- end modal dialog-->
                </div>
                <!-- end modal-->
            </div>
            <!-- end col-12 -->
        </div> <!-- end row -->

    </div> <!-- container -->
    <input type="hidden" name="" class="data-url" data-url="{{ route('spaces.availability.get', $id) }}">
@endsection

@section('script')
    <script>
        var id = {{$id}};
        var _url = '{{route('spaces.availability_save')}}';
        var _url_get = $('.data-url').attr('data-url');
        
    </script>
    <!-- plugin js -->
    <script src="{{ URL::asset('assets/libs/moment/moment.min.js')}}"></script>
    <script src="{{ URL::asset('assets/libs/jquery-ui/jquery-ui.min.js')}}"></script>
    <script src="{{ URL::asset('assets/libs/fullcalendar/fullcalendar.min.js')}}"></script>
    <!-- Calendar init -->
    <script src="{{ URL::asset('assets/js/pages/calendar-space.init.js')}}"></script>

@endsection