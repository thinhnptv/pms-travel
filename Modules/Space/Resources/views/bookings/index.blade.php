@extends('base::layouts.master')
@section('css')
    <link href="{{ URL::asset('assets/libs/bootstrap-table/bootstrap-table.min.css')}}" rel="stylesheet"
          type="text/css"/>
    <link href="{{ URL::asset('assets/libs/select2/select2.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ URL::asset('assets/libs/toastr/toastr.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ URL::asset('assets/css/huy.css')}}" rel="stylesheet" type="text/css" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection
@section('content')
    <!-- Start Content-->
    <div class="container-fluid">
        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">UBold</a></li>
                            <li class="breadcrumb-item"><a href="javascript: void(0);">News</a></li>
                            <li class="breadcrumb-item active">All</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Bookings Spaces</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row mb-2">
                            <div class="col-md-7">
                                <form class="form-inline">
                                    <div class="form-group mb-2">
                                        <label for="inputPassword2" class="sr-only">Search</label>
                                        <input type="search" class="form-control" id="inputPassword2" placeholder="Search...">
                                    </div>
                                </form>
                            </div>
                            <div class="col-md-5">
                                <div class="form-inline">
                                    <div class="mr-2">
                                        <select class="status form-control mb-2">
                                            <option value="">Bulk Actions</option>
                                            <option value="0">Completed</option>
                                            <option value="1">Processing</option>
                                            <option value="2">Confirmed</option>
                                            <option value="3">Cancelled</option>
                                            <option value="4">Paid</option>
                                            <option value="5">Unpaid</option>
                                            <option value="6">Partial Payment</option>
                                        </select>
                                    </div>
                                    <button type="button"  class="status-change btn-info btn btn-icon dungdt-apply-form-btn mb-2 mr-2" data-value="">Apply</button>
                                    <button type="button" id="delete-checkbox-all" class="delete-all btn btn-danger waves-effect waves-light mb-2 mr-2">Delete All</button>
                                </div>
                            </div><!-- end col-->
                        </div>

                        <div class="table-responsive">
                            <table class="table table-borderless table-hover mb-0">
                                <thead class="thead-light">
                                <tr>
                                    <th style="width: 20px;">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="customCheckAll">
                                            <label class="custom-control-label" for="customCheckAll">&nbsp;</label>
                                        </div>
                                    </th>
                                    <th>Name</th>
                                    <th>Customer</th>
                                    <th>Total</th>
                                    <th>Status</th>
                                    <th>Payment Method</th>
                                    <th>Created At</th>
                                    <th style="width: 82px;">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(isset($bookings))
                                    @foreach($bookings as $key => $val)
                                        <tr id="tr" class="approved">
                                            <td>
                                                <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="checkbox custom-control-input" id="customCheck-{{ $val->id }}" data-value="{{ $val->id }}">
                                                    <label class="custom-control-label" for="customCheck-{{ $val->id }}">&nbsp;</label>
                                                </div>
                                            </td>
                                            @if(isset($val->bookingSpace->id))
                                                <td>
                                                    {{ optional($val->bookingSpace)->name }}
                                                </td>
                                            @endif
                                            @if(isset($val->user->id))
                                                <td>
                                                    <ul>
                                                        <li>Name: {{ optional($val->user)->full_name }}</li>
                                                        <li>Email: {{ optional($val->user)->email }}</li>
                                                        <li>Phone: {{ optional($val->user)->phone }}</li>
                                                        <li>Address: {{ optional($val->user)->address }}</li>
                                                        <li>Phone: {{ optional($val->user)->phone }}</li>
                                                        <li>Custom Requirement: {{ $val->special_requirement }}</li>
                                                    </ul>
                                                </td>
                                            @endif
                                            <td>
                                                ${{ $val->price * $dates + $val->total }}
                                            </td>
                                            <td class="td-status">
                                                {{ showStatusBooking($val->status) }}
                                            </td>
                                            <td>
                                                {{ $val->payment_method }}
                                            </td>
                                            <td>
                                                {{ $val->created_at->format('d/m/Y H:i') }}
                                            </td>
                                            <td>
                                                <div class="dropdown">
                                                    <button class="btn btn-secondary btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Actions</button>
                                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                                                        <a class="dropdown-item" href="#" data-toggle="modal" data-target="#modal-booking-{{ $val->id }}">Detail</a>
                                                        {{--<a class="dropdown-item" href="http://sandbox.bookingcore.org/admin/module/report/booking/email_preview/5">Email Preview</a>--}}
                                                    </div>
                                                </div>
                                                <div class="modal fade" id="modal-booking-{{ $val->id }}">
                                                    <div class="modal-dialog modal-dialog-centered modal-lg">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h4 class="modal-title">Booking ID: #{{ $val->id }}</h4>
                                                            </div>

                                                            <div class="modal-body">
                                                                <ul class="nav nav-tabs">
                                                                    <li class="nav-item">
                                                                        <a class="nav-link active" data-toggle="tab" href="#booking-detail-{{ $val->id }}">Booking Detail</a>
                                                                    </li>
                                                                    <li class="nav-item">
                                                                        <a class="nav-link" data-toggle="tab" href="#booking-customer-{{ $val->id }}">
                                                                            Customer Information
                                                                        </a>
                                                                    </li>
                                                                </ul>
                                                                <div class="tab-content">
                                                                    <div id="booking-detail-{{ $val->id }}" class="tab-pane active">
                                                                        <br />
                                                                        <div class="booking-review">
                                                                            <div class="booking-review-content">
                                                                                <div class="review-section">
                                                                                    <div class="info-form">
                                                                                        <ul>
                                                                                            <li>
                                                                                                <div class="label">Booking Status</div>
                                                                                                <div class="val">
                                                                                                    {{ showStatusBooking($val->status) }}
                                                                                                </div>
                                                                                            </li>
                                                                                            <li>
                                                                                                <div class="label">Booking Date</div>
                                                                                                <div class="val">{{ $val->created_at->format('d/m/Y') }}</div>
                                                                                            </li>
                                                                                            <li>
                                                                                                <div class="label">Payment Method</div>
                                                                                                <div class="val">Offline Payment</div>
                                                                                            </li>
                                                                                            @if(isset($val->user->id))
                                                                                            <li>
                                                                                                <div class="label">Vendor</div>
                                                                                                    <div class="val"><a href="javastript:void(0)" target="_blank">{{ optional($val->user)->full_name }}</a></div>
                                                                                            </li>
                                                                                            @endif
                                                                                        </ul>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="more-booking-review">
                                                                            <div class="booking-review">
                                                                                <div class="booking-review-content">
                                                                                    <div class="review-section">
                                                                                        <ul class="review-list">
                                                                                            <li class="li-status">
                                                                                                <div class="label">Start date:</div>
                                                                                                <div class="val">
                                                                                                    {{ $startDate->format('d/m/Y') }}
                                                                                                </div>
                                                                                            </li>
                                                                                            <li>
                                                                                                <div class="label">End date:</div>
                                                                                                <div class="val">
                                                                                                    {{ $endDate->format('d/m/Y') }}
                                                                                                </div>
                                                                                            </li>
                                                                                            <li>
                                                                                                <div class="label">Days:</div>
                                                                                                <div class="val">
                                                                                                    {{ $dates }}
                                                                                                </div>
                                                                                            </li>
                                                                                            @if(isset($val->bookingSpace->id))
                                                                                            <li>
                                                                                                <div class="label">Adults:</div>
                                                                                                <div class="val">
                                                                                                    {{ optional($val->bookingSpace)->adults }}
                                                                                                </div>
                                                                                            </li>
                                                                                            <li>
                                                                                                <div class="label">Children:</div>
                                                                                                <div class="val">
                                                                                                    {{ optional($val->bookingSpace)->children }}
                                                                                                </div>
                                                                                            </li>
                                                                                            @endif
                                                                                        </ul>
                                                                                    </div>
                                                                                    <div class="review-section total-review">
                                                                                        <ul class="review-list">
                                                                                            <li>
                                                                                                <div class="label">Rental price</div>
                                                                                                <div class="val">
                                                                                                     ${{ $val->price * $dates }}
                                                                                                </div>
                                                                                            </li>
                                                                                            <li>
                                                                                                <div class="label-title"><strong>Extra Prices:</strong></div>
                                                                                            </li>
                                                                                            @if(isset($val->spaceExtraPriceBooking) && count($val->spaceExtraPriceBooking) > 0)
                                                                                            <li class="no-flex">
                                                                                                <ul>
                                                                                                    @foreach($val->spaceExtraPriceBooking as $keys => $vals)
                                                                                                    <li>
                                                                                                        <div class="label">{{ $vals->title }}</div>
                                                                                                        <div class="val">
                                                                                                            ${{ $vals->price }}
                                                                                                        </div>
                                                                                                    </li>
                                                                                                    @endforeach
                                                                                                </ul>
                                                                                            </li>
                                                                                            @endif
                                                                                            <li>
                                                                                                <div class="label">
                                                                                                    Cleaning fee
                                                                                                    <i class="icofont-info-circle" data-toggle="tooltip" data-placement="top" title="One-time fee charged by host to cover the cost of cleaning their space."></i>
                                                                                                </div>
                                                                                                <div class="val">
                                                                                                    $100
                                                                                                </div>
                                                                                            </li>
                                                                                            <li>
                                                                                                <div class="label">
                                                                                                    Service fee
                                                                                                    <i class="icofont-info-circle" data-toggle="tooltip" data-placement="top" title="This helps us run our platform and offer services like 24/7 support on your trip."></i>
                                                                                                </div>
                                                                                                <div class="val">
                                                                                                    $200
                                                                                                </div>
                                                                                            </li>
                                                                                            <li class="final-total d-block">
                                                                                                <div class="d-flex justify-content-between">
                                                                                                    <div class="label">Total:</div>
                                                                                                    <div class="val">${{ $val->price * $dates + $val->total }}</div>
                                                                                                </div>
                                                                                                <div class="d-flex justify-content-between">
                                                                                                    <div class="label">Paid:</div>
                                                                                                    <div class="val">${{ $val->paid }}</div>
                                                                                                </div>
                                                                                                <div class="d-flex justify-content-between">
                                                                                                    <div class="label">Remain:</div>
                                                                                                    <div class="val">${{ $val->price * $dates + $val->total - $val->paid}}</div>
                                                                                                </div>
                                                                                            </li>
                                                                                        </ul>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div id="booking-customer-{{ $val->id }}" class="tab-pane fade">
                                                                        <br />
                                                                        <div class="booking-review">
                                                                            <h4 class="booking-review-title">Your Information</h4>
                                                                            <div class="booking-review-content">
                                                                                <div class="review-section">
                                                                                    <div class="info-form">
                                                                                        @if(isset($val->user->id))
                                                                                        <ul>
                                                                                            <li class="info-first-name">
                                                                                                <div class="label">First name</div>
                                                                                                <div class="val">{{ optional($val->user)->firstname }}</div>
                                                                                            </li>
                                                                                            <li class="info-last-name">
                                                                                                <div class="label">Last name</div>
                                                                                                <div class="val">{{ optional($val->user)->lastname }}</div>
                                                                                            </li>
                                                                                            <li class="info-email">
                                                                                                <div class="label">Email</div>
                                                                                                <div class="val"><a href="#" class="__cf_email__" data-cfemail="">{{ optional($val->user)->email }}</a></div>
                                                                                            </li>
                                                                                            <li class="info-phone">
                                                                                                <div class="label">Phone</div>
                                                                                                <div class="val">{{ optional($val->user)->phone }}</div>
                                                                                            </li>
                                                                                            <li class="info-address">
                                                                                                <div class="label">Address line 1</div>
                                                                                                <div class="val">Bắc Ninh</div>
                                                                                            </li>
                                                                                            <li class="info-address2">
                                                                                                <div class="label">Address line 2</div>
                                                                                                <div class="val">Hà Nội</div>
                                                                                            </li>
                                                                                            <li class="info-city">
                                                                                                <div class="label">City</div>
                                                                                                <div class="val"></div>
                                                                                            </li>
                                                                                            <li class="info-state">
                                                                                                <div class="label">State/Province/Region</div>
                                                                                                <div class="val"></div>
                                                                                            </li>
                                                                                            <li class="info-zip-code">
                                                                                                <div class="label">ZIP code/Postal code</div>
                                                                                                <div class="val"></div>
                                                                                            </li>
                                                                                            <li class="info-country">
                                                                                                <div class="label">Country</div>
                                                                                                <div class="val">Viet Nam</div>
                                                                                            </li>
                                                                                            <li class="info-notes">
                                                                                                <div class="label">Special Requirements</div>
                                                                                                <div class="val"></div>
                                                                                            </li>
                                                                                        </ul>
                                                                                        @endif
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="modal-footer">
                                                                <span class="btn btn-secondary" data-dismiss="modal">Close</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div> <!-- end card-body-->
                </div> <!-- end card-->
            </div> <!-- end col -->
        </div>
    </div>
@stop
@section('script-bottom')
    <script src="{{ URL::asset('assets/libs/toastr/toastr.min.js')}}"></script>
    <script type="text/javascript">
        @if(session('success'))
        toastr.success('{{ session('success') }}', 'Thông báo', {timeOut: 5000});
        @endif

        $(document).ready(function () {


            $(document).on("click", "#customCheckAll", function () {
                if($(this).is(':checked',true))
                {
                    $(".checkbox").prop('checked', true);
                } else {
                    $(".checkbox").prop('checked',false);
                }
            });

            $(document).on("click", ".status-change", function () {
                let value_id = [];
                let status = $('.status').val();
                // console.log(status);
                $('.checkbox:checked').each(function () {
                    let _this = $(this);
                    value_id.push(_this.data('value'));
                    // console.log(value_id);
                });
                console.log(value_id);
                if (value_id.length > 0){
                    if (status >= 0){
                        $.ajax({
                            url : '{{ route('spaces.booking.change_status') }}',
                            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                            dataType : 'json',
                            type : 'post',
                            data: {
                                ids : value_id,
                                status : status,
                            },
                            success: function (result) {
                                if (result.type == 1){
                                    toastr.success(result.success, 'Thông báo', {timeOut: 1500});
                                    $('.checkbox:checked').each(function () {
                                        $(this).parents("tr").children('.td-status').html(result.status);
                                        $(this).parents("tr").find('.li-status').children('.val').html(result.status);

                                    });
                                }
                            }
                        });
                    }
                }
                else {
                    toastr.error("Error", "Bạn cần chọn hàng để thay đổi trạng thái", "error", 1000);
                }
            });

        });

        // $(document).ready(function () {
        //     $(document).on("click", ".delete", function () {
        //         alert("Bạn chắc chắn muốn xóa");
        //     });
        //     $(document).on("click", "#customCheckAll", function () {
        //         $('input:checkbox').not(this).prop('checked', this.checked);
        //     });
        //
        //     $(document).on("click", "#delete-checkbox-all", function () {
        //         let value_id = [];
        //         $('.checkbox:checked').each(function () {
        //             value_id.push($(this).data('value'));
        //             console.log(value_id);
        //         });
        //
        //         if (value_id.length > 0) {
        //             $.ajax({
        //                 url : 'spaces/deleteAll',
        //                 headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        //                 dataType : 'json',
        //                 type : 'delete',
        //                 data: {
        //                     ids : value_id,
        //                 },
        //                 success: function (result) {
        //                     alert("Bạn chắc chắn muốn xóa");
        //                     toastr.success(result.success, 'Thông báo', {timeOut: 5000});
        //                     $.each(value_id, function (key, value) {
        //                         console.log(value);
        //                         $('#tr-'+value).hide();
        //                     })
        //                 }
        //             });
        //         } else {
        //             toastr.error("Error", "Bạn cần chọn hàng cần xóa", "error", 1000);
        //         };
        //     });
        //
        // });
    </script>
    <!-- Init js -->
    <script src="{{ URL::asset('assets/libs/bootstrap-table/bootstrap-table.min.js')}}"></script>
    <!-- Bootstrap Tables js -->
    <script src="{{ URL::asset('assets/js/pages/bootstrap-tables.init.js')}}"></script>
    <script src="{{ URL::asset('assets/libs/select2/select2.min.js') }}"></script>
@endsection




