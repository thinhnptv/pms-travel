@extends('base::layouts.master')
@section('css')
    <link href="{{ URL::asset('assets/libs/bootstrap-table/bootstrap-table.min.css')}}" rel="stylesheet"
          type="text/css"/>
    <link href="{{ URL::asset('assets/libs/select2/select2.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ URL::asset('assets/libs/toastr/toastr.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ URL::asset('assets/css/webt.css')}}" rel="stylesheet" type="text/css" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection
@section('content')
    <!-- Start Content-->
    <div class="container-fluid">
        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">UBold</a></li>
                            <li class="breadcrumb-item"><a href="javascript: void(0);">News</a></li>
                            <li class="breadcrumb-item active">All</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Reviews Spaces</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row mb-2">
                            <div class="col-md-7">
                                <form class="form-inline">
                                    <div class="form-group mb-2">
                                        <label for="inputPassword2" class="sr-only">Search</label>
                                        <input type="search" class="form-control" id="inputPassword2" placeholder="Search...">
                                    </div>
                                </form>
                            </div>
                            <div class="col-md-5">
                                <div class="form-inline">
                                    <div class="mr-2">
                                        <select class="status form-control mb-2">
                                            <option value="">Bulk Actions</option>
                                            <option value="0">Approved</option>
                                            <option value="1">Pending</option>
                                            <option value="2">Spam</option>
                                            <option value="3">Trash</option>
                                        </select>
                                    </div>
                                    <button type="button"  class="status-change btn-info btn btn-icon dungdt-apply-form-btn mb-2 mr-2" data-value="">Apply</button>
                                    <button type="button" id="delete-checkbox-all" class="delete-all btn btn-danger waves-effect waves-light mb-2 mr-2">Delete All</button>
                                </div>
                            </div><!-- end col-->
                        </div>

                        <div class="table-responsive">
                            <table class="table table-borderless table-hover mb-0">
                                <thead class="thead-light">
                                <tr>
                                    <th style="width: 20px;">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="customCheckAll">
                                            <label class="custom-control-label" for="customCheckAll">&nbsp;</label>
                                        </div>
                                    </th>
                                    <th >Author</th>
                                    <th>Review Content</th>
                                    <th>In Response To</th>
                                    <th>Status</th>
                                    <th>Submitted On</th>
                                    <th style="width: 82px;">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(isset($reviews))
                                    @foreach($reviews as $key => $val)
                                        <tr id="tr" class="approved">
                                            <td>
                                                <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="checkbox custom-control-input" id="customCheck-{{ $val->id }}" data-value="{{ $val->id }}">
                                                    <label class="custom-control-label" for="customCheck-{{ $val->id }}">&nbsp;</label>
                                                </div>
                                            </td>
                                            @if(isset($val->user->id))
                                            <td>
                                                {{ optional($val->user)->full_name }}
                                            </td>
                                            @else
                                                <td>[Author deleted]</td>
                                            @endif
                                            <td>
                                                <div style="color: #333333;font-weight: 800; font-size: 15px">{{ $val->title }}</div>
                                                <div>{{ $val->content }}</div>
                                                <table class="table table-borderless">
                                                    <thead class="">
                                                    <tr>
                                                        <th>Average:</th>
                                                        <th>
                                                            <ul class="review-star d-flex flex-row ml-2">
                                                                @for ($i = 0; $i < $val->average; $i++)
                                                                    <li><i class="fa fa-star"></i></li>
                                                                @endfor
                                                                @for ($i = 0; $i < 5-$val->average; $i++)
                                                                    <li><i class="far fa-star"></i></li>
                                                                @endfor
                                                            </ul>
                                                        </th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr>
                                                        <td>Sleep:</td>
                                                        <td>
                                                            <ul class="review-star d-flex flex-row  ml-2">
                                                                @for ($i = 0; $i < $val->sleep; $i++)
                                                                    <li><i class="fa fa-star"></i></li>
                                                                @endfor
                                                                @for ($i = 0; $i < 5-$val->sleep; $i++)
                                                                    <li><i class="far fa-star"></i></li>
                                                                @endfor
                                                            </ul>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Location:</td>
                                                        <td>
                                                            <ul class="review-star d-flex flex-row  ml-2">
                                                                @for ($i = 0; $i < $val->location; $i++)
                                                                    <li><i class="fa fa-star"></i></li>
                                                                @endfor
                                                                @for ($i = 0; $i < 5-$val->location; $i++)
                                                                    <li><i class="far fa-star"></i></li>
                                                                @endfor
                                                            </ul>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Service:</td>
                                                        <td>
                                                            <ul class="review-star d-flex flex-row  ml-2">
                                                                @for ($i = 0; $i < $val->service; $i++)
                                                                    <li><i class="fa fa-star"></i></li>
                                                                @endfor
                                                                @for ($i = 0; $i < 5-$val->service; $i++)
                                                                    <li><i class="far fa-star"></i></li>
                                                                @endfor
                                                            </ul>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Clearness:</td>
                                                        <td>
                                                            <ul class="review-star d-flex flex-row  ml-2">
                                                                @for ($i = 0; $i < $val->clearness; $i++)
                                                                    <li><i class="fa fa-star"></i></li>
                                                                @endfor
                                                                @for ($i = 0; $i < 5-$val->clearness; $i++)
                                                                    <li><i class="far fa-star"></i></li>
                                                                @endfor
                                                            </ul>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Rooms:</td>
                                                        <td>
                                                            <ul class="review-star d-flex flex-row  ml-2">
                                                                @for ($i = 0; $i < $val->room; $i++)
                                                                    <li><i class="fa fa-star"></i></li>
                                                                @endfor
                                                                @for ($i = 0; $i < 5-$val->room; $i++)
                                                                    <li><i class="far fa-star"></i></li>
                                                                @endfor
                                                            </ul>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                            <td>
                                                <a href="">{{ optional($val->reviewSpace)->name }}</a>
                                            </td>
                                            <td class="td-status">
                                                {{ showStatusSpace($val->status) }}
                                            </td>
                                            <td>
                                                {{ $val->created_at->format('Y-m-d ') }}
                                            </td>
                                            <td>
                                                <a href="" class="action-icon"> <i class="mdi mdi-square-edit-outline"></i></a>
                                                <a data-id="" type="button" href="" class="action-icon delete"> <i class="mdi mdi-delete"></i></a>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div> <!-- end card-body-->
                </div> <!-- end card-->
            </div> <!-- end col -->
        </div>
    </div>
@stop
@section('script-bottom')
    <script src="{{ URL::asset('assets/libs/toastr/toastr.min.js')}}"></script>
    <script type="text/javascript">
        @if(session('success'))
        toastr.success('{{ session('success') }}', 'Thông báo', {timeOut: 5000});
        @endif

        $(document).ready(function () {
            
            
            $(document).on("click", "#customCheckAll", function () {
                if($(this).is(':checked',true))
                {
                    $(".checkbox").prop('checked', true);
                } else {
                    $(".checkbox").prop('checked',false);
                }
            });
            
            $(document).on("click", ".status-change", function () {
                let value_id = [];
                let status = $('.status').val();
                // console.log(status);
                $('.checkbox:checked').each(function () {
                    let _this = $(this);
                    value_id.push(_this.data('value'));
                    // console.log(value_id);
                });
                console.log(value_id);
                if (value_id.length == 0){
                    toastr.error("Error", "Bạn cần chọn hàng để thay đổi trạng thái", "error", 1000);

                }
                else {
                    if (status >= 0){
                        $.ajax({
                            url : '{{ route('spaces.review.change_status') }}',
                            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                            dataType : 'json',
                            type : 'post',
                            data: {
                                ids : value_id,
                                status : status,
                            },
                            success: function (result) {
                                if (result.type == 1){
                                    toastr.success(result.success, 'Thông báo', {timeOut: 1500});
                                    $('.checkbox:checked').each(function () {
                                        $(this).parents("tr").children('.td-status').html(result.status);

                                    });
                                }

                            }
                        });
                    }
                }
            });
            
        });
        // $(document).ready(function () {
        //     $(document).on("click", ".delete", function () {
        //         alert("Bạn chắc chắn muốn xóa");
        //     });
        //     $(document).on("click", "#customCheckAll", function () {
        //         $('input:checkbox').not(this).prop('checked', this.checked);
        //     });
        //
        //     $(document).on("click", "#delete-checkbox-all", function () {
        //         let value_id = [];
        //         $('.checkbox:checked').each(function () {
        //             value_id.push($(this).data('value'));
        //             console.log(value_id);
        //         });
        //
        //         if (value_id.length > 0) {
        //             $.ajax({
        //                 url : 'spaces/deleteAll',
        //                 headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        //                 dataType : 'json',
        //                 type : 'delete',
        //                 data: {
        //                     ids : value_id,
        //                 },
        //                 success: function (result) {
        //                     alert("Bạn chắc chắn muốn xóa");
        //                     toastr.success(result.success, 'Thông báo', {timeOut: 5000});
        //                     $.each(value_id, function (key, value) {
        //                         console.log(value);
        //                         $('#tr-'+value).hide();
        //                     })
        //                 }
        //             });
        //         } else {
        //             toastr.error("Error", "Bạn cần chọn hàng cần xóa", "error", 1000);
        //         };
        //     });
        //
        // });
    </script>
    <!-- Init js -->
    <script src="{{ URL::asset('assets/libs/bootstrap-table/bootstrap-table.min.js')}}"></script>
    <!-- Bootstrap Tables js -->
    <script src="{{ URL::asset('assets/js/pages/bootstrap-tables.init.js')}}"></script>
    <script src="{{ URL::asset('assets/libs/select2/select2.min.js') }}"></script>
@endsection




