@extends('base::layouts.master')
@section('css')
    <link href="{{ URL::asset('assets/libs/bootstrap-table/bootstrap-table.min.css')}}" rel="stylesheet"
          type="text/css"/>
    <link href="{{ URL::asset('assets/libs/toastr/toastr.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ URL::asset('assets/libs/sweetalert2/sweetalert2.min.css')}}" rel="stylesheet" type="text/css" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection
@section('content')
    <!-- Start Content-->
    <div class="container-fluid">
        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">UBold</a></li>
                            <li class="breadcrumb-item"><a href="javascript: void(0);">News</a></li>
                            <li class="breadcrumb-item active">All</li>
                        </ol>
                    </div>
                    <h4 class="page-title">All Spaces</h4>
                </div>
            </div>
        </div>
    <!-- end page title -->

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row mb-2">
                            <div class="col-md-6">
                                    <div class="form-group mb-2">
                                        <label for="inputPassword2" class="sr-only">Search</label>
                                        <input type="search" class="form-control" name='search' id='search' placeholder="Search name space">
                                    </div>
                            </div>
                            <div class="col-md-6">
                                <div class="text-md-right">
                                    <a href="{{ route('spaces.create') }}" style="color: #fff">
                                        <button type="button" class="btn w-sm btn-success waves-effect waves-light mb-2 mr-2">Add  Spaces</button>
                                    </a>
                                    <button type="button" id="delete-checkbox-all" class="delete-all btn btn-danger waves-effect waves-light mb-2 mr-2">Delete All</button>
                                </div>
                            </div><!-- end col-->
                        </div>

                        <div class="table-responsive">
                            <table class="table table-centered table-borderless table-hover mb-0">
                                <thead class="thead-light">
                                <tr>
                                    <th style="width: 20px;">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="customCheckAll">
                                            <label class="custom-control-label" for="customCheckAll">&nbsp;</label>
                                        </div>
                                    </th>
                                    <th>STT</th>
                                    <th>Name</th>
                                    <th>Location</th>
                                    <th>Author</th>
                                    <th>Status</th>
                                    <th>Reviews</th>
                                    <th>Date</th>
                                    <th style="width: 82px;">Action</th>
                                    <th>Availability</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php
                                    $stt = 1;
                                    $start = ($spaces->currentPage()-1)*$spaces->perPage() +1;
                                @endphp
                                @foreach($spaces as $key => $val)
                                    <tr id="tr-{{ $val->id }}">
                                        <td>
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="checkbox custom-control-input" id="customCheck-{{ $val->id }}" data-value="{{ $val->id }}">
                                                <label class="custom-control-label" for="customCheck-{{ $val->id }}">&nbsp;</label>
                                            </div>
                                        </td>
                                        <td>{{ $start ++}}</td>
                                        <td>
                                            {{ $val->name }}
                                        </td>
                                        <td>
                                            {{ optional($val->locationSpace)->name }}
                                        </td>
                                        <td>
                                            {{ optional($val->user)->full_name }}
                                        </td>

                                        <td class="td-status">
                                            @if($val->status == 1)
                                            <a href="" style="display: inline-block" data-url="{{ route('spaces.changeStatus', $val->id) }}" data-id="{{ $val->id }}" class="status-space badge badge-success">{{ "Publish" }}</a>
                                            @else
                                            <a href="" style="display: inline-block" data-url="{{ route('spaces.changeStatus', $val->id) }}" data-id="{{ $val->id }}" class="status-space badge badge-dark">{{ "Draft" }}</a>
                                            @endif
                                        </td>
                                        <td>
                                            {{ count($val->review) }}
                                        </td>
                                        <td>
                                            {{ $val->created_at->format('d-m-Y') }}
                                        </td>
                                        <td>
                                            <a href="{{ route('spaces.edit', $val->id) }}" class="action-icon"> <i class="mdi mdi-square-edit-outline"></i></a>
                                            <a data-id="" type="button" href="{{ route('spaces.delete', $val->id) }}" class="action-icon delete"> <i class="mdi mdi-delete"></i></a>
                                        </td>
                                        <td>
                                        <a href="{{ route('spaces.availability.list', $val->id) }}" class="btn btn-success">Availability</a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <div class="mt-2 text-center">
                                {{ $spaces->render() }}
                            </div>
                        </div>
                    </div> <!-- end card-body-->

                </div> <!-- end card-->
            </div> <!-- end col -->
        </div>
    </div>
@stop
@section('script-bottom')
    <script src="{{ URL::asset('assets/libs/toastr/toastr.min.js')}}"></script>
    <link href="{{ URL::asset('assets/libs/sweetalert2/sweetalert2.min.js')}}" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        @if(session('success'))
        toastr.success('{{ session('success') }}', 'Thông báo', {timeOut: 5000});
        @endif

        $(document).ready(function () {

            $(document).on('keyup','#search',function(){
                var query = $(this).val();
                search_space(query);
            });
            function search_space(query = ' '){
                $.ajax({
                    type: "GET",
                    url: "{{ route('space.searchspace') }}",
                    data: {query:query},
                    success: function (data) {
                        $('tbody').html(data)
                    }
                });
            }

            $(document).on("click", ".delete", function () {
                confirm('Bạn chắc chắn muốn xoá?');
            });

            $(document).on("click", "#customCheckAll", function () {
                $('input:checkbox').not(this).prop('checked', this.checked);
            });

            $(document).on("click", "#delete-checkbox-all", function () {
                let value_id = [];
                $('.checkbox:checked').each(function () {
                    value_id.push($(this).data('value'));
                    console.log(value_id);
                });

                if (value_id.length > 0) {
                    $.ajax({
                        url : 'spaces/deleteAll',
                        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                        dataType : 'json',
                        type : 'delete',
                        data: {
                            ids : value_id,
                        },
                        success: function (result) {
                            confirm('Bạn chắc chắn muốn xoá?');
                            toastr.success(result.success, 'Thông báo', {timeOut: 5000});
                            $.each(value_id, function (key, value) {
                                console.log(value);
                                $('#tr-'+value).remove();
                            })
                        }
                    });
                } else {
                    toastr.error("Error", "Bạn cần chọn hàng cần xóa", "error", 1000);
                };
            });


            $(document).on("click", ".status-space", function (e) {
                e.preventDefault();
                let _this = $(this);
                let id = _this.data('id');
                let _url = _this.attr('data-url');
                $.ajax({
                    url : _url,
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    dataType : 'json',
                    data : {
                        id : id,
                    },
                    type : 'post',
                    success : function (result){
                        if(result.status == 0){
                            _this.html('Draft');
                            _this.removeClass('badge-success');
                            _this.addClass('badge-dark');
                            toastr.success(result.success, 'Thông báo', {timeOut: 5000});
                            toastr.clear()
                        }
                        else{
                            _this.html('Publish');
                            _this.removeClass('badge-dark');
                            _this.addClass('badge-success');
                            toastr.success(result.success, 'Thông báo', {timeOut: 5000});
                            toastr.clear()
                        }
                    }

                });
            });



        });
    </script>
    <!-- Init js -->
    <script src="{{ URL::asset('assets/libs/bootstrap-table/bootstrap-table.min.js')}}"></script>
    <!-- Bootstrap Tables js -->
    <script src="{{ URL::asset('assets/js/pages/bootstrap-tables.init.js')}}"></script>
@endsection




