@php
    $stt = 1;
    $start = ($spaces->currentPage()-1)*$spaces->perPage() +1;
@endphp
@foreach($spaces as $key => $val)
<tr id="tr-{{ $val->id }}">
    <td>
        <div class="custom-control custom-checkbox">
            <input type="checkbox" class="checkbox custom-control-input" id="customCheck-{{ $val->id }}" data-value="{{ $val->id }}">
            <label class="custom-control-label" for="customCheck-{{ $val->id }}">&nbsp;</label>
        </div>
    </td>
    <td>{{ $start ++}}</td>
    <td>
        {{ $val->name }}
    </td>
    <td>
        {{ optional($val->locationSpace)->name }}
    </td>
    <td>
        {{ optional($val->user)->full_name }}
    </td>

    <td class="td-status">
        @if($val->status == 1)
        <a href="" style="display: inline-block" data-url="{{ route('spaces.changeStatus', $val->id) }}" data-id="{{ $val->id }}" class="status-space badge badge-success">{{ "Publish" }}</a>
        @else
        <a href="" style="display: inline-block" data-url="{{ route('spaces.changeStatus', $val->id) }}" data-id="{{ $val->id }}" class="status-space badge badge-dark">{{ "Draft" }}</a>
        @endif
    </td>
    <td>
        {{ count($val->review) }}
    </td>
    <td>
        {{ $val->created_at->format('d-m-Y') }}
    </td>
    <td>
        <a href="{{ route('spaces.edit', $val->id) }}" class="action-icon"> <i class="mdi mdi-square-edit-outline"></i></a>
        <a data-id="" type="button" href="{{ route('spaces.delete', $val->id) }}" class="action-icon delete"> <i class="mdi mdi-delete"></i></a>
    </td>
    <td>
    <a href="{{ route('spaces.availability.list', $val->id) }}" class="btn btn-success">Availability</a>
    </td>
</tr>
@endforeach
