@extends('base::layouts.master')
@section('css')
    <!-- Plugins css-->
    <link href="{{ URL::asset('assets/libs/select2/select2.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ URL::asset('assets/libs/quill/quill.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{ URL::asset('assets/libs/dropzone/dropzone.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{ URL::asset('assets/libs/dropify/dropify.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{ URL::asset('assets/css/webt.css')}}" rel="stylesheet" type="text/css"/>
    {{--<link href="{{ URL::asset('assets/libs/summernote/summernote.min.css')}}" rel="stylesheet" type="text/css" />--}}
    <style>
        .error-text{
            color: red;
        }
        .star{
            color: red;
        }
    </style>
@endsection
@section('content')
    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">UBold</a></li>
                            <li class="breadcrumb-item"><a href="javascript: void(0);">eCommerce</a></li>
                            <li class="breadcrumb-item active">Product Edit</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Edit Space</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->
        <form action="" method="post">
            @csrf
            <div class="row">
                <div class="col-lg-9">
                    <div class="card-box">
                        <h5 class="mt-0 mb-3 bg-light p-2">Space Content</h5>
                        <div class="form-group mb-3">
                            <label for="">Name <span class="star">*</span></label>
                            <input type="text" id="" value="{{ $spaces->name }}" class="form-control" placeholder="Name of the space" name="name">
                            @if($errors->has('name'))
                                <div class="error-text">
                                    {{$errors->first('name')}}
                                </div>
                            @endif
                        </div>

                        <div class="form-group mb-3">
                            <label for="">Content <span class="star">*</span></label>
                            <textarea class="form-control" id="space-content" rows="5" name="content" placeholder="">{{ $spaces->content }}</textarea>
                            @if($errors->has('content'))
                                <div class="error-text">
                                    {{$errors->first('content')}}
                                </div>
                            @endif
                        </div>

                        {{-- <div class="form-group mb-3">
                            <label for="">Youtube Video</label>
                            <input type="text" id=""  value="{{ $spaces->video }}" class="form-control" placeholder="Youtube link video" name="video">
                        </div> --}}

                        <div class="form-group form-group-item mb-3">
                            <label class="control-label">FAQs</label>
                            <div class="g-items-header">
                                <div class="row">
                                    <div class="col-md-5">Title</div>
                                    <div class="col-md-5">Content</div>
                                    <div class="col-md-1"></div>
                                </div>
                            </div>
                            <div class="g-items g-items-faq">
                                @php
                                    $i = 0;
                                @endphp
                                @if(isset($spaces->faq))
                                    @foreach($spaces->faq as $key => $val)
                                        <div class="item" data-number="{{ $i+1 }}">
                                            <div class="row">
                                                <div class="col-md-5">
                                                    <input type="text" name="faqs[{{ $i+1 }}][title]" class="form-control" placeholder="Eg: Can I bring my pet?" value="{{ $val->title }}">
                                                </div>
                                                <div class="col-md-6">
                                                    <textarea name="faqs[{{ $i+1 }}][content]" class="form-control" placeholder="">{{ $val->content }}</textarea>
                                                </div>
                                                <div class="col-md-1">
                                                    <span class="btn btn-danger btn-sm btn-remove-item"><i class="fa fa-trash"></i></span>
                                                </div>
                                            </div>
                                        </div>
                                    @php
                                        $i++;
                                    @endphp
                                    @endforeach
                                @endif


                            </div>
                            <div class="text-right">
                                <span class="btn btn-info btn-sm btn-add-item btn-add-item-faq"><i class="fe-plus-circle"></i> Add item</span>
                            </div>
                        </div>

                        <div class="form-group mb-3">
                            <label for="banner">Banner Image <span class="star">*</span></label>
                            <div style="" class="col-md-6">
                                <div class="input-group-btn">
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bd-example-modal-banner">Upload image</button>
                                        <input type="hidden" name="banner" id="banner" value="{{ $spaces->banner }}">
                                    </span>
                                </div>
                                <div id="preview_banner" class="mt-3">
                                    @if(!empty($spaces->banner))
                                    <div class="box_imgg position-relative">
                                        <img src="{{ config('app.url') }}{{ $spaces->banner }}" class="show-img" id='show-img-banner' style="width:100%;">
                                        <i class="mdi mdi-close-circle-outline style_icon_remove style_icon_remove_banner" title="delete"></i>
                                    </div>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="form-group mb-3">
                            <label for="album">Gallery <span class="star">*</span></label>
                                <div style="padding: 0 12px" class="">
                                    @php
                                        $str_album_value = '';
                                    @endphp
                                    @if(!empty($spaces->gallery))
                                        @php
                                            $arr_album_selected = json_decode($spaces->gallery, true);
                                        @endphp
                                        @foreach($arr_album_selected as $key => $val)
                                            @php
                                                $arr_album_value[] = config('app.url').$val;
                                            @endphp
                                        @endforeach
                                        @php
                                            $str_album_value .= '["';
                                            $str_album_value .= implode ('","', $arr_album_value);
                                            $str_album_value .='"]';
                                        @endphp
                                    @endif
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bd-example-modal-album">Select album</button>
                                        <input type="hidden" name="album" id="album" value="{{ $str_album_value }}">
                                        @if($errors->has('album'))
                                            <div class="error-text">
                                                {{$errors->first('album')}}
                                            </div>
                                        @endif
                                    </span>

                                    <div id="preview_album" class="mt-3">
                                        <div class="row row_preview_album" id="row_preview_album">
                                            @if(!empty($spaces->gallery))
                                                @foreach ($arr_album_selected as $key => $val)
                                                    <div class="col-3">
                                                        <div class="box_imgg position-relative">
                                                            <img src="{{ config('app.url') }}{{ $val }}" class="show-img img-height-110" style="width:100%; height=110px;">
                                                            <i class="fas fa-times-circle style_icon_remove style_icons_remove_album" title="delete"></i>
                                                        </div>
                                                    </div>
                                                @endforeach
                                            @endif
                                        </div>
                                    </div>
                                </div>
                        </div>

                        <div class="form-group mb-3">
                            <h5 class="mt-0 mb-3 bg-light p-2">Extra Info</h5>
                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="form-group mb-3">
                                        <label for="">No. Bed <span class="star">*</span></label>
                                        <input min="0" type="number" id="" name="bedroom" class="form-control"
                                               placeholder="Example: 3" value="{{ $spaces->number_bed }}">
                                        <p class="help is-danger">{{ $errors->first('bedroom') }}</p>
                                    </div>
                                </div>

                                <div class="col-lg-4">
                                    <div class="form-group mb-3">
                                        <label for="">No. Bathroom <span class="star">*</span></label>
                                        <input min="0" type="number" id="" name="bathroom" class="form-control"
                                               placeholder="Example: 5" value="{{ $spaces->number_bathroom }}">
                                        <p class="help is-danger">{{ $errors->first('bathroom') }}</p>
                                    </div>
                                </div>

                                <div class="col-lg-4">
                                    <div class="form-group mb-3">
                                        <label for="">Square <span class="star">*</span></label>
                                        <input min="0" type="number" id="" name="square" class="form-control"
                                               placeholder="Example: 100" value="{{ $spaces->size }}">
                                        <p class="help is-danger">{{ $errors->first('square') }}</p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group mb-3">
                            <h5 class="mt-0 mb-3 bg-light p-2">Locations <span class="star">*</span></h5>
                            <div class="form-group mb-3">
                                <select type="text" id="" name="location_id" class="form-control">
                                    @if(isset($spaces->locationSpace->id))
                                    <option value="{{ $spaces->locationSpace->id }}">{{ $spaces->locationSpace->name }}</option>
                                    @foreach($locations as $key => $val)
                                        @if($val->id != $spaces->location_id)
                                        <option value="{{ $val->id }}">{{ $val->name }}</option>
                                        @endif
                                    @endforeach
                                    @endif

                                    @if($errors->has('location_id'))
                                        <div class="error-text">
                                            {{$errors->first('location_id')}}
                                        </div>
                                    @endif
                                </select>
                            </div>
                            <div class="form-group mb-3">
                                <label for="">Real address <span class="star">*</span></label>
                                <input name="address" value="{{ $spaces->address }}" type="text" class="form-control" id="" placeholder="Real address">
                                @if($errors->has('address'))
                                    <div class="error-text">
                                        {{$errors->first('address')}}
                                    </div>
                                @endif
                            </div>

                            <div class="form-group mb-3">
                                <label for="">The geographic coordinate</label>
                                <div class="row">
                                    <div class="col-lg-9">
                                        <div class="form-group mb-3">
                                            <div id="gmaps-basic" class="gmaps"></div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group mb-3">
                                            <div class="form-group mb-3">
                                                <label for="map_lat">Map Latitude: <span class="star">*</span></label>
                                                <input type="text" id="map_lat" name="map_lat" class="form-control"
                                                       placeholder="" value="{{ $spaces->latitu }}">
                                                <p class="help is-danger">{{ $errors->first('map_lat') }}</p>
                                            </div>
                                            <div class="form-group mb-3">
                                                <label for="map_lng">Map Longitude: <span class="star">*</span></label>
                                                <input type="text" id="map_lng" name="map_lng" class="form-control"
                                                       placeholder="" value="{{ $spaces->longtitu }}">
                                            <p class="help is-danger">{{ $errors->first('map_lng') }}</p>
                                            </div>
                                            <div class="form-group mb-3">
                                                <label for="map_zoom">Map Zoom:</label>
                                                <input type="text" id="map_zoom" name="map_zoom" class="form-control"
                                                       placeholder="" value="{{ $spaces->mapzoom }}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group mb-3">
                            <h5 class="mt-0 mb-3 bg-light p-2">Pricing</h5>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group mb-3">
                                        <label for="">Price <span class="star">*</span></label>
                                        <input min="0" type="number" id="" name="price" class="form-control"
                                               placeholder="Space Price" value="{{ $spaces->price }}">
                                        @if($errors->has('price'))
                                            <div class="error-text">
                                                {{$errors->first('price')}}
                                            </div>
                                        @endif
                                    </div>
                                    <div class="form-group mb-3">
                                        <label for="">Max Guests <span class="star">*</span></label>
                                        <input min="0" type="number" id="" name="max_guests" class="form-control"
                                               placeholder="" value="{{ $spaces->max_guests }}">
                                        @if($errors->has('max_guests'))
                                            <div class="error-text">
                                                {{$errors->first('max_guests')}}
                                            </div>
                                        @endif
                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <div class="form-group mb-3">
                                        <label for="">Sale Price</label>
                                        <input min="0" type="number" id="" name="sale_price" class="form-control"
                                               placeholder="Space Sale Price" value="{{ empty($spaces->sale_price) ? old('sale_price') : $spaces->sale_price }}">
                                        <div class="error-text">
                                            {{$errors->first('sale_price')}}
                                        </div>
                                        <span><i>If the regular price is less than the discount , it will show the regular price</i></span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group form-group-item mb-3">
                                <label class="control-label">Extra Price</label>
                                <div class="g-items-header">
                                    <div class="row">
                                        <div class="col-md-5">Title</div>
                                        <div class="col-md-3">Price</div>
                                        <div class="col-md-3">Type</div>
                                        <div class="col-md-1"></div>
                                    </div>
                                </div>
                                <div class="g-items g-items-extra-price">
                                    @php
                                        $j = 0;
                                    @endphp
                                    @if(isset($spaces->extra_price))
                                        @foreach($spaces->extra_price as $key => $val)
                                        <div class="item" data-number="{{ $j+1 }}">
                                            <div class="row">
                                                <div class="col-md-5">
                                                    <input type="text" name="extra_price[{{ $j+1 }}][title]" class="form-control" value="{{ $val->name }}">
                                                </div>
                                                <div class="col-md-3">
                                                    <input type="number" min="0" name="extra_price[{{ $j+1 }}][price]" class="form-control" value="{{ $val->price }}">
                                                </div>
                                                <div class="col-md-3">
                                                    <select name="extra_price[{{ $j+1 }}][type]" class="form-control">
                                                        @if($val->type == 0)
                                                            <option value="0">One-time</option>
                                                            <option value="1">Per hour</option>
                                                            <option value="2">Per day</option>
                                                        @elseif($val->type == 1)
                                                            <option value="1">Per hour</option>
                                                            <option value="2">Per day</option>
                                                            <option value="0">One-time</option>
                                                        @else
                                                            <option value="2">Per day</option>
                                                            <option value="0">One-time</option>
                                                            <option value="1">Per hour</option>
                                                        @endif
                                                    </select>
                                                </div>
                                                <div class="col-md-1">
                                                    <span class="btn btn-danger btn-sm btn-remove-item"><i class="fa fa-trash"></i></span>
                                                </div>
                                            </div>
                                        </div>
                                        @php
                                            $j++;
                                        @endphp
                                        @endforeach
                                    @endif

                                    @php
                                    $j = 0;
                                @endphp
                                @if (!empty(old('extra_price')))
                                    @foreach (old('extra_price') as $item)
                                    <div class="item" data-number="{{ $j }}">
                                        <div class="row">
                                            <div class="col-md-5">
                                                <input type="text" name="extra_price[{{ $j }}][title]" class="form-control" placeholder="Extra price name" value="{{ $item['title'] }}">
                                            </div>
                                            <div class="col-md-3">
                                                <input type="number" min="0" name="extra_price[{{ $j }}][price]" class="form-control" value="{{ $item['price'] }}">
                                            </div>
                                            <div class="col-md-3">
                                                <select name="extra_price[{{ $j }}][type]" class="form-control">
                                                    <option value="0" {{ $item['type'] == 0 ? 'selected' : ' ' }}>One-time</option>
                                                    <option value="1" {{ $item['type'] == 1 ? 'selected' : ' ' }}>Per hour</option>
                                                    <option value="2" {{ $item['type'] == 2 ? 'selected' : ' ' }}>Per day</option>
                                                </select>
                                            </div>
                                            <div class="col-md-1">
                                                <span class="btn btn-danger btn-sm btn-remove-item"><i class="fa fa-trash"></i></span>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                @php
                                        $i++;
                                @endphp
                                @endif
                                </div>
                                <div class="text-right">
                                    <span class="btn btn-info btn-sm btn-add-item btn-add-item-extra-price"><i class="fe-plus-circle"></i> Add item</span>
                                </div>
                            </div>
                        </div>

                        <div class="form-group mb-3">
                            <h5 class="mt-0 mb-3 bg-light p-2">Seo Manager</h5>
                            <div class="form-group mb-3">
                                <label for="product-meta-title">Seo title</label>
                                <input name="seo_title" value="{{ $spaces->seo_title }}" type="text" class="form-control" id="product-meta-title" placeholder="Enter title">
                            </div>
                            <div class="form-group mb-3">
                                <label for="product-meta-description">Seo Description</label>
                                <textarea name="seo_description" class="form-control" rows="5" id="product-meta-description" placeholder="Enter description">{{ $spaces->seo_description }}</textarea>
                            </div>

                            {{--<div class="form-group mb-3">--}}
                            {{--<div style="padding: 0;" class="col-md-6">--}}
                            {{--<label for="product-meta-description">Featured Image</label>--}}
                            {{--<div class="form-group mb-3">--}}
                            {{--<input type="file" name="" class="dropify" data-height=""/>--}}
                            {{--</div>--}}
                            {{--</div>--}}
                            {{--</div>--}}
                        </div>
                    </div>
                </div>

                <div class="col-lg-3">
                    <div class="card-box">
                        <div class="form-group mb-3">
                            <h5 class="mt-0 mb-3 bg-light p-2">Publish</h5>
                            <select class="form-control" id="" name="status">
                                @if($spaces->status == 0)
                                    <option value="0">Draft</option>
                                    <option value="1">Publish</option>
                                @else
                                    <option value="1">Publish</option>
                                    <option value="0">Draft</option>
                                @endif

                            </select>
                        </div>
                    </div>

                    <div class="card-box">
                        <div class="form-group mb-3">
                            <h5 class="mt-0 mb-3 bg-light p-2">Author Setting</h5>
                            <select type="text" id="" name="vendor_id" class="form-control">
                                @if(isset($spaces->user->id))
                                <option value="{{ $spaces->user->id }}">{{ $spaces->user->firstname. ' '. $spaces->user->lastname }}</option>
                                @foreach($users as $key => $val)
                                    @if($val->id != $spaces->vendor_id)
                                    <option value="{{ $val->id }}">{{ $val->firstname. ' '. $val->lastname }}</option>
                                    @endif
                                @endforeach
                                @endif
                            </select>
                        </div>
                    </div>

                    <div class="card-box">
                        <div class="form-group mb-3">
                            <h5 class="mt-0 mb-3 bg-light p-2">Availability</h5>
                            <div class="form-group mb-0">
                                <label for="">Space Featured</label>
                            </div>
                            <div class="form-group mb-0">
                                <input type="checkbox" id="" name="featured" value="1" {{ ($spaces->featured == 1) ? 'checked' : ' ' }}>
                                <label for="">Enable featured</label>
                            </div>
                        </div>

                        <div class="form-group mb-3">
                            <div class="form-group mb-0">
                                <label for="">Default State</label>
                                <select type="text" id="" name="state" class="custom-select">
                                    @if($spaces->state == 1)
                                    <option value="1">Always available</option>
                                    <option value="2">Only available on specific dates</option>
                                    @else
                                    <option value="2">Only available on specific dates</option>
                                    <option value="1">Always available</option>
                                    @endif
                                </select>
                            </div>
                        </div>
                    </div>

                    {{-- @foreach ($spaceAttribute as $item)
                    <div class="card-box">
                        <div class="form-group mb-3">
                            <h5 class="mt-0 mb-3 bg-light p-2">Attribute: {{ $item->name }}</h5>
                            <div class="form-group mb-0">
                                @foreach ($spaceType as $key => $val)
                                    @if ($item->id == $key)
                                        @foreach($val as $key => $val)
                                            @if(in_array($val->id, $space_type))
                                            <label>
                                                <input type="checkbox" id="" name="spacetype[]" value="{{ $val->id }}" checked>{{ $val->name }}
                                            </label><br>
                                            @else
                                            <label >
                                                <input type="checkbox" id="" name="spacetype[]" value="{{ $val->id }}">{{ $val->name }}
                                            </label><br>
                                            @endif
                                        @endforeach
                                    @endif
                                @endforeach
                            </div>
                        </div>
                    </div>
                    @endforeach --}}

                    <div class="card-box">
                        <div class="form-group mb-3">
                            <h5 class="mt-0 mb-3 bg-light p-2">Attribute: Car Type</h5>
                            <div class="form-group mb-0">
                                @foreach ($spaceType as $key => $val)
                                    @if(in_array($val->id, $space_type))
                                        <input type="checkbox" id="spaceType-{{ $val->id }}" name="spacetype[]" value="{{ $val->id }}"
                                                checked
                                        >
                                        <label for="spaceType-{{ $val->id }}">{{ $val->name }}</label><br>
                                    @else
                                    <input type="checkbox" id="spaceType-{{ $val->id }}" name="spacetype[]" value="{{ $val->id }}">
                                        <label for="spaceType-{{ $val->id }}">{{ $val->name }}</label><br>
                                    @endif
                                @endforeach
                            </div>
                        </div>
                    </div>

                    <div class="card-box">
                        <div class="form-group mb-3">
                            <h5 class="mt-0 mb-3 bg-light p-2">Attribute: Car Amenity</h5>
                            <div class="form-group mb-0">
                                @foreach ($spaceAmenity as $key => $val)
                                @if(in_array($val->id, $space_amenity))
                                    <input type="checkbox" id="spaceAmenity-{{ $val->id }}" name="spaceAmenity[]" value="{{ $val->id }}"
                                            checked
                                    >
                                    <label for="spaceAmenity-{{ $val->id }}">{{ $val->name }}</label><br>
                                @else
                                <input type="checkbox" id="spaceAmenity-{{ $val->id }}" name="spaceAmenity[]" value="{{ $val->id }}">
                                    <label for="spaceAmenity-{{ $val->id }}">{{ $val->name }}</label><br>
                                @endif
                            @endforeach
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="text-center mb-3">
                        <button type="submit" class="btn w-sm btn-success waves-effect waves-light">Save</button>
                        <a href="{{ route('spaces.list') }}"><button type="button" class="btn w-sm btn-danger waves-effect waves-light">Cancel</button></a>
                </div>
            </div>
        </form>
    </div>

@section('js')
    <div class="modal fade bd-example-modal-image" id="modal-file" tabindex="-1" role="dialog" data-backdrop="false" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Manger Image</h5>
                    <button type="button" class="close" data-dismiss="modal"><i class="fas fa-times"></i></button>
                </div>
                <div class="modal-body">
                    <iframe src="{{ config('app.url') }}/filemanager/dialog.php?akey=4Ans6pfTs6A9DzHj8AZq1NVGKoYxpyoI8ivCN1bOow&field_id=avatar&multiple=0" frameborder="0" style="width: 100%; height: 500px; overflow-y: auto"></iframe>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade bd-example-modal-banner" id="modal-file-banner" tabindex="-1" role="dialog" data-backdrop="false" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Manger Image</h5>
                    <button type="button" class="close" data-dismiss="modal"><i class="fas fa-times"></i></button>
                </div>
                <div class="modal-body">
                    <iframe src="{{ config('app.url') }}/filemanager/dialog.php?akey=4Ans6pfTs6A9DzHj8AZq1NVGKoYxpyoI8ivCN1bOow&field_id=banner&multiple=0" frameborder="0" style="width: 100%; height: 500px; overflow-y: auto"></iframe>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade bd-example-modal-album" id="modal-file" tabindex="-1" role="dialog" data-backdrop="false" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Manger Image</h5>
                    <button type="button" class="close" data-dismiss="modal"><i class="fas fa-times"></i></button>
                </div>
                <div class="modal-body">
                    <iframe src="{{ config('app.url') }}/filemanager/dialog.php?akey=4Ans6pfTs6A9DzHj8AZq1NVGKoYxpyoI8ivCN1bOow&field_id=album&multiple=1" frameborder="0" style="width: 100%; height: 500px; overflow-y: auto"></iframe>
                </div>
            </div>
        </div>
    </div>
@stop

@stop
@section('script')

    <script src="{{ URL::asset('assets/libs/dropzone/dropzone.min.js')}}"></script>
    <script src="{{ URL::asset('assets/libs/dropify/dropify.min.js')}}"></script>
    {{--<script src="{{ URL::asset('assets/libs/summernote/summernote.min.js') }}"></script>--}}

    <!-- Init js-->
    <script src="{{ URL::asset('assets/js/pages/form-fileuploads.init.js')}}"></script>
    <!-- Select2 js-->
    <script src="{{ URL::asset('assets/libs/toastr/toastr.min.js')}}"></script>
    <!-- Bootstrap Tables js -->
    <script src="{{ URL::asset('assets/js/pages/bootstrap-tables.init.js')}}"></script>
    <!-- Dropzone file uploads-->

    <script src="https://maps.google.com/maps/api/js?key=AIzaSyDsucrEdmswqYrw0f6ej3bf4M4suDeRgNA"></script>
    <script src="{{ URL::asset('assets/libs/gmaps/gmaps.min.js') }}"></script>
    <script src="{{ URL::asset('assets/js/pages/google-maps.init.js') }}"></script>
    <script src="{{ URL::asset('assets/js/map-engine.js') }}"></script>

    <script src="{{ URL::asset('assets/js/pages/add-product.init.js')}}"></script>
    <script src="{{ URL::asset('ckeditor/ckeditor.js')}}"></script>
    <script type="text/javascript" src="js/ckeditor/ckeditor.js"></script>

    <script>
        var bookingCore = {
            url: 'http://sandbox.bookingcore.org',
            map_provider: 'gmap',
            map_gmap_key: '',
            csrf: 'JMrM5NwcxQy6HWOmuo7LQ2kHPh7pGwfbOPpXxkue'
        };
        var i18n = {
            warning: "Warning",
            success: "Success",
            confirm_delete: "Do you want to delete?",
            confirm: "Confirm",
            cancel: "Cancel",
        };
    </script>
    <script>
        // jQuery(document).ready(function(){
        //     $.ajaxSetup({
        //         headers: {
        //             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        //         }
        //     });
        //     // Summernote
        //     $('#space-content').summernote({
        //         height: 180,                 // set editor height
        //         minHeight: null,             // set minimum height of editor
        //         maxHeight: null,             // set maximum height of editor
        //         focus: false                 // set focus to editable area after initializing summernote
        //     });
        //
        //     // Select2
        //     $('.select2').select2();
        //
        //
        // });
        // var options = {
        //     filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
        //     filebrowserImageUploadUrl: '/laravel-filemanager/upload?type=Images&_token=',
        //     filebrowserBrowseUrl: '/laravel-filemanager?type=Files',
        //     filebrowserUploadUrl: '/laravel-filemanager/upload?type=Files&_token='
        // };
        // CKEDITOR.replace('space-content');


        CKEDITOR.replace( 'space-content' ,{
            filebrowserBrowseUrl : 'filemanager/dialog.php?type=2&editor=ckeditor&fldr=',
            filebrowserUploadUrl : 'filemanager/dialog.php?type=2&editor=ckeditor&fldr=',
            filebrowserImageBrowseUrl : 'filemanager/dialog.php?type=1&editor=ckeditor&fldr='
        });
        // $(document).ready(function () {
        //     $('#article-thumb').filemanager('image', {'prefix': '/laravel-filemanager'});
        // });

        jQuery(function ($) {
            new BravoMapEngine('gmaps-basic', {
                fitBounds: true,
                center: [{{$spaces->latitu ?? "51.505"}}, {{$spaces->longtitu ?? "-0.09"}}],
                zoom: {{ "8"}},
                ready: function (engineMap) {
                    @if($spaces->latitu && $spaces->longtitu)
                    engineMap.addMarker([{{$spaces->latitu}}, {{$spaces->longtitu}}], {
                        icon_options: {}
                    });
                    @endif
                    engineMap.on('click', function (dataLatLng) {
                        engineMap.clearMarkers();
                        engineMap.addMarker(dataLatLng, {
                            icon_options: {}
                        });
                        $("input[name=map_lat]").attr("value", dataLatLng[0]);
                        $("input[name=map_lng]").attr("value", dataLatLng[1]);
                    });
                    engineMap.on('zoom_changed', function (zoom) {
                        $("input[name=map_zoom]").attr("value", zoom);
                    })
                }
            });
        })
    </script>
    <script >
        var i = 0 , j=0;
        $(document).on('click', '.btn-add-item-faq', function() {
            var html = `
                <div class="item" data-number="${i}">
                    <div class="row">
                        <div class="col-md-5">
                            <input type="text" name="faqs[${i}][title]" class="form-control" placeholder="Eg: Can I bring my pet?">
                            </div>
                        <div class="col-md-6">
                            <textarea name="faqs[${i}][content]" class="form-control" placeholder=""></textarea>
                        </div>
                        <div class="col-md-1">
                            <span class="btn btn-danger btn-sm btn-remove-item"><i class="fa fa-trash"></i></span>
                        </div>
                    </div>
                </div>
            `;
            $('.g-items-faq').append(html);
            i++;
        });
        $(document).on('click', '.btn-add-item-extra-price', function() {
            var html = `
                <div class="item" data-number="${j}">
                    <div class="row">
                        <div class="col-md-5">
                            <input type="text" name="extra_price[${j}][title]" class="form-control" placeholder="Extra price name">
                        </div>
                        <div class="col-md-3">
                            <input type="number" min="0" name="extra_price[${j}][price]" class="form-control" value="">
                        </div>
                        <div class="col-md-3">
                            <select name="extra_price[${j}][type]" class="form-control">
                                <option value="0">One-time</option>
                                <option value="1">Per hour</option>
                                <option value="2">Per day</option>
                            </select>
                        </div>
                        <div class="col-md-1">
                            <span class="btn btn-danger btn-sm btn-remove-item"><i class="fa fa-trash"></i></span>
                        </div>
                    </div>
                </div>
            `;
            $('.g-items-extra-price').append(html);
            j++;
        });
        $(document).on('click', '.btn-remove-item', function(){
            $(this).parents('.item').remove();
        });

        $(document).on('hide.bs.modal', '.bd-example-modal-image', function () {
            var _img = $('input#avatar').val();
            if (!_img.length) {
                $('#preview_avatar').empty();
            } else {
                $('#preview_avatar').empty();
                $html = `
            <div class="box_imgg position-relative">
                <img src="" id='show-img-hotel' style="width:100%;">
                <i class="mdi mdi-close-circle-outline style_icon_remove style_icon_remove_avatar" title="delete"></i>
            </div>
        `;
                $('#preview_avatar').append($html);
                $('#show-img-hotel').attr('src', _img);
            }
        });

        $(document).on('click', '.style_icon_remove_avatar', function() {
            $(this).parent('.box_imgg').hide('slow', function () {
                $(this).remove();
                $('#avatar').val('');
            });
        });

        $(document).on('hide.bs.modal', '.bd-example-modal-banner', function () {
            var _img = $('input#banner').val();
            if (!_img.length) {
                $('#preview_banner').empty();
            } else {
                $('#preview_banner').empty();
                $html = `
            <div class="box_imgg position-relative">
                <img src="" id='show-img-banner' class="show-img" style="width:100%;">
                <i class="mdi mdi-close-circle-outline style_icon_remove style_icon_remove_banner" title="delete"></i>
            </div>
        `;
                $('#preview_banner').append($html);
                $('#show-img-banner').attr('src', _img);
            }
        });

        $(document).on('click', '.style_icon_remove_banner', function() {
            $(this).parent('.box_imgg').hide('slow', function () {
                $(this).remove();
                $('#banner').val('');
            });
        });



        $(document).on('hide.bs.modal', '.bd-example-modal-album', function () {
            var _img = $('input#album').val();
            if (!_img.length) {
                $('#preview_album').empty();
            } else {
                if(_img[0] == '[') {
                    var array = JSON.parse(_img);

                    $('#row_preview_album').empty();
                    var html = '';
                    $.each(array, function( index, value ) {
                        html += `
                            <div class="col-3 mt-3">
                                <div class="box_imgg position-relative">
                                    <img src="${value}" class="show-img img-height-110" style="width:100%; height=110px;">
                                    <i class="fas fa-times-circle style_icon_remove style_icons_remove_album" title="delete"></i>
                                </div>
                            </div>
                        `;
                    });
                    $('#row_preview_album').append(html);

                }else{

                    $html = `
                        <div class="col-3 mt-3">
                            <div class="box_imgg position-relative">
                                <img src="" id="show-img-album" class="show-img" style="width:100%;">
                                <i class="fas fa-times-circle style_icon_remove style_icons_remove_album" title="delete"></i>
                            </div>
                        </div>
                    `;
                    $('#row_preview_album').empty();
                    $('#row_preview_album').append($html);
                    $('#show-img-album').attr('src', _img);
                    var str_src = '';
                    str_src += "[\"";
                    str_src += _img;
                    str_src += "\"]";
                    $('#album').val(str_src);
                }

            }
        });

        // $(document).on('click', '.style_icons_remove_album', function() {
        //     $(this).parent('.box_imgg').hide('slow', function () {
        //         $(this).remove();
        //         $('#album').val('');
        //     });
        // });


        $(document).on('click', '.style_icons_remove_album', function() {
            $(this).parents('.col-3').hide('slow', function () {
                $(this).remove();
                var arr_image = [];
                var str_src = '';
                $('.row_preview_album').find('.col-3').each(function () {
                    var str_image = '';
                    str_image += '"';
                    str_image += $(this).find('.img-height-110').attr('src');
                    str_image += '"';
                    arr_image.push(str_image);
                });
                str_src += "[";
                str_src += arr_image.toString();
                str_src += "]";
                if(arr_image.length == 0){
                    $('#album').val('');
                }else{
                    $('#album').val(str_src);
                }
            });
        });
    </script>
@endsection
