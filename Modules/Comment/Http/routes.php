<?php

Route::group(['middleware' => ['web', 'adminLogin'], 'prefix' => 'admin/comment', 'namespace' => 'Modules\Comment\Http\Controllers'], function()
{
    Route::get('/', 'CommentController@index')->name('comment.list');
    Route::post('/change-status/{id}', 'CommentController@changeStatus')->name('comment.change_status');
    Route::get('/edit/{id}', 'CommentController@edit')->name('comment.edit');
    Route::post('/edit/{id}', 'CommentController@update')->name('comment.update');
    Route::get('/delete/{id}', 'CommentController@destroy')->name('comment.delete');
    Route::post('/delete-all', 'CommentController@deleteAll')->name('comment.delete_all');
});
