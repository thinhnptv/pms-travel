<?php

namespace Modules\Comment\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Comment\Entities\Comment;
use Modules\User\Entities\User;
use Modules\Post\Entities\Article;
use Modules\Comment\Http\Requests\EditCommentRequest;

class CommentController extends Controller
{
    protected $modelComment;
    protected $modelUser;
    protected $modelArticle;

    public function __construct(Comment $modelComment, User $modelUser, Article $modelArticle)
    {
        $this->modelComment = $modelComment;
        $this->modelArticle = $modelArticle; 
        $this->modelUser = $modelUser; 
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $list_comment = $this->modelComment->with('user')->with('article')->paginate(15);
        return view('comment::comment.list', compact('list_comment'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('comment::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('comment::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {
        $user = $this->modelUser->all();
        $comment = $this->modelComment->findOrFail($id);
        return view('comment::comment.edit', compact('user', 'comment'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update($id, EditCommentRequest $request)
    {
        $edit_comment = $this->modelComment->findOrFail($id);
        $edit_comment->content = $request->content;
        $edit_comment->save();
        return redirect()->route('comment.list')->with('success', 'Sửa thành công');
    }
    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {
        $delete_comment = $this->modelComment->findOrFail($id);
        $delete_comment->delete();
        return redirect()->route('comment.list')->with('success', 'Xóa thành công');
    }
    public function deleteAll(Request $request)
    {
        $list_id = $request->list_id;
        $this->modelComment->whereIn('id', $list_id)->delete();
        return response()->json([
            'type'=>1,
            'mess'=>"Xóa thành công"
        ]);
    }
    public function changeStatus($id)
    {
        $comment = $this->modelComment->findorFail($id);
        if($comment->status == 1) {
            $comment->status = 0;
            $comment->save();
            return Response()->json([
                'type' => 1,
                'mess' => 'Chuyển thành công',
            ]);
        }else {
            $comment->status = 1;
            $comment->save();
            return Response()->json([
                'type' => 2,
                'mess' => 'Chuyển thành công',
            ]);
        }
    }
}
