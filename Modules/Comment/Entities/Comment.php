<?php

namespace Modules\Comment\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\User\Entities\User;
use Modules\Post\Entities\Article;

class Comment extends Model
{
    protected $table = 'comments';
    protected $fillable = [
        'id', 'user_id', 'article_id', 'content', 'created_at', 'updated_at'
    ];
    public function user() 
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
    public function article() 
    {
        return $this->belongsTo(Article::class, 'article_id', 'id');
    }
}
