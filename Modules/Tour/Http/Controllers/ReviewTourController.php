<?php

namespace Modules\Tour\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Tour\Entities\ReviewTour;

class ReviewTourController extends Controller
{
    protected $modelReviewTour;

    public function __construct(ReviewTour $modelReviewTour)
    {
        $this->modelReviewTour = $modelReviewTour;
        
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $list_review = $this->modelReviewTour::query()->with('user')->with('tour')->paginate(15);
        return view('tour::review.list', compact('list_review'));
    }
    public function listReviewForTourId($id)
    {
        $list_review = $this->modelReviewTour::query()->with('user')->with('tour')->where('tour_id',$id)->paginate(15);
        return view('tour::review.list', compact('list_review'));
    }
    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('tour::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('tour::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('tour::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
    public function deleteAll(Request $request)
    {
        $list_id = $request->list_id;
        foreach($list_id as $item){
            $delete_review_tour = $this->modelReviewTour->findOrFail($item);
            $delete_review_tour->delete();
        }
        return response()->json([
            'type' => 1,
            'mess' => "Xóa thành công"
        ]);
    }
    public function changeStatus(Request $request)
    {
        $list_id = $request->list_id;
        $selected_action = $request->selected_action;
        foreach($list_id as $item){
            $change_status_review = $this->modelReviewTour->findOrFail($item);
            $change_status_review->status = $selected_action;
            $change_status_review->save();
        }
        if($selected_action == 1) {
            return response()->json([
                'type' => 1,
                'mess' => "Chuyển thành công thành Approved"
            ]);
        }else if($selected_action == 2) {
            return response()->json([
                'type' => 2,
                'mess' => "Chuyển thành công thành Pending"
            ]);
        }else if($selected_action == 3) {
            return response()->json([
                'type' => 3,
                'mess' => "Chuyển thành công thành Spam"
            ]);
        }else if($selected_action == 4) {
            return response()->json([
                'type' => 4,
                'mess' => "Chuyển thành công thành Trash"
            ]);
        }
        
    }
}
