<?php

namespace Modules\Tour\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Tour\Entities\Tour;
use Modules\User\Entities\User;
use Modules\Category\Entities\Location;
use Modules\Category\Entities\TourCategory;
use Modules\Category\Entities\TourTerm;
use App\Helpers\HelperClass;
use Modules\Tour\Http\Requests\CreateTourRequest;
use Modules\Tour\Http\Requests\EditTourRequest;
use Modules\Tour\Entities\FaqTour;
use Modules\Tour\Entities\IncludeTour;
use Modules\Tour\Entities\ExcludeTour;
use Modules\Tour\Entities\TourVehicleCategory;
use Modules\Tour\Entities\TourStyleCategory;
use Modules\Tour\Entities\ItineraryTour;
use Modules\Tour\Entities\ExtraPriceTour;
use Modules\Tour\Entities\TourStartDate;
use Modules\Tour\Entities\DiscountOfPeopleTour;
use Modules\Tour\Entities\OpenHourTour;
use Modules\Tour\Entities\PersonTypeTour;
use Modules\Tour\Entities\Destination;

class TourController extends Controller
{
    protected $modelTour;
    protected $modelUser;
    protected $modelLocation;
    protected $modelDestination;
    protected $modelTourCategory;
    protected $modelTourVehicleCategory;
    protected $modelTourStyleCategory;
    protected $modelTourTerm;
    protected $modelFaqTour;
    protected $modelIncludeTour;
    protected $modelExcludeTour;
    protected $modelItineraryTour;
    protected $modelPersonTypeTour;
    protected $modelExtraPriceTour;
    protected $modelDiscountOfPeopleTour;
    protected $modelOpenHourTour;
    protected $modelTourStartDate;

    public function __construct(TourStyleCategory $modelTourStyleCategory, TourVehicleCategory $modelTourVehicleCategory, TourStartDate $modelTourStartDate, Destination $modelDestination, OpenHourTour $modelOpenHourTour, DiscountOfPeopleTour $modelDiscountOfPeopleTour, ExtraPriceTour $modelExtraPriceTour, PersonTypeTour $modelPersonTypeTour, ItineraryTour $modelItineraryTour, ExcludeTour $modelExcludeTour, Tour $modelTour, User $modelUser, Location $modelLocation, TourCategory $modelTourCategory, TourTerm $modelTourTerm, FaqTour $modelFaqTour, IncludeTour $modelIncludeTour)
    {
        $this->modelTour = $modelTour;
        $this->modelTourVehicleCategory = $modelTourVehicleCategory;
        $this->modelTourStyleCategory = $modelTourStyleCategory;
        $this->modelUser = $modelUser;
        $this->modelLocation = $modelLocation;
        $this->modelDestination = $modelDestination;
        $this->modelTourCategory = $modelTourCategory;
        $this->modelTourTerm = $modelTourTerm;
        $this->modelFaqTour = $modelFaqTour;
        $this->modelIncludeTour = $modelIncludeTour;
        $this->modelExcludeTour = $modelExcludeTour;
        $this->modelItineraryTour = $modelItineraryTour;
        $this->modelPersonTypeTour = $modelPersonTypeTour;
        $this->modelExtraPriceTour = $modelExtraPriceTour;
        $this->modelDiscountOfPeopleTour = $modelDiscountOfPeopleTour;
        $this->modelOpenHourTour = $modelOpenHourTour;
        $this->modelTourStartDate = $modelTourStartDate;
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $list_tour = $this->modelTour::query()->with(['user', 'location','reviewTour'])->paginate(15);
        return view('tour::tour.list',compact('list_tour'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create(HelperClass $helperclass)
    {
        $vendor = $this->modelUser::query()->where('status', 1)->where('role_id', 3)->get();
        $location = $this->modelLocation::query()->where('status', 1)->get();
        $destination = $this->modelDestination::query()->where('status', 1)->get();

        $tourCategories =$this->modelTourCategory->where('status', 1)->get()->toArray();
        $tourVehicleCategory = $this->modelTourVehicleCategory::all();
        $tourTypeCategory = $this->modelTourStyleCategory::all();
        $tourType = $this->modelTourTerm::query()->where('attribute_id', 1)->get();
        $tourFacility = $this->modelTourTerm::query()->where('attribute_id', 2)->get();
        return view('tour::tour.add', compact(
            'vendor', 
            'location', 
            'tourCategories', 
            'tourType', 
            'tourFacility', 
            'destination', 
            'tourVehicleCategory', 
            'tourTypeCategory'
        ));
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(CreateTourRequest $request)
    {
        // dd($request->all());
        $str_avatar = '';
        $str_album = '';
        
        if ($request->input('avatar')) {
            $str_avatar = checkImage($request->input('avatar'));
        }
        if($request->input('album')) {
            $str_album = checkMultipleImage($request->input('album'));
        }
        
        $newTour = new $this->modelTour;
        $newTour->title = $request->title;
        $newTour->content = $request->content;
        $newTour->description = $request->description;
        $newTour->category_id = $request->category_id;
        $newTour->video = $request->video;
        $newTour->duration = $request->duration;
        $newTour->min_people = $request->min_people;
        $newTour->max_people = $request->max_people;
        $newTour->album = $str_album;
        $newTour->thumnail = $str_avatar;
        $newTour->location_id = $request->location_id;
        $newTour->destination_id = $request->destination_id;
        $newTour->address = $request->address;
        $newTour->latitu = $request->latitu;
        $newTour->longtitu = $request->longtitu;
        $newTour->mapzoom = $request->mapzoom;
        $newTour->price = $request->price;
        $newTour->sale = $request->sale;
        if(empty($request->enable_open_hours)) {
            $newTour->is_open_hour = 0;
        } else {
            $newTour->is_open_hour = $request->enable_open_hours;
        }
        if(empty($request->enable_person_types)) {
            $newTour->is_person_type = 0;
        } else {
            $newTour->is_person_type = $request->enable_person_types;
        }
        $newTour->seo_title = $request->seo_title;
        $newTour->seo_description = $request->seo_description;
        $newTour->slug = str_slug($request->title);
        $newTour->status = $request->status;
        $newTour->vendor_id = $request->vendor_id;
        if(empty($request->featured)) {
            $newTour->is_featured = 0;
        } else {
            $newTour->is_featured = $request->featured;
        }
        if(empty($request->sale_holiday)) {
            $newTour->sale_holiday = 0;
        } else {
            $newTour->sale_holiday = $request->sale_holiday;
        }
        // $newTour->deafault_state = $request->deafault_state;
        $newTour->save();

        if(!empty($request->faqs)) {
            foreach($request->faqs as $item) {
                if(!empty($item['title']) && !empty($item['content'])) {
                    $newFaq = new $this->modelFaqTour;
                    $newFaq->title = $item['title'];
                    $newFaq->content = $item['content'];
                    $newFaq->tour_id = $newTour->id;
                    $newFaq->save();
                }
            }
        }
        if(!empty($request->include)) {
            foreach($request->include as $item) {
                if(!empty($item['title'])) {
                    $newInclude = new $this->modelIncludeTour;
                    $newInclude->title = $item['title'];
                    $newInclude->tour_id = $newTour->id;
                    $newInclude->save();
                }
            }
        }
        if(!empty($request->exclude)) {
            foreach($request->exclude as $item) {
                if(!empty($item['title'])) {
                    $newExclude = new $this->modelExcludeTour;
                    $newExclude->title = $item['title'];
                    $newExclude->tour_id = $newTour->id;
                    $newExclude->save();
                }
            }
        }
        if(!empty($request->itinerary)) {
            foreach($request->itinerary as $item) {
                $str_image = '';
                if (!empty($item['image'])) {
                    $str_image = checkImage($item['image']);
                }
                if(!empty($item['title']) && !empty($item['content']) && !empty($item['desc']) && !empty($item['image'])) {
                    $newItineraryTour = new $this->modelItineraryTour;
                    $newItineraryTour->title = $item['title'];
                    $newItineraryTour->content = $item['content'];
                    $newItineraryTour->description = $item['desc'];
                    $newItineraryTour->image = $str_image;
                    $newItineraryTour->tour_id = $newTour->id;
                    $newItineraryTour->save();
                }
            }
        }
        if(empty($request->enable_person_types)) {
            if(!empty($request->person_types)) {
                foreach($request->person_types as $item) {
                    if(!empty($item['title']) && !empty($item['desc']) && !empty($item['min']) && !empty($item['max']) && !empty($item['price'])) {
                        $newPersonTypeTour = new $this->modelPersonTypeTour;
                        $newPersonTypeTour->title = $item['title'];
                        $newPersonTypeTour->description = $item['desc'];
                        $newPersonTypeTour->min = $item['min'];
                        $newPersonTypeTour->max = $item['max'];
                        $newPersonTypeTour->price = $item['price'];
                        $newPersonTypeTour->tour_id = $newTour->id;
                        $newPersonTypeTour->save();
                    }
                }
            }
        }
        if(!empty($request->extra_price)) {
            foreach($request->extra_price as $item) {
                if(!empty($item['title']) && !empty($item['price'])) {
                    $newExtraPtice =  new $this->modelExtraPriceTour;
                    $newExtraPtice->title = $item['title'];
                    $newExtraPtice->price = $item['price'];
                    $newExtraPtice->type = $item['type'];
                    $newExtraPtice->tour_id = $newTour->id;
                    if(!empty($item['per_person'])) {
                        $newExtraPtice->per_person = $item['per_person'];
                    }else {
                        $newExtraPtice->per_person = 0;
                    }
                    $newExtraPtice->save();
                }
            }
        }
        if(!empty($request->discount_by_people)) {
            foreach($request->discount_by_people as $item) {
                if(!empty($item['from']) && !empty($item['to']) && !empty($item['amount'])) {
                   $newDiscountByPeople = new $this->modelDiscountOfPeopleTour;
                   $newDiscountByPeople->from =  $item['from'];
                   $newDiscountByPeople->to =  $item['to'];
                   $newDiscountByPeople->amount =  $item['amount'];
                   $newDiscountByPeople->type =  $item['type'];
                   $newDiscountByPeople->tour_id = $newTour->id;
                   $newDiscountByPeople->save();
                }
            }
        }
        if(!empty($request->enable_open_hours)) {
            if(!empty($request->open_hours)) {
                foreach($request->open_hours as $key => $value) {
                    $newOpenHourTour = new $this->modelOpenHourTour;
                    $newOpenHourTour->from = $value['from'];
                    $newOpenHourTour->to = $value['to'];
                    $newOpenHourTour->day_of_week = $key;
                    $newOpenHourTour->tour_id = $newTour->id;
                    if(!empty($value['enable'])) {
                        $newOpenHourTour->status = $value['enable'];
                    }else {
                        $newOpenHourTour->status = 0;
                    }
                    $newOpenHourTour->save();
                }
            }
        }
        if(!empty($request->tourtype)){
            $newTour->termstyle()->attach($request->tourtype);
        }
        if(!empty($request->tourfacilities)){
            $newTour->facility()->attach($request->tourfacilities);
        }
        if(!empty($request->vehiclecategory)){
            $newTour->tourVehicle()->attach($request->vehiclecategory);
        }
        if(!empty($request->typecategory)){
            $newTour->tourismType()->attach($request->typecategory);
        }
        return redirect()->route('tour.list')->with('success', 'Thêm mới thành công');
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('tour::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id, HelperClass $helperclass)
    {
        $tourEdit = $this->modelTour::query()->with('faq')->with('include')->with('exclude')->with('itinerary')->with('personType')->with('extraPrice')->with('discountOfPeople')->with('openHour')->with('termstyle')->with('facility')->with('tourVehicle')->with('tourismType')->findOrFail($id);
        $vendor = $this->modelUser::query()->where('status', 1)->where('role_id', 3)->get();
        $location = $this->modelLocation::query()->where('status', 1)->get();
        $destination = $this->modelDestination::query()->where('status', 1)->get();
        $tourCategories =$helperclass->showCategoriesTour($this->modelTourCategory->all()->toArray(), $tourEdit->category_id);
        $tourVehicleCategory = $this->modelTourVehicleCategory::all();
        $tourTypeCategory = $this->modelTourStyleCategory::all();
        $tourType = $this->modelTourTerm::query()->where('attribute_id', 1)->get();
        $tourFacility = $this->modelTourTerm::query()->where('attribute_id', 2)->get();
        $tour_styles_selected = [];
        foreach($tourEdit->termstyle as $item) {
            $tour_styles_selected[] = $item->id;
        }
        $tour_facility_selected = [];
        foreach($tourEdit->facility as $item) {
            $tour_facility_selected[] = $item->id;
        }
        $tour_vehicle_selected = [];
        foreach($tourEdit->tourVehicle as $item) {
            $tour_vehicle_selected[] = $item->id;
        }
        $tour_type_selected = [];
        foreach($tourEdit->tourismType as $item) {
            $tour_type_selected[] = $item->id;
        }
        return view('tour::tour.edit', compact(
            'vendor', 
            'location', 
            'destination', 
            'tourCategories', 
            'tourType', 
            'tourFacility', 
            'tourEdit', 
            'tour_styles_selected', 
            'tour_facility_selected', 
            'tour_vehicle_selected', 
            'tourVehicleCategory',
            'tour_type_selected',
            'tourTypeCategory'
        ));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update($id, EditTourRequest $request)
    {
        $str_avatar = '';
        $str_album = '';
        
        if ($request->input('avatar')) {
            $str_avatar = checkImage($request->input('avatar'));
        }
        if($request->input('album')) {
            $str_album = checkMultipleImage($request->input('album'));
        }
        
        $updateTour = $this->modelTour->findOrFail($id);
        $updateTour->title = $request->title;
        $updateTour->content = $request->content;
        $updateTour->description = $request->description;
        $updateTour->category_id = $request->category_id;
        $updateTour->video = $request->video;
        $updateTour->duration = $request->duration;
        $updateTour->min_people = $request->min_people;
        $updateTour->max_people = $request->max_people;
        $updateTour->album = $str_album;
        $updateTour->thumnail = $str_avatar;
        $updateTour->location_id = $request->location_id;
        $updateTour->destination_id = $request->destination_id;
        $updateTour->address = $request->address;
        $updateTour->latitu = $request->latitu;
        $updateTour->longtitu = $request->longtitu;
        $updateTour->mapzoom = $request->mapzoom;
        $updateTour->price = $request->price;
        $updateTour->sale = $request->sale;
        if(empty($request->enable_open_hours)) {
            $updateTour->is_open_hour = 0;
        } else {
            $updateTour->is_open_hour = $request->enable_open_hours;
        }
        if(empty($request->enable_person_types)) {
            $updateTour->is_person_type = 0;
        } else {
            $updateTour->is_person_type = $request->enable_person_types;
        }
        $updateTour->seo_title = $request->seo_title;
        $updateTour->seo_description = $request->seo_description;
        $updateTour->slug = str_slug($request->title);
        $updateTour->status = $request->status;
        $updateTour->vendor_id = $request->vendor_id;
        if(empty($request->featured)) {
            $updateTour->is_featured = 0;
        } else {
            $updateTour->is_featured = $request->featured;
        }
        if(empty($request->sale_holiday)) {
            $updateTour->sale_holiday = 0;
        } else {
            $updateTour->sale_holiday = $request->sale_holiday;
        }
        // $updateTour->deafault_state = $request->deafault_state;
        $updateTour->save();

        $delete_faqs = $this->modelFaqTour::query()->where('tour_id', $id)->delete();
        if(!empty($request->faqs)) {
            foreach($request->faqs as $item) {
                if(!empty($item['title']) && !empty($item['content'])) {
                    $newFaq = new $this->modelFaqTour;
                    $newFaq->title = $item['title'];
                    $newFaq->content = $item['content'];
                    $newFaq->tour_id = $updateTour->id;
                    $newFaq->save();
                }
            }
        }
        $delete_include = $this->modelIncludeTour::query()->where('tour_id', $id)->delete();
        if(!empty($request->include)) {
            foreach($request->include as $item) {
                if(!empty($item['title'])) {
                    $newInclude = new $this->modelIncludeTour;
                    $newInclude->title = $item['title'];
                    $newInclude->tour_id = $updateTour->id;
                    $newInclude->save();
                }
            }
        }
        $delete_exclude = $this->modelExcludeTour::query()->where('tour_id', $id)->delete();
        if(!empty($request->exclude)) {
            foreach($request->exclude as $item) {
                if(!empty($item['title'])) {
                    $newExclude = new $this->modelExcludeTour;
                    $newExclude->title = $item['title'];
                    $newExclude->tour_id = $updateTour->id;
                    $newExclude->save();
                }
            }
        }
        $delete_itinerary = $this->modelItineraryTour::query()->where('tour_id', $id)->delete();
        if(!empty($request->itinerary)) {
            foreach($request->itinerary as $item) {
                $str_image = '';
                if (!empty($item['image'])) {
                    $str_image = checkImage($item['image']);
                }
                if(!empty($item['title']) && !empty($item['content']) && !empty($item['desc']) && !empty($item['image'])) {
                    $newItineraryTour = new $this->modelItineraryTour;
                    $newItineraryTour->title = $item['title'];
                    $newItineraryTour->content = $item['content'];
                    $newItineraryTour->description = $item['desc'];
                    $newItineraryTour->image = $str_image;
                    $newItineraryTour->tour_id = $updateTour->id;
                    $newItineraryTour->save();
                }
            }
        }
        $delete_person_types = $this->modelPersonTypeTour::query()->where('tour_id', $id)->delete();
        if(empty($request->enable_person_types)) {
            if(!empty($request->person_types)) {
                foreach($request->person_types as $item) {
                    if(!empty($item['title']) && !empty($item['desc']) && !empty($item['min']) && !empty($item['max']) && !empty($item['price'])) {
                        $newPersonTypeTour = new $this->modelPersonTypeTour;
                        $newPersonTypeTour->title = $item['title'];
                        $newPersonTypeTour->description = $item['desc'];
                        $newPersonTypeTour->min = $item['min'];
                        $newPersonTypeTour->max = $item['max'];
                        $newPersonTypeTour->price = $item['price'];
                        $newPersonTypeTour->tour_id = $updateTour->id;
                        $newPersonTypeTour->save();
                    }
                }
            }
        }
        $delete_extra_price = $this->modelExtraPriceTour::query()->where('tour_id', $id)->delete();
        if(!empty($request->extra_price)) {
            foreach($request->extra_price as $item) {
                if(!empty($item['title']) && !empty($item['price'])) {
                    $newExtraPtice =  new $this->modelExtraPriceTour;
                    $newExtraPtice->title = $item['title'];
                    $newExtraPtice->price = $item['price'];
                    $newExtraPtice->type = $item['type'];
                    $newExtraPtice->tour_id = $updateTour->id;
                    if(!empty($item['per_person'])) {
                        $newExtraPtice->per_person = $item['per_person'];
                    }else {
                        $newExtraPtice->per_person = 0;
                    }
                    $newExtraPtice->save();
                }
            }
        }
        $delete_discount_by_people = $this->modelDiscountOfPeopleTour::query()->where('tour_id', $id)->delete();
        if(!empty($request->discount_by_people)) {
            foreach($request->discount_by_people as $item) {
                if(!empty($item['from']) && !empty($item['to']) && !empty($item['amount'])) {
                   $newDiscountByPeople = new $this->modelDiscountOfPeopleTour;
                   $newDiscountByPeople->from =  $item['from'];
                   $newDiscountByPeople->to =  $item['to'];
                   $newDiscountByPeople->amount =  $item['amount'];
                   $newDiscountByPeople->type =  $item['type'];
                   $newDiscountByPeople->tour_id = $updateTour->id;
                   $newDiscountByPeople->save();
                }
            }
        }
        $delete_open_hours = $this->modelOpenHourTour::query()->where('tour_id', $id)->delete();
        if(!empty($request->enable_open_hours)) {
            if(!empty($request->open_hours)) {
                foreach($request->open_hours as $key => $value) {
                    $newOpenHourTour = new $this->modelOpenHourTour;
                    $newOpenHourTour->from = $value['from'];
                    $newOpenHourTour->to = $value['to'];
                    $newOpenHourTour->day_of_week = $key;
                    $newOpenHourTour->tour_id = $updateTour->id;
                    if(!empty($value['enable'])) {
                        $newOpenHourTour->status = $value['enable'];
                    }else {
                        $newOpenHourTour->status = 0;
                    }
                    $newOpenHourTour->save();
                }
            }
        }
        if(!empty($request->tourtype)){
            $updateTour->termstyle()->sync($request->tourtype);
        }else {
            $updateTour->termstyle()->detach();
        }
        if(!empty($request->tourfacilities)){
            $updateTour->facility()->sync($request->tourfacilities);
        }else {
            $updateTour->facility()->detach();
        }
        if(!empty($request->vehiclecategory)){
            $updateTour->tourVehicle()->sync($request->vehiclecategory);
        }else {
            $updateTour->tourVehicle()->detach();
        }
        if(!empty($request->typecategory)){
            $updateTour->tourismType()->sync($request->typecategory);
        }else {
            $updateTour->tourismType()->detach();
        }
        return redirect()->route('tour.list')->with('success', 'Chỉnh sửa thành công');
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {
        $deleteTour = $this->modelTour->findorFail($id);
        $deleteTour->termstyle()->detach();
        $deleteTour->tourVehicle()->detach();
        $deleteTour->tourismType()->detach();
        $deleteTour->facility()->detach();
        $deleteTour->delete();
        return redirect()->route('tour.list')->with('success', 'Xóa thành công');
    }
    public function deleteAll(Request $request)
    {
        $list_id = $request->list_id;
        foreach($list_id as $item){
            $deleteTour = $this->modelTour->findorFail($item);
            $deleteTour->termstyle()->detach();
            $deleteTour->tourVehicle()->detach();
            $deleteTour->tourismType()->detach();
            $deleteTour->facility()->detach();
            $deleteTour->delete();
        }
        return response()->json([
            'type' => 1,
            'mess' => "Xóa thành công"
        ]);
    }
    public function changeStatus($id)
    {
        $tour = $this->modelTour->findorFail($id);
        if($tour->status == 1) {
            $tour->status = 0;
            $tour->save();
            return Response()->json([
                'type' => 1,
                'mess' => 'Chuyển thành công',
            ]);
        }else {
            $tour->status = 1;
            $tour->save();
            return Response()->json([
                'type' => 2,
                'mess' => 'Chuyển thành công',
            ]);
        }
    }
    public function listAvailability($id)
    {
        $listAvailability = $this->modelTourStartDate::query()->where('tour_id', $id)->paginate(15);
        return view('tour::availability.list', compact('listAvailability'));
    }
}
