<?php

namespace Modules\Tour\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Tour\Entities\Destination;
use Modules\Tour\Http\Requests\CreateDestinationsRequest;

class DestinationsController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    protected $modelDestinations;

    public function __construct(Destination $modelDestinations)
    {
        $this->modelDestinations =$modelDestinations;
    }
    public function index()
    {
        $listDestinations = $this->modelDestinations::query()->paginate(15);
        return view('tour::destinations.list-add', compact('listDestinations'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('tour::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(CreateDestinationsRequest $request)
    {
        $modelDestinations = new $this->modelDestinations;
        $modelDestinations->name = $request->name;
        $modelDestinations->status = $request->status;
        $modelDestinations->save();
        return redirect()->route('tour.destinations')->with('success', 'Thêm mới thành công');
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('tour::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {
        $destinations = $this->modelDestinations::query()->findOrFail($id);
        return view('tour::destinations.edit', compact('destinations'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(CreateDestinationsRequest $request)
    {
        $updateDestinations = $this->modelDestinations;
        $updateDestinations->name = $request->name;
        $updateDestinations->status = $request->status;
        $updateDestinations->save();
        return redirect()->route('tour.destinations')->with('success', 'Chỉnh sửa thành công');
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {
        $deleteDestinations = $this->modelDestinations::query()->findOrFail($id);
        $deleteDestinations->delete();
        return redirect()->route('tour.destinations')->with('success', 'Xóa thành công');
    }
    public function changeStatus($id)
    {
        $changeStatus = $this->modelDestinations::query()->findOrFail($id);
        if ($changeStatus->status == 1 ) {
            $changeStatus->status = 0;
            $changeStatus->save();
            return response()->json([
                'type' => 1,
                'mess' => 'Chuyển thành công'
            ]);
        }else{
            $changeStatus->status = 1;
            $changeStatus->save();
            return response()->json([
                'type' => 2,
                'mess' => 'Chuyển thành công'
            ]);
        }
    }
}
