<?php

namespace Modules\Tour\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Tour\Entities\Tour;
use Modules\Tour\Entities\TourAvailability;
use Carbon\Carbon;

class TourAvailabilityController extends Controller
{
    protected $modelTour;
    protected $modelTourAvailability;

    public function __construct(
        Tour $modelTour, 
        TourAvailability $modelTourAvailability
        )
    {
        $this->modelTour = $modelTour;
        $this->modelTourAvailability = $modelTourAvailability;
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index($id)
    {
        return view('tour::availability.index', compact('id') );
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function get($id, Request $request) 
    {
        $tour = $this->modelTour::query()->with('openHour')->findOrFail($id);
        $start = Carbon::createFromFormat('Y-m-d', $request->start);
        $end = $start->copy()->addDays(41);

        $arrOpenHour = [];
        if($tour->is_open_hour == 1 && sizeof($tour->openHour) != 0) {
            foreach ($tour->openHour as $item) {
                if($item->status == 1) {
                    $arrOpenHour[] = $item->day_of_week;
                }
            }
        }
        
        $numberDay = $end->diffInDays($start);
        $arr = [];
        $arr[] = $start->format('Y-m-d');
        for ($i = 1; $i <= $numberDay ; $i++) {
            $day    = $start->addDays($i);
            $arr[] = $day->format('Y-m-d');
            $start->subDays($i);
        }

        $tourAvavailability = $this->modelTourAvailability->where('tour_id', $id)->get();
        $arrDate = [];
        foreach ($tourAvavailability as $item) {
            $arrDate[] = $item->date;
        }
        
        $arrResult = array_map(function($date) use ($tourAvavailability, $arrDate, $tour, $arrOpenHour) {  
            if(in_array($date, $arrDate)) {
                foreach ($tourAvavailability as $items) {
                    if($date == $items->date) {
                        if($items->remaining == 0) {
                            $event = 'Full book';
                            $className = 'bg-danger';
                        }else{
                            $event = $items->remaining . " x " . $items->price;
                            $className = 'bg-success';
                        }
                        $remaining = $items->remaining;
                        $price = $items->price;
                        $tourId = $items->tour_id;
                    }
                }
                $result = [
                    'start' => $date,
                    'tourId' => $tourId,
                    'remaining' => $remaining,
                    'price' => $price,
                    'className' => $className,
                    'title' => $event       
                ]; 
                
            }else{
                if($tour->is_open_hour == 1 && sizeof($tour->openHour) != 0) {
                    $dayOfWeekOfDate = Carbon::createFromFormat('Y-m-d', $date)->dayOfWeek;
                    if(in_array($dayOfWeekOfDate, $arrOpenHour)) {
                        $event = $tour->max_people . " x " . $tour->price;
                        $remaining = $tour->max_people;
                        $price = $tour->price;
                        $tourId = $tour->id;
                        $result = [
                            'start' => $date,
                            'tourId' => $tourId,
                            'remaining' => $remaining,
                            'price' => $price,
                            'className' => 'bg-warning',
                            'title' => $event
                        ]; 
                    }else {
                        $event = 'Bloked';
                        $remaining = $tour->max_people;
                        $price = $tour->price;
                        $tourId = $tour->id;
                        $result = [
                            'start' => $date,
                            'tourId' => $tourId,
                            'remaining' => $remaining,
                            'price' => $price,
                            'className' => 'bg-secondary',
                            'title' => $event
                        ]; 
                    }
                } else {
                    $event = $tour->max_people . " x " . $tour->price;
                    $remaining = $tour->max_people;
                    $price = $tour->price;
                    $tourId = $tour->id;
                    $result = [
                        'start' => $date,
                        'tourId' => $tourId,
                        'remaining' => $remaining,
                        'price' => $price,
                        'className' => 'bg-warning',
                        'title' => $event
                    ]; 
                }
                
                
            }
            return $result;
        }, $arr);
        return response()->json($arrResult);
        
    }
    public function save(Request $request)
    {
        // dd($request->all());
        if($request->remaining <= 0 ){
            $remaining = 0;
        }else {
            $remaining = $request->remaining;
        }
        if($request->price <= 0 ){
            $price = 0;
        }else {
            $price = $request->price;
        }
        $tourAvavailability = $this->modelTourAvailability->where('date', $request->day)->where('tour_id', $request->tourId)->first();
        if($tourAvavailability != null) {
            $availability = $this->modelTourAvailability->findOrFail($tourAvavailability->id);
            $availability->remaining = $remaining;
            $availability->price = $price;
            $availability->save();
            
        }else {
            $availability = new $this->modelTourAvailability;
            $availability->remaining = $remaining;
            $availability->tour_id = $request->tourId;
            $availability->date = $request->day;
            $availability->price = $price;
            $availability->save();
        }
        return Response()->json([
            'type' => 1,
            'mess' => 'Cập nhật thành công',
        ]);
    }
    public function create()
    {
        return view('tour::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('tour::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('tour::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
}
