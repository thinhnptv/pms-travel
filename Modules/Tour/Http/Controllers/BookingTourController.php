<?php

namespace Modules\Tour\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Tour\Entities\BookingTour;

class BookingTourController extends Controller
{
    protected $modelBookingTour;
    
    public function __construct(BookingTour $modelBookingTour)
    {
        $this->modelBookingTour = $modelBookingTour;
        
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $listBookingTour = $this->modelBookingTour::query()
        ->with([
            'user',
            'extraPriceBooked',
            'personTypeBooked',
            'tour'=> function($query){
                $query->with('user');
            }
        ])->paginate(15);
        foreach($listBookingTour as $item) {
            $item->total = getTotalBookingTour($item);
        }
        return view('tour::booking.list',compact('listBookingTour'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('tour::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('tour::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('tour::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
    public function deleteAll(Request $request)
    {
        $listId = $request->listId;
        foreach($listId as $item){
            $deleteBookingTour = $this->modelBookingTour->findOrFail($item);
            $deleteBookingTour->delete();
        }
        return response()->json([
            'type' => 1,
            'mess' => "Xóa thành công"
        ]);
    }
    public function changeStatus(Request $request)
    {
        $listId = $request->listId;
        $selectedAction = $request->selectedAction;
        foreach($listId as $item){
            $changeStatusBookingTour = $this->modelBookingTour->findOrFail($item);
            $changeStatusBookingTour->status = $selectedAction;
            $changeStatusBookingTour->save();
        }
        return response()->json([
            'type' => 1,
            'status' => showStatusBooking($selectedAction),
            'mess' => "Chuyển thành công"
        ]);
    }
}
