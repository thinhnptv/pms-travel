<?php

namespace Modules\Tour\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class CreateTourRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        // dd((int)$request->price);
        return [
            'title'=>'required|min:3',
            'price'=>'required|numeric|min:1',
            'max_people'=>'required|numeric|min:1',
            'min_people'=>'nullable|numeric|max:'.(int)$request->max_people,
            'duration'=>'required|integer|min:1',
            'avatar'=>'required',
            'video'=>'required',
            'album'=>'required',
            'address'=>'required',
            'latitu'=>'required',
            'longtitu'=>'required',
            'sale'=>'nullable|numeric|max:'.(int)$request->price
        ];
    }
    public function messages () 
    {
        return [
            'title.required' => "Tên tour không được để trống!",
            'title.min' => "Tên tour không được ít hơn 3 ký tự!",
            'price.required' => "Giá tour không được để trống",
            'price.numeric' => "Giá tour phải là số",
            'price.min' => "Giá tour phải lớn hơn 0",
            'max_people.required' => "Số lượng người lớn nhất không được để trống",
            'max_people.numeric' => "Số lượng người lớn nhất phải là số",
            'max_people.min' => "Số lượng người lớn nhất phải lớn hơn 0",
            'min_people.max' => "Số lượng người nhỏ nhất phải nhỏ hơn số lượng người lớn nhất",
            'duration.required' => "Thời lượng tour không được để trống",
            'duration.integer' => "Thời lượng tour phải là số nguyên",
            'duration.min' => "Thời lượng tour phải lớn hơn 0",
            'avatar.required' => "Ảnh đại diện tour không được để trống!",
            'video.required' => "Link youtobe không được để trống!",
            'album.required' => "Album không được để trống!",
            'address.required' => "Địa chỉ không được để trống!",
            'latitu.required' => "Vĩ độ không được để trống!",
            'longtitu.required' => "Kinh độ không được để trống!",
            'sale.numeric' => "Giá sale phải là số",
            'sale.max' => "Giá sale không được lớn hơn giá gốc"
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
