<?php

Route::group(['middleware' => ['web', 'adminLogin'], 'prefix' => 'admin/tour', 'namespace' => 'Modules\Tour\Http\Controllers'], function()
{
    Route::get('/', 'TourController@index')->name('tour.list');
    Route::post('/change-status/{id}', 'TourController@changeStatus')->name('tour.change_status');
    Route::get('/add-tour', 'TourController@create')->name('tour.get_add');
    Route::post('/add-tour', 'TourController@store')->name('tour.post_add');
    Route::get('/edit-tour/{id}', 'TourController@edit')->name('tour.get_edit');
    Route::post('/edit-tour/{id}', 'TourController@update')->name('tour.post_edit');
    Route::get('/delete-tour/{id}', 'TourController@destroy')->name('tour.delete');
    Route::post('/delete-all-tour', 'TourController@deleteAll')->name('tour.delete_all');

    Route::get('/list-review', 'ReviewTourController@index')->name('tour.review.list');
    Route::post('/delete-review-all', 'ReviewTourController@deleteAll')->name('tour.review.delete_all');
    Route::post('/change-review-status', 'ReviewTourController@changeStatus')->name('tour.review.change_status');
    Route::get('/list-review-for-tour/{id}', 'ReviewTourController@listReviewForTourId')->name('tour.review.list_for_tour');

    Route::get('/list-booking-tour', 'BookingTourController@index')->name('tour.booking.list');
    Route::post('/delete-booking-tour-all', 'BookingTourController@deleteAll')->name('tour.booking.delete_all');
    Route::post('/change-booking-tour-status', 'BookingTourController@changeStatus')->name('tour.booking.change_status');

    Route::get('/availability-tour/{id}', 'TourAvailabilityController@index')->name('tour.list_availability'); 
    Route::get('/get-tour-availability/{id}', 'TourAvailabilityController@get')->name('tour.get_availability'); 
    Route::post('/save-tour-availability', 'TourAvailabilityController@save')->name('tour.availability.save');

    Route::get('/destinations', 'DestinationsController@index')->name('tour.destinations');
    Route::post('/add-destinations', 'DestinationsController@store')->name('tour.destinations.post-add');
    Route::post('/change-status-destinations/{id}', 'DestinationsController@changeStatus')->name('tour.destinations.change_status');
    Route::get('/delete/{id}', 'DestinationsController@destroy')->name('tour.destinations.delete');
    Route::get('/edit-destinations/{id}', 'DestinationsController@edit')->name('tour.destinations.edit');
    Route::post('/edit-destinations/{id}', 'DestinationsController@update')->name('tour.destinations.update');
});
