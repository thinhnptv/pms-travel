@extends('base::layouts.master')
@section('css')
        <!-- Plugins css-->
        <link href="{{ URL::asset('assets/libs/select2/select2.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ URL::asset('assets/libs/summernote/summernote.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ URL::asset('assets/libs/dropzone/dropzone.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ URL::asset('assets/css/webt.css')}}" rel="stylesheet" type="text/css" />
@endsection
@section('content')
<div class="container-fluid">

    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="javascript: void(0);">UBold</a></li>
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Apps</a></li>
                        <li class="breadcrumb-item active">Destinations</li>
                    </ol>
                </div>
                <h4 class="page-title">Destinations</h4>
                <div class="text-sm-right">
                    {{-- <a href="" class="btn btn-primary waves-effect waves-light mb-2" ><i class=" mdi mdi-gesture-swipe"></i> Back to hotel</a> --}}
                </div>
            </div>
        </div>
    </div>
    <div class="notification">
        {{-- @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
        @endif --}}
        {{-- @if (session('success'))
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
        @endif --}}
        @if (session('errorsss'))
                <div class="alert alert-danger">
                    {{ session('errorsss') }}
                </div>
        @endif
    </div>
    
    <!-- end page title -->
    <form action="{{ route('tour.destinations.post-add') }}" method="POST" enctype="multipart/form-data">
        <input type="hidden" name="_token" value="{{csrf_token()}}" /> 
        <div class="row">
            <div class="col-md-12 col-lg-4">
                <div class="card p-2">
                    <div class="row">
                        <div class="col-md-12 col-lg-12">
                            <h4>Add Destinations</h4>
                            <hr>
                            <div class="form-group mb-3">
                                <label for="name">Name</label>
                                <span class="text-danger">*</span>
                                <input type="text" id="name" name="name" class="form-control" placeholder="enter destinations name">
                                <p class="help is-danger">{{ $errors->first('name') }}</p>
                            </div> 
                            <div class="form-group mb-3">
                                <h4>status</h4>
                                <div class="form-group mb-3">
                                    <select name="status" id="" class="form-control">
                                        <option value="1">Publish</option>
                                        <option value="2">Block</option>
                                    </select>
                                </div> 
                            </div>
                        </div>
                    </div> 
                    <div class="form-group mb-3 text-right">
                        <button type="submit" class="btn w-sm btn-success waves-effect waves-light">Save Changes</button>
                        <button type="reset" class="btn w-sm btn-primary waves-effect waves-light">Reset</button>
                    </div>     
                </div>
            </div>
        </form>
            <div class="col-md-12 col-lg-8">
                
                <div class="row">
                    <div class="col-md-12 col-lg-12">
                        <div class="card p-2">
                            <div class="table-responsive">
                                <table class="table table-bordered  mb-0">
                                    <thead>
                                        <tr>
                                            <th width="50px"><input type="checkbox" id="master" class="check_master"></th>
                                            <th class="text-center">STT</th>
                                            <th class="text-center">Name</th>
                                            <th class="text-center">Status</th>
                                            <th class="text-center">#</th>
                                            <th class="text-center">#</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php
                                            $stt = 1;
                                        @endphp
                                        @foreach ($listDestinations as $item)
                                        <tr>
                                            <td><input type="checkbox" class="sub-chk" data-id="{{ $item->id }}"></td>
                                            <th class="text-center" scope="row">{{ $stt }}</th>
                                            <td>{{ $item->name }}</td>
                                            @if ($item->status)
                                                <td class="text-center"><a href="javascrip:void(0)" class="btn btn-primary status_destinations"  data-id="{{ $item->id }}" data-url="{{ route('tour.destinations.change_status', $item->id) }}">Publish</a></td>
                                            @else
                                                <td class="text-center"><a href="javascrip:void(0)" class="btn btn-danger status_destinations"  data-id="{{ $item->id }}" data-url="{{ route('tour.destinations.change_status', $item->id) }}">Blocked</a></td>
                                            @endif
                                            <td class="text-center">
                                                <a href="{{ route('tour.destinations.edit',  $item->id) }}" class="btn btn-success"  data-id="">Edit</a>
                                            </td>
                                            <td>
                                                <a href="{{ route('tour.destinations.delete', $item->id) }}" class="btn btn-danger" onclick="return confirm('Bạn chắc chắn muốn xoá?')">Delete</a>
                                            </td>
                                        </tr>
                                        @php
                                            $stt++;
                                        @endphp
                                        @endforeach
                                    </tbody>
                                </table>
                                <div class="mt-2">
                                    {{-- phân trang --}}
                                    {{ $listDestinations->render() }}
                                </div>
                            </div>
                        </div>
                            
                    </div>
                </div>
            </div>
        </div>
        
    <!-- end row -->
    
</div>
@endsection

@push('js')
    <!-- Summernote js -->
    <script src="{{ URL::asset('assets/libs/summernote/summernote.min.js') }}"></script>
    <!-- Select2 js-->
    <script src="{{ URL::asset('assets/libs/select2/select2.min.js') }}"></script>
    <!-- Dropzone file uploads-->
    <script src="{{ URL::asset('assets/libs/dropzone/dropzone.min.js') }}"></script>
    <!-- Init js -->
    <script>
        jQuery(document).ready(function(){
        $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
        });
        // Summernote
    
        $('#content').summernote({
            height: 180,                 // set editor height
            minHeight: null,             // set minimum height of editor
            maxHeight: null,             // set maximum height of editor
            focus: false                 // set focus to editable area after initializing summernote
        });
        
        // Select2
        $('.select2').select2();
    
        
    });
        $( document ).ready(function() {
            var checkNotification =  $('.notification').find('.alert-success').length;
            if(checkNotification != 0) {
                    setTimeout(function(){
                        $('.notification').empty();
                    }, 3000);
            }
        });


         $(document).on('click','.check_master', function(e) {
            if($(this).is(':checked',true))
            {
                $(".sub-chk").prop('checked', true);
            } else {
                $(".sub-chk").prop('checked',false);
            }
        });


        $(document).on('click','.status_destinations',function(e) {
            e.preventDefault();
            toastr.clear();
            toastr.options = {
                "closeButton": true,
                "timeOut": "5000",
                "positionClass": "toast-top-right"
            }
            const __this = this;
            $(__this).prop('disabled', true);
            var id = $(__this).attr('data-id');
            const __token = $('meta[name="csrf-token"]').attr('content');
            data_ = {
                _token: __token
            }
            var url_ = $(__this).attr('data-url');
            var request = $.ajax({
                url: url_,
                type: 'POST',
                data: data_,
                dataType: "json"
            });
            request.done(function (msg) {
                if (msg.type == 1) {
                    $('.alert-success').remove();
                    $(__this).prop('disabled', false);
                    $(__this).html('Blocked');
                    $(__this).removeClass('btn-primary');
                    $(__this).addClass('btn-danger');
                    toastr.success(msg.mess);
                }else{
                    $('.alert-success').remove();
                    $(__this).prop('disabled', false);
                    $(__this).html('Publish');
                    $(__this).removeClass('btn-danger');
                    $(__this).addClass('btn-primary');
                    toastr.success(msg.mess);
                }
                return false;
            });
            request.fail(function (jqXHR, textStatus) {
                alert("Không thể gửi yêu cầu mã lỗi : " + textStatus);
            });
            
        });


        $(document).on('click','.page-link',function(e) {
            e.preventDefault();
            url_ = this.href;
            var url_string = location.href;
            var url = new URL(url_string);
            var role = url.searchParams.get("role");
            if(role != null){
                url_ = url_ + '&role='+role;
            }
            window.location.href=url_;
        })

        
    </script>
    <script>
        $( document ).ready(function() {
            var url_string = location.href;
            var url = new URL(url_string);
            var role = url.searchParams.get("role");
            if(role != null){
                $('.filter_role').val(role);
            }
             
         });
     </script>
@endpush
   