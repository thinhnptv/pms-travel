@extends('base::layouts.master')
@section('css')
        <link href="{{ URL::asset('assets/css/webt.css')}}" rel="stylesheet" type="text/css" />
@endsection
@section('content')
    <!-- Start Content-->
    <div class="container-fluid">
        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">UBold</a></li>
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Apps</a></li>
                            <li class="breadcrumb-item active">List Tours</li>
                        </ol>
                    </div>
                    <h4 class="page-title">List Tours</h4>
                </div>
            </div>
        </div>
        @if (session('success'))
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
        @endif
        <!-- end page title -->
    <div class="row">
        <div class="col-4">
            <input type="date" name="" id="">
        </div>
        <div class="col-8">
            <div class="card p-2">
                <div class="row mb-2">
                    <div class="col-sm-4">
                        
                    </div>
                    <div class="col-sm-8">
                        <div class="text-sm-right">
                            
                            <a href="javascript:void(0)" class="btn btn-danger waves-effect waves-light mb-2 delete_all" ><i class="mdi mdi-delete mr-1"></i> Delete All</a>
                            
                        </div>
                    </div><!-- end col-->
                </div>
                <div class="table-responsive">
                    <table class="table table-bordered  mb-0">
                        <thead>
                            <tr>
                                <th width="50px"><input type="checkbox" id="master" class="check_master"></th>
                                <th class="text-center">ID</th>
                                <th class="text-center">Date</th>
                                <th class="text-center">#</th>
                                <th class="text-center">#</th>
                            </tr>
                        </thead>
                        <tbody>
                           
                            @foreach ($listAvailability as $item)
                            
                            <tr>
                                <td><input type="checkbox" class="sub-chk" data-id="{{ $item->id }}"></td>
                                <th class="text-center" scope="row">{{$item->id}}</th>
                                <td>{{ $item->start_date }}</td>
                                <td class="text-center">
                                    <a href="" class="btn btn-info">Edit</a>
                                </td>
                                <td class="text-center">
                                    <a href="" class="btn btn-danger" onclick="return confirm('Bạn chắc chắn muốn xoá?')">Delete</a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div class="mt-2">
                        {{ $listAvailability->render() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('js')
   
    
    <script>
        
        
    </script>
    
@endpush
