@extends('base::layouts.master')
@section('css')
        <!-- Plugins css-->
        <link href="{{ URL::asset('assets/libs/select2/select2.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ URL::asset('assets/libs/summernote/summernote.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ URL::asset('assets/libs/dropzone/dropzone.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ URL::asset('assets/css/webt.css')}}" rel="stylesheet" type="text/css" />
@endsection
@section('content')
    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">UBold</a></li>
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Apps</a></li>
                            <li class="breadcrumb-item active">List Tours</li>
                        </ol>
                    </div>
                    <h4 class="page-title">List Tours</h4>
                </div>
            </div>
        </div>
        <div class="notification">
            @if (session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif
        </div>
        
        <!-- end page title -->
    <div class="row">
        <div class="col-12">
            <div class="card p-2">
                <div class="row mb-2">
                    <div class="col-sm-4">
                        
                    </div>
                    <div class="col-sm-8">
                        <div class="text-sm-right">
                            
                            <a href="javascript:void(0)" class="btn btn-danger waves-effect waves-light mb-2 delete_all" ><i class="mdi mdi-delete mr-1"></i> Delete All</a>
                            <a href="{{ route('tour.get_add') }}" class="btn btn-primary waves-effect waves-light mb-2" ><i class="mdi mdi-plus-circle mr-1"></i> Add</a>
                        </div>
                    </div><!-- end col-->
                </div>
                <div class="table-responsive">
                    <table class="table table-bordered  mb-0">
                        <thead>
                            <tr>
                                <th width="50px"><input type="checkbox" id="master" class="check_master"></th>
                                <th class="text-center">STT</th>
                                <th class="text-center">Name</th>
                                <th class="text-center">Location</th>
                                <th class="text-center">Author</th>
                                <th class="text-center">Status</th>
                                <th class="text-center">Reviews</th>
                                <th class="text-center">Date</th>
                                <th class="text-center">#</th>
                                <th class="text-center">#</th>
                                <th class="text-center">#</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                                $stt = ($list_tour->perPage() * ($list_tour->currentPage() - 1)) + 1;
                            @endphp
                            @foreach ($list_tour as $item)
                                <tr>
                                    <td><input type="checkbox" class="sub-chk" data-id="{{ $item->id }}"></td>
                                    <th class="text-center" scope="row">{{ $stt }}</th>
                                    <td>{{ $item->title }}</td>
                                    <td>{{ optional($item->location)->name }}</td>
                                    <td>{{ optional($item->user)->firstname }} {{ optional($item->user)->lastname }}</td>
                                    @if ($item->status == 1)
                                        <td class="text-center"><a href="" class="btn btn-primary status_tour" data-url="{{ route('tour.change_status', $item->id) }}" data-id="{{ $item->id }}">Publish</a></td>
                                    @else
                                        <td class="text-center"><a href="" class="btn btn-danger status_tour" data-url="{{ route('tour.change_status', $item->id) }}" data-id="{{ $item->id }}">Blocked</a></td>
                                    @endif
                                    <td class="text-center">
                                        <a target="_blank" href="{{ route('tour.review.list_for_tour', $item->id) }}" class="review-count-approved">{{ sizeof($item->reviewTour) }}</a>
                                    </td>
                                    <td>{{ $item->created_at }}</td>
                                    <td class="text-center">
                                        <a href="{{ route('tour.get_edit', $item->id) }}" class="btn btn-info">Edit</a>
                                    </td>
                                    <td class="text-center">
                                        <a href="{{ route('tour.delete', $item->id) }}" class="btn btn-danger" onclick="return confirm('Bạn chắc chắn muốn xoá?')">Delete</a>
                                    </td>
                                    <td class="text-center">
                                        <a href="{{ route('tour.list_availability', $item->id) }}" class="btn btn-success" >Availability</a>
                                    </td>
                                </tr>
                                @php
                                    $stt++;
                                @endphp
                            @endforeach
                        </tbody>
                    </table>
                    <div class="mt-2 text-center">
                        {{ $list_tour->render() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('js')
    <!-- Summernote js -->
    <script src="{{ URL::asset('assets/libs/summernote/summernote.min.js') }}"></script>
    <!-- Select2 js-->
    <script src="{{ URL::asset('assets/libs/select2/select2.min.js') }}"></script>
    <!-- Dropzone file uploads-->
    <script src="{{ URL::asset('assets/libs/dropzone/dropzone.min.js') }}"></script>
    <!-- Init js -->
    <script src="{{ URL::asset('assets/js/webt.js') }}"></script>
    
    <script>
        $( document ).ready(function() {
            var checkNotification =  $('.notification').find('.alert-success').length;
            if(checkNotification != 0) {
                    setTimeout(function(){
                        $('.notification').empty();
                    }, 3000);
            }
        });
         $(document).on('click', '.check_master', function(e) {
            if($(this).is(':checked', true))
            {
                $(".sub-chk").prop('checked', true);
            } else {
                $(".sub-chk").prop('checked', false);
            }
        });
        $(document).on('click','.delete_all',function(e) {
            const __this = this;
            $(__this).prop('disabled', true);
            var list_id = [];
            $('.sub-chk:checked').each(function () {
                var sub_id =parseInt($(this).attr('data-id'));
                list_id.push(sub_id);
            });
            
            if(list_id.length == 0) {
                toastr.error("Vui lòng chọn hàng cần xóa");
            }else {
                var check_sure = confirm("Bạn chắc chắn muốn xóa?");
                if(check_sure == true){
                    const __token = $('meta[name="csrf-token"]').attr('content');
                    data_ = {
                        list_id: list_id,
                        _token: __token
                    }
                    var request = $.ajax({
                        url: '{{route('tour.delete_all')}}',
                        type: 'POST',
                        data: data_,
                        dataType: "json"
                    });
                    request.done(function (msg) {
                        if (msg.type == 1) {
                            // $('.alert-success').remove();
                            $('.sub-chk:checked').each(function () {
                                $(this).parents("tr").remove();
                            });
                            $(__this).prop('disabled', false);
                            toastr.success(msg.mess);
                        }
                        return false;
                    });
                    request.fail(function (jqXHR, textStatus) {
                        alert("Không thể gửi yêu cầu mã lỗi : " + textStatus);
                    });
                }
            }
        });
        $(document).on('click','.status_tour',function(e) {
            e.preventDefault();
            toastr.clear();
            toastr.options = {
                "closeButton": true,
                "timeOut": "5000",
                "positionClass": "toast-top-right"
            }
            const __this = this;
            $(__this).prop('disabled', true);
            var id = $(__this).attr('data-id');
            const __token = $('meta[name="csrf-token"]').attr('content');
            data_ = {
                _token: __token
            }
            var url_ = $(__this).attr('data-url');
            var request = $.ajax({
                url: url_,
                type: 'POST',
                data: data_,
                dataType: "json"
            });
            request.done(function (msg) {
                if (msg.type == 1) {
                    $('.alert-success').remove();
                    $(__this).prop('disabled', false);
                    $(__this).html('Blocked');
                    $(__this).removeClass('btn-primary');
                    $(__this).addClass('btn-danger');
                    toastr.success(msg.mess);
                }else{
                    $('.alert-success').remove();
                    $(__this).prop('disabled', false);
                    $(__this).html('Publish');
                    $(__this).removeClass('btn-danger');
                    $(__this).addClass('btn-primary');
                    toastr.success(msg.mess);
                }
                return false;
            });
            request.fail(function (jqXHR, textStatus) {
                alert("Không thể gửi yêu cầu mã lỗi : " + textStatus);
            });
            
        });
        
    </script>
    
@endpush
