@extends('base::layouts.master')
@section('css')
        <!-- Plugins css-->
        <link href="{{ URL::asset('assets/libs/select2/select2.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ URL::asset('assets/libs/summernote/summernote.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ URL::asset('assets/libs/dropzone/dropzone.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ URL::asset('assets/css/webt.css')}}" rel="stylesheet" type="text/css" />
@endsection
@section('content')
    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">UBold</a></li>
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Apps</a></li>
                            <li class="breadcrumb-item active">Edit Tour</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Edit Tour</h4>
                </div>
            </div>
        </div>
       
        <!-- end page title -->
        <form action="" method="post" enctype="multipart/form-data">
            <input type="hidden" name="_token" value="{{csrf_token()}}" /> 
            <div class="row">
                <div class="col-md-12 col-lg-9">
                    <div class="card p-2">
                        <div class="row">
                            <div class="col-md-12 col-lg-12">
                                <div class="form-group mb-3">
                                    <label for="title">Title <span class="text-danger">*</span></label>
                                    <input type="text" id="title" name="title" class="form-control" placeholder="Please enter name of the car" value="{{ $tourEdit->title }}">
                                    <p class="help is-danger">{{ $errors->first('title') }}</p>
                                </div> 
                                <div class="form-group mb-3">
                                    <label for="content">Content</label>
                                    <textarea class="form-control" id="content" name="content" rows="5" placeholder="Please enter content">{{ $tourEdit->content }}</textarea>
                                </div>
                                <div class="form-group mb-3">
                                    <label for="description">Description</label>
                                    <textarea class="form-control" id="description" name="description" rows="5" placeholder="Please enter description">{{ $tourEdit->description }}</textarea>
                                </div>
                                <div class="form-group mb-3">
                                    <label for="category_id">Category</label>
                                    <select type="text" id="category_id" name="category_id" class="form-control cate">
                                        {!! $tourCategories !!}}
                                    </select>
                                </div>
                                <div class="form-group mb-3">
                                    <label for="video">Youtube Video <span class="text-danger">*</span></label>
                                    <input type="text" id="video" name="video" class="form-control" placeholder="Please enter Youtobe link video" value="{{ $tourEdit->video }}">
                                    <p class="help is-danger">{{ $errors->first('video') }}</p>
                                </div> 
                                <div class="form-group mb-3">
                                    <label for="duration">Duration <span class="text-danger">*</span></label>
                                    <div class="row">
                                        <div class="col-6">
                                            <input type="number" id="duration" name="duration" min="1" class="form-control" placeholder="Please enter duration" value="{{ $tourEdit->duration }}">
                                        </div> 
                                        <div class="col-6">
                                            <h4>ngày</h4>
                                        </div>
                                    </div>
                                    <p class="help is-danger">{{ $errors->first('duration') }}</p>
                                </div> 
                                <div class="row">
                                    <div class="col-6">
                                        <div class="form-group mb-3">
                                            <label for="min_people">Tour Min People</label>
                                            <input type="number" id="min_people" min="0" name="min_people" class="form-control" placeholder="Please enter min_people" value="{{ $tourEdit->min_people }}">
                                            <p class="help is-danger">{{ $errors->first('min_people') }}</p>
                                        </div> 
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group mb-3">
                                            <label for="max_people">Tour Max People <span class="text-danger">*</span></label>
                                            <input type="number" id="max_people" min="0" name="max_people" class="form-control" placeholder="Please enter max_people" value="{{ $tourEdit->max_people }}">
                                            <p class="help is-danger">{{ $errors->first('max_people') }}</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group form-group-item mb-3">
                                    <label class="control-label">FAQs</label>
                                    <div class="g-items-header">
                                        <div class="row">
                                            <div class="col-md-5">Title</label></div>
                                            <div class="col-md-5">Content</label></div>
                                            <div class="col-md-1"></div>
                                        </div>
                                    </div>
                                    <div class="g-items g-items-faq">
                                        @php
                                            $i = 0;
                                        @endphp
                                        @if(!empty($tourEdit->faq))
                                            @foreach($tourEdit->faq as $item)
                                                <div class="item" data-number="{{ $i }}">
                                                    <div class="row">
                                                        <div class="col-md-5">
                                                            <input type="text" name="faqs[{{ $i }}][title]" class="form-control" placeholder="Eg: Can I bring my pet?" required value="{{ $item->title }}" required>
                                                            </div>
                                                        <div class="col-md-6">
                                                            <textarea name="faqs[{{ $i }}][content]" class="form-control" placeholder="" required>{{ $item->content }}</textarea>
                                                        </div>
                                                        <div class="col-md-1">
                                                            <span class="btn btn-danger btn-sm btn-remove-item"><i class="fa fa-trash"></i></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                @php
                                                    $i++;
                                                @endphp
                                            @endforeach
                                        @endif
                                    </div>
                                    <div class="text-right">
                                        <span class="btn btn-info btn-sm btn-add-item btn-add-item-faq"><i class="fe-plus-circle"></i> Add item</span>
                                    </div>
                                </div>
                                <div class="form-group form-group-item mb-3">
                                    <label class="control-label">Include</label>
                                    <div class="g-items-header">
                                        <div class="row">
                                            <div class="col-md-11">Title</label></div>
                                            <div class="col-md-1"></div>
                                        </div>
                                    </div>
                                    <div class="g-items g-items-include">
                                        @php
                                            $j = 0;
                                        @endphp
                                        @if(!empty($tourEdit->include))
                                            @foreach($tourEdit->include as $item)
                                                <div class="item" data-number="{{ $j }}">
                                                    <div class="row">
                                                        <div class="col-md-11">
                                                            <input type="text" name="include[{{ $j }}][title]" class="form-control" placeholder="Eg: Specialized bilingual guide" required value="{{ $item->title }}" required>
                                                        </div>
                                                        <div class="col-md-1">
                                                            <span class="btn btn-danger btn-sm btn-remove-item"><i class="fa fa-trash"></i></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                @php
                                                    $j++;
                                                @endphp
                                            @endforeach
                                        @endif
                                    </div>
                                    <div class="text-right">
                                        <span class="btn btn-info btn-sm btn-add-item btn-add-item-include"><i class="fe-plus-circle"></i> Add item</span>
                                    </div>
                                </div>
                                <div class="form-group form-group-item mb-3">
                                    <label class="control-label">Note</label>
                                    <div class="g-items-header">
                                        <div class="row">
                                            <div class="col-md-11">Title</label></div>
                                            <div class="col-md-1"></div>
                                        </div>
                                    </div>
                                    <div class="g-items g-items-exclude">
                                        @php
                                            $k = 0;
                                        @endphp
                                        @if(!empty($tourEdit->exclude))
                                            @foreach($tourEdit->exclude as $item)
                                                <div class="item" data-number="{{ $k }}">
                                                    <div class="row">
                                                        <div class="col-md-11">
                                                            <input type="text" name="exclude[{{ $k }}][title]" class="form-control" placeholder="Eg: Specialized bilingual guide" required value="{{ $item->title }}" required>
                                                        </div>
                                                        <div class="col-md-1">
                                                            <span class="btn btn-danger btn-sm btn-remove-item"><i class="fa fa-trash"></i></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                @php
                                                    $k++;
                                                @endphp
                                            @endforeach
                                        @endif
                                    </div>
                                    <div class="text-right">
                                        <span class="btn btn-info btn-sm btn-add-item btn-add-item-exclude"><i class="fe-plus-circle"></i> Add item</span>
                                    </div>
                                </div> 
                                <div class="form-group form-group-item mb-3">
                                    <label class="control-label">Itinerary</label>
                                    <div class="g-items-header">
                                        <div class="row">
                                            <div class="col-md-2">Image</label></div>
                                            <div class="col-md-4">Title - Desc</label></div>
                                            <div class="col-md-5">Content</label></div>
                                            <div class="col-md-1"></div>
                                        </div>
                                    </div>
                                    <div class="g-items g-items-itinerary">
                                        @php
                                            $q = 0;
                                        @endphp
                                        @if(!empty($tourEdit->itinerary))
                                            @foreach($tourEdit->itinerary as $item)
                                                <div class="item" data-number="{{ $q }}">
                                                    <div class="row">
                                                        <div class="col-md-2 col-itinerary-image">
                                                            <span class="input-group-btn">
                                                                @if(!empty($item->image))
                                                                    <button type="button" class="btn btn-primary btn-show-modal-choose-itinerary d-none" data-toggle="modal" data-target="#modal-itinerary-image-{{ $q }}">Select image</button>
                                                                    <input type="hidden" class="itinerary-image" name="itinerary[{{ $q }}][image]" id="itinerary-image-{{ $q }}" value="{{ config('app.url') }}{{ $item->image }}" required>
                                                                @else
                                                                    <button type="button" class="btn btn-primary btn-show-modal-choose-itinerary" data-toggle="modal" data-target="#modal-itinerary-image-{{ $q }}">Select image</button>
                                                                    <input type="hidden" class="itinerary-image" name="itinerary[{{ $q }}][image]" id="itinerary-image-{{ $q }}" required>
                                                                @endif
                                                            </span>
                                                            <div id="preview_itinerary-image-{{ $q }}" class="mt-3 show-itinerary-image">
                                                                @if(!empty($item->image))
                                                                    <div class="box_imgg position-relative">
                                                                        <img src="{{ config('app.url') }}{{ $item->image }}"  style="width:100%;">
                                                                        <i class="fas fa-times-circle style_icon_remove style_icon_remove_itinerary" title="delete"></i>
                                                                    </div>
                                                                @endif
                                                            </div>
                                                            <div class="modal fade test modal-itinerary-image" id="modal-itinerary-image-{{ $q }}" tabindex="-1" role="dialog" data-backdrop="false" aria-hidden="true">
                                                                <div class="modal-dialog modal-lg">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <h5 class="modal-title" id="exampleModalLabel">Manger Image</h5>
                                                                            <button type="button" class="close" data-dismiss="modal"><i class="fas fa-times"></i></button>
                                                                        </div>
                                                                        <div class="modal-body">
                                                                            <iframe src="{{ config('app.url') }}/filemanager/dialog.php?akey=4Ans6pfTs6A9DzHj8AZq1NVGKoYxpyoI8ivCN1bOow&field_id=itinerary-image-{{ $q }}&multiple=0" frameborder="0" style="width: 100%; height: 500px; overflow-y: auto"></iframe>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4 flex-column">
                                                            <input type="text" name="itinerary[{{ $q }}][title]" class="form-control" placeholder="Title: Day 1" required value="{{ $item->title }}">
                                                            <input type="text" name="itinerary[{{ $q }}][desc]" class="form-control" placeholder="Desc: TP. HCM City" required value="{{ $item->description }}">
                                                        </div>
                                                        <div class="col-md-5">
                                                            <textarea name="itinerary[{{ $q }}][content]" class="form-control full-h" placeholder="..." required>{{ $item->content }}</textarea>
                                                        </div>
                                                        <div class="col-md-1">
                                                            <span class="btn btn-danger btn-sm btn-remove-item"><i class="fa fa-trash"></i></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                @php
                                                    $q++;
                                                @endphp
                                            @endforeach
                                        @endif
                                    </div>
                                    <div class="text-right">
                                        <span class="btn btn-info btn-sm btn-add-item btn-add-item-itinerary"><i class="fe-plus-circle"></i> Add item</span>
                                    </div>
                                </div> 
                               
                                <div class="form-group mb-3">
                                    <label for="banner">Album  <span class="text-danger">*</span></label>
                                    <div class="row">
                                        <div class="col-3">
                                            @php
                                                $str_album_value = '';
                                            @endphp
                                            @if(!empty($tourEdit->album))
                                                @php
                                                    $arr_album_selected = json_decode($tourEdit->album, true);
                                                    $arr_album_value = [];
                                                @endphp
                                                @foreach($arr_album_selected as $item)
                                                    @php
                                                        $arr_album_value[] = config('app.url').$item ;
                                                    @endphp
                                                @endforeach
                                                @php
                                                    $str_album_value .= '["';
                                                    $str_album_value .= implode ('","', $arr_album_value);
                                                    $str_album_value .='"]';
                                                @endphp
                                            @endif
                                            <span class="input-group-btn">
                                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bd-example-modal-album">Select album</button>
                                                <input type="hidden" name="album" id="album" value="{{ $str_album_value }}">
                                                <p class="help is-danger">{{ $errors->first('album') }}</p>
                                            </span>
                                        </div>
                                    </div>
                                    <div id="preview_album" class="mt-3">
                                        <div class="row row_preview_album" id="row_preview_album">
                                            @if(!empty($tourEdit->album))
                                                @foreach ($arr_album_selected as $item)
                                                    <div class="col-3 mt-3">
                                                        <div class="box_imgg position-relative">
                                                            <img src="{{ config('app.url') }}{{ $item }}" class="show-img img-height-110" style="width:100%; height=110px;">
                                                            <i class="fas fa-times-circle style_icon_remove style_icons_remove_album" title="delete"></i>
                                                        </div>
                                                    </div>
                                                @endforeach
                                            @endif
                                        </div>
                                    </div>
                                </div> 
                                <div class="form-group mb-3">
                                    <label for="location_id">Location</label>
                                    <select class="form-control" id="location_id" name="location_id">
                                        @foreach ($location as $item)
                                            @if($item->id == $tourEdit->location_id)
                                                <option value="{{ $item->id }}" selected="selected">{{ $item->name }}</option>
                                            @else
                                                <option value="{{ $item->id }}">{{ $item->name }}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                    <p class="help is-danger">{{ $errors->first('location_id') }}</p>
                                </div> 
                                <div class="form-group mb-3">
                                    <label for="destination_id">Destination</label>
                                    <select class="form-control" id="destination_id" name="destination_id">
                                        @foreach ($destination as $item)
                                            @if($item->id == $tourEdit->destination_id)
                                                <option value="{{ $item->id }}" selected="selected">{{ $item->name }}</option>
                                            @else
                                                <option value="{{ $item->id }}">{{ $item->name }}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                    <p class="help is-danger">{{ $errors->first('destination_id') }}</p>
                                </div> 
                                <div class="form-group mb-3">
                                    <label for="address">Address  <span class="text-danger">*</span></label>
                                    <input type="text" id="address" name="address" class="form-control" placeholder="Please enter address"  value="{{ $tourEdit->address }}">
                                    <p class="help is-danger">{{ $errors->first('address') }}</p>
                                </div> 
                                <h4 class="header-title mb-3">The geographic coordinate</h4>
                                <div class="row">
                                    <div class="col-8">
                                        <div class="form-group mb-3">
                                            <div id="gmaps-basic" class="gmaps"></div>
                                        </div>
                                    </div>
                                    <div class="col-4 ">
                                        <div class="form-group mb-3">
                                            <label for="latitu">Map Latitude: <span class="text-danger">*</span></label>
                                            <input type="text" id="latitu" name="latitu" class="form-control" value="{{ $tourEdit->latitu }}">
                                            <p class="help is-danger">{{ $errors->first('latitu') }}</p>
                                        </div>
                                        <div class="form-group mb-3">
                                            <label for="longtitu">Map Longitude: <span class="text-danger">*</span></label>
                                            <input type="text" id="longtitu" name="longtitu" class="form-control" value="{{ $tourEdit->longtitu }}">
                                            <p class="help is-danger">{{ $errors->first('longtitu') }}</p>
                                        </div>
                                        <div class="form-group mb-3">
                                            <label for="mapzoom">Map Zoom: </label>
                                            <input type="text" id="mapzoom" name="mapzoom" class="form-control" value="{{ $tourEdit->mapzoom }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group mb-3">
                                    <div class="row">
                                        <div class="col-6">
                                            <label for="price">Price  <span class="text-danger">*</span></label>
                                            <input type="number" id="price" min="0" name="price" class="form-control" placeholder="Car Price" value="{{ $tourEdit->price }}">
                                            <p class="help is-danger">{{ $errors->first('price') }}</p>
                                        </div>
                                        <div class="col-6">
                                            <label for="sale">Sale</label>
                                            <input type="number" id="sale" name="sale" class="form-control" placeholder="Car Sale" value="{{ $tourEdit->sale }}">
                                            <span><i>If the regular price is less than the discount , it will show the regular price</i></span>
                                            <p class="help is-danger">{{ $errors->first('sale') }}</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group mb-3">
                                    <label class="control-label">Is Person Types</label><br />
                                    <label class="mb-3">
                                        @if($tourEdit->is_person_type == 1)
                                            <input type="checkbox" class="enable_person_types" name="enable_person_types" value="1" checked> Enable Person Types
                                        @elseif($tourEdit->is_person_type == 0)
                                            <input type="checkbox" class="enable_person_types" name="enable_person_types" value="1"> Enable Person Types
                                        @endif
                                    </label>
                                </div>
                                
                                @if($tourEdit->is_person_type == 1)
                                    <div class="form-group form-group-item mb-3 table-person-type d-block">
                                @else
                                    <div class="form-group form-group-item mb-3 table-person-type d-none">
                                @endif
                                    <label class="control-label">Person Types</label>
                                    <div class="g-items-header">
                                        <div class="row">
                                            <div class="col-md-5">Person Type</div>
                                            <div class="col-md-2">Min</div>
                                            <div class="col-md-2">Max</div>
                                            <div class="col-md-2">Price</div>
                                            <div class="col-md-1"></div>
                                        </div>
                                    </div>
                                    <div class="g-items g-items-person-types">
                                        @php
                                            $l = 0;
                                        @endphp
                                        @if(!$tourEdit->personType->isEmpty() && $tourEdit->is_person_type == 1)
                                            @foreach($tourEdit->personType as $item)
                                                <div class="item" data-number="{{ $l }}">
                                                    <div class="row">
                                                        <div class="col-md-5 flex-column">
                                                            <input type="text" name="person_types[{{ $l }}][title]" class="form-control" placeholder="Eg: Adults" value="{{ $item->title }}">
                                                            <input type="text" name="person_types[{{ $l }}][desc]" class="form-control" placeholder="Description" value="{{ $item->description }}">
                                                        </div>
                                                        <div class="col-md-2">
                                                            <input type="number" min="0" name="person_types[{{ $l }}][min]" class="form-control" placeholder="Minimum per booking" value="{{ $item->min }}">
                                                        </div>
                                                        <div class="col-md-2">
                                                            <input type="number" min="0" name="person_types[{{ $l }}][max]" class="form-control" placeholder="Maximum per booking" value="{{ $item->max }}"> 
                                                        </div>
                                                        <div class="col-md-2">
                                                            <input type="number" min="0" name="person_types[{{ $l }}][price]" class="form-control" placeholder="per 1 item" value="{{ $item->price }}">
                                                        </div>
                                                        <div class="col-md-1">
                                                            <span class="btn btn-danger btn-sm btn-remove-item"><i class="fa fa-trash"></i></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                @php
                                                    $l++;
                                                @endphp
                                            @endforeach
                                        @endif
                                    </div>
                                    <div class="text-right">
                                        <span class="btn btn-info btn-sm btn-add-item btn-add-item-person-types"><i class="fe-plus-circle"></i> Add item</span>
                                    </div>
                                </div>
                                <div class="form-group form-group-item mb-3">
                                    <label class="control-label">Extra Price</label>
                                    <div class="g-items-header">
                                        <div class="row">
                                            <div class="col-md-5">Title</label></div>
                                            <div class="col-md-3">Price</label></div>
                                            <div class="col-md-3">Type</label></div>
                                            <div class="col-md-1"></div>
                                        </div>
                                    </div>
                                    <div class="g-items g-items-extra-price">
                                        @php
                                            $m = 0;
                                        @endphp
                                        @if($tourEdit->extraPrice->isEmpty())
                                            @foreach($tourEdit->extraPrice as $item)
                                                <div class="item" data-number="{{ $m }}">
                                                    <div class="row">
                                                        <div class="col-md-5">
                                                            <input type="text" name="extra_price[{{ $m }}][title]" class="form-control" value="{{ $item->title }}" placeholder="Extra price name" required>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <input type="number" min="0" name="extra_price[{{ $m }}][price]" class="form-control" value="{{ $item->price }}" required>
                                                        </div>
                                                        <div class="col-md-3 flex-column">
                                                            <select name="extra_price[{{ $m }}][type]" class="form-control">
                                                                @if($item->type == 0)
                                                                    <option value="0" selected>One-time</option>
                                                                    <option value="1">Per hour</option>
                                                                    <option value="2">Per day</option>
                                                                @elseif($item->type == 1)
                                                                    <option value="0">One-time</option>
                                                                    <option value="1" selected>Per hour</option>
                                                                    <option value="2">Per day</option>
                                                                @elseif($item->type == 2)
                                                                    <option value="0">One-time</option>
                                                                    <option value="1" selected>Per hour</option>
                                                                    <option value="2">Per day</option>
                                                                @endif
                                                            </select>
                                                            <label>
                                                                <input type="checkbox" name="extra_price[{{ $m }}][per_person]" value="1">
                                                            Price per person
                                                            </label>
                                                        </div>
                                                        <div class="col-md-1">
                                                            <span class="btn btn-danger btn-sm btn-remove-item"><i class="fa fa-trash"></i></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                @php
                                                    $m++;
                                                @endphp
                                            @endforeach
                                        @endif
                                    </div>
                                    <div class="text-right">
                                        <span class="btn btn-info btn-sm btn-add-item btn-add-item-extra-price"><i class="fe-plus-circle"></i> Add item</span>
                                    </div>
                                </div>
                                <div class="form-group form-group-item mb-3">
                                    <label class="control-label">Discount by number of people</label>
                                    <div class="g-items-header">
                                        <div class="row">
                                            <div class="col-md-4">No of people</label></div>
                                            <div class="col-md-4">Discount</label></div>
                                            <div class="col-md-3">Type</label></div>
                                            <div class="col-md-1"></div>
                                        </div>
                                    </div>
                                    <div class="g-items g-items-discount">
                                        @php
                                            $n = 0;
                                        @endphp
                                        @if($tourEdit->discountOfPeople->isEmpty())
                                            @foreach($tourEdit->discountOfPeople as $item)
                                                <div class="item" data-number="{{ $n }}">
                                                    <div class="row">
                                                        <div class="col-md-2">
                                                            <input type="number" min="0" name="discount_by_people[{{ $n }}][from]" class="form-control" value="{{ $item->from }}" placeholder="From" required>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <input type="number" min="0" name="discount_by_people[{{ $n }}][to]" class="form-control" value="{{ $item->to }}" placeholder="To" required>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <input type="number" min="0" name="discount_by_people[{{ $n }}][amount]" class="form-control" value="{{ $item->amount }}" required>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <select name="discount_by_people[{{ $n }}][type]" class="form-control">
                                                                @if($item->type == 0)
                                                                    <option value="0" selected>Fixed</option>
                                                                    <option value="1">Percent</option>
                                                                @elseif($item->type == 1)
                                                                    <option value="0" >Fixed</option>
                                                                    <option value="1" selected>Percent</option>
                                                                @endif
                                                            </select>
                                                        </div>
                                                        <div class="col-md-1">
                                                            <span class="btn btn-danger btn-sm btn-remove-item"><i class="fa fa-trash"></i></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                @php
                                                    $n++;
                                                @endphp
                                            @endforeach
                                        @endif
                                    </div>
                                    <div class="text-right">
                                        <span class="btn btn-info btn-sm btn-add-item btn-add-item-discount"><i class="fe-plus-circle"></i> Add item</span>
                                    </div>
                                </div> 
                                <div class="form-group mb-3">
                                    <label for="">Open Hours</label> <br>
                                    <label class="mb-3">
                                        @if($tourEdit->is_open_hour == 1)
                                            <input type="checkbox" class="enable_open_hours" name="enable_open_hours" value="1" checked> Enable Open Hours
                                        @else
                                            <input type="checkbox" class="enable_open_hours" name="enable_open_hours" value="1"> Enable Open Hours
                                        @endif
                                    </label>
                                </div> 
                                    @if($tourEdit->is_open_hour == 1)
                                    <div class="table-responsive table-enable_open_hours form-group" >
                                    @else
                                    <div class="table-responsive table-enable_open_hours form-group d-none" >
                                    @endif
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th>Enable?</th>
                                                <th>Day of Week</th>
                                                <th>Open</th>
                                                <th>Close</th>
                                            </tr>
                                        </thead>
                                       <tbody>
                                            @if(!$tourEdit->openHour->isEmpty())
                                                @foreach ($tourEdit->openHour as $item)
                                                    <tr>
                                                        <td>
                                                            @if ($item->status == 0)
                                                                <input style="display: inline-block" type="checkbox" name="open_hours[{{ $item->day_of_week }}][enable]" value="1">
                                                            @else
                                                                <input style="display: inline-block" type="checkbox" name="open_hours[{{ $item->day_of_week }}][enable]" value="1" checked>
                                                            @endif
                                                            
                                                        </td>
                                                        <td><strong>
                                                            {!! day_of_week($item->day_of_week) !!}
                                                            </strong>
                                                        </td>
                                                        <td>
                                                            <select class="form-control" name="open_hours[{{ $item->day_of_week }}][from]">
                                                                {!! hour_of_day($item->from) !!}
                                                            </select>
                                                        </td>
                                                        <td>
                                                            <select class="form-control" name="open_hours[{{ $item->day_of_week }}][to]">
                                                                {!! hour_of_day($item->to) !!}
                                                            </select>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            @else
                                                @for ($i_ = 0; $i_ <= 6; $i_++)
                                                    <tr>
                                                        <td>
                                                            <input style="display: inline-block" type="checkbox" name="open_hours[{{ $i_ }}][enable]" value="1">
                                                        </td>
                                                        <td>
                                                            <strong>
                                                            {!! day_of_week($i_) !!}
                                                            </strong>
                                                        </td>
                                                        <td>
                                                            <select class="form-control" name="open_hours[{{ $i_ }}][from]">
                                                                {!! hour_of_day() !!}
                                                            </select>
                                                        </td>
                                                        <td>
                                                            <select class="form-control" name="open_hours[{{ $i_ }}][to]">
                                                                {!! hour_of_day() !!}
                                                            </select>
                                                        </td>
                                                    </tr>
                                                @endfor
                                            @endif
                                       </tbody>
                                    </table>
                                 </div>
                                <div class="form-group mb-3">
                                    <label for="seo_title">Seo Title</label> <br>
                                    <input type="text" id="seo_title" name="seo_title" class="form-control" placeholder="Please enter seo_title" value="{{ $tourEdit->seo_title }}">
                                </div>
                                <div class="form-group mb-3">
                                    <label for="seo_description">Seo Description</label>
                                    <input type="text" id="seo_description" name="seo_description" class="form-control" placeholder="Please enter seo_description" value="{{ $tourEdit->seo_description }}">
                                </div>
                            </div>
                        </div>     
                    </div>
                </div>
                <div class="col-md-12 col-lg-3">
                    <div class="card p-2">
                        <div class="card-box text-center">
                            <label for="">Avatar <span class="text-danger">*</span></label> <br/>
                            <span class="input-group-btn">
                                <button type="button" class="btn btn-warning" data-toggle="modal" data-target=".bd-example-modal-avatar">Select avatar</button>
                                @if(!empty($tourEdit->thumnail))
                                    <input type="hidden" name="avatar" id="avatar" value="{{ config('app.url') }}{{ $tourEdit->thumnail }}">
                                @else
                                    <input type="hidden" name="avatar" id="avatar" value="">
                                @endif
                                <p class="help is-danger">{{ $errors->first('avatar') }}</p>
                            </span>
                            <div id="preview_avatar" class="mt-3">
                                @if(!empty($tourEdit->thumnail))
                                    <div class="box_imgg position-relative">
                                        <img src="{{ config('app.url') }}{{ $tourEdit->thumnail }}" id='show-img-avatar' style="width:100%;">
                                        <i class="fas fa-times-circle style_icon_remove style_icon_remove_avatar" title="delete"></i>
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="form-group mb-3">
                            <label for="status_car">Status</label>
                            <select class="form-control" id="status_car" name="status">
                                @if($tourEdit->status == 0)
                                    <option value="0" selected>Blocked</option>
                                    <option value="1">Publish</option>
                                @elseif($tourEdit->status == 1)
                                    <option value="0">Blocked</option>
                                    <option value="1" selected>Publish</option>
                                @endif
                            </select>
                        </div> 
                        <div class="form-group mb-3">
                            <label for="vendor_id">Vendor</label>
                            <select class="form-control" id="vendor_id" name="vendor_id">
                                <option value="">--Select User--</option>
                                @foreach ($vendor as $item)
                                    @if($item->id == $tourEdit->vendor_id)
                                        <option value="{{ $item->id }}" selected>{{ $item->firstname }} {{ $item->lastname }}</option>
                                    @else
                                        <option value="{{ $item->id }}">{{ $item->firstname }} {{ $item->lastname }}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group mb-3">
                            <label >Tour Featured</label>
                            <br />
                            <label>
                                @if ($tourEdit->is_featured == 1)
                                    <input type="checkbox" name="featured" value="1" checked> Enable featured
                                @else
                                    <input type="checkbox" name="featured" value="1"> Enable featured
                                @endif
                            </label>
                        </div>
                        <div class="form-group mb-3">
                            <label >Tour Sale Holiday</label>
                            <br />
                            <label>
                                @if ($tourEdit->sale_holiday == 1)
                                    <input type="checkbox" name="sale_holiday" value="1" checked> Enable sale holiday
                                @else
                                    <input type="checkbox" name="sale_holiday" value="1"> Enable sale holiday
                                @endif
                            </label>
                        </div>
                        <div class="form-group mb-3">
                            <label for="">Attribute: Travel Styles</label>
                            <div class="terms-scrollable">
                                @foreach($tourType as $item)
                                    @if(in_array($item->id, $tour_styles_selected))
                                        <label class="term-item">
                                            <input type="checkbox" name="tourtype[]" value="{{ $item->id }}" checked>
                                            <span class="term-name">{{ $item->name }}</span>
                                        </label>
                                    @else
                                        <label class="term-item">
                                            <input type="checkbox" name="tourtype[]" value="{{ $item->id }}" >
                                            <span class="term-name">{{ $item->name }}</span>
                                        </label>
                                    @endif
                                @endforeach
                            </div>
                        </div>
                        <div class="form-group mb-3">
                            <label for="">Attribute: Facilities</label>
                            <div class="terms-scrollable">
                                @foreach ($tourFacility as $item)
                                    @if(in_array($item->id, $tour_facility_selected))
                                        <label class="term-item">
                                            <input type="checkbox" name="tourfacilities[]" value="{{ $item->id }}" checked>
                                            <span class="term-name">{{ $item->name }}</span>
                                        </label>
                                    @else
                                        <label class="term-item">
                                            <input type="checkbox" name="tourfacilities[]" value="{{ $item->id }}">
                                            <span class="term-name">{{ $item->name }}</span>
                                        </label>
                                    @endif
                                @endforeach
                            </div>
                        </div>
                        <div class="form-group mb-3">
                            <label for="">Vehicle</label>
                            <div class="terms-scrollable">
                                @foreach ($tourVehicleCategory as $item)
                                    @if(in_array($item->id, $tour_vehicle_selected))
                                        <label class="term-item">
                                            <input type="checkbox" name="vehiclecategory[]" value="{{ $item->id }}" checked>
                                            <span class="term-name">{{ $item->title }}</span>
                                        </label>
                                    @else
                                        <label class="term-item">
                                            <input type="checkbox" name="vehiclecategory[]" value="{{ $item->id }}">
                                            <span class="term-name">{{ $item->title }}</span>
                                        </label>
                                    @endif
                                @endforeach
                            </div>
                        </div>
                        <div class="form-group mb-3">
                            <label for="">Type of Tour</label>
                            <div class="terms-scrollable">
                                @foreach ($tourTypeCategory as $item)
                                    @if(in_array($item->id, $tour_type_selected))
                                        <label class="term-item">
                                            <input type="checkbox" name="typecategory[]" value="{{ $item->id }}" checked>
                                            <span class="term-name">{{ $item->title }}</span>
                                        </label>
                                    @else
                                        <label class="term-item">
                                            <input type="checkbox" name="typecategory[]" value="{{ $item->id }}">
                                            <span class="term-name">{{ $item->title }}</span>
                                        </label>
                                    @endif
                                @endforeach
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <div class="text-center mb-3">
                                    <button type="submit" class="btn w-sm btn-success waves-effect waves-light">Save</button>
                                    <a href="{{route('tour.list')}}" class="btn w-sm btn-warning waves-effect waves-light">Cancel</a>
                                    {{-- <button type="reset" class="btn w-sm btn-danger waves-effect waves-light">Reset</button> --}}
                                </div>
                            </div> <!-- end col -->
                        </div>
                    </div>
                </div>
            </div>
        <!-- end row -->
        </form>
    </div>
@section('js')
<div class="section-js">
    <div class="modal fade bd-example-modal-avatar" id="modal-file-avatar" tabindex="-1" role="dialog" data-backdrop="false" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel-avatar">Manger Image</h5>
                    <button type="button" class="close" data-dismiss="modal"><i class="fas fa-times"></i></button>
                </div>
                <div class="modal-body">
                    <iframe src="{{ config('app.url') }}/filemanager/dialog.php?akey=4Ans6pfTs6A9DzHj8AZq1NVGKoYxpyoI8ivCN1bOow&field_id=avatar&multiple=0" frameborder="0" style="width: 100%; height: 500px; overflow-y: auto"></iframe>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade bd-example-modal-album" id="modal-file-album bd-example-modal-album" tabindex="-1" role="dialog" data-backdrop="false" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel-avatar">Manger Image</h5>
                    <button type="button" class="close" data-dismiss="modal"><i class="fas fa-times"></i></button>
                </div>
                <div class="modal-body">
                    <iframe src="{{ config('app.url') }}/filemanager/dialog.php?akey=4Ans6pfTs6A9DzHj8AZq1NVGKoYxpyoI8ivCN1bOow&field_id=album&multiple=1" frameborder="0" style="width: 100%; height: 500px; overflow-y: auto"></iframe>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
@endsection

@push('js')
    <!-- Summernote js -->
    <script src="{{ URL::asset('assets/libs/summernote/summernote.min.js') }}"></script>
    <!-- Select2 js-->
    <script src="{{ URL::asset('assets/libs/select2/select2.min.js') }}"></script>
    <!-- Dropzone file uploads-->
    <script src="{{ URL::asset('assets/libs/dropzone/dropzone.min.js') }}"></script>
    <!-- Init js -->
    
    <script>
        jQuery(document).ready(function(){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        // Summernote
        $('#content').summernote({
            height: 180,                 // set editor height
            minHeight: null,             // set minimum height of editor
            maxHeight: null,             // set maximum height of editor
            focus: false                 // set focus to editable area after initializing summernote
        });
        $(window).keydown(function(event){
            if(event.keyCode == 13) {
            event.preventDefault();
            return false;
            }
        });
        
    });
    </script>
    <script>
        var i = {{ $i }} ; j = {{ $j }}; k = {{ $k }}; l = {{ $l }}; m= {{ $m }}; n = {{ $n }};
        $(document).on('click', '.btn-add-item-faq', function() {
            var html = `
                <div class="item" data-number="${i}">
                    <div class="row">
                        <div class="col-md-5">
                            <input type="text" name="faqs[${i}][title]" class="form-control" placeholder="Eg: Can I bring my pet?" required>
                            </div>
                        <div class="col-md-6">
                            <textarea name="faqs[${i}][content]" class="form-control" placeholder="" required></textarea>
                        </div>
                        <div class="col-md-1">
                            <span class="btn btn-danger btn-sm btn-remove-item"><i class="fa fa-trash"></i></span>
                        </div>
                    </div>
                </div>
            `;
            $('.g-items-faq').append(html);
            i++;
        });
        $(document).on('click', '.btn-add-item-include', function() {
            var html = `
                <div class="item" data-number="${j}">
                    <div class="row">
                        <div class="col-md-11">
                            <input type="text" name="include[${j}][title]" class="form-control" placeholder="Eg: Specialized bilingual guide" required>
                        </div>
                        <div class="col-md-1">
                            <span class="btn btn-danger btn-sm btn-remove-item"><i class="fa fa-trash"></i></span>
                        </div>
                    </div>
                </div>
            `;
            $('.g-items-include').append(html);
            j++;
        });
        $(document).on('click', '.btn-add-item-exclude', function() {
            var html = `
                <div class="item" data-number="${k}">
                    <div class="row">
                        <div class="col-md-11">
                            <input type="text" name="exclude[${k}][title]" class="form-control" placeholder="Eg: Specialized bilingual guide" required>
                        </div>
                        <div class="col-md-1">
                            <span class="btn btn-danger btn-sm btn-remove-item"><i class="fa fa-trash"></i></span>
                        </div>
                    </div>
                </div>
            `;
            $('.g-items-exclude').append(html);
            k++;
        });
        $(document).on('click', '.btn-add-item-extra-price', function() {
            var html = `
                <div class="item" data-number="${m}">
                    <div class="row">
                        <div class="col-md-5"> 
                            <input type="text" name="extra_price[${m}][title]" class="form-control" value="" placeholder="Extra price name" required>
                        </div>
                        <div class="col-md-3">
                            <input type="number" min="0" name="extra_price[${m}][price]" class="form-control" value="" required>
                        </div>
                        <div class="col-md-3 flex-column">
                            <select name="extra_price[${m}][type]" class="form-control">
                                <option value="0">One-time</option>
                                <option value="1">Per hour</option>
                                <option value="2">Per day</option>
                            </select>
                            <label>
                                <input type="checkbox" name="extra_price[${m}][per_person]" value="1">
                            Price per person
                            </label>
                        </div>
                        <div class="col-md-1">
                            <span class="btn btn-danger btn-sm btn-remove-item"><i class="fa fa-trash"></i></span>
                        </div>
                    </div>
                </div>
            `;
            $('.g-items-extra-price').append(html);
            m++;
        });
        $(document).on('click', '.btn-add-item-person-types', function() {
            var html = `
                <div class="item" data-number="${l}">
                    <div class="row">
                        <div class="col-md-5 flex-column">
                            <input type="text" name="person_types[${l}][title]" class="form-control" value="" placeholder="Eg: Adults" required>
                            <input type="text" name="person_types[${l}][desc]" class="form-control" value="" placeholder="Description" required>
                        </div>
                        <div class="col-md-2">
                            <input type="number" min="0" name="person_types[${l}][min]" class="form-control" value="" placeholder="Minimum per booking" required>
                        </div>
                        <div class="col-md-2">
                            <input type="number" min="0" name="person_types[${l}][max]" class="form-control" value="" placeholder="Maximum per booking" required>
                        </div>
                        <div class="col-md-2">
                            <input type="number" min="0" name="person_types[${l}][price]" class="form-control" value="" placeholder="per 1 item" required>
                        </div>
                        <div class="col-md-1">
                            <span class="btn btn-danger btn-sm btn-remove-item"><i class="fa fa-trash"></i></span>
                        </div>
                    </div>
                </div>
            `;
            $('.g-items-person-types').append(html);
            l++;
        });
        $(document).on('click', '.btn-add-item-discount', function() {
            var html = `
                <div class="item" data-number="${n}">
                    <div class="row">
                        <div class="col-md-2">
                            <input type="number" min="0" name="discount_by_people[${n}][from]" class="form-control" value="" placeholder="From" required>
                        </div>
                        <div class="col-md-2">
                            <input type="number" min="0" name="discount_by_people[${n}][to]" class="form-control" value="" placeholder="To" required>
                        </div>
                        <div class="col-md-4">
                            <input type="number" min="0" name="discount_by_people[${n}][amount]" class="form-control" value="" required>
                        </div>
                        <div class="col-md-3">
                            <select name="discount_by_people[${n}][type]" class="form-control">
                                <option value="0">Fixed</option>
                                <option value="1">Percent</option>
                            </select>
                        </div>
                        <div class="col-md-1">
                            <span class="btn btn-danger btn-sm btn-remove-item"><i class="fa fa-trash"></i></span>
                        </div>
                    </div>
                </div>
            `;
            $('.g-items-discount').append(html);
            n++;
        });
        var q = {{ $q }};
        $(document).on('click', '.btn-add-item-itinerary', function() {
            var html = `
                <div class="item" data-number="${q}">
                    <div class="row">
                        <div class="col-md-2 col-itinerary-image">
                            <span class="input-group-btn">
                                <button type="button" class="btn btn-primary btn-show-modal-choose-itinerary" data-toggle="modal" data-target="#modal-itinerary-image-${q}">Select image</button>
                                <input type="hidden" class="itinerary-image" name="itinerary[${q}][image]" id="itinerary-image-${q}" required>
                            </span>
                            <div id="preview_itinerary-image-${q}" class="mt-3 show-itinerary-image">
                    
                            </div>
                            <div class="modal fade test modal-itinerary-image" id="modal-itinerary-image-${q}" tabindex="-1" role="dialog" data-backdrop="false" aria-hidden="true">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">Manger Image</h5>
                                            <button type="button" class="close" data-dismiss="modal"><i class="fas fa-times"></i></button>
                                        </div>
                                        <div class="modal-body">
                                            <iframe src="{{ config('app.url') }}/filemanager/dialog.php?akey=4Ans6pfTs6A9DzHj8AZq1NVGKoYxpyoI8ivCN1bOow&field_id=itinerary-image-${q}&multiple=0" frameborder="0" style="width: 100%; height: 500px; overflow-y: auto"></iframe>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 flex-column">
                            <input type="text" name="itinerary[${q}][title]" class="form-control" placeholder="Title: Day 1" required>
                            <input type="text" name="itinerary[${q}][desc]" class="form-control" placeholder="Desc: TP. HCM City" required>
                        </div>
                        <div class="col-md-5">
                            <textarea name="itinerary[${q}][content]" class="form-control full-h" placeholder="..." required></textarea>
                        </div>
                        <div class="col-md-1">
                            <span class="btn btn-danger btn-sm btn-remove-item"><i class="fa fa-trash"></i></span>
                        </div>
                    </div>
                </div>
            `;
            $('.g-items-itinerary').append(html);
            q++;
        });

        $(document).on('hide.bs.modal', '.modal-itinerary-image', function () {
          var _img = $(this).parents('.col-itinerary-image').children('.input-group-btn').children('.itinerary-image').val();
          
          if (!_img.length) {
                $(this).parents('.col-itinerary-image').children('.show-itinerary-image').empty();
            } else {
                $(this).parents('.col-itinerary-image').children('.show-itinerary-image').empty();
                $html = `
                    <div class="box_imgg position-relative">
                        <img src="${_img}"  style="width:100%;">
                        <i class="fas fa-times-circle style_icon_remove style_icon_remove_itinerary" title="delete"></i>
                    </div>
                `;
                $(this).parents('.col-itinerary-image').children('.show-itinerary-image').append($html);
                $(this).parents('.col-itinerary-image').children('.input-group-btn').children('.btn-show-modal-choose-itinerary').addClass('d-none');
                $(this).parents('.col-itinerary-image').children('.input-group-btn').children('.btn-show-modal-choose-itinerary').removeClass('d-block');
            }
          
        });
        $(document).on('click', '.style_icon_remove_itinerary', function() {
            $(this).parent('.box_imgg').hide('slow', function () { 
                $(this).parent('.show-itinerary-image').parent('.col-itinerary-image').children('.input-group-btn').children('.itinerary-image').val('');
                $(this).parents('.col-itinerary-image').children('.input-group-btn').children('.btn-show-modal-choose-itinerary').addClass('d-block');
                $(this).parents('.col-itinerary-image').children('.input-group-btn').children('.btn-show-modal-choose-itinerary').removeClass('d-none');
                $(this).remove(); 
            });
        });
        $(document).on('click', '.btn-remove-item', function(){
            $(this).parents('.item').remove();
        });
        $(document).on('click', '.enable_open_hours', function(e) {
            if($(this).is(':checked', true))
            {
                $(".table-enable_open_hours").removeClass('d-none');
                $(".table-enable_open_hours").addClass('d-block');
            } else {
                $(".table-enable_open_hours").addClass('d-none');
                $(".table-enable_open_hours").removeClass('d-block');
            }
        });
        $(document).on('hide.bs.modal', '.bd-example-modal-avatar', function () {
            var _img = $('input#avatar').val();
            if (!_img.length) {
                $('#preview_avatar').empty();
            } else {
                $('#preview_avatar').empty();
                $html = `
                    <div class="box_imgg position-relative">
                        <img src="" id='show-img-avatar' style="width:100%;">
                        <i class="fas fa-times-circle style_icon_remove style_icon_remove_avatar" title="delete"></i>
                    </div>
                `;
                $('#preview_avatar').append($html);
                $('#show-img-avatar').attr('src', _img);
            }
        });
        $(document).on('click', '.style_icon_remove_avatar', function() {
            $(this).parent('.box_imgg').hide('slow', function () { 
                $(this).remove(); 
                $('#avatar').val('');
            });
        });
   
       $(document).on('hide.bs.modal', '.bd-example-modal-album', function () {
            var _img = $('input#album').val();
            if (!_img.length) {
                $('#preview_album').empty();
            } else {
                if(_img[0] == '[') {
                    var array = JSON.parse(_img);
                    $('#row_preview_album').empty();
                    var html = '';
                    $.each(array, function( index, value ) {
                        html += `
                            <div class="col-3 mt-3">
                                <div class="box_imgg position-relative">
                                    <img src="${value}" class="img-height-110" style="width:100%; height=110px;">
                                    <i class="fas fa-times-circle style_icon_remove style_icons_remove_album" title="delete"></i>
                                </div>
                            </div>
                        `;
                    });
                    $('#row_preview_album').append(html);

                }else{
                    
                    $html = `
                        <div class="col-3 mt-3">
                            <div class="box_imgg position-relative">
                                <img src="" id='show-img-album' style="width:100%;">
                                <i class="fas fa-times-circle style_icon_remove style_icons_remove_album" title="delete"></i>
                            </div>
                        </div>
                    `;
                    $('#row_preview_album').empty();
                    $('#row_preview_album').append($html);
                    $('#show-img-album').attr('src', _img);
                    var str_src = '';  
                    str_src += "[\"";
                    str_src += _img;
                    str_src += "\"]";
                    $('#album').val(str_src);
                }
                
            }
        });
        $(document).on('click', '.style_icons_remove_album', function() {
            $(this).parents('.col-3').hide('slow', function () { 
                $(this).remove(); 
                var arr_image = [];
                var str_src = '';               
                $('.row_preview_album').find('.col-3').each(function () {
                    var str_image = '';
                    str_image += '"';
                    str_image += $(this).find('.img-height-110').attr('src');
                    str_image += '"';
                    arr_image.push(str_image);
                });
                str_src += "[";
                str_src += arr_image.toString();
                str_src += "]";
                if(arr_image.length == 0){
                    $('#album').val('');
                }else{
                    $('#album').val(str_src);
                }
            });
        });
        $(document).on('click', '.enable_person_types', function() {
            if($(this).is(':checked', true))
            {
                $(".table-person-type").removeClass('d-none');
                $(".table-person-type").addClass('d-block');
            } else {
                $(".table-person-type").addClass('d-none');
                $(".table-person-type").removeClass('d-block');
            } 
        })
    </script>
    <script>
        var bookingCore = {
            url: 'http://sandbox.bookingcore.org',
            map_provider: 'gmap',
            map_gmap_key: '',
            csrf: 'JMrM5NwcxQy6HWOmuo7LQ2kHPh7pGwfbOPpXxkue'
        };
    </script>

    <script src="https://maps.google.com/maps/api/js?key=AIzaSyDsucrEdmswqYrw0f6ej3bf4M4suDeRgNA"></script>
    <script src="{{ URL::asset('assets/libs/gmaps/gmaps.min.js') }}"></script>
    <script src="{{ URL::asset('assets/js/pages/google-maps.init.js') }}"></script>
    <script src="{{ URL::asset('assets/js/map-engine.js') }}"></script>
    <script>
        jQuery(function ($) {
            new BravoMapEngine('gmaps-basic', {
                fitBounds: true,
                center: [{{ $tourEdit->latitu ?? "51.505"}}, {{ $tourEdit->longtitu ?? "-0.09"}}],
                zoom:{{ $tourEdit->mapzoom ?? "8"}},
                ready: function (engineMap) {
                    @if($tourEdit->latitu && $tourEdit->longtitu)
                            engineMap.addMarker([{{ $tourEdit->latitu }}, {{ $tourEdit->longtitu }}], {
                            icon_options: {}
                        });
                    @endif
                    engineMap.on('click', function (dataLatLng) {
                        engineMap.clearMarkers();
                        engineMap.addMarker(dataLatLng, {
                            icon_options: {}
                        });
                        $("input[name=latitu]").attr("value", dataLatLng[0]);
                        $("input[name=longtitu]").attr("value", dataLatLng[1]);
                    });
                    engineMap.on('zoom_changed', function (zoom) {
                        $("input[name=mapzoom]").attr("value", zoom);
                    })
                }
            });
        })
    </script>
@endpush

