<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookingToursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('booking_tours', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tour_id')->unsigned()->nullable();
            $table->foreign('tour_id')->references('id')->on('tours')->onDelete('set null');
            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('set null');
            $table->tinyInteger('status')->default(1)->comment('0: completed, 1: processing, 2: confirmed, 3: cancelled, 4: paid, 5: unpaid, 6: partial_payment');
            $table->tinyInteger('payment_method')->default(0)->comment('0: offline payment');
            $table->date('start_date');
            $table->integer('duration');
            $table->integer('price');
            $table->tinyInteger('is_person_type');
            $table->integer('number');
            $table->integer('paid');
            $table->longText('special_requirements');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('booking_tours');
    }
}
