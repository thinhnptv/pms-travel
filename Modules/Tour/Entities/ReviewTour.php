<?php

namespace Modules\Tour\Entities;
use Modules\User\Entities\User;
use Modules\Tour\Entities\Tour;

use Illuminate\Database\Eloquent\Model;

class ReviewTour extends Model
{
    protected $fillable = [];

    public function user() 
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
    public function tour() 
    {
        return $this->belongsTo(Tour::class, 'tour_id', 'id');
    }
}
