<?php

namespace Modules\Tour\Entities;

use Illuminate\Database\Eloquent\Model;

class IncludeTour extends Model
{
    protected $table = 'include_tours';
    protected $fillable = [];
}
