<?php

namespace Modules\Tour\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Tour\Entities\Tour;

class OpenHourTour extends Model
{
    protected $fillable = [];
    public function tour() 
    {
        return $this->belongsTo(Tour::class, 'tour_id', 'id');
    }
}
