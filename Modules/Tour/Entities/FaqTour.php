<?php

namespace Modules\Tour\Entities;

use Illuminate\Database\Eloquent\Model;

class FaqTour extends Model
{
    protected $table = 'faq_tours';
    protected $fillable = [];
}
