<?php

namespace Modules\Tour\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\User\Entities\User;
use Modules\Category\Entities\Location;
use Modules\Tour\Entities\ReviewTour;
use Modules\Category\Entities\TourTerm;
use Modules\Category\Entities\TourCategory;
use Modules\Tour\Entities\FaqTour;
use Modules\Tour\Entities\IncludeTour;
use Modules\Tour\Entities\ExcludeTour;
use Modules\Tour\Entities\ItineraryTour;
use Modules\Tour\Entities\PersonTypeTour;
use Modules\Tour\Entities\ExtraPriceTour;
use Modules\Tour\Entities\DiscountOfPeopleTour;
use Modules\Tour\Entities\OpenHourTour;
use Modules\Tour\Entities\TourWishList;
use Modules\Tour\Entities\TourStartDate;
use Modules\Tour\Entities\Destination;
use Modules\Tour\Entities\TourStyleCategory;
use Modules\Tour\Entities\TourVehicleCategory;
use Modules\Tour\Entities\TourAvailability;
use App\Helpers\Filterable;
class Tour extends Model
{
    use Filterable;

    protected $table = 'tours';
    protected $fillable = [];

    protected $fillterable = ['id', 'location_id', 'destination_id'];

    public function user() 
    {
        return $this->belongsTo(User::class, 'vendor_id', 'id');
    }
    public function location() 
    {
        return $this->belongsTo(Location::class, 'location_id', 'id');
    }
    public function destination() 
    {
        return $this->belongsTo(Destination::class, 'destination_id', 'id');
    }
    public function tourCategory() 
    {
        return $this->belongsTo(TourCategory::class, 'category_id', 'id');
    }
    public function reviewTour() 
    {
        return $this->hasMany(ReviewTour::class, 'tour_id', 'id');
    }
    public function wishListTour() 
    {
        return $this->hasMany(TourWishList::class, 'tour_id', 'id');
    }
    public function startDateTour() 
    {
        return $this->hasMany(TourStartDate::class, 'tour_id', 'id');
    }
    public function termstyle()
    {
        return $this->belongsToMany(TourTerm::Class, 'tour_termstyles', 'tour_id', 'term_style_id');
    }
    public function facility()
    {
        return $this->belongsToMany(TourTerm::Class, 'tour_termfacilities', 'tour_id', 'term_factility_id');
    }
    public function faq() 
    {
        return $this->hasMany(FaqTour::class, 'tour_id', 'id');
    } 
    public function include() 
    {
        return $this->hasMany(IncludeTour::class, 'tour_id', 'id');
    } 
    public function exclude() 
    {
        return $this->hasMany(ExcludeTour::class, 'tour_id', 'id');
    } 
    public function itinerary() 
    {
        return $this->hasMany(ItineraryTour::class, 'tour_id', 'id');
    } 
    public function personType() 
    {
        return $this->hasMany(PersonTypeTour::class, 'tour_id', 'id');
    } 
    public function extraPrice() 
    {
        return $this->hasMany(ExtraPriceTour::class, 'tour_id', 'id');
    }
    public function discountOfPeople() 
    {
        return $this->hasMany(DiscountOfPeopleTour::class, 'tour_id', 'id');
    }
    public function openHour() 
    {
        return $this->hasMany(OpenHourTour::class, 'tour_id', 'id');
    }
    public function tourismType()
    {
        return $this->belongsToMany(TourStyleCategory::Class, 'tour_tour_style_category', 'tour_id', 'tour_style_category_id');
    }
    public function tourVehicle()
    {
        return $this->belongsToMany(TourVehicleCategory::Class, 'tour_tour_vehicle_category', 'tour_id', 'tour_vehicle_category_id');
    }
    public function availability() 
    {
        return $this->hasMany(TourAvailability::class, 'tour_id', 'id');
    } 
    public function wishlistTourBy($user)
    {
        return $this->wishListTour()->where('user_id',$user->id)->exists();
    }
}
