<?php

namespace Modules\Tour\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\User\Entities\User;
use Modules\Tour\Entities\Tour;
use Modules\Tour\Entities\TourExtraPriceBooked;
use Modules\Tour\Entities\TourPersonTypeBooked;

class BookingTour extends Model
{
    protected $fillable = [];
    
    public function user() 
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
    public function tour() 
    {
        return $this->belongsTo(Tour::class, 'tour_id', 'id');
    }
    public function extraPriceBooked() 
    {
        return $this->hasMany(TourExtraPriceBooked::class, 'booking_tour_id', 'id');
    }
    public function personTypeBooked() 
    {
        return $this->hasMany(TourPersonTypeBooked::class, 'booking_tour_id', 'id');
    }
}
