<?php

namespace Modules\Tour\Entities;

use Illuminate\Database\Eloquent\Model;

class TourVehicleCategory extends Model
{
    protected $table = 'tour_vehicle_categories';
    protected $fillable = [];
}
