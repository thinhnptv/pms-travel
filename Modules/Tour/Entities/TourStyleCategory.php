<?php

namespace Modules\Tour\Entities;

use Illuminate\Database\Eloquent\Model;

class TourStyleCategory extends Model
{
    protected $table ="tour_style_categories";
    protected $fillable = [];
}
