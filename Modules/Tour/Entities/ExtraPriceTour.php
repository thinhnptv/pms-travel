<?php

namespace Modules\Tour\Entities;

use Illuminate\Database\Eloquent\Model;

class ExtraPriceTour extends Model
{
    protected $table = 'extra_price_tours';
    protected $fillable = [];
}
