<?php
Route::group(['middleware' => ['web', 'adminLogin'], 'prefix' => 'admin', 'namespace' => 'Modules\Base\Http\Controllers'], function()
{
    Route::get('/', 'BaseController@index')->name('base.index');
});
