<!-- ========== Left Sidebar Start ========== -->
<div class="left-side-menu">

  <div class="slimscroll-menu">

	<!--- Sidemenu -->
	<div id="sidebar-menu">

	  <ul class="metismenu" id="side-menu">

		<li class="menu-title">Navigation</li>

		<li>
		  <a href="javascript: void(0);">
			<i class="fe-airplay"></i>
			<span class="badge badge-success badge-pill float-right">4</span>
			<span> Dashboards </span>
		  </a>
		  <ul class="nav-second-level" aria-expanded="false">
			<li>
			  <a href="index">Dashboard 1</a>
			</li>
			<li>
			  <a href="dashboard-2">Dashboard 2</a>
			</li>
			<li>
			  <a href="dashboard-3">Dashboard 3</a>
			</li>
			<li>
			  <a href="dashboard-4">Dashboard 4</a>
			</li>
		  </ul>
		</li>

		<li>
		  <a href="javascript: void(0);">
			<i class="fe-pocket"></i>
			<span> Apps </span>
			<span class="menu-arrow"></span>
		  </a>
		  <ul class="nav-second-level" aria-expanded="false">
			<li>
			  <a href="apps-kanbanboard">Kanban Board</a>
			</li>
			<li>
			  <a href="apps-calendar">Calendar</a>
			</li>
			<li>
			  <a href="apps-contacts">Contacts</a>
			</li>
			<li>
			  <a href="apps-projects">Projects</a>
			</li>
			<li>
			  <a href="apps-tickets">Tickets</a>
			</li>
			<li>
			  <a href="apps-companies">Companies</a>
			</li>
		  </ul>
		</li>

		<li>
		  <a href="javascript: void(0);">
			<i class="fe-pocket"></i>
			<span> Space </span>
		  <span class="menu-arrow"></span>
		  </a>
		  <ul class="nav-second-level" aria-expanded="false">
			  <li>
				  <a href="{{ route('spaces.list') }}">All Spaces</a>
			  </li>
			<li>
			  <a href="{{ route('spaceAttributes.list') }}">Attribute</a>
			</li>
			  <li>
				  <a href="{{ route('spaces.review.list') }}">Reviews Spaces</a>
			  </li>
			  <li>
				  <a href="{{ route('spaces.booking.list') }}">Bookings Spaces</a>
			  </li>

		  </ul>
		</li>
		<li>
		  <a href="javascript: void(0);">
			<i class="fe-pocket"></i>
			<span> Hotel </span>
			<span class="menu-arrow"></span>
		  </a>
		  <ul class="nav-second-level" aria-expanded="false">
			<li>
				<a href="{{ route('hotel.list') }}">All Hotel</a>
			</li>
			<li>
				<a href="{{ route('hotel.get_add') }}">Add New Hotel</a>
			</li>
			<li>
				<a href="{{ route('hotelAttributes.index') }}">Attributes</a>
			</li>
			<li>
				<a href="{{ route('type') }}">Type of Hotel</a>
			</li>
			<li>
			  	<a href="{{ route('review.list') }}">Reviews Hotel</a>
			</li>
			<li>
			  	<a href="{{ route('roomAttributes.list') }}">Room Attributes</a>
			</li>
			<li>
			  	<a href="{{ route('room.list_booking_rooms') }}">Booking Report</a>
			</li>
		  </ul>
		</li>
		<li>
		<a href="javascript: void(0);">
			<i class="fe-pocket"></i>
			<span> Page </span>
			<span class="menu-arrow"></span>
			</a>
			<ul class="nav-second-level" aria-expanded="false">
			<li>
				<a href="{{ route('page.list') }}">All Page</a>
			</li>
			<li>
				<a href="{{ route('page.get_add') }}">Add Page</a>
			</li>
			</ul>
		</li>
		<li>
		  <a href="javascript: void(0);">
			<i class="fas fa-book"></i>
			<span> News </span>
			<span class="menu-arrow"></span>
		  </a>
		  <ul class="nav-second-level" aria-expanded="false">
			<li>
			  <a href="{{ route('articles.list_article') }}">All News</a>
			</li>
			<li>
			  <a href="{{ route('tags.list_tag') }}">Tags</a>
			</li>
			<li>
			  <a href="apps-contacts">Comments</a>
			</li>
		  </ul>
		</li>
		<li>
		  <a href="javascript: void(0);">
			<i class="fe-pocket"></i>
			<span> Car </span>
			<span class="menu-arrow"></span>
		  </a>
		  <ul class="nav-second-level" aria-expanded="false">
			<li>
				<a href="{{ route('car.list') }}">List Car</a>
			</li>
			<li>
				<a href="{{ route('car.get_add') }}">Add New Car</a>
			</li>
			<li>
			  <a href="{{ route('carAttributes.list') }}">Attributes</a>
            </li>
            <li>
                <a href="{{ route('car.brand-list') }}">Car Brand</a>
              </li>
			<li>
			  <a href="{{ route('car.review.list') }}">Review Car</a>
			</li>
			<li>
			  <a href="{{ route('car.booking.list') }}">List Booking Car</a>
			</li>
		  </ul>
		</li>
		<li>
		  <a href="{{ route('locationCategories.index') }}">
			<i class="fas fa-book"></i>
			<span> Location </span>
		  </a>
		</li>
		<li>
		  <a href="javascript: void(0);">
			<i class="fas fa-book"></i>
			<span> Users </span>
			<span class="menu-arrow"></span>
		  </a>
		  <ul class="nav-second-level" aria-expanded="false">
			<li>
			  <a href="{{ route('user.list') }}">All Users</a>
			</li>
			<li>
			  <a href="{{ route('user.list_vendor') }}">List Vendor</a>
			</li>
			<li>
			  <a href="{{ route('role.list') }}">List Role</a>
			</li>
			<li>
			  <a href="{{ route('vendorrequest.list') }}">List Vendor Request</a>
			</li>
			<li>
			  <a href="{{ route('customer_review.list') }}">Customer Review</a>
			</li>
		  </ul>
		</li>
		<li>
		  <a href="javascript: void(0);">
			<i class="fas fa-book"></i>
			<span> Comments </span>
			<span class="menu-arrow"></span>
		  </a>
		  <ul class="nav-second-level" aria-expanded="false">
			<li>
			  <a href="{{ route('comment.list') }}">All Comments</a>
			</li>
		  </ul>
		</li>
		<li>
			<a href="javascript: void(0);">
			  <i class="fe-pocket"></i>
			  <span> Tour </span>
			  <span class="menu-arrow"></span>
			</a>
			<ul class="nav-second-level" aria-expanded="false">
			  	<li>
				  <a href="{{ route('tour.list') }}">List Tour</a>
				</li>
				<li>
					<a href="{{ route('tour.get_add') }}">Add new tour</a>
				</li>
				<li>
					<a href="{{ route('tourCategories.list') }}">List cate</a>
				</li>
				<li>
					<a href="{{ route('tourAttributes.index') }}">Attribute Tour</a>
				</li>
				<li>
					<a href="{{ route('tour.review.list') }}">Review Tour</a>
				</li>
				<li>
					<a href="{{ route('tour.booking.list') }}">List Bookings</a>
				</li>
				<li>
					<a href="{{ route('tour.destinations') }}">Destinations</a>
				</li>
			</ul>
		  </li>
		  <li>
			<a href="javascript: void(0);">
			  <i class="fe-pocket"></i>
			  <span> Contact </span>
			  <span class="menu-arrow"></span>
			</a>
			<ul class="nav-second-level" aria-expanded="false">
			  <li>
				  <a href="{{ route('list_contact') }}"> List Contact</a>
			  </li>
			  <li>
				  <a href="{{ route('list-subscriber') }}"> List Subscriber</a>
			  </li>
			</ul>
		  </li>
		  <li>
			<a href="javascript: void(0);">
			  <i class="fe-pocket"></i>
			  <span>Settings </span>
			  <span class="menu-arrow"></span>
			</a>
			<ul class="nav-second-level" aria-expanded="false">
			  <li>
				  <a href="{{ route('contact.setting.edit') }}"> General Settings</a>
			  </li>
			</ul>
		  </li>
	  </ul>
	</div>
	<!-- End Sidebar -->

	<div class="clearfix"></div>

  </div>
  <!-- Sidebar -left -->

</div>
<!-- Left Sidebar End -->
