<?php

namespace Modules\GeneralSettings\Entities;

use Illuminate\Database\Eloquent\Model;

class ContactSetting extends Model
{
    protected $table = 'contact_setting';
    protected $fillable = ['id', 'title', 'sub_title', 'email', 'fax', 'tel', 'company_office', 'created_at', 'updated_at'];
}
