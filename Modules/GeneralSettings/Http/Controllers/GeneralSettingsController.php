<?php

namespace Modules\GeneralSettings\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\GeneralSettings\Entities\ContactSetting;

class GeneralSettingsController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    protected $modelContactSetting;

    public function __construct(ContactSetting $modelContactSetting)
    {
        $this->modelContactSetting = $modelContactSetting;
    }
    public function index()
    {
        return view('generalsettings::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {

        return view('generalsettings::setting.add');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $addContactSetting = new $this->modelContactSetting;
        $addContactSetting->title = $request->title;
        $addContactSetting->sub_title = $request->sub_title;
        $addContactSetting->email = $request->email;
        $addContactSetting->fax = $request->fax;
        $addContactSetting->tel = $request->tel;
        $addContactSetting->company_office = $request->company_office;
        $addContactSetting->map = $request->map;
        $addContactSetting->save();
        return redirect()->route('home.setting.general.get')->with('success', 'Thêm mới thành công');
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('generalsettings::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id = 1)
    {
        $editContact = $this->modelContactSetting::query()->findOrFail($id);
        // dd($editContact);
        return view('generalsettings::setting.edit', compact('editContact'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update($id = 1, Request $request)
    {
        $updateContactSetting = $this->modelContactSetting->findOrFail($id);
        // dd($updateContactSetting);
        $updateContactSetting->title = $request->title;
        $updateContactSetting->sub_title = $request->sub_title;
        $updateContactSetting->email = $request->email;
        $updateContactSetting->fax = $request->fax;
        $updateContactSetting->tel = $request->tel;
        $updateContactSetting->company_office = $request->company_office;
        $updateContactSetting->map = $request->map;
        $updateContactSetting->save();
        return redirect()->route('contact.setting.edit')->with('success', 'Update thành công');
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
}
