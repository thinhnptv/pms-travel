<?php

Route::group(['middleware' => ['web', 'adminLogin'], 'prefix' => 'admin/generalsettings', 'namespace' => 'Modules\GeneralSettings\Http\Controllers'], function()
{
    Route::get('/', 'GeneralSettingsController@index');
    Route::get('/contact-setting', 'GeneralSettingsController@create')->name('home.setting.general.get');
    Route::post('/contact-setting', 'GeneralSettingsController@store')->name('home.setting.general.post');
    Route::get('/save-update-contact', 'GeneralSettingsController@edit')->name('contact.setting.edit');
    Route::post('/save-update-contact', 'GeneralSettingsController@update')->name('contact.setting.update');
});
