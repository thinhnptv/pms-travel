@extends('base::layouts.master')
@section('css')
        <!-- Plugins css-->
        <link href="{{ URL::asset('assets/libs/select2/select2.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ URL::asset('assets/libs/summernote/summernote.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ URL::asset('assets/libs/dropzone/dropzone.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ URL::asset('assets/css/webt.css')}}" rel="stylesheet" type="text/css" />
@endsection
@section('content')
<div class="container-fluid">

    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="javascript: void(0);">UBold</a></li>
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Apps</a></li>
                        <li class="breadcrumb-item active">Contact Setting</li>
                    </ol>
                </div>
                <h4 class="page-title">Contact Setting</h4>
            </div>
        </div>
    </div>
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @if (session('errorsss'))
            <div class="alert alert-danger">
                {{ session('errorsss') }}
            </div>
        @endif
    <!-- end page title -->
    <form action="{{ route('home.setting.general.post') }}" method="POST" enctype="multipart/form-data">
        <input type="hidden" name="_token" value="{{csrf_token()}}" /> 
        <div class="row">
            <div class="col-md-12 col-lg-9">
                <div class="card p-2">
                    <div class="row">
                        <div class="col-md-12 col-lg-12">
                            {{-- <h1>Page contact settings</h1> --}}
                            <h4>Page contact settings</h4>
                            <div class="form-group mb-3">
                                <label for="title">Contact Title </label>
                                <input type="text" id="title" name="title" class="form-control" placeholder="Enter title...." >
                            </div> 
                            <div class="form-group mb-3">
                                <label for="sub_title">Contact Sub Title </label>
                                <input type="text" id="sub_title" name="sub_title" class="form-control" placeholder="enter sub title....">
                            </div> 
                            <div class="form-group mb-3">
                                <label for="company_office">Company Office</label>
                                <input type="text" id="company_office" name="company_office" class="form-control" placeholder="">
                            </div> 
                            <div class="form-group mb-3">
                                <label for="tel">Telephone </label>
                                <input type="text" id="tel" name="tel" class="form-control" placeholder="">
                            </div> 
                            <div class="form-group mb-3">
                                <label for="fax">Fax</label>
                                <input type="text" id="fax" name="fax" class="form-control" placeholder="">
                            </div> 
                            <div class="form-group mb-3">
                                <label for="email">Email</label>
                                <input type="text" id="email" name="email" class="form-control" placeholder="">
                            </div> 
                            <div class="form-group mb-3">
                                <label for="map">Map</label>
                                <input type="text" id="map" name="map" class="form-control" placeholder="">
                            </div>
                        </div>
                    </div>     
                </div>
            </div>
        </div>
        <div class="form-group mb-3 text-right">
            <button type="submit" class="btn w-sm btn-success waves-effect waves-light">Save Changes</button>
        </div> 
    <!-- end row -->
    </form>
</div>



{{-- @section('js')
    <div class="modal fade bd-example-modal-image-banner" id="modal-abbum" tabindex="-1" role="dialog" data-backdrop="false" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Manger Image</h5>
                    <button type="button" class="close" data-dismiss="modal"><i class="fas fa-times"></i></button>
                </div>
                <div class="modal-body">
                    <iframe src="{{ config('app.url') }}/filemanager/dialog.php?akey=4Ans6pfTs6A9DzHj8AZq1NVGKoYxpyoI8ivCN1bOow&field_id=banner&multiple=0" frameborder="0" style="width: 100%; height: 500px; overflow-y: auto"></iframe>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade bd-example-modal-image-album" id="modal-abbum" tabindex="-1" role="dialog" data-backdrop="false" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Manger Image</h5>
                    <button type="button" class="close" data-dismiss="modal"><i class="fas fa-times"></i></button>
                </div>
                <div class="modal-body">
                    <iframe src="{{ config('app.url') }}/filemanager/dialog.php?akey=4Ans6pfTs6A9DzHj8AZq1NVGKoYxpyoI8ivCN1bOow&field_id=album&multiple=1" frameborder="0" style="width: 100%; height: 500px; overflow-y: auto"></iframe>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade bd-example-modal-image-feature" id="modal-abbum" tabindex="-1" role="dialog" data-backdrop="false" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Manger Image</h5>
                    <button type="button" class="close" data-dismiss="modal"><i class="fas fa-times"></i></button>
                </div>
                <div class="modal-body">
                    <iframe src="{{ config('app.url') }}/filemanager/dialog.php?akey=4Ans6pfTs6A9DzHj8AZq1NVGKoYxpyoI8ivCN1bOow&field_id=feature&multiple=0" frameborder="0" style="width: 100%; height: 500px; overflow-y: auto"></iframe>
                </div>
            </div>
        </div>
    </div>
@stop --}}
@endsection

@push('js')
    <!-- Summernote js -->
    <script src="{{ URL::asset('assets/libs/summernote/summernote.min.js') }}"></script>
    <!-- Select2 js-->
    <script src="{{ URL::asset('assets/libs/select2/select2.min.js') }}"></script>
    <!-- Dropzone file uploads-->
    <script src="{{ URL::asset('assets/libs/dropzone/dropzone.min.js') }}"></script>
    <!-- Init js -->
    <script>
        jQuery(document).ready(function(){
        $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
        });
        // Summernote
    
        $('#content').summernote({
            height: 180,                 // set editor height
            minHeight: null,             // set minimum height of editor
            maxHeight: null,             // set maximum height of editor
            focus: false                 // set focus to editable area after initializing summernote
        });
        
        // Select2
        $('.select2').select2();
    
        
    });
         $(document).on('click','.check_master', function(e) {
            if($(this).is(':checked',true))
            {
                $(".sub-chk").prop('checked', true);
            } else {
                $(".sub-chk").prop('checked',false);
            }
        });
        $(document).on('click','.page-link',function(e) {
            e.preventDefault();
            url_ = this.href;
            var url_string = location.href;
            var url = new URL(url_string);
            var role = url.searchParams.get("role");
            if(role != null){
                url_ = url_ + '&role='+role;
            }
            window.location.href=url_;
        });

        
    </script>
    <script>
        $( document ).ready(function() {
            var url_string = location.href;
            var url = new URL(url_string);
            var role = url.searchParams.get("role");
            if(role != null){
                $('.filter_role').val(role);
            }
             
         });
     </script>
     {{-- ........................ --}}


      <script>
    var bookingCore = {
        url: 'http://sandbox.bookingcore.org',
        map_provider: 'gmap',
        map_gmap_key: '',
        csrf: 'JMrM5NwcxQy6HWOmuo7LQ2kHPh7pGwfbOPpXxkue'
    };
    var i18n = {
        warning: "Warning",
        success: "Success",
        confirm_delete: "Do you want to delete?",
        confirm: "Confirm",
        cancel: "Cancel",
    };
    </script>
    <script src="https://maps.google.com/maps/api/js?key=AIzaSyDsucrEdmswqYrw0f6ej3bf4M4suDeRgNA"></script>
    <script src="{{ URL::asset('assets/libs/gmaps/gmaps.min.js') }}"></script>
    <script src="{{ URL::asset('assets/js/pages/google-maps.init.js') }}"></script>
    <script src="{{ URL::asset('assets/js/map-engine.js') }}"></script>
    <script>
    jQuery(function ($) {
        new BravoMapEngine('gmaps-basic', {
            fitBounds: true,
            center: [{{"51.505"}}, {{"-0.09"}}],
            zoom:{{"8"}},
            ready: function (engineMap) {
                console.log(1);

                engineMap.on('click', function (dataLatLng) {
                    engineMap.clearMarkers();
                    engineMap.addMarker(dataLatLng, {
                        icon_options: {}
                    });
                    $("input[name=latitu]").attr("value", dataLatLng[0]);
                    $("input[name=longtitu]").attr("value", dataLatLng[1]);
                });
                engineMap.on('zoom_changed', function (zoom) {
                    $("input[name=zoom]").attr("value", zoom);
                })
            }
        });
    })
    </script>

@endpush
   