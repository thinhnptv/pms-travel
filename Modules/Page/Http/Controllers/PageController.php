<?php

namespace Modules\Page\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Page\Entities\Page;

class PageController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    protected $modelPage;

    public function __construct(Page $modelPage)
    {
        $this->modelPage = $modelPage;
    }
    public function index(Request $request)
    {
        $listPage = $this->modelPage::query();
        if(!empty($request->title)){
            $name = $request->title;
            $listPage->where('title', 'like', '%' .$name. '%');
        }
        $listPage = $listPage->get();
        return view('page::pages.list', compact('listPage'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('page::pages.add');

    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $str_avatar = '';
        if ($request->input('avatar')) {
            $file_url = parse_url($request->input('avatar'));
            $url = public_path($file_url['path']);
            $str_avatar = $file_url['path'];
        }
        $addPage = $this->modelPage;
        $addPage->title = $request->title;
        $addPage->content = $request->content;
        $addPage->status = $request->status;
        $addPage->style = $request->style;
        $addPage->avatar = $str_avatar;
        $addPage->slug = str_slug($request->title);
        $addPage->save();
        return redirect()->route('page.list')->with('success', 'Thêm mới thành công');
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('page::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {
        $page = $this->modelPage::query()->findOrFail($id);
        // dd($page);
        return view('page::pages.edit', compact('page'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update($id, Request $request)
    {
        $str_avatar = '';
        if ($request->input('avatar')) {
            $file_url = parse_url($request->input('avatar'));
            $url = public_path($file_url['path']);
            $str_avatar = $file_url['path'];
        }
        $updatePage = $this->modelPage->findOrFail($id);
        $updatePage->title = $request->title;
        $updatePage->content = $request->content;
        $updatePage->status = $request->status;
        $updatePage->style = $request->style;
        $updatePage->avatar = $str_avatar;
        $updatePage->slug = str_slug($request->title);
        $updatePage->save();
        return redirect()->route('page.list')->with('success', 'Update thành công');
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {
        // dd($this->modelPage->findorFail($id));
        $deletePage = $this->modelPage->findorFail($id);
        $deletePage->delete();
        return redirect()->route('page.list')->with('success', 'Xóa thành công');
    }
    public function status($id)
    {
        $page = $this->modelPage->findorFail($id);
        if ($page->status ==1) {
            $page->status = 0;
            $page->save();
            return response()->json([
                'type' => 1,
                'mess' => 'Chuyển thành công'
            ]);
        }else{
            $page->status =1;
            $page->save();
            return response()->json([
                'type' => 2,
                'mess' => 'Chuyển thành công'
            ]);
        }
    }
}
