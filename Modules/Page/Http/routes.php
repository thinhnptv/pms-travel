<?php

Route::group(['middleware' => ['web', 'adminLogin'], 'prefix' => 'admin/page', 'namespace' => 'Modules\Page\Http\Controllers'], function()
{
    Route::get('/', 'PageController@index')->name('page.list');
    Route::get('/add', 'PageController@create')->name('page.get_add');
    Route::post('/add', 'PageController@store')->name('page.post_add');
    Route::get('/edit/{id}', 'PageController@edit')->name('page.edit');
    Route::post('/edit/{id}', 'PageController@update')->name('page.update');
    Route::get('/delete/{id}', 'PageController@destroy')->name('page.delete');
    Route::post('/status/{id}', 'PageController@status')->name('page.status');
});
