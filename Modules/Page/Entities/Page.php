<?php

namespace Modules\Page\Entities;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    protected $table = 'pages';
    protected $fillable = ['id', 'title', 'banner', 'content', 'avatar', 'status', 'created_at', 'updated_at'];
}
